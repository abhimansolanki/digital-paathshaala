<?php

use App\Model\backend\UserType;
use App\Model\backend\Classes;
use App\Model\backend\School;
use App\Model\backend\StudentType;
use App\Model\backend\CasteCategory;
use App\Model\backend\Section;
use App\Model\backend\Session;
use App\Model\backend\FeeType;
use App\Model\backend\FeeCircular;
use App\Model\backend\Student;
use App\Model\backend\StudentHistory;
use App\Model\backend\Bank;
use App\Model\backend\PaymentMode;
use Illuminate\Support\Facades\Crypt;
use App\Model\backend\StudentAttendance;
use App\Model\backend\UserRole;
use App\Model\backend\Department;
use App\Model\backend\Shift;
use App\Model\backend\Holiday;
use App\Model\backend\Employee;
use App\Model\backend\ExamCategory;
use App\Model\backend\ExamType;

//    use App\Model\backend\CreateExam;
//    use App\Model\backend\SubjectGroup;
use App\Model\backend\CoScholastic;
use App\Model\backend\Grade;
use App\Model\backend\Vehicle;
use App\Model\backend\DocumentType;

//    use App\Model\backend\ClassSection;
use Session as LSession;
use Carbon\Carbon;

//    skoozin_school_diary
//skoozin_s_diary
//=(o6?aBhJnq{

function p($p, $exit = 1)
{
    echo '<pre>';
    print_r($p);
    echo '</pre>';
    if ($exit == 1) {
        exit;
    }
}

function set_session($session_id, $session_year)
{
    $session_data = array(
        'session_id' => $session_id,
        'session_year' => $session_year,
    );
    LSession::put('set_session_year', $session_data);
    return LSession::get('set_session_year');
}

function get_loggedin_user_data()
{
    $user_data = array();
    $admin_user = Auth::guard('admin')->user();
    if (!empty($admin_user['admin_user_id'])) {
        $admin_user_type = UserType::Find($admin_user['user_type_id']);
        $arr_allowed_user = \Config::get('custom.allowed_admin_user_login');

        $user_data = array(
            'admin_user_id' => $admin_user['admin_user_id'],
            'password' => $admin_user['password'],
            'user_name' => $admin_user['user_name'],
            'email' => $admin_user['email'],
            'user_type_id' => $admin_user['user_type_id'],
            'user_type' => $admin_user_type['user_type'],
            'allowed_user' => $arr_allowed_user,
        );
    }
    return $user_data;
}

function last_query($raw = false, $last = false)
{
    $log = DB::getQueryLog();
    if ($raw) {
        foreach ($log as $key => $query) {
            $log[$key] = vsprintf(str_replace('?', "'%s'", $query['query']), $query['bindings']);
        }
    }
    //         return ($last) ? end($log) : $log; // return single table query
    return $log;  // return join tables queries
}

function check_empty($value, $type)
{
    if (!empty($value)) {
        return $value;
    } else {
        if ($type == 'int') {
            return $value;
        } else {
            return '';
        }
    }
}

//    function get_gallery_event_image($image_name)
//    {
//        $return = '';
//        if (!empty($image_name))
//        {
//            $data['event_gallery_config'] = \Config::get('custom.event_gallery');
//            $image_url                    = url($data['event_gallery_config']['upload_path'] . $image_name);
//
////            if (File::isFile($image_url))
////            {
//            $return = $image_url;
////            }
//        }
//        return $return;
//    }

function get_class_order()
{

    $arr_classes = [];
    $arr_existing_classes = Classes::where(array('class_status' => 1))->select('class_name', 'class_order', 'class_id')->orderBy('class_order', 'ASC')->get();
    foreach ($arr_existing_classes as $existing_class) {
        $arr_classes[$existing_class['class_id'] . '_' . $existing_class['class_order']] = $existing_class['class_name'];
    }
    return $arr_classes;
}

function get_all_classes($class_id = null)
{
    $arr_classes = [];

    $arr_classes_list = Classes::where('class_status', 1)
        ->where(function ($query) use ($class_id) {
            if (!empty($class_id)) {
                $query->where('class_id', $class_id);
            }
        })->select('class_name', 'class_order', 'class_id')
        ->orderBy('class_order', 'ASC')->get();
    if (!empty($arr_classes_list)) {
        foreach ($arr_classes_list as $arr_class) {
            $arr_classes[$arr_class['class_id']] = $arr_class['class_name'];
        }
    }
    return $arr_classes;
}

function add_blank_option($arr, $option)
{
    $arr_option = array();
    if (!empty($option)) {
        $arr_option[''] = $option;
    } else {
        $arr_option[''] = '';
    }
    // operator on array
    $result = $arr_option + $arr;

    return $result;
}

function get_formatted_date($date, $format = 'database')
{
    $return = '';
    if (!empty($date) && $date != '0000-00-00') {
        if ($format == 'display') {
            $date = Carbon::createFromFormat('Y-m-d', $date);
            $return = $date->format('d/m/Y');
        } else {
            $date = Carbon::createFromFormat('d/m/Y', $date);
            $return = $date->format('Y-m-d');
        }
    }
    return $return;
}

function get_caste_category()
{
    $arr_caste = [];
    $arr_caste_category = CasteCategory::where('caste_category_status', 1)->select('caste_category_id', 'caste_name')->get();
    if (!empty($arr_caste_category)) {
        foreach ($arr_caste_category as $arr_caste_category) {
            $arr_caste[$arr_caste_category['caste_category_id']] = $arr_caste_category['caste_name'];
        }
    }
    return $arr_caste;
}

function get_student_type()
{
    $student_type = [];
    $arr_student_type = StudentType::where('student_type_status', 1)->select('student_type_id', 'student_type')->get();
    if (!empty($arr_student_type)) {
        foreach ($arr_student_type as $arr_type) {
            $student_type[$arr_type['student_type_id']] = $arr_type['student_type'];
        }
    }
    return $student_type;
}

function get_section()
{
    $section = [];
    $arr_section = Section::where('section_status', 1)->select('section_id', 'section_name')->get();
    if (!empty($arr_section)) {
        foreach ($arr_section as $value) {
            $section[$value['section_id']] = $value['section_name'];
        }
    }
    return $section;
}

function get_session($switch = 'no')
{
    $updated_session = [];
    $arr_session = Session::orderBy('start_date', 'ASC')
        ->select('session_id', 'session_year')
        ->where(function ($q) use ($switch) {
            if ($switch == 'no') {
                $q->where('session_status', 1);
                $q->orWhere(function ($query) {
                    $query->where('start_date', '<=', date('Y-m-d'));
                    $query->where('end_date', '>=', date('Y-m-d'));
                });
            } elseif ($switch == 'current') {
                ;
                $q->where('session_status', 1);
            }
        })
        ->get();
    foreach ($arr_session as $session) {
        if (!empty($session->session_id)) {
            $updated_session[$session->session_id] = $session['session_year'];
        }
    }
    return $updated_session;
}

//    function get_arr_session()
//    {
//        $session     = [];
//        $arr_session = Session::select('session_id', 'session_year')->get();
//        if (!empty($arr_session))
//        {
//            foreach ($arr_session as $value)
//            {
//                $session[$value['session_id']] = $value['session_year'];
//            }
//        }
//        return add_blank_option($session, '--Select session --');
//    }

function get_encrypted_value($key, $encrypt = false)
{
    $encrypted_key = null;
    if (!empty($key)) {
        if ($encrypt == true) {
            $key = Crypt::encrypt($key);
        }
        $encrypted_key = $key;
    }
    return $encrypted_key;
}

function get_decrypted_value($key, $decrypt = false)
{
    $decrypted_key = null;
    if (!empty($key)) {
        if ($decrypt == true) {
            $key = Crypt::decrypt($key);
        }
        $decrypted_key = $key;
    }
    return $decrypted_key;
}

function get_fee_type($root_id = null)
{
    $fee_type = [];
    $arr_root_id = [];
    $arr_fee_type = FeeType::orderBy('order')->where('fee_type_status', 1)->where(function ($query) use ($root_id) {
        if (!empty($root_id)) {
            $query->where('root_id', $root_id);
        } elseif ($root_id === 0) {
            $query->where('root_id', 0);
            $query->whereNotIn('fee_type_id', array(1, 2));
        }
    })->select('fee_type_id', 'fee_type', 'root_id')->get();

    if (!empty($arr_fee_type)) {
        foreach ($arr_fee_type as $value) {
            $fee_type[$value['fee_type_id']] = $value['fee_type'];
            $arr_root_id[$value['root_id']] = $value['root_id'];
        }
        if ($root_id === null) {
            $arr_root_id = array_unique($arr_root_id);
            $fee_type = array_diff_key($fee_type, $arr_root_id);
        }
    }
    return $fee_type;
}

function get_fee_circular($fee_circular_id = null)
{
    $fee_circular = [];
    $arr_fee_circular = FeeCircular::where('fee_circular_status', 1)
        ->where(function ($q) use ($fee_circular_id) {
            if (!empty($fee_circular_id)) {
                $q->where('fee_circular_id', $fee_circular_id);
            }
        })->select('fee_circular_id', 'fee_circular')->get();
    if (!empty($arr_fee_circular)) {
        $arr_custom_fee_circular = get_fee_circular_custom();
        foreach ($arr_fee_circular as $value) {
            $fee_circular[$value['fee_circular_id']] = $arr_custom_fee_circular[$value['fee_circular']];
        }
    }
    return $fee_circular;
}

function get_student_list_by_class_id($class_id, $arr_student_id = array(), $section_id = null)
{
    $session_id = null;
    $arr_student_list = [];
    $session = get_current_session();
    if (!empty($session)) {
        $session_id = $session['session_id'];
        $type = 'model';
        $query = get_student_table($session_id, $type);
        $arr_student = $query
            ->where(function ($query) use ($class_id, $arr_student_id) {
                if (!empty($class_id) && !empty($arr_student_id)) {
                    $query->where('current_class_id', $class_id);
                    $query->orWhereIn('student_id', $arr_student_id);
                } elseif (!empty($class_id)) {
                    $query->where('current_class_id', $class_id);
                } elseif (!empty($arr_student_id)) {

                    $query->WhereIn('student_id', $arr_student_id);
                }
            })
            ->where('current_session_id', $session_id)
            ->where('student_left', "No")
            ->with(['getParent' => function ($q) {
                $q->select(['student_parent_id', 'father_first_name', 'father_last_name', 'care_of', 'guardian_first_name', 'guardian_last_name', 'mother_first_name', 'mother_last_name']);
            }])
            ->get();
        if (!empty($arr_student[0]->student_id)) {
            $arr_student_list = $arr_student;
        }
        //            }
    }
    return $arr_student_list;
}

function get_student_list_by_class_section($session_id, $class_id, $section_id = null)
{
    $student = [];
    if (!empty($session_id) && !empty($class_id)) {
        $type = 'table';
        $table = get_student_table($session_id, $type);
        $query = DB::table($table)->where('student_status', 1)
            ->select('student_id', 'first_name', 'middle_name', 'last_name', 'enrollment_number')
            ->where('current_session_id', $session_id)
            ->where('current_class_id', $class_id)
            ->where('student_left', 'No');
        if (!empty($section_id)) {
            $query->where('current_section_id', $section_id);
        }

        $arr_student = $query->get();
        if (!empty($arr_student)) {
            foreach ($arr_student as $value) {
                $value = (array)$value;
                $student[$value['student_id']] = $value['enrollment_number'] . ' : ' . $value['first_name'] . ' ' . $value['middle_name'] . ' ' . $value['last_name'];
            }
        }
    }
    return $student;
}

function get_student_data($arr_student_id = array(), $class_id = null, $session_id = null, $offset = null, $limit = null, $left_student_also = false)
{
    if (!is_array($arr_student_id)) {
        $arr_student_id = array($arr_student_id);
    }
    $student = [];
    $student_array = [];

    if (!empty($session_id)) {
        $type = 'table';
        $student_table = get_student_table($session_id, $type);
    } else {
        $session_status = 0;
        $session_id = null;
        $session = get_current_session();

        if (!empty($session)) {
            $session_id = $session['session_id'];
            $session_status = $session['session_status'];
        }
        if ($session_status == 1) {
            $student_table = 'students as s';
        } else {
            $student_table = 'student_histories as s';
        }
    }

    $arr_care_of = \Config::get('custom.care_of');
    $student_query = DB::table($student_table)
        ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
        ->join('caste_categories as cc', 'cc.caste_category_id', '=', 's.caste_category_id')
        ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
        ->join('sessions as ss', 'ss.session_id', '=', 's.current_session_id')
        ->leftJoin('sections as sec', 'sec.section_id', '=', 's.current_section_id')
        ->select('s.*', 'sp.*', 'c.class_name', 'sec.section_name', 'cc.caste_name', 'ss.session_year')
        ->where('s.student_status', 1);
    if(!$left_student_also){
        $student_query->where('s.student_left', "No");
    }
//    if ($session_status == 1) {
//        $student_query->where('s.student_left', "No");
//    }
    if (!empty($class_id)) {
        $student_query->where('s.current_class_id', $class_id);
    }
    if (!empty($session_id)) {
        $student_query->where('s.current_session_id', $session_id);
    }
    if (!empty($arr_student_id)) {
        $student_query->whereIn('s.student_id', $arr_student_id);
    }
    //        if (!empty($limit))
    //        {
    //            $student_query->offset($offset);
    //            $student_query->limit($limit);
    //        }
    $arr_student = $student_query->get();

    //                $a = last_query($showBindings = true, $showLastQuery = true);
    //                p($a);
    foreach ($arr_student as $key => $student) {
        $student_data = [];
        $student = (array)$student;
        if (!empty($student['student_id'])) {
            $student_data = array(
                'form_number' => $student['form_number'],
                'enrollment_number' => $student['enrollment_number'],
                'student_id' => $student['student_id'],
                'current_session_id' => $student['current_session_id'],
                'session_year' => $student['session_year'],
                'current_class_id' => $student['current_class_id'],
                'current_section_id' => $student['current_section_id'],
                'student_type_id' => $student['student_type_id'],
                'caste_category_id' => $student['caste_category_id'],
                'new_student' => $student['new_student'],
                'discount_date' => get_formatted_date($student['discount_date'], 'display'),
                'admission_date' => get_formatted_date($student['admission_date'], 'display'),
                'join_date' => get_formatted_date($student['join_date'], 'display'),
                'fee_discount' => $student['fee_discount'],
                'previous_due_fee' => $student['previous_due_fee'],
                'previous_due_fee_original' => $student['previous_due_fee_original'],
                'remark' => check_empty($student['remark'], 'varchar'),
                'fee_calculate_month' => $student['fee_calculate_month'],
                'student_left' => $student['student_left'],
                'reason' => $student['reason'],
                'admin_user_id' => $student['admin_user_id'],
                'student_parent_id' => $student['student_parent_id'],
                'care_of' => $student['care_of'],
                'sibling_exist' => $student['sibling_exist'],
                'care_of_rel' => $arr_care_of[$student['care_of']],
                'father_email' => $student['father_email'],
                'father_aadhaar_number' => $student['father_aadhaar_number'],
                'father_name' => $student['father_first_name'] . ' ' . $student['father_middle_name'] . ' ' . $student['father_last_name'],
                'father_first_name' => $student['father_first_name'],
                'father_middle_name' => $student['father_middle_name'],
                'father_last_name' => $student['father_last_name'],
                'father_occupation' => $student['father_occupation'],
                'father_contact_number' => $student['father_contact_number'],
                'father_income' => $student['father_income'],
                'mother_name' => $student['mother_first_name'] . ' ' . $student['mother_middle_name'] . ' ' . $student['mother_last_name'],
                'mother_email' => $student['mother_email'],
                'mother_aadhaar_number' => $student['mother_aadhaar_number'],
                'mother_first_name' => $student['mother_first_name'],
                'mother_middle_name' => $student['mother_middle_name'],
                'mother_last_name' => $student['mother_last_name'],
                'mother_occupation' => $student['mother_occupation'],
                'mother_contact_number' => $student['mother_contact_number'],
                'mother_income' => $student['mother_income'],
                'guardian_email' => $student['guardian_email'],
                'guardian_aadhaar_number' => $student['guardian_aadhaar_number'],
                'guardian_name' => $student['guardian_first_name'] . ' ' . $student['guardian_middle_name'] . ' ' . $student['guardian_last_name'],
                'guardian_first_name' => $student['guardian_first_name'],
                'guardian_middle_name' => $student['guardian_middle_name'],
                'guardian_last_name' => $student['guardian_last_name'],
                'guardian_occupation' => $student['guardian_occupation'],
                'guardian_contact_number' => $student['guardian_contact_number'],
                'guardian_relation' => $student['guardian_relation'],
                'guardian_contact_number' => $student['guardian_contact_number'],
                'father_income' => $student['father_income'],
                'app_access_fee_status' => $student['app_access_fee_status'],
                'first_name' => $student['first_name'],
                'student_name' => $student['first_name'] . ' ' . $student['middle_name'] . ' ' . $student['last_name'],
                'middle_name' => $student['middle_name'],
                'last_name' => $student['last_name'],
                'birth_place' => $student['birth_place'],
                'dob' => get_formatted_date($student['dob'], 'display'),
                'tc_date' => get_formatted_date($student['tc_date'], 'display'),
                'gender_name' => get_gender($student['gender'], 'student_gender'),
                'gender' => $student['gender'],
                'aadhaar_number' => $student['aadhaar_number'],
                'profile' => check_file_exist($student['profile_photo'], 'student_profile'),
                'previous_school_name' => $student['previous_school_name'],
                'previous_class_name' => $student['previous_class_name'],
                'previous_result' => $student['previous_result'],
                'tc_number' => $student['tc_number'],
                'address_line1' => $student['address_line1'],
                'address_line2' => $student['address_line2'],
                'caste_name' => $student['caste_name'],
                'class_name' => $student['class_name'],
                'religion' => $student['religion'],
                'student_doc' => !empty($student['student_doc']) ? explode(',', $student['student_doc']) : [],
            );
            if ($student_data['care_of'] == 1) {
                $student_data['care_of_name'] = $student_data['father_name'];
                $student_data['care_of_contact'] = $student_data['father_contact_number'];
            } elseif ($student_data['care_of'] == 2) {
                $student_data['care_of_name'] = $student_data['mother_name'];
                $student_data['care_of_contact'] = $student_data['mother_contact_number'];
            } else {
                $student_data['care_of_name'] = $student_data['guardian_name'];
                $student_data['care_of_contact'] = $student_data['guardian_contact_number'];
            }
            $student_array[] = $student_data;
        }
        //            }
    }
    return $student_array;
}

function get_current_session()
{
    $set_session = LSession::get('set_session_year');
    $session_id = null;
    if (!empty($set_session)) {
        $session_id = $set_session['session_id'];
    }
    $session_data = [];

    $arr_session = Session::where(function ($query) use ($session_id) {
        if (!empty($session_id)) {
            $query->where('session_id', $session_id);
        } else {
            $query->where('session_status', 1);
        }
    })->select('session_id', 'session_year', 'start_date', 'end_date', 'session_status')->first();
    if (!empty($arr_session)) {
        $start_date = $arr_session->start_date;
        $end_date = $arr_session->end_date;
        $arr_year = explode('-', $arr_session->session_year);
        $start_year = $arr_year[0];
        $end_year = $arr_year[1];
        $date = date($start_year . '-m-d');
        $date1 = date($end_year . '-m-d');
        if ($date >= $start_date && $date <= $end_date) {
            $attendance_date = $date;
        } elseif ($date1 >= $start_date && $date1 <= $end_date) {
            $attendance_date = $date1;
        } else {
            $attendance_date = date('Y-m-d');
        }
        $session_start_date = date_create($arr_session->start_date);
        $session_end_date = date_create($arr_session->end_date);
        $session_period = date_diff($session_start_date, $session_end_date);
        $session_period = $session_period->format("%a");
        $session_days = $session_period + 1; // included end date
        $session_data = array(
            'session_id' => $arr_session->session_id,
            'session_year' => $arr_session->session_year,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'session_status' => $arr_session->session_status,
            'session_days' => $session_days,
            'attendance_date' => $attendance_date,
        );
    }
    return $session_data;
}

function get_bank()
{
    $bank = [];
    $arr_bank = Bank::where('bank_status', 1)->select('bank_id', 'bank_name', 'bank_branch', 'bank_alias')->get();
    if (!empty($arr_bank)) {
        foreach ($arr_bank as $value) {
            $bank[$value['bank_id']] = $value['bank_name'] . '(' . $value['bank_alias'] . ')';
        }
    }
    return $bank;
}

function get_payment_mode()
{
    $payment_mode = [];
    $arr_payment_mode = PaymentMode::where('payment_mode_status', 1)->select('payment_mode_id', 'payment_mode')->get();
    if (!empty($arr_payment_mode)) {
        foreach ($arr_payment_mode as $value) {
            $payment_mode[$value['payment_mode_id']] = $value['payment_mode'];
        }
    }
    return $payment_mode;
}

function get_current_session_months()
{
    $months = [];
    $session = (object)get_current_session();
    if (!empty($session->start_date)) {
        $start_date = $session->start_date;
        $end_date = $session->end_date;
        $start_year = date('Y', strtotime($start_date));
        $end_year = date('Y', strtotime($end_date));
        if ($start_year != $end_year) {
            $start_month = date('m', strtotime($start_date));
            $end_month = date('m', strtotime($end_date));
            $first_year_months = range($start_month, 12);
            $second_year_months = range(1, $end_month);
            $all_month = array_merge($first_year_months, $second_year_months);
            $arr_custom_month = \Config::get('custom.month'); // month list from custom file
            foreach ($all_month as $key => $value) {
                $months[$value] = $arr_custom_month[$value]; // get month name
            }
        }
    }
    return $months;
}

//    function get_fee_months($student_start_month, $session_id)
//    {
//        $installments_in_quaters = [];
//
//        /*
//         * total month of current session
//         */
//        $arr_sessions_month = get_current_session_months();
//
//        /*
//         * monthly installment 
//         */
//        $pos                     = array_search($student_start_month, array_keys($arr_sessions_month));
//        $installments_in_months  = array_slice($arr_sessions_month, $pos, null, true);
//        /*
//         * quaterly installment 
//         */
//        $arr_custom_quater_chunk = \Config::get('custom.quarter_chunk');
//        $installments_quaters    = array_chunk($arr_sessions_month, $arr_custom_quater_chunk);
//
//        /*
//         * half yearly installment 
//         */
//        $total_month             = count($arr_sessions_month);
//        $halfyearly_period       = round($total_month) / 2;
//        $installments_halfyearly = array_chunk($arr_sessions_month, $halfyearly_period);
//
//
//        $arr_custom_quaters = \Config::get('custom.quarter'); // month list from custom file
//        foreach ($installments_quaters as $key => $value)
//        {
//            $key_val                           = 0;
//            $key_val                           = $key + 1;
//            $installments_in_quaters[$key_val] = $arr_custom_quaters[$key_val];
//        }
//        $installments = array(
//            'monthly'   => $installments_in_months,
//            'quarterly' => $installments_in_quaters,
//        );
//        return $installments;
//    }

function check_file_exist($file_name, $custome_key, $with_base_url = 'no', $default = false)
{
    $return_file = '';
    if ($default == true) {
        $return_file = url('public/st.jpg');
    }
    $config_upload_path = \Config::get('custom.' . $custome_key);
    if (!empty($file_name)) {
        if (is_file($config_upload_path['display_path'] . $file_name)) {
            if ($with_base_url == 'yes') {
                $return_file = url($config_upload_path['display_path'] . $file_name);
            } else {
                $return_file = $config_upload_path['display_path'] . $file_name;
            }
        }
    }
    return $return_file;
}

function get_gender($gender_option = null, $type = '')
{
    $gender_name = '';
    if (!empty($type)) {
        $gender = \Config::get('custom.' . $type);
        $gender_name = $gender[$gender_option];
    }
    return $gender_name;
}

function get_student_attendance($arr_student_id = array())
{
    if (!is_array($arr_student_id)) {
        $arr_student_id = array($arr_student_id);
    }
    $student = [];
    $arr_student = [];
    $student_array = [];
    $session_id = null;
    $session = get_current_session();
    $session_status = 0;
    if (!empty($session)) {
        $session_id = $session['session_id'];
        $session_status = $session['session_status'];
    }
    if ($session_status == 1) {
        $arr_student = StudentAttendance::where(function ($query) use ($arr_student_id) {
            if (!empty($arr_student_id)) {
                foreach ($arr_student_id as $student_id) {
                    $query->where('student_id', $student_id);
                }
            }
        })->where(function ($query) use ($session_id) {
            if (!empty($session_id)) {
                $query->where('session_id', $session_id);
            }
        })->where('attendance_date', date('Y-m-d'))->get();

        foreach ($arr_student as $key => $student) {
            if (!empty($student['student_id'])) {
                $student_array[] = array(
                    'student_id' => $student['student_id'],
                    'attendance_status' => $student['attendance_status'],
                    'attendance_date' => $student['attendance_date'],
                );
            }
        }
    }
    return $student_array;
}

function get_school_data()
{
    $school = [];
    $school_data = DB::table('schools')->where('status', 1)->first();
//        p($school_data);
    if (!empty($school_data)) {
        $school_data = (array)$school_data;
        $school_data['transport_fee_month'] = json_decode($school_data['transport_fee_month'], true);
        $school_data['school_logo'] = get_school_logo();
        $school = $school_data;
//	$school = array(
//	    'school_name' => $school_data['school_name'],
//	    'email' => $school_data['email'],
//	    'website_url' => $school_data['website_url'],
//	    'contact_number' => $school_data['contact_number'],
//	    'mobile_number' => $school_data['mobile_number'],
//	    'address' => $school_data['address'],
//	    'address' => $school_data['address'],
//	);
    }
    return $school;
}

function get_user_role()
{
    $arr_role = [];
    $arr_employee_role = UserRole::where('user_role_status', 1)->where('user_type_id', 3)->select('user_role_id', 'user_role_name')->get();
    if (!empty($arr_employee_role)) {
        foreach ($arr_employee_role as $employee_role) {
            $arr_role[$employee_role['user_role_id']] = $employee_role['user_role_name'];
        }
    }
    return $arr_role;
}

function get_employee_department()
{
    $departments = [];
    $arr_department = Department::where('department_status', 1)->select('department_id', 'department_name')->get();
    if (!empty($arr_department)) {
        foreach ($arr_department as $department) {
            $departments[$department['department_id']] = $department['department_name'];
        }
    }
    return $departments;
}

function get_employee_shift()
{
    $shifts = [];
    $arr_shift = Shift::where('shift_status', 1)->select('shift_id', 'shift_name')->get();
    if (!empty($arr_shift)) {
        foreach ($arr_shift as $shift) {
            $shifts[$shift['shift_id']] = $shift['shift_name'];
        }
    }
    return $shifts;
}

function get_subject_type($type = null)
{
    $subject_type = '';
    if (!empty($type)) {
        $arr_subject_type = \Config::get('custom.subject_type');
        $subject_type = $arr_subject_type[$type];
    }
    return $subject_type;
}

// school's defined/sheduled holiday list
function get_sheduled_school_holidays($start_date = null, $end_date = null, $session_id = null)
{

    $total_holidays = 0;
    if (empty($session_id)) {
        $session_data = get_current_session();
        $session_id = $session_data['session_id'];
    }
    $arr_holiday = Holiday::where(function ($query) use ($session_id) {
        if (!empty($session_id)) {
            $query->where('session_id', $session_id);
        }
    })
        ->where(function ($query) use ($start_date, $end_date) {
            if (!empty($start_date)) {
                $query->whereBetween('holiday_start_date', array($start_date, $end_date));
                $query->orWhereBetween('holiday_end_date', array($start_date, $end_date));
            }
        })
        ->where('holiday_status', 1)->get();


    if (!empty($arr_holiday)) {
        $holiday_list['holiday_details'] = [];
        foreach ($arr_holiday as $key => $holiday) {
            // calculate holidays period
            $start_date = $holiday['holiday_start_date'];
            $end_date = $holiday['holiday_end_date'];
            $format = "Y-m-d";
            $begin = new DateTime($start_date);
            $end = new DateTime($end_date);
            $end = $end->modify('+1 day');
            $interval_day = new DateInterval('P1D');
            $date_range = new DatePeriod($begin, $interval_day, $end);

            $holiday_period = [];
            foreach ($date_range as $key => $date) {
                $holiday_period[] = $date->format($format);
            }
            $holiday_list['holiday_details'][] = array(
                'holiday_id' => $holiday['holiday_id'],
                'holiday_name' => $holiday['holiday_name'],
                'holiday_start_date' => $start_date,
                'holiday_end_date' => $end_date,
                'holiday_period_count' => count($holiday_period),
                'holiday_period' => $holiday_period,
            );
        }
    }
    return $holiday_list;
}

// get all dates of a session
function get_session_date_range($type = '', $start_date = null, $end_date = null)
{

    if (empty($start_date) && empty($end_date)) {
        $session = get_current_session();
        $start_date = $session['start_date'];
        if ($type == 'full') // full year data
        {
            $end_date = $session['end_date'];
        } else // till current data
        {
            $end_date = date('Y-m-d') < $session['end_date'] ? date('Y-m-d') : $session['end_date'];
        }
    }
    // session date range list
    $begin = new DateTime($start_date);
    $end = new DateTime($end_date);
    $end = $end->modify('+1 day');
    $interval_day = new DateInterval('P1D');
    $arr_date_range = new DatePeriod($begin, $interval_day, $end);
    return $arr_date_range;
}

function get_school_all_holidays($start_date = null, $end_date = null, $session_id = null)
{
    // variable iniliazation
    $sunday_list = [];
    $arr_holiday_final_list = [];
    $sheduled_holiday_list = [];

    // get sundays holiday list
    if (!empty($start_date) && !empty($start_date)) {
        $arr_session_date = get_session_date_range('', $start_date, $end_date);
    } else {
        $arr_session_date = get_session_date_range('full');
    }

    foreach ($arr_session_date as $key => $date) {
        if ($date->format('w') == 0) {
            $sunday_list[] = $date->format('Y-m-d');
        }
    }
    // sheduled holiday list
    $sheduled_school_holidays = get_sheduled_school_holidays($start_date, $end_date, $session_id);
    $list = array_column($sheduled_school_holidays['holiday_details'], 'holiday_period');
    foreach ($list as $key => $arr_date) {
        foreach ($arr_date as $key => $date) {
            $sheduled_holiday_list[] = $date;
        }
    }

    // both sunday and sheduled holiday list
    $arr_holiday_list = array(
        'sunday' => $sunday_list,
        'other' => $sheduled_holiday_list,
    );

    // merged sunday and sheduled holiday date
    $arr_holiday_combine_list = array_merge($arr_holiday_list['sunday'], $arr_holiday_list['other']);

    // sorting holiday list
    asort($arr_holiday_combine_list);
    foreach ($arr_holiday_combine_list as $key => $value) {
        $arr_holiday_final_list[] = $value;
    }
    // return final holidays list of a session
    // remove duplicate dates
    $holiday_final_list = array_unique($arr_holiday_final_list);
    return $holiday_final_list;
}

function send_sms($mobile_number, $message_text)
{
    $curl = curl_init();
    $message_text = urlencode($message_text);
    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://msg.yantraainfotech.in/api/sendhttp.php?authkey=138970A7hHCSerL588dd38d&mobiles=" . $mobile_number . "&message=" . $message_text . "&sender=SHTECH&route=4&country=India",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    //        if ($err)
    //        {
    //            echo "cURL Error #:" . $err;
    //        }
    //        else
    //        {
    //            echo $response;
    //        }
    //        p($response);
}

function get_fee_circular_custom()
{
    return \Config::get('custom.fee_circular');
}

function get_employee($arr_employee_id = [], $department_id = null)
{
    $arr_employee_list = [];
    $arr_employee = Employee::where(function ($query) use ($arr_employee_id) {
        if (!empty($arr_employee_id)) {
            $query->WhereIn('employee_id', $arr_employee_id);
        }
    })
        ->where(function ($query) use ($department_id) {
            if (!empty($department_id)) {
                $query->where('department_id', $department_id);
            }
        })->get();

    if (!empty($arr_employee[0]->employee_id)) {
        $arr_employee_list = $arr_employee;
    }
    return $arr_employee_list;
}

function get_employee_by_department($department_id)
{
    $arr_employee_list = [];
    $arr_employee = Employee::where('department_id', $department_id)->where('employee_status', 1)->get();
    if (!empty($arr_employee[0]->employee_id)) {
        $arr_employee_list = $arr_employee;
    }
    return $arr_employee_list;
}

function quick_random($length = 8)
{
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
}

function get_class_section($class_id, $session_id, $section_id = null)
{

    $arr_class_section = [];
    if (!empty($class_id)) {
        if (empty($session_id)) {
            $session = get_current_session();
            $session_id = $session['session_id'];
        }
        $query = DB::table('sections as s')
            ->join('class_sections as cs', 's.section_id', '=', 'cs.section_id')
            ->where('cs.class_id', $class_id)
            ->where('cs.session_id', $session_id)
            ->where('s.section_status', 1);
        if (!empty($section_id)) {
            $query->where('cs.section_id', $section_id);
        }
        $classes = $query->get();

        if (isset($classes)) {
            foreach ($classes as $key => $class_section) {
                if (!empty($section_id)) {
                    if ($section_id == $class_section->section_id) {
                        $arr_class_section[$class_section->section_id] = $class_section->section_name;
                    }
                } else {
                    $arr_class_section[$class_section->section_id] = $class_section->section_name;
                }
            }
        }
    }
    return $arr_class_section;
}

function amount_format($money)
{
    $amount = number_format($money);
    if ($money > 0) {
        $amount = ' &#x20B9; ' . number_format($money);
    }
    return $amount;
}

function get_all_exam_category($exam_category_id = null)
{
    $arr_exam = [];
    $arr_exam_category = ExamCategory::where(function ($query) use ($exam_category_id) {
        if (!empty($exam_category_id)) {
            $query->where('exam_category_id', $exam_category_id);
        }
    })->select('exam_category', 'exam_category_id')->orderBy('exam_category', 'ASC')->get();
    if (!empty($arr_exam_category)) {
        foreach ($arr_exam_category as $exam_category) {
            $arr_exam[$exam_category['exam_category_id']] = $exam_category['exam_category'];
        }
    }
    return $arr_exam;
}

function get_all_exam_type($exam_type_id = [])
{
    $arr_type = [];
    if (!is_array($exam_type_id)) {
        $exam_type_id = array($exam_type_id);
    }
    $arr_exam_type = ExamType::where('exam_type_status', 1)
        ->where(function ($query) use ($exam_type_id) {
            if (!empty($exam_type_id)) {
                $query->whereIn('exam_type_id', $exam_type_id);
            }
        })->select('exam_type', 'exam_type_id')
        ->orderBy('exam_type', 'DESC')->get();
    if (!empty($arr_exam_type)) {
        foreach ($arr_exam_type as $exam_type) {
            $arr_type[$exam_type['exam_type_id']] = $exam_type['exam_type'];
        }
    }
    return $arr_type;
}

function get_all_exam($exam_id = null, $class_id = null, $session_id = null, $section_id = null)
{
    $arr_exam_list = [];
    if (empty($session_id)) {
        $session = db_current_session();
        $session_id = $session['session_id'];
    }
    $query = DB::table('create_exams as ce')->where('exam_status', 1);
    if (!empty($exam_id)) {
        $query->where('ce.exam_id', $exam_id);
    }
    if (!empty($class_id)) {
        $query->join('exam_classes as ec', 'ec.exam_id', '=', 'ce.exam_id');
        $query->where('ec.session_id', $session_id);
        $query->where('class_id', $class_id);
        $query->where('section_id', $section_id);
    }
    if (!empty($session_id)) {
        $query->where('ce.session_id', $session_id);
    }
    $arr_exam = $query->select('ce.exam_name', 'ce.exam_id')->orderBy('exam_name', 'ASC')->get();
    if (!empty($arr_exam)) {
        foreach ($arr_exam as $exam) {
            $exam = (array)$exam;
            $arr_exam_list[$exam['exam_id']] = $exam['exam_name'];
        }
    }
    return $arr_exam_list;
}

function get_all_subject_group($session_id = null, $class_id = null, $section_id = null, $subject_group_id = null)
{
    //        p($session_id,0);
    //        p($class_id,0);
    //        p($section_id,0);
    //        p($subject_group_id,0);
    $arr_group = [];
    $query = DB::table('subject_groups as sg')->where('group_status', 1)
        ->select('sg.group_name', 'sg.subject_group_id')->orderBy('group_name', 'ASC');
    if (!empty($subject_group_id)) {
        $query->where('subject_group_id', $subject_group_id);
    }
    if (!empty($class_id)) {
        $query->join('class_subjects as cs', 'sg.subject_group_id', '=', 'cs.subject_group_id');
        $query->where('session_id', $session_id);
        $query->where('class_id', $class_id);
        $query->where('section_id', $section_id);
    }
    $subject_group = $query->get();
    if (!empty($subject_group)) {
        foreach ($subject_group as $group) {
            $group = (array)$group;
            $arr_group[$group['subject_group_id']] = $group['group_name'];
        }
    }
    return $arr_group;
}

function get_vehicle_list($vehicle_id = null)
{
    $all_vehicle = [];
    $arr_vehicle = Vehicle::where('vehicle_status', 1)
        ->select('vehicle_number', 'vehicle_name', 'vehicle_id')->orderBy('vehicle_number', 'ASC')->get();
    if (!empty($arr_vehicle)) {
        foreach ($arr_vehicle as $vehicle) {
            $all_vehicle[$vehicle['vehicle_id']] = $vehicle['vehicle_number'];
        }
    }
    return $all_vehicle;
}

function get_result_in()
{
    return \Config::get('custom.arr_result_in');
}

function get_status()
{
    return \Config::get('custom.status');
}

function get_root_coscholastic()
{
    $arr_coscholastic = [];
    $arr_coscholastic_data = CoScholastic::addSelect('co_scholastic_id', 'co_scholastic_name')->where('status', 1)->where('root_id', 0)->get();
    foreach ($arr_coscholastic_data as $key => $coscholastic) {
        $arr_coscholastic[$coscholastic['co_scholastic_id']] = $coscholastic['co_scholastic_name'];
    }
    return $arr_coscholastic;
}

function get_all_coscholastic()
{
    $arr_coscholastic = [];
    $arr_coscholastic_data = CoScholastic::addSelect('co_scholastic_id', 'co_scholastic_name', 'root_id')->where('status', 1)->get();
    $arr_root_coscholastic = [];
    $arr_sub_coscholastic = [];
    $arr_final_coscholastic = [];
    foreach ($arr_coscholastic_data as $key => $coscholastic) {
        if ($coscholastic['root_id'] == 0) {
            $arr_root_coscholastic[$coscholastic['co_scholastic_id']] = $coscholastic['co_scholastic_name'];
        }
        if (in_array($coscholastic['root_id'], array_keys($arr_root_coscholastic))) {
            $arr_sub_coscholastic[$coscholastic['root_id']][$coscholastic['co_scholastic_id']] = $coscholastic['co_scholastic_name'];
        }
    }
    if (!empty($arr_root_coscholastic)) {
        foreach ($arr_root_coscholastic as $co_scholastic_id => $root_coscholastic) {
            $arr_final_coscholastic[$co_scholastic_id] = array(
                'co_scholastic_id' => $co_scholastic_id,
                'co_scholastic_name' => $root_coscholastic,
                'sub_coscholastic' => [],
            );

            if (!empty($arr_sub_coscholastic) && isset($arr_sub_coscholastic[$co_scholastic_id])) {
                $arr_final_coscholastic[$co_scholastic_id]['sub_coscholastic'] = $arr_sub_coscholastic[$co_scholastic_id];
            }
        }
    }
    return $arr_final_coscholastic;
}

//    function get_marks_grade($marks)
//    {
//        $grade = '';
//        if ($marks > 0)
//        {
//            $grade_data = Grade::addSelect('grade_name')->where('minimum', '<=', $marks)->where('maximum', '>=', $marks)->first();
//            if (!empty($grade_data->grade_name))
//            {
//                $grade = $gradget_marks_gradee_data->grade_name;
//            }
//        }
//        return $grade;
//    }
function get_marks_grade($session_id = null)
{
    $arr_grade = [];
    if (empty($session_id)) {
        $session = get_current_session();
        $session_id = $session['session_id'];
    }
    $arr_grade_data = Grade::select('grade_id', 'grade_name', 'minimum', 'maximum', 'grade_comments')->where('session_id', $session_id)->get();
    foreach ($arr_grade_data as $grade_data) {
        $arr_grade[] = array(
            'grade_id' => $grade_data->grade_id,
            'range' => implode(',', range($grade_data->minimum, $grade_data->maximum)),
            'minimum' => $grade_data->minimum,
            'maximum' => $grade_data->maximum,
            'grade_name' => $grade_data->grade_name,
            'grade_comments' => $grade_data->grade_comments,
        );
    }
    return $arr_grade;
}

function get_marks_division($percent = null)
{
    $division = '';
//        $fi = "I<sup>st</sup>";
//        p($fi);
//        $se = "II<sup>nd</sup>";
//        $th = "III<sup>rd</sup>";
    $arr_division = [
        'Ist' => range(60, 100),
        'IInd' => range(48, 59),
        'IIIrd' => range(1, 47),
    ];
    foreach ($arr_division as $key => $data) {
        if (in_array(round($percent), $data)) {
            $division = $key;
        }
    }
    return $division;
}

function get_result($percent = null)
{
    $d = get_marks_division($percent);
    return !empty($d) ? 'Pass/' . $d : 'Fail';
}

function text_pattern()
{
    return \Config::get('custom.text_pattern');
}

function numberTowords($num)
{
    $ones = array(
        0 => "ZERO",
        1 => "ONE",
        2 => "TWO",
        3 => "THREE",
        4 => "FOUR",
        5 => "FIVE",
        6 => "SIX",
        7 => "SEVEN",
        8 => "EIGHT",
        9 => "NINE",
        10 => "TEN",
        11 => "ELEVEN",
        12 => "TWELVE",
        13 => "THIRTEEN",
        14 => "FOURTEEN",
        15 => "FIFTEEN",
        16 => "SIXTEEN",
        17 => "SEVENTEEN",
        18 => "EIGHTEEN",
        19 => "NINETEEN",
        "014" => "FOURTEEN"
    );
    $tens = array(
        0 => "ZERO",
        1 => "TEN",
        2 => "TWENTY",
        3 => "THIRTY",
        4 => "FORTY",
        5 => "FIFTY",
        6 => "SIXTY",
        7 => "SEVENTY",
        8 => "EIGHTY",
        9 => "NINETY"
    );
    $hundreds = array(
        "HUNDRED",
        "THOUSAND",
        "MILLION",
        "BILLION",
        "TRILLION",
        "QUARDRILLION"
    ); //limit t quadrillion
    $num = number_format($num, 2, ".", ",");
    $num_arr = explode(".", $num);
    $wholenum = $num_arr[0];
    $decnum = $num_arr[1];
    $whole_arr = array_reverse(explode(",", $wholenum));
    krsort($whole_arr, 1);
    $rettxt = "";
    foreach ($whole_arr as $key => $i) {
        while (substr($i, 0, 1) == "0")
            $i = substr($i, 1, 5);
        if ($i < 20) {
            $rettxt .= $ones[$i];
        } elseif ($i < 100) {
            if (substr($i, 0, 1) != "0")
                $rettxt .= $tens[substr($i, 0, 1)];
            if (substr($i, 1, 1) != "0")
                $rettxt .= " " . $ones[substr($i, 1, 1)];
        } else {
            if (substr($i, 0, 1) != "0")
                $rettxt .= $ones[substr($i, 0, 1)] . " " . $hundreds[0];
            if (substr($i, 1, 1) != "0")
                $rettxt .= " " . $tens[substr($i, 1, 1)];
            if (substr($i, 2, 1) != "0")
                $rettxt .= " " . $ones[substr($i, 2, 1)];
        }
        if ($key > 0) {
            $rettxt .= " " . $hundreds[$key] . " ";
        }
    }
    if ($decnum > 0) {
        $rettxt .= " and ";
        if ($decnum < 20) {
            $rettxt .= $ones[$decnum];
        } elseif ($decnum < 100) {
            $rettxt .= $tens[substr($decnum, 0, 1)];
            $rettxt .= " " . $ones[substr($decnum, 1, 1)];
        }
    }
    return $rettxt;
}

function covert_dob_words($birth_date)
{
    $dob_in_words = '';
    if (!empty($birth_date)) {
        $new_birth_date = explode('/', $birth_date);
        $day = $new_birth_date[0];
        $month = $new_birth_date[1];
        $year = $new_birth_date[2];
        $birth_day = numberTowords($day);
        $birth_year = numberTowords($year);
        $monthNum = $month;
        $dateObj = Carbon::createFromFormat('!m', $monthNum); //Convert the number into month name
        $monthName = strtoupper($dateObj->format('F'));
        $dob_in_words = $birth_day . ' ' . $monthName . ' ' . $birth_year;
    }
    return $dob_in_words;
}

function age_calculator($dob)
{
    $arr_dob = explode('/', $dob);
    $age = Carbon::createFromDate($arr_dob[2], $arr_dob[1], $arr_dob[0])->diff(Carbon::now())->format('%y years, %m months and %d days');
    return $age;
}

function get_school_logo($color_logo = false)
{

    $school_logo = url('/public/logo/logo.png'); // default logo
    $school = School::select('school_logo', 'school_logo_color')->first();
    if (!empty($school['school_logo']) || !empty($school['school_logo_color'])) {
        if ($color_logo == true && check_file_exist($school['school_logo_color'], 'school_profile')) {
            $school_logo = check_file_exist($school['school_logo_color'], 'school_profile');
        }
        if ($color_logo == false && check_file_exist($school['school_logo'], 'school_profile')) {
            $school_logo = check_file_exist($school['school_logo'], 'school_profile');
        }
    }
    return $school_logo;
}

function get_student_table($session_id, $type = 'table')
{
    $session = (array)DB::table('sessions')->where('session_id', $session_id)->select('session_status')->first();
    if ($type == 'table') {
        $model_table = 'student_histories as s';
        if (!empty($session) && $session['session_status'] == 1) {
            $model_table = 'students as s';
        }
    } else {
        $model_table = StudentHistory::where('student_status', 1);
        if (!empty($session) && $session['session_status'] == 1) {
            $model_table = Student::where('student_status', 1);
        }
    }
    return $model_table;
}

// Current Session from db
function db_current_session()
{
    $session = (array)DB::table('sessions')
        ->where('session_status', 1)
        ->select('session_id', 'session_year', 'start_date', 'end_date', 'session_status')
        ->first();
    $session['session_start_date'] = get_formatted_date($session['start_date'], 'display');
    $session['session_end_date'] = get_formatted_date($session['end_date'], 'display');
//        p($session);
    return $session;
}

// Current Session after set other session
function get_current_next_session()
{
    $arr_session_data = Session::where('session_status', 1)
        ->orWhere('end_date', '>', date('Y-m-d'))
        ->select('session_id', 'session_year')
        ->get();
    $arr_session = [];
    foreach ($arr_session_data as $key => $value) {
        $arr_session[$value['session_id']] = $value['session_year'];
    }
    return $arr_session;
}

function get_class_subject($class_id, $session_id)
{
    $class_subject = [];
    $arr_subject = DB::table('subjects as s')
        ->join('class_subjects as cs', 'cs.subject_id', '=', 's.subject_id')
        ->where('cs.class_id', $class_id)
        ->where('cs.session_id', $session_id)
        ->where('s.subject_status', 1)
        ->select('s.subject_id', 'subject_name')
        ->get();
    if (!empty($arr_subject)) {
        foreach ($arr_subject as $key => $subject) {
            $subject = (array)$subject;
            $class_subject[$subject['subject_id']] = $subject['subject_name'];
        }
    }
    return $class_subject;
}

function create_scholar_number($class_id)
{
    $arr_prefix = \Config::get('custom.student_scholar_prefix');
    if ($class_id == 1) {
        // PG student scholar no.
        $prefix = $arr_prefix['play_group'];
        $enrollment_no = $prefix . '01';
        $data_enrollment = Student::select('student_id', 'enrollment_number')->where('current_class_id', 1)->orderBy('student_id', 'desc')->first();
        if (!empty($data_enrollment->student_id)) {
            $last_number = trim($data_enrollment->enrollment_number, $prefix) + 1;
            $last_number = str_pad($last_number, 2, '0', STR_PAD_LEFT);
            $enrollment_no = $prefix . $last_number;
        }
    } else {
        // all student scholar no, except play group.
        $prefix = '';//$arr_prefix['other'];
        $enrollment_no = $prefix . '01';
//	$data_enrollment = Student::select('student_id', 'enrollment_number')->where('current_class_id', '!=', 1)->orderBy('student_id', 'desc')->first();
        $data_enrollment = Student::select('student_id', DB::raw('max(cast(enrollment_number as unsigned)) as enrollment_number'))->where('current_class_id', '!=', 1)->orderBy('enrollment_number', 'desc')->first();
        if (!empty($data_enrollment->student_id)) {
//	    $last_number = trim($data_enrollment->enrollment_number, $prefix) + 1;
//	    $last_number = str_pad($last_number, 2, '0', STR_PAD_LEFT);
            $last_number = $data_enrollment->enrollment_number + 1;
            $enrollment_no = $prefix . $last_number;
        }
    }
    return $enrollment_no;
}

// get row span for class, section and subject-group 
function get_row_span_view_subject($session_id, $class_id, $section_id = null, $subject_group_id = null)
{
    $row_span = null;
    if (!empty($session_id)) {
        $q = DB::table('class_subjects as cs')
            ->leftJoin('subjects as sb', 'cs.subject_id', '=', 'sb.subject_id')
            ->where('cs.session_id', $session_id)
            ->where('cs.class_id', $class_id)
            ->select('cs.subject_id', 'sb.root_id', 'cs.section_id', 'cs.class_id');
        if (!empty($section_id)) {
            $q->where('cs.section_id', $section_id);
        }
        if (!empty($subject_group_id)) {
            $q->where('cs.subject_group_id', $subject_group_id);
        }
        $subject_count = $q->get();
        $subject_count = json_decode(json_encode($subject_count), true);
        $arr_subject = [];
        foreach ($subject_count as $subject) {
            $arr_subject[$subject['section_id']][] = $subject;
        }
        //calculate row span according to section
        if (!empty($arr_subject)) {
            $row_span = 0;
            foreach ($arr_subject as $subject_data) {
                $parent_subject = count(array_unique(array_column($subject_data, 'root_id')));
                $all_subject = count($subject_data);
                $row_span = $row_span + ($all_subject - $parent_subject) + 1;
                // 1 added because parent 0 subject also subtracted.
            }
        }
    }
    return $row_span;
}

/*
 * get class data from class subject mapping
 */

function get_class_subject_mapping($session_id, $class_id = null)
{
    $query = DB::table('class_subjects as cs')
        ->join('classes as c', 'cs.class_id', '=', 'c.class_id')
        ->where('cs.session_id', $session_id);
    if (!empty($class_id)) {
        $query->where('cs.class_id', $class_id);
    }

    $arr_class_data = $query->select('c.class_name', 'cs.class_id')
        ->groupBy('cs.class_id')
        ->groupBy('c.class_name')->get();
    return json_decode(json_encode($arr_class_data), true);
}

/*
 * get class -section data from class subject mapping
 */

function get_class_subject_section($session_id, $class_id, $section_id = null)
{
    $query = DB::table('class_subjects as cs')
        ->join('sections as s', 'cs.section_id', '=', 's.section_id')
        ->where('cs.session_id', $session_id)
        ->where('cs.class_id', $class_id);
    if (!empty($section_id)) {
        $query->where('cs.section_id', $section_id);
    }
    $arr_section_data = $query->select('s.section_name', 'cs.section_id')
        ->groupBy('cs.section_id')
        ->groupBy('s.section_name')
        ->get()->toArray();
    $arr_section_data = json_decode(json_encode($arr_section_data), true);
    return $arr_section_data;
}

/*
 * get class -section-group  data from class subject mapping
 */

function get_class_subject_section_group($session_id, $class_id, $section_id, $subject_group_id = null)
{
    $query1 = DB::table('class_subjects as cs')
        ->join('subject_groups as sg', 'cs.subject_group_id', '=', 'sg.subject_group_id')
        ->where('cs.session_id', $session_id)
        ->where('cs.class_id', $class_id)
        ->where('cs.section_id', $section_id);

    $arr_group_data = $query1->select('sg.group_name', 'cs.subject_group_id', DB::raw('count(cs.subject_group_id) as total_group'))
        ->groupBy('cs.subject_group_id')
        ->groupBy('sg.group_name')
        ->get()->toArray();
    $arr_group_data = json_decode(json_encode($arr_group_data), true);
    return $arr_group_data;
}

/*
 * get class-section-group-subject data from class subject mapping
 */

function get_class_subject_section_group_subject($session_id, $class_id, $section_id, $subject_group_id)
{
    $query2 = DB::table('class_subjects as cs')
        ->join('subjects as sb', 'cs.subject_id', '=', 'sb.subject_id')
        ->where('sb.root_id', 0)
        ->where('cs.session_id', $session_id)
        ->where('cs.class_id', $class_id)
        ->where('cs.section_id', $section_id)
        ->where('cs.subject_group_id', $subject_group_id);

    $arr_subject_data = $query2->select('sb.subject_name', 'cs.subject_id', 'sb.root_id')
        ->get()->toArray();

    return json_decode(json_encode($arr_subject_data), true);
}

/*
 * get class-section-group-sub-subject data from class subject mapping
 */

function get_class_subject_section_group_sub_subject($session_id, $class_id, $section_id, $subject_group_id, $subject_id)
{
    $query3 = DB::table('class_subjects as cs')
        ->join('subjects as sb', 'cs.subject_id', '=', 'sb.subject_id')
        ->where('cs.session_id', $session_id)
        ->where('sb.root_id', $subject_id)
        ->where('cs.class_id', $class_id)
        ->where('cs.section_id', $section_id)
        ->where('cs.subject_group_id', $subject_group_id);

    $arr_sub_subject = $query3->select('sb.subject_name', 'cs.subject_id', 'sb.root_id')
        ->get()->toArray();

    return json_decode(json_encode($arr_sub_subject), true);
}

function insert_data_in_modules()
{
    $modules = \Config::get('custom.modules');
    if (!empty($modules)) {
        foreach ($modules as $key => $value) {
            DB::table('modules')->insert(
                array(
                    'key' => $value['key'],
                    'module' => $value['name'],
                    'module_action' => $value['action'],
                    'module_url' => $value['url'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                )
            );
        }
    }
}

function get_arr_subject($arr_subject_id = [])
{
    $class_subject = [];
    $arr_subject = DB::table('subjects as s')
        ->where(function ($query) use ($arr_subject_id) {
            if ($arr_subject_id) {
                $query->whereIn('subject_id', $arr_subject_id);
            }
        })
        ->select('s.subject_id', 'subject_name', 's.subject_code')
        ->get()->toArray();
    if (!empty($arr_subject)) {
        foreach ($arr_subject as $key => $subject) {
            $subject = (array)$subject;
            $class_subject[$subject['subject_id']] = $subject['subject_name'] . ' (' . $subject['subject_code'] . ')';
        }
    }
    return $class_subject;
}

function get_arr_employee($arr_employee_id = [], $department_id = null)
{
    $arr_employee_list = [];
    $arr_employee = Employee::where(function ($query) use ($arr_employee_id) {
        if (!empty($arr_employee_id)) {
            $query->WhereIn('employee_id', $arr_employee_id);
        }
    })
        ->where(function ($query) use ($department_id) {
            if (!empty($department_id)) {
                $query->where('department_id', $department_id);
            }
        })->get();
    foreach ($arr_employee as $key => $value) {
        $arr_employee_list[$value['employee_id']] = $value['full_name'];
    }
    return $arr_employee_list;
}

// get paymet status array from custom file
function arr_payment_status()
{
    return \Config::get('custom.arr_payment_status');
}

function get_student_total_academic_fee($arr_condition)
{
    $student = DB::table('fee_details as fd')
        ->join('fees as f', 'fd.fee_id', '=', 'f.fee_id')
        ->where('f.session_id', $arr_condition['session_id'])
        ->where(function ($query) use ($arr_condition) {
            // check fee of particular fee type
            if (!empty($arr_condition['fee_type_id'])) {
                $query->where('f.fee_type_id', $arr_condition['fee_type_id']);
            }
//            if (!empty($arr_condition['class_id'])) {
//                $query->where('f.class_id', $arr_condition['class_id']);
//            }
            if (!empty($arr_condition['fee_circular_id'])) {
                $query->where('f.fee_circular_id', $arr_condition['fee_circular_id']);
            }
            if(!empty($arr_condition['fee_circular_between'])){
                $query->whereIn('fd.fee_sub_circular', $arr_condition['fee_circular_between']);
            }elseif (!empty($arr_condition['fee_circular_name'])) {
                $query->where('fd.fee_sub_circular', $arr_condition['fee_circular_name']);
            }
            $query->whereNull("fd.deleted_at");
            $query->where(function($k) use($arr_condition){
                $apply_to = array('all_student');
                if ($arr_condition['new_student'] == 1) {
                    array_push($apply_to,'new_student');
                }
                if (!empty($arr_condition['class_id'])) {
                    $k->where('f.class_id', $arr_condition['class_id'])->orWhereIn('f.apply_to', $apply_to);
                }else{
                    $k->whereIn('f.apply_to', $apply_to);
                }
                if (!empty($arr_condition['student_type_id'])) {
                    $k->whereIn('f.student_type_id', array(0, $arr_condition['student_type_id']));
                }
            });
        })
        ->select(DB::raw('SUM(fee_amount) as total_amount'))
        ->first();
    return $student->total_amount;
}

function check_current_session()
{
    $dashboard_session = get_current_session();
    $database_session = db_current_session();
    $return = false;
    if ($dashboard_session['session_id'] == $database_session['session_id']) {
        $return = true;
    }
    return $return;
}

function get_transport_fee_months()
{
    $month_list = [];
    $arr_month_name = \Config::get('custom.month');
    $school_data = School::where('status', 1)->first();
    $arr_month = json_decode($school_data['transport_fee_month'], true);
    if (!empty($arr_month)) {
        foreach ($arr_month as $month_id) {
            $month_list[$month_id] = $arr_month_name[$month_id];
        }
    }
    return $month_list;
}

function get_current_session_month_days()
{
    $session = get_current_session();
    $start_date = $session['start_date'];
    $end_date = $session['end_date'];
    $begin = new DateTime($start_date);
    $end = new DateTime($end_date);
    $end = $end->modify('+1 day');
    $interval_day = new DateInterval('P1D');
    $arr_date_range = new DatePeriod($begin, $interval_day, $end);
    $keys = [];
    $arr_months_date = [];
    foreach ($arr_date_range as $key => $date) {
        if (!in_array($date->format('m'), $keys)) {
            $date_range = [];
        }
        $date_range[] = $date->format('Y-m-d');
        $arr_months_date[$date->format('m')] = $date_range;
        $keys = array_keys($arr_months_date);
    }
    return $arr_months_date;
}

//    get student transport fee of all months as decided by school
function get_student_transport_fee($arr_data)
{
    $arr_month_fair = [];
    if (!empty($arr_data['student_id'])) {
        $session = get_current_session();
        $student_assigned_list = DB::table('student_vehicle_assigned_details as svad')
            ->join('student_vehicle_assigned as sva', 'sva.student_vehicle_assigned_id', '=', 'svad.assigned_id')
            ->join('stop_points as sp', 'sva.stop_point_id', '=', 'sp.stop_point_id')
            ->where('svad.student_id', $arr_data['student_id'])
            ->where('svad.assigned_to', 1)
            ->where('sva.session_id', $session['session_id'])
            ->orderBy('svad.assigned_status', 'ASC')
            ->get();

        $arr_assigned_month = [];
        $arr_assigned_list = [];
        foreach ($student_assigned_list as $key => $assigned_list) {
            $assigned_list = (array)$assigned_list;
            $arr_assigned_month = [];
            if ($assigned_list['assigned_status'] == 1) {
                $arr_assigned_month = array(
                    'join_date' => $assigned_list['join_date'],
                    'stop_date' => $session['end_date'],
                    'stop_point_id' => $assigned_list['stop_point_id'],
                    'stop_fair' => $assigned_list['stop_fair'],
                    'arr_month' => get_month_days($assigned_list['join_date'], $session['end_date']),
                );
            } else {
                $arr_assigned_month = array(
                    'join_date' => $assigned_list['join_date'],
                    'stop_date' => $assigned_list['stop_date'],
                    'stop_fair' => $assigned_list['stop_fair'],
                    'stop_point_id' => $assigned_list['stop_point_id'],
                    'arr_month' => get_month_days($assigned_list['join_date'], $assigned_list['stop_date']),
                );
            }

            $arr_assigned_list[] = $arr_assigned_month;
        }
        if (!empty($arr_assigned_list)) {
            $arr_month_id = [];
            $arr_month_fair = [];
            $school_transport_fee_month = array_keys(get_transport_fee_months());
            foreach ($arr_assigned_list as $assigned_data) {
                $stop_fair = $assigned_data['stop_fair'];
                foreach ($assigned_data['arr_month'] as $month_id => $data) {

                    //condition for selected month if month id is not empty
                    $status = true;
                    if (!empty($arr_data['month_id'])) {
                        $status = $arr_data['month_id'] == $month_id ? true : false;
                    }
                    if ($status == true) {
                        // check school's selected fee months
                        if (in_array($month_id, $school_transport_fee_month)) {
                            $perday_fair = $stop_fair / $data['total_days'];
                            $assigned_fair = $perday_fair * $data['days'];
                            if (!in_array($month_id, $arr_month_id)) {
                                $arr_month_fair[$month_id] = round($assigned_fair);
                            } else {
                                $previous_amount = $arr_month_fair[$month_id];
                                $arr_month_fair[$month_id] = round($previous_amount + $assigned_fair);
                            }
                            $arr_month_id = array_keys($arr_month_fair);
                        }
                    }
                }
            }
        }
    }
    // return month and fee as configured by studen
    return $arr_month_fair;
}

//    get all days and thier months between 2 dates
function get_month_days($start_date, $end_date)
{
    $arr_month_total_days = get_current_session_month_days();
    $begin = new DateTime($start_date);
    $end = new DateTime($end_date);
    $end = $end->modify('+1 day');
    $interval_day = new DateInterval('P1D');
    $arr_date_range = new DatePeriod($begin, $interval_day, $end);
    $keys = [];
    $arr_months_date = [];
    foreach ($arr_date_range as $key => $date) {
        if (!in_array($date->format('m'), $keys)) {
            $date_range = [];
        }
        $date_range[] = $date->format('Y-m-d');
        $arr_months_date[$date->format('m')] = $date_range;
        $keys = array_keys($arr_months_date);
    }
    $arr_month_days = [];
    foreach ($arr_months_date as $key => $days) {
        if (isset($arr_month_total_days[$key])) {
            $arr_month_days[$key] = array(
                'days' => count($days),
                'total_days' => count($arr_month_total_days[$key]),
            );
        }
    }
    return $arr_month_days;
}

//return only subject name and id
function get_class_section_subject($session_id, $class_id, $section_id)
{
    $class_subject = [];
    $arr_subject = DB::table('subjects as s')
        ->join('class_subjects as cs', 'cs.subject_id', '=', 's.subject_id')
        ->where('cs.class_id', $class_id)
        ->where('cs.session_id', $session_id)
        ->where('cs.section_id', $section_id)
        ->where('s.subject_status', 1)
        ->select('s.subject_id', 's.subject_name', 's.root_id')
        ->get();
    $arr_subject = json_decode($arr_subject, true);
    if (!empty($arr_subject)) {
        $arr_root_id = array_unique(array_column($arr_subject, 'root_id'));
        foreach ($arr_subject as $key => $value) {
            if (!in_array($value['subject_id'], $arr_root_id)) {
                $class_subject[$value['subject_id']] = $value['subject_name'];
            }
        }
    }
    return $class_subject;
}

function get_class_section_subject_detail($session_id, $class_id, $section_id)
{
    $class_subject_detail = [];
    $arr_subject = DB::table('subjects as s')
        ->join('class_subjects as cs', 'cs.subject_id', '=', 's.subject_id')
        ->where('cs.class_id', $class_id)
        ->where('cs.session_id', $session_id)
        ->where('cs.section_id', $section_id)
        ->where('s.subject_status', 1)
        ->select('s.subject_id', 's.subject_name', 's.root_id')
        ->get();
    $arr_subject = json_decode($arr_subject, true);
    if (!empty($arr_subject)) {
        $arr_root_id = array_unique(array_column($arr_subject, 'root_id'));
        foreach ($arr_subject as $key => $value) {
            if (!in_array($value['subject_id'], $arr_root_id)) {
//                        $value                         = (array) $value;
                $class_subject_detail[$value['subject_id']] = $value;
            }
        }
    }
    return $class_subject_detail;
}

/* get document */
function get_document_type($document_type_id = [])
{
    $arr_document = [];
    $arr_document_type = DocumentType::where('status', 1)
        ->where(function ($query) use ($document_type_id) {
            if (!empty($document_type_id)) {
                $query->whereIn('document_type_id', $document_type_id);
            }
        })->select('document_type', 'document_type_id')
        ->orderBy('document_type')->get();

    if (!empty($arr_document_type)) {
        foreach ($arr_document_type as $document_type) {
            $arr_document[$document_type['document_type_id']] = $document_type['document_type'];
        }
    }
    return $arr_document;
}

function get_fee_sub_circluar_between($fee_circular_id, $fee_circular_name){
    $fee_circular = FeeCircular::find($fee_circular_id);
    $fee_circulars = \Config::get("custom.fee_sub_circular");
    $sub_fee_circular = $fee_circulars[$fee_circular->fee_circular];
    $sub_fee_circular = array_keys($sub_fee_circular);
    $temp = [];
    foreach($sub_fee_circular as $sub_circular){
        array_push($temp, $sub_circular);
        if($sub_circular == $fee_circular_name){
            break;
        }
    }
    return $temp;
}
