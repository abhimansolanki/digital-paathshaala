<?php

    namespace App\Model\frontend;

    use Illuminate\Mail\Mailer;
    use Illuminate\Mail\Message;
    use App\Model\frontend\ActivationKey;
    use App\Notifications\ActivationKeyCreatedNotification;

    class ActivationService
    {

        protected $mailer;
        protected $activationRepo;
        protected $resendAfter = 1;

        public function __construct(Mailer $mailer, ActivationRepository $activationRepo, User $User)
        {
            $this->mailer = $mailer;
            $this->activationRepo = $activationRepo;
            $this->user = $User;
        }

        public function sendActivationMail($user)
        {

            if ($user->activated || !$this->shouldSend($user))
            {
                return;
            }

            $token = $this->activationRepo->createActivation($user);
            $this->sendUserActivationCode($token, $user);
        }

        public function activateUser($token)
        {
            $activation = $this->activationRepo->getActivationByToken($token);

            if ($activation === null)
            {
                return null;
            }

            $user = User::find($activation->user_id);
            $user->activated = true;

            $user->save();

            $this->activationRepo->deleteActivation($token);

            return $user;
        }

        private function shouldSend($user)
        {
            $activation = $this->activationRepo->getActivation($user);
            return $activation === null || strtotime($activation->created_at) + 60 * 60 * $this->resendAfter < time();
        }

        public function sendUserActivationCode($token, $user)
        {
            //$this->notify(new UserActivationCode());
            $response = $user->notify(new ActivationKeyCreatedNotification($token));
        }

    }
    