<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkDiary extends Model
{
    use SoftDeletes;
     protected $table      = 'work_diaries';
     protected $primaryKey = 'work_diary_id';
     
     public function workClass()
        {
            return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }

        public function workSubject()
        {
            return $this->belongsTo('App\Model\backend\Subject', 'subject_id');
        }
}
