<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Section extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'sections';
        protected $primaryKey = 'section_id';

        public function getStudentSection()
        {
            return $this->hasMany('App\Models\Student');
        }
 
        public function sectionClass()
        {
            return $this->belongsToMany('App\Model\backend\ClassSection', 'section_id');
        }

    }
    