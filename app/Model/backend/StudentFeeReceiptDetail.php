<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentFeeReceiptDetail extends Model
{
    use SoftDeletes;
    protected $table = 'student_fee_receipt_details';
    protected $primaryKey = 'student_fee_receipt_detail_id';
    
        public function getFeeReceipt()
        {
            return $this->belongsTo('App\Model\backend\StudentFeeReceipt','student_fee_receipt_id');
        }
        
        public function getFeeType()
        {
            /* fee_type_id is foreign key in fee table */

            return $this->belongsTo('App\Model\backend\FeeType', 'fee_type_id');
        }

        public function getFeeCircular()
        {
            /* fee_circular_id is foreign key in fee table */

            return $this->belongsTo('App\Model\backend\FeeCircular', 'fee_circular_id');
        }
}
