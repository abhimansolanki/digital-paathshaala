<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Story extends Model
    {
        use SoftDeletes;
        protected $table      = 'stories';
        protected $primaryKey = 'story_id';

        public function getSession()
        {
            /* session_id is foreign key in story table */
            return $this->belongsTo('App\Model\backend\Session', 'session_id');
        }

    }
    