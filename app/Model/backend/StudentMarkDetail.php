<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class StudentMarkDetail extends Model
    {
        use SoftDeletes;
        protected $table      = 'student_mark_details';
        protected $primaryKey = 'student_mark_detail_id';

        public function studentMark()
        {
            return $this->belongsTo('App\Model\backend\StudentMark', 'student_mark_id');
        }

        public function markStudent()
        {
            return $this->belongsTo('App\Model\backend\Student', 'student_id');
        }

    }
    