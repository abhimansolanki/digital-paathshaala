<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class TransportFee extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'transport_fees';
        protected $primaryKey = 'transport_fee_id';

        public function transportFeeDetail()
        {
            return $this->hasMany('App\Model\backend\TransportFeeDetail', 'transport_fee_id');
        }
        
        public function getTransportFeeStudent()
        {
            /* student_id is foreign key in fee table */
            return $this->belongsTo('App\Model\backend\Student', 'student_id');
        }
        public function getVehicleAssignedDetail()
        {
            /* student_id is foreign key in fee table */
            return $this->belongsTo('App\Model\backend\StudentVehicleAssignedDetail', 'student_vehicle_assigned_detail_id');
        }

        public static function getNewReceiptId()
        {
            $receipt_id = null;
            if(!empty(TransportFee::all()->last())){
                $receipt_id = TransportFee::all()->last()->transport_fee_id;
            }
            return $receipt_id + 1;
        }
    }
    