<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    protected $table = 'students';
    protected $primaryKey = 'student_id';

    public function getClass()
    {
        /* current_class_id is foreign key in student table */

        return $this->belongsTo('App\Model\backend\Classes', 'current_class_id');
    }

    public function getStudentType()
    {
        /* student_type_id is foreign key in student table */

        return $this->belongsTo('App\Model\backend\StudentType', 'student_type_id');
    }

    public function getSection()
    {
        /* current_section_id is foreign key in student table */

        return $this->belongsTo('App\Model\backend\Section', 'current_section_id');
    }

    public function getSession()
    {
        /* current_session_id is foreign key in student table */

        return $this->belongsTo('App\Model\backend\Session', 'current_session_id');
    }

    public function getParent()
    {
        return $this->belongsTo('App\Model\backend\StudentParent', 'student_parent_id');

    }

    public function getCaste()
    {
        /* student_type_id is foreign key in student table */

        return $this->belongsTo('App\Model\backend\CasteCategory', 'caste_category_id');
    }

    public function getProgress()
    {
        /* student_id is foreign key in progress table */
        return $this->hasMany('App\Model\backend\StudentProgress', 'student_id');
    }

    public function getLeave()
    {
        /* student_id is foreign key in progress table */
        return $this->hasMany('App\Model\backend\StudentLeave', 'student_id');
    }

    public function getRollNo()
    {
        /* student_id is foreign key in progress table */
        return $this->hasOne('App\Model\backend\ExamRollNumber', 'student_id');
    }

    public function studentAttendance()
    {
        return $this->hasMany('App\Model\backend\StudentAttendance', 'class_id');
    }

    public function getStudentFeeReceipt()
    {
        return $this->hasMany('App\Model\backend\StudentFeeReceipt', 'student_id');
    }

    public function StudentTc()
    {
        return $this->hasMany(StudentTc::class, 'class_id');
    }
}
    