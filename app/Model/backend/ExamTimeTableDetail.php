<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class ExamTimeTableDetail extends Model
    {
        use SoftDeletes;
        protected $table      = 'exam_time_table_details';
        protected $primaryKey = 'detail_id';

        public function examTimeTable()
        {
            /* exam_time_table_id is foreign key in Exam Time Table Detail */
            return $this->belongsTo('App\Model\backend\ExamTimeTable', 'exam_time_table_id');
        }

        public function examTimeTableClass()
        {
            return $this->belongsTo('App\Model\backend\Classes', 'class_id')->select('class_id','class_name');
        }
     
        public function examTimeTableSection()
        {
            return $this->belongsTo('App\Model\backend\Section', 'section_id')->select('section_id','section_name');
        }

        public function examTimeTableSubjects()
        {
            return $this->hasMany('App\Model\backend\ExamTimeTableSubject', 'detail_id');
        }

    }
    