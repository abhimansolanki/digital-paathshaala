<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class ExamRollNumber extends Model
    {
        use SoftDeletes;
        protected $table      = 'exam_roll_numbers';
        protected $primaryKey = 'exam_roll_number_id';

    }
    