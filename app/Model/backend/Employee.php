<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Employee extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'employees';
        protected $primaryKey = 'employee_id';

        public function employeeShift()
        {
            return $this->belongsTo('App\Model\backend\Shift', 'shift_id');
        }

        public function employeeBank()
        {
            return $this->belongsTo('App\Model\backend\Bank', 'bank_id');
        }

        public function employeeDepartment()
        {
            return $this->belongsTo('App\Model\backend\Department', 'department_id');
        }

        public function employeeUser()
        {
            return $this->belongsTo('App\Model\backend\AdminUser','admin_user_id');
            
        }
        public function employeeDocument()
        {
            return $this->hasMany('App\Model\backend\EmployeeDocument','employee_id');
            
        }

    }
    