<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class SubjectGroup extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'subject_groups';
        protected $primaryKey = 'subject_group_id';

        public function groupClassSubject()
        {
            return $this->hasMany('App\Model\backend\ClassSubject', 'subject_group_id');
        }

    }
    