<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class FeeCircular extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'fee_circulars';
        protected $primaryKey = 'fee_circular_id';

        public function getFeeCircular()
        {
            return $this->hasMany('App\Models\Fee');
        }

    }
    