<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class ClassSubject extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'class_subjects';
        protected $primaryKey = 'class_subject_id';

        public function classSub()
        {
            return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }

        public function getSub()
        {
            return $this->belongsTo('App\Model\backend\Subject', 'subject_id');
        }

        public function subGroup()
        {
            return $this->belongsTo('App\Model\backend\SubjectGroup', 'subject_group_id');
        }

    }
    