<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SmsTemplate extends Model
{
    use SoftDeletes;
    protected $table      = 'sms_templates';
    protected $primaryKey = 'sms_template_id';
}
