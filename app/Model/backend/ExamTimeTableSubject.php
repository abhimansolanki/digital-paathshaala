<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExamTimeTableSubject extends Model
{
    use SoftDeletes;
    protected $table      = 'exam_time_table_subjects';
    protected $primaryKey = 'detail_subject_id';
    
      public function subject()
        {
            return $this->belongsTo('App\Model\backend\Subject', 'subject_id')->select('subject_id','subject_name');
        }
}
