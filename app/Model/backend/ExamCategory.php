<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class ExamCategory extends Model
    {
        use SoftDeletes;
        protected $table      = 'exam_categories';
        protected $primaryKey = 'exam_category_id';

    }
    