<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class StudentTc extends Model
    {
        use SoftDeletes;
        protected $primaryKey = 'student_tc_id';
        protected $table = 'student_tcs';

        public function getClass()
        {
	/* current_class_id is foreign key in student table */

	return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }

        public function getSection()
        {
	/* current_section_id is foreign key in student table */

	return $this->belongsTo('App\Model\backend\Section', 'section_id');
        }

        public function getStudent()
        {
	/* current_section_id is foreign key in student table */

	return $this->belongsTo('App\Model\backend\Student', 'student_id');
        }

        public function getSession()
        {
	/* current_session_id is foreign key in student table */

	return $this->belongsTo('App\Model\backend\Session', 'session_id');
        }

    }
    