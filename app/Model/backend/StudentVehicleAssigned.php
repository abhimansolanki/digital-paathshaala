<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class StudentVehicleAssigned extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'student_vehicle_assigned';
        protected $primaryKey = 'student_vehicle_assigned_id';

        public function vehicleAssignedSession()
        {
            /* session_id is foreign key in student table */
            return $this->belongsTo('App\Model\backend\Session', 'session_id');
        }

        public function vehicleAssignedClass()
        {
            /* class_id is foreign key in student table */
            return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }

        public function vehicleAssignedStopPoint()
        {
            /* stop_point_id is foreign key in student table */

            return $this->belongsTo('App\Model\backend\StopPoint', 'stop_point_id');
        }

        public function vehicleAssignedVehicle()
        {
            /* vehicle_id is foreign key in student table */

            return $this->belongsTo('App\Model\backend\Vehicle', 'vehicle_id');
        }
   
        public function vehicleAssignedStudentDetail()
        {
            /* student_vehicle_assigned_id is foreign key in student_vehicle_assigned table */
            return $this->hasMany('App\Model\backend\StudentVehicleAssignedDetail', 'assigned_id');
        }

    }
    