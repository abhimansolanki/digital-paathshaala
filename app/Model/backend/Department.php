<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Department extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'departments';
        protected $primaryKey = 'department_id';

        public function departmentEmployee()
        {
            return $this->hasMany('App\Model\backend\Employee', 'department_id');
        }

    }
    