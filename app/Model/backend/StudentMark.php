<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class StudentMark extends Model
    {
        use SoftDeletes;
        protected $table      = 'student_marks';
        protected $primaryKey = 'student_mark_id';

        public function markDetails()
        {
            return $this->hasMany('App\Model\backend\StudentMarkDetail', 'student_mark_id');
        }

        public function markClass()
        {
            return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }
        public function markSection()
        {
            return $this->belongsTo('App\Model\backend\Section', 'section_id');
        }

        public function markExam()
        {
            return $this->belongsTo('App\Model\backend\CreateExam', 'exam_id');
        }
        public function markSession()
        {
            return $this->belongsTo('App\Model\backend\Session', 'session_id');
        }

    }
    