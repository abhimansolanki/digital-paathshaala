<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Fee extends Model
    {
        use SoftDeletes;
        //

        protected $table      = 'fees';
        protected $primaryKey = 'fee_id';

        public function getFeeType()
        {
            /* fee_type_id is foreign key in fee table */

            return $this->belongsTo('App\Model\backend\FeeType', 'fee_type_id');
        }

        public function getFeeCircular()
        {
            /* fee_circular_id is foreign key in fee table */

            return $this->belongsTo('App\Model\backend\FeeCircular', 'fee_circular_id');
        }

        public function getFeeClass()
        {
            /* class_id is foreign key in fee table */

            return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }

        public function getFeeSession()
        {
            /* session_id is foreign key in fee table */

            return $this->belongsTo('App\Model\backend\Session', 'session_id');
        }

        public function feeDetail()
        {
            /* fee_id is foreign key in fee_details table */
            return $this->hasMany('App\Model\backend\FeeDetail', 'fee_id');
        }

    }
    