<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EClassRoom extends Model
{
    use SoftDeletes;
    protected $table      = 'e_class_rooms';
    protected $primaryKey = 'e_class_room_id';

    public function classSub()
    {
        return $this->belongsTo('App\Model\backend\Classes', 'class_id');
    }

    public function getSub()
    {
        return $this->belongsTo('App\Model\backend\Subject', 'subject_id');
    }

    public function Section()
    {
        return $this->belongsTo(Section::class, 'section_id');
    }
}
