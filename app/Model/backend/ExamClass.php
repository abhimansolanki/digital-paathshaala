<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class ExamClass extends Model
    {
        use SoftDeletes;
        //
        protected $table = 'exam_classes';
        protected $primaryKey = 'exam_class_id';

        public function createExam()
        {
	/* exam_id is foreign key in Exam Class table */
	return $this->belongsTo('App\Model\backend\CreateExam', 'exam_id');
        }

        public function examClasses()
        {
	/* classs_id is foreign key in Exam Class table */
	return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }

        public function examSection()
        {
	/* section_id is foreign key in Exam Class table */
	return $this->belongsTo('App\Model\backend\Section', 'section_id');
        }

    }
    