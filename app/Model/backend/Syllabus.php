<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Syllabus extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'syllabi';
        protected $primaryKey = 'syllabus_id';

        public function syllabusClass()
        {
            return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }

        public function syllabusSubject()
        {
            return $this->belongsTo('App\Model\backend\Subject', 'subject_id');
        }

    }
    