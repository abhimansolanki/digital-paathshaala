<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;

    class UserType extends Model
    {

        //
        protected $table      = 'user_types';
        protected $primaryKey = 'user_type_id';

        public function userRole()
        {
            /* user_type_id is foreign key in role  table */
            return $this->hasOne('App\Model\backend\UserRole', 'user_type_id');
        }

    }
    