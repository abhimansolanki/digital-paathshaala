<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentType extends Model
{
    use SoftDeletes;
    protected $table = 'student_types';
    protected $primaryKey = 'student_type_id';
    
    public function getStudentType()
    {
        return $this->hasMany('App\Models\Student');
    }
}
