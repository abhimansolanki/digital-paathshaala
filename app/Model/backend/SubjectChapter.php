<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubjectChapter extends Model
{
    use SoftDeletes;
    protected $table      = 'subject_chapters';
    protected $primaryKey = 'subject_chapter_id';

    public function classSub()
    {
        return $this->belongsTo('App\Model\backend\Classes', 'class_id');
    }

    public function getSub()
    {
        return $this->belongsTo('App\Model\backend\Subject', 'subject_id');
    }

    public function Section()
    {
        return $this->belongsTo(Section::class, 'section_id');
    }

    public function Session()
    {
        return $this->belongsTo(Session::class, 'session_id');
    }

    public function ChapterTopic()
    {
        return $this->hasMany(ChapterTopic::class, 'chapter_topic_id');
    }
}
