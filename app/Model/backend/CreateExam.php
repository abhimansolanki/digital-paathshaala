<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class CreateExam extends Model
    {
        use SoftDeletes;
        protected $table      = 'create_exams';
        protected $primaryKey = 'exam_id';

        public function Examclass()
        {
            return $this->hasMany('App\Model\backend\ExamClass','exam_id');
        }

        public function examCategory()
        {
            /* exam_category_id is foreign key in Exam table */
            return $this->belongsTo('App\Model\backend\ExamCategory', 'exam_category_id');
        }

    }
    