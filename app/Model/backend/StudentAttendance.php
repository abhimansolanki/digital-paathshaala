<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentAttendance extends Model
{
    use SoftDeletes;
    protected $table = 'student_attendances';
    protected $primaryKey = 'student_attendance_id';
    
     public function attendanceStudent()
        {
            /* student_id is foreign key in student table */

            return $this->belongsTo('App\Model\backend\Student', 'student_id');
        }
     public function studentAttendanceClass()
        {
            /* student_id is foreign key in student table */

            return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }
}
