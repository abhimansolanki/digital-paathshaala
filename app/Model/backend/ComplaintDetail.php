<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class ComplaintDetail extends Model
    {
        use SoftDeletes;
        protected $table      = 'complaint_details';
        protected $primaryKey = 'complaint_detail_id';

        public function Complaint()
        {
            /* complaint_id is foreign key in complaint detail table */
            return $this->belongsTo('App\Model\backend\Complaint', 'complaint_id');
        }

        public function AdminUser()
        {
            /* complaint_id is foreign key in complaint detail table */
            return $this->belongsTo('App\Model\backend\AdminUser', 'admin_user_id');
        }

    }
    