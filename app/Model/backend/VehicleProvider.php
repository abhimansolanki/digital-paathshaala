<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleProvider extends Model
{
    use SoftDeletes;
    protected $table = 'vehicle_providers';
    protected $primaryKey = 'vehicle_provider_id';
}
