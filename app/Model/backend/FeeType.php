<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class FeeType extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'fee_types';
        protected $primaryKey = 'fee_type_id';

        public function getFeeType()
        {
            return $this->hasMany('App\Models\Fee');
        }

    }
    