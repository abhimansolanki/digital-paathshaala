<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class SchoolGallery extends Model
    {
        use SoftDeletes;
        protected $primaryKey = 'school_gallery_id';
        protected $table = 'school_galleries';

        public function getGalleryAlbum()
        {
	return $this->belongsTo('App\Model\backend\SchoolGalleryAlbum', 'album_id');
        }

    }
    