<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasteCategory extends Model
{
    use SoftDeletes;
    protected $table = 'caste_categories';
    protected $primaryKey = 'caste_category_id';
    
    public function getStudentCaste()
    {
        return $this->hasMany('App\Models\Student','caste_category_id');
    }
}
