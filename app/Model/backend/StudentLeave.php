<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class StudentLeave extends Model
    {
        use SoftDeletes;
        protected $table      = 'student_leaves';
        protected $primaryKey = 'student_leave_id';
        
        public function studentLeaveClass()
        {
            return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }

    }
    