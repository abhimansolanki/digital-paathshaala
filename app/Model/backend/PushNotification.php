<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PushNotification extends Model
{
    use SoftDeletes;
    protected $table ="push_notifications";
    protected $primaryKey ="push_notification_id";
    
    public function notificationDevice()
    {
        return $this->hasMany('App\Model\backend\PushNotificationDevice','push_notification_id');
    }
}
