<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventGallery extends Model
{
    use SoftDeletes;
    protected $table = 'event_galleries';
    protected $primaryKey = 'event_gallery_id';
    
        public function getEvent()
        {
            return $this->belongsTo('App\Model\backend\Event','event_id');
        }
}
