<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class StudentEnquire extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'student_enquires';
        protected $primaryKey = 'student_enquire_id';

        public function studentEnquireClass()
        {
            /* class_id is foreign key in student enquire table */

            return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }
          public function getCaste()
        {
            /* caste_category_id is foreign key in student enquire table */

            return $this->belongsTo('App\Model\backend\CasteCategory', 'caste_category_id');
        }


    }
    