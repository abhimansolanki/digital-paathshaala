<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeDocument extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'employee_document_id';
    protected $table = 'employee_documents';
}
