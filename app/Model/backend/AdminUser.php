<?php

    namespace App\Model\backend;

//    use App\Notifications\AdminResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Illuminate\Auth\Passwords\CanResetPassword;

    class AdminUser extends Authenticatable
    {

        use Notifiable;
        use CanResetPassword;
        use SoftDeletes;

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
//        protected $fillable = [
//            'name', 'email', 'password',
//        ];

        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden     = [
            'password', 'remember_token',
        ];
        protected $table      = 'admin_users';
        protected $primaryKey = 'admin_user_id';

        public function getUserSchool()
        {
            return $this->has('App\Models\backend\School','admin_user_id');
        }
        
        public function adminStudentParent()
        {
            return $this->hasOne('App\Model\backend\StudentParent','admin_user_id');
            
        }
        public function userEmployee()
        {
            return $this->hasOne('App\Model\backend\Employee','admin_user_id');
            
        }
         public function userRole()
        {
            return $this->belongsTo('App\Model\backend\UserRole', 'user_role_id');
        }
         public function userType()
        {
            return $this->belongsTo('App\Model\backend\UserType', 'user_type_id');
        }

    }
    