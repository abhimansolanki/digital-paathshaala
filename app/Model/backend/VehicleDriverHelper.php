<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleDriverHelper extends Model
{
    use SoftDeletes;
    protected $table = 'vehicle_driver_helpers';
    protected $primaryKey = 'vehicle_helper_id';
}
