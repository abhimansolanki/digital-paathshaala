<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Bank extends Model
    {
        use SoftDeletes;
        protected $table      = 'banks';
        protected $primaryKey = 'bank_id';

        public function bankEmployee()
        {
            return $this->hasMany('App\Model\backend\Employee', 'bank_id');
        }

    }
    