<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Complaint extends Model
    {
        use SoftDeletes;
        protected $table      = 'complaints';
        protected $primaryKey = 'complaint_id';

        public function complaintDetail()
        {
            /* complaint_id is foreign key in complaint detail table */
            return $this->hasMany('App\Model\backend\ComplaintDetail', 'complaint_id');
        }

        public function Student()
        {
            /* complaint_id is foreign key in complaint detail table */
            return $this->belongsTo('App\Model\backend\Student', 'student_id');
        }

    }
    