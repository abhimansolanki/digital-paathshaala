<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class StopPoint extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'stop_points';
        protected $primaryKey = 'stop_point_id';

        public function stopPointRoute()
        {
            return $this->belongsTo('App\Model\backend\Route', 'route_id');
        }

    }
    