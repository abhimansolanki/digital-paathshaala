<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptionDetail extends Model
{
    use SoftDeletes;
    protected $table      = 'subscription_details';
    protected $primaryKey = 'subscription_detail_id';
}
