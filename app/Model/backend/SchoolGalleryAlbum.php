<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolGalleryAlbum extends Model
{
    use SoftDeletes;
    protected $primaryKey  = 'album_id';
    protected $table  = 'school_gallery_albums';
    public function getGalleryImages()
    {
        return $this->hasMany('App\Model\backend\SchoolGallery','album_id');
    }
}
