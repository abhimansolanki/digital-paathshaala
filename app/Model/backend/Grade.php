<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Grade extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'grades';
        protected $primaryKey = 'grade_id';

    }
    