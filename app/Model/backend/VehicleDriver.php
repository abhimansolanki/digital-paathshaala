<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleDriver extends Model
{
    use SoftDeletes;
    protected $table = 'vehicle_drivers';
    protected $primaryKey = 'vehicle_driver_id';
}
