<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class PaymentMode extends Model
    {
        use SoftDeletes;
        protected $table      = 'payment_modes';
        protected $primaryKey = 'payment_mode_id';

    }
    