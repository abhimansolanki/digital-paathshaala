<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentParent extends Model
{
    //
    use SoftDeletes;
    protected $table = 'student_parents';
    protected $primaryKey = 'student_parent_id';

    public function getSchool()
    {
        return $this->belongsTo('App\Model\backend\School','school_id');
    }
    
    public function getParentStudent()
    {
        return $this->hasMany('App\Model\backend\Student','student_parent_id');
    }
}
