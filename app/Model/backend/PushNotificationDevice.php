<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PushNotificationDevice extends Model
{
    use SoftDeletes;
    protected $table ="push_notification_devices";
    protected $primaryKey ="push_notification_device_id";
}
