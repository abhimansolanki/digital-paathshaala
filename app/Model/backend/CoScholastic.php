<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoScholastic extends Model
{
    use SoftDeletes;
     protected $table      = 'co_scholastics';
     protected $primaryKey = 'co_scholastic_id';
}
