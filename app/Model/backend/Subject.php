<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Subject extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'subjects';
        protected $primaryKey = 'subject_id';

//        public function subjectClass()
//        {
//            return $this->belongsToMany('App\Model\backend\Classes', 'class_subjects');
//        }

        public function subjectGroup()
        {
            return $this->belongsToMany('App\Model\backend\SubjectGroup', 'subject_group_id');
        }

        public function subSubject()
        {
            return $this->hasMany('App\Model\backend\SubSubject', 'subject_id');
        }
        public function subjectClass()
        {
            return $this->hasMany('App\Model\backend\ClassSubject', 'subject_id');
        }

    }
    