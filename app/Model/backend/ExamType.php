<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExamType extends Model
{
    use SoftDeletes;
    protected $table      = 'exam_types';
    protected $primaryKey = 'exam_type_id';
}
