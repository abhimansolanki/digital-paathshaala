<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Classes extends Model
    {
        use SoftDeletes;
        protected $table      = 'classes';
        protected $primaryKey = 'class_id';

        public function getStudentClass()
        {
            $session = get_current_session();
            if ($session['session_status'] != 1)
            {
                return $this->hasMany('App\Model\backend\StudentHistory', 'current_class_id');
            }
            else
            {
                return $this->hasMany('App\Model\backend\Student', 'current_class_id');
            }
        }

        public function classStudentEnquire()
        {
            return $this->hasMany('App\Model\backend\StudentEnquire', 'class_id');
        }

        public function getClassFee()
        {
            return $this->hasMany('App\Model\backend\Fee');
        }

        public function classStudentAttendance()
        {
            return $this->hasMany('App\Model\backend\StudentAttendance', 'class_id');
        }

        public function classSubject()
        {
            return $this->hasMany('App\Model\backend\ClassSubject', 'class_id');
        }

//        public function classSubject()
//        {
//            return $this->belongsToMany('App\Model\backend\Subject', 'class_subjects', 'class_id', 'subject_id','subject_group_id')
//                    ->withPivot('subject_type')
//                    ->withTimestamps();
//        }
        public function classSection()
        {
            return $this->hasMany('App\Model\backend\ClassSection', 'class_id');
        }

        public function classStudentLeave()
        {
            return $this->hasMany('App\Model\backend\StudentLeave', 'class_id');
        }

        public function classExam()
        {
            return $this->belongsToMany('App\Model\backend\CreateExam', 'exam_classes', 'class_id', 'exam_id');
        }

    }
    