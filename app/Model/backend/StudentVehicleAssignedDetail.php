<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class StudentVehicleAssignedDetail extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'student_vehicle_assigned_details';
        protected $primaryKey = 'student_vehicle_assigned_detail_id';

        public function StudentVehicleAssigned()
        {
            /* student_vehicle_assigned_id is foreign key in student_vehicle_assigned table */
            return $this->belongsTo('App\Model\backend\StudentVehicleAssigned', 'student_vehicle_assigned_id');
        }
        public function vehicleAssignedStudent()
        {
            /* student_id is foreign key in student table */

            return $this->belongsTo('App\Model\backend\Student', 'student_id');
        }
        public function vehicleAssignedEmployee()
        {
            /* student_id is foreign key in student table */

            return $this->belongsTo('App\Model\backend\Student', 'student_id');
        }

    }
    