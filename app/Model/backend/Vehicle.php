<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Vehicle extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'vehicles';
        protected $primaryKey = 'vehicle_id';

        public function vehicleRoute()
        {
            return $this->belongsTo('App\Model\backend\Route', 'route_id');
        }

        public function vehicleProvider()
        {
            return $this->belongsTo('App\Model\backend\VehicleProvider', 'vehicle_provider_id');
        }

        public function vehicleDriver()
        {
            return $this->belongsTo('App\Model\backend\VehicleDriver', 'vehicle_driver_id');
        }

        public function vehicleHelper()
        {
            return $this->belongsTo('App\Model\backend\VehicleDriverHelper', 'vehicle_helper_id');
        }
        public function vehicleAssigned()
        {
            return $this->hasMany('App\Model\backend\StudentVehicleAssigned', 'vehicle_id');
        }

    }
    