<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shift extends Model
{
    use SoftDeletes;
    protected $table = 'shifts';
    protected $primaryKey = 'shift_id';
    
    public function shiftEmployee()
        {
            return $this->hasMany('App\Model\backend\Employee', 'shift_id');
        }
}
