<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class FeeDetail extends Model
    {
        use SoftDeletes;
        protected $table      = 'fee_details';
        protected $primaryKey = 'fee_detail_id';

        public function feeDetailFee()
        {
            /* fee_id is foreign key in fee table */

            return $this->belongsTo('App\Model\backend\Fee', 'fee_id');
        }

    }
    