<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{
    //
    use SoftDeletes;
    protected $table = 'schools';
    protected $primaryKey = 'school_id';

    public function getStudentParent()
    {
        return $this->hasMany('App\Models\backend\StudentParent');
    }
    
    public function getSchoolStudent()
    {
        return $this->hasMany('App\Models\backend\Student');
    }
    
    public function getSchoolEvents()
    {
        return $this->hasMany('App\Models\backend\Event');
    }
    public function getSchoolHolidays()
    {
        return $this->hasMany('App\Models\backend\Holiday');
    }
    
    public function getschoolUser()
    {
        return $this->belongsTo('App\Model\backend\AdminUser','admin_user_id');
       
    }
}
