<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Route extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'routes';
        protected $primaryKey = 'route_id';

        public function routeStopPoint()
        {
            return $this->hasMany('App\Model\backend\StopPoint', 'route_id');
        }

    }
    