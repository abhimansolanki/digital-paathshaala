<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Event extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'events';
        protected $primaryKey = 'event_id';

        public function getSchool()
        {
            return $this->belongsTo('App\Model\backend\School', 'school_id');
        }

        public function getSession()
        {
            /* current_session_id is foreign key in student table */

            return $this->belongsTo('App\Model\backend\Session', 'session_id');
        }

        public function getEventGallery()
        {
            return $this->hasMany('App\Model\backend\EventGallery','event_id');
        }

    }
    