<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Session extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'sessions';
        protected $primaryKey = 'session_id';

        public function getSessionStudent()
        {
            return $this->hasMany('App\Models\Student');
        }

        public function getSessionFee()
        {
            return $this->hasMany('App\Models\Fee');
        }
        
        public function getSessionEmployee()
        {
            return $this->hasMany('App\Models\Employee','session_id');
        }

    }
    