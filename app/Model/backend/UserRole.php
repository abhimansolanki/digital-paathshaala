<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRole extends Model
{
    use SoftDeletes;
        //
    protected $table = 'user_roles';
    protected $primaryKey = 'user_role_id';

    public function userRoleTypeEmployee()
    {
        return $this->hasMany('App\Model\backend\Employee', 'user_role_id');
    }

}
    