<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class ClassCoScholastic extends Model
    {
        use SoftDeletes;
        protected $table = 'class_co_scholastics';
        protected $primaryKey = 'class_co_scholastic_id';

        public function CoScholasticClass()
        {
	/* class_id is foreign key in Class CoScholastic table */

	return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }

        public function CoScholasticSection()
        {
	/* class_id is foreign key in Class CoScholastic table */

	return $this->belongsTo('App\Model\backend\Section', 'section_id');
        }

        public function CoScholasticExamCategory()
        {
	/* exam_category_id is foreign key in Class CoScholastic table */

	return $this->belongsTo('App\Model\backend\ExamCategory', 'exam_category_id');
        }

    }
    