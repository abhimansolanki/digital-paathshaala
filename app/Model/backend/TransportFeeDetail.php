<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class TransportFeeDetail extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'transport_fee_details';
        protected $primaryKey = 'transport_fee_detail_id';

        public function transportFeeDetail()
        {
            return $this->belongsTo('App\Model\backend\TransportFee', 'transport_fee_id');
        }

    }
    