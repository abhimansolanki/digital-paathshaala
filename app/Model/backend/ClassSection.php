<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class ClassSection extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'class_sections';
        protected $primaryKey = 'class_section_id';

        public function classSection()
        {
            return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }
        public function Section()
        {
            return $this->belongsTo('App\Model\backend\Section', 'section_id');
        }

    }
    