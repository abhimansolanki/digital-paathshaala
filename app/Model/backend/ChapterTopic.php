<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChapterTopic extends Model
{
    use SoftDeletes;
    protected $table      = 'chapter_topics';
    protected $primaryKey = 'chapter_topic_id';

    public function SubjectChapter()
    {
        return $this->belongsTo(SubjectChapter::class, 'subject_chapter_id');
    }
}
