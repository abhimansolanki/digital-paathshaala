<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class ExamTimeTable extends Model
    {
        use SoftDeletes;
        protected $table      = 'exam_time_tables';
        protected $primaryKey = 'exam_time_table_id';
        
        public function timeTableExam()
        {
            /* exam_id is foreign key in Exam Time Table */
            return $this->belongsTo('App\Model\backend\CreateExam', 'exam_id')->select('exam_id','exam_name');
        }

        public function examTimeTableDetails()
        {
            return $this->hasMany('App\Model\backend\ExamTimeTableDetail', 'exam_time_table_id');
        }

    }
    