<?php

    namespace App\Model\backend;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class StudentFeeReceipt extends Model
    {
        use SoftDeletes;
        //
        protected $table      = 'student_fee_receipts';
        protected $primaryKey = 'student_fee_receipt_id';

        public function feeReceiptDetails()
        {
            return $this->hasMany('App\Model\backend\StudentFeeReceiptDetail', 'student_fee_receipt_id');
        }
        public function getFeeClass()
        {
            /* class_id is foreign key in fee table */
            return $this->belongsTo('App\Model\backend\Classes', 'class_id');
        }
        public function getFeeSession()
        {
            /* session_id is foreign key in fee table */
            return $this->belongsTo('App\Model\backend\Session', 'session_id');
        }
        public function getFeeStudent()
        {
            /* student_id is foreign key in fee table */
            return $this->belongsTo('App\Model\backend\Student', 'student_id');
        }
        public function getPaymentMode()
        {
            /* payment_mode_id is foreign key in fee table */
            return $this->belongsTo('App\Model\backend\PaymentMode', 'payment_mode_id');
        }

        public static function getNewReceiptId()
        {
            $receipt_id = null;
            if(!empty(StudentFeeReceipt::all()->last())){
                //$receipt_id = StudentFeeReceipt::all()->last()->student_fee_receipt_id;
                $receipt_id = StudentFeeReceipt::all()->last()->receipt_number;
            }
            return $receipt_id + 1;
        }

    }
    