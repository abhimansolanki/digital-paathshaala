<?php

    namespace App\Mail;

    use Illuminate\Bus\Queueable;
    use Illuminate\Mail\Mailable;
    use Illuminate\Queue\SerializesModels;
    use Illuminate\Contracts\Queue\ShouldQueue;

    class SendMailable extends Mailable
    {

        use Queueable,
            SerializesModels;

        /**
         * Create a new message instance.
         *
         * @return void
         */
        public function __construct($data)
        {
            $this->data = $data;
        }

        /**
         * Build the message.
         *
         * @return $this
         */
        // not taking data in build method thats why its passed in construct
        public function build()
        {
            $view_blade = $this->data['view_blade'];
//            $address    = $this->data['cc_address'];
//            $name       = $this->data['cc_name'];
            $subject    = $this->data['subject'];
            $body       = $this->data['body'];
            return $this->view('backend.send-mail.' . $view_blade)
//                    ->cc($address, $name)
//                    ->bcc($address, $name)
//                    ->replyTo($address, $name)
                    ->subject($subject)
                    ->with(['mail_body' => $body]);
        }

    }
    