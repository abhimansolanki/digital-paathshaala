<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\backend\AdminUser;
use App\Model\backend\UserType;
use Illuminate\Support\Facades\Hash;
use Session as LSession;

class CheckAdmin
{

    public function handle($request, Closure $next)
    {
        $admin_user = get_loggedin_user_data();

        if (!empty($admin_user)) {
            return $next($request);
                // if (in_array($admin_user['user_type'], $admin_user['allowed_user']))
                // {
                // }
                // else
                // {
                //     auth()->guard()->logout();
                //     $request->session()->invalidate();
                //     return redirect()->intended('admin/login/')->with('status', 'You are unauthorized to access dashboard!');
                // }
        } else {
            return redirect()->intended('admin/login/')->with('status', 'Please Login to access admin');
        }
    }

}
    