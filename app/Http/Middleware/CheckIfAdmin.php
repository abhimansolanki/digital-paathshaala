<?php

    namespace App\Http\Middleware;

    use Closure;
    use Illuminate\Support\Facades\Auth;

    class CheckIfAdmin
    {

        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Closure  $next
         * @param  string|null  $guard
         * @return mixed
         */
        public function handle($request, Closure $next, $guard = 'admin')
        {
            // check login condition
            if (!(Auth::guard('admin')->user()) && $guard != 'admin')
            {
                return redirect()->intended('admin/login/')->with('status', 'Please Login to access admin panel');
            }
            // already loggedin and try to access login url
            if (Auth::guard('admin')->user())
            {
                return redirect()->intended('admin/dashboard/');
            }

            return $next($request);
        }

    }
    