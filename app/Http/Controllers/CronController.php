<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Model\backend\PushNotification;
use App\Model\backend\PushNotificationDevice;

class CronController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        DB::table('test_cron')->insert(['text' => 'hello there']);
    }

    public function pushNotificationEvent()
    {
        $current_date = date('Y-m-d');

        // send notification for event
        $event_date = date('Y-m-d', strtotime($current_date . ' + 3 days'));
        $arr_event = DB::table('events')->where('event_start_date', "$event_date")
            ->orWhere('event_start_date', "$current_date")->get();
        $arr_user = DB::table('user_devices')->where('notification_status', 'Yes')->get();
        if (count($arr_event) > 0) {
            foreach ($arr_event as $event) {
                foreach ($arr_user as $key => $user_device) {
                    $json_data = [
                        "to" => $user_device->device_id,
//		    "notification" => [
//		        "title" => $event->event_name,
//		        "icon" => "ic_launcher"
//		    ],
                        "data" => [
                            "body" => html_entity_decode($event->event_description),
                            "title" => $event->event_name,
                            "id" => $event->event_id,
                            "notification_type" => 1,// for vent
                        ]
                    ];

                    $this->sendNotification($json_data);
                }
            }
        }
    }

    public function pushNotificationNotice($notice, $notice_for, $arr_student, $class_id, $section_id, $session_id)
    {
//	$current_date = date('Y-m-d');
//	// send notification for notice
//	$notice_date = date('Y-m-d', strtotime($current_date . ' + 3 days'));
//	$arr_notice = DB::table('notices')->where('start_date', $notice_date)->where('notice_for', 1)
//		    ->orWhere('event_start_date', $current_date)->get();
//	$arr_user = DB::table('user_devices')->where('notification_status', 'Yes')->get();
        // send notification for notice

        if ($notice_for == 1 || $notice_for == 7) {
            $query = DB::table('students as s')
                ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
                ->join('user_devices as ud', 'ud.admin_user_id', '=', 'sp.admin_user_id')
                ->where('notification_status', 'Yes')
                ->where('s.current_session_id', $session_id)
                ->select('s.first_name', 's.middle_name', 's.last_name', 'ud.device_id');
            if (!empty($arr_student)) {
                $query->whereIn('s.student_id', $arr_student);
            }
            if (!empty($class_id)) {
                if(is_array($class_id)){
                    $query->whereIn('s.current_class_id', $class_id);
                }else{
                    $query->where('s.current_class_id', $class_id);
                }
            }
            if (!empty($section_id)) {
                if(is_array($section_id)){
                    $query->whereIn('s.current_section_id', $section_id);
                }else{
                    $query->where('s.current_section_id', $section_id);
                }
            }

            $arr_student = $query->get();

            foreach ($arr_student as $student) {

                $json_data = [
                    "to" => $student->device_id,
//	        "notification" => [
//		"icon" => "ic_launcher",
//		"title" => $notice->notice_title,
//	        ],
                    "data" => [
                        "body" => $notice->notice,
                        "title" => $notice->notice_title,
                        "id" => $notice->notice_id,
                        "notification_type" => 2, // for notice
                    ]
                ];
                $this->sendNotification($json_data);
            }
        }
    }

    public function pushNotificationAttendance($arr_student = [], $session_id, $student_attendance_id)
    {

        // send notification for notice
        $query = DB::table('students as s')
            ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
            ->join('user_devices as ud', 'ud.admin_user_id', '=', 'sp.admin_user_id')
            ->where('notification_status', 'Yes')
            ->where('s.current_session_id', $session_id)
            ->whereIn('s.student_id', $arr_student)
            ->select('s.student_id', 's.first_name', 's.middle_name', 's.last_name', 'ud.device_id');

        $arr_user = $query->get();
        if (!empty($arr_user)) {
            foreach ($arr_user as $key => $user) {
                $json_data = [
                    "to" => $user->device_id,
//		"notification" => [
//		    "icon" => "ic_launcher",
//		    "title" => 'Attendance alert !',
//		],
                    "data" => [
                        "title" => 'Attendance alert !',
                        "student_id" => $user->student_id,
                        "id" => $student_attendance_id,
                        "notification_type" => 3, // for attendance
                        "body" => 'Dear Parent, Your pupil ' . $user->first_name . ' ' . $user->middle_name . ' ' . $user->last_name . ' is absent today',
                    ]
                ];

                $this->sendNotification($json_data);
            }
        }
    }


    public function pushNotificationWorkDiary($work_diary)
    {
        // send notification for notice
        $student_id = $work_diary->student_id;
        $query = DB::table('students as s')
            ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
            ->join('user_devices as ud', 'ud.admin_user_id', '=', 'sp.admin_user_id')
            ->where('notification_status', 'Yes')
            ->where('s.current_session_id', $work_diary->session_id)
            ->where('s.current_class_id', $work_diary->class_id)
            ->where('s.current_section_id', $work_diary->section_id)
            ->where(function ($q) use ($student_id) {
                if (!empty($student_id)) {
                    $q->where('s.student_id', $student_id);
                }
            })
            ->select('s.student_id', 's.first_name', 's.middle_name', 's.last_name', 'ud.device_id');

        $arr_user = $query->get();
        if (!empty($arr_user)) {
            foreach ($arr_user as $key => $user) {
                $json_data = [
                    "to" => $user->device_id,
//		"notification" => [
//		    "icon" => "ic_launcher",

//		],
                    "data" => [
                        "title" => 'Work Diary Updated !',
                        "student_id" => $user->student_id,
                        "id" => $work_diary->work_diary_id,
                        "notification_type" => 4, // for work diary
                        "body" => 'Dear Parent, Its time to check ' . $work_diary->work_type == 1 ? 'C.W.' : 'H.W.' . ' of subject : ' . $work_diary->workSubject->subject_name,
                    ]
                ];

                $this->sendNotification($json_data);
            }
        }
    }

    public function pushNotificationStudentTC($student_tc)
    {
        $student_id = $student_tc->student_id;
        $arr_user = DB::table('students as s')
            ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
            ->join('user_devices as ud', 'ud.admin_user_id', '=', 'sp.admin_user_id')
            ->where('s.student_id', $student_id)
            ->select('s.student_id', 's.first_name', 's.middle_name', 's.last_name', 'ud.device_id', 's.student_left')
            ->get();
        if (!empty($arr_user)) {
            foreach ($arr_user as $key => $user) {
                $body = "";
                if($user->student_left == "Yes"){
                    $body = "Dear Parent, School Issue the TC document of subject : $user->first_name $user->middle_name $user->last_name";
                }else{
                    $body = "Dear Parent, School has cancelled your Issued the TC document of subject : $user->first_name $user->middle_name $user->last_name";
                }
                $json_data = [
                    "to" => $user->device_id,
//                    "notification" => [
//                        "icon" => "ic_launcher",
//                    ],
                    "data" => [
                        "title" => 'Student TC Issued !',
                        "student_id" => $user->student_id,
                        "id" => $student_tc->student_tc_id,
                        "notification_type" => 5,
                        "body" => $body,
                    ]
                ];
                $this->sendNotification($json_data);
            }
        }
    }

    public function pushNotificationInactiveStudent($student_ids)
    {
        $arr_user = DB::table('students as s')
            ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
            ->join('user_devices as ud', 'ud.admin_user_id', '=', 'sp.admin_user_id')
            ->whereIn('s.student_id', $student_ids)
            ->select('s.student_id', 's.first_name', 's.middle_name', 's.last_name', 'ud.device_id')
            ->get();
        if (!empty($arr_user)) {
            foreach ($arr_user as $key => $user) {
                $json_data = [
                    "to" => $user->device_id,
//                    "notification" => [
//                        "icon" => "ic_launcher",
//                    ],
                    "data" => [
                        "title" => 'Student App Inactive !',
                        "student_id" => $user->student_id,
                        "id" => null,
                        "notification_type" => 6,
                        "body" => "Dear Parent, You fee is due. Please pay to use app features.",
                    ]
                ];
                $this->sendNotification($json_data);
            }
        }
    }

    public function pushNotificationStudentResult($student_ids)
    {
        $arr_user = DB::table('students as s')
            ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
            ->join('user_devices as ud', 'ud.admin_user_id', '=', 'sp.admin_user_id')
            ->whereIn('s.student_id', $student_ids)
            ->select('s.student_id', 's.first_name', 's.middle_name', 's.last_name', 'ud.device_id')
            ->get();
        if (!empty($arr_user)) {
            foreach ($arr_user as $key => $user) {
                $json_data = [
                    "to" => $user->device_id,
//                    "notification" => [
//                        "icon" => "ic_launcher",
//                    ],
                    "data" => [
                        "title" => 'Student Result !',
                        "student_id" => $user->student_id,
                        "id" => null,
                        "notification_type" => 7,
                        "body" => "Dear Student, Your result has been published.",
                    ]
                ];
                $this->sendNotification($json_data);
            }
        }
    }

    public function pushNotificationStudentHoliday($holiday)
    {
        $arr_user = DB::table('students as s')
            ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
            ->join('user_devices as ud', 'ud.admin_user_id', '=', 'sp.admin_user_id')
            ->where('s.current_session_id', $holiday->session_id)
            ->select('s.student_id', 's.first_name', 's.middle_name', 's.last_name', 'ud.device_id')
            ->where('student_left',"No")
            ->get();
        $start_date = get_formatted_date($holiday->holiday_start_date, "display");
        $end_date = get_formatted_date($holiday->holiday_end_date, "display");
        if ($start_date == $end_date) {
            $body = "Dear Parent, School Holiday announced on $start_date";
        } else {
            $body = "Dear Parent, School Holiday announced from $start_date to $end_date";
        }
        if (!empty($arr_user)) {
            foreach ($arr_user as $key => $user) {
                $json_data = [
                    "to" => $user->device_id,
//                    "notification" => [
//                        "icon" => "ic_launcher",
//                    ],
                    "data" => [
                        "title" => 'School Holiday !',
                        "student_id" => $user->student_id,
                        "id" => null,
                        "notification_type" => 9,
                        "body" => $body
                    ]
                ];
                $this->sendNotification($json_data);
            }
        }
    }

    public function pushNotificationStudentFee($student_id, $fee_amount = null, $is_reminder = false)
    {
        $arr_user = DB::table('students as s')
            ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
            ->join('user_devices as ud', 'ud.admin_user_id', '=', 'sp.admin_user_id')
            ->where('s.student_id', $student_id)
            ->select('s.student_id', 's.first_name', 's.middle_name', 's.last_name', 'ud.device_id')
            ->where('student_left',"No")
            ->get();
        if($is_reminder){
            if(!empty($fee_amount)){
                $body = "Dear Parent, This is reminder for student fee of $fee_amount amount. Please pay due amount to avoid late fees.";
            }else{
                $body = "Dear Parent, This is reminder for student fee. Please pay due amount to avoid late fees.";
            }
        } else {
            if(!empty($fee_amount)) {
                $body = "Dear Parent, Student fee is due of $fee_amount amount. Please pay due fee amount.";
            }else{
                $body = "Dear Parent, Student fee is due. Please pay due fee amount.";
            }
        }
        if (!empty($arr_user)) {
            foreach ($arr_user as $key => $user) {
                $json_data = [
                    "to" => $user->device_id,
//                    "notification" => [
//                        "icon" => "ic_launcher",
//                    ],
                    "data" => [
                        "title" => 'Student Fees !',
                        "student_id" => $user->student_id,
                        "id" => null,
                        "notification_type" => 10,
                        "body" => $body
                    ]
                ];
                $this->sendNotification($json_data);
            }
        }
    }

    /**
     * @param $json_data
     * Event - 1
     * Notice - 2
     * Attendance - 3
     * Work Diary  - 4
     * Logout - 5
     * Inactive App - 6
     * Exam Result - 7
     * Syllabus Update - 8
     * Holiday - 9
     * Student Fee Notify - 10
     */
    function sendNotification($json_data)
    {
        DB::beginTransaction(); //Start transaction!
        try {
//	$json_data = [
//		"to" => "eQCndAc00yc:APA91bECdVlfGJwbfNRHBljCVJct9EEYqpt7EZl96O6QtPtLdCTCNlDJvkJj8TQrEPGTuyxxgJSEt3dAmrcpEF7yc5v0pX717uDnBuVmQToz4Qw9nl3xFi0QMBxuUPCyrI4RxT-YYvkt",
//		"notification" => [
//		    "icon" => "ic_launcher",
//		    "title" => 'Attendance alert !',
//		    "body" => 'Dear Parent, Your pupil ',
//		],
//		    //    "data" => [
//		    //        "ANYTHING EXTRA HERE"
//		    //    ]
//	        ];

            $data = json_encode($json_data);
//	p($data);
            //FCM API end-point
            $url = 'https://fcm.googleapis.com/fcm/send';
            //api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
//	$server_key = 'AAAAglEC2WA:APA91bHYg19EJkE24EKrCLlqp2cYekR8U9HUrju0lbbVjsddAo7azZMh50ZwSpEao-9QxjT9N0Zpr5QSJj6Uhe5yRT4A-GG-lfdxx1ZRbCGUnFOIDG2wqhi0_chs4dA8Fbd7BfWnox70';
            $server_key = 'AAAAglEC2WA:APA91bFjFFF8ftQmc3T-sZzt0f8DTP1rlWS2uDaYonKdAboP-bN_4iS8UMsqdDwSwW56VUxKcOlVtQlnBuYizRQ77NYE9bAdCc-p-YVAJ6iFOrkhWlMx6yKYQhjbRFqhdF8ObMF2D9Ws';
            //header with content_type api key
            $headers = array(
                'Content-Type:application/json',
                'Authorization:key=' . $server_key
            );
            //CURL request to route notification to FCM connection server (provided by Google)
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($ch);

            $err = curl_error($ch);
            if ($result === FALSE) {
//                die('Oops! FCM Send Error: ' . curl_error($ch));
            }
            if ($err) {
//                echo "cURL Error #:" . $err;
            } else {
//                echo $result;
            }
            $data = json_decode($result, true);

            $notification_data = new PushNotification;
            $notification_data->multicast_id = $data['multicast_id'];
            $notification_data->canonical_ids = $data['canonical_ids'];
            $notification_data->results = json_encode($data['results'], true);
            $notification_data->success = $data['success'];
//		$notification_data->failed=$data['failed'];
            $notification_data->failure = $data['failure'];
            $notification_data->notification_type = 1;
            $notification_data->notification_data = json_encode($json_data['data']);

            $notification_data->save();
            $device_data = [];
            foreach ($json_data as $key => $value) {
                $notification_device = new PushNotificationDevice;
                $notification_device->device_id = $json_data['to'];
                $device_data[] = $notification_device;
            }
            $notification_data->notificationDevice()->saveMany($device_data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
//            p($message);
        }
        DB::commit();
    }

}

