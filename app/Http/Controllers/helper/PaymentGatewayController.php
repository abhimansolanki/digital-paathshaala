<?php


namespace App\Http\Controllers\helper;

use App\Model\backend\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PaymentGatewayController
{
    public function orderCreate($receipt, $amount, $fee_type = 1, $appid, $secretkey)
    {
        try {
            $appid_secretkey = $appid . ':' . $secretkey;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.razorpay.com/v1/orders');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"amount\": $amount,\n  \"currency\": \"INR\",\n  \"receipt\": \"$receipt\"\n}");
            curl_setopt($ch, CURLOPT_USERPWD, $appid_secretkey);
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                p($ch);
            }
            curl_close($ch);
            $result = json_decode($result);
            return $result;
        } catch (\Exception $e) {
            p($e->getMessage());
        }
    }

    public function checkPaymentStatus($order_id, $payment_receipt, $fee_type){
        if ($fee_type == 1) {
            $school = DB::table("schools")->first();
            $appid = $school->payment_gateway_app_id;
            $secretkey = $school->payment_gateway_secret_key;
            //          Shishu Vatika School Razorpay account details
//            $appid = 'rzp_live_KeRBRVuOk7dKjL';
//            $secretkey = 'kakLedcG8niGGvHtvIPkTUZE';
        } else {
            //          Abhishek Solanki Razorpay account details
            $appid = 'rzp_live_xZM69cIh0K6ZfD';
            $secretkey = 's4VPfUZMR9OV4nHcxliBUtzz';
            //          $appid = 'rzp_test_vutZgyRBM4ZugJ';
            //          $secretkey = 'KM9lqvM9B7TTqTG20rKUSY9O';
        }
        $detail_payment_receipt = json_decode($payment_receipt);
        $transaction = Transaction::where('order_id',$order_id)->first();
        $transaction->payment_receipt = $payment_receipt;
        $str = $detail_payment_receipt->razorpay_order_id."|".$detail_payment_receipt->razorpay_payment_id;
        $generated_signature = hash_hmac('sha256', $str, $secretkey);
        if ($generated_signature == $detail_payment_receipt->razorpay_signature) {
            $payment_status = 1;
        }else{
            $payment_status = 2;
        }
        $transaction->payment_status = $payment_status;
        $transaction->save();
        if($payment_status == 1){
            return true;
        }else{
            return false;
        }
    }
}