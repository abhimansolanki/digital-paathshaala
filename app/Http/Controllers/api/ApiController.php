<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\backend\StudentTransportFeeReceiptController;
use App\Http\Controllers\helper\PaymentGatewayController;
use App\Model\backend\ChapterTopic;
use App\Model\backend\EClassRoom;
use App\Model\backend\Fee;
use App\Model\backend\FeeDetail;
use App\Model\backend\School;
use App\Model\backend\Session;
use App\Model\backend\StudentFeeReceipt;
use App\Model\backend\StudentFeeReceiptDetail;
use App\Model\backend\SubscriptionDetail;
use App\Model\backend\Transaction;
use Illuminate\Http\Request;
use App\Model\backend\SchoolGalleryAlbum;

//    use App\Model\backend\Student;
use App\Model\backend\Event;
use App\Model\backend\Holiday;

//    use App\Model\backend\School;
use App\Model\backend\StudentAttendance;
use App\Model\backend\Student;
use App\Model\backend\WorkDiary;
use App\Model\backend\Complaint;
use App\Model\backend\ComplaintDetail;
use App\Model\backend\StudentLeave;

//    use App\Model\backend\ExamTimeTable;
use App\Model\backend\ExamTimeTableDetail;
use App\Model\backend\StudentProgress;
use App\Http\Controllers\Controller;

//    use App\Http\Resources\Api as ApiResource;
//    use App\Http\Resources\ApiCollection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use App\Http\Controllers\backend\StudentAttendanceController;
use DateTime;
use App\Model\backend\Notice;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\backend\StudentFeeReceiptNewController;
use App\Model\backend\SubjectChapter;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ApiController extends Controller
{
//        public function show($id)
//        {
//	return new ApiResource(AdminUser::find($id));
//        }
//
//        public function showall()
//        {
//	return new ApiCollection(AdminUser::all());
//        }


    /*
     * Configuration API LIST
     */

    /**
     * show complaint counts on
     */
    function generateUserToken($admin_user_id)
    {
        $latest_two = DB::table('api_token')->where(array('admin_user_id' => $admin_user_id))->orderBy('token_id', 'desc')->limit(1)->get()->pluck('token_id')->toArray();
        DB::table('api_token')->where(array('admin_user_id' => $admin_user_id))->whereNotIn('token_id', $latest_two)->delete();
        $token = quick_random(8);
        $arr_toke_data = array(
            'admin_user_id' => $admin_user_id,
            'device_id' => '',
            'token' => Hash::make($token),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );
        DB::table('api_token')->insert($arr_toke_data);
        return $token;
    }

    function verifyToken($request)
    {
        $return = false;
        if (!empty($request['admin_user_id']) && !empty($request['token'])) {
            $token = $request['token'];
            $admin_user_id = $request['admin_user_id'];
            $arr_token = DB::table('api_token')->where('admin_user_id', $admin_user_id)->where('status', 1)->get();
            foreach ($arr_token as $db_token) {
                if (Hash::check($token, $db_token->token)) {
                    $return = true;
                }
            }
        }
        return $return;
    }

    function get_all_exam($exam_id = null, $class_id = null, $session_id = null, $section_id = null)
    {
        $arr_exam_list = [];
        if (empty($session_id)) {
            $session = db_current_session();
            $session_id = $session['session_id'];
        }
        $query = DB::table('create_exams as ce')->where('exam_status', 1);
        if (!empty($exam_id)) {
            $query->where('ce.exam_id', $exam_id);
        }
        if (!empty($class_id)) {
            $query->join('exam_classes as ec', 'ec.exam_id', '=', 'ce.exam_id');
            $query->where('ec.session_id', $session_id);
            $query->where('class_id', $class_id);
            $query->where('section_id', $section_id);
        }
        if (!empty($session_id)) {
            $query->where('ce.session_id', $session_id);
        }
        $arr_exam_list = $query->select('ce.exam_name', 'ce.exam_id')->orderBy('exam_name', 'ASC')->get();
        $arr_exam_list = json_decode($arr_exam_list);
        return $arr_exam_list;
    }

    function get_current_session_months_api()
    {
        $arr = [];
        $arr_month = get_current_session_months();
        foreach ($arr_month as $key => $value) {
            $arr[] = array(
                'month_id' => $key,
                'month' => $value,
            );
        }
        return $arr;
    }

    function get_class_section_subject($session_id, $class_id, $section_id)
    {
        $class_subject = [];
        $arr_subject = DB::table('subjects as s')
            ->join('class_subjects as cs', 'cs.subject_id', '=', 's.subject_id')
            ->where('cs.class_id', $class_id)
            ->where('cs.session_id', $session_id)
            ->where('cs.section_id', $section_id)
            ->where('s.subject_status', 1)
            ->select('s.subject_id', 's.subject_name', 's.root_id')
            ->get();
        $arr_subject = json_decode($arr_subject, true);
        if (!empty($arr_subject)) {
            $arr_root_id = array_unique(array_column($arr_subject, 'root_id'));
            foreach ($arr_subject as $key => $value) {
                if (!in_array($value['subject_id'], $arr_root_id)) {
                    $class_subject[] = array(
                        'subject_id' => $value['subject_id'],
                        'subject_name' => $value['subject_name'],
                    );
                }
            }
        }
        return $class_subject;
    }

    /*
     * Parent App API List
     */

    /* ======================================================= Parent App Start===================================================================================== */

    /*         * login parent */

    public function parentLogin(Request $request)
    {
        /*
         * Variable initialization
         */
        $result_data = (object)[];
        /*
         * input variables
         */
        $mobile = $request->input('mobile');
        $password = $request->input('password');

        if (!empty($request->input())) {
            $arr_conditions = [
                'mobile_number' => $mobile,
                'password' => $password,
            ];
            $user_mobile = DB::table('admin_users')->where('mobile_number', $arr_conditions['mobile_number'])->first();
            if (!empty($user_mobile)) {
                if (Hash::check($arr_conditions['password'], $user_mobile->password)) {
                    $data['admin_user_id'] = $user_mobile->admin_user_id;
                    $data['device_id'] = $request->input('device_id');
                    $save_device = $this->saveUserDevice($data);

                    /*
                     * generate api  token and insert into db
                     */
                    $token = $this->generateUserToken($user_mobile->admin_user_id);

                    $parent_student_info = $this->getStudentParentInformation($user_mobile->admin_user_id);
                    $is_session_promoted = false;
                    if (empty($parent_student_info)) {
                        $is_session_promoted = true;
                    }
                    $parent_student_info['token'] = $token;
                    $parent_student_info['is_session_promoted'] = $is_session_promoted;
                    $result_data = $parent_student_info;
                    $status = trans('language.success_status');
                    $message = trans('language.success_message');
                } else {
                    $status = trans('language.failed_status');
                    $message = trans('language.invalid_password');
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.invalid_mobile');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.empty_fields');
        }
        $result_response['data'] = $result_data;

        if (!empty($result_data->admin_user_id)) {
            $result_response['data']['school_logo'] = url(get_school_logo());
        }
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /*
     * get student school and parent information
     */
    public function getStudentParentInformation($admin_user_id)
    {
        $parent_student_info = [];
        $arr_care_of = \Config::get('custom.care_of');
        $arr_student = DB::table('student_parents as sp')
            ->join('students as s', 's.student_parent_id', '=', 'sp.student_parent_id')
            ->join('caste_categories as cc', 'cc.caste_category_id', '=', 's.caste_category_id')
            ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
            ->join('sessions as ses', 'ses.session_id', '=', 's.current_session_id')
            ->join('sections as sec', 'sec.section_id', '=', 's.current_section_id')
            ->select('s.*', 'sp.*', 'c.class_name', 'sec.section_name', 'cc.caste_name')
            ->where('s.student_status', 1)
            ->where('sp.admin_user_id', $admin_user_id)
            ->where('ses.session_status', 1)->get();

        $student_parent = isset($arr_student[0]) ? $arr_student[0] : [];
        if (!empty($student_parent)) {
            $student_parent = (array)$student_parent;

            /*
                     * generate api  token and insert into db
                     */
//            $token = $this->generateUserToken($admin_user_id);
            $school = School::find(1);

            $active_subscription = false;
            $active_subscription_details = [];
            $subscription = SubscriptionDetail::where([['student_parent_id', '=', $student_parent['student_parent_id']], ['payment_status', '=', 1]])->where(function ($query) {
//                $query->whereDate('start_date','=>',date('Y-m-d'));
//                $query->whereDate('end_date','<=',date('Y-m-d'));
                // $query->where(DB::raw('DATE(start_date) >= ' . date('Y-m-d') . ' AND DATE(end_date) <= ' . date('Y-m-d')));
                $query->whereDate('end_date', '>=', date('Y-m-d'));
            })->first();
            if (!empty($subscription)) {
                $active_subscription = true;
                array_push($active_subscription_details, array(
                    'order_id' => $subscription->order_id,
                    'amount' => $subscription->amount,
                    'payment_date' => $subscription->payment_date,
                    'start_date' => $subscription->start_date,
                    'end_date' => $subscription->end_date,
                ));
            }

            $parent_student_info = array(
                'admin_user_id' => check_empty($student_parent['admin_user_id'], 'int'),
                'token' => "",
                'student_parent_id' => check_empty($student_parent['student_parent_id'], 'int'),
                'care_of' => check_empty($student_parent['care_of'], 'varchar'),
                'sibling_exist' => check_empty($student_parent['sibling_exist'], 'int'),
                'care_of_rel' => check_empty($arr_care_of[$student_parent['care_of']], 'varchar'),
                'father_email' => check_empty($student_parent['father_email'], 'varchar'),
                'father_aadhaar_number' => check_empty($student_parent['father_aadhaar_number'], 'varchar'),
                'father_name' => $student_parent['father_first_name'] . ' ' . $student_parent['father_middle_name'] . ' ' . $student_parent['father_last_name'],
                'father_first_name' => check_empty($student_parent['father_first_name'], 'varchar'),
                'father_middle_name' => check_empty($student_parent['father_middle_name'], 'varchar'),
                'father_last_name' => check_empty($student_parent['father_last_name'], 'varchar'),
                'father_occupation' => check_empty($student_parent['father_occupation'], 'varchar'),
                'father_contact_number' => check_empty($student_parent['father_contact_number'], 'varchar'),
                'father_income' => check_empty($student_parent['father_income'], 'varchar'),
                'mother_name' => $student_parent['mother_first_name'] . ' ' . $student_parent['mother_middle_name'] . ' ' . $student_parent['mother_last_name'],
                'mother_email' => check_empty($student_parent['mother_email'], 'varchar'),
                'mother_aadhaar_number' => check_empty($student_parent['mother_aadhaar_number'], 'varchar'),
                'mother_first_name' => check_empty($student_parent['mother_first_name'], 'varchar'),
                'mother_middle_name' => check_empty($student_parent['mother_middle_name'], 'varchar'),
                'mother_last_name' => check_empty($student_parent['mother_last_name'], 'varchar'),
                'mother_occupation' => check_empty($student_parent['mother_occupation'], 'varchar'),
                'mother_contact_number' => check_empty($student_parent['mother_contact_number'], 'varchar'),
                'mother_income' => check_empty($student_parent['mother_income'], 'varchar'),
                'guardian_email' => check_empty($student_parent['guardian_email'], 'varchar'),
                'guardian_aadhaar_number' => check_empty($student_parent['guardian_aadhaar_number'], 'varchar'),
                'guardian_name' => $student_parent['guardian_first_name'] . ' ' . $student_parent['guardian_middle_name'] . ' ' . $student_parent['guardian_last_name'],
                'guardian_first_name' => check_empty($student_parent['guardian_first_name'], 'varchar'),
                'guardian_middle_name' => check_empty($student_parent['guardian_middle_name'], 'varchar'),
                'guardian_last_name' => check_empty($student_parent['guardian_last_name'], 'varchar'),
                'guardian_occupation' => check_empty($student_parent['guardian_occupation'], 'varchar'),
                'guardian_contact_number' => check_empty($student_parent['guardian_contact_number'], 'varchar'),
                'guardian_relation' => check_empty($student_parent['guardian_relation'], 'varchar'),
                'guardian_contact_number' => check_empty($student_parent['guardian_contact_number'], 'varchar'),
                'father_income' => check_empty($student_parent['father_income'], 'varchar'),
                'school_details' => array(
                    'whatsapp_phone' => check_empty($school->whatsapp_phone, 'varchar'),
                    'website_url' => check_empty($school->website_url, 'varchar'),
                    'facebook_url' => check_empty($school->facebook_url, 'varchar'),
                    'instagram_url' => check_empty($school->instagram_url, 'varchar'),
                    'twitter_url' => check_empty($school->twitter_url, 'varchar'),
                    'youtube_url' => check_empty($school->youtube_url, 'varchar'),
                    'linkedin_url' => check_empty($school->linkedin_url, 'varchar'),
                    'app_version' => intval(number_format($school->app_version, 2)),
                    'mandatory_update' => ($school->mandatory_update == 1),
                ),
                'active_subscription' => $active_subscription,
                'active_subscription_details' => $active_subscription_details,
                'arr_student' => [],
            );

            $arr_student_document = \Config::get('custom.student_document');
            foreach ($arr_student as $student) {
                $student = (array)$student;
                if (!empty($student['student_id'])) {

                    $doc_status = 'Pending';
                    $arr_document = [];
                    foreach ($arr_student_document as $key => $doc) {
                        $doc_status = 'Pending';
                        if (!empty($student['student_doc']) && in_array($key, explode(',', $student['student_doc']))) {
                            $doc_status = 'Submitted';
                        }
                        $arr_document[] = ['document_name' => $doc, 'status' => $doc_status];
                    }
                    $parent_student_info['arr_student'][] = array(
                        'student_id' => check_empty($student['student_id'], 'int'),
                        'form_number' => check_empty($student['form_number'], 'varchar'),
                        'enrollment_number' => check_empty($student['enrollment_number'], 'varchar'),
                        'session_id' => check_empty($student['current_session_id'], 'int'),
                        'class_id' => check_empty($student['current_class_id'], 'int'),
                        'section_id' => check_empty($student['current_section_id'], 'int'),
                        'section_name' => check_empty($student['section_name'], 'varchar'),
                        'student_type_id' => check_empty($student['student_type_id'], 'int'),
                        'caste_category_id' => check_empty($student['caste_category_id'], 'int'),
                        'new_student' => check_empty($student['new_student'], 'int'),
                        'discount_date' => get_formatted_date($student['discount_date'], 'display'),
                        'admission_date' => get_formatted_date($student['admission_date'], 'display'),
                        'join_date' => get_formatted_date($student['join_date'], 'display'),
                        'fee_discount' => check_empty($student['fee_discount'], 'varchar'),
                        'previous_due_fee' => check_empty($student['previous_due_fee'], 'varchar'),
                        'remark' => check_empty($student['remark'], 'varchar'),
                        'fee_calculate_month' => check_empty($student['fee_calculate_month'], 'varchar'),
                        'student_left' => check_empty($student['student_left'], 'int'),
                        'reason' => check_empty($student['reason'], 'varchar'),
                        'first_name' => check_empty($student['first_name'], 'varchar'),
                        'student_name' => check_empty($student['first_name'] . ' ' . $student['middle_name'] . ' ' . $student['last_name'], 'varchar'),
                        'middle_name' => check_empty($student['middle_name'], 'varchar'),
                        'last_name' => check_empty($student['last_name'], 'varchar'),
                        'birth_place' => check_empty($student['birth_place'], 'varchar'),
                        'dob' => get_formatted_date($student['dob'], 'display'),
                        'tc_date' => get_formatted_date($student['tc_date'], 'display'),
                        'gender_name' => get_gender($student['gender'], 'student_gender'),
                        'gender' => check_empty($student['gender'], 'varchar'),
                        'aadhaar_number' => check_empty($student['aadhaar_number'], 'varchar'),
                        'profile' => check_file_exist($student['profile_photo'], 'student_profile', 'yes', true),
                        'previous_school_name' => check_empty($student['previous_school_name'], 'varchar'),
                        'previous_class_name' => check_empty($student['previous_class_name'], 'varchar'),
                        'previous_result' => check_empty($student['previous_result'], 'varchar'),
                        'tc_number' => check_empty($student['tc_number'], 'varchar'),
                        'address_line1' => check_empty($student['address_line1'], 'varchar'),
                        'address_line2' => check_empty($student['address_line2'], 'varchar'),
                        'caste_name' => check_empty($student['caste_name'], 'varchar'),
                        'class_name' => check_empty($student['class_name'], 'varchar'),
                        'app_access_fee_status' => $student_parent['app_access_fee_status'] == 1 ? true : false,
                        'arr_document' => $arr_document,
                        'app_accessbility_status' => $student_parent['app_accessbility_status'] == 1 ? true : false,
                    );
                }
            }
        }
        return $parent_student_info;
    }

    /*
     * get event list for parent app
     */

    public function eventsList(Request $request)
    {
        $result_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $event_for = $request->input('event_for');
            if ($event_for == 1 || $event_for == 2) {
                $session = db_current_session();
                $session_id = $session['session_id'];

                $arr_events = Event::
                where(function ($query) use ($event_for) {
                    $query->where('event_for', $event_for);
                    $query->orWhere('event_for', 3); // for  both
                })
                    ->where('session_id', $session_id)
                    ->with('getEventGallery')
                    ->orderBy('event_start_date', 'DESC')
                    ->get();

                if (!empty($arr_events)) {
                    $past_event = [];
                    $future_event = [];
                    foreach ($arr_events as $events) {
                        $arr_event_gallery = [];
                        foreach ($events['getEventGallery'] as $event_gallery) {
                            $arr_event_gallery[] = array(
                                'event_gallery_id' => $event_gallery->event_gallery_id,
                                'url' => check_file_exist($event_gallery->name, 'event_gallery', 'yes'),
                            );
                        }
                        $result_data[] = array(
                            'event_id' => $events->event_id,
                            'event_for' => $events->event_for,
                            'created_at' => date('Y-m-d H:i:s', strtotime($events->created_at)),
                            'event_name' => check_empty($events->event_name, 'varchar'),
                            'event_start_date' => check_empty($events->event_start_date, 'varchar'),
//                            'event_end_date' => check_empty($events->event_end_date, 'varchar'),
//                            'event_time' => check_empty($events->event_time, 'varchar'),
                            'event_description' => check_empty($events->event_description, 'varchar'),
                            'event_status' => check_empty($events->event_status, 'int'),
                            'event_gallery' => $arr_event_gallery,
                        );
                    }
                    // sort fee by fee type
//		$name = 'start_date';
//		usort($fee_receipt, function ($a, $b) use(&$name)
//		{
//		    return $a[$name] - $b[$name];
//		});
                    $status = trans('language.success_status');
                    $message = trans('language.success_message');
                } else {
                    $status = trans('language.failed_status');
                    $message = trans('language.failed_message');
                }
            } else {
                $status = trans('language.failed_status');
                $message = 'Invalid Request'; //trans('language.invalid_request');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $result_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /*
     * get event list for parent app
     */

    public function getSchoolGallery(Request $request)
    {
        $result_data = [];
        if ($this->verifyToken($request->input()) == true) {

            $arr_school_gallery = SchoolGalleryAlbum::with(['getGalleryImages' => function ($q) {
                $q->orderBy('created_at', 'DESC');
            }])->orderBy('created_at', 'DESC')->get()->toArray();

            if (!empty($arr_school_gallery)) {
                foreach ($arr_school_gallery as $school_gallery) {
                    $arr_gallery = [];
                    foreach ($school_gallery['get_gallery_images'] as $gallery_images) {
                        $arr_gallery[] = array(
                            'school_gallery_id' => $gallery_images['school_gallery_id'],
                            'image_url' => check_file_exist($gallery_images['image_url'], 'school_gallery', 'yes'),
                            'created_at' => date('Y-m-d H:i:s', strtotime($gallery_images['created_at'])),
                        );
                    }
                    $result_data[] = array(
                        'album_id' => $school_gallery['album_id'],
                        'album_title' => $school_gallery['album_title'],
                        'description' => $school_gallery['description'],
                        'created_at' => date('Y-m-d H:i:s', strtotime($school_gallery['created_at'])),
                        'gallery_images' => $arr_gallery,
                    );
                }
                $status = trans('language.success_status');
                $message = trans('language.success_message');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.failed_message');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $result_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /*
     * get Holiday list for parent app
     */

    public function holidaysList(Request $request)
    {
        $result_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $session = db_current_session();
            $session_id = $session['session_id'];
            $arr_holidays = Holiday::select('holiday_id', 'holiday_name', 'holiday_start_date', 'holiday_end_date')
                ->where('session_id', $session_id)->orderBy('created_at', 'DESC')->get()->toArray();

            if (!empty($arr_holidays[0]['holiday_id'])) {
                $result_data = $arr_holidays;
                $status = trans('language.success_status');
                $message = trans('language.success_message');
            } else {
                $status = trans('language.success_status');
                $message = trans('language.failed_message');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $result_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* Apply leave by parent or student */

    public function saveStudentLeave(Request $request)
    {
        $data_response = (object)[];
        if ($this->verifyToken($request->input()) == true) {
            $student_leave_id = $request->input('student_leave_id');
            $student_id = $request->input('student_id');
            $class_id = $request->input('class_id');
            $session = db_current_session();
            $session_id = $session['session_id'];
            $section_id = $request->input('section_id');
            $leave_type = $request->input('leave_type');
            $leave_start_date = get_formatted_date($request->input('leave_start_date'), 'database');
            $leave_end_date = get_formatted_date($request->input('leave_end_date'), 'database');
            $reason = $request->input('reason');

            if (!empty($class_id) && !empty($student_id) && !empty($session_id) && !empty($leave_start_date) && !empty($leave_end_date) && (!empty($reason) && !empty($leave_type))) {
                DB::beginTransaction();
                try {
                    DB::enableQueryLog();
                    $query = DB::table('student_leaves')->select('student_leave_id')->where('class_id', $class_id)
                        ->where('session_id', $session_id)->where('student_id', $student_id)
                        ->where(function ($q) use ($leave_start_date, $leave_end_date) {
                            $q->whereBetween('leave_start_date', array("$leave_start_date", "$leave_end_date"));
                            $q->orWhereBetween('leave_end_date', array("$leave_start_date", "$leave_end_date"));
                        });
                    if (!empty($student_leave_id)) {
                        $query->where('student_leave_id', '!=', $student_leave_id);
                    }
                    $leave_data = $query->first();
                    if (empty($leave_data->student_leave_id)) {
                        if (!empty($student_leave_id)) {
                            $student_leave = StudentLeave::find($student_leave_id);
                        } else {
                            $student_leave = new StudentLeave;
                        }
                        $student_leave->class_id = $class_id;
                        $student_leave->student_id = $student_id;
                        $student_leave->section_id = $section_id;
                        $student_leave->session_id = $session_id;
                        $student_leave->leave_type = $leave_type;
                        $student_leave->reason = $reason;
                        $student_leave->leave_start_date = $leave_start_date;
                        $student_leave->leave_end_date = $leave_end_date;
                        $student_leave->save();
                        $status = trans('language.success_status');
                        $message = trans('language.leave_save_success');
                    } else {
                        $status = trans('language.failed_status');
                        $message = trans('language.leave_already_exist');
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    //p($error_message);
                    $status = trans('language.failed_status');
                    $message = trans('language.leave_save_failed');
                }
                DB::commit();
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $data_response;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /*
     * get Leave list for parent app
     */

    public function leaveList(Request $request)
    {
        $result_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $student_id = $request->input('student_id');
            if (!empty($student_id)) {
                $session = db_current_session();
                $session_id = $session['session_id'];
                $arr_leave = StudentLeave::select('student_leave_id', 'leave_start_date', 'leave_end_date', 'leave_type', 'reason', 'student_id')
                    ->where('session_id', $session_id)
                    ->where('student_id', $student_id)
                    ->orderBy('created_at', 'DESC')
                    ->get()->toArray();
                if (!empty($arr_leave[0]['student_leave_id'])) {
                    $result_data = $arr_leave;
                    $status = trans('language.success_status');
                    $message = trans('language.success_message');
                } else {
                    $status = trans('language.success_status');
                    $message = trans('language.failed_message');
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $result_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* get story list* */

    public function storyList(Request $request)
    {
        $result_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $date = date('Y-m-d');
            $limit = \Config::get('custom.story_count');
            $arr_stories = DB::table('stories as st')
                ->join('sessions as s', 's.session_id', '=', 'st.session_id')
                ->where('s.session_status', 1)
                ->where('st.story_status', 1)
                ->orderBy('st.created_at', 'DESC')
                ->where('st.start_date', '<=', $date)
                ->where('st.end_date', '>=', $date)
                ->select('st.story_id', 'story_title', 'st.story_url', 'st.created_at')
                ->get();
            if (!empty($arr_stories)) {
                foreach ($arr_stories as $story) {
                    $result_data[] = array(
                        'story_id' => $story->story_id,
                        'post_on' => $story->created_at,
                        'story_title' => check_empty($story->story_title, 'varchar'),
                        'story_url' => check_file_exist($story->story_url, 'story', 'yes'),
                    );
                }
                $status = trans('language.success_status');
                $message = trans('language.success_message');
            } else {
                $status = trans('language.success_status');
                $message = trans('language.failed_message');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $result_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* Save Complaint */

    public function saveComplaint(Request $request)
    {
        $data_response = (object)[];
        if ($this->verifyToken($request->input()) == true) {
            $complaint_id = $request->input('complaint_id');
            $student_id = $request->input('student_id');
            $session = db_current_session();
            $session_id = $session['session_id'];
            $heading = $request->input('heading');
            $description = $request->input('description');

            if (!empty($heading) && !empty($student_id) && !empty($session_id) && !empty($description)) {
                DB::beginTransaction();
                try {
                    if (!empty($complaint_id)) {
                        $complaint = Complaint::find($complaint_id);
                    } else {
                        $complaint = new Complaint;
                    }
                    $complaint->student_id = $student_id;
                    $complaint->session_id = $session_id;
                    $complaint->heading = $heading;
                    $complaint->description = $description;
                    $complaint->save();

                    $status = trans('language.success_status');
                    $message = trans('language.complaint_save_success');
                } catch (\Exception $e) {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $status = trans('language.failed_status');
                    $message = trans('language.complaint_save_failed');
                }
                DB::commit();
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $data_response;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* Complaint list  with details */

    public function complaintList(Request $request)
    {
        $result_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $admin_user_id = $request->input('admin_user_id');
            $student_id = $request->input('student_id');
            $session = db_current_session();
            $session_id = $session['session_id'];
            if (!empty($request->input('student_id'))) {
                //1:Pending, 2:Resolved, 3:Rejected
                $arr_complaint = Complaint::with(['complaintDetail' => function ($query) use ($admin_user_id) {

                }])
                    ->select('student_id', 'complaint_id', 'heading', 'description', 'status')
                    ->where('student_id', $student_id)
                    ->where('session_id', $session_id)
                    ->orderBy('complaint_id', 'DESC')
                    ->get()->toArray();

                if (!empty($arr_complaint)) {
                    $result_data = $arr_complaint;
                    $status = trans('language.success_status');
                    $message = trans('language.success_message');
                } else {
                    $status = trans('language.success_status');
                    $message = trans('language.failed_message');
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $result_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* Save Complaint Message */

    public function saveComplaintMessage(Request $request)
    {
        $data_response = (object)[];
        if ($this->verifyToken($request->input()) == true) {
            $complaint_id = $request->input('complaint_id');
            $message = $request->input('message'); // chat message
            $admin_user_id = $request->input('admin_user_id'); // chat message

            if (!empty($complaint_id) && !empty($message)) {
                DB::beginTransaction();
                try {
                    $complaint_detail = new ComplaintDetail;
                    $complaint_detail->complaint_id = $complaint_id;
                    $complaint_detail->message = $message;
                    $complaint_detail->admin_user_id = $admin_user_id;
                    $complaint_detail->save();

                    $status = trans('language.success_status');
                    $message = trans('language.complaint_message_sent');
                } catch (\Exception $e) {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                    $status = trans('language.failed_status');
                    $message = trans('language.complaint_save_failed');
                }
                DB::commit();
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $data_response;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* get student work  like CW or HW */

    public function getStudentWork(Request $request)
    {
        $work_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $student_id = $request->input('student_id');
            $class_id = $request->input('class_id');
            $session = db_current_session();
            $session_id = $session['session_id'];
            $section_id = $request->input('section_id');
            $work_type = $request->input('work_type');
            $work_date = $request->input('work_date');
            $result_data = [];

            if (!empty($class_id) && !empty($session_id)) {
                if (!empty($work_date)) {
                    $date = get_formatted_date($work_date, 'database');
                }
//	        else
//	        {
//		$date = get_formatted_date($work_date, 'database');
//	        }
                $query = DB::table('work_diaries as wd')
                    ->join('subjects as s', 's.subject_id', '=', 'wd.subject_id')
                    ->select('wd.*', 's.subject_id', 's.subject_name')
                    ->where(function ($q) use ($class_id, $section_id, $student_id) {
                        $q->where('wd.class_id', $class_id);
                        $q->where('wd.section_id', $section_id);
                        if (!empty($student_id)) {
                            $q->orWhere('wd.student_id', $student_id);
                        }
                    })
                    ->where('wd.session_id', $session_id)
                    ->OrderBy('wd.work_start_date', 'DESC')
                    ->OrderBy('wd.subject_id');
                if (!empty($date)) {
                    $query->where('wd.work_start_date', '<=', "$date");
                    $query->where('wd.work_end_date', '>=', "$date");
                }


                if (!empty($work_type)) {
                    $query->where('wd.work_type', $work_type);
                }

                $arr_work = $query->get();
                if (!empty($arr_work)) {
                    $work_type = \Config::get('custom.arr_work_type');
                    foreach ($arr_work as $key => $work) {
                        $work = (array)$work;
                        $work_data[] = array(
                            'work_diary_id' => check_empty($work['work_diary_id'], 'int'),
                            'class_id' => check_empty($work['class_id'], 'int'),
                            'session_id' => check_empty($work['session_id'], 'int'),
                            'section_id' => check_empty($work['section_id'], 'int'),
                            'work_start_date' => get_formatted_date($work['work_start_date'], 'display'),
                            'work_end_date' => get_formatted_date($work['work_end_date'], 'display'),
                            'subject_id' => check_empty($work['subject_id'], 'int'),
                            'subject_name' => check_empty($work['subject_name'], 'varchar'),
                            'work_type' => check_empty($work['work_type'], 'int'),
                            'work_type_name' => check_empty($work_type[$work['work_type']], 'varchar'),
                            'work' => check_empty($work['work'], 'varchar'),
                            'work_doc_title' => check_empty($work['work_doc_file_title'], 'varchar'),
                            'work_doc' => check_file_exist($work['work_doc_file'], 'work_doc', 'yes'),
                            'work_doc_download_permission' => (isset($work['work_doc_file_download_permission']) && $work['work_doc_file_download_permission'] == 1) ? true : false,
                            'work_doc_video_title' => check_empty($work['work_doc_video_title'], 'varchar'),
                            'work_doc_video' => check_file_exist($work['work_doc_video'], 'work_doc', 'yes'),
                            'work_doc_link_title' => check_empty($work['work_doc_link_title'], 'varchar'),
                            'work_doc_link' => $work['work_doc_link'],
                        );
                    }
                    $status = trans('language.success_status');
                    $message = trans('language.work_diary_success_message');
                } else {
                    $status = trans('language.success_status');
                    $message = trans('language.work_diary_failed_message');
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $work_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* get student profile */

    public function getSchoolProfile(Request $request)
    {
        $result_data = (object)[];
        if ($this->verifyToken($request->input()) == true) {
            $result_data = get_school_data();
            $result_data['school_logo'] = url($result_data['school_logo']);
            $status = trans('language.success_status');
            $message = trans('language.success_message');
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $result_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* get student Progress report subject wise* */

    public function getStudentProgress(Request $request)
    {
        $progress = [];
        if ($this->verifyToken($request->input()) == true) {
            $student_id = $request->input('student_id');
            $class_id = $request->input('class_id');
            $session = db_current_session();
            $session_id = $session['session_id'];
            $section_id = $request->input('section_id');

            if (!empty($student_id) && !empty($session_id)) {
                DB::enableQueryLog();
                $progress_data = DB::table('student_progresses as spr')
                    ->join('subjects as s', 'spr.subject_id', '=', 's.subject_id')
                    ->select('spr.student_id', 'spr.subject_id', 'spr.progress', 's.subject_name', 'spr.month', 'spr.student_progress_id')
                    ->where('spr.session_id', $session_id)
                    ->where('spr.student_id', $student_id)
                    ->where('spr.class_id', $class_id)
                    ->where('spr.section_id', $section_id)
                    ->orderBy('month', 'DESC')
                    ->get();
                $progress_data = json_decode($progress_data, true);
                $progress['progress'] = [];
                if (!empty($progress_data)) {
                    $progress['progress'] = $progress_data;
                    $progress['arr_subject'] = $this->get_class_section_subject($session_id, $class_id, $section_id);
                    $progress['progress_note'] = \Config::get('custom.progress_message');
                }
                $status = trans('language.success_status');
                $message = trans('language.success_message');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response = $progress;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* get student exam marks * */

    public function getStudentExamMarks(Request $request)
    {
        $arr_return = [];
        $arr_subject_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $student_id = $request->input('student_id');
            $class_id = $request->input('class_id');
            $exam_id = $request->input('exam_id');
            $section_id = $request->input('section_id');
            $session = db_current_session();
            $session_id = $session['session_id'];


            if (!empty($student_id) && !empty($session_id) && !empty($class_id) && !empty($section_id)) {
                DB::enableQueryLog();
                $arr_marks_data = DB::table('student_mark_details as smd')
                    ->join('student_marks as sm', 'smd.student_mark_id', '=', 'sm.student_mark_id')
                    ->join('exam_roll_numbers as ern', 'smd.student_id', '=', 'ern.student_id')
                    ->join('students as st', 'smd.student_id', '=', 'st.student_id')
                    ->join('create_exams as ce', 'sm.exam_id', '=', 'ce.exam_id')
                    ->join('exam_classes as ec', 'sm.exam_id', '=', 'ec.exam_id')
                    ->select('st.first_name', 'st.middle_name', 'st.last_name', 'ern.prefix', 'ern.roll_number', 'ec.subject_marks_criteria', 'ce.exam_name', 'smd.obtained_marks', 'smd.percentage', 'sm.exam_id', 'sm.result_in', 'sm.publish_status', 'smd.student_id', 'smd.rank')
                    ->where('smd.student_id', $student_id)
                    ->where('sm.session_id', $session_id)
                    ->where('sm.class_id', $class_id)
                    ->where('sm.section_id', $section_id)
                    ->where('ec.session_id', $session_id)
                    ->where('ec.class_id', $class_id)
                    ->where('ec.section_id', $section_id)
                    ->where('ern.class_id', $class_id)
                    ->where('sm.publish_status', 1)
                    ->get();
//	        p($arr_marks_data);
                $arr_marks_data = json_decode(json_encode($arr_marks_data), true);
                $arr_subject = get_class_section_subject($session_id, $class_id, $section_id);
                $arr_exam_type = get_all_exam_type();
                $arr_return['marks'] = [];
                if (!empty($arr_marks_data)) {
                    foreach ($arr_marks_data as $key => $marks_data) {

                        $marks_data['obtained_marks'] = json_decode($marks_data['obtained_marks'], true);
                        $marks_data['subject_marks_criteria'] = json_decode($marks_data['subject_marks_criteria'], true);

                        $arr_exam_marks_data = array(
                            'exam_id' => $marks_data['exam_id'],
                            'exam_name' => $marks_data['exam_name'],
                            'percentage' => $marks_data['percentage'],
                            'student_name' => $marks_data['first_name'] . ' ' . $marks_data['middle_name'] . ' ' . $marks_data['last_name'],
                            'rank' => $marks_data['rank'],
                            'result' => get_result($marks_data['percentage']),
                            'division' => get_marks_division($marks_data['percentage']),
                            'total_obtained' => $marks_data['obtained_marks']['tot_obtained'],
                            'roll_no' => $marks_data['prefix'] . $marks_data['roll_number'],
                        );

                        $arr_subject_data = [];
                        unset($marks_data['obtained_marks']['tot_obtained']);
                        foreach ($marks_data['obtained_marks'] as $key => $subject_marks) {
                            $subject_marks_criteria = $marks_data['subject_marks_criteria'][$key];
                            $subject_max_marks = $subject_marks_criteria['total_max_marks'];
                            unset($subject_marks_criteria['total_max_marks']);
                            $subject_data = array(
                                'subject_id' => check_empty($key, 'int'),
                                'subject_name' => check_empty($arr_subject[$key], 'varchar'),
                                'subject_tot_obtained' => check_empty($subject_marks['sub_tot_obtained'], 'int'),
                                'subject_max_marks' => check_empty($subject_max_marks, 'int'),
                            );
                            $exam_type_data = [];
                            $arr_exam_type_data = [];
                            $subject_min_marks = 0;
                            unset($subject_marks['sub_tot_obtained']);
                            foreach ($subject_marks as $exam_type_key => $exam_type_marks) {
                                $arr_exam_type_data = array(
                                    'exam_type_id' => check_empty($exam_type_key, 'int'),
                                    'exam_type' => check_empty($arr_exam_type[$exam_type_key], 'varchar'),
                                    'exam_type_marks' => $exam_type_marks
                                );

                                $exam_type_data[] = $arr_exam_type_data;
                                $subject_min_marks = $subject_min_marks + $subject_marks_criteria[$exam_type_key]['min_marks'];
                            }
                            $subject_data['subject_min_marks'] = $subject_min_marks;
                            $subject_data['exam_type_data'] = $exam_type_data;
                            $arr_subject_data[] = $subject_data;
                        }
                        $arr_exam_marks_data['arr_subject_marks'] = $arr_subject_data;
                        $arr_return['marks'][] = $arr_exam_marks_data;
                    }
                }


                $status = trans('language.success_status');
                $message = trans('language.success_message');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response = $arr_return;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* get student exam time table * */

    public function getExamTimeTable(Request $request)
    {
        $arr_return = [];
        if ($this->verifyToken($request->input()) == true) {
            $class_id = $request->input('class_id');
            $section_id = $request->input('section_id');
            $session = db_current_session();
            $session_id = $session['session_id'];

            if (!empty($class_id) && !empty($session_id) && !empty($section_id)) {
                $arr_time_table = ExamTimeTableDetail::where('class_id', $class_id)
                    ->where('section_id', $section_id)
                    ->with(['examTimeTable.timeTableExam' => function ($q) use ($session_id) {
                        $q->where('session_id', $session_id);
                        $q->orderBy('created_at', 'DESC');
                    }])
                    ->whereHas('examTimeTable.timeTableExam', function ($q) use ($session_id) {
                        $q->where('session_id', $session_id);
                        $q->orderBy('created_at', 'DESC');
                    })
                    ->with(['examTimeTableSubjects.Subject' => function ($q) use ($class_id, $section_id) {

                    }])
                    ->with(['examTimeTableSubjects' => function ($q) use ($class_id, $section_id) {
                        $q->orderBy('exam_date', 'asc');
                        $q->orderBy('start_time', 'asc');
                    }])
                    ->get();
                $arr_exam_type = get_all_exam_type();
                if (!empty($arr_time_table)) {
                    $arr_time_table = json_decode($arr_time_table, true);
                    foreach ($arr_time_table as $time_table) {
                        $exam_time_table_data = array(
                            'exam_id' => $time_table['exam_time_table']['exam_id'],
                            'class_id' => $time_table['class_id'],
                            'section_id' => $time_table['section_id'],
                            'exam_name' => $time_table['exam_time_table']['time_table_exam']['exam_name'],
                        );

                        $arr_subject_data = [];
                        foreach ($time_table['exam_time_table_subjects'] as $key => $subject) {
                            $subject_data = array(
                                'subject_id' => $subject['subject_id'],
                                'subject_name' => $subject['subject']['subject_name'],
                                'exam_date' => $subject['exam_date'],
                                'start_time' => date('h:i a', strtotime($subject['start_time'])),
                                'end_time' => date('h:i a', strtotime($subject['end_time'])),
                                'exam_type_id' => $subject['exam_type_id'],
                                'exam_type' => $arr_exam_type[$subject['exam_type_id']],
                            );
                            $arr_subject_data[] = $subject_data;
                        }

                        $exam_time_table_data['arr_subject'] = $arr_subject_data;
                        $arr_return[] = $exam_time_table_data;
                    }
                }
                $status = trans('language.success_status');
                $message = trans('language.success_message');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['exam_time_table'] = $arr_return;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* get student attendance * */

    public function studentAttendanceQuery($class_id, $section_id, $session_id, $start_date, $end_date, $student_id)
    {
        $query = Student::where('current_class_id', $class_id)
            ->where('current_section_id', $section_id)
            ->where('student_id', $student_id)
            ->select('student_id', 'current_class_id', 'enrollment_number', 'first_name', 'middle_name', 'last_name')
            ->with(['getClass.classStudentAttendance' => function ($q) use ($session_id, $start_date, $end_date, $section_id, $class_id) {
                $q->select('class_id', 'attendance_date', 'student_id', 'total_present', 'total_absent', 'on_leave');
                if (!empty($session_id)) {
                    $q->where('session_id', $session_id);
                }
                if (!empty($start_date)) {
                    $q->whereBetween('attendance_date', array($start_date, $end_date));
                }
                if (!empty($section_id)) {
                    $q->where('section_id', $section_id);
                }
                if (!empty($class_id)) {
                    $q->where('class_id', $class_id);
                }
            }])
            ->with(['getLeave' => function ($q) use ($session_id, $start_date, $end_date, $class_id, $section_id) {
                $q->select('class_id', 'leave_start_date', 'leave_end_date', 'student_id', 'student_leave_id', 'leave_day');
                if (!empty($session_id)) {
                    $q->where('session_id', $session_id);
                }
                if (!empty($class_id)) {
                    $q->where('class_id', $class_id);
                }
                if (!empty($section_id)) {
                    $q->where('section_id', $section_id);
                }
                if (!empty($start_date)) {
                    $q->whereBetween('leave_start_date', array($start_date, $end_date));
                }
                if (!empty($start_date)) {
                    $q->whereBetween('leave_end_date', array($start_date, $end_date));
                }
            }])
            ->with(['getParent' => function ($q) {
                $q->addSelect('student_parent_id', 'father_first_name', 'father_middle_name', 'father_last_name', 'father_contact_number');
            }])
            ->first();
//                  ->toArray();
        return $query;
    }

    public function getStudentAttendance(Request $request)
    {
        $arr_return = (object)[];
        if ($this->verifyToken($request->input()) == true) {
            $session = db_current_session();
            $session_id = $session['session_id'];
            $class_id = $request->input('class_id');
            $section_id = $request->input('section_id');
            $student_id = $request->input('student_id');
            if (!empty($student_id) && !empty($class_id) && !empty($section_id) && !empty($session_id)) {
                $studentCont = new StudentAttendanceController;
                $start_date = $session['start_date']; // session start date
                $end_date = date('Y-m-d'); // current date
                $month_calendar = $studentCont->monthCalendar($view_type = '', null, $start_date, $end_date, $with_holiday = 'yes');
                $arr_attendance_data = [];
                $arr_leave_data = [];
                $student_detail = [];
                // get attendance data
                $arr_attendance = $this->studentAttendanceQuery($class_id, $section_id, $session_id, $start_date, $end_date, $student_id);
                if (isset($arr_attendance['getClass']['classStudentAttendance'])) {
                    $arr_attendance_symbol = \Config::get('custom.attendance_symbol');
                    foreach ($arr_attendance['getClass']['classStudentAttendance'] as $key => $attendance) {
                        // attendance data
                        $arr_student_absent = json_decode($attendance['student_id'], true);
                        $arr_attendance_data[$attendance['attendance_date']] = array(
                            'attendance_date' => $attendance['attendance_date'],
                            'student_id' => !empty($arr_student_absent) ? array_column($arr_student_absent, 'student_id') : [],
                        );
                    }
                    // leave data
                    foreach ($arr_attendance['getLeave'] as $key => $leave) {
                        $leave_day = $leave['leave_day'];
                        $arr_leave_date[] = $leave['leave_start_date'];
                        if ($leave_day > 1) {
                            for ($i = 1; $i <= $leave_day; $i++) {
                                array_push($arr_leave_date, date('Y-m-d', strtotime('+' . $i . ' day', strtotime($leave['leave_start_date']))));
                            }
                        }
                        $arr_leave_data[$leave['student_id']] = $arr_leave_date;
                    }
                    $student_detail = array(
                        'student_id' => $student_id,
                        'enrollment_number' => $arr_attendance['enrollment_number'],
                        'student_name' => $arr_attendance['first_name'] . ' ' . $arr_attendance['middle_name'] . ' ' . $arr_attendance['last_name']
                    );

                    $present_count = 0;
                    $absence_count = 0;
                    $leave_count = 0;
                    foreach ($month_calendar as $key => $moth_date) {
                        $status = '';
                        // holiday
                        if (!empty($moth_date['holiday'])) {
                            if ($moth_date['val'] == 'Sun') {
                                $status = $arr_attendance_symbol['w'];
                            } else {
                                $status = $arr_attendance_symbol['h'];
                            }
                        } // on leave
                        elseif (isset($arr_leave_data[$student_detail['student_id']]) && in_array($moth_date['date'], $arr_leave_data[$student_detail['student_id']])) {
                            $status = $arr_attendance_symbol['l'];
                            $leave_count++;
                        }
//                            elseif ($moth_date['date'] < $student_detail['admission_date'])
//                            {
//                                $status = '-';
//                            }
                        else {
                            $attendance_info = isset($arr_attendance_data[$moth_date['date']]) ? $arr_attendance_data[$moth_date['date']] : [];
                            if (!empty($attendance_info)) {
                                $arr_student_absent = $attendance_info['student_id'];
                                if (in_array($student_detail['student_id'], $arr_student_absent)) {
                                    $status = $arr_attendance_symbol['a'];
                                    $absence_count++;
                                } else {
                                    $status = $arr_attendance_symbol['p'];
                                    $present_count++;
                                }
                            } else {
                                $status = $arr_attendance_symbol['nar'];
                            }
                        }
                        $date = new DateTime($moth_date['date']);

                        $attendance_data[] = array(
                            'attendance_date' => $date->format('d/m/Y'),
                            'status' => $status,
                        );
                    }

                    $total_working_days = date_diff(date_create($start_date), date_create($end_date))->format('%a') + 1; // including cuurent day
                    $total_holiday = count(get_school_all_holidays($start_date, $end_date, $session_id));
                    $student_detail['total_working_days'] = (int)$total_working_days;
                    $student_detail['total_present'] = $present_count;
                    $student_detail['total_absent'] = $total_working_days - ($present_count + $total_holiday);
                    $student_detail['arr_attendance'] = $attendance_data;
//                    $student_detail['holidays'] = get_school_all_holidays($start_date, $end_date, $session_id);
                    $holidays = get_school_all_holidays($start_date, $end_date, $session_id);
                    $new_holidays = [];
                    foreach ($holidays as $holiday) {
                        array_push($new_holidays, array('date' => date('Y-m-d', strtotime($holiday))));
                    }
                    $student_detail['holidays'] = $new_holidays;
                }
                $arr_return = $student_detail;
                // here status means return status
                $status = trans('language.success_status');
                $message = trans('language.success_message');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }

        $result_response['data'] = $arr_return;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* Student exam and monthly syllabus* */

    public function getStudentSyllabus(Request $request)
    {

        $result_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $class_id = $request->input('class_id');
            $section_id = $request->input('section_id');
            $student_id = $request->input('student_id');

            if (!empty($class_id) && !empty($student_id) && !empty($section_id)) {
                $session = db_current_session();
                $session_id = $session['session_id'];
                $arr_exam_list = $this->get_all_exam(null, $class_id, $session_id, $section_id);
                $arr_exam_id = array_column($arr_exam_list, 'exam_id');
                $arr_month = $this->get_current_session_months_api();
                $arr_month_id = array_column($arr_month, 'month_id');
                $arr_syllabus = DB::table('syllabi as sy')
                    ->join('subjects as s', 's.subject_id', '=', 'sy.subject_id')
                    ->select('sy.*', 's.subject_id', 'sy.class_id', 's.subject_name')
                    ->where('sy.class_id', $class_id)
                    ->where('sy.section_id', $section_id)
                    ->where('sy.session_id', $session_id)
                    ->where(function ($q) use ($arr_exam_id, $arr_month_id) {
                        $q->whereIn('sy.exam_id', $arr_exam_id);
                        $q->orWhereIn('sy.month', $arr_month_id);
                    })
                    ->OrderBy('sy.exam_id')
                    ->OrderBy('sy.month')
                    ->whereNull('sy.deleted_at')
                    ->get();
                $arr_syllabus = json_decode($arr_syllabus, true);
                if (!empty($arr_syllabus)) {
                    $exam_data = [];
                    $monthly_data = [];
//		foreach ($arr_syllabus as $key => $syllabus)
//		{
//		    $syllabus = (array) $syllabus;
//		    if ($syllabus['syllabus_type'] == 1)
//		    {
//		        if (!in_array($syllabus['exam_id'], array_keys($exam_data)))
//		        {
//			$arr_subject = [];
//		        }
//		        $arr_subject[$syllabus['subject_id']] = array(
//			'subject_id' => check_empty($syllabus['subject_id'], 'int'),
//			'subject_name' => check_empty($syllabus['subject_name'], 'varchar'),
//			'syllabus_id' => check_empty($syllabus['syllabus_id'], 'int'),
//			'syllabus_content' => check_empty($syllabus['syllabus_content'], 'varchar'),
//		        );
//		        $exam_data[$syllabus['exam_id']] = array(
//			'exam_id' => check_empty($syllabus['exam_id'], 'int'),
//			'arr_subject' => $arr_subject,
//		        );
//		    } else if ($syllabus['syllabus_type'] == 2)
//		    {
//		        if (!in_array($syllabus['month'], array_keys($monthly_data)))
//		        {
//			$arr_subject_month = [];
//		        }
//		        $arr_subject_month[$syllabus['subject_id']] = array(
//			'subject_id' => check_empty($syllabus['subject_id'], 'int'),
//			'subject_name' => check_empty($syllabus['subject_name'], 'varchar'),
//			'syllabus_id' => check_empty($syllabus['syllabus_id'], 'int'),
//			'syllabus_content' => check_empty($syllabus['syllabus_content'], 'varchar'),
//		        );
//		        $monthly_data[$syllabus['month']] = array(
//			'month' => check_empty($syllabus['month'], 'int'),
//			'arr_subject' => $arr_subject_month,
//		        );
//		    }
//		}

                    $result_data['syllabus'] = $arr_syllabus;
                    $result_data['arr_exam_list'] = $arr_exam_list;
//		$result_data['monthly_syllabus'] = $monthly_data;
                    $result_data['arr_month'] = $arr_month;
                    $status = trans('language.success_status');
                    $message = trans('language.success_message');
                } else {
                    $status = trans('language.success_status');
                    $message = trans('language.failed_message');
                }
            } else {

                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response = $result_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* ======================================================= Parent App End ===================================================================================== */


    /* Teacher App */
    /* ======================================================= Teacher App start ===================================================================================== */

    /* Add student attendance */

    public function addStudentAttendance(Request $request)
    {
        $student_attendance_id = $request->input('student_attendance_id');
        $session = db_current_session();
        $session_id = $session['session_id'];
        $section_id = $request->input('section_id');
        $class_id = $request->input('class_id');
        $teacher_id = $request->input('teacher_id');
        $student_id = $request->input('student_id');
        $attendance_date = $request->input('attendance_date');
        $result_data = [];

        if (!empty($session_id) && !empty($class_id) && !empty($teacher_id) && !empty($student_id) && !empty($attendance_date)) {

            if (!empty($student_attendance_id)) {
                $student_attendance = StudentAttendance::Find($student_attendance_id);
            } else {
                $student_attendance = new StudentAttendance;
            }
            $attendance[0] = array(
                'student_id' => 1,
                'status' => 1,
            );
            $attendance[1] = array(
                'student_id' => 2,
                'status' => 1,
            );

            $attendance = json_encode($attendance);
            $student_attendance->session_id = $session_id;
            $student_attendance->section_id = $section_id;
            $student_attendance->class_id = $class_id;
            $student_attendance->teacher_id = $teacher_id;
            $student_attendance->student_id = $attendance;
            $student_attendance->attendance_date = get_formatted_date($attendance_date, 'database');
            $student_attendance->total_present = 5;
            $student_attendance->total_absent = 2;
            $student_attendance->attendance_status = 1;

            $student_attendance->save();

            if (!empty($student_attendance->class_id)) {
                $result_data = array('class_id' => $student_attendance->class_id);
                $status = trans('language.success_status');
                $message = trans('language.attendance_success_message');
            } else {
                $status = trans('language.success_status');
                $message = trans('language.attendance_failed_message');
            }
        } else {

            $status = trans('language.failed_status');
            $message = trans('language.empty_fields');
        }
        $result_response['data'] = $result_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* Save student Progress* */

    public function saveStudentProgress(Request $request)
    {
        $student_progress_id = $request->input('student_progress_id');
        $student_id = $request->input('student_id');
        $class_id = $request->input('class_id');
        $progress_result = $request->input('progress');
        $session = db_current_session();
        $session_id = $session['session_id'];
        $section_id = $request->input('section_id');
        $month = $request->input('month');
        $type = $request->input('type');
        $subject_id = $request->input('subject_id');

        if (!empty($class_id) && !empty($student_id) && !empty($session_id) && !empty($month) && !empty($progress_result)) {
            DB::beginTransaction();
            try {
                DB::enableQueryLog();
                $query = DB::table('student_progresses')->select('student_progress_id')->where('class_id', $class_id)
                    ->where('session_id', $session_id)->where('student_id', $student_id)->where('month', $month);

                if (!empty($student_progress_id)) {
                    $query->where('student_progress_id', '!=', $student_progress_id);
                }
                $progress_data = $query->first();
                if (empty($progress_data->student_progress_id)) {
                    if (!empty($student_progress_id)) {
                        $progress = StudentProgress::find($student_progress_id);
                    } else {
                        $progress = new StudentProgress;
                    }
                    $progress->class_id = $class_id;
                    $progress->student_id = $student_id;
                    $progress->section_id = $section_id;
                    $progress->session_id = $session_id;
                    $progress->type = $type;
                    $progress->month = $month;
                    $progress->subject_id = $subject_id;
                    $progress->progress = $progress_result;
                    $progress->save();
                    $status = trans('language.success_status');
                    $message = trans('language.progress_save_success');
                } else {
                    $status = trans('language.failed_status');
                    $message = trans('language.progress_already_exist');
                }
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                $status = trans('language.failed_status');
                $message = trans('language.progress_save_failed');
            }
            DB::commit();
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.empty_fields');
        }
        $result_response['data'] = [];
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* Save student diary work */

    public function saveStudentWork(Request $request)
    {
        if ($this->verifyToken($request->input()) == true) {
            $work_diary_id = $request->input('work_diary_id');
            $subject_id = $request->input('subject_id');
            $class_id = $request->input('class_id');
            $session = db_current_session();
            $session_id = $session['session_id'];
            $section_id = $request->input('section_id');
            $work_type = $request->input('work_type');
            $work_start_date = $request->input('work_start_date');
            $work_end_date = $request->input('work_end_date');
            $work = $request->input('work');
            $work_doc = $request->input('work_doc');
            $work_for = $request->input('for');  //1:single_student, 2:all_student
            $student_id = $request->input('student_id');
            if (!empty($class_id) && !empty($section_id) && !empty($subject_id) && !empty($session_id) && !empty($work_for) && !empty($work_start_date) && !empty($work_end_date) && (!empty($work) || !empty($work_doc))) {
                DB::beginTransaction();
                try {
                    if (!empty($work_diary_id)) {
                        $work_diary = WorkDiary::find($work_diary_id);
                    } else {
                        $work_diary = new WorkDiary;
                    }
                    $work_diary->class_id = $class_id;
                    $work_diary->subject_id = $subject_id;
                    $work_diary->section_id = $section_id;
                    $work_diary->session_id = $session_id;
                    $work_diary->work_type = $work_type;
                    $work_diary->work = $work;
                    $work_diary->for = $work_for;
                    $work_diary->student_id = $student_id;
                    $work_diary->work_start_date = get_formatted_date($work_start_date, 'database');
                    $work_diary->work_end_date = get_formatted_date($work_end_date, 'database');

                    if ($request->hasFile('work_doc')) {
                        $file = $request->file('work_doc');
                        $config_upload_path = \Config::get('custom.work_doc');
                        $destinationPath = public_path() . $config_upload_path['upload_path'];
                        $filename = $file->getClientOriginalName();
                        $rand_string = quick_random();
                        $filename = $rand_string . '-' . $filename;
                        $file->move($destinationPath, $filename);
                        $work_diary->work_doc = $filename;
                    }
                    $work_diary->save();
                    $status = trans('language.success_status');
                    $message = trans('language.work_diary_save_success');
                } catch (\Exception $e) {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $status = trans('language.failed_status');
                    $message = trans('language.work_diary_save_failed');
                }
                DB::commit();
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = [];
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* ======================================================= Teacher App End ===================================================================================== */

    /* Common API */
    /* ======================================================= Common API for Parent and Teacher APP ===================================================================================== */

    public function noticeList(Request $request)
    {
        $result_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $request_for = $request->input('request_for');
            $student_id = $request->input('student_id');
            $notice_for = $request->input('notice_for');
            $employee_id = $request->input('employee_id');
            if ($request_for == 'student' || $request_for == 'staff') {
                if (!empty($notice_for) && (!empty($student_id) || !empty($employee_id))) {
                    $class_id = $request->input('class_id');
                    $section_id = $request->input('section_id');

                    $arr_notice = DB::table('notices as n')
                        ->join('sessions as s', 's.session_id', '=', 'n.session_id')
                        ->where('s.session_status', 1)
                        ->where(function ($q) use ($request_for) {
                            if ($request_for == 'student') {
                                $q->whereIn('n.notice_for', [1, 3]);
                            } else {
                                $q->whereIn('n.notice_for', [2, 3]);
                            }
                        })
                        ->orWhere(function ($q) use ($student_id, $employee_id, $request_for) {
                            if ($request_for == 'student') {
                                $q->orWhereIn('n.notice_for', [4, 6]);
                                $q->whereRaw("FIND_IN_SET($student_id,student_id)");
                            } else {
                                $q->orWhereIn('n.notice_for', [5, 6]);
                                $q->whereRaw("FIND_IN_SET($employee_id, employee_id)");
                            }
                        })
                        ->select('n.*')
                        ->orderBy('n.start_date', 'DESC')
                        ->get();
                    if (!empty($arr_notice)) {
                        foreach ($arr_notice as $notice) {
                            $result_data[] = array(
                                'notice_id' => $notice->notice_id,
                                'start_date' => $notice->start_date,
                                'end_date' => $notice->end_date,
                                'notice' => check_empty($notice->notice, 'varchar'),
                                'notice_title' => check_empty($notice->notice_title, 'varchar'),
                                'notice_image' => check_file_exist($notice->notice_image, 'notice', 'yes'),
                                'created_at' => date('Y-m-d H:i:s', strtotime($notice->created_at)),
                            );
                        }
                        $status = trans('language.success_status');
                        $message = trans('language.success_message');
                    } else {
                        $status = trans('language.success_status');
                        $message = trans('language.failed_message');
                    }
                } else {
                    $status = trans('language.failed_status');
                    $message = trans('language.empty_fields');
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.invalid_request');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $result_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* Forgot password */

    public function forgotPassword(Request $request)
    {
        $data_response = (object)[];
        $mobile = $request->input('mobile');
        if (!empty($request->input())) {
            $user_data = DB::table('admin_users as au')
                ->join('student_parents as sp', 'au.admin_user_id', '=', 'sp.admin_user_id')
                ->where('au.mobile_number', $mobile)
                ->select('au.admin_user_id', 'sp.father_email', 'sp.mother_email', 'au.admin_user_id', 'sp.father_first_name', 'sp.mother_first_name', 'sp.father_contact_number', 'sp.mother_contact_number')
                ->first();
            if (!empty($user_data)) {
                // update new password
                $password = quick_random(8);
                $arr_data = array(
                    'password' => Hash::make($password),
                );

                DB::table('admin_users')->where('admin_user_id', $user_data->admin_user_id)->update($arr_data);

                $data['view_blade'] = 'app-forgot-password';
                $data['subject'] = trans('language.forgot_password');
                $data['cc_address'] = '';
                $data['cc_name'] = '';
                $message_text_mail = trans('language.forgot_password_message');
                $data['body'] = array(
                    'message' => $message_text_mail,
                    'user_data' => $user_data->mother_email,
                    'password' => $password,
                );
                if ($user_data->father_email) {
                    $email = $user_data->father_email;
                } else {
                    $email = $user_data->mother_email;
                }
                if (!empty($email)) {
                    Mail::to($email)->send(new SendMailable($data));
                }
                // send sms
                $sms_mobile_number = $user_data->father_contact_number . ',' . $user_data->mother_contact_number;
                $message_text = "Dear Parent, password has been reset successfully. ";
                $message_text .= "New password is : " . $password;
                $response = send_sms($sms_mobile_number, $message_text);
                $status = trans('language.success_status');
                $message = trans('language.password_reset_success');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.invalid_mobile');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.empty_fields');
        }
        $result_response['data'] = $data_response;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* Logout */

    public function logout(Request $request)
    {
        $data_response = (object)[];
        if ($this->verifyToken($request->input()) == true) {
            if (!empty($request->input())) {
                $admin_user_id = $request->input('admin_user_id');
                $token = $request->input('token');
                $token_data = DB::table('api_token')->where('admin_user_id', $admin_user_id)->get();
                foreach ($token_data as $key => $value) {
                    if (Hash::check($token, $value->token)) {
                        DB::table('api_token')->where('token_id', $value->token_id)->delete();

                        $status = trans('language.success_status');
                        $message = trans('language.logout_success_message');
                        break;
                    } else {
                        $status = trans('language.failed_status');
                        $message = trans('language.invalid_token');
                    }
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $data_response;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* Change password */

    public function changePassword(Request $request)
    {
        if ($this->verifyToken($request->input()) == true) {
            $passwordErr = '';
            $admin_user_id = $request->input('admin_user_id');
            $password = $request->input('current_password'); // current password
            $new_password = $request->input('new_password'); // new password
            $confirm_password = $request->input('confirm_password'); // confirm password
            if (!empty($new_password)) {
                if ($new_password == $password) {
                    $passwordErr = "Current Password and new password can not be same";
                }
                if ($new_password != $confirm_password) {
                    $passwordErr = "Confirm password is not matching";
                } elseif (!preg_match("#[0-9]+#", $new_password)) {
                    $passwordErr = "Your Password Must Contain At Least 1 Number!";
                } elseif (!preg_match("#[A-Z]+#", $new_password)) {
                    $passwordErr = "Your Password Must Contain At Least 1 Upper Case Letter!";
                } elseif (!preg_match("#[a-z]+#", $new_password)) {
                    $passwordErr = "Your Password Must Contain At Least 1 Lower Case Letter!";
                } elseif (strlen($new_password) < 8) {
                    $passwordErr = "Your Password Length Must be 8 or more";
                }
                if (empty($passwordErr)) {
                    $user_data = DB::table('admin_users as au')
                        ->where('au.admin_user_id', $admin_user_id)
                        ->first();
                    if (!empty($user_data)) {
                        if (Hash::check($password, $user_data->password)) {
                            $arr_data = array(
                                'password' => Hash::make($new_password)
                            );
                            DB::table('admin_users')->where('admin_user_id', $user_data->admin_user_id)->update($arr_data);
                            $status = trans('language.success_status');
                            $message = trans('language.password_changed');
                        } else {

                            $status = trans('language.failed_status');
                            $message = trans('language.invalid_cur_password');
                        }
                    }
                } else {
                    $status = trans('language.failed_status');
                    $message = $passwordErr;
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }

        $result_response['data'] = (object)[];
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /** Save Device Token */
    public function saveUserDevice($data)
    {
//	if ($this->verifyToken($request->input()) == true)
//	{
//	p($request->input());
        if ($data['admin_user_id'] && $data['device_id']) {
            $arr_device_token = array(
                'admin_user_id' => $data['admin_user_id'],
                'device_id' => $data['device_id'],
                'notification_status' => 'Yes',
            );

            $user_device = DB::table('user_devices')
                ->where('admin_user_id', $arr_device_token['admin_user_id'])
                ->where('device_id', $arr_device_token['device_id'])
                ->first();

            // check id if not exist then inserts
            if (empty($user_device->user_device_id)) {
                DB::table('user_devices')->insert($arr_device_token);
            }

//	        return true;
//	        $status = trans('language.success_status');
//	        $message = trans('language.success_message');
        }
//	    else
//	    {
//	        $status = trans('language.failed_status');
//	        $message = trans('language.empty_fields');
//	    }
//	} else
//	{
//	    $status = trans('language.failed_status');
//	    $message = trans('language.invalid_token');
////	}
//	$result_response['data'] = (object) [];
//	$result_response['status'] = $status;
//	$result_response['message'] = $message;
//	return response()->json($result_response, 200);
    }

    /* Set Push Notification Status* */

    public function saveNotificationStatus()
    {
        if ($this->verifyToken($request->input()) == true) {
            if (!empty($request->input())) {
                $arr_device_token = array(
                    'admin_user_id' => $request->input('admin_user_id'),
                    'device_id' => $request->input('device_id'),
                    'notification_status' => $request->input('notification_status'),
                );

                $user_device = DB::table('user_devices')
                    ->where('admin_user_id', $arr_device_token['admin_user_id'])
                    ->where('device_id', $arr_device_token['device_id'])
                    ->first();

                // check id if not exist then inserts
                if (!empty($user_device->user_device_id)) {
                    DB::table('user_devices')->update(['notification_status' => $arr_device_token['notification_status']])
                        ->where('user_device_id', $user_device->user_device_id);
                }
                $status = trans('language.success_status');
                $message = trans('language.success_message');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = [];
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* get Exam list by class section */

    public function getClassSectionExam(Request $request)
    {
        $result_data = [];
        if ($this->verifyToken($request->input()) == true) {
            if (!empty($request->input())) {
                $class_id = $request->input('class_id');
                $session = db_current_session();
                $session_id = $session['session_id'];
                $section_id = $request->input('section_id');
                $result_data['school'] = get_all_exam(null, $class_id, $session_id, $section_id);
                $status = trans('language.success_status');
                $message = trans('language.success_message');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $result_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* get unvisited notification device* */

    public function getUnvisitedDevice(Request $request)
    {
        $notification['notification_device'] = [];
        if ($this->verifyToken($request->input()) == true) {
            $start_date = $request->input('start_date');
            $end_date = $request->input('end_date');
            $device_data = DB::table('push_notification_devices as pnd')
                ->join('push_notifications as pn', 'pnd.push_notification_id', '=', 'pn.push_notification_id')
                ->select('pnd.device_id', 'pnd.push_notification_device_id', 'pn.notification_data')
                ->where('pnd.visited', 0)
                ->where('pn.success', '>', 0)
                ->get();
            $device_data = json_decode($device_data, true);

            if (!empty($device_data)) {
                $notification['notification_device'] = $device_data;
                $status = trans('language.success_status');
                $message = trans('language.success_message');
            } else {
                $status = trans('language.success_status');
                $message = trans('language.failed_message');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response = $notification;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /* get unvisited notification device* */

    public function updateVisitedStatus(Request $request)
    {
//	$notification = [];
        if ($this->verifyToken($request->input()) == true) {
            $id = $request->input('push_notification_device_id');
            $status = $request->input('status');
            $device = \App\Model\backend\PushNotificationDevice::Find($id);
            $device->visited = $status;
            if ($device->save()) {
                $status = trans('language.success_status');
                $message = trans('language.success_message');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.failed_message');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
//	$result_response = $notification;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /*
     * get event details for parent app
     */
    public function eventsDetails(Request $request)
    {
        $result_data = [];
        if ($this->verifyToken($request->input()) == true) {

            $event_id = $request->input('event_id');
            if (!empty($event_id)) {
                $events = Event::
                where('event_id', $event_id)
                    ->with('getEventGallery')
                    ->orderBy('event_start_date', 'DESC')
                    ->first();

                if (!empty($events)) {
                    $arr_event_gallery = [];
                    foreach ($events['getEventGallery'] as $event_gallery) {
                        $arr_event_gallery[] = array(
                            'event_gallery_id' => $event_gallery->event_gallery_id,
                            'url' => check_file_exist($event_gallery->name, 'event_gallery', 'yes'),
                        );
                    }
                    $result_data = array(
                        'event_id' => $events->event_id,
                        'event_for' => $events->event_for,
                        'created_at' => date('Y-m-d H:i:s', strtotime($events->created_at)),
                        'event_name' => check_empty($events->event_name, 'varchar'),
                        'event_start_date' => check_empty($events->event_start_date, 'varchar'),
//                        'event_end_date' => check_empty($events->event_end_date, 'varchar'),
//                        'event_time' => check_empty($events->event_time, 'varchar'),
                        'event_description' => check_empty($events->event_description, 'varchar'),
                        'event_status' => check_empty($events->event_status, 'int'),
                        'event_gallery' => $arr_event_gallery,
                    );
                    $status = trans('language.success_status');
                    $message = trans('language.success_message');
                } else {
                    $status = trans('language.failed_status');
                    $message = trans('language.failed_message');
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $result_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }


    /*
     * get notice details for parent app
     */
    public function noticeDetails(Request $request)
    {
        $result_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $notice_id = $request->input('notice_id');
            if (!empty($notice_id)) {
                $notice = Notice:: where('notice_id', $notice_id)->first();
                if (!empty($notice)) {
                    $result_data =
                        array(
                            'notice_id' => $notice->notice_id,
                            'start_date' => $notice->start_date,
                            'end_date' => $notice->end_date,
                            'notice' => check_empty($notice->notice, 'varchar'),
                            'notice_title' => check_empty($notice->notice_title, 'varchar'),
                            'notice_image' => check_file_exist($notice->notice_image, 'notice', 'yes'),
                        );
                    $status = trans('language.success_status');
                    $message = trans('language.success_message');
                } else {
                    $status = trans('language.failed_status');
                    $message = trans('language.failed_message');
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $result_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }


    /* get student work  like CW or HW */

    public function workDiaryDetails(Request $request)
    {
        $work_data = [];
        if ($this->verifyToken($request->input()) == true) {

            $work_diary_id = $request->input('work_diary_id');
            $result_data = [];

            if (!empty($work_diary_id)) {
                $work = DB::table('work_diaries as wd')
                    ->join('subjects as s', 's.subject_id', '=', 'wd.subject_id')
                    ->select('wd.*', 's.subject_id', 's.subject_name')
                    ->where('wd.work_diary_id', $work_diary_id)->first();

                if (!empty($work)) {
                    $work_type = \Config::get('custom.arr_work_type');

                    $work = (array)$work;
                    $work_data = array(
                        'work_diary_id' => check_empty($work['work_diary_id'], 'int'),
                        'class_id' => check_empty($work['class_id'], 'int'),
                        'session_id' => check_empty($work['session_id'], 'int'),
                        'section_id' => check_empty($work['section_id'], 'int'),
                        'work_start_date' => get_formatted_date($work['work_start_date'], 'display'),
                        'work_end_date' => get_formatted_date($work['work_end_date'], 'display'),
                        'subject_id' => check_empty($work['subject_id'], 'int'),
                        'subject_name' => check_empty($work['subject_name'], 'varchar'),
                        'work_type' => check_empty($work['work_type'], 'int'),
                        'work_type_name' => check_empty($work_type[$work['work_type']], 'varchar'),
                        'work' => check_empty($work['work'], 'varchar'),
                        'work_doc' => check_file_exist($work['work_doc'], 'work_doc', 'yes'),
                    );

                    $status = trans('language.success_status');
                    $message = trans('language.work_diary_success_message');
                } else {
                    $status = trans('language.success_status');
                    $message = trans('language.work_diary_failed_message');
                }
            } else {

                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $work_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    public function getFeeDetails(Request $request)
    {
        $fee_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $student_id = $request->input('student_id');
            if (!empty($student_id)) {
                if (true) {
                    $fee_data = array(
                        'education_fee' => array(
                            'total_fees' => 17010,
                            'paid_fees' => 8510,
                            'pending_fees' => 8500,
                            'due_fees_arr' => array(
                                array(
                                    'id' => 10,
                                    'due_date' => '15-10-2020',
                                    'fee_type' => 'Tuition Fee',
                                    'fees' => 4250,
                                    'late_charges' => 425,
                                    'late_fee_desc' => 'No Description'
                                )
                            ),
                            'upcoming_fees_arr' => array(
                                array(
                                    'id' => 9,
                                    'due_date' => '10-10-2020',
                                    'fee_type' => 'Tuition Fee',
                                    'fees' => 4250,
                                    'late_fee_desc' => 'No Description'
                                )
                            ),
                            'paid_fees_arr' => array(
                                array(
                                    'id' => 1,
                                    'due_date' => '10-10-2020',
                                    'payment_mode' => 'CASH',
                                    'receipt_no' => '#12530',
                                    'fee_type' => 'App Access Fee',
                                    'fees' => 10,
                                    'late_charges' => 0,
                                    'late_fee_desc' => 'No Description'
                                ),
                                array(
                                    'id' => 2,
                                    'due_date' => '10-10-2020',
                                    'payment_mode' => 'CASH',
                                    'receipt_no' => '#12530',
                                    'fee_type' => 'Tuition Fee',
                                    'fees' => 4250,
                                    'late_charges' => 425,
                                    'late_fee_desc' => 'No Description'
                                ),
                                array(
                                    'id' => 3,
                                    'due_date' => '10-10-2020',
                                    'payment_mode' => 'CASH',
                                    'receipt_no' => '#12530',
                                    'fee_type' => 'Tuition Fee',
                                    'fees' => 4250,
                                    'late_charges' => 425,
                                    'late_fee_desc' => 'No Description'
                                )
                            )
                        ),
                        'transport_fee' => array(
                            'total_fees' => 6000,
                            'paid_fees' => 3000,
                            'pending_fees' => 2000,
                            'due_fees_arr' => array(
                                array(
                                    'id' => 10,
                                    'due_date' => '15-10-2020',
                                    'fee_type' => 'Transport Fee',
                                    'fees' => 1000,
                                    'late_charges' => 10,
                                    'late_fee_desc' => 'No Description'
                                )
                            ),
                            'upcoming_fees_arr' => array(
                                array(
                                    'id' => 9,
                                    'due_date' => '10-10-2020',
                                    'fee_type' => 'Tuition Fee',
                                    'fees' => 1000,
                                    'late_fee_desc' => 'No Description'
                                )
                            ),
                            'paid_fees_arr' => array(
                                array(
                                    'id' => 1,
                                    'due_date' => '10-10-2020',
                                    'payment_mode' => 'CASH',
                                    'receipt_no' => '#12530',
                                    'fee_type' => 'App Access Fee',
                                    'fees' => 1000,
                                    'late_charges' => 0,
                                    'late_fee_desc' => 'No Description'
                                ),
                                array(
                                    'id' => 2,
                                    'due_date' => '10-10-2020',
                                    'payment_mode' => 'CASH',
                                    'receipt_no' => '#12530',
                                    'fee_type' => 'Tuition Fee',
                                    'fees' => 1000,
                                    'late_charges' => 0,
                                    'late_fee_desc' => 'No Description'
                                ),
                                array(
                                    'id' => 3,
                                    'due_date' => '10-10-2020',
                                    'payment_mode' => 'CASH',
                                    'receipt_no' => '#12530',
                                    'fee_type' => 'Tuition Fee',
                                    'fees' => 1000,
                                    'late_charges' => 0,
                                    'late_fee_desc' => 'No Description'
                                )
                            )
                        ),
                    );
                    $status = trans('language.success_status');
                    $message = trans('language.fee_details_success_message');
                } else {
                    $status = trans('language.success_status');
                    $message = trans('language.fee_details_not_found');
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $fee_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    public function payFees(Request $request)
    {
        $fee_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $student_id = $request->input('student_id');
            $total_fee_amount = $request->input('total_fee_amount');
            $pay_fee_ids = $request->input('pay_fee_ids');
            if (!empty($student_id) && !empty($total_fee_amount) && !empty($pay_fee_ids)) {
                $fee_data = array(
                    'receipt_no' => '#12563',
                    'payment_method_id' => 'CASH',
                    'bank_id' => '',
                    'receipt_date' => '10-10-2020',
                    'total_amount' => $total_fee_amount,
                    'discount_amount' => 0,
                    'net_amount' => $total_fee_amount,
                    'cheque_number' => '',
                    'cheque_date' => '',
                    'transaction_id' => '',
                    'transaction_date' => '',
                );
                $status = trans('language.success_status');
                $message = trans('language.fee_details_success_message');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $fee_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    public function getFeeDetailsNew(Request $request)
    {
        $fee_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $student_id = $request->input('student_id');
            if (!empty($student_id)) {
                $arr_payment_mode = get_payment_mode();
                $obj = new StudentFeeReceiptNewController();
                $request->request->add(['arr_student' => [$request->student_id], 'fee_receipt_date' => date('d/m/Y'), 'fee_type_id' => NULL, 'request_type' => 'API']);
                $response = $obj->studentFeeDetail();
                $transport_obj = new StudentTransportFeeReceiptController();
                $transport_fee = $transport_obj->studentTransportFee($request->student_id);
//                p($transport_fee);
                if (true) {
                    $due_fees_arr = [];
                    $upcoming_fees_arr = [];
                    $paid_fees_arr = [];
                    $transport_due_fees_arr = [];
                    $transport_upcoming_fees_arr = [];
                    $transport_paid_fees_arr = [];
                    $fee_details = $response['fee'][0];
                    foreach ($fee_details['student_fee'] as $key => $fees) {
                        if ($key == 'pending') {
                            foreach ($fees as $due_fee) {
                                $fee_details = $due_fee['arr_fee_detail'];
                                foreach ($fee_details as $fee_detail) {
                                    if ($fee_detail['installment_fine_amount'] > 0) {
                                        $date1 = date_create(date('Y-m-d', strtotime($fee_detail['fine_date'])));
                                        $date2 = date_create(date('Y-m-d'));
                                        $diff = date_diff($date1, $date2);
                                        if ($diff->format('%y') >= 1) {
                                            $desc = $diff->format('%y Year %m Month %d Days Over from Due Date');
                                        } else {
                                            $desc = $diff->format('%m Month %d Days Over from Due Date');
                                        }
                                        array_push($due_fees_arr, array(
                                            'fee_id' => $fee_detail['fee_id'],
                                            'fee_detail_id' => $fee_detail['fee_detail_id'],
                                            'due_date' => date('d-m-Y', strtotime($fee_detail['fine_date'])),
                                            'fee_type' => ucfirst($due_fee['fee_type']) . " (" . ucfirst($fee_detail['fee_sub_circular_name']) . ")",
                                            'fees' => $fee_detail['final_pending_amount'],
                                            'late_charges' => $fee_detail['installment_fine_amount'],
                                            'late_fee_desc' => $desc
                                        ));
                                    } else {
                                        $desc = "";
                                        $fine = $due_fee['fine_amount'];
                                        if ($fine > 0) {
                                            if ($due_fee['fine_type'] == 1) {
                                                $fine .= " %";
                                            } else {
                                                $fine .= " Rupees Per Day";
                                            }
                                            $desc = "Late fees Will Charge @$fine After Due Date";
                                        }
                                        array_push($upcoming_fees_arr, array(
                                            'fee_id' => $fee_detail['fee_id'],
                                            'fee_detail_id' => $fee_detail['fee_detail_id'],
                                            'due_date' => date('d-m-Y', strtotime($fee_detail['fine_date'])),
                                            'fee_type' => ucfirst($due_fee['fee_type']) . " (" . ucfirst($fee_detail['fee_sub_circular_name']) . ")",
                                            'fees' => $fee_detail['final_pending_amount'],
                                            'late_fee_desc' => $desc
                                        ));
                                    }
                                }
                            }
                        } elseif ($key == 'paid') {
                            foreach ($fees as $paid_fee) {
                                $fee_details = $paid_fee['fee_receipt_detail'];
                                foreach ($fee_details as $fee_detail) {
                                    array_push($paid_fees_arr, array(
                                        'id' => $fee_detail['fee_id'],
                                        'payment_mode' => $arr_payment_mode[$paid_fee['payment_mode_id']],
                                        'receipt_no' => $paid_fee['receipt_number'], //$paid_fee['student_fee_receipt_id'],
                                        'fee_type' => ucfirst($fee_detail['fee_type']) . " (" . ucfirst($fee_detail['fee_circular_name']) . ")",
                                        'fees' => $fee_detail['installment_paid_amount'],
                                        'late_charges' => $fee_detail['paid_fine_amount'],
                                        'late_fee_desc' => 'No Description',
                                        'paid_date' => date('d-m-Y', strtotime($paid_fee['receipt_date'])),
                                    ));
                                }
                            }
                        }
                    }
                    foreach ($transport_fee as $key => $fees) {
                        if ($key == 'paid') {
                            foreach ($fees as $paid_fee) {
                                if ($paid_fee['fee_status'] == 1) {
                                    foreach ($paid_fee['paid_fee_detail'] as $paid_fee_detail) {
                                        array_push($transport_paid_fees_arr, array(
                                            'id' => $paid_fee['transport_fee_id'],
                                            'due_date' => $paid_fee['receipt_date'],
                                            'payment_mode' => $arr_payment_mode[$paid_fee['payment_mode_id']],
                                            'receipt_no' => $paid_fee['receipt_number'],
                                            'fee_type' => $paid_fee_detail['month'],
                                            'fees' => $paid_fee_detail['paid_fee_amount'],
                                            'late_charges' => 0,
                                            'late_fee_desc' => 'No Description'
                                        ));
                                    }
                                }
                            }
                        }
                        if ($key == 'pending') {
                            foreach ($fees as $pending_fee) {
                                if (date('F') == $pending_fee['month'] || date('F', strtotime('+1 months')) == $pending_fee['month']) {
                                    array_push($transport_upcoming_fees_arr, array(
                                        'id' => $pending_fee['month_id'],
                                        'due_date' => 'NA',
                                        'fee_type' => $pending_fee['month'],
                                        'fees' => $pending_fee['pending_amount'],
                                        'late_fee_desc' => 'No Description'
                                    ));
                                } else {
                                    array_push($transport_due_fees_arr, array(
                                        'id' => $pending_fee['month_id'],
                                        'due_date' => 'NA',
                                        'fee_type' => $pending_fee['month'],
                                        'fees' => $pending_fee['pending_amount'],
                                        'late_charges' => "NA",
                                        'late_fee_desc' => 'No Description'
                                    ));
                                }
                            }
                        }
                    }
                    $fee_data = array(
                        'education_fee' => array(
                            'total_fees' => $response['fee_detail']['total_fee'],
                            'paid_fees' => $response['fee_detail']['paid_fee'],
                            'pending_fees' => $response['fee_detail']['unpaid_fee'] + $response['fee_detail']['late_fee'],
                            'due_fees_arr' => $due_fees_arr,
                            'upcoming_fees_arr' => $upcoming_fees_arr,
                            'paid_fees_arr' => $paid_fees_arr
                        ),
                        'transport_fee' => array(
                            'total_fees' => $transport_fee['fee_details']['total_fee'],
                            'paid_fees' => $transport_fee['fee_details']['paid_fee'],
                            'pending_fees' => $transport_fee['fee_details']['payable_fee'],
                            'due_fees_arr' => $transport_due_fees_arr,
//                                array(
//                                array(
//                                    'id' => 10,
//                                    'due_date' => '15-10-2020',
//                                    'fee_type' => 'Transport Fee',
//                                    'fees' => 1000,
//                                    'late_charges' => 10,
//                                    'late_fee_desc' => 'No Description'
//                                )
//                            ),
                            'upcoming_fees_arr' => $transport_upcoming_fees_arr,
//                                array(
//                                array(
//                                    'id' => 9,
//                                    'due_date' => '10-10-2020',
//                                    'fee_type' => 'Tuition Fee',
//                                    'fees' => 1000,
//                                    'late_fee_desc' => 'No Description'
//                                )
//                            ),
                            'paid_fees_arr' => $transport_paid_fees_arr,
//                                array(
//                                array(
//                                    'id' => 1,
//                                    'due_date' => '10-10-2020',
//                                    'payment_mode' => 'CASH',
//                                    'receipt_no' => '#12530',
//                                    'fee_type' => 'App Access Fee',
//                                    'fees' => 1000,
//                                    'late_charges' => 0,
//                                    'late_fee_desc' => 'No Description'
//                                ),
//                                array(
//                                    'id' => 2,
//                                    'due_date' => '10-10-2020',
//                                    'payment_mode' => 'CASH',
//                                    'receipt_no' => '#12530',
//                                    'fee_type' => 'Tuition Fee',
//                                    'fees' => 1000,
//                                    'late_charges' => 0,
//                                    'late_fee_desc' => 'No Description'
//                                ),
//                                array(
//                                    'id' => 3,
//                                    'due_date' => '10-10-2020',
//                                    'payment_mode' => 'CASH',
//                                    'receipt_no' => '#12530',
//                                    'fee_type' => 'Tuition Fee',
//                                    'fees' => 1000,
//                                    'late_charges' => 0,
//                                    'late_fee_desc' => 'No Description'
//                                )
//                            )
                        ),
                    );
                    $status = trans('language.success_status');
                    $message = trans('language.fee_details_success_message');
                } else {
                    $status = trans('language.success_status');
                    $message = trans('language.fee_details_not_found');
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $fee_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    public function payFeesNew(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'student_id' => 'required|exists:students,student_id',
            'total_fee_amount' => 'required',
            'fee_detail_ids' => 'required',
            'payment_method_id' => 'required',
            'order_id' => 'required',
            'payment_receipt' => 'required|json'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $result_response['data'] = [];
            $result_response['status'] = trans('language.failed_status');
            $result_response['message'] = $errors->first();
            return response()->json($result_response, 200);
        }
        DB::beginTransaction();
        try {
            $fee_data = [];
            if ($this->verifyToken($request->input()) == true) {
                $pending_fee_data = [];
                $obj = new StudentFeeReceiptNewController();
                $request->request->add(['arr_student' => [$request->student_id], 'fee_receipt_date' => date('d/m/Y'), 'fee_type_id' => NULL, 'request_type' => 'API']);
                $response = $obj->studentFeeDetail();
                $fee_details = isset($response['fee'][0]) ? $response['fee'][0] : array();
                if (!empty($fee_details) && isset($fee_details['student_fee']) && !empty($fee_details['student_fee'])) {
                    foreach ($fee_details['student_fee'] as $key => $fees) {
                        if ($key == 'pending') {
                            array_push($pending_fee_data, $fees);
                        }
                    }
                }
                $student_id = $request->input('student_id');
                $total_fee_amount = $request->input('total_fee_amount');
                $pay_fee_ids = $request->input('fee_detail_ids');
                $payment_method_id = $request->input('payment_method_id');
                $payment_gateway = new PaymentGatewayController;
                $result = $payment_gateway->checkPaymentStatus($request->order_id, $request->payment_receipt, 1);
                if ($result) {
                    $order_transaction = Transaction::where('order_id', $request->order_id)->first();
                    $transaction_id = $order_transaction->order_id;
                    $transaction_date = $order_transaction->created_at->format('Y-m-d');
                    if ($payment_method_id == 3 || $payment_method_id == 4) {
                        if (empty($transaction_id) && empty($transaction_date)) {
                            $result_response['data'] = [];
                            $result_response['status'] = trans('language.failed_status');
                            $result_response['message'] = trans('language.transaction_id_and_date_required');
                            return response()->json($result_response, 200);
                        }
                    } else {
                        $result_response['data'] = [];
                        $result_response['status'] = trans('language.failed_status');
                        $result_response['message'] = trans('language.invalid_payment_method');
                        return response()->json($result_response, 200);
                    }
                    $student = Student::find($student_id);
                    $student_fee_receipt = new StudentFeeReceipt;
                    $student_fee_receipt->student_id = $student_id;
                    $student_fee_receipt->class_id = $student->current_class_id;
                    $student_fee_receipt->session_id = $student->current_session_id;
                    $student_fee_receipt->receipt_number = StudentFeeReceipt::getNewReceiptId();
                    $student_fee_receipt->receipt_date = get_formatted_date(date('d/m/Y'), 'database');
                    $student_fee_receipt->is_receipt_print = 0;
                    $student_fee_receipt->is_sms_send = 0;
                    $student_fee_receipt->payment_mode_id = $payment_method_id;
                    $student_fee_receipt->transaction_date = $transaction_date;
                    $student_fee_receipt->transaction_id = $transaction_id;

                    $fine_amount = 0;
                    if ($request->input('total_fine_amount') > 0) {
                        $fine_amount = $request->input('total_fine_amount');
                        $student_fee_receipt->fine_amount = $fine_amount;
                    }
                    $total_amount = 0;
                    if ($request->input('total_fee_amount') > 0) {
                        $total_amount = $request->input('total_fee_amount');
                    }
                    $student_fee_receipt->total_amount = $total_amount;
                    $student_fee_receipt->net_amount = ($total_amount + $fine_amount);
                    $student_fee_receipt->save();
                    $paid_fee_details = explode(',', $request->input('fee_detail_ids'));
                    $total_fee_segment = count($paid_fee_details);
                    $pay_fee_segment = 0;
                    foreach ($paid_fee_details as $paid_fee_detail) {
                        foreach ($pending_fee_data as $pending_fee_datum) {
                            foreach ($pending_fee_datum as $fee_det) {
                                foreach ($fee_det['arr_fee_detail'] as $item) {
                                    if (isset($item['fee_detail_id']) && $item['fee_detail_id'] == $paid_fee_detail) {
                                        $fee_details = FeeDetail::find($paid_fee_detail);
                                        $fee = Fee::find($fee_details->fee_id);
                                        $student_fee_receipt_detail = new StudentFeeReceiptDetail;
                                        $student_fee_receipt_detail->student_fee_receipt_id = $student_fee_receipt->student_fee_receipt_id;
                                        $student_fee_receipt_detail->fee_id = $fee_details->fee_id;
                                        $student_fee_receipt_detail->fee_detail_id = $fee_details->fee_detail_id;
                                        $student_fee_receipt_detail->installment_paid_amount = $item['final_pending_amount'];
                                        $student_fee_receipt_detail->installment_amount = $item['final_pending_amount'];
                                        $student_fee_receipt_detail->fee_circular_name = $fee_details->fee_sub_circular;
                                        $student_fee_receipt_detail->fee_type_id = $fee->fee_type_id;
                                        $student_fee_receipt_detail->fee_circular_id = $fee->fee_circular_id;
                                        $student_fee_receipt_detail->installment_payment_status = 1;
                                        $student_fee_receipt_detail->fine_amount = $item['installment_fine_amount'];
                                        $student_fee_receipt_detail->save();
                                        $pay_fee_segment++;
                                        if ($fee_det['fee_type_id'] == 2) {
                                            $student_obj = Student::find($student_id);
                                            $student_obj->previous_due_fee = $student_obj->previous_due_fee - $item['final_pending_amount'];
                                            $student_obj->Save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ($total_fee_segment != $pay_fee_segment) {
                        throw new \Exception('Paid fee not found in given total fee.');
                    }
                    $fee_data = array(
                        'receipt_no' => '#' . $student_fee_receipt->student_fee_receipt_id,
                        'payment_method_id' => 'ONLINE',
                        'bank_id' => '',
                        'receipt_date' => get_formatted_date($student_fee_receipt->receipt_date, 'display'),
                        'total_amount' => $student_fee_receipt->total_amount,
                        'discount_amount' => $student_fee_receipt->discount_amount,
                        'net_amount' => $student_fee_receipt->net_amount,
                        'cheque_number' => $student_fee_receipt->cheque_number,
                        'cheque_date' => get_formatted_date($student_fee_receipt->cheque_date, 'display'),
                        'transaction_id' => $student_fee_receipt->transaction_id,
                        'transaction_date' => get_formatted_date($student_fee_receipt->transaction_date, 'display'),
                    );
                    $status = trans('language.success_status');
                    $message = trans('language.fee_details_success_message');
                } else {
                    $status = trans('language.failed_status');
                    $message = trans('language.order_payment_failed');
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.invalid_token');
            }
        } catch (\Exception $e) {
            DB::rollback();
            $result_response['data'] = [];
            $result_response['status'] = trans('language.failed_status');
            $result_response['message'] = $e->getMessage();
            return response()->json($result_response, 200);
        }
        DB::commit();
        $result_response['data'] = $fee_data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    public function getStudentDetails(Request $request)
    {
        $parent_student_info = (object)[];
        if ($this->verifyToken($request->input()) == true) {
            if (isset($request->admin_user_id) && $request->admin_user_id != '')
                $parent_student_info = $this->getStudentParentInformation($request->admin_user_id);
            $status = trans('language.success_status');
            $message = trans('language.success_message');
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $parent_student_info;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /*
     * Eclass Room Apis
     */
    public function subjectList(Request $request)
    {
        $subjects = [];
        if ($this->verifyToken($request->input()) == true) {
            $student_id = $request->input('student_id');
            $class_id = $request->input('class_id');
            $section_id = $request->input('section_id');
            if (!empty($student_id) && !empty($class_id) && !empty($section_id)) {
                $student = Student::find($student_id);
                $subjects = $this->get_class_section_subject($student->current_session_id, $class_id, $section_id);
                $status = trans('language.success_status');
                $message = trans('language.subject_list');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $subjects;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    public function chapterList(Request $request)
    {
        $chapters = [];
        if ($this->verifyToken($request->input()) == true) {
            $student_id = $request->input('student_id');
            $class_id = $request->input('class_id');
            $section_id = $request->input('section_id');
            $subject_id = $request->input('subject_id');
            if (!empty($student_id) && !empty($class_id) && !empty($section_id) && !empty($subject_id)) {
                $student = Student::find($student_id);
                $chapters_data = SubjectChapter::where([['session_id', '=', $student->current_session_id], ['subject_chapter_id', '=', $subject_id], ['class_id', '=', $class_id], ['section_id', '=', $section_id]])->get();
                foreach ($chapters_data as $chapter) {
                    array_push($chapters, array(
                            'id' => $chapter->subject_chapter_id,
                            'name' => $chapter->name,
                        )
                    );
                }
                $status = trans('language.success_status');
                $message = trans('language.subject_list');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $chapters;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    public function topicList(Request $request)
    {
        $topics = [];
        if ($this->verifyToken($request->input()) == true) {
            $student_id = $request->input('student_id');
            $chapter_id = $request->input('chapter_id');
            if (!empty($student_id) && !empty($chapter_id)) {
                $student = Student::find($student_id);
                $topics_data = ChapterTopic::where([['subject_chapter_id', '=', $chapter_id]])->get();
                foreach ($topics_data as $topic) {
                    array_push($topics, array(
                            'id' => $topic->chapter_topic_id,
                            'name' => $topic->name,
                        )
                    );
                }
                $status = trans('language.success_status');
                $message = trans('language.subject_topic_list');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $topics;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    public function getEBook(Request $request)
    {
        $e_class_subjects = [];
        if ($this->verifyToken($request->input()) == true) {
            $student_id = $request->input('student_id');
            $class_id = $request->input('class_id');
            $section_id = $request->input('section_id');
            if (!empty($student_id) && !empty($class_id) && !empty($section_id)) {
                $student = Student::find($student_id);
                $subjects = $this->get_class_section_subject($student->current_session_id, $class_id, $section_id);
                foreach ($subjects as $subject) {
                    $chapters = [];
                    $chapters_data = SubjectChapter::where([['subject_id', '=', $subject['subject_id']], ['session_id', '=', $student->current_session_id], ['class_id', '=', $class_id], ['section_id', '=', $section_id]])->get();
                    if (count($chapters_data) > 0) {
                        foreach ($chapters_data as $chapter) {
                            $topics = [];
                            $subject_topics = ChapterTopic::where('subject_chapter_id', $chapter->subject_chapter_id)->get();
                            if (count($subject_topics) > 0) {
                                foreach ($subject_topics as $subject_topic) {
                                    array_push($topics, array(
                                        'id' => $subject_topic->chapter_topic_id,
                                        'name' => $subject_topic->name,
                                    ));
                                }
                            }
                            array_push($chapters, array(
                                    'id' => $chapter->subject_chapter_id,
                                    'name' => $chapter->name,
                                    'topics' => $topics
                                )
                            );
                        }
                    }
                    array_push($e_class_subjects, array(
                            'id' => $subject['subject_id'],
                            'subject' => $subject['subject_name'],
                            'chapters' => $chapters
                        )
                    );
                }
                $status = trans('language.success_status');
                $message = trans('language.eclass_room_list');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $e_class_subjects;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    public function getEBookDetails(Request $request)
    {
        $topic_details = [];
        if ($this->verifyToken($request->input()) == true) {
            $topic_id = $request->input('topic_id');
            if (!empty($topic_id)) {
                $topic = ChapterTopic::find($topic_id);
                if (!empty($topic)) {
                    $topic_details = array(
                        'id' => $topic->chapter_topic_id,
                        'name' => $topic->name,
                        'video_flag' => !empty($topic->video_link) ? true : false,
                        'reading_flag' => !empty($topic->reading_text) || !empty($topic->image_file) ? true : false,
                        'video_details' => array(
//                            'id' => 1,
                            'title' => 'Topic Video',
                            'description' => 'Please check topic video for detail description of topic.',
                            'url' => $topic->video_link
//                            'url' => url('/') . '/public/uploads/song.mp4'
                        ),
                        'reading_details' => array(
//                            'id' => 2,
                            'download_permission' => ($topic->download_permission == 1),
                            'file' => check_file_exist($topic->image_file, 'eclass_room', 'yes', false),
//                            'file' => url('/') . '/public/uploads/paper.pdf',
//                            'image' => url('/') . '/public/uploads/paper.jpg',
                            'title' => 'Topic Contents',
                            'desc' => $topic->reading_text
                        ),
                    );
                    $status = trans('language.success_status');
                    $message = trans('language.topic_details');
                } else {
                    $status = trans('language.failed_status');
                    $message = trans('language.topic_not_found');
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.empty_fields');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $topic_details;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }


    /*
     * Subscritpion APIS
     */
    public function storeSubscriptionDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'student_parent_id' => 'required|exists:student_parents,student_parent_id',
            'amount' => 'required|integer',
            'order_id' => 'required',
            'payment_receipt' => 'required|json'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $result_response['data'] = [];
            $result_response['status'] = trans('language.failed_status');
            $result_response['message'] = $errors->first();
            return response()->json($result_response, 200);
        }
        if ($this->verifyToken($request->input()) == true) {
            $payment_gateway = new PaymentGatewayController;
            $result = $payment_gateway->checkPaymentStatus($request->order_id, $request->payment_receipt, 2);
            if ($result) {
                $start_date = date('Y-m-d  H:i:s');
                $current_session = (object)get_current_session();
                $end_date = $current_session->end_date;
                $subscription = new SubscriptionDetail;
                $subscription->student_parent_id = $request->student_parent_id;
                $subscription->order_id = $request->order_id;
                $subscription->payment_status = $request->payment_status;
                $subscription->amount = $request->amount;
                $subscription->payment_date = date('Y-m-d  H:i:s');
                $subscription->start_date = $start_date;
                $subscription->end_date = $end_date;
                $subscription->save();
                $status = trans('language.success_status');
                $message = trans('language.subscription_details_saved_successfully');
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.order_payment_failed');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = [];
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    /*
     * Create Payment order api
     */

    public function orderCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'student_parent_id' => 'required|exists:student_parents,student_parent_id',
            'amount' => 'required|integer',
            'fee_type' => 'required|in:1,2',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $result_response['data'] = [];
            $result_response['status'] = trans('language.failed_status');
            $result_response['message'] = $errors->first();
            return response()->json($result_response, 200);
        }
        $data = [];
        if ($this->verifyToken($request->input()) == true) {
            $payment_gateway = new PaymentGatewayController;
            $receipt = "Receipt_" . time();
            if ($request->fee_type == 1) {
                $school = DB::table("schools")->first();
                $appid = $school->payment_gateway_app_id;
                $secretkey = $school->payment_gateway_secret_key;
                //          Shishu Vatika School Razorpay account details
//                $appid = 'rzp_live_KeRBRVuOk7dKjL';
//                $secretkey = 'kakLedcG8niGGvHtvIPkTUZE';
            } else {
                //          Abhishek Solanki Razorpay account details
                $appid = 'rzp_live_xZM69cIh0K6ZfD';
                $secretkey = 's4VPfUZMR9OV4nHcxliBUtzz';
                //          $appid = 'rzp_test_vutZgyRBM4ZugJ';
                //          $secretkey = 'KM9lqvM9B7TTqTG20rKUSY9O';
            }
            $order = $payment_gateway->orderCreate($receipt, $request->amount, $request->fee_type, $appid, $secretkey);
            if (!empty($order) && isset($order->id) && $order->id != '') {
                $transaction = new Transaction;
                $transaction->student_parent_id = $request->student_parent_id;
                $transaction->order_id = $order->id;
                $transaction->receipt_id = $receipt;
                $transaction->amount = $request->amount;
                $transaction->save();
                $status = trans('language.success_status');
                $message = trans('language.order_created');
                $data = array('order_id' => $transaction->order_id, 'receipt_id' => $transaction->receipt_id, 'rpay_id' => $appid);
            } else {
                $data = $order;
                $status = trans('language.failed_to_create_order');
                $message = trans('language.order_created');
            }
        } else {
            $status = trans('language.failed_status');
            $message = trans('language.invalid_token');
        }
        $result_response['data'] = $data;
        $result_response['status'] = $status;
        $result_response['message'] = $message;
        return response()->json($result_response, 200);
    }

    public function getEClassDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'student_id' => 'required||exists:students,student_id',
            'class_id' => 'required|exists:classes,class_id',
            'section_id' => 'required|exists:sections,section_id',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $result_response['data'] = [];
            $result_response['status'] = trans('language.failed_status');
            $result_response['message'] = $errors->first();
            return response()->json($result_response, 200);
        }
        $data = [];
        $subject_data = [];
        if ($this->verifyToken($request->input()) == true) {
            $student = Student::find($request->student_id);
            $arr_subject_data = $this->get_class_section_subject($student->current_session_id, $request->class_id, $request->section_id);
            if (!empty($arr_subject_data)) {
                foreach ($arr_subject_data as &$subject) {
                    $eclass_room = EClassRoom::where([['session_id', '=', $student->current_session_id], ['class_id', '=', $request->class_id], ['section_id', '=', $request->section_id], ['subject_id', '=', $subject['subject_id']]])->first();
                    if (!empty($eclass_room)) {
                        if ($eclass_room['teacher_name'] != "" && $eclass_room['meeting_link'] != ""
                            && $eclass_room['meeting_start_time'] != "" && $eclass_room['meeting_end_time'] != ""
                            && $eclass_room['meeting_day'] != "") {
                            $subject['e_class_room_id'] = $eclass_room['e_class_room_id'];
                            $subject['session_id'] = $eclass_room['session_id'];
                            $subject['class_id'] = $eclass_room['class_id'];
                            $subject['section_id'] = $eclass_room['section_id'];
                            $subject['teacher_name'] = $eclass_room['teacher_name'];
                            $subject['meeting_link'] = $eclass_room['meeting_link'];
                            $subject['meeting_start_time'] = $eclass_room['meeting_start_time'];
                            $subject['meeting_end_time'] = $eclass_room['meeting_end_time'];
                            $subject['meeting_day'] = explode(",", $eclass_room['meeting_day']);
                            array_push($subject_data, $subject);
                        }
                    }
                    // $data['arr_subject_data'] = $arr_subject_data;
                    $data['arr_subject_data'] = $subject_data;
                    $data['days'] = Config::get('custom.days');
                }
                if (!empty($data)) {
                    $status = trans('language.success_status');
                    $message = trans('language.e_class_room') . ' ' . trans('language.details');
                } else {
                    $status = trans('language.failed_status');
                    $message = trans('language.e_class_room') . ' ' . trans('language.details') . ' ' . trans('language.failed_message');
                }
            } else {
                $status = trans('language.failed_status');
                $message = trans('language.invalid_token');
            }
            $result_response['data'] = $data;
            $result_response['status'] = $status;
            $result_response['message'] = $message;
            return response()->json($result_response, 200);
        }
    }

    public function changePasswordForAdmin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|exists:admin_users,mobile_number',
            'password' => 'required',
            'code' => [
                Rule::in(["8824447888"]),
            ]
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $result_response['data'] = [];
            $result_response['status'] = trans('language.failed_status');
            $result_response['message'] = $errors->first();
            return response()->json($result_response, 200);
        }
        try {
            $arr_data = array(
                'password' => Hash::make($request->password)
            );
            DB::table('admin_users')->where('mobile_number', $request->phone)->update($arr_data);
            $result_response['data'] = [];
            $result_response['status'] = trans('language.success_status');
            $result_response['message'] = "Password changed successfully";
            return response()->json($result_response, 200);
        } catch (\Exception $e) {
            $result_response['data'] = [];
            $result_response['status'] = trans('language.failed_status');
            $result_response['message'] = $e->getMessage();
            return response()->json($result_response, 200);
        }
    }
}