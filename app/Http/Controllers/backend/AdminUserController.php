<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\AdminUser;
use App\Model\backend\Student;
use App\Model\backend\StudentFeeReceipt;
use App\Model\backend\StudentParent;
use App\Model\backend\SubscriptionDetail;
use Illuminate\Validation\Rule;
use Validator;
use Datatables;

use Illuminate\Http\Request;
use App\Model\backend\Complaint;
use App\Model\backend\UserRole;
use App\Http\Controllers\Controller;
//    use Illuminate\Support\Facades\Input;
//    use Illuminate\Support\Facades\Lang;
//    use Illuminate\Support\Facades\Log;
//     use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View as View;
use Hash;

class AdminUserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//            DB::enableQueryLog();
        $this->EmployeeController = new EmployeeController();
        $this->StudentController = new StudentController();
        $this->StudentFeeReceipt = new StudentFeeReceiptController();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

            // return view('home');
        return view('backend.send-mail.admission-mail');

            // return view('backend.test');
    }

    public function upcomingModule()
    {

            // return view('home');
        return view('backend.upcoming-module');

            // return view('backend.test');
    }

    public function dashboard()
    {
//        $srs = StudentFeeReceipt::get();
//        $no = 869;
//        foreach($srs as $sr){
//            $sr->receipt_number = $no;
//            $sr->save();
//            $no++;
//        }
//        p(Hash::make('abhi@school98!'));
//        $2y$10$kdBWI3elkUUtK6va4JPbgupZWeVWSn6lQ8Sd6x8PWJuCDDgDAWgzq
        $total_students = [];
        $total_employees = [];
        $arr_complaint_pending = [];
        $arr_complaint = [];
        $session = get_current_session();
        if (!empty($session)) {
            $attendance_date = $session['attendance_date'];
            $session_id = $session['session_id'];
            $table = get_student_table($session_id, 'table');
            $student = DB::table($table)->select(DB::raw('COUNT(*) AS total'), DB::raw('SUM(CASE WHEN gender = 1 THEN 1 ELSE 0 END )AS boy'), DB::raw('SUM(CASE WHEN gender = 0 THEN 1 ELSE 0 END )AS girl'))->where('current_session_id', $session_id)->where('student_status', 1)->first();
            $attendance = DB::table('student_attendances as sa')->select(DB::raw('SUM(total_present) AS present'), DB::raw('SUM(total_absent) AS absent'))->where('session_id', $session_id)->where('attendance_date', $attendance_date)->first();
            $total_students = array(
                'total_students' => $student->total,
                'total_boys' => $student->boy,
                'total_girls' => $student->girl,
                'present_students' => $attendance->present,
                'absent_students' => $attendance->absent,
            );

            // Sum of Teacher in This teacher id is 3 (User_type_id)
            $teacher = DB::table('employees as e')->join('admin_users as ur', 'e.admin_user_id', '=', 'ur.admin_user_id')->select(DB::raw('COUNT(*) AS total'), DB::raw('SUM(CASE WHEN gender = 1 THEN 1 ELSE 0 END )AS t_male'), DB::raw('SUM(CASE WHEN gender = 0 THEN 1 ELSE 0 END )AS t_female'))->where('ur.user_role_id', 3)->first();
            // Sum of Staff in This Staff id is 4 (User_type_id)
            $employee = DB::table('employees as e')->join('admin_users as ur', 'e.admin_user_id', '=', 'ur.admin_user_id')->select(DB::raw('COUNT(*) AS total'), DB::raw('SUM(CASE WHEN gender = 1 THEN 1 ELSE 0 END )AS male'), DB::raw('SUM(CASE WHEN gender = 0 THEN 1 ELSE 0 END )AS female'))->where('ur.user_role_id', 4)->first();

            $total_employees = array(
                'total_employees' => $teacher->total + $employee->total,
                'total_teachers' => $teacher->total,
                'total_teacher_male' => $teacher->t_male,
                'total_teacher_female' => $teacher->t_female,
                'total_present_teachers' => 0,
                'total_absent_teachers' => 0,
                'total_non_teaching_statff' => $employee->total,
                'total_non_teaching_female' => $employee->female,
                'total_non_teaching_male' => $employee->male,
                'total_present_non_teaching_staff' => 0,
                'total_absent_non_teaching_staff' => 0,
            );

            $arr_complaint_pending = Complaint::orderBy('created_at', 'DESC')->where('status', 1)->where('session_id', $session_id)
                    //->whereHas('complaintDetail', function ($query) use ($admin_user_id)
                    //{
                    //    $query->where('admin_user_id', $admin_user_id);
                    //})
                ->take(5)->get();

            $arr_complaint = Complaint::where('session_id', $session_id)->count();
        }

        $school = get_school_data();
        $admin = get_loggedin_user_data();
        $admin_user_id = $admin['admin_user_id'];


        $arr_return['school_summary'] = array(
            'student' => $total_students,
            'employee' => $total_employees,
            'school' => $school,
            'arr_complaint_pending' => $arr_complaint_pending,
            'arr_complaint' => $arr_complaint,
        );
        $data['dashboard_data'] = View::make('backend.header.dashboard_body')->with($arr_return)->render();
        return view('admin_panel.dashboard')->with($data);
    }

    public function setLanguage(Request $request)
    {
        $locale = $request->id;
        Session::put('locale', $locale);
        echo json_encode(true);
    }

    /**
     * Add and view for User Role
     * @Bhuvanesh on 09 Feb 2019
     */
    public function userRoleAdd(Request $request, $id = null)
    {
        $user_role = [];
        $user_role_id = null;
        if (!empty($id)) {
            $decrypted_user_role_id = get_decrypted_value($id, true);
            $user_role = UserRole::where('user_role_id', $decrypted_user_role_id)->first();
            if (empty($user_role)) {
                return redirect('admin/user-role')->withError('User Role not found!');
            }
            $encrypted_user_role_id = get_encrypted_value($user_role['user_role_id'], true);
            $save_url = url('admin/user-role/save/' . $encrypted_user_role_id);
            $submit_button = 'Update';
            $user_role_id = $decrypted_user_role_id;
        } else {
            $save_url = url('admin/user-role/save');
            $submit_button = 'Save';
        }
        $user_type = \Config::get('custom.user_type');

        $data = array(
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'user_role' => $user_role,
            'user_type' => $user_type,
            'redirect_url' => url('admin/user-role'),
        );
        return view('backend.user-role.add')->with($data);
    }

    /**
     * Any Data for User Role
     * @Bhuvanesh on 09 Feb 2019
     */
    public function userRoleAnyData(Request $request)
    {
        $user_roles = [];
        $user_roles = UserRole::orderBy('user_role_name')->get()->toArray();

        return Datatables::of($user_roles)
            ->addColumn('user_role_type', function ($user_roles) {
                $user_type = \Config::get('custom.user_type');
                return $user_type[$user_roles['user_type_id']];
            })
            ->addColumn('user_role_status', function ($user_roles) {
                if ($user_roles['user_role_status'] == '1') {
                    return 'Active';
                } else {
                    return 'Inactive';
                }
            })
            ->addColumn('action', function ($user_roles) {
                $delete = '';
                if ($user_roles['user_role_id'] != 1) {
                    $delete = ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $user_roles['user_role_id'] . '"><i class="fa fa-trash"></i></button>';
                }
                $encrypted_user_role_id = get_encrypted_value($user_roles['user_role_id'], true);
                //p($encrypted_user_role_id);
                return '<a title="Edit" id="deletebtn1" href="' . url('admin/user-role/' . $encrypted_user_role_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                    . $delete;
            })->rawColumns(['user_role_type' => 'user_role_type', 'user_role_status' => 'user_role_status', 'action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     * Save User Role
     * @Bhuvanesh on 09 Feb 2019
     */
    public function userRoleSave(Request $request, $id = null)
    {
        $decrypted_user_role_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $user_role = UserRole::find($decrypted_user_role_id);

            if (!$user_role) {
                return redirect('/admin/user-role/')->withError('User Role not found!');
            }
            $success_msg = 'User role updated successfully!';
        } else {
            $user_role = new UserRole;
            $success_msg = 'User role saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
            'user_role_name' => 'required|unique:user_roles,user_role_name,' . $decrypted_user_role_id . ',user_role_id',
        ]);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction();
            try {
                $user_role->user_role_name = Input::get('user_role_name');
                $user_role->user_type_id = Input::get('user_role_type');
                $user_role->save();
            } catch (\Exception $e) {
                    //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin/user-role')->withSuccess($success_msg);
    }

    /**
     * Delete User Role
     * @Bhuvanesh on 09 Feb 2019
     */
    public function userRoleDestroy(Request $request)
    {
        $user_role_id = Input::get('user_role_id');
        $user_role = UserRole::find($user_role_id);
        if ($user_role) {
            DB::beginTransaction(); //Start transaction!
            try {
                $user_role->delete();
                $return_arr = array(
                    'status' => 'success',
                    'message' => 'User role deleted successfully!'
                );
            } catch (\Exception $e) {
                    //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                $return_arr = array(
                    'status' => 'used',
                    'message' => trans('language.delete_message')
                );
            }
            DB::commit();
        } else {
            $return_arr = array(
                'status' => 'error',
                'message' => 'User role not found!'
            );
        }
        return response()->json($return_arr, 200);
    }

    public function adminUsers(Request $request){
        $arr_class = get_all_classes();
        return view('backend.admin-users.index')->with(['arr_class' => $arr_class]);
    }

    public function adminUsersData(Request $request){
        $mobile_number = Input::get('mobile_number');
        $app_user_arr = [];
        $app_users = AdminUser::where([["user_type_id","=",2]])->where(function($q) use($mobile_number){
            if(!empty($mobile_number)){
                $q->where("mobile_number","LIKE","%$mobile_number%");
            }
        })->latest()->get();
        foreach($app_users as $app_user){
            $student_parent = StudentParent::where('admin_user_id', $app_user->admin_user_id)->first();
            if(!empty($student_parent)){
                $student_parent_name = $student_parent->father_first_name." ".$student_parent->father_middle_name." ".$student_parent->father_last_name;
                if(empty($student_parent_name)){
                    $student_parent_name = $student_parent->mother_first_name." ".$student_parent->mother_middle_name." ".$student_parent->mother_last_name;
                }
                $app_user_students = Student::where([
                    ["student_parent_id","=",$student_parent->student_parent_id],
                    ["student_status","=",1],
                    ["student_left","=","No"],
                ])->get();
                if(!empty($app_user_students)){
                    $student_names = "";
                    foreach($app_user_students as $app_user_student){
                        $student_names .= $app_user_student->first_name." ".$app_user_student->middle_name." ".$app_user_student->last_name.", ";
                    }
                    $subscription_html = "<table class='table'>
                                            <thead>
                                                <tr>
                                                    <th>Order Id</th>
                                                    <th>Amount</th>
                                                    <th>Payment Date</th>
                                                    <th>Start Date</th>
                                                    <th>End Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>";
                    $subscriptions = SubscriptionDetail::where([['student_parent_id', '=', $student_parent->student_parent_id], ['payment_status', '=', 1]])->latest()->get();
                    if (!empty($subscriptions)) {
                        foreach($subscriptions as $subscription){
                            $amount = $subscription->amount/100;
                            $subscription_html .= "<tr>
                                                    <th>$subscription->order_id</th>
                                                    <th>$amount</th>
                                                    <th>$subscription->payment_date</th>
                                                    <th>$subscription->start_date</th>
                                                    <th>$subscription->end_date</th>
                                                </tr>";
                        }
                    }
                    $subscription_html .= "</tbody></table>";
                    array_push($app_user_arr,array(
                        "admin_user_id" => $app_user->admin_user_id,
                        "mobile_number" => $app_user->mobile_number,
                        "student_parent_name" => $student_parent_name,
                        "students" => $student_names,
                        "subscription_html" => $subscription_html,
                    ));
                }
            }
        }

        return Datatables::of($app_user_arr)
            ->addColumn('action', function ($app_user_arr) {
                $encrypted_admin_user_id = get_encrypted_value($app_user_arr['admin_user_id'], true);
                return '<a title="Update Password" id="update-password-'.$app_user_arr['admin_user_id'].'" onclick="viewUpdatePassword('.$app_user_arr['admin_user_id'].')" tb-data="'.$encrypted_admin_user_id.'"><span class="label label-primary">Update Password</span></a>'.'<a title="Subscription" id="set-subscription-'.$app_user_arr['admin_user_id'].'"  onclick="setSubscriptionData('.$app_user_arr['admin_user_id'].')" tb-data="'.$app_user_arr['subscription_html'].'"><span class="label label-success">Subscription</span></a>';
            })
            ->rawColumns(['action'])->make(true);
    }

    public function updateAdminUserPassword(Request $request){
        $decrypted_user_role_id = get_decrypted_value($request->update_admin_user_id, true);
        $request->request->add(["admin_user_id" => $decrypted_user_role_id]);
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'admin_user_id' => 'required|exists:admin_users,admin_user_id',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->back()->withErrors($errors);
        }
        DB::beginTransaction();
        try {
            $arr_data = array(
                'password' => \Illuminate\Support\Facades\Hash::make($request->password)
            );
            DB::table('admin_users')->where('admin_user_id', $request->admin_user_id)->update($arr_data);
            $message= "Password changed successfully";
            DB::commit();
            return redirect()->back()->withSuccess($message);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}

