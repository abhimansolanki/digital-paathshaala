<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Session;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    //use Session as LSession; 
    use Illuminate\Support\Facades\DB;
    use App\Model\backend\ClassSection;
    use App\Http\Controllers\backend\HistoryController;

    class SessionController extends Controller
    {

        public function __construct()
        {
            
        }

        public function add(Request $request, $id = NULL)
        {
            $data    = [];
            $session = [];
            if (!empty($id))
            {
                $decrypted_session_id  = get_decrypted_value($id, true);
                $session               = Session::select('session_id','session_year','session_status',DB::raw('DATE_FORMAT(start_date,"%d/%m/%Y") as start_date'),DB::raw('DATE_FORMAT(end_date,"%d/%m/%Y") as end_date'))->Find($decrypted_session_id);
                if (!$session)
                {
                    return redirect('admin/session')->withError('Session not found!');
                }
                $page_title           = 'Edit Session';
                $encrypted_session_id = get_encrypted_value($session->session_id, true);
                $save_url             = url('admin/session/save/' . $encrypted_session_id);
                $submit_button        = 'Update';
            }
            else
            {
                $page_title    = 'Create Session';
                $save_url      = url('admin/session/save');
                $submit_button = 'Save';
            }
            $arr_current_session            = \Config::get('custom.current_session');
            $session['arr_current_session'] = $arr_current_session;
            //$session['enable_sync_sesson'] = !empty($id) ? false : true ;
            $session['arr_existing_sessions'] = add_blank_option(Session::get()->pluck("session_year","session_id")->toArray(),"--Select Session--");

            $data = array(
                'page_title'    => $page_title,
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'session'       => $session,
                'redirect_url'  => url('admin/session/'),
            );
            return view('backend.session.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_session_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $session = Session::find($decrypted_session_id);

                if (!$session)
                {
                    return redirect('/admin/session/')->withError('Session not found!');
                }
                $success_msg = 'Session updated successfully!';
            }
            else
            {
                $session     = New Session;
                $success_msg = 'Session saved successfully!';
            }
            $validation = [
                'session_year'   => 'required|unique:sessions,session_year,' . $decrypted_session_id . ',session_id',
                'start_date'     => 'required',
                'end_date'       => 'required',
                'session_status' => 'required',
            ];
            if($id == NULL){
                $validation = array_merge($validation, ['source_session_id' => 'required',]);
            }

            $validatior = Validator::make($request->all(), $validation);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $session->session_year   = Input::get('session_year');
                    $session->start_date     = get_formatted_date(Input::get('start_date'), 'database');
                    $session->end_date       = get_formatted_date(Input::get('end_date'), 'database');
                    $session->description    = Input::get('description');
                    $session->session_status = Input::get('session_status');
                    $session->save();

                    if(Input::get('session_status') == 1){
                        Session::where('session_id','!=',$session->session_id)->update(['session_status' => 0]);
                        $session_id   = $session->session_id;
                        $session_year = $session->session_year;
                        $set_data   = set_session($session_id, $session_year);
                        $success_msg .= ' and Current session set successfully!';
                    }

                    if(isset($request->source_session_id) && $request->source_session_id != ""){
                        HistoryController::syncData($session->session_id, Input::get('source_session_id'));
                        $success_msg .= " & Data Synced successfully.";
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                    return redirect()->back()->withInput()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/session')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $session_id = Input::get('session_id');
            $session    = Session::find($session_id);
            if ($session)
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $session->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Session deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Session not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $offset      = Input::get('start');
            $limit       = Input::get('length');
            $session = Session::select('session_id','session_year',DB::raw('DATE_FORMAT(start_date,"%d/%m/%Y") as start_date'),DB::raw('DATE_FORMAT(end_date,"%d/%m/%Y") as end_date'),DB::raw("IF(session_status=1,'Yes', 'No') AS current_session"))
                ->orderBy('start_date', 'ASC')
//                ->skip($offset)->take($limit)
                ->get();
            return Datatables::of($session)
                    ->addColumn('action', function ($session)
                    {
                        $encrypted_session_id = get_encrypted_value($session->session_id, true);
                        return '<a title="Edit" id="deletebtn1" href="' . url('admin/session/' . $encrypted_session_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>';
//                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $session->session_id . '"><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

        public function checkSession(Request $request)
        {
            $session_id = Input::get('session_id');
            $session    = Session::where('session_status', 1)->first();
            if ((isset($session->session_id) && !empty($session_id) && $session->session_id != $session_id))
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => "Sorry, you can't set it as current session!"
                );
            }
            else
            {
                $return_arr = array(
                    'status'  => 'success',
                    'message' => 'Sucessfully set current session!'
                );
            }
            return response()->json($return_arr);
        }

        public function setSession(Request $request)
        {
            $session_id = Input::get('session_id');
            $session    = Session::where('session_id', $session_id)->first();

            if (isset($session->session_id))
            {
                $session_id   = $session->session_id;
                $session_year = $session->session_year;
                if (!empty($session_id) && !empty($session_year))
                {
                    $set_data   = set_session($session_id, $session_year);
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Session set successfully!'
                    );
                }
            }
            else
            {
                $return_arr = array(
                    'status'  => 'success',
                    'message' => 'Set session as current!'
                );
            }
            return response()->json($return_arr);
        }

        public function getSessionData(Request $request)
        {
            $session_id = Input::get('session_id');
            $session    = Session::where('session_id', $session_id)->first();

            if (isset($session->session_id))
            {
                $start_date = get_formatted_date($session->start_date, 'display');
                $end_date   = get_formatted_date($session->end_date, 'display');
                $return_arr = array(
                    'start_date' => $start_date,
                    'end_date'   => $end_date,
                    'status'     => 'success',
                    'message'    => 'Session get successfully!'
                );
            }
            else
            {
                $return_arr = array(
                    'status'  => 'success',
                    'message' => 'Set session as current!'
                );
            }
            return response()->json($return_arr);
        }

    }
    