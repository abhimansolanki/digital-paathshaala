<?php

namespace App\Http\Controllers\backend;

use PDF;
use View;
use Validator;
use Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\Module;
use App\Model\backend\UserRole;
use App\Model\backend\UserPermission;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Index for Permission Page
     * @Bhuvanesh on 02 Feb 2019
     */
    public function index()
    {
        $user_role_arr = [];
        $user_role = UserRole::select('user_role_id', 'user_role_name')->get()->toArray();
        if (!empty($user_role)) {
            foreach ($user_role as $key => $value) {
                $user_role_arr[$value['user_role_id']] = $value['user_role_name'];
            }
        }
        $user_role_arr = \add_blank_option($user_role_arr, 'Select User Role');
        $data = array(
            'user_roles' => $user_role_arr,
        );
        return view('backend.permission.add')->with($data);
    }

    /**
     * View Data for Permission view table
     * @Bhuvanesh on 21 Feb 2019
     */
    public function viewTable(Request $request)
    {
        if(isset($request) && $request->get('user_role_id') != ''){
            $permissions = [];
            $permissions = UserPermission::where('user_role_id', $request->get('user_role_id'))->get()->first();
            if(!empty($permissions)){
                $permissions['user_permission_modules']  =  explode(',', $permissions['user_permission_modules']);
            }
            $modules_action = \Config::get('custom.module_action');
            $modules_arr = [];
            $modules = Module::get()->toArray();
            foreach ($modules as $key => $value) {
                $modules_arr[$value['key']]['module_name'] = $value['module'];
                $modules_arr[$value['key']]['key'] = $value['key'];
                $modules_arr[$value['key']]['details'][$value['module_action']] = $value;
                $modules_arr[$value['key']]['existModuleIds'][] = $value['module_id'];
            }
            $data = array(
                'user_role_id' => $request->get('user_role_id'),
                'modules' => $modules_arr,
                'modules_action' => $modules_action,
                'permissions' => $permissions,
            );
            $preview = View::make('backend.permission.permission-view')->with($data)->render();
            return response()->json($preview);
        }
        else{
            return response()->json();
        }
    }

    /**
     * Save Permissions
     * @Bhuvanesh on 02 Feb 2019
     */
    public function save(Request $request)
    {
        if(isset($request) && $request->get('user_role_id')){
            $user_role = UserRole::where('user_role_id', $request->get('user_role_id'))->first();
            if($user_role){
                $permission_arr = [];
                $success_msg = '';
                $map = $request->get('map');
                foreach($map as $module){
                    foreach($module as $key => $value){
                        array_push($permission_arr, $key);
                    }    
                }
                $permission_arr_imp = implode(',',$permission_arr);
                $permission = UserPermission::where('user_role_id', $user_role['user_role_id'])->first();
                if(!empty($permission)){
                    $success_msg = "User permission updated successfully";
                }
                else{
                    $permission = new UserPermission;
                    $success_msg = "User permission saved successfully";
                }
                DB::beginTransaction();
                try{
                    $permission->user_role_id = $user_role['user_role_id'];
                    $permission->user_permission_modules = $permission_arr_imp;
                    $permission->save();
                } catch(\Exception $e) {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return array('status' => 'error', 'message' => $error_message);
                }
                DB::commit();
                return array('status' => 'success', 'message' => $success_msg);
            }
            else{
                return array('status' => 'error', 'message' => 'User Role Not Found');
            }
        }
        else{
            return array('status' => 'error', 'message' => 'Invalid Parameter');
        }
    }

    /**
     * Add and view for User Role
     * @Bhuvanesh on 09 Feb 2019
     */
    public function userRoleAdd(Request $request, $id = null)
    {
        $user_role = [];
        $user_role_id = null;
        if (!empty($id)) {
            $decrypted_user_role_id = get_decrypted_value($id, true);
            $user_role = UserRole::where('user_role_id', $decrypted_user_role_id)->first();
            if (empty($user_role)) {
                return redirect('admin/user-role')->withError('User Role not found!');
            }
            $encrypted_user_role_id = get_encrypted_value($user_role['user_role_id'], true);
            $save_url = url('admin/user-role/save/' . $encrypted_user_role_id);
            $submit_button = 'Update';
            $user_role_id = $decrypted_user_role_id;
        } else {
            $save_url = url('admin/user-role/save');
            $submit_button = 'Save';
        }
        $user_type = \Config::get('custom.user_type');

        $data = array(
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'user_role' => $user_role,
            'user_type' => $user_type,
            'redirect_url' => url('admin/user-role'),
        );
        return view('backend.user-role.add')->with($data);
    }

    /**
     * Any Data for User Role
     * @Bhuvanesh on 09 Feb 2019
     */
    public function userRoleAnyData(Request $request)
    {
        $user_roles = [];
        $user_roles = UserRole::orderBy('user_role_name')->get()->toArray();

        return Datatables::of($user_roles)
            ->addColumn('user_role_type', function ($user_roles) {
                $user_type = \Config::get('custom.user_type');
                return $user_type[$user_roles['user_type_id']];
            })
            ->addColumn('user_role_status', function ($user_roles) {
                if ($user_roles['user_role_status'] == '1') {
                    return 'Active';
                } else {
                    return 'Inactive';
                }
            })
            ->addColumn('action', function ($user_roles) {
                $delete = '';
                if ($user_roles['user_role_id'] != 1) {
                    $delete = ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $user_roles['user_role_id'] . '"><i class="fa fa-trash"></i></button>';
                }
                $encrypted_user_role_id = get_encrypted_value($user_roles['user_role_id'], true);
                //p($encrypted_user_role_id);
                return '<a title="Edit" id="deletebtn1" href="' . url('admin/user-role/' . $encrypted_user_role_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                    . $delete;
            })->rawColumns(['user_role_type' => 'user_role_type', 'user_role_status' => 'user_role_status', 'action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     * Save User Role
     * @Bhuvanesh on 09 Feb 2019
     */
    public function userRoleSave(Request $request, $id = null)
    {
        $decrypted_user_role_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $user_role = UserRole::find($decrypted_user_role_id);

            if (!$user_role) {
                return redirect('/admin/user-role/')->withError('User Role not found!');
            }
            $success_msg = 'User role updated successfully!';
        } else {
            $user_role = new UserRole;
            $success_msg = 'User role saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
            'user_role_name' => 'required|unique:user_roles,user_role_name,' . $decrypted_user_role_id . ',user_role_id',
        ]);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction();
            try {
                $user_role->user_role_name = Input::get('user_role_name');
                $user_role->user_type_id = Input::get('user_role_type');
                $user_role->save();
            } catch (\Exception $e) {
                    //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin/user-role')->withSuccess($success_msg);
    }

}
