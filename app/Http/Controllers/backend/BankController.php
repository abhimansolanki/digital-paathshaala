<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Bank;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class BankController extends Controller
    {

        public function __construct()
        {
            
        }


        public function add(Request $request, $id = NULL)
        {
            $bank = [];
            $data = [];
            if (!empty($id))
            {
                $decrypted_bank_id = get_decrypted_value($id, true);
                $bank              = Bank::Find($decrypted_bank_id);
                if (!$bank)
                {
                    return redirect('admin/bank')->withError('Bank not found!');
                }
                $encrypted_bank_id = get_encrypted_value($bank->bank_id, true);
                $save_url          = url('admin/bank/save/' . $encrypted_bank_id);
                $submit_button     = 'Update';
            }
            else
            {
                $save_url      = url('admin/bank/save');
                $submit_button = 'Save';
            }

            $data = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'bank'          => $bank,
                'redirect_url'  => url('admin/bank/'),
            );
            return view('backend.bank.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_bank_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $bank = Bank::find($decrypted_bank_id);

                if (!$bank)
                {
                    return redirect('/admin/bank/')->withError('Bank not found!');
                }
                $success_msg = 'Bank updated successfully !';
            }
            else
            {
                $bank        = New Bank;
                $success_msg = 'Bank saved successfully !';
            }
             $bank_name = Input::get('bank_name');
             $ifsc_code = Input::get('ifsc_code');
             $bank_branch = Input::get('bank_branch');
            $validatior = Validator::make($request->all(), [
                    'bank_name'   => 'required|unique:banks,bank_name,' . $decrypted_bank_id .',bank_id,ifsc_code,'.$ifsc_code.',bank_branch,'.$bank_branch,
                    'ifsc_code'   => 'required|unique:banks,ifsc_code,' .',bank_id',
//                    'ifsc_code'   => 'required|unique:banks,ifsc_code,' . $decrypted_bank_id .',bank_id,bank_name,'.$bank_name.',bank_branch,'.$bank_branch,
                    'bank_branch' => 'required|unique:banks,bank_branch,' . $decrypted_bank_id .',bank_id,bank_name,'.$bank_name.',ifsc_code,'.$ifsc_code,
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $bank->bank_name   = $bank_name;
                    $bank->ifsc_code   = $ifsc_code;
                    $bank->bank_alias  = Input::get('bank_alias');
                    $bank->bank_branch = $bank_branch;
                    $bank->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withInput()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/bank')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $bank_id = Input::get('bank_id');
            $bank    = Bank::find($bank_id);
            if ($bank)
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $bank->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Bank deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Bank not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $bank = Bank::orderBy('bank_name', 'ASC')->get();
            return Datatables::of($bank)
                    ->addColumn('action', function ($bank)
                    {
                        $encrypted_bank_id = get_encrypted_value($bank->bank_id, true);

                        return '<a title="Edit" id="deletebtn1" href="'.url('admin/bank/' . $encrypted_bank_id). '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $bank->bank_id . '"><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

    }
    