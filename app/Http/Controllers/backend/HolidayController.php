<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\CronController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\Holiday;
use Validator;
use Illuminate\Support\Facades\Input;
use Datatables;
use Illuminate\Support\Facades\DB;

class HolidayController extends Controller
{

    public function __construct()
    {

    }

    public function add(Request $request, $id = NULL)
    {
        $holiday = [];
        $holiday_id = null;
        if (!empty($id)) {
            $decrypted_holiday_id = get_decrypted_value($id, true);
            $holiday = $this->getHolidayData($decrypted_holiday_id);
            $holiday = isset($holiday[0]) ? $holiday[0] : [];
            if (!$holiday) {
                return redirect('admin/holiday')->withError('Holiday not found!');
            }
            $encrypted_holiday_id = get_encrypted_value($holiday['holiday_id'], true);
            $save_url = url('admin/holiday/save/' . $encrypted_holiday_id);
            $submit_button = 'Update';
            $holiday_id = $decrypted_holiday_id;
        } else {
            $save_url = url('admin/holiday/save');
            $submit_button = 'Save';
        }
        $session = db_current_session();
        $holiday['session_start_date'] = $session['session_start_date'];
        $holiday['session_end_date'] = $session['session_end_date'];
        $data = array(
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'holiday' => $holiday,
            'redirect_url' => url('admin/holiday/'),
        );
//	p($data);
        return view('backend.holiday.add')->with($data);
    }

    public function save(Request $request, $id = NULL)
    {
        $decrypted_holiday_id = get_decrypted_value($id, true);
        $session = db_current_session();
        $session_id = $session['session_id'];
        if (!empty($id)) {
            $holiday = Holiday::find($decrypted_holiday_id);

            if (!$holiday) {
                return redirect('/admin/holiday/')->withError('Holiday not found!');
            }
            $success_msg = 'Holiday updated successfully!';
        } else {
            $holiday = new Holiday;
            $success_msg = 'Holiday saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'holiday_name' => 'required|unique:holidays,holiday_name,' . $decrypted_holiday_id . ',holiday_id,session_id,' . $session_id,
            'holiday_start_date' => 'required',
            'holiday_end_date' => 'required',
        ]);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction();
            try {

                if (empty($id)) {
                    $holiday->session_id = $session_id;
                }
                $holiday->holiday_name = Input::get('holiday_name');
                $holiday->holiday_start_date = get_formatted_date(Input::get('holiday_start_date'), 'database');
                $holiday->holiday_end_date = get_formatted_date(Input::get('holiday_end_date'), 'database');
                $holiday->save();
                $cron = new CronController;
                $cron->pushNotificationStudentHoliday($holiday);
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withInput()->withErrors($error_message);
            }
            DB::commit();
        }

        return redirect('admin/holiday')->withSuccess($success_msg);
    }

    public function destroy(Request $request)
    {
        $holiday_id = Input::get('holiday_id');
        $holiday = Holiday::find($holiday_id);
        if ($holiday) {
            $holiday->delete();
            $return_arr = array(
                'status' => 'success',
                'message' => 'Holiday deleted successfully!'
            );
        } else {
            $return_arr = array(
                'status' => 'error',
                'message' => 'Holiday not found!'
            );
        }
        return response()->json($return_arr);
    }

    public function anyData()
    {
        $holiday = [];
        $offset = Input::get('start');
        $limit = Input::get('length');
        $holiday_id = null;
        $arr_holiday = $this->getHolidayData($holiday_id, $offset, $limit);
        foreach ($arr_holiday as $key => $holiday_data) {
            $holiday[$key] = (object)$holiday_data;
        }
        return Datatables::of($holiday)
            ->addColumn('action', function ($holiday) {
                $encrypted_holiday_id = get_encrypted_value($holiday->holiday_id, true);
                return '<a title="Edit" id="deletebtn1" href="' . url('admin/holiday/' . $encrypted_holiday_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                    . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $holiday->holiday_id . '"><i class="fa fa-trash"></i></button>';
            })->make(true);
    }

    public function getHolidayData($holiday_id = null, $offset = null, $limit = null)
    {
        $holiday_return = [];
        $session_id = null;
        $session = get_current_session();
        if (!empty($session)) {
            $session_id = $session['session_id'];
        }
        $arr_holiday_data = Holiday::where('session_id', $session_id)->where(function ($query) use ($holiday_id) {
            if (!empty($holiday_id)) {
                $query->where('holiday_id', $holiday_id);
            }
        })
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
            ->get();
        if (!empty($arr_holiday_data)) {
            foreach ($arr_holiday_data as $key => $holiday_data) {
                $holiday_return[] = array(
                    'holiday_id' => $holiday_data['holiday_id'],
                    'session_id' => $holiday_data['session_id'],
                    'holiday_name' => $holiday_data['holiday_name'],
                    'holiday_start_date' => get_formatted_date($holiday_data['holiday_start_date'], 'display'),
                    'holiday_end_date' => get_formatted_date($holiday_data['holiday_end_date'], 'display'),
                );
            }
        }
        return $holiday_return;
    }

}
    