<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\FeeType;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\View as View;

    class FeeTypeController extends Controller
    {

        public function __construct()
        {
            
        }

//        public function index()
//        {
//            $data = array(
//                'redirect_url' => url('admin/fee-type'),
//            );
//            return view('backend.fee-type.index')->with($data);
//        }

        public function add(Request $request, $id = NULL)
        {
            $fee_type    = [];
            $fee_type_id = null;
            if (!empty($id))
            {
                $decrypted_fee_type_id = get_decrypted_value($id, true);
                $fee_type              = $this->getFeeTypeData($decrypted_fee_type_id);
                $fee_type              = isset($fee_type[0]) ? $fee_type[0] : [];
                if (!$fee_type)
                {
                    return redirect('admin/fee-type')->withError('FeeType not found!');
                }
                $encrypted_fee_type_id = get_encrypted_value($fee_type['fee_type_id'], true);
                $save_url              = url('admin/fee-type/save/' . $encrypted_fee_type_id);
                $submit_button         = 'Update';
                $fee_type_id           = $decrypted_fee_type_id;
            }
            else
            {
                $save_url      = url('admin/fee-type/save');
                $submit_button = 'Save';
            }
            $arr_is_root              = \Config::get('custom.is_root');
            $fee_type['arr_is_root']  = $arr_is_root;
            $root_id= 0;
            $arr_fee_type             = get_fee_type($root_id);
            $fee_type['arr_fee_type'] = add_blank_option($arr_fee_type, '--Select fee type --');
            $data                     = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'fee_type'      => $fee_type,
                'redirect_url'  => url('admin/fee-type/'),
            );
            return view('backend.fee-type.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_fee_type_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $fee_type = FeeType::find($decrypted_fee_type_id);

                if (!$fee_type)
                {
                    return redirect('/admin/fee-type/')->withError('Fee Type not found!');
                }
                $success_msg = 'Fee Type updated successfully!';
            }
            else
            {
                $fee_type    = New FeeType;
                $success_msg = 'Fee Type saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'fee_type' => 'required|unique:fee_types,fee_type,' . $decrypted_fee_type_id . ',fee_type_id',
                    'is_root'  => 'required',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction(); //Start transaction!

                try
                {
                    if (empty($id))
                    {
                        $order          = 1;
                        $fee_type_order = FeeType::max('order');
                        if ($fee_type_order)
                        {
                            $order           = $fee_type_order + $order;
                            $fee_type->order = $order;
                        }
                    }
                    $fee_type->fee_type = Input::get('fee_type');
                    $fee_type->is_root  = Input::get('is_root');
                    $fee_type->root_id  = 0;
                    if (Input::get('is_root') == 0)
                    {
                        $fee_type->root_id = Input::get('fee_type_id');
                    }
                    $fee_type->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withInput()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/fee-type')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $fee_type_id = Input::get('fee_type_id');
            $fee_type    = FeeType::find($fee_type_id);
            if ($fee_type)
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $fee_type->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Fee Type deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Fee Type not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $fee_type          = [];
            $root_id           = 0;
            $offset            = Input::get('start');
            $limit             = Input::get('length');
            $arr_fee_type_root = get_fee_type($root_id);
            $arr_fee_type      = $this->getFeeTypeData($fee_type_id       = null, $offset, $limit);
            foreach ($arr_fee_type as $key => $fee_type_data)
            {
                if ($fee_type_data['root_id'] != 0)
                {
                    $fee_type_data['fee_type'] = $arr_fee_type_root[$fee_type_data['root_id']] . '->' . $fee_type_data['fee_type'];
                }
                $fee_type[$key] = (object) $fee_type_data;
            }
            return Datatables::of($fee_type)
                    ->addColumn('action', function ($fee_type)
                    {
                        $disabled = '';
                        if ($fee_type->is_fixed == 1)
                        {
                            $disabled = 'disabled';
                        }
                        $encrypted_fee_type_id = get_encrypted_value($fee_type->fee_type_id, true);
                        return '<a title="Edit" id="deletebtn1" href="' . url('admin/fee-type/' . $encrypted_fee_type_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>';
//                            . '<button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $fee_type->fee_type_id . '" ' . $disabled . '><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

        public function getFeeTypeData($fee_type_id = null, $offset = null, $limit = null)
        {
            $fee_type_return   = [];
            $arr_fee_type_data = FeeType::
                where(function($query) use ($fee_type_id)
                {
                    if (!empty($fee_type_id))
                    {
                        $query->where('fee_type_id', $fee_type_id);
                    }
                })
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
                ->orderBy('order', 'ASC')
                ->get();

            if (!empty($arr_fee_type_data))
            {
                foreach ($arr_fee_type_data as $key => $fee_type_data)
                {
                    $fee_type_return[] = array(
                        'fee_type_id' => $fee_type_data['fee_type_id'],
                        'fee_type'    => $fee_type_data['fee_type'],
                        'root_id'     => $fee_type_data['root_id'],
                        'is_root'     => $fee_type_data['is_root'],
                        'is_fixed'    => $fee_type_data['is_fixed'],
                        'order'       => $fee_type_data['order'],
                    );
                }
            }
            return $fee_type_return;
        }

        function getFeeOrder(Request $request)
        {
            $fee_type             = FeeType::where('fee_type_status', 1)->orderBy('order', 'ASC')->get();
            $data['arr_fee_type'] = $fee_type;
            $data['status']      = 'success';
            return response()->json($data);
        }

        function setFeeOrder(Request $request)
        {
            if ($request->ajax())
            {
                $arr_fee_order = Input::get('item');
                foreach ($arr_fee_order as $key => $class_id)
                {
                    $fee_order       = $key + 1;
                    $fee_type        = FeeType::find($class_id);
                    $fee_type->order = $fee_order;
                    $fee_type->save();
                }
                return response()->json('success');
            }
            $fee_type             = FeeType::where('fee_type_status', 1)->orderBy('order', 'ASC')->get();
            $data['arr_fee_type'] = $fee_type;
            return view('backend.fee-type.order')->with($data);
        }

    }
    