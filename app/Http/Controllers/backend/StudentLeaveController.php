<?php

    namespace App\Http\Controllers\backend;

//    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
//    use App\Model\backend\StudentLeave;
    use Illuminate\Support\Facades\Input;
    use Datatables;

    use Illuminate\Support\Facades\DB;

    class StudentLeaveController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index()
        {
            $data = array();
            return view('backend.student-leave.index')->with($data);
        }

        public function anyData()
        {
            $student_leave = [];
            $session       = get_current_session();
            if (!empty($session))
            {
                $session_id        = $session['session_id'];
                $offset            = Input::get('start');
                $limit             = Input::get('length');
                $arr_student_leave = DB::table('student_leaves as sl')
                        ->join('students as s', 's.student_id', '=', 'sl.student_id')
                        ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
                        ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
                        ->leftJoin('sections as sec', 'sec.section_id', '=', 's.current_section_id')
                        ->select('sl.*','s.student_id','s.enrollment_number','s.address_line1','sp.father_contact_number', 'c.class_name', 'sec.section_name', 
                           's.first_name', 's.middle_name', 's.last_name',
                           'sp.father_first_name', 'sp.father_middle_name', 'sp.father_last_name'
                        )
                        ->where('s.student_status', 1)
                        ->where('sl.session_id', $session_id)
//                        ->skip($offset)->take($limit)
                    ->get();
//              
                foreach ($arr_student_leave as $key => $student_leave_data)
                {   
                    $student_leave_data = (array)$student_leave_data;
                    $student_leave_data['student_name'] = $student_leave_data['first_name'] .' '.$student_leave_data['middle_name'].' '.$student_leave_data['last_name'];
                    $student_leave_data['father_name'] = $student_leave_data['father_first_name'] .' '.$student_leave_data['father_middle_name'].' '.$student_leave_data['father_last_name'];
                    $student_leave[$key] = (object) $student_leave_data;
                }
            }
            return Datatables::of($student_leave)->make(true);
        }

    }
    