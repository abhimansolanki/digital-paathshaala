<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\ExamClass;
    use App\Model\backend\CreateExam;
    use App\Model\backend\ExamType;
    use Illuminate\Support\Facades\View as View;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class ExamClassController extends Controller
    {

        public function __construct()
        {
	
        }

        public function add(Request $request, $exam_id, $id = NULL)
        {
	$class_id = null;
	$session_id = null;
	$save_url = '';
	$submit_button = '';
	$exam_class = [];
	$exam = CreateExam::select('exam_id', 'exam_name')->where('exam_id', $exam_id)->where('exam_status', 1)->first();
	if (!empty($exam->exam_id))
	{
	    $exam_id = $exam->exam_id;
	    $exam_class = [];
	    $exam_class_id = null;
	    if (!empty($id))
	    {
	        $decrypted_exam_id = get_decrypted_value($id, true);
	        $exam_class = $this->getExamClassData($exam_id, $decrypted_exam_id);
	        $exam_class = isset($exam_class[0]) ? $exam_class[0] : [];
//	        p($exam_class);
	        if (!$exam_class)
	        {
		return redirect('admin/exam-class')->withError('Exam Class mapping not found!');
	        }
	        $encrypted_exam_id = get_encrypted_value($exam_class['exam_class_id'], true);
	        $save_url = url('admin/exam-class/save/' . $exam_id . '/' . $encrypted_exam_id);
	        $submit_button = 'Update';
	        $exam_class_id = $decrypted_exam_id;
	        $class_id = $exam_class['class_id'];
	        $session_id = $exam_class['session_id'];
	    } else
	    {
	        $save_url = url('admin/exam-class/save/' . $exam_id);
	        $submit_button = 'Save';
	        $current_session = db_current_session();
	        $exam_class['session_id'] = $current_session['session_id'];
	    }
	    $exam_class['arr_class'] = add_blank_option(get_all_classes(), '--Select class --');
	    $exam_class['arr_section'] = add_blank_option(get_class_section($class_id, $session_id), '-- Select Setion -- ');
	    $exam_class['arr_exam_type'] = get_all_exam_type();
	    $exam_class['arr_session'] = add_blank_option(get_current_next_session(), '-- Academic year --');
	    $exam_class['arr_class_subject'] = [];
	    $exam_class['exam_name'] = $exam->exam_name;
	    $exam_class['exam_id'] = $exam->exam_id;
	}

	$data = array(
	    'save_url' => $save_url,
	    'submit_button' => $submit_button,
	    'exam_class' => $exam_class,
	    'redirect_url' => url('admin/create-exam/'),
	);

	return view('backend.exam-class.add')->with($data);
        }

        /*
         * Save Exam Class mapping for each section
         * It will be unique for each class ans Section
         */

        public function save(Request $request, $exam_id, $id = NULL)
        {
	$class_id = Input::get('class_id');
	$session_id = Input::get('session_id');
	$section_id = Input::get('section_id');
	$decrypted_exam_id = get_decrypted_value($id, true);
	if (!empty($id))
	{
	    $exam_class = ExamClass::find($decrypted_exam_id);
	    if (!$exam_class)
	    {
	        return redirect('/admin/exam-class/')->withError('Exam Class not found!');
	    }
	    $success_msg = 'Exam class mapping updated successfully!';
	} else
	{
	    $exam_class = ExamClass::where('session_id', $session_id)
		        ->where('class_id', $class_id)
		        ->where('section_id', $session_id)
		        ->where('exam_id', $exam_id)->first();
	    if (empty($exam_class->exam_class_id))
	    {
	        $exam_class = New ExamClass;
	    } else
	    {
	        $exam_class = ExamClass::Find($exam_class->exam_class_id);
	    }
	    $success_msg = 'Exam class mapping saved successfully!';
	}
	$validatior = Validator::make($request->all(), [
		'session_id' => 'required',
		'class_id' => 'required',
		'exam_id' => 'required',
		'section_id' => 'required',
		'checked_subject_id.*' => 'required',
	]);
	if ($validatior->fails())
	{
	    return redirect()->back()->withInput()->withErrors($validatior);
	} else
	{
	    DB::beginTransaction(); //Start transaction!
	    try
	    {
	        $arr_exam_type_id = array_unique(Input::get('exam_type_id'));
	        // checked subject id
	        $arr_subject = Input::get('arr_subject_id');
	        $subject_marks = [];
	        if (!empty($arr_subject))
	        {
		$arr_subject_marks = [];
		$arr_subject_type = [];
		$subject_max_tot = 0;
		foreach ($arr_subject as $subject_id)
		{
		    $min_marks = 0;
		    $max_marks = 0;
		    $selected_exam_type = [];
		    $subject_type = [];

		    foreach ($arr_exam_type_id as $exam_type_id)
		    {
		        if ($request->has('subject_type_' . $subject_id . '_' . $exam_type_id))
		        {
			if ($request->has('min_marks_' . $subject_id . '_' . $exam_type_id))
			{
			    $min_marks = Input::get('min_marks_' . $subject_id . '_' . $exam_type_id);
			}
			if ($request->has('max_marks_' . $subject_id . '_' . $exam_type_id))
			{
			    $max_marks = Input::get('max_marks_' . $subject_id . '_' . $exam_type_id);
			}
			$selected_exam_type[$exam_type_id] = array(
			    'min_marks' => $min_marks,
			    'max_marks' => $max_marks,
			);
		        }
		    }
		    $subject_max_tot = array_sum(array_column($selected_exam_type, 'max_marks'));
		    $selected_exam_type['total_max_marks'] = $subject_max_tot;
		    $arr_subject_marks[$subject_id] = $selected_exam_type;
		}
		$subject_marks = json_encode($arr_subject_marks);
	        }
	        $exam_class->class_id = $class_id;
	        $exam_class->exam_id = $exam_id;
	        $exam_class->section_id = $section_id;
	        $exam_class->session_id = $session_id;
	        $exam_class->subject_marks_criteria = $subject_marks;
	        $exam_class->save();
	    } catch (\Exception $e)
	    {
	        //failed logic here
	        DB::rollback();
	        $error_message = $e->getMessage();
	        p($error_message);
	        return redirect()->back()->withErrors($error_message);
	    }
	    DB::commit();
	}
	return redirect('admin/create-exam/')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
	$exam_class_id = Input::get('exam_class_id');
	$exam_class = ExamClass::find($exam_class_id);
	if ($exam_class->exam_class_id)
	{
	    DB::beginTransaction(); //Start transaction!
	    try
	    {
	        $exam_class->delete();
	        $return_arr = array(
		'status' => 'success',
		'message' => 'Exam Class deleted successfully!'
	        );
	    } catch (\Exception $e)
	    {
	        //failed logic here
	        DB::rollback();
	        $error_message = $e->getMessage();
	        $return_arr = array(
		'status' => 'used',
		'message' => trans('language.delete_message')
	        );
	    }

	    DB::commit();
	} else
	{
	    $return_arr = array(
	        'status' => 'error',
	        'message' => 'Exam Class not found!'
	    );
	}
	return response()->json($return_arr);
        }

        public function anyData()
        {
	$exam_id = Input::get('exam_id');
	$exam_class = [];
	$exam_class = $this->getExamClassData($exam_id);
	return Datatables::of($exam_class)
		    ->addColumn('action', function ($exam_class)
		    {
		        $encrypted_exam_class_id = get_encrypted_value($exam_class->exam_class_id, true);

		        return '<a title="Edit" id="deletebtn1" href="' . url('admin/exam-class/' . $exam_class->exam_id . '/' . $encrypted_exam_class_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
			    . '<button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $exam_class->exam_class_id . '"><i class="fa fa-trash"></i></button>';
		    })
		    ->make(true);
        }

        public function getExamClassData($exam_id = null, $exam_class_id = null, $class_id = null, $section_id = null)
        {
	$exam_class_return = [];
	$query = DB::table('exam_classes as ec')
	        ->join('create_exams as ce', 'ec.exam_id', '=', 'ce.exam_id')
	        ->join('classes as c', 'ec.class_id', '=', 'c.class_id')
	        ->join('sections as s', 'ec.section_id', '=', 's.section_id')
	        ->where('ce.exam_id', $exam_id)
	        ->select(
	        'ec.exam_class_id', 'ec.session_id', 'ec.class_id', 'c.class_name', 'ec.exam_id', 's.section_name', 'ce.exam_name', 'ec.section_id', 'ec.subject_marks_criteria'
	);
	if (!empty($class_id))
	{
	    $query->where('ec.class_id', $class_id);
	}
	if (!empty($section_id))
	{
	    $query->where('ec.section_id', $section_id);
	}
	if (!empty($exam_class_id))
	{
	    $query->where('ec.exam_class_id', $exam_class_id);
	}
	$arr_exam_class_data = $query->get()->toArray();
	if (!empty($arr_exam_class_data))
	{
	    foreach ($arr_exam_class_data as $exam_class_data)
	    {
	        $exam_class_data = (array) $exam_class_data;
	        $exam_class = array(
		'exam_class_id' => $exam_class_data['exam_class_id'],
		'exam_id' => $exam_class_data['exam_id'],
		'exam_name' => $exam_class_data['exam_name'],
		'class_name' => $exam_class_data['class_name'],
		'section_name' => $exam_class_data['section_name'],
		'class_id' => $exam_class_data['class_id'],
		'session_id' => $exam_class_data['session_id'],
//		'exam_type_id' => json_decode($exam_class_data['exam_type_id'], true),
		'section_id' => $exam_class_data['section_id'],
		'subject_marks_criteria' => json_decode($exam_class_data['subject_marks_criteria'], true),
	        );
	        $exam_class_return[] = $exam_class;
	    }
	}
	return $exam_class_return;
        }

        public function getClassSubjectMark(Request $request)
        {
	$session_id = Input::get('session_id');
	$class_id = Input::get('class_id');
	$section_id = Input::get('section_id');
	$exam_id = Input::get('exam_id');
	$exam_class_id = Input::get('exam_class_id');
	$arr_subject_detail = [];
	$arr_exam_type = [];
	$data = [];
	$return_data = [];
	$message = '';
	$status = 'failed';
	if (!empty($class_id))
	{
	    $arr_subject_data = DB::table('class_subjects as cs')
		->join('subjects as s', 's.subject_id', '=', 'cs.subject_id')
		->where('cs.session_id', $session_id)
		->where('cs.class_id', $class_id)
		->where('cs.section_id', $section_id)
		->orderBy('cs.class_subject_order')
		->select('s.subject_id', 's.subject_name', 's.subject_code', 's.root_id', 'cs.class_subject_id'
//                        ,DB::raw('FIND_IN_SET')
//                        ,DB::raw('(SELECT COUNT(*) FROM subjects as sb WHERE sb.root_id = s.subject_id AND sb.subject_id IN(SELECT cs.subject_id FROM class_subjects) AND cs.subject_id = sb.subject_id) AS sub_count')
		)
//                    ->having('sub_count','=', 0)
//                    ->whereNotIn('s.root_id', function ($query) {
//                    $query->select('cs.subject_id')->from('class_subjects');
//    })
		->get();
	    if (!empty($arr_subject_data))
	    {
	        // get unique id of all subjects
	        $arr_subject_data = json_decode($arr_subject_data, true);
	        $arr_root_id = array_unique(array_column($arr_subject_data, 'root_id'));

	        $exam_class_subject_marks = [];
	        $exam_class_subject = [];
	        $exam_class = $this->getExamClassData($exam_id, $exam_class_id, $class_id, $section_id);
	        $exam_class_data = isset($exam_class[0]) ? $exam_class[0] : [];
	        if ((!empty($exam_class_id) && !empty($exam_class_data)) || (empty($exam_class_id) && empty($exam_class)))
	        {
		if (isset($exam_class_data['subject_marks_criteria']))
		{
		    $exam_class_subject = $exam_class_data['subject_marks_criteria'];
		}
		foreach ($arr_subject_data as $key => $subject)
		{
		    if (!in_array($subject['subject_id'], $arr_root_id))
		    {
		        $arr_subject_type = [];
		        if (isset($exam_class_subject[$subject['subject_id']]))
		        {
			$exam_subject_type = $exam_class_subject[$subject['subject_id']];
			unset($exam_subject_type['total_max_marks']);
			$arr_subject_type = array_keys($exam_subject_type);
		        }

		        $exam_class_subject_marks = isset($exam_class_subject[$subject['subject_id']]) ? $exam_class_subject[$subject['subject_id']] : [];
		        $arr_subject_detail[] = array(
			'class_id' => $class_id,
			'subject_id' => $subject['subject_id'],
			'subject_name' => $subject['subject_name'],
			'subject_type_id' => $arr_subject_type,
			'subject_code' => $subject['subject_code'],
			'subject_marks' => $exam_class_subject_marks,
		        );
//		        }
		    }
		}
//		
		$arr_exam_type_data = ExamType::select('exam_type', 'exam_type_id')
		        ->where('exam_type_status', 1)
		        ->get();
		$arr_exam_type_data = json_decode($arr_exam_type_data, true);
		$arr_exam_type = array_column($arr_exam_type_data, NULL, 'exam_type_id');
//		    
		$status = 'success';
		$data = array(
		    'arr_subject' => $arr_subject_detail,
		    'arr_exam_type' => $arr_exam_type,
		);
		$return_data = View::make('backend.exam-class.subject-marks')->with($data)->render();
	        } elseif (empty($exam_class_id) && !empty($exam_class))
	        {
		$message = 'Mapping had already done for selected data, So please  <b><a href="' . url('admin/exam-class/' . $exam_class_data['exam_id'] . '/' . get_encrypted_value($exam_class_data['exam_class_id'], true)) . '" style="color:blue !important;">click here</a></b> to edit it.';
	        } else
	        {
		$message = 'Somthing went wrong!';
	        }
	    } else
	    {
	        $message = 'No subjects found for selected data.';
	    }
	}
	return response()->json(array('status' => $status, 'data' => $return_data, 'message' => $message));
        }
    }
    