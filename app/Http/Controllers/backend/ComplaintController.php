<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;
    use App\Model\backend\Complaint;
    use App\Model\backend\ComplaintDetail;
    use Form;
    use Auth;

    class ComplaintController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index()
        {
            $data = array();
            return view('backend.complaint.index')->with($data);
        }

        public function anyData()
        {
            $complaint  = [];
            $session    = get_current_session();
            $arr_status = \Config::get('custom.complaint_status');
            if (!empty($session))
            {
                $session_id    = $session['session_id'];
                $offset        = Input::get('start');
                $limit         = Input::get('length');
                $arr_complaint = DB::table('complaints as co')
                        ->join('students as s', 's.student_id', '=', 'co.student_id')
                        ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
                        ->select('co.*', 's.student_id', 's.enrollment_number', 'sp.father_contact_number', 's.first_name', 's.middle_name', 's.last_name', 'sp.father_first_name', 'sp.father_middle_name', 'sp.father_last_name')
                        ->where('s.student_status', 1)
                        ->where('co.session_id', $session_id)
                        ->orderBy('co.created_at', 'DESC')
//                        ->skip($offset)->take($limit)
                    ->get();
                foreach ($arr_complaint as $key => $complaint_data)
                {
                    $complaint_data                 = (array) $complaint_data;
                    $complaint_data['status_name']  = $arr_status[$complaint_data['status']];
                    $complaint_data['student_name'] = $complaint_data['first_name'] . ' ' . $complaint_data['middle_name'] . ' ' . $complaint_data['last_name'];
                    $complaint_data['father_name']  = $complaint_data['father_first_name'] . ' ' . $complaint_data['father_middle_name'] . ' ' . $complaint_data['father_last_name'];
                    $complaint[$key]                = (object) $complaint_data;
                }
            }
            return Datatables::of($complaint)
                    ->addColumn('update_status', function ($complaint)
                    {
                        return Form::select('status', \Config::get('custom.complaint_status'), $complaint->status, ['class' => 'complaint_status', 'id' => $complaint->complaint_id]);
                    })
                    ->addColumn('action', function ($complaint)
                    {
                        $encrypted_complaint_id = get_encrypted_value($complaint->complaint_id, true);
                        return '<a title="View Detail" href="' . url('admin/complaint-detail/' . $encrypted_complaint_id) . '" class="btn btn-info prinr_r centerAlign"  target="_blank" style="text-align:center;"><i class="fa fa-eye" ></i></a>';
                    })
                    ->rawColumns(['update_status', 'action'])->make(true);
        }

        public function getComplaintDetail(Request $request, $id = null)
        {
            if (!empty($id))
            {
                $decrypted_complaint_id = get_decrypted_value($id, true);
                $complaint              = Complaint::orderBy('created_at', 'ASC')->where('complaint_id', $decrypted_complaint_id)
                        ->with(['Student' => function($query)
                            {
                                $query->select('student_id', 'enrollment_number', 'first_name', 'middle_name', 'last_name');
                            }])->with(['Student.getParent' => function($query)
                            {
                                $query->select('student_parent_id', 'father_contact_number', 'father_first_name', 'father_middle_name', 'father_last_name', DB::raw("CONCAT(`father_first_name`, ' ', `father_last_name`) as father_name"));
                            }])->with('complaintDetail')
                        ->select('complaint_id', 'created_at', 'student_id', 'heading', 'description')->first();
                if (!$complaint)
                {
                    return redirect('admin/complaint')->withError('Complaint not found!');
                }
                $save_url = url('admin/complaint-detail/save/');
            }
            $data = array(
                'save_url'     => $save_url,
                'complaint'    => $complaint,
                'logged_user'  => Auth::guard('admin')->user(),
                'redirect_url' => url('admin/complaint'),
            );
            return view('backend.complaint.add')->with($data);
        }

        public function saveComplaintChat(Request $request)
        {
            $complaint_id  = Input::get('complaint_id');
            $message       = Input::get('message');
            $admin_user    = Auth::guard('admin')->user();
            $admin_user_id = $admin_user['admin_user_id'];
            $status        = 'failed';
            if (!empty($admin_user_id))
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $complaint_detail                = new ComplaintDetail;
                    $complaint_detail->complaint_id  = $complaint_id;
                    $complaint_detail->message       = $message;
                    $complaint_detail->admin_user_id = $admin_user_id;
                    $complaint_detail->save();
                    $status                          = 'success';
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withInput()->withErrors($error_message);
                }

                DB::commit();
            }
            return response()->json(array('status' => $status));
        }

        public function saveComplaintStatus(Request $request, $id, $status)
        {
            $r_status = 'failed';
            if (!empty($id))
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    Complaint::where('complaint_id', $id)->update(['status' => $status]);
                    $r_status = 'success';
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withInput()->withErrors($error_message);
                }

                DB::commit();
            }
            return response()->json(array('status' => $r_status));
        }

    }
    