<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\School;
    use App\Model\backend\StudentType;
    use Symfony\Component\HttpFoundation\File\File;
    use Validator;
    use Auth;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class StudentTypeController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index()
        {
            $data = array(
                'page_title' => trans('language.list_student_type'),
            );
            return view('backend.student-type.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {

            $student_type = [];
            $data         = [];
            if (!empty($id))
            {
                $student_type = StudentType::Find($id);
                if (!$student_type)
                {
                    return redirect('admin/student-type')->withError('Student Type not found!');
                }
                $page_title    = 'Edit Student Type';
                $save_url      = url('admin/student-type/save/' . $student_type->student_type_id);
                $submit_button = 'Update';
            }
            else
            {
                $page_title    = 'Create Student Type';
                $save_url      = url('admin/student-type/save');
                $submit_button = 'Save';
            }

            $data = array(
                'page_title'    => $page_title,
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'student_type'  => $student_type,
            );
            return view('backend.student-type.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            if (!empty($id))
            {
                $student_type = StudentType::find($id);

                if (!$student_type)
                {
                    return redirect('/admin/student-type/')->withError('Student Type not found!');
                }
                $success_msg = 'Student Type updated successfully!';
            }
            else
            {
                $student_type = New StudentType;
                $success_msg  = 'Student Type saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'student_type' => 'required|unique:student_types,student_type,' . $id . ',student_type_id,school_id,' . $school_id,
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $student_type->student_type = Input::get('student_type');
                    $student_type->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/student-type')->withSuccess($success_msg);
        }

        public function anyData()
        {
            $student_type = StudentType::orderBy('student_type', 'ASC')->get();
            return Datatables::of($student_type)
                    ->addColumn('action', function ($student_type)
                    {
                        return '<a href="student-type/add/' . $student_type->student_type_id . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>'
                            . ' <button id="' . $student_type->student_type_id . '" class="btn btn-xs btn-danger delete-button"><i class="glyphicon glyphicon-remove"></i>Delete</button>';
                    })->make(true);
        }

    }
    