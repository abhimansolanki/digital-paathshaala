<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\api\ApiController;
use App\Model\backend\ChapterTopic;
use App\Model\backend\ClassSubject;
use App\Model\backend\Subject;
use App\Model\backend\SubjectChapter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Input;
use Datatables;
use Illuminate\Support\Facades\View as View;
use Illuminate\Support\Facades\DB;

class EBookController extends Controller
{
    //
    public function __construct()
    {

    }

    /*
     * Show the fee list
     */
    public function index()
    {
        $session = db_current_session();
        $data = array(
            'redirect_url' => url('admin/e-class-room/add'),
            'session' => $session,
            'arr_session' => add_blank_option(get_session(), '-- Select  --'),
            'arr_class' => add_blank_option(get_all_classes(), '-- Select  --'),
        );
        return view('backend.e-book.index')->with($data);
    }

    public function save(Request $request, $id = NULL)
    {
        $user_type_id = null;
        $decrypted_fee_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $fee = Fee::find($decrypted_fee_id);
            if (!$fee) {
                return redirect('/admin/e-class-room/')->withError('Fee not found!');
            }
            $success_msg = 'Fee updated successfully!';
        } else {
            $fee = new Fee;
            $success_msg = 'Fee saved successfully!';
        }

        $session_id = Input::get('session_id');
        $class_id = Input::get('class_id');
        $arr_input = [
            'session_id' => 'required',
            'fee_circular_id' => 'required',
            'fine_date' => 'required',
            'apply_to' => 'required',
        ];
        if (!empty($class_id)) {
            $arr_input['fee_type_id'] = 'required|unique:fees,fee_type_id,' . $decrypted_fee_id . ',fee_id,session_id,' . $session_id . ',class_id,' . $class_id;
        } else {
            $arr_input['fee_type_id'] = 'required|unique:fees,fee_type_id,' . $decrypted_fee_id . ',fee_id,session_id,' . $session_id;
        }
        $validatior = Validator::make($request->all(), $arr_input);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction(); //Start transaction!
            try {
                $arr_fee_amount = $request->has('fee_amount') ? Input::get('fee_amount') : [];
                // insert into fee table
                $fee->class_id = Input::get('class_id');
                $fee->session_id = Input::get('session_id');
                $fee->fee_type_id = Input::get('fee_type_id');
                $fee->fee_circular_id = Input::get('fee_circular_id');
                $fee->apply_to = Input::get('apply_to');
                $fee->fine_amount = Input::get('fine_amount');
                $fee->fine_type = Input::get('fine_type');
                $fee->student_type_id = Input::get('student_type_id');
                $fee->total_fee = array_sum($arr_fee_amount);
                $fee->save();

                // insert into fee detail table
                $arr_sub_circular = Input::get('fee_sub_circular') ? Input::get('fee_sub_circular') : [];
                $arr_start_month = $request->has('start_month') ? Input::get('start_month') : [];
                $arr_end_month = $request->has('end_month') ? Input::get('end_month') : [];
                $arr_fine_date = $request->has('fine_date') ? Input::get('fine_date') : [];
                $arr_grace_period = $request->has('grace_period') ? Input::get('grace_period') : [];
                $arr_fee_detail_id = $request->has('fee_detail_id') ? Input::get('fee_detail_id') : [];
                if (!empty($arr_sub_circular)) {
                    foreach ($arr_sub_circular as $key => $sub_circular) {
                        $fee_detail_id = $arr_fee_detail_id[$key];
                        if ($request->has('circular_edit') && Input::get('circular_edit') == 'not_allowed') {
                            $fee_detail = FeeDetail::find($fee_detail_id);
                        } else {
                            $fee_detail = new FeeDetail;
                        }

                        $fee_detail->fee_sub_circular = $sub_circular;
                        if (isset($arr_start_month[$key])) {
                            $fee_detail->start_month = $arr_start_month[$key];
                        }
                        if (isset($arr_end_month[$key])) {
                            $fee_detail->end_month = $arr_end_month[$key];
                        }
                        if (isset($arr_fee_amount[$key])) {
                            $fee_detail->fee_amount = $arr_fee_amount[$key];
                        }
                        if (isset($arr_grace_period[$key])) {
                            $fee_detail->grace_period = $arr_grace_period[$key];
                        }
                        if (isset($arr_fine_date[$key]) && !empty($arr_fine_date[$key])) {
                            $fee_detail->fine_date = get_formatted_date($arr_fine_date[$key], 'database');
                        }
                        $save_fee_detail[] = $fee_detail;
                    }
                }
                /*
                 *  delete perious record and then insert new record in fee_details while editing fee master
                 */
                if (!empty($decrypted_fee_id) && ($request->has('circular_edit') && Input::get('circular_edit') == 'allowed')) {
                    $fee_detail = FeeDetail::where('fee_id', $decrypted_fee_id);
                    $fee_detail->delete();
                }
                $fee->feeDetail()->saveMany($save_fee_detail);
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
//                    p($error_message);
                return redirect()->back()->withInput()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin/e-class-room')->withSuccess($success_msg);
    }

    public function destroy(Request $request)
    {
        $fee_id = Input::get('fee_id');
        $fee = Fee::find($fee_id);
        if ($fee) {
            DB::beginTransaction(); //Start transaction!
            try {
                $fee->delete();
                $return_arr = array(
                    'status' => 'success',
                    'message' => 'Fee deleted successfully!'
                );
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                $return_arr = array(
                    'status' => 'used',
                    'message' => trans('language.delete_message')
                );
            }
            DB::commit();
        } else {
            $return_arr = array(
                'status' => 'error',
                'message' => 'Fee not found!'
            );
        }
        return response()->json($return_arr);
    }

    public function getChapters(Request $request)
    {
        try {
            $subject_chapters = SubjectChapter::with('ChapterTopic')->where([['session_id', '=', $request->session_id], ['class_id', '=', $request->class_id], ['section_id', '=', $request->section_id], ['subject_id', '=', $request->subject_id]])->latest('subject_chapter_id')->get();
            $data['subject_chapters'] = $subject_chapters;
            $result = View::make('backend.e-book.subject-chapters')->with($data)->render();
            $message = 'Data fetch successfully.';
            return $result;
            return json_encode(array('status' => 'success', 'message' => $message, 'data' => $result));
        } catch (\Exception $e) {
            return json_encode(array('status' => 'error', 'message' => $e->getMessage()));
        }
    }

    public function getClassSubjects(Request $request)
    {
        $class_id = Input::get('class_id');
        $session_id = Input::get('session_id');
        $section_id = Input::get('section_id');
        $arr_subject = [];
        $status = 'failed';

        $api_obj = new ApiController();
        $arr_subject_data = $api_obj->get_class_section_subject($session_id, $class_id, $section_id);
//        $arr_subject_data = Subject::
//        where('root_id', 0)
//            ->whereHas('subjectClass', function ($query) use ($class_id, $section_id, $session_id) {
//                $query->where('class_id', $class_id)
//                    ->where('section_id', $section_id)
//                    ->where('session_id', $session_id);
//            })->get();

        if (!empty($arr_subject_data)) {
            foreach ($arr_subject_data as $key => $subject) {
                $arr_subject[$subject['subject_id']] = $subject['subject_name'];
            }
            $status = 'success';
        }
        $return_data = array(
            'status' => $status,
            'arr_subject' => $arr_subject,
        );
        return response()->json($return_data);
    }

    public function addChapter(Request $request)
    {
        DB::beginTransaction(); //Start transaction!
        try {
            $chapter_id = $request->chapter_id;
            if ($chapter_id != '') {
                $chapter = SubjectChapter::find($chapter_id);
                if(!empty($chapter)){
                    $chapter->name = $request->chapter_name;
                    $chapter->save();
                    $return_arr = array(
                        'status' => 'success',
                        'message' => 'Chapter successfully updated!'
                    );
                }else{
                    $return_arr = array(
                        'status' => 'error',
                        'message' => 'Chapter not found!'
                    );
                }
            } else {
                $chapter = new SubjectChapter;
                $chapter->name = $request->chapter_name;
                $chapter->session_id = $request->session_id;
                $chapter->class_id = $request->class_id;
                $chapter->section_id = $request->section_id;
                $chapter->subject_id = $request->subject_id;
                $chapter->save();
                $return_arr = array(
                    'status' => 'success',
                    'message' => 'Chapter successfully created!'
                );
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $return_arr = array(
                'status' => 'used',
                'message' => $e->getMessage()
            );
        }
        return response()->json($return_arr);
    }

    public function deleteChapter(Request $request)
    {
        DB::beginTransaction(); //Start transaction!
        try {
            $chapter_id = $request->chapter_id;
            if ($chapter_id != '') {
                $chapter = SubjectChapter::find($chapter_id);
                if(!empty($chapter)){
                    $chapter->delete();
                    $return_arr = array(
                        'status' => 'success',
                        'message' => 'Chapter successfully deleted!'
                    );
                }else{
                    $return_arr = array(
                        'status' => 'error',
                        'message' => 'Chapter not found!'
                    );
                }
            } else {
                $return_arr = array(
                    'status' => 'error',
                    'message' => 'Chapter not found!'
                );
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $return_arr = array(
                'status' => 'used',
                'message' => $e->getMessage()
            );
        }
        return response()->json($return_arr);
    }

    public function addTopic(Request $request)
    {
        DB::beginTransaction(); //Start transaction!
        try {
            $topic_id = $request->topic_id;
            if ($topic_id != '') {
                $topic = ChapterTopic::find($topic_id);
                if(!empty($topic)){
                    $topic->name = $request->topic_title;
                    $topic->video_link = $request->video_link;
                    if($request->hasFile('image_file')){
                        $file                 = $request->file('image_file');
                        $config_upload_path   = \Config::get('custom.eclass_room');
                        $destinationPath      = public_path() . $config_upload_path['upload_path'];
                        $filename             = $file->getClientOriginalName();
                        $rand_string          = quick_random();
                        $filename             = $rand_string . '-' . $filename;
                        $file->move($destinationPath, $filename);
                        $topic->image_file = $filename;
                    }
                    $topic->download_permission = ($request->download_permission == 'on') ? 1 : 2;
                    $topic->reading_text = $request->reading_text;
                    $topic->save();
                    $return_arr = array(
                        'status' => 'success',
                        'message' => 'Topic successfully updated!'
                    );
                }else{
                    $return_arr = array(
                        'status' => 'error',
                        'message' => 'Topic not found!'
                    );
                }
            } else {
                $topic = new ChapterTopic;
                $topic->name = $request->topic_title;
                $topic->subject_chapter_id = $request->chapter_id;
                $topic->video_link = $request->video_link;
                if($request->hasFile('image_file')){
                    $file                 = $request->file('image_file');
                    $config_upload_path   = \Config::get('custom.eclass_room');
                    $destinationPath      = public_path() . $config_upload_path['upload_path'];
                    $filename             = $file->getClientOriginalName();
                    $rand_string          = quick_random();
                    $filename             = $rand_string . '-' . $filename;
                    $file->move($destinationPath, $filename);
                    $topic->image_file = $filename;
                }
                $topic->download_permission = ($request->download_permission == 'on') ? 1 : 2;
                $topic->reading_text = $request->reading_text;
                $topic->save();
                $return_arr = array(
                    'status' => 'success',
                    'message' => 'Topic successfully created!'
                );
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            if(isset($request->is_ajax) && $request->is_ajax == 1){
                return json_encode(array('result' => 'error','message' => $e->getMessage()));
            }else{
                return redirect()->back()->withErrors($e->getMessage());
            }
        }
        if(isset($request->is_ajax) && $request->is_ajax == 1){
            return json_encode(array('result' => 'success','message' =>$return_arr['message']));
        }else{
            return redirect()->back()->withSuccess($return_arr['message']);
        }
    }

    public function deleteTopic(Request $request)
    {
        DB::beginTransaction(); //Start transaction!
        try {
            $topic_id = $request->topic_id;
            if ($topic_id != '') {
                $topic = ChapterTopic::find($topic_id);
                if(!empty($topic)){
                    $topic->delete();
                    $return_arr = array(
                        'status' => 'success',
                        'message' => 'Topic successfully deleted!'
                    );
                }else{
                    $return_arr = array(
                        'status' => 'error',
                        'message' => 'Topic not found!'
                    );
                }
            } else {
                $return_arr = array(
                    'status' => 'error',
                    'message' => 'Topic not found!'
                );
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $return_arr = array(
                'status' => 'used',
                'message' => $e->getMessage()
            );
        }
        return response()->json($return_arr);
    }

    public function viewTopics(Request $request, $chapter_id)
    {
        $chapter = SubjectChapter::find($chapter_id);
        if(empty($chapter)){
            return redirect()->back()->withInput()->withErrors("Subject Chapter not found.");
        }
        $topics = $chapter->ChapterTopic;
        $data = array(
            'redirect_url' => url('admin/e-class-room'),
            'chapter' => $chapter,
            'topics' => $topics
        );
        return view('backend.e-book.chapter-topics')->with($data);
    }

    public function getTopic(Request $request)
    {
        $topic = ChapterTopic::find($request->topic_id);
        if(empty($topic)){
            return array('status' => 'failed', 'data' => "Chapter topic not found.");
        }
        return array('status' => 'success', 'data' => $topic);
    }

    public function getChapterTopics(Request $request)
    {
        try {
            $chapter_topics = ChapterTopic::where([['subject_chapter_id', '=', $request->chapter_id]])->latest('chapter_topic_id')->get();
            $data['chapter_topics'] = $chapter_topics;
            $result = View::make('backend.e-book.chapter-topics-data')->with($data)->render();
            $message = 'Data fetch successfully.';
            return $result;
            return json_encode(array('status' => 'success', 'message' => $message, 'data' => $result));
        } catch (\Exception $e) {
            return json_encode(array('status' => 'error', 'message' => $e->getMessage()));
        }
    }
}
    