<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Grade;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class GradeController extends Controller
    {

        public function __construct()
        {
            
        }

        public function add(Request $request, $id = NULL)
        {
            $grade    = [];
            $grade_id = null;
            if (!empty($id))
            {
                $decrypted_grade_id = get_decrypted_value($id, true);
                $grade              = $this->getGradeData($decrypted_grade_id);
                $grade              = isset($grade[0]) ? $grade[0] : [];
//                $grade              = (object) $grade;
                if (!$grade)
                {
                    return redirect('admin/grade')->withError('Grade not found!');
                }
                $encrypted_grade_id = get_encrypted_value($grade['grade_id'], true);
                $save_url           = url('admin/grade/save/' . $encrypted_grade_id);
                $submit_button      = 'Update';
                $grade_id           = $decrypted_grade_id;
            }
            else
            {
                $save_url      = url('admin/grade/save');
                $submit_button = 'Save';
            }
            $grade['arr_session'] = add_blank_option(get_session($switch               = 'no'), '--Select--');
            $data                 = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'grade'         => $grade,
                'redirect_url'  => url('admin/grade/'),
            );
            return view('backend.grade.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_grade_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $grade = Grade::find($decrypted_grade_id);

                if (!$grade)
                {
                    return redirect('/admin/grade/')->withError('Grade not found!');
                }
                $success_msg = 'Grade updated successfully!';
            }
            else
            {
                $grade       = New Grade;
                $success_msg = 'Grade saved successfully!';
            }
            $session_id = Input::get('session_id');
            $validatior = Validator::make($request->all(), [
                    'grade_name' => 'required|unique:grades,grade_name,' . $decrypted_grade_id . ',grade_id,session_id,'.$session_id,
                    'minimum'    => 'required',
                    'maximum'    => 'required',
                    'session_id' => 'required',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $grade->session_id  = $session_id;
                    $grade->grade_name  = Input::get('grade_name');
                    $grade->grade_alias = Input::get('grade_alias');
                    $grade->minimum     = Input::get('minimum');
                    $grade->maximum     = Input::get('maximum');
                    $grade->grade_comments     = Input::get('grade_comments');
                    $grade->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/grade')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $grade_id = Input::get('grade_id');
            $grade    = Grade::find($grade_id);
            if ($grade)
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $grade->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Grade deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Grade not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $grade     = [];
            $offset    = Input::get('start');
            $limit     = Input::get('length');
            $grade_id  = null;
            $arr_grade = $this->getGradeData($grade_id, $limit, $offset);
            foreach ($arr_grade as $key => $grade_data)
            {
                $grade[$key] = (object) $grade_data;
            }
            return Datatables::of($grade)
                    ->addColumn('action', function ($grade)
                    {
                        $encrypted_grade_id = get_encrypted_value($grade->grade_id, true);
                        return '<a title="Edit" id="deletebtn1" href="' . url('admin/grade/' . $encrypted_grade_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $grade->grade_id . '"><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

        public function getGradeData($grade_id = null, $limit = null, $offset = null)
        {
            $grade_return   = [];
            $session = get_current_session();
            if(!empty($session['session_id']))
            {
            $arr_grade_data = Grade::where('session_id',$session['session_id'])
                ->where('grade_status',1)
                ->where(function($query) use ($grade_id)
                    {
                        if (!empty($grade_id))
                        {
                            $query->where('grade_id', $grade_id);
                        }
                    })
//                    ->where(function($query) use ($limit, $offset)
//                    {
//                        if (!empty($limit))
//                        {
//                            $query->skip($offset);
//                            $query->take($limit);
//                        }
//                    })
                        ->get();
            if (!empty($arr_grade_data))
            {
                foreach ($arr_grade_data as $key => $grade_data)
                {
                    $grade_return[] = array(
                        'grade_id'    => $grade_data['grade_id'],
                        'session_id'    => $grade_data['session_id'],
                        'grade_name'  => $grade_data['grade_name'],
                        'grade_alias' => $grade_data['grade_alias'],
                        'minimum'     => $grade_data['minimum'],
                        'maximum'     => $grade_data['maximum'],
                        'grade_comments'     => $grade_data['grade_comments'],
                    );
                }
            }
            }
            return $grade_return;
        }

    }
    