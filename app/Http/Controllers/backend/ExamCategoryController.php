<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\ExamCategory;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class ExamCategoryController extends Controller
    {

        public function __construct()
        {
            
        }

        public function add(Request $request, $id = NULL)
        {
            $exam_category    = [];
            $exam_category_id = null;
            if (!empty($id))
            {
                $decrypted_exam_category_id = get_decrypted_value($id, true);
                $exam_category              = $this->getExamCategoryData($decrypted_exam_category_id);
                $exam_category              = isset($exam_category[0]) ? $exam_category[0] : [];

                if (!$exam_category)
                {
                    return redirect('admin/exam-category')->withError('Exam Category not found!');
                }
                $encrypted_exam_category_id = get_encrypted_value($exam_category['exam_category_id'], true);
                $save_url               = url('admin/exam-category/save/' . $encrypted_exam_category_id);
                $submit_button          = 'Update';
                $exam_category_id           = $decrypted_exam_category_id;
            }
            else
            {
                $save_url      = url('admin/exam-category/save');
                $submit_button = 'Save';
            }

            $data = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'exam_category'         => $exam_category,
            );
            return view('backend.exam-category.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_exam_category_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $exam_category = ExamCategory::find($decrypted_exam_category_id);

                if (!$exam_category)
                {
                    return redirect('/admin/exam-category/')->withError('Exam Category not found!');
                }
                $success_msg = 'Exam Category updated successfully!';
            }
            else
            {
                $exam_category   = New ExamCategory;
                $success_msg = 'Exam Category saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'exam_category' => 'required|unique:exam_categories,exam_category,' . $decrypted_exam_category_id . ',exam_category_id',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $exam_category->exam_category = Input::get('exam_category');
                    $exam_category->exam_category_alias = Input::get('exam_category_alias');
                    $exam_category->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/exam-category')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            
            $exam_category_id = Input::get('exam_category_id');
            $exam_category    = ExamCategory::find($exam_category_id);
            if ($exam_category)
            {
                DB::beginTransaction();
                 try
                {
                   $exam_category->delete();
                   $return_arr = array(
                    'status'  => 'success',
                    'message' => 'Exam Category deleted successfully!'
                );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr = array(
                    'status'  => 'used',
                    'message' =>trans('language.delete_message')
                );
                    
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Exam Category not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $exam_category = [];
            $arr_grade = $this->getExamCategoryData();
            foreach ($arr_grade as $key => $exam_category_data)
            {
                $exam_category[$key] = (object) $exam_category_data;
            }
            return Datatables::of($exam_category)
                    ->addColumn('action', function ($exam_category)
                    {
                        $encrypted_exam_category_id = get_encrypted_value($exam_category->exam_category_id, true);
                        return '<a title="Edit" id="deletebtn1" href="exam-category/' . $encrypted_exam_category_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $exam_category->exam_category_id . '"><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

        public function getExamCategoryData($exam_category_id = array())
        {
            $exam_category_return   = [];
            $arr_exam_category_data = ExamCategory::where(function($query) use ($exam_category_id)
                {
                    if (!empty($exam_category_id))
                    {
                        $query->where('exam_category_id', $exam_category_id);
                    }
                })->get();
            if (!empty($arr_exam_category_data))
            {
                foreach ($arr_exam_category_data as $key => $exam_category_data)
                {
                    $exam_category_return[] = array(
                        'exam_category_id' => $exam_category_data['exam_category_id'],
                        'exam_category'    => $exam_category_data['exam_category'],
                        'exam_category_alias'    => $exam_category_data['exam_category_alias'],
                    );
                }
            }
            return $exam_category_return;
        }

    }
    
