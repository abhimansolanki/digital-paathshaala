<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\StudentAttendance;
use App\Model\backend\Classes;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use DateTime;
use Datatables;
use DateInterval;
use DatePeriod;
use Illuminate\Support\Facades\View as View;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CronController;

class StudentAttendanceController extends Controller
{

    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = array(
            'redirect_url' => url('admin/student-attendance/add'),
            'page_title' => trans('language.list_school'),
        );
        return view('backend.student-attendance.index')->with($data);
    }

    public function add(Request $request, $id = NULL, $type = '')
    {
        $attendance = [];
        $class_id = null;
        $session_id = null;
        if (!empty($id)) {
            $decrypted_student_attendance_id = get_decrypted_value($id, true);
            $attendance = StudentAttendance::Find($decrypted_student_attendance_id);
            $class_id = $attendance['class_id'];
            $session_id = $attendance['session_id'];
            $section_id = $attendance['section_id'];
            $attendance['attendance_date'] = get_formatted_date($attendance['attendance_date'], 'display');
            if (!$attendance) {
                return redirect('admin/student-attendance/add')->withError('Student AttendancAttendanc not found!');
            }

            $encrypted_student_attendance_id = get_encrypted_value($attendance['student_attendance_id'], true);
            $page_title = 'Edit Student';
            $save_url = url('admin/student-attendance-save/' . $encrypted_student_attendance_id);
            $submit_button = 'Update';
            $arr_absent_student = json_decode($attendance['student_id'], true);
            if (!empty($arr_absent_student)) {
                $arr_absent_student = array_column($arr_absent_student, 'student_id');
            }
            $attendance['student_list'] = $this->studentData($session_id, $class_id, $section_id, $arr_absent_student);
        } else {
            $page_title = 'Create Student';
            $save_url = url('admin/student-attendance-save');
            $submit_button = 'Save';
        }
        $attendance['session'] = db_current_session(); // tart_date
        $arr_class = get_all_classes();
        $arr_section = get_class_section($class_id, $session_id);
        $attendance['arr_section'] = add_blank_option($arr_section, '--Select section --');
        $attendance['arr_class'] = add_blank_option($arr_class, '--Select class --');
        $data = array(
            'page_title' => $page_title,
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'attendance' => $attendance,
            'redirect_url' => url('admin/student-attendance-view'),
        );
//	p($data);
        return view('backend.student-attendance.add')->with($data);
    }

    public function save(Request $request, $id = NULL)
    {
        $attendance_attendance_id = null;
        $decrypted_student_attendance_id = null;
        if (!empty($id)) {
            $decrypted_student_attendance_id = get_decrypted_value($id, true);
            $student_attendance = StudentAttendance::find($decrypted_student_attendance_id);
            if (!$student_attendance) {
                return redirect('/admin/student-attendance/')->withError('Student Attendanc not found!');
            }
            $success_msg = 'Student Attendanc updated successfully!';
        } else {
            $student_attendance = new StudentAttendance;
            $success_msg = 'Student Attendanc saved successfully!';
        }
        $class_id = Input::get('class_id');
        $section_id = Input::get('section_id');
        $session_id = Input::get('session_id');
        $attendance_date = Input::get('attendance_date');
        $attendance_date = get_formatted_date($attendance_date, 'database');

        $validatior = Validator::make($request->all(), [
            'class_id' => 'required|unique:student_attendances,class_id,' . $decrypted_student_attendance_id . ',student_attendance_id,attendance_date,' . $attendance_date . ',section_id,' . $section_id . ',session_id,' . $session_id,
            'attendance_date' => 'required',
            'section_id' => 'required',
            'session_id' => 'required',
            'total_present' => 'required',
            'total_absent' => 'required',
        ]);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction(); //Start transaction!
            try {
                //inserting data in multiples tables
                $teacher_id = 1;
                $arr_student_id = Input::get('student_id');
                $attendance = [];
                //p($arr_student_id);
                if (!empty($arr_student_id)) {
                    foreach ($arr_student_id as $key => $student_id) {
                        if ($request->input('arr_student_id_' . $student_id) == 2) {
                            $attendance[] = array(
                                'student_id' => $student_id,
                                'status' => 1,
                            );
                        }
                    }
                }

                $total_absent = count($attendance);
                $total_present = count($arr_student_id) - $total_absent;
                $student_attendance->session_id = $session_id;
                $student_attendance->section_id = $section_id;
                $student_attendance->class_id = $class_id;
//	        $student_attendance->teacher_id = $teacher_id;
                //only absent student
                $student_attendance->student_id = json_encode($attendance);
                $student_attendance->attendance_date = $attendance_date;
                $student_attendance->total_present = $total_present;
                $student_attendance->total_absent = $total_absent;
                $student_attendance->attendance_status = 1;
                $student_attendance->save();
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                p($error_message);
                return redirect()->back()->withInput()->withErrors($error_message);
            }
            DB::commit();

            /**
             * Send notification to absent student's parents
             * Only add attendance type
             */
            if (!empty($attendance) && $id == null) {
                $cron = new CronController;
                $cron->pushNotificationAttendance(array_column($attendance, 'student_id'), $session_id, $student_attendance->student_attendance_id);
            }
        }
        return redirect('admin/student-attendance-view')->withSuccess($success_msg);
    }

    /*View attendance*/
    public function anyData(Request $request)
    {
        $attendance_date = get_formatted_date($request->attendance_date, 'database');
        $query = DB::table('student_attendances as att')
            ->join('classes as c', 'att.class_id', '=', 'c.class_id')
            ->join('sections as s', 'att.section_id', '=', 's.section_id')
            ->join('sessions as ses', 'att.session_id', '=', 'ses.session_id')
            ->where('ses.session_status', 1);
        if(!empty($attendance_date)){
            $query->where("attendance_date",$attendance_date);
        }
        $student_attendance = $query->select('att.*', 'c.class_name', 's.section_name')
            ->orderBy("attendance_date",'desc')
            ->get();
        return Datatables::of($student_attendance)
            ->addColumn('action', function ($student_attendance) {
                $encrypted_student_attendance_id = get_encrypted_value($student_attendance->student_attendance_id, true);
                return '<a title="Edit" id="deletebtn1" href="' . url('admin/student-attendance/add/' . $encrypted_student_attendance_id) . '" class="btn btn-success centerAlign"><i class="fa fa-edit" ></i></a>';
            })
            ->rawColumns(['action'])->make(true);
    }

    /*get student list based on class and section */
    public function studentTableData(Request $request)
    {
        $session_id = Input::get('session_id');
        $class_id = Input::get('class_id');
        $section_id = Input::get('section_id');

        $data = $this->studentData($session_id, $class_id, $section_id);
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    public function studentData($session_id, $class_id, $section_id, $arr_absent_student = [])
    {
        $data = '';
        if (!empty($session_id) && !empty($class_id) && !empty($section_id)) {
            $student['arr_student'] = DB::table('students as s')
                ->select('student_id', 'enrollment_number', "first_name", "middle_name", "last_name")->where('current_session_id', $session_id)
                ->where('current_class_id', $class_id)->where('current_section_id', $section_id)->where("student_left","No")->orderBy("first_name")->orderBy('middle_name','ASC')->orderBy('last_name','ASC')->get()->toArray();
            $student['absent_student_id'] = $arr_absent_student;
            $data = View::make('backend.student-attendance.student-list')->with($student)->render();
        }
        return $data;
    }


    /*Attendance Report*/
    public function studentAttendanceQuery($class_id, $section_id, $session_id, $start_date, $end_date)
    {
        $query = Classes:: where('class_id', $class_id)
            ->with(['classStudentAttendance' => function ($q) use ($session_id, $start_date, $end_date, $section_id) {
                $q->select('class_id', 'attendance_date', 'student_id', 'total_present', 'total_absent', 'on_leave');
                if (!empty($session_id)) {
                    $q->where('session_id', $session_id);
                }
                if (!empty($start_date)) {
                    $q->whereBetween('attendance_date', array($start_date, $end_date));
                }
                if (!empty($section_id)) {
                    $q->where('section_id', $section_id);
                }
            }])
            ->with(['classStudentLeave' => function ($q) use ($session_id, $start_date, $end_date, $class_id, $section_id) {
                $q->select('class_id', 'leave_start_date', 'leave_end_date', 'student_id', 'student_leave_id', 'leave_day');
                if (!empty($session_id)) {
                    $q->where('session_id', $session_id);
                }
                if (!empty($class_id)) {
                    $q->where('class_id', $class_id);
                }
                if (!empty($section_id)) {
                    $q->where('section_id', $section_id);
                }
                if (!empty($start_date)) {
                    $q->whereBetween('leave_start_date', array($start_date, $end_date));
                }
                if (!empty($start_date)) {
                    $q->whereBetween('leave_end_date', array($start_date, $end_date));
                }
            }])
            ->with(['getStudentClass.getParent' => function ($q) {
                $q->select('student_parent_id', 'father_first_name', 'father_middle_name', 'father_last_name', 'father_contact_number');
            }])
            ->with(['getStudentClass' => function ($q) use ($section_id) {
                if (!empty($section_id)) {
                    $q->where('current_section_id', $section_id);
                }
                $q->select('student_id', 'current_class_id', 'enrollment_number', 'first_name', 'middle_name', 'last_name', 'admission_date');
            }])
            ->select('class_id', 'class_name')->first();
        return $query;
    }

    // here current month start date become end date
    public function studentTotAttendance($class_id, $section_id, $session_id, $start_date, $end_date)
    {
        $arr_student_tot_atte = [];
        $arr_tot_attendance = $this->studentAttendanceQuery($class_id, $section_id, $session_id, $start_date, $end_date);
        $s_date = null;
        $till_last_month_calendar = $this->monthCalendar('upto_this_month', $s_date, $start_date, $end_date);
        if (isset($arr_tot_attendance['getStudentClass'])) {
            foreach ($arr_tot_attendance['classStudentAttendance'] as $key => $attendance) {
                // attendance data
                $arr_student_absent = json_decode(json_encode(json_decode($attendance['student_id'])), true);
                $arr_attendance_data[$attendance['attendance_date']] = array(
                    'attendance_date' => $attendance['attendance_date'],
                    'student_id' => !empty($arr_student_absent) ? array_column($arr_student_absent, 'student_id') : [],
                );
            }
            // leave data
            foreach ($arr_tot_attendance['classStudentLeave'] as $key => $leave) {
                $leave_day = $leave['leave_day'];
                $arr_leave_date[] = $leave['leave_start_date'];
                if ($leave_day > 1) {
                    for ($i = 1; $i <= $leave_day; $i++) {
                        array_push($arr_leave_date, date('Y-m-d', strtotime('+' . $i . ' day', strtotime($leave['leave_start_date']))));
                    }
                }
                $arr_leave_data[$leave['student_id']] = $arr_leave_date;
            }
            foreach ($arr_tot_attendance['getStudentClass'] as $key => $student) {
                $present_count = 0;
                $absence_count = 0;
                $leave_count = 0;
                foreach ($till_last_month_calendar as $key => $moth_date) {
                    $status = '';
                    if (empty($moth_date['holiday'])) // No holiday
                    {   // on leave
                        if (isset($arr_leave_data[$student['student_id']]) && in_array($moth_date['date'], $arr_leave_data[$student['student_id']])) {
                            $leave_count++;
                        } else {
                            $attendance_info = isset($arr_attendance_data[$moth_date['date']]) ? $arr_attendance_data[$moth_date['date']] : [];
                            if (!empty($attendance_info)) {
                                $arr_student_absent = $attendance_info['student_id'];
                                if (in_array($student['student_id'], $arr_student_absent)) {
                                    $absence_count++;
                                } else {
                                    $present_count++;
                                }
                            }
                        }
                    }
                }
                $att_detail = array(
                    'present' => $present_count,
                    'absent' => $absence_count,
                    'leave' => $leave_count,
                );
                $arr_student_tot_atte[$student['student_id']] = $att_detail;
            }
        }
        return $arr_student_tot_atte;
    }

    public function getStudentAttendance($view_type, $class_id, $section_id, $s_date)
    {
        $session = get_current_session();
        $arr_student = [];
        $return = [];
        $session_id = $session['session_id'];
        if ($view_type == 'student-wise') {
            $model = get_student_table($session_id, 'model');
            $arr_student_data = $model->where('current_class_id', $class_id)
                ->where('current_section_id', $section_id)
                ->where('current_session_id', $session_id)
                ->with('getParent')->get();
            $serial_no = 1;
            foreach ($arr_student_data as $key => $student) {
                $student_detail = array(
                    'student_id' => $student['student_id'],
                    'serial_no' => $serial_no,
                    'enrollment_number' => $student['enrollment_number'],
                    'student_name' => $student['first_name'] . ' ' . $student['middle_name'] . ' ' . $student['last_name'],
                    'father_name' => $student['getParent']['father_first_name'] . ' ' . $student['getParent']['father_middle_name'] . ' ' . $student['getParent']['father_last_name'],
                );
                $serial_no++;
                $return[$student['student_id']] = (object)$student_detail;
            }
        } else {
            $month_calendar = $this->monthCalendar($view_type, $s_date);
            $first = reset($month_calendar); // first date of month
            $end = end($month_calendar); // end date of month
            $start_date = $first['date'];
            $end_date = $end['date'];
            $arr_attendance_data = [];
            $arr_leave_data = [];
            // get attendance data
            $arr_attendance = $this->studentAttendanceQuery($class_id, $section_id, $session_id, $start_date, $end_date);
            if (isset($arr_attendance['getStudentClass'])) {
                $tot_att_start_date = $session['start_date'];
                $e_date = new DateTime($start_date);
                $e_date->modify("last day of previous month");
                $tot_att_end_date = $e_date->format('Y-m-d');
                $arr_att_upto_this_month = $this->studentTotAttendance($class_id, $section_id, $session_id, $tot_att_start_date, $tot_att_end_date);
                $arr_attendance_symbol = \Config::get('custom.attendance_symbol');
                foreach ($arr_attendance['classStudentAttendance'] as $key => $attendance) {
                    // attendance data
                    $arr_student_absent = json_decode(json_encode(json_decode($attendance['student_id'])), true);
                    $arr_attendance_data[$attendance['attendance_date']] = array(
                        'attendance_date' => $attendance['attendance_date'],
                        'student_id' => !empty($arr_student_absent) ? array_column($arr_student_absent, 'student_id') : [],
                        'total_present' => $attendance['total_present'],
                        'total_absent' => $attendance['total_absent'],
                        'on_leave' => $attendance['on_leave'],
                        'total_student' => $attendance['total_present'] + $attendance['total_absent'],
                    );
                }
                // leave data
                foreach ($arr_attendance['classStudentLeave'] as $key => $leave) {
                    $leave_day = $leave['leave_day'];
                    $arr_leave_date[] = $leave['leave_start_date'];
                    if ($leave_day > 1) {
                        for ($i = 1; $i <= $leave_day; $i++) {
                            array_push($arr_leave_date, date('Y-m-d', strtotime('+' . $i . ' day', strtotime($leave['leave_start_date']))));
                        }
                    }
                    $arr_leave_data[$leave['student_id']] = $arr_leave_date;
                }
                $serial_no = 1;
                foreach ($arr_attendance['getStudentClass'] as $key => $student) {
                    $student_detail = array(
                        'student_id' => $student['student_id'],
                        'admission_date' => $student['admission_date'],
                        'serial_no' => $serial_no,
                        'enrollment_number' => $student['enrollment_number'],
                        'student_name' => $student['first_name'] . ' ' . $student['middle_name'] . ' ' . $student['last_name']
                    );

                    $present_count = 0;
                    $absence_count = 0;
                    $leave_count = 0;
                    foreach ($month_calendar as $key => $moth_date) {
                        $status = '';
                        // holiday
                        if (!empty($moth_date['holiday'])) {
                            if ($moth_date['val'] == 'Sun') {
                                $status = $arr_attendance_symbol['w'];
                            } else {
                                $status = $arr_attendance_symbol['h'];
                            }
                        } // on leave
                        elseif (isset($arr_leave_data[$student_detail['student_id']]) && in_array($moth_date['date'], $arr_leave_data[$student_detail['student_id']])) {
                            $status = $arr_attendance_symbol['l'];
                            $leave_count++;
                        } elseif ($moth_date['date'] < $student_detail['admission_date']) {
                            $status = '-';
                        } else {
                            $attendance_info = isset($arr_attendance_data[$moth_date['date']]) ? $arr_attendance_data[$moth_date['date']] : [];
                            if (!empty($attendance_info)) {
                                $arr_student_absent = $attendance_info['student_id'];
                                if (in_array($student_detail['student_id'], $arr_student_absent)) {
                                    $status = $arr_attendance_symbol['a'];
                                    $absence_count++;
                                } else {
                                    $status = $arr_attendance_symbol['p'];
                                    $present_count++;
                                }
                            } else {
                                $status = $arr_attendance_symbol['nar'];
                            }
                        }
                        $date = new DateTime($moth_date['date']);
                        // here d is day of date
                        $attendance_data[$date->format('d')] = $status;
                    }

                    $student_attendance = $student_detail + $attendance_data;
                    $att_detail = array(
                        'present' => $present_count,
                        'absent' => $absence_count,
                        'leave' => $leave_count,
                    );
                    $upto_this_present = 0;
                    $upto_this_absent = 0;
                    $upto_this_leave = 0;
                    $upto_this_attendance = isset($arr_att_upto_this_month[$student['student_id']]) ? $arr_att_upto_this_month[$student['student_id']] : [];
                    if (!empty($upto_this_attendance)) {
                        $upto_this_present = $upto_this_attendance['present'];
                        $upto_this_absent = $upto_this_attendance['absent'];
                        $upto_this_leave = $upto_this_attendance['leave'];
                    }
                    $student_attendance['attendance_this'] = $present_count;
                    $student_attendance['attendance_upto_this'] = $upto_this_present;
                    $student_attendance['attendance_tot'] = $present_count + $upto_this_present;
                    $student_attendance['absence_this'] = $absence_count;
                    $student_attendance['absence_upto_this'] = $upto_this_absent;
                    $student_attendance['absence_tot'] = $absence_count + $upto_this_absent;
                    $student_attendance['leave_this'] = $leave_count;
                    $student_attendance['leave_upto_this'] = $upto_this_leave;
                    $student_attendance['leave_tot'] = $leave_count + $upto_this_leave;

                    $arr_student[$student['student_id']] = $student_attendance;
                    $serial_no++;
                }
//	        // extra rows and columns for table summary and shown at the end of table 
                $arr_col_name = $this->tableHeaderCol($view_type, $s_date);
                $arr_extra_row = array(
                    'total_present' => 'Tot. Attendance',
                    'total_absent' => 'Tot. Absence',
                    'on_leave' => 'Tot. Leave',
                    'total_student' => "Tot. Scholars",
                );
                $extra_rows_col = [];
                $arr_attendance_sum = [];
                foreach ($arr_extra_row as $row_key => $row_value) {
                    foreach ($arr_col_name as $col_key => $col_value) {
                        if (is_numeric($col_value['key'])) {
                            if (!empty($col_value['holiday'])) {
                                $extra_rows_col[$col_value['key']] = '';
                            } else {
                                $data_atte_summary = isset($arr_attendance_data[$col_value['date']]) ? $arr_attendance_data[$col_value['date']] : [];
                                if (!empty($data_atte_summary)) {
                                    $extra_rows_col[$col_value['key']] = $data_atte_summary[$row_key];
                                } else {
                                    $extra_rows_col[$col_value['key']] = '';
                                }
                            }
                        }
                    }

                    $arr_attendance_sum[$row_key] = $extra_rows_col;
                }
                $return['arr_attendance'] = $arr_student;
                $return['att_summary'] = $arr_attendance_sum;
            }
        }
//	p($return);
        return $return;
    }

    public function monthCalendar($view_type = 'month', $s_date = null, $c_start_date = null, $c_end_date = null, $with_holiday = 'yes')
    {
        $arr_months_date = [];
        $session = get_current_session();
        $selected_date = !empty($s_date) ? $s_date : $session['attendance_date'];
        $d = new DateTime($selected_date);
        if ($view_type == 'week') {
            $start_date = $d->modify('6 days ago');
            $start_date = $start_date->format('Y-m-d');
            $d_end = new DateTime($selected_date);
//                    $end_date   = $d_end->modify('previous day');
            $end_date = $d_end->format('Y-m-d');
        } elseif ($view_type == 'month') {
            $start_date = $d->format('Y-m-01');
            $end_date = $d->format('Y-m-t');
        } else {
            $start_date = $c_start_date; // equal to session start date
            $end_date = $c_end_date; // equal to last month end date
        }
        $begin = new DateTime($start_date);
        $end = new DateTime($end_date);
        $end = $end->modify('+1 day');
        $interval_day = new DateInterval('P1D');
        $arr_date_range = new DatePeriod($begin, $interval_day, $end);
        $arr_school_holiday = get_sheduled_school_holidays();
        foreach ($arr_date_range as $date) {
            $holiday_detail = [];
            if ($date->format('D') == 'Sun') {
                $holiday_detail = array('name' => 'Sunday');
            } else {
                foreach ($arr_school_holiday['holiday_details'] as $holiday) {
                    if (in_array($date->format('Y-m-d'), $holiday['holiday_period'])) {
                        $holiday_detail = array(
                            'name' => $holiday['holiday_name']
                        );
                    }
                }
            }
            if ($with_holiday == 'yes') {
                $arr_months_date[] = array(
                    'key' => $date->format("d"),
                    'val' => $date->format('D'),
                    'date' => $date->format('Y-m-d'),
                    'holiday' => $holiday_detail,
                );
            } else {
                if (empty($holiday_detail)) {
                    $arr_months_date[] = array(
                        'key' => $date->format("d"),
                        'val' => $date->format('D'),
                        'date' => $date->format('Y-m-d'),
//                    'holiday' => $holiday_detail,
                    );
                }
            }
        }
        return $arr_months_date;
    }

    public function tableHeaderCol($view_type = 'month', $c_date = null)
    {
        $arr_col1[0] = array('key' => 'student_id', 'val' => '');
        $arr_col1[1] = array('key' => 'serial_no', 'val' => 'S.No');
        $arr_col1[2] = array('key' => 'enrollment_number', 'val' => 'SR.No');
        $arr_col1[3] = array('key' => 'student_name', 'val' => ' Student Name ');

        if ($view_type == 'student-wise') {
            $arr_col1[4] = array('key' => 'father_name', 'val' => 'Father Name');
            $arr_col1[5] = array('key' => 'action', 'val' => 'Action');
            $arr_col2 = [];
            $arr_col3 = [];
        } else {
            $arr_col2 = $this->monthCalendar($view_type, $c_date);
            $arr_col3[0] = array('key' => 'attendance', 'val' => 'Attendance', 'child' =>
                [array('key' => 'this', 'val' => 'In this month'),
                    array('key' => 'upto_this', 'val' => 'Up to this month'),
                    array('key' => 'tot', 'val' => 'Total')]
            );
            $arr_col3[1] = array('key' => 'absence', 'val' => 'Absence', 'child' =>
                [array('key' => 'this', 'val' => 'In this month'),
                    array('key' => 'upto_this', 'val' => 'Up to this month'),
                    array('key' => 'tot', 'val' => 'Total')]
            );
            $arr_col3[2] = array('key' => 'leave', 'val' => 'Leave', 'child' =>
                [array('key' => 'this', 'val' => 'In this month'),
                    array('key' => 'upto_this', 'val' => 'Up to this month'),
                    array('key' => 'tot', 'val' => 'Total')]
            );
        }

        $arr_col = array_merge($arr_col1, $arr_col2, $arr_col3);
        return $arr_col;
    }


    public function attendanceRegister($view_type = '')
    {
        $data = [];
        $session = get_current_session();
        if ($session) {
            $date = new DateTime($session['start_date']);
            $start_date = $date->format('D F d, Y');
            $date1 = new DateTime($session['end_date']);
            $end_date = $date1->format('D F d, Y');
            $session['start_date'] = $start_date;
            $session['end_date'] = $end_date;
            $class_id = 1;
            $table_view = array(
                'month' => 'Monthly',
                'week' => 'Weekly',
                'student-wise' => 'Student wise',
            );
            $data = array(
                'arr_table_view' => add_blank_option($table_view, '-- Select table view --'),
                'arr_class' => add_blank_option(get_all_classes(), '--select class--'),
                'arr_section' => add_blank_option(get_class_section($class_id, $session['session_id']), '--select section--'),
                'class_id' => $class_id,
                'section_id' => $class_id,
                'session' => $session,
            );
        }
        return view('backend.student-attendance.attendance-register')->with($data);
    }

    public function attendanceRegisterReport(Request $request)
    {
        $arr_student_data = [];
        $class_id = $request->input('class_id');
        $section_id = $request->input('section_id');
        $c_date = $request->input('c_date');
        $view_type = $request->input('view_type');
        $arr_student_data = $this->getStudentAttendance($view_type, $class_id, $section_id, $c_date);
        if ($view_type == 'student-wise') {
            $data['attendance_view'] = $arr_student_data;
            $return = View::make('backend.student-attendance.view-student-attendance')->with($data)->render();
        } else {
            $data = [];
            $data['header'] = $this->tableHeaderCol($view_type, $c_date);
            $data['body'] = $arr_student_data;
            $data['view_type'] = $view_type;
            $data['summary'] = $this->attendanceRegisterSummary($class_id, $section_id, $c_date, $view_type);
            $return = View::make('backend.student-attendance.register')->with($data)->render();

        }
        return response()->json(['status' => 'success', 'data' => $return]);
    }

    public function attendanceRegisterSummary($class_id, $section_id, $c_date, $view_type)
    {
        $data = [];
        if (!empty($class_id)) {
            $obc_boys = 0;
            $obc_girls = 0;
            $obc_total = 0;
            $gen_boys = 0;
            $gen_girls = 0;
            $gen_total = 0;
            $sc_boys = 0;
            $sc_girls = 0;
            $sc_total = 0;
            $st_boys = 0;
            $st_girls = 0;
            $st_total = 0;
            $mi_boys = 0;
            $mi_girls = 0;
            $mi_total = 0;

            $this_month_working = 0;
            $upto_this_month_working = 0;
            $session = get_current_session();
            $session_id = $session['session_id'];
            $this_month_calendar = $this->monthCalendar('month', $c_date);
            $first = reset($this_month_calendar); // first date of month
            $end = end($this_month_calendar); // end date of month
            $start_date = $first['date'];
            $end_date = $end['date'];
            foreach ($this_month_calendar as $key => $value) {
                if (empty($value['holiday'])) {
                    $this_month_working++;
                }
            }
            $upto_start = $session['start_date']; // session start date
            $e_date = new DateTime($start_date);
            $e_date->modify("last day of previous month");
            $upto_end = $e_date->format('Y-m-d'); // last date of previous date

            $upto_this_month_calendar = $this->monthCalendar('upto_this_month', null, $upto_start, $upto_end);
            foreach ($upto_this_month_calendar as $key => $value) {
                if (empty($value['holiday'])) {
                    $upto_this_month_working++;
                }
            }
            $table = get_student_table($session_id, 'table');
            $student_query = DB::table($table)
                ->select('caste_category_id', DB::raw('COUNT(*) AS total'), DB::raw('SUM(CASE WHEN gender = 1 THEN 1 ELSE 0 END )AS boy'), DB::raw('SUM(CASE WHEN gender = 0 THEN 1 ELSE 0 END )AS girl'))
                ->where('current_session_id', $session_id)
                ->where('current_class_id', $class_id)
                ->where('current_section_id', $section_id)
                ->where('student_status', 1)
                ->groupBy('caste_category_id')
//		->whereBetween('admission_date', array($start_date, $end_date))
                ->orderBy('caste_category_id');
            $student_data = $student_query->get();
            $student_data = json_decode(json_encode($student_data), true);
            foreach ($student_data as $key => $value) {
                if ($value['caste_category_id'] == 1) {
                    $obc_boys = $value['boy'];
                    $obc_girls = $value['girl'];
                    $obc_total = $value['total'];
                } elseif ($value['caste_category_id'] == 2) {
                    $gen_boys = $value['boy'];
                    $gen_girls = $value['girl'];
                    $gen_total = $value['total'];
                } elseif ($value['caste_category_id'] == 3) {
                    $sc_boys = $value['boy'];
                    $sc_girls = $value['girl'];
                    $sc_total = $value['total'];
                } elseif ($value['caste_category_id'] == 4) {
                    $st_boys = $value['boy'];
                    $st_girls = $value['girl'];
                    $st_total = $value['total'];
                } elseif ($value['caste_category_id'] == 5) {
                    $mi_boys = $value['boy'];
                    $mi_girls = $value['girl'];
                    $mi_total = $value['total'];
                }
            }
            $new_student_query = DB::table($table)
                ->select(DB::raw('COUNT(*) AS total'), DB::raw('SUM(CASE WHEN gender = 1 THEN 1 ELSE 0 END )AS boy'), DB::raw('SUM(CASE WHEN gender = 0 THEN 1 ELSE 0 END )AS girl'))
                ->where('current_session_id', $session_id)
                ->where('current_class_id', $class_id)
                ->where('new_student', 1)->where('student_status', 1)
                ->where('current_section_id', $section_id)
                ->whereBetween('admission_date', array($start_date, $end_date));

            $new_student = $new_student_query->first();
            $new_boys = $new_student->boy;
            $new_girls = $new_student->girl;
            $new_total = $new_student->total;

            $left_student_query = DB::table($table)
                ->select(DB::raw('COUNT(*) AS total'), DB::raw('SUM(CASE WHEN gender = 1 THEN 1 ELSE 0 END )AS boy'), DB::raw('SUM(CASE WHEN gender = 0 THEN 1 ELSE 0 END )AS girl'))
                ->where('current_session_id', $session_id)->where('current_class_id', $class_id)
                ->where('student_left', 1)->where('student_status', 1)
                ->where('current_section_id', $section_id)
                ->whereBetween('admission_date', array($start_date, $end_date));
            $left_student = $left_student_query->first();

            $drop_boys = $left_student->boy;
            $drop_girls = $left_student->girl;
            $drop_total = $left_student->total;
//	    p('obc'.$obc_boys,0);
//	    p('$gen_boys'.$gen_boys,0);
//	    p('$sc_boys'.$sc_boys,0);
//	    p('$st_boys'.$st_boys,0);
//	    p('$mi_boys'.$mi_boys,0);
//	    p('$new_boys'.$new_boys,0);
            $total_boys = $obc_boys + $gen_boys + $sc_boys + $st_boys + $mi_boys;
            $total_girls = $obc_girls + $gen_girls + $sc_girls + $st_girls + $mi_girls;
//p($total_boys);
            $arr_data = array(
                'total_boys' => $total_boys,
                'total_girls' => $total_girls,
                'total_students' => $total_boys + $total_girls,
                'obc_boys' => $obc_boys,
                'obc_girls' => $obc_girls,
                'obc_total' => $obc_total,
                'gen_boys' => $gen_boys,
                'gen_girls' => $gen_girls,
                'gen_total' => $gen_total,
                'sc_boys' => $sc_boys,
                'sc_girls' => $sc_girls,
                'sc_total' => $sc_total,
                'st_boys' => $st_boys,
                'st_girls' => $st_girls,
                'st_total' => $st_total,
                'minority_boys' => $mi_boys,
                'minority_girls' => $mi_girls,
                'minority_total' => $mi_total,
                'promotion_boys' => 0,
                'promotion_girls' => 0,
                'promotion_total' => 0,
                'new_admission_boys' => $new_boys,
                'new_admission_girls' => $new_girls,
                'new_admission_total' => $new_total,
                'drop_boys' => $drop_boys,
                'drop_girls' => $drop_girls,
                'drop_total' => $drop_total,
                'teaching_this_month' => $this_month_working,
                'teaching_upto_this_month' => $upto_this_month_working,
                'teaching_total' => $this_month_working + $upto_this_month_working,
                'average_attendance' => '',
            );
        }
        return $arr_data;
//            p($data);
    }

    public function studentAttendanceDetail(Request $request, $student_id)
    {
        $data['arr_student'] = [];
        $session = get_current_session();
        if (!empty($session)) {
            $table = get_student_table($session['session_id'], $type = 'table');
            $arr_data = DB::table($table)->where('current_class_id', function ($query) use ($student_id, $table) {
                $query->where('student_id', $student_id)->select('current_class_id')->from($table);
            })->select('student_id', 'enrollment_number', 'first_name', 'middle_name', 'last_name')->orderBy('enrollment_number')->get();

            foreach ($arr_data as $student_data) {
                $student_data = (array)$student_data;
                $data['arr_student'][] = array(
                    'id' => $student_data['student_id'],
                    'name' => $student_data['first_name'] . ' ' . $student_data['middle_name'] . ' ' . $student_data['last_name'],
                );
            }
        }
        return view('backend.student-attendance.student-attendance')->with($data);
    }

    public function studentAttendanceCalendar(Request $request, $id, $start_date, $end_date)
    {

        $student_id = $id;
        $session = get_current_session();
        $date_calendar = [];
        if (!empty($session)) {
            $e_date = new DateTime($end_date);
            $e_date->modify('-1 day');
            $end_date = $e_date->format('Y-m-d');
            $arr_holiday = get_sheduled_school_holidays($start_date, $end_date);
            $session_id = $session['session_id'];
            $month_calendar = $this->monthCalendar($view_type = 'month_calendar', $s_date = null, $start_date, $end_date);
            $arr_attendance_symbol = \Config::get('custom.attendance_symbol');

            $arr_leave_data = DB::table('student_leaves as sl')->where('session_id', $session_id)
                ->where('student_id', $student_id)
                ->whereBetween('leave_start_date', array($start_date, $end_date))
                ->orWhereBetween('leave_end_date', array($start_date, $end_date))
                ->select('student_leave_id', 'leave_start_date', 'leave_end_date')
                ->get();
            $arr_leave_data = json_decode(json_encode($arr_leave_data), true);

            $arr_attendance_data = DB::table('student_attendances as sa')
                ->join('students as s', 'current_class_id', '=', 'class_id')
                ->where('sa.session_id', $session_id)
                ->where('s.student_id', $student_id)
                ->whereBetween('attendance_date', array($start_date, $end_date))
                ->select('sa.student_attendance_id', 'sa.student_id', 'sa.attendance_date')
                ->get();
            $arr_attendance_data = array_column(json_decode(json_encode($arr_attendance_data), true), null, 'attendance_date');

            $date_calendar = [];
            $temp_holiday_date = [];
//                p($month_calendar);
            foreach ($month_calendar as $key => $moth_date) {
                $status = '';
                $m_date = $moth_date['date'];
                if (!empty($moth_date['holiday'])) {

                    foreach ($arr_holiday['holiday_details'] as $key => $holiday) {
                        if ($holiday['holiday_start_date'] == $m_date) {

                            array_push($date_calendar, array(
                                    "title" => $holiday['holiday_name'],
                                    "allDay" => true,
                                    "start_date" => $holiday['holiday_start_date'],
                                    "end_date" => $holiday['holiday_end_date'],
                                    "url" => null,
                                    "color" => 'greenyellow',
                                    "className" => 'attendance_holiday',
                                    "textColor" => 'black',
                                )
                            );

                            array_push($temp_holiday_date, $holiday['holiday_period']);
                        }
                    }
                } // on leave
                else if (array_search($m_date, array_column($arr_leave_data, 'leave_start_date'))) {
                    foreach ($arr_leave_data as $key => $leave) {
                        if ($leave['leave_start_date'] == $m_date) {
                            array_push($date_calendar, array(
                                    "title" => $arr_attendance_symbol['l'],
                                    "allDay" => true,
                                    "start_date" => $leave['leave_start_date'],
                                    "end_date" => $leave['leave_end_date'],
                                    "url" => null,
                                    "color" => 'yellow',
                                    "textColor" => 'black',
                                    "className" => 'student_attendance',
                                )
                            );
                        }
                    }
                } else {
                    $attendance_info = isset($arr_attendance_data[$m_date]) ? $arr_attendance_data[$moth_date['date']] : [];
                    if (!empty($attendance_info)) {
                        $arr_student_absent = json_decode(json_decode(json_encode($attendance_info['student_id'])), true);
                        if (in_array($student_id, array_column($arr_student_absent, 'student_id'))) {
                            array_push($date_calendar, array(
                                    "title" => $arr_attendance_symbol['a'],
                                    "allDay" => true,
                                    "start_date" => $m_date,
                                    "end_date" => $m_date,
                                    "url" => null,
                                    "color" => '#ff0000',
                                    "textColor" => 'white',
                                    "className" => 'student_attendance',
                                )
                            );
                        }
                    } elseif ($m_date <= date('Y-m-d') && $moth_date['val'] != 'Sun') {
                        if (!in_array($m_date, $temp_holiday_date)) {
                            array_push($date_calendar, array(
                                    "title" => $arr_attendance_symbol['na'],
                                    "allDay" => true,
                                    "start_date" => $m_date,
                                    "end_date" => $m_date,
                                    "url" => null,
                                    "color" => 'pink',
                                    "textColor" => 'black',
                                    "className" => 'student_no_attendance',
                                )
                            );
                        }
                    }
                }
            }
        }
        $status = !empty($date_calendar) ? 'success' : 'failed';
//            }
        return response()->json(array('status' => $status, 'data' => $date_calendar));
//           
    }

    function studentAbsenceSummary()
    {
        $data = [];
        $data['session'] = get_current_session();
        return view('backend.student-attendance.student-absence-summary')->with($data);
    }

    function studentAbsenceSummaryData()
    {
        $arr_student_data = [];
        $offset = Input::get('start');
        $limit = Input::get('length');
        $session = get_current_session();
        $table = get_student_table($session['session_id']);
        $attendance_date = Input::get('attendance_date');
        if (empty($attendance_date)) {
            $attendance_date = $session['attendance_date'];
        }

        $date = new DateTime($attendance_date);
        $month_year = $date->format('Y-m');
        $date1 = $date->format($month_year . '-01');
        $date2 = $date->format($month_year . '-t');
        $arr_attendance = DB::table('student_attendances as sa')
            ->whereBetween('attendance_date', array($date1, $date2))
            ->where('attendance_status', 1)
            ->where('student_id', '!=', '')
            ->orderBy('attendance_date')
            ->orderBy('class_id', 'DESC')
            ->select('attendance_date', 'student_attendance_id', 'student_id', 'class_id')
            ->get();
        if (!empty($arr_attendance)) {
            $arr_attendance = json_decode(json_encode($arr_attendance), true);
            $arr_absent_sudent = [];
            $arr_today_absent_student = [];
            foreach ($arr_attendance as $key => $attendance) {
                $today_status = $attendance_date == $attendance['attendance_date'] ? true : false;

                $arr_student_attendance = json_decode($attendance['student_id'], true);
                foreach ($arr_student_attendance as $key => $student_attendance) {
                    array_push($arr_absent_sudent, $student_attendance['student_id']);
                }
                if ($today_status == true) {
                    $arr_today_absent_student[$attendance['class_id']] = array_column($arr_student_attendance, 'student_id');
                }
            }
            if (!empty($arr_today_absent_student)) {
                $arr_holiday = get_school_all_holidays();
                foreach ($arr_today_absent_student as $class_id => $absent_student) {
                    $arr_conti_absence = DB::table('student_attendances as sa')
                        ->where('sa.class_id', $class_id)
                        ->whereBetween('attendance_date', array($session['start_date'], $attendance_date))
                        ->where('attendance_status', 1)
                        ->where('sa.student_id', '!=', '')
                        ->orderBy('class_id', 'DESC')
                        ->orderBy('attendance_date', 'DESC')
                        ->select('sa.attendance_date', 'sa.student_attendance_id', 'sa.student_id', 'sa.class_id')
                        ->get();
                    foreach ($absent_student as $key => $student_id) {
                        $countine_absent = 0;
                        $arr[$student_id] = [];
                        foreach ($arr_conti_absence as $key => $absence_attendance) {
                            $absence_attendance = (array)$absence_attendance;
                            $absent_student_id = json_decode($absence_attendance['student_id'], true);
                            if (in_array($student_id, array_column($absent_student_id, 'student_id'))) {
                                $absent_attendance_date = $absence_attendance['attendance_date'];
                                if (empty($arr[$student_id])) {
                                    $arr[$student_id][] = $absent_attendance_date;
                                } else {
                                    $last_absent_date = new DateTime(end($arr[$student_id]));
                                    $absent_date = new DateTime($absence_attendance['attendance_date']);
                                    $diff = date_diff($absent_date, $last_absent_date);
                                    $days = $diff->format("%a");
                                    if ($days == 1) {
                                        $arr[$student_id][] = $absent_attendance_date;
                                    } elseif ($days > 1) {
                                        for ($i = 1; $i < $days; $i++) {
                                            $absent_attendance_date = date('Y-m-d', strtotime($absent_attendance_date . ' +1 day'));
                                            if (!in_array($absent_attendance_date, $arr_holiday)) {
                                                break;
                                            }
                                        }
                                    }
                                }
                            } else {
                                break 1;
                            }
                        }
                    }
                }
            }

            $arr_absent_student_id = array_count_values($arr_absent_sudent);
            $arr_student_id = array_keys($arr_absent_student_id);
            $student_data = DB::table($table)
                ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
                ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
                ->leftJoin('sections as sec', 'sec.section_id', '=', 's.current_section_id')
                ->select('s.enrollment_number', 's.student_id', 'sp.father_first_name', 'sp.father_last_name', 'sp.father_middle_name', 'sp.father_contact_number', 's.first_name', 's.last_name', 's.middle_name', 'c.class_name', 'sec.section_name')
                ->where('student_status', 1)
                ->whereIn('student_id', $arr_student_id)
                ->where('s.current_session_id', $session['session_id'])
                ->orderBy('c.class_order', 'ASC')
                ->skip($offset)
                ->take($limit)->get();
            foreach ($student_data as $key => $data) {
                $data = (array)$data;
                $absent_from_days = '';
                if (isset($arr[$data['student_id']])) {
                    $days = count($arr[$data['student_id']]);
                    if ($days > 1) {
                        $absent_from_days = 'From ' . $days . ' Days';
                    } else {
                        $absent_from_days = 'From today ';
                    }
                }
                $student = array(
                    's_no' => $key + 1,
                    'student_id' => $data['student_id'],
                    'enrollment_number' => $data['enrollment_number'],
                    'father_contact_number' => $data['father_contact_number'],
                    'student_name' => $data['first_name'] . ' ' . $data['middle_name'] . ' ' . $data['last_name'],
                    'father_name' => $data['father_first_name'] . ' ' . $data['father_middle_name'] . ' ' . $data['father_last_name'],
                    'class_name' => $data['class_name'] . '(' . $data['section_name'] . ')',
                    'absent_from_days' => $absent_from_days,
                    'absent_this_month' => $arr_absent_student_id[$data['student_id']],
                );
                $arr_student_data[] = (object)$student;
            }
        }
        return Datatables::of($arr_student_data)
            ->addColumn('action', function ($arr_student_data) {
                return '<a title="View" id="" href="' . url('admin/view-student-attendance/' . $arr_student_data->student_id) . '"">View Attendance</a>';
            })
            ->rawColumns(['action'])->make(true);
    }

    public function studentAttendanceSummary()
    {
        $session = get_current_session();
        $date1 = new DateTime($session['start_date']);
        $session['start_date'] = $date1->format('F Y');
        $date2 = new DateTime($session['end_date']);
        $session['end_date'] = $date2->format('F Y');
        $data = array(
            'arr_class' => add_blank_option(get_all_classes(), '--select class--'),
            'arr_section' => add_blank_option(get_class_section(null, null), '--select section--'),
            'session' => $session,
            'summary_type' => add_blank_option(array('month' => 'Monthly', 'overall' => 'Overall'), '-- View type --'),
        );
        return view('backend.student-attendance.student-attendance-summary')->with($data);
    }

    public function studentAttendanceSummaryData(Request $request)
    {
        $arr_student = [];
        $session = get_current_session();
        if (!empty($session)) {
            $session_id = $session['session_id'];
            $class_id = $request->input('class_id');
            $section_id = $request->input('section_id');
            $summary_type = $request->input('summary_type'); // monthly or allover
            $month_year = $request->input('month_year');
//                p($request->input());
            if ($summary_type == 'month') {
                $date = new DateTime($month_year);
                if ($date->format('Y-m') == date('Y-m')) {
                    $start_date = $date->format('Y-m-01');
                    $end_date = date('Y-m-d');
                } elseif ($date->format('Y-m') < date('Y-m')) {
                    $start_date = $date->format('Y-m-01');
                    $end_date = $date->format('Y-m-t');
                }
            } elseif ($summary_type == 'overall') {
                $start_date = $session['start_date'];
                $end_date = date('Y-m-d');
            }

            if (!empty($class_id) && !empty($summary_type) && !empty($start_date) && !empty($end_date)) {
                $month_calendar = $this->monthCalendar('summary-type', null, $start_date, $end_date, 'no'); // date array without holiday
                $first = reset($month_calendar); // first date
                $end = end($month_calendar); // end date
                $start_date = $first['date'];
                $end_date = $end['date'];
                $arr_attendance_data = [];
                $arr_leave_data = [];
                // get attendance data
                $arr_attendance = $this->studentAttendanceQuery($class_id, $section_id, $session_id, $start_date, $end_date);
                if (isset($arr_attendance['getStudentClass'])) {
                    $total_working_days = count($month_calendar);
                    foreach ($arr_attendance['classStudentAttendance'] as $key => $attendance) {
                        // attendance data
                        $arr_student_absent = json_decode(json_encode(json_decode($attendance['student_id'])), true);

                        $arr_attendance_data[$attendance['attendance_date']] = array(
                            'attendance_date' => $attendance['attendance_date'],
                            'student_id' => !empty($arr_student_absent) ? array_column($arr_student_absent, 'student_id') : [],
                        );
                    }
                    $serial_no = 1;
                    foreach ($arr_attendance['getStudentClass'] as $key => $student) {
                        $student_detail = array(
                            'student_id' => $student['student_id'],
                            'serial_no' => $serial_no,
                            'enrollment_number' => $student['enrollment_number'],
                            'student_name' => $student['first_name'] . ' ' . $student['middle_name'] . ' ' . $student['last_name'],
                            'father_name' => $student['getParent']['father_first_name'] . ' ' . $student['getParent']['father_middle_name'] . ' ' . $student['getParent']['father_last_name'],
                            'father_contact_number' => $student['getParent']['father_contact_number']
                        );
                        $absent_count = 0;
                        $no_attendance = 0;
                        $total_days = $total_working_days;
                        foreach ($month_calendar as $key => $moth_date) {
                            $attendance_info = isset($arr_attendance_data[$moth_date['date']]) ? $arr_attendance_data[$moth_date['date']] : [];
                            if (!empty($attendance_info)) {
                                $arr_student_absent = $attendance_info['student_id'];
                                if (in_array($student_detail['student_id'], $arr_student_absent)) {
                                    $absent_count++;
                                }
                            } else {
                                $no_attendance++;
                            }
                        }
                        $final_days = $total_days - $no_attendance;
                        $present_per = 0;
                        $persent = 0;
                        if ($final_days > 0) {
                            $persent = $final_days - $absent_count;
                            $present_per = ($persent / $final_days) * 100;
                        }
                        $student_detail['total_days'] = $final_days;
                        $student_detail['present_days'] = $persent;
                        $student_detail['absent_days'] = $absent_count;
                        $student_detail['present_per'] = $present_per;
                        $arr_student[$student['student_id']] = $student_detail;
                        $serial_no++;
                    }
                }
            }
        }
        return Datatables::of($arr_student)->make(true);
    }
}