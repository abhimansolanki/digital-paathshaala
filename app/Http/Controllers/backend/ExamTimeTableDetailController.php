<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\ExamTimeTable;
    use Illuminate\Support\Facades\View as View;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;
    use App\Model\backend\ExamTimeTableDetail;
    use App\Model\backend\ExamTimeTableSubject;

    class ExamTimeTableDetailController extends Controller
    {

        public function __construct()
        {
            
        }

        public function add(Request $request, $id = NULL, $detail_id = null)
        {
            $class_id          = null;
            $session_id        = null;
            $save_url          = '';
            $submit_button     = '';
            $time_table_detail = [];
            $exam_time_table_id = $id;
            $preview_data  =  '';
            if($exam_time_table_id != '')
            {
                $exam_time_table_id = \get_decrypted_value($id, true);
                $time_table        = ExamTimeTable::select('exam_time_table_id', 'title', 'exam_id', 'session_id')->where(['exam_time_table_id' => $exam_time_table_id, 'status' => 1])->with('timeTableExam')->first();
                if(!empty($time_table)){                
                    // Edit Case
                    if (!empty($detail_id))
                    {
                        $decrypted_detail_id = get_decrypted_value($detail_id, true);
                        $time_table_detail   =  ExamTimeTableDetail::find($decrypted_detail_id);
                        if(empty($time_table_detail)) {
                            return redirect('admin/exam-time-table')->withError('Exam Class mapping not found!');
                        }
                        $class_id         =  $time_table_detail['class_id'];
                        $section_id       =  $time_table_detail['section_id'];            
                        $save_url         =  url('exam-time-table-detail/save/' . $detail_id);
                        $submit_button    =  'Update';
                        $preview_data     =  $this->getPartialViewTimeTableSubject($time_table['exam_id'], 
                        $time_table['session_id'], $class_id, $section_id, $decrypted_detail_id);
//                        p($preview_data);
                    } else {
                        // Add Case
                        $save_url      = url('exam-time-table-detail/save');
                        $submit_button = 'Save';
                    }
                    $details['arr_class']         = add_blank_option(get_all_classes(), '--Select class --');
                    $details['arr_section']       = add_blank_option(get_class_section($class_id, 
                    $time_table['session_id']), '-- Select Setion -- ');
                    $details['title'] = $time_table['title'];
                    $details['exam_time_table_id'] = get_encrypted_value($time_table['exam_time_table_id'], true);
                    $details['exam_id'] = $time_table['exam_id'];
                    $details['exam_name'] = $time_table['timeTableExam']['exam_name'];

                    $data = array(
                        'save_url'      => $save_url,
                        'submit_button' => $submit_button,
                        'time_table_detail'    => $time_table_detail,
                        'exam_time_table_detail_id'  =>  $detail_id,
                        'details'       => $details,
                        'preview_data'  => $preview_data,
                        'redirect_url'  => url('admin/exam-time-table/add'),
                    );
                    return view('backend.exam-time-table-detail.add')->with($data);
                } else {
                    return view('backend.exam-time-table.add')->with('Exam time table Not found');
                }
            } else {
                return view('backend.exam-time-table.add')->with('Invalid exam time table');
            }
        }

        public function save(Request $request, $id = NULL)
        {
            $class_id            = Input::get('class_id');
            $section_id          = Input::get('section_id');
            $exam_time_table_id  = get_decrypted_value(Input::get('exam_time_table_id'), true);
            $decrypted_detail_id = get_decrypted_value($id, true);
            $exam_time_table_subjects = Input::get('map');
            if (!empty($id))
            {
                $time_table_detail = ExamTimeTableDetail::find($decrypted_detail_id);
                if (!$time_table_detail)
                {
                    return redirect('/admin/exam-time-table-detail/')->withError('Exam Detail not found!');
                }
                $success_msg = 'Exam class mapping updated successfully!';
            }
            else
            {
                $time_table_detail = new ExamTimeTableDetail;     
                $success_msg = 'Exam class mapping saved successfully!';
            }
            $validatior = Validator::make($request->all(), [
                    'class_id'         => 'required',
                    'section_id'       => 'required',
            ]);
            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $time_table_detail->exam_time_table_id = $exam_time_table_id;
                    $time_table_detail->class_id = $class_id;
                    $time_table_detail->section_id = $section_id;
                    $time_table_detail->save();

                    if(!empty($exam_time_table_subjects)){
                        foreach($exam_time_table_subjects as $key => $value) {
                            $exam_type_id = $key;
                            foreach($value as $details_key => $details_value) {
                                $detail_subject_id  = $details_value['detail_subject_id'];
                                
                                if($detail_subject_id != '') {
                                    $time_table_subject = ExamTimeTableSubject::find($detail_subject_id);
                                } else {
                                    $time_table_subject = new ExamTimeTableSubject;
                                }
                                
                                $exam_date = date_create($details_value['exam_date'])->format('Y-m-d');
                                $time_table_subject->detail_id = $time_table_detail->detail_id;
                                $time_table_subject->subject_id = $details_key;
                                $time_table_subject->exam_type_id = $exam_type_id;
//                                $time_table_subject->employee_id = $details_value['employee_id'];
                                $time_table_subject->exam_date = $exam_date;
                                $time_table_subject->start_time = $details_value['start_time'];
                                $time_table_subject->end_time = $details_value['end_time'];
                                $time_table_subject->save();
                            }
                        }
                    }
                }
                catch (\Exception $e)
                {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }
                DB::commit();
            }
            return redirect('admin/exam-time-table')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $detail_id            = Input::get('detail_id');
            $time_table_detail    = ExamTimeTableDetail::find($detail_id);
            if (!empty($time_table_detail))
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $time_table_detail->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Mapping deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Mapping not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $exam_time_table_id = Input::get('exam_id');
            $time_table_detail  = [];
            //$arr_section_list = get_section();
            $arr_exam_type      = get_all_exam_type();
            $arr_exam_class     = $this->getExamTimeTableDetailData($exam_time_table_id);
            foreach ($arr_exam_class as $key => $value)
            {
                $exam_type = '';
                foreach ($value['exam_type_id'] as $key => $exam_type_id)
                {
                    if (isset($arr_exam_type[$exam_type_id]))
                    {
                        $exam_type .= $arr_exam_type[$exam_type_id] . ', ';
                    }
                }
                $value['exam_type_name'] = rtrim($exam_type, ',');
                $time_table_detail[]     = (object) $value;
            }
            return Datatables::of($time_table_detail)
                    ->addColumn('action', function ($time_table_detail)
                    {
                        $encrypted_exam_class_id = get_encrypted_value($time_table_detail->exam_class_id, true);

                        return '<a title="Edit" id="deletebtn1" href="' . url('admin/exam-time-table-detail/' . $time_table_detail->exam_id . '/' . $encrypted_exam_class_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . '<button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $time_table_detail->exam_class_id . '"><i class="fa fa-trash"></i></button>';
                    })
                    ->make(true);
        }

        
        public function getExamTimeTableDetail($exam_time_table_id = null, $time_table_detail_id = null, $class_id = null, $section_id = null)
        {
            $time_table_detail_return = [];
            $query                    = DB::table('exam_classes as ec')
                ->join('create_exams as ce', 'ec.exam_id', '=', 'ce.exam_id')
                ->join('classes as c', 'ec.class_id', '=', 'c.class_id')
                ->join('sections as s', 'ec.section_id', '=', 's.section_id')
                ->where('ce.exam_id', $exam_time_table_id)
                ->select(
                'ec.exam_class_id', 'ec.session_id', 'ec.class_id', 'c.class_name', 'ec.exam_id', 's.section_name', 'ce.exam_name', 'ec.exam_type_id', 'ec.section_id', 'ec.subject_marks_criteria'
            );
            if (!empty($class_id))
            {
                $query->where('ec.class_id', $class_id);
            }
            if (!empty($section_id))
            {
                $query->where('ec.section_id', $section_id);
            }
            if (!empty($time_table_detail_id))
            {
                $query->where('ec.exam_class_id', $time_table_detail_id);
            }
            $arr_exam_class_data = $query->get()->toArray();
            if (!empty($arr_exam_class_data))
            {
                foreach ($arr_exam_class_data as $time_table_detail_data)
                {
                    $time_table_detail_data     = (array) $time_table_detail_data;
                    $time_table_detail          = array(
                        'exam_class_id'          => $time_table_detail_data['exam_class_id'],
                        'exam_id'                => $time_table_detail_data['exam_id'],
                        'exam_name'              => $time_table_detail_data['exam_name'],
                        'class_name'             => $time_table_detail_data['class_name'],
                        'section_name'           => $time_table_detail_data['section_name'],
                        'class_id'               => $time_table_detail_data['class_id'],
                        'session_id'             => $time_table_detail_data['session_id'],
                        'exam_type_id'           => json_decode($time_table_detail_data['exam_type_id'], true),
                        'section_id'             => $time_table_detail_data['section_id'],
                        'subject_marks_criteria' => json_decode($time_table_detail_data['subject_marks_criteria'], true),
                    );
                    $time_table_detail_return[] = $time_table_detail;
                }
            }
            return $time_table_detail_return;
        }

        public function getTimeTableSubject(Request $request)
        {
            $return_data          = [];
            if(isset($request) && $request->get('class_id') != '' && $request->get('section_id')){
                $class_id             = $request->get('class_id');
                $section_id           = $request->get('section_id');
                $exam_id              = $request->get('exam_id');
                $exam_time_table_id = $request->get('exam_time_table_id');
                $decrypted_exam_time_table_id = get_decrypted_value($exam_time_table_id, true);
                $current_session      = db_current_session();
                $session_id           = $current_session['session_id'];

                $arr_subject_detail   = [];
                $arr_exam_type        = [];
                $status               = 'failed';
                $getExistingRecord    = ExamTimeTableDetail::where(['class_id' => $class_id, 'section_id' => $section_id, 'exam_time_table_id' => $decrypted_exam_time_table_id])->get()->count();
                if($getExistingRecord == 0) {
                    $return_data          = $this->getPartialViewTimeTableSubject($exam_id, $session_id, $class_id, $section_id);
                    if($return_data != '') {
                        $status = 'success';                    
                    }
                } else {
                    return response()->json(array('status' => $status, 'message' => 'This class section mapping already done.'));
                }
                return response()->json(array('status' => $status, 'data' => $return_data));
            }
            else{   
                return response()->json(array('status' => $status, 'data' => $return_data));                
            }
        }

        public function getPartialViewTimeTableSubject($exam_id, $session_id, $class_id, $section_id, $decrypted_detail_id = null)
        {
            $return_data  = [];
            $time_table_subjects = [];
            if($decrypted_detail_id != null) {
                $time_table_subjects = ExamTimeTableSubject::where('detail_id', $decrypted_detail_id)->get();
                $arr_data = [];
                foreach($time_table_subjects as $key => $value) {
                    $arr_data[$value['exam_type_id']][$value['subject_id']][] = date_create($value['exam_date'])->format('d M Y');
                    $arr_data[$value['exam_type_id']][$value['subject_id']][] = $value['start_time'];
                    $arr_data[$value['exam_type_id']][$value['subject_id']][] = $value['end_time'];
//                    $arr_data[$value['exam_type_id']][$value['subject_id']][] = $value['employee_id'];
                    $arr_data[$value['exam_type_id']][$value['subject_id']][] = $value['detail_subject_id'];
                }
                $time_table_subjects   =  $arr_data;
            }
            $arr_subject_data = DB::table('exam_classes as ec')
                ->where('ec.exam_id', $exam_id)
                ->where('ec.session_id', $session_id)
                ->where('ec.class_id', $class_id)
                ->where('ec.section_id', $section_id)
                ->select('ec.exam_id', 'ec.class_id', 'ec.section_id', 'ec.subject_marks_criteria')
                ->get()->toArray();

            if (!empty($arr_subject_data))
            {
                $arr_subject_data = json_decode(json_encode($arr_subject_data), true);
                $selected_subject_for_exam_time_table  =  json_decode($arr_subject_data[0]['subject_marks_criteria'], true);
                $subject_ids   =  array_keys($selected_subject_for_exam_time_table);
	    $subjects  =  get_arr_subject($subject_ids);
                $exam_types = get_all_exam_type();
                $final_array = [];
                foreach($exam_types as $exam_type_key => $exam_type_value) {
                    foreach($selected_subject_for_exam_time_table as $sub_key => $sub_value) {
                        if(isset($sub_value[$exam_type_key])) {
                            $final_array[$exam_type_key]['exam_type'] = $exam_type_value;
                            $final_array[$exam_type_key]['subjects'][$sub_key] = $subjects[$sub_key];
                        }
                    }
                }
                $teacher_list = get_arr_employee();
                $teacher_list = \add_blank_option($teacher_list, 'Select Teacher');
                $data        = array(
                    'arr_subject'   => $final_array,
                    'time_table_subjects' => $time_table_subjects,
                    'arr_teachers' => $teacher_list,
                );
//                p($data);
                $return_data = View::make('backend.exam-time-table-detail.time-table-details')->with($data)->render();
            }
            return $return_data;
        }

        public function getEmployeeAvailability(Request $request)
        {
            p($request->all());
        }
    }
    