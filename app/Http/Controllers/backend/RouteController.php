<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Route;
    use App\Model\backend\StopPoint;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class RouteController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index()
        {
            $data = array(
                'redirect_url' => url('admin/route/add'),
            );
            return view('backend.route.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {
            $route              = [];
            $decrypted_route_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $route = $this->getRouteData($decrypted_route_id);
                $route = isset($route[0]) ? $route[0] : [];

                if (!$route)
                {
                    return redirect('admin/route')->withError('Route not found!');
                }
                $encrypted_route_id = get_encrypted_value($route['route_id'], true);
                $save_url           = url('admin/route/save/' . $encrypted_route_id);
                $submit_button      = 'Update';
            }
            else
            {
                $save_url      = url('admin/route/save');
                $submit_button = 'Save';
            }
            $arr_session          = get_session();
            $route['arr_session'] = add_blank_option($arr_session, '-- Academic year --');
            $data                 = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'route'         => $route,
                'redirect_url'  => url('admin/route/'),
            );
            return view('backend.route.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_route_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $route = Route::find($decrypted_route_id);

                if (!$route)
                {
                    return redirect('/admin/route/')->withError('Route not found!');
                }
                $success_msg = 'Route updated successfully!';
            }
            else
            {
                $route       = New Route;
                $success_msg = 'Route saved successfully!';
            }
            $session_id = Input::get('session_id');
            $validatior = Validator::make($request->all(), [
                    'route'      => 'required|unique:routes,route,' . $decrypted_route_id . ',route_id,session_id,'.$session_id,
                    'route_fair' => 'required',
                    'session_id' => 'required',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $route->route       = Input::get('route');
                    $route->session_id  = $session_id;
                    $route->save();
                    $main_stop_point_id = Input::get('main_stop_point_id');
                    if (!empty($main_stop_point_id))
                    {
                        $route_stop = StopPoint::find($main_stop_point_id);
                    }
                    else
                    {
                        $route_stop = new StopPoint;
                    }
                    $route_stop->session_id      = Input::get('session_id');
                    $route_stop->stop_point      = Input::get('route');
                    $route_stop->stop_fair       = Input::get('route_fair');
                    $route_stop->main_stop_point = 1;
                    $route->routeStopPoint()->save($route_stop);

                    // insert into stop points table
                    $arr_stop_point    = $request->has('stop_point') ? Input::get('stop_point') : [];
                    $arr_stop_point_id = $request->has('stop_point_id') ? Input::get('stop_point_id') : [];
                    $arr_stop_fair     = $request->has('stop_fair') ? Input::get('stop_fair') : [];
                    $save_stop_point   = [];
                    if (!empty($arr_stop_point))
                    {

                        foreach ($arr_stop_point as $key => $stop_point)
                        {
                            if (!empty($stop_point))
                            {
                                $stop_point_id = isset($arr_stop_point_id[$key]) ? $arr_stop_point_id[$key] : null;
                                if (!empty($stop_point_id))
                                {
                                    $stop = StopPoint::find($stop_point_id);
                                }
                                else
                                {
                                    $stop = new StopPoint;
                                }
                                $stop->session_id = Input::get('session_id');
                                $stop->stop_point = $stop_point;
                                $stop->stop_fair  = 0;
                                if (isset($arr_stop_fair[$key]))
                                {
                                    $stop->stop_fair = $arr_stop_fair[$key];
                                }
                                $save_stop_point[] = $stop;
                            }
                        }
                        $route->routeStopPoint()->saveMany($save_stop_point);
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                    return redirect()->back()->withErrors($error_message);
                }

                DB::commit();
            }
            return redirect('admin/route')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $route_id = Input::get('route_id');
            $route    = Route::find($route_id);
            if ($route)
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $route->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Route deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Route not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function deleteRouteStop(Request $request)
        {
            $stop_point_id = Input::get('stop_point_id');
            $stop_point    = StopPoint::find($stop_point_id);
            if ($stop_point)
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $stop_point->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Route Stop Point deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Route Stop Point not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $route     = [];
            $offset    = Input::get('start');
            $limit     = Input::get('length');
            $arr_route = $this->getRouteData(null,$limit,$offset);
            foreach ($arr_route as $key => $route_data)
            {
                $route[] = (object) $route_data;
            }
            return Datatables::of($route)
                    ->addColumn('sub_subject', function ($route)
                    {
                        return '<div class="text-center addpusintable"><button  data-toggle="modal" class="open_sub_subject_model" data-backdrop="static" data-keyboard="false" class="btn btn-primary" ><i class="fas fa-plus-square"></i></button></div>';
                    })
                    ->addColumn('action', function ($route)
                    {
                        $encrypted_route_id = get_encrypted_value($route->route_id, true);
                        return '<a title="Edit" id="deletebtn1" href="route/add/' . $encrypted_route_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $route->route_id . '"><i class="fa fa-trash"></i></button>';
                    })
                    ->rawColumns(['sub_subject', 'action'])->make(true);
        }

        public function getRouteData($route_id = null, $limit = null, $offset = null)
        {
            $route_return   = [];
            $session_id = get_current_session();
            $arr_route_data = Route::where('route_status', 1)->where('session_id', $session_id['session_id'])->where(function($query) use ($route_id)
                {
                    if (!empty($route_id))
                    {
                        $query->where('route_id', $route_id);
                    }
                })->with('routeStopPoint')
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
                    ->get();

            if (!empty($arr_route_data))
            {
                foreach ($arr_route_data as $key => $route_data)
                {
                    $route = array(
                        'route_id'   => $route_data['route_id'],
                        'session_id' => $route_data['session_id'],
                        'route'      => $route_data['route'],
                    );
                    foreach ($route_data['routeStopPoint'] as $key => $stop_point)
                    {
                        if ($stop_point['main_stop_point'] == 1)
                        {
                            $route['route_fair']         = $stop_point['stop_fair'];
                            $route['main_stop_point_id'] = $stop_point['stop_point_id'];
                        }
                        else
                        {
                            $route['arr_stop_point'][] = array(
                                'stop_point_id'   => $stop_point['stop_point_id'],
                                'stop_point'      => $stop_point['stop_point'],
                                'stop_fair'       => $stop_point['stop_fair'],
                                'main_stop_point' => $stop_point['main_stop_point'],
                            );
                        }
                    }
                    $route_return[] = $route;
                }
            }
//            p($route_return);
            return $route_return;
        }

        public function getRouteStopPointData($stop_point_id = null)
        {
            $stop_point_return    = [];
            $arr_route_stop_point = Route::
                with(['routeStopPoint' => function($query) use ($stop_point_id)
                    {
                        if (!empty($stop_point_id))
                        {
                            $query->where('stop_point_id', $stop_point_id);
                        }
                    }])->get();

            if (!empty($arr_route_stop_point))
            {
                foreach ($arr_route_stop_point as $key => $route_data)
                {
                    $route_stop_point = [];
                    foreach ($route_data['routeStopPoint'] as $key => $stop_point)
                    {
                        $route_stop_point[$stop_point['stop_point_id']] = $stop_point['stop_point'];
                    }
                    $stop_point_return[$route_data['route']] = $route_stop_point;
                }
            }
            return $stop_point_return;
        }

        public function getStopPoint()
        {
            $route_id         = Input::get('route_id');
            $route_stop_point = [];
            if (!empty($route_id))
            {
                $arr_route_stop_point = StopPoint::where('route_id', $route_id)->get();
                if (!empty($arr_route_stop_point))
                {
                    foreach ($arr_route_stop_point as $key => $route_data)
                    {
                        $route_stop_point[$route_data['stop_point_id']] = $route_data['stop_point'];
                    }
                }
            }
            $return_arr = array('status' => 'success', 'data' => $route_stop_point);
            return response()->json($return_arr);
        }

    }
    