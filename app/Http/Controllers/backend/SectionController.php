<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\Section;
use Validator;
use Illuminate\Support\Facades\Input;
use Datatables;
use Illuminate\Support\Facades\DB;

class SectionController extends Controller
{

    public function __construct()
    {

    }

    public function add(Request $request, $id = null)
    {
        $section = [];
        $section_id = null;
        if (!empty($id)) {
            $decrypted_section_id = get_decrypted_value($id, true);
            $section = $this->getSectionData($decrypted_section_id);
            $section = isset($section[0]) ? $section[0] : [];
            if (!$section) {
                return redirect('admin/section')->withError('Section not found!');
            }
            $encrypted_section_id = get_encrypted_value($section['section_id'], true);
            $save_url = url('admin/section/save/' . $encrypted_section_id);
            $submit_button = 'Update';
            $section_id = $decrypted_section_id;
        } else {
            $save_url = url('admin/section/save');
            $submit_button = 'Save';
        }
        $arr_class = get_all_classes();
        $section['arr_class'] = add_blank_option($arr_class, '--Select class --');
        $data = array(
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'section' => $section,
            'redirect_url' => url('admin/section/'),
        );
        return view('backend.section.add')->with($data);
    }

    public function save(Request $request, $id = null)
    {
        $decrypted_section_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $section = Section::find($decrypted_section_id);

            if (!$section) {
                return redirect('/admin/section/')->withError('Section not found!');
            }
            $success_msg = 'Section updated successfully!';
        } else {
            $section = new Section;
            $success_msg = 'Section saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
            'section_name' => 'required|unique:sections,section_name,' . $decrypted_section_id . ',section_id',
        ]);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction();
            try {
                $section->section_name = Input::get('section_name');
                $section->save();
            } catch (\Exception $e) {
                    //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }

        return redirect('admin/section')->withSuccess($success_msg);
    }

    public function destroy(Request $request)
    {
        $section_id = Input::get('section_id');
        $section = Section::find($section_id);
        if ($section) {
            DB::beginTransaction(); //Start transaction!
            try {
                $section->delete();
                $return_arr = array(
                    'status' => 'success',
                    'message' => 'Section deleted successfully!'
                );
            } catch (\Exception $e) {
                    //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                $return_arr = array(
                    'status' => 'used',
                    'message' => trans('language.delete_message')
                );
            }
            DB::commit();
        } else {
            $return_arr = array(
                'status' => 'error',
                'message' => 'Section not found!'
            );
        }
        return response()->json($return_arr);
    }

    public function anyData()
    {
        $section = [];
        $limit = Input::get('length');
        $offset = Input::get('start');
        $arr_section = $this->getSectionData($section_id = null, $offset, $limit);
        foreach ($arr_section as $key => $section_data) {
            $section[$key] = (object)$section_data;
        }
        return Datatables::of($section)
            ->addColumn('action', function ($section) {
                $delete = '';
                if ($section->is_fixed != 1) {
                    $delete = ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $section->section_id . '"><i class="fa fa-trash"></i></button>';
                }
                $encrypted_section_id = get_encrypted_value($section->section_id, true);
                return '<a title="Edit" id="deletebtn1" href="' . url('admin/section/' . $encrypted_section_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                    . $delete;
            })->make(true);
    }

    public function getSectionData($section_id = null, $offset = null, $limit = null)
    {
        $section_return = [];
        $section = [];
        $arr_section_data = Section::where(function ($query) use ($section_id) {
            if (!empty($section_id)) {
                $query->where('section_id', $section_id);
            }
        })
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
            ->get();
        if (!empty($arr_section_data)) {
            foreach ($arr_section_data as $key => $section_data) {
                $section = array(
                    'section_id' => $section_data['section_id'],
                    'section_name' => $section_data['section_name'],
                    'is_fixed' => $section_data['is_fixed'],
                );

                $section_return[] = $section;
            }
        }
        return $section_return;
    }

    function sectionList()
    {
        $class_id = Input::get('class_id');
        $session_id = Input::get('session_id');
        $arr_section = [];
        $message = 'Section not found!';
        $status = 'failed';
        if (!empty($class_id)) {
            $arr_section = get_class_section($class_id, $session_id);
            $message = 'Got result successfully';
            $status = 'success';
        }
        $arr_return = $arr_section;
        return response()->json($result = array('status' => $status, 'message' => $message, 'data' => $arr_return));
    }

        /*
     * Add new section from class screen
     */

    public function addSection(Request $request)
    {
        $arr_section = [];
        $section_name = Input::get('section_name');
        $section = Section::where('section_name', $section_name)->first();

        if (!empty($section->section_name)) {
            $return_arr = array(
                'status' => 'error',
                'message' => 'Section already exist',
                'arr_section' => $arr_section
            );
        } else {
            $add_section = new Section;
            $add_section->section_name = $section_name;
            $add_section->save();
            $arr_section = get_section();
            $return_arr = array(
                'status' => 'success',
                'message' => 'Section saved successfully!',
                'arr_section' => $arr_section
            );
        }
        return response()->json($return_arr);
    }

}
    