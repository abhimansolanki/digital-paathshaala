<?php

    namespace App\Http\Controllers\backend;

    use App\Model\backend\EmployeeDocument;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Validator;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Input;
    use App\Model\backend\Route;
    use App\Model\backend\StopPoint;
    use App\Model\backend\Student;
    use App\Model\backend\StudentVehicleAssigned;
    use App\Model\backend\StudentVehicleAssignedDetail;
    use App\Model\backend\Section;
    use App\Model\backend\ClassSection;
    use App\Model\backend\ClassSubject;
    use App\Model\backend\Grade;
    use App\Model\backend\Employee;
    use App\Model\backend\CreateExam;
    use App\Model\backend\ExamClass;
    use App\Model\backend\ExamTimeTableDetail;
    use App\Model\backend\ExamTimeTable;
    use App\Model\backend\Vehicle;

    

    class HistoryController extends Controller
    {

        public function __construct()
        {
            
        }

        function getSession()
        {
            $current_date = date('Y-m-d');
            $current      = db_current_session();
            $previous     = (array) DB::table('sessions')
                    ->where('session_status', '!=', 1)
                    ->where('end_date', '<', $current_date)
                    ->select('session_id', 'session_year', 'start_date', 'end_date', 'session_status')
                    ->orderBy('end_date', 'DESC')->first();
            $return  = [];
            if (!empty($current['session_id']) && !empty($previous['session_id']))
            {
                $return = array(
                    'current'  => $current,
                    'previous' => $previous,
                );
            }
            return $return;
        }

        function vehicleHistory()
        {
            $session           = $this->getSession();
            $arr_stop_point_id = [];
            if (!empty($session))
            {
                //
                DB::beginTransaction(); //Start transaction!
                try
                {
                    // history of route and its stop point assign
                    $arr_route_data = Route::where('session_id', $session['previous']['session_id'])->with('routeStopPoint')->get();
                    foreach ($arr_route_data as $key => $route_data)
                    {
                        $route               = new Route;
                        $route->session_id   = $session['current']['session_id'];
                        $route->route        = $route_data['route'];
                        $route->previous_id  = $route_data['route_id'];
                        $route->route_status = $route_data['route_status'];
                        if ($route->save())
                        {
                            $route_details = [];
                            foreach ($route_data['routeStopPoint'] as $key => $stop_point_data)
                            {
                                $stop_point                  = new StopPoint;
                                $stop_point->session_id      = $session['current']['session_id'];
                                $stop_point->stop_point      = $stop_point_data['stop_point'];
                                $stop_point->stop_fair       = $stop_point_data['stop_fair'];
                                $stop_point->main_stop_point = $stop_point_data['main_stop_point'];
                                $stop_point->stop_status     = $stop_point_data['stop_status'];
                                $stop_point->previous_id     = $stop_point_data['stop_point_id'];
                                $route_details[]             = $stop_point;
                            }
                            if (!empty($route_details))
                            {
                                $route->routeStopPoint()->saveMany($route_details);
                                // last inserted id of stop point
                                foreach ($route_details as $key => $value)
                                {
                                    $arr_stop_point_id[$value['previous_id']] = $value['stop_point_id'];
                                }
                            }
                        }
                    }

                    // history of vehicle assign
                    $arr_assigned_id = [];
                    if (!empty($arr_stop_point_id))
                    {
                        $arr_vehicle_assigned = StudentVehicleAssigned::where('session_id', $session['previous']['session_id'])->with('vehicleAssignedStudentDetail')->get();
                        foreach ($arr_vehicle_assigned as $key => $assigned)
                        {
                            $vehicle_assigned                = new StudentVehicleAssigned;
                            $vehicle_assigned->session_id    = $session['current']['session_id'];
                            $vehicle_assigned->vehicle_id    = $assigned['vehicle_id'];
                            $vehicle_assigned->stop_point_id = $arr_stop_point_id[$assigned['stop_point_id']];
                            $vehicle_assigned->pickup_time   = $assigned['pickup_time'];
                            $vehicle_assigned->return_time   = $assigned['return_time'];

                            if ($vehicle_assigned->save())
                            {
                                $arr_assigned_detail = [];
                                foreach ($assigned['vehicleAssignedStudentDetail'] as $key => $detail_data)
                                {
                                    $assigned_detail              = new StudentVehicleAssignedDetail;
                                    $assigned_detail->join_date   = $detail_data['join_date'];
                                    $assigned_detail->join_date   = $detail_data['join_date'];
                                    $assigned_detail->assigned_to = $detail_data['assigned_to'];
                                    $assigned_detail->student_id  = $detail_data['student_id'];
                                    $assigned_detail->employee_id = $detail_data['employee_id'];
                                    $arr_assigned_detail[]        = $assigned_detail;
                                }
                                if (!empty($arr_assigned_detail))
                                {
                                    $vehicle_assigned->vehicleAssignedStudentDetail()->saveMany($arr_assigned_detail);
                                }
                            }
                        }
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                }
                DB::commit();
            }
            p('done');
        }

        function studentHistory()
        {
            $session             = $this->getSession();
            $arr_student_history = [];
            if (!empty($session))
            {
                //
                DB::beginTransaction(); //Start transaction!
                try
                {
                    // history of route and its stop point assign
                    $arr_student_data = Student::where('current_session_id', $session['previous']['session_id'])->get();
                    foreach ($arr_student_data as $key => $student_data)
                    {
                        $student_history                              = [];
                        $student_history['student_id']                = $student_data['student_id'];
                        $student_history['form_number']               = $student_data['form_number'];
                        $student_history['student_parent_id']         = $student_data['student_parent_id'];
                        $student_history['enrollment_number']         = $student_data['enrollment_number'];
                        $student_history['current_session_id']        = $session['current']['session_id'];
                        $student_history['current_class_id']          = $student_data['current_class_id'];
                        $student_history['current_section_id']        = $student_data['current_section_id'];
                        $student_history['student_type_id']           = $student_data['student_type_id'];
                        $student_history['caste_category_id']         = $student_data['caste_category_id'];
                        $student_history['first_name']                = $student_data['first_name'];
                        $student_history['middle_name']               = $student_data['middle_name'];
                        $student_history['last_name']                 = $student_data['last_name'];
                        $student_history['address_line1']             = $student_data['address_line1'];
                        $student_history['address_line2']             = $student_data['address_line2'];
                        $student_history['dob']                       = $student_data['dob'];
                        $student_history['gender']                    = $student_data['gender'];
                        $student_history['birth_place']               = $student_data['birth_place'];
                        $student_history['contact_number']            = $student_data['contact_number'];
                        $student_history['blood_group']               = $student_data['blood_group'];
                        $student_history['body_sign']                 = $student_data['body_sign'];
                        $student_history['email']                     = $student_data['email'];
                        $student_history['aadhaar_number']            = $student_data['aadhaar_number'];
                        $student_history['previous_section_name']     = $student_data['previous_section_name'];
                        $student_history['previous_class_name']       = $student_data['previous_class_name'];
                        $student_history['previous_school_name']      = $student_data['previous_school_name'];
                        $student_history['previous_result']           = $student_data['previous_result'];
                        $student_history['profile_photo']             = $student_data['profile_photo'];
                        $student_history['tc_number']                 = $student_data['tc_number'];
                        $student_history['tc_date']                   = $student_data['tc_date'];
                        $student_history['admission_date']            = $student_data['admission_date'];
                        $student_history['join_date']                 = $student_data['join_date'];
                        $student_history['fee_discount']              = $student_data['fee_discount'];
                        $student_history['previous_due_fee']          = $student_data['previous_due_fee'];
                        $student_history['previous_due_fee_original'] = $student_data['previous_due_fee'];
                        $student_history['remark']                    = $student_data['remark'];
                        $student_history['fee_calculate_month']       = $student_data['fee_calculate_month'];
                        $student_history['student_left']              = $student_data['student_left'];
                        $student_history['reason']                    = $student_data['reason'];
                        $student_history['new_student']               = $student_data['new_student'];
                        $student_history['discount_date']             = $student_data['discount_date'];
                        $student_history['created_at']                = date('Y-m-d H:i:s');
                        $student_history['updated_at']                = date('Y-m-d H:i:s');
                        $arr_student_history[]                        = $student_history;
                    }
                    if (!empty($arr_student_history))
                    {
                        App\Model\backend\StudentHistory::insert($arr_student_history);
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                }
                DB::commit();
            }
            p('done');
        }

        function classSubjectHistory()
        {
            $session               = $this->getSession();
            $class_subject_history = [];
            if (!empty($session))
            {
                //
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $arr_class_subject = \App\Model\backend\ClassSubject::where('session_id', $session['previous']['session_id'])->get();
                    foreach ($arr_class_subject as $key => $class_subject)
                    {
                        $class_subject_data                          = [];
                        $class_subject_data['class_id']              = $class_subject['class_id'];
                        $class_subject_data['session_id']            = $session['current']['session_id'];
                        $class_subject_data['subject_group_id']      = $class_subject['subject_group_id'];
                        $class_subject_data['subject_id']            = $class_subject['subject_id'];
                        $class_subject_data['section_id']            = $class_subject['section_id'];
                        $class_subject_data['unassigned_student_id'] = $class_subject['unassigned_student_id'];
                        $class_subject_data['include_in_card']       = $class_subject['include_in_card'];
                        $class_subject_data['include_in_rank']       = $class_subject['include_in_rank'];
                        $class_subject_data['class_subject_order']   = $class_subject['class_subject_order'];
                        $class_subject_data['created_at']            = date('Y-m-d H:i:s');
                        $class_subject_data['updated_at']            = date('Y-m-d H:i:s');
                        $class_subject_history[]                     = $class_subject_data;
                    }
                    if (!empty($class_subject_history))
                    {
                        \App\Model\backend\ClassSubject::insert($class_subject_history);
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                }
                DB::commit();
            }
            p('done');
        }

        function classSectionHistory()
        {
            $session               = $this->getSession();
            $class_section_history = [];
            if (!empty($session))
            {
                //
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $arr_class_section = \App\Model\backend\ClassSection::where('session_id', $session['previous']['session_id'])->get();
                    foreach ($arr_class_section as $key => $class_section)
                    {
                        $class_section_data               = [];
                        $class_section_data['class_id']   = $class_section['class_id'];
                        $class_section_data['section_id'] = $class_section['section_id'];
                        $class_section_data['session_id'] = $session['current']['session_id'];
                        $class_section_data['created_at'] = date('Y-m-d H:i:s');
                        $class_section_data['seats'] = $class_section['seats'];
                        $class_section_data['strength'] = $class_section['strength'];
                        $class_section_history[]          = $class_section_data;
                    }
                    if (!empty($class_section_history))
                    {
                        \App\Model\backend\ClassSection::insert($class_section_history);
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                }
                DB::commit();
            }
            p('done');
        }

        function gradeHistory()
        {
            $session       = $this->getSession();
            $grade_history = [];
            if (!empty($session))
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $arr_grade = \App\Model\backend\Grade::where('session_id', $session['previous']['session_id'])->get();
                    foreach ($arr_grade as $key => $grade)
                    {
                        $grade_data                = [];
                        $grade_data['previous_id'] = $grade['grade_id'];
                        $grade_data['grade_name']  = $grade['grade_name'];
                        $grade_data['grade_alias'] = $grade['grade_alias'];
                        $grade_data['maximum']     = $grade['maximum'];
                        $grade_data['minimum']     = $grade['minimum'];
                        $grade_data['grade_comments']     = $grade['grade_comments'];
                        $grade_data['session_id']  = $session['current']['session_id'];
                        $grade_data['created_at']  = date('Y-m-d H:i:s');
                        $grade_data['updated_at']  = date('Y-m-d H:i:s');
                        $grade_history[]           = $grade_data;
                    }
                    if (!empty($grade_history))
                    {
                        \App\Model\backend\Grade::insert($grade_history);
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                }
                DB::commit();
            }
            p('done');
        }

        public function sessionDataForward(){
            $target_session = get_session();
            $ss = get_current_session();
            $source_session[$ss['session_id']] = $ss['session_year'];
            $data = array(
                'page_title'    => trans('language.sync_session_data'),
                'save_url'      => url('admin/session-data-forward-save'),
                'submit_button' => trans('language.sync_data'),
                'target_session'       => add_blank_option($target_session,"--Select Target--"),
                'source_session'       => add_blank_option($source_session,"--Select Source--"),
                'redirect_url'  => url('admin/session/'),
            );
            return view('backend.session.sync_data')->with($data);
        }

        public function sessionDataForwardSave(Request $request){

            $request->validate([
                    'source_session'   => 'required|integer',
                    'target_session'   => 'required|integer',
            ]);
            DB::beginTransaction();
            try{
                if($request->source_session == $request->target_session){
                    return redirect()->back()->withErrors("Source and Target Session should be different. "); 
                }
                $result = $this->syncData($request->target_session, $request->source_session);
                if(!$result){
                    return redirect()->back()->withErrors("Something Went Wrong"); 
                }
            }catch(\Exception $e){
                DB::rollback();
                return redirect()->back()->withErrors($e->getMessage());
            }
            DB::commit();
            return redirect()->back()->with("success", "Data successfully Synced.");
        }

        public static function syncData($target_session_id, $source_session_id)
        {
            /** Transfer Configuration Data **/

            // Move classes and sessions
            $class_sections = ClassSection::where('session_id',$source_session_id)->get();
            if(!empty($class_sections)){
                foreach($class_sections as $class_section){
                    // Check Target Record is exist or not
                    $exist_record = ClassSection::where(
                        array(
                            'session_id' => $target_session_id,
                            'class_id' => $class_section->class_id,
                            'section_id' => $class_section->section_id,
                        )
                    )->get();
                    if(count($exist_record) == 0){
                        $new_class_section = $class_section->replicate();
                        $new_class_section->session_id = $target_session_id;
                        $new_class_section->save();
                    }
                }
            }

            // Move Class subjects
            $class_subjects = ClassSubject::where('session_id', $source_session_id)->get();
            if(!empty($class_subjects)){
                foreach($class_subjects as $class_subject){
                    // Check Target Record is exist or not
                    $exist_record = ClassSubject::where(
                        array(
                            'session_id' => $target_session_id,
                            'class_id' => $class_subject->class_id,
                            'section_id' => $class_subject->section_id,
                            'subject_group_id' => $class_subject->subject_group_id,
                            'subject_id' => $class_subject->subject_id,
                        )
                    )->get();
                    if(count($exist_record)  == 0){
                        $new_class_subjects = $class_subject->replicate();
                        $new_class_subjects->session_id = $target_session_id;
                        $new_class_subjects->save();
                    }    
                }
            }

            // Move Grades
            $grades = Grade::where('session_id', $source_session_id)->get();
            if(!empty($grades)){
                foreach($grades as $grade){
                    // Check Target Record is exist or not
                    $exist_record = Grade::where(
                        array(
                            'session_id' => $target_session_id,
                            'grade_alias' => $grade->grade_alias,
                        )
                    )->get();
                    if(count($exist_record) == 0){
                        $new_grade = $grade->replicate();
                        $new_grade->session_id = $target_session_id;
                        $new_grade->save();
                    }    
                }
            }


            /** Transfer Staff Manager Data **/

            $old_new_employee_mapping = [];
            // Move Employees
            $employees = Employee::where('session_id', $source_session_id)->get();
            if(!empty($employees)){
                foreach($employees as $employee){
                    // Check Target Record is exist or not
                    $exist_record = Employee::where(
                        array(
                            'session_id' => $target_session_id,
                            'employee_code' => $employee->employee_code,
                        )
                    )->get();
                    if(count($exist_record) == 0){
                        $new_employee = $employee->replicate();
                        $new_employee->session_id = $target_session_id;
                        $new_employee->save();
                        $old_new_employee_mapping[$new_employee->employee_id] = $employee->employee_id;

                        // Move Employee Document
                        $employee_documents = EmployeeDocument::where('employee_id', $new_employee->employee_id)->get();
                        if(count($employee_documents) == 0){
                            foreach($employee_documents as $employee_document){
                                $new_document = $employee_document->replicate();
                                $new_document->employee_id = $new_employee->employee_id;
                                $new_document->save();
                            }
                        }
                    }    
                }
            }


            /** Transfer Exam Manager Data  (Not considering exam student roll number)**/ 

            // Move Create Exam
            $create_exams = CreateExam::where('session_id', $source_session_id)->get();
            if(!empty($create_exams)){
                foreach($create_exams as $create_exam){
                    // Check Target Record is exist or not
                    $exist_record = CreateExam::where(
                        array(
                            'session_id' => $target_session_id,
                            'exam_category_id' => $create_exam->exam_category_id,
                        )
                    )->get();
                    if(count($exist_record) == 0){
                        $new_create_exam = $create_exam->replicate();
                        $new_create_exam->session_id = $target_session_id;
                        $new_create_exam->save();

                        // Move Exam Classes
                        $exam_classes = ExamClass::where(
                            array(
                                'exam_id' => $new_create_exam->exam_id,
                                'session_id' => $new_create_exam->exam_id,
                            ))->get();
                        if(count($exam_classes) == 0){
                            foreach($exam_classes as $exam_class){
                                $new_exam_class = $exam_class->replicate();
                                $new_exam_class->exam_id = $new_create_exam->exam_id;
                                $new_exam_class->session_id = $target_session_id;
                                $new_exam_class->save();
                            }
                        }

                        // Move Exam Time Table
//                        $exam_time_tables = ExamTimeTable::where([['session_id','=',$source_session_id],['exam_id','=',$create_exam->exam_id]])->get();
//                        if(!empty($exam_time_tables)){
//                            foreach($exam_time_tables as $exam_time_table){
//                                // Check Target Record is exist or not
//                                $exist_record = ExamTimeTable::where(
//                                    array(
//                                        'session_id' => $target_session_id,
//                                        'exam_id' => $exam_time_table->exam_id,
//                                    )
//                                )->get();
//                                if(count($exist_record) == 0){
//                                    $new_exam_time_table = $exam_time_table->replicate();
//                                    $new_exam_time_table->session_id = $target_session_id;
//                                    $new_exam_time_table->session_id = $exam_time_table->exam_id;
//                                    $new_exam_time_table->save();
//
//                                    // Move Exam Time Table Details
//                                    $exam_time_table_details = ExamTimeTableDetail::where(
//                                        array(
//                                            'exam_time_table_id' => $new_exam_time_table->exam_time_table_id
//                                        ))->get();
//                                    if(count($exam_time_table_details) == 0){
//                                        foreach($exam_time_table_details as $exam_time_table_detail){
//                                            $new_exam_class = $exam_time_table_detail->replicate();
//                                            $new_exam_class->exam_id = $new_exam_time_table->exam_time_table_id;
//                                            $new_exam_class->save();
//
//
//                                            // Move Exam Time Table Subjects
//                                            $exam_time_table_subjects = ExamTimeTableSubject::where([['detail_id','=',$new_exam_class->detail_id]])->get();
//                                            if(count($exam_time_table_subjects) == 0){
//                                                foreach($exam_time_table_subjects as $exam_time_table_subject)
//                                                {
//                                                    $new_exam_time_table_subject = $exam_time_table_subject->replicate();
//                                                    $new_exam_time_table_subject->detail_id = $new_exam_class->detail_id;
//                                                    $new_exam_time_table_subject->save();
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
                    }    
                }
            }


            /** Transfer Transport Manager Data **/

            $old_new_route_mapping = [];
            // Move Routes
            $routes = Route::where('session_id', $source_session_id)->get();
            if(!empty($routes)){
                $arr_stop_point_id = [];
                foreach($routes as $route){
                    $new_route = $route->replicate();
                    $new_route->session_id = $target_session_id;
                    $new_route->save();
                    $route_details = [];
                    foreach ($route['routeStopPoint'] as $key => $stop_point_data)
                    {
                        $new_stop_point = $stop_point_data->replicate();
                        $new_stop_point->route_id = $new_route->route_id;
                        $new_stop_point->session_id = $target_session_id;
                        $new_stop_point->save();
                        $arr_stop_point_id[$stop_point_data->stop_point_id] = $new_stop_point->stop_point_id;
                    }
                    $old_new_route_mapping[$route->route_id] = $new_route->route_id;
                }
            }   


            $old_new_vehicle_mapping = [];
            // Move Vehicles
            $vehicles = Vehicle::where('session_id', $source_session_id)->get();
            if(!empty($vehicles) && !empty($old_new_route_mapping)){
                $existing_routes = Route::select("route_id")->where('session_id', $target_session_id)->get()->toArray();
                foreach($vehicles as $vehicle){
                    // Check Target Record is exist or not
                    $exist_record = Vehicle::where(
                        array(
                            'session_id' => $target_session_id,
                            'vehicle_provider_id' => $vehicle->vehicle_provider_id,
                            'vehicle_driver_id' => $vehicle->vehicle_driver_id,
                            'vehicle_helper_id' => $vehicle->vehicle_helper_id,
                        )
                    )->whereIn("route_id",$existing_routes)->get();
                    if(count($exist_record) == 0){
                        $new_vehicle = $vehicle->replicate();
                        $new_vehicle->session_id = $target_session_id;
                        $new_vehicle->route_id = $old_new_route_mapping[$vehicle->route_id];
                        $new_vehicle->save();
                        $old_new_vehicle_mapping[$vehicle->vehicle_id] = $new_vehicle->vehicle_id;
                    }    
                }
            }

            // Move Stop Points
            $old_new_stop_point_mapping = [];
            $stop_points = StopPoint::where('session_id', $source_session_id)->get();
            if(!empty($stop_points) && !empty($old_new_route_mapping)){
                $arr_stop_point_id = [];
                foreach($stop_points as $stop_point){
                    $new_stop_point = $stop_point->replicate();
                    $new_stop_point->session_id = $target_session_id;
                    $new_stop_point->route_id = $old_new_route_mapping[$stop_point['route_id']];
                    $new_stop_point->save();
                    $old_new_stop_point_mapping[$stop_point->stop_point_id] = $new_stop_point->stop_point_id;
                }
            }   
            // Move Student Vehicle Assigned
            if (!empty($old_new_stop_point_mapping))
            {
                $arr_vehicle_assigned = StudentVehicleAssigned::where('session_id', $source_session_id)->get();
                foreach ($arr_vehicle_assigned as $key => $assigned)
                {
                    $new_assigned_vehicle                = $assigned->replicate();
                    $new_assigned_vehicle->session_id    = $target_session_id;
                    $new_assigned_vehicle->vehicle_id    = $old_new_vehicle_mapping[$assigned['vehicle_id']];
                    $new_assigned_vehicle->stop_point_id = $old_new_stop_point_mapping[$assigned['stop_point_id']];
                    $new_assigned_vehicle->save();

                    $arr_vehicle_assigned_details = StudentVehicleAssignedDetail::where('assigned_id', $assigned->student_vehicle_assigned_id)->get();    
                    foreach ($arr_vehicle_assigned_details as $key => $detail_data)
                    {
                        $new_assigned_detail              = $detail_data->replicate();
                        $new_assigned_detail->assigned_id = $new_assigned_vehicle->student_vehicle_assigned_id;
                        //$new_assigned_detail->employee_id = $old_new_employee_mapping[$detail_data->employee_id];
                        $new_assigned_detail->save();
                    }
                }
            }

            return true;
        }
    }
    