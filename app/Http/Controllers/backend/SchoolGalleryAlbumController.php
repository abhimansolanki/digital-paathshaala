<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\SchoolGalleryAlbum;
    use App\Model\backend\SchoolGallery;
    use Validator;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Input;

    class SchoolGalleryAlbumController extends Controller
    {

        public function __construct()
        {
	
        }

        public function add(Request $request, $id = null)
        {
	if (!empty($id))
	{
	    $album = SchoolGalleryAlbum::Find($id);
	    if (empty($album->album_id))
	    {
	        return redirect('admin/school-gallery-album');
	    }
	    $save_url = url('admin/school-gallery-album/save/' . $id);
	} else
	{
	    $save_url = url('admin/school-gallery-album/save');
	}
	$school_gallery = SchoolGalleryAlbum::with(['getGalleryImages' => function($q)
		{
		    $q->orderBy('created_at', 'DESC');
		}])->orderBy('created_at', 'DESC')->get()->toArray();
	$school_gallery_album['arr_album_images'] = array_column($school_gallery, null, 'album_id');

	$submit_button = 'Save';
	$data = array(
	    'save_url' => $save_url,
	    'submit_button' => $submit_button,
	    'album' => $school_gallery_album,
	);
	return view('backend.school-gallery-album.add')->with($data);
        }

        public function save(Request $request, $id = null)
        {
	if (!empty($id))
	{
	    $school_gallery_album = SchoolGalleryAlbum::find($id);
	    if (!empty($school_gallery_album->album_id))
	    {
	        $success_msg = 'Album updated successfully!';
	    }
	} else
	{
	    $school_gallery_album = New SchoolGalleryAlbum;
	    $success_msg = 'Album saved successfully!';
	}
	$validatior = Validator::make($request->all(), [
		'album_title' => 'required|min:3|max:250',
	]);

	if ($validatior->fails())
	{
	    return redirect()->back()->withInput()->withErrors($validatior);
	} else
	{
	    DB::beginTransaction(); //Start transaction!
	    try
	    {
	        $school_gallery_album->album_title = Input::get('album_title');
	        $school_gallery_album->description = Input::get('description');
	        if ($school_gallery_album->save())
	        {
		if ($request->hasFile('image_url'))
		{
		    $arr_gallery_images = [];
		    $arr_file = $request->file('image_url');
		    foreach ($arr_file as $key => $file)
		    {
		        $school_gallery = new SchoolGallery;
		        $config_upload_path = \Config::get('custom.school_gallery');
		        $destinationPath = public_path() . $config_upload_path['upload_path'];
		        $filename = time() . $file->getClientOriginalName();
		        $file->move($destinationPath, $filename);
		        $school_gallery->image_url = $filename;
		        $school_gallery->type = 1;
		        $arr_gallery_images[] = $school_gallery;
		    }
		    if (!empty($arr_gallery_images))
		    {
		        $school_gallery_album->getGalleryImages()->saveMany($arr_gallery_images);
		    }
		}
	        }
	    } catch (\Exception $e)
	    {
	        //failed logic here
	        DB::rollback();
	        $error_message = $e->getMessage();
	        return redirect()->back()->withErrors($error_message);
	    }
	    DB::commit();
	    return redirect('admin/school-gallery-album')->withSuccess($success_msg);
	}
        }

        public function destroy(Request $request)
        {
	$id = Input::get('id');
	$type = Input::get('type');
	if ($type == 'Album')
	{
	    $data = SchoolGalleryAlbum::find($id);
	} elseif ($type == 'Image')
	{
	    $data = SchoolGallery::find($id);
	}
	if ($data)
	{
	    DB::beginTransaction(); //Start transaction!
	    try
	    {
	        $data->delete();
	        $return_arr = array(
		'status' => 'success',
		'message' => 'Album deleted successfully!'
	        );
	    } catch (\Exception $e)
	    {
	        //failed logic here
	        DB::rollback();
	        $error_message = $e->getMessage();
//	        p($error_message);
	        $return_arr = array(
		'status' => 'used',
		'message' => 'Album deleted successfully!'
	        );
	    }
	    DB::commit();
	} else
	{
	    $return_arr = array(
	        'status' => 'error',
	        'message' => 'Album not found!'
	    );
	}
	return response()->json($return_arr);
        }

    }
    