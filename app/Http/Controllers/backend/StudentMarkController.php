<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\CronController;
use App\Model\backend\Classes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\StudentMark;
use App\Model\backend\StudentMarkDetail;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Input;

//    use App\Model\backend\Subject;
use Illuminate\Support\Facades\View as View;
use Datatables;
use DateTime;

class StudentMarkController extends Controller
{

    public function __construct()
    {

    }

    public function index(Request $request)
    {
        $data = array(
            'redirect_url' => url('admin/student-mark/add'),
        );
        return view('backend.student-mark.index')->with($data);
    }

    public function add(Request $request, $id = null)
    {
        $class_id = null;
        $exam_id = null;
        $session_id = null;
        $section_id = null;
        $student_mark = [];
        $student_mark_id = null;
        if (!empty($id)) {
            $decrypted_student_mark_id = get_decrypted_value($id, true);
            $student_mark = $this->getStudentMarkData($decrypted_student_mark_id);
            $student_mark = isset($student_mark[0]) ? $student_mark[0] : [];
            if (!$student_mark) {
                return redirect('admin/student-mark')->withError('Student Marks mapping not found!');
            }
            $encrypted_student_mark_id = get_encrypted_value($student_mark['student_mark_id'], true);
            $save_url = url('admin/student-mark/save/' . $encrypted_student_mark_id);
            $submit_button = 'Update';
            $student_mark_id = $decrypted_student_mark_id;
            $class_id = $student_mark['class_id'];
            $exam_id = $student_mark['exam_id'];
            $session_id = $student_mark['session_id'];
            $section_id = $student_mark['section_id'];
            $arr_session = get_session();
        } else {
            $student_mark['student_mark_id'] = null;
            $save_url = url('admin/student-mark/save/');
            $submit_button = 'Save';
            $arr_session = get_session('current'); // add marks of current marks
            $student_mark['session_id'] = key($arr_session); // selected current session
//                p($arr_session);
        }
        $student_mark['arr_class'] = add_blank_option(get_all_classes($class_id), '--Select class --');
        $student_mark['arr_section'] = add_blank_option(get_class_section($class_id, $session_id, $section_id), '--Select section--');
        $student_mark['arr_exam'] = add_blank_option(get_all_exam($exam_id, $class_id, $session_id, $section_id), '--Select exam--');
        $student_mark['arr_result_in'] = get_result_in();
        $student_mark['arr_session'] = add_blank_option($arr_session, '-- Academic year --');
        $student_mark['arr_student_mark_mark'] = [];

        $data = array(
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'student_mark' => $student_mark,
            'redirect_url' => url('admin/student-mark/'),
        );
        return view('backend.student-mark.add')->with($data);
    }

    public function save(Request $request, $id = NULL)
    {
//            p($request->input());
        $class_id = Input::get('class_id');
        $section_id = Input::get('section_id');
        $exam_id = Input::get('exam_id');
        $session_id = Input::get('session_id');
        $decrypted_student_mark_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $student_mark = StudentMark::find($decrypted_student_mark_id);

            if (!$student_mark) {
                return redirect('/admin/student-mark/')->withError('Student Marks not found!');
            }
            $success_msg = 'Student Marks  updated successfully!';
        } else {
            $student_mark = new StudentMark;
            $success_msg = 'Student Marks  saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'exam_id' => 'required|unique:student_marks,exam_id,' . $decrypted_student_mark_id . ',student_mark_id,session_id,' . $session_id . ',class_id,' . $class_id . ',section_id,' . $section_id,
            'session_id' => 'required',
            'class_id' => 'required',
            'result_in' => 'required',
        ]);
        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction(); //Start transaction!
            try {
                $student_mark->class_id = $class_id;
                $student_mark->section_id = $section_id;
                $student_mark->exam_id = $exam_id;
                $student_mark->session_id = $session_id;
                $student_mark->result_in = Input::get('result_in');
                $student_mark->publish_status = $request->has('publish_status') ? 1 : 0;
                if ($student_mark->save()) {
                    $arr_student_id = Input::get('arr_student_id');
                    $arr_subject_id = array_unique(Input::get('arr_subject_id'));
                    // subject type and exam type are same (written, practical and oral)
                    $arr_exam_type_id = array_unique(Input::get('arr_subject_type_id'));
                    $arr_mark_detail = [];
                    if (!empty($arr_student_id)) {
                        foreach ($arr_student_id as $student_id) {
                            if ($request->has('mark_detail_id_' . $student_id) && !empty(Input::get('mark_detail_id_' . $student_id))) {
                                $mark_detail = StudentMarkDetail::find(Input::get('mark_detail_id_' . $student_id));
                            } else {
                                $mark_detail = new StudentMarkDetail;
                            }
                            $subject_marks = [];
                            foreach ($arr_subject_id as $subject_id) {
                                $min_marks = 0;
                                $max_marks = 0;
                                $arr_exam_type_marks = [];

                                foreach ($arr_exam_type_id as $exam_type_id) {
                                    if ($request->has('obtained_subject_marks_' . $student_id . '_' . $subject_id . '_' . $exam_type_id)) {
                                        $arr_exam_type_marks[$exam_type_id] = Input::get('obtained_subject_marks_' . $student_id . '_' . $subject_id . '_' . $exam_type_id);
                                    }
                                }
                                $arr_exam_type_marks['sub_tot_obtained'] = array_sum($arr_exam_type_marks);


                                $subject_marks[$subject_id] = $arr_exam_type_marks;
                            }
                            $subject_marks['tot_obtained'] = array_sum(array_column($subject_marks, 'sub_tot_obtained'));
                            $mark_detail->student_id = $student_id;
                            $mark_detail->percentage = $request->has('percentage' . $student_id) ? Input::get('percentage' . $student_id) : 0;
                            $mark_detail->rank = $request->has('rank' . $student_id) ? Input::get('rank' . $student_id) : 0;
                            $mark_detail->grade = $request->has('grade' . $student_id) ? Input::get('grade' . $student_id) : 0;
                            $mark_detail->obtained_marks = json_encode($subject_marks);
                            $arr_mark_detail[] = $mark_detail;
                        }
//                            p($arr_mark_detail);
                        if (!empty($arr_mark_detail)) {
                            $student_mark->markDetails()->saveMany($arr_mark_detail);
                        }
                        if ($student_mark->publish_status == 1) {
                            $cron = new CronController;
                            $cron->pushNotificationStudentResult($arr_student_id);
                        }
                    }
                }
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                p($error_message);
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin/student-mark/')->withSuccess($success_msg);
    }

    public function anyData(Request $request)
    {
        $arr_marks = [];
        $arr_status = get_status();
        $limit = Input::get('length');
        $offset = Input::get('start');
        $arr_student_marks = $this->getStudentMarkData($student_mark_id = null, $offset, $limit);
        foreach ($arr_student_marks as $key => $student_marks) {
            $student_marks['publish_status'] = $arr_status[$student_marks['publish_status']];
            $arr_marks[$key] = (object)$student_marks;
        }
        return Datatables::of($arr_marks)
            ->addColumn('action', function ($arr_marks) {
                $encrypted_student_mark_id = get_encrypted_value($arr_marks->student_mark_id, true);
                return '<a title="Edit" id="deletebtn1" href="' . url('admin/student-mark/add/' . $encrypted_student_mark_id) . '" class="btn btn-success centerAlign"><i class="fa fa-edit" ></i></a>';
            })
            ->addColumn('view', function ($arr_marks) {
                $encrypted_student_mark_id = get_encrypted_value($arr_marks->student_mark_id, true);
                return '<a title="View" id="deletebtn1" href="' . url('admin/student-mark/view/' . $encrypted_student_mark_id) . '" class="btn btn-success centerAlign"><i class="fa fa-eye" ></i></a>';
            })
            ->rawColumns(['action', 'view'])->make(true);
    }

    public function getStudentSubjectMark(Request $request)
    {
        $return_data = [];
        $arr_subject_id = [];
        $arr_exam_class = [];
        $arr_student_obtained = [];
        $class_id = Input::get('class_id');
        $exam_id = Input::get('exam_id');
        $session_id = Input::get('session_id');
        $section_id = Input::get('section_id');
        $student_mark_id = Input::get('student_mark_id');
        $status = 'failed';

        $arr_exam_class['exam_subject_marks'] = [];
        $arr_exam_class['subject_marks_criteria'] = [];

        // get marks subject's entered marks means edit case
        $student_marks_exist = DB::table('student_marks as sm')
            ->where('class_id', $class_id)->where('session_id', $session_id)
            ->where('exam_id', $exam_id)->where('section_id', $section_id)
            ->first();
        if (!empty($class_id) && (empty(!$student_mark_id) || empty($student_mark_id) && empty($student_marks_exist->student_mark_id))) {

            // subjects from exam class
            $arr_class_exam_subject_marks = DB::table('exam_classes as ec')
                ->where('ec.class_id', $class_id)
                ->where('ec.session_id', $session_id)
                ->where('ec.section_id', $section_id)
                ->where('ec.exam_id', $exam_id)->first();

            if (!empty($arr_class_exam_subject_marks->exam_class_id)) {
                $exam_subject = (array)$arr_class_exam_subject_marks;
                //get subject from exam-class mapping
                $subject_marks_criteria = json_decode(json_encode(json_decode($exam_subject['subject_marks_criteria'])), true);
                $arr_exam_class['subject_marks_criteria'] = $subject_marks_criteria;
                // get subject id of selected/mapped with exam
                $arr_subject_id = array_keys($subject_marks_criteria);
            }
            if (!empty($arr_subject_id)) {
                // get class's subject list
                $arr_subject_detail = [];
                $arr_subject = DB::table('subjects as s')
                    ->join('class_subjects as cs', 'cs.subject_id', '=', 's.subject_id')
                    ->select('s.subject_id', 's.subject_name', 'cs.unassigned_student_id', 's.subject_code')
                    ->where('cs.session_id', $session_id)
                    ->where('cs.class_id', $class_id)
                    ->where('cs.section_id', $section_id)
                    ->where('s.subject_status', 1)
                    ->whereIn('s.subject_id', $arr_subject_id)
                    ->get();

                if (!empty($arr_subject)) {
                    // conver object into array
                    $arr_subject_detail = json_decode(json_encode(json_decode($arr_subject)), true);
                    // send subject id on node of array
                    $arr_exam_class['exam_subject_marks'] = array_column($arr_subject_detail, null, 'subject_id');
                }
            }
            if (!empty($arr_exam_class)) {
                // get class student
                $table = get_student_table($session_id, 'table');
                $query = DB::table('' . $table . ' as s')
                    ->join('exam_roll_numbers as ern', 'ern.student_id', '=', 's.student_id')
                    ->where('s.current_class_id', $class_id)
                    ->where('s.current_session_id', $session_id)
                    ->where('s.student_left', 'No')
                    ->where('ern.class_id', $class_id)
                    ->where('ern.session_id', $session_id)
                    ->where('s.current_section_id', $section_id)
//                    ->orderBy('s.enrollment_number')
                    ->select('s.enrollment_number', 's.current_section_id', 's.student_id', 's.first_name', 's.middle_name', 's.last_name', DB::raw("CONCAT(s.first_name,' ',s.middle_name,' ',s.last_name) as student_name"), DB::raw("CONCAT(ern.prefix,' ',ern.roll_number) as roll_number"));
                $arr_class_student_data = $query->orderBy('s.first_name', 'ASC')->orderBy('s.middle_name', 'ASC')->orderBy('s.last_name', 'ASC')->get();
                if (!empty($student_mark_id)) {
                    $arr_student_obtained = $this->getStudentMarkData($student_mark_id);
                    $arr_student_obtained = isset($arr_student_obtained[0]['student_obtained_marks']) ? $arr_student_obtained[0]['student_obtained_marks'] : [];
                }
                $arr_exam_class['exam_student_list'] = [];
                foreach ($arr_class_student_data as $student) {
                    $student = (array)$student;
                    $student_obtained = isset($arr_student_obtained[$student['student_id']]) ? $arr_student_obtained[$student['student_id']] : [];
                    $arr_exam_class['exam_student_list'][$student['student_id']] = array(
                        'student_id' => $student['student_id'],
                        'section_id' => $student['current_section_id'],
                        'student_name' => $student['first_name'] . ' ' . $student['middle_name'] . ' ' . $student['last_name'],
                        'enrollment_number' => $student['enrollment_number'],
                        'roll_number' => $student['roll_number'],
                        'student_mark_detail_id' => isset($student_obtained['student_mark_detail_id']) ? $student_obtained['student_mark_detail_id'] : null,
                        'obtained_marks' => isset($student_obtained['obtained_marks']) ? $student_obtained['obtained_marks'] : [],
                    );
                }
            }
            if (!empty($arr_exam_class['exam_student_list'] && $arr_exam_class['exam_subject_marks'])) {
                $status = 'success';
                $data = array(
                    'arr_student_exam' => $arr_exam_class,
                    'arr_subject_type' => get_all_exam_type(),
                );
//                    p($data);
                $return_data = View::make('backend.student-mark.add-subject-marks')->with($data)->render();
            }
        } else {
            $status = 'already_exist';
        }
//            p($return_data);
        return response()->json(array('status' => $status, 'data' => $return_data));
    }

    function getStudentMarkData($student_mark_id = null, $offset = null, $limit = null)
    {
        $session = get_current_session();
        $session_id = $session['session_id'];
        $arr_student_mark = [];
        $arr_student_mark_data = StudentMark::where('mark_status', 1)
            ->where(function ($query) use ($student_mark_id) {
                if (!empty($student_mark_id)) {
                    $query->where('student_mark_id', $student_mark_id);
                }
            })->with(['markClass' => function ($q) {
                $q->select(['class_id', 'class_name']);
            }])
            ->with(['markExam' => function ($q) {
                $q->select(['exam_id', 'exam_name']);
            }])
            ->with(['markSection' => function ($q) {
                $q->select(['section_id', 'section_name']);
            }])
            ->with(['markSession' => function ($q) {
                $q->select(['session_id', 'session_year']);
            }])
            ->with(['markDetails' => function ($q) {
                $q->select(['student_mark_id', 'student_mark_detail_id', 'student_id', 'obtained_marks']);
            }])
            ->select('*', DB::raw('CASE WHEN publish_status = 1 THEN "Yes" ELSE "No" END AS publish'))
            ->where('session_id', $session_id)
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
            ->get();
//            p($arr_student_mark_data);
        if (!empty($arr_student_mark_data)) {
            $arr_result_in = get_result_in();
            foreach ($arr_student_mark_data as $key => $mark_data) {
                $student_mark = array(
                    'student_mark_id' => $mark_data['student_mark_id'],
                    'session_id' => $mark_data['session_id'],
                    'class_id' => $mark_data['class_id'],
                    'section_id' => $mark_data['section_id'],
                    'exam_id' => $mark_data['exam_id'],
                    'result_in' => $mark_data['result_in'],
                    'result_in_name' => $arr_result_in[$mark_data['result_in']],
                    'publish_status' => $mark_data['publish_status'],
                    'publish' => $mark_data['publish'],
                    'exam_name' => $mark_data['markExam']['exam_name'],
                    'class_name' => $mark_data['markClass']['class_name'],
                    'section_name' => $mark_data['markSection']['section_name'],
                    'session_year' => $mark_data['markSession']['session_year'],
                    'student_obtained_marks' => [],
                );
                foreach ($mark_data['markDetails'] as $key => $mark_detail) {
                    $student_mark['student_obtained_marks'][$mark_detail['student_id']] = array(
                        'student_mark_detail_id' => $mark_detail['student_mark_detail_id'],
                        'student_id' => $mark_detail['student_id'],
                        'obtained_marks' => json_decode(json_encode(json_decode($mark_detail['obtained_marks'])), true),
                    );
                }
                $arr_student_mark[] = $student_mark;
            }
        }
        return $arr_student_mark;
    }

    function getMarksGrade()
    {
        $grade = get_marks_grade();
        $status = 'success';
        return response()->json(array('status' => $status, 'grade' => $grade));
    }

    public function viewData(Request $request, $id = NULL)
    {
        $data = [];
        $session = get_current_session();
        if (!empty($id) && !empty($session)) {
            $student_mark_id = get_decrypted_value($id, true);

            $session_id = $session['session_id'];

            $marks_data = StudentMark::where('mark_status', 1)
                ->where(function ($query) use ($student_mark_id) {
                    if (!empty($student_mark_id)) {
                        $query->where('student_mark_id', $student_mark_id);
                    }
                })->with(['markClass' => function ($q) {
                    $q->select(['class_id', 'class_name']);
                }])
                ->with(['markExam' => function ($q) {
                    $q->select(['exam_id', 'exam_name']);
                }])
                ->with(['markSection' => function ($q) {
                    $q->select(['section_id', 'section_name']);
                }])
                ->with(['markSession' => function ($q) {
                    $q->select(['session_id', 'session_year']);
                }])
                ->with(['markDetails' => function ($q) {
                    $q->select(['student_mark_id', 'student_mark_detail_id', 'student_id', 'obtained_marks']);
                }])
                ->with(['markDetails.markStudent.getRollNo' => function ($q) {
//                                $q->select(['student_id', 'first_name', 'last_name', 'middle_name', 'enrollment_number']);
                }])
                ->select('*', DB::raw('CASE WHEN publish_status = 1 THEN "Yes" ELSE "No" END AS publish'))
                ->where('session_id', $session_id)
                ->first()->toArray();
            if (!$marks_data) {
                return redirect('admin/student-mark')->withError('Student Marks mapping not found!');
            }

            $class_id = $marks_data['class_id'];
            $section_id = $marks_data['section_id'];
            $exam_id = $marks_data['exam_id'];
            // subjects from exam class
            $arr_class_exam_subject_marks = DB::table('exam_classes as ec')
                ->where('ec.session_id', $session_id)
                ->where('ec.class_id', $class_id)
                ->where('ec.section_id', $section_id)
                ->where('ec.exam_id', $exam_id)->first();

            if ($arr_class_exam_subject_marks->exam_class_id) {
                $exam_subject = (array)$arr_class_exam_subject_marks;
                //get subject from exam-class mapping
                $subject_marks_criteria = json_decode(json_encode(json_decode($exam_subject['subject_marks_criteria'])), true);
                // get subject id of selected/mapped with exam
                $arr_subject_id = array_keys($subject_marks_criteria);
            }

            if (!empty($arr_subject_id)) {
                // get class's subject list
                $arr_subject_detail = [];
                $arr_subject = DB::table('subjects as s')
                    ->join('class_subjects as cs', 'cs.subject_id', '=', 's.subject_id')
                    ->select('s.subject_id', 's.subject_name', 'cs.unassigned_student_id', 's.subject_code')
                    ->where('cs.session_id', $session_id)
                    ->where('cs.class_id', $class_id)
                    ->where('cs.section_id', $section_id)
                    ->where('s.subject_status', 1)
                    ->whereIn('s.subject_id', $arr_subject_id)->get();

                if (!empty($arr_subject)) {
                    // conver object into array
                    $arr_subject_detail = json_decode(json_encode(json_decode($arr_subject)), true);
                    // send subject id on node of array
                    $exam_subject_marks = array_column($arr_subject_detail, null, 'subject_id');
                }
            }
            $arr_student_makrs_data['exam_subject_marks'] = $exam_subject_marks;
            $arr_student_makrs_data['subject_marks_criteria'] = $subject_marks_criteria;
            $arr_student_makrs_data['arr_marks'] = $marks_data;
            $data = array(
                'arr_student' => $arr_student_makrs_data,
                'arr_subject_type' => get_all_exam_type(),
                'redirect_url' => url('admin/student-mark/'),
            );
        }
        return view('backend.student-mark.view-marks')->with($data);
    }

    /*
     * Student Exam Report
     */

    public function ExamReport()
    {
        $data = array(
            'arr_class' => add_blank_option(get_all_classes(), '--select class--'),
            'arr_section' => add_blank_option(get_class_section(null, null, null), '--select section--'),
            'arr_exam' => add_blank_option(get_all_exam(null, null, null), '--select exam --'),
            'session' => db_current_session(),
        );
//	p($data);
        return view('backend.report.exam-report')->with($data);
    }

    /*
     * Student Detail Report Data
     */

    public function examReportData(Request $request)
    {
        $arr_student_data = [];
        $arr_marks_data = [];
        $class_id = Input::get('class_id');
        $session_id = Input::get('session_id');
        $section_id = Input::get('section_id');
        $exam_id = Input::get('exam_id');
        $type = Input::get('type');
        $arr_checked_student = Input::get('arr_checked_student');
        if (!empty($session_id) && !empty($class_id) && !empty($section_id) && !empty($exam_id)) {
            $table = get_student_table($session_id);
            $arr_marks_data = DB::table('student_mark_details as smd')
                ->join('student_marks as sm', 'smd.student_mark_id', '=', 'sm.student_mark_id')
                ->join('exam_roll_numbers as ern', 'smd.student_id', '=', 'ern.student_id')
                ->join("$table", 'smd.student_id', '=', 's.student_id')
                ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
                ->join('create_exams as ce', 'sm.exam_id', '=', 'ce.exam_id')
                ->join('classes as c', 'sm.class_id', '=', 'c.class_id')
                ->join('sections as se', 'sm.section_id', '=', 'se.section_id')
                ->join('exam_classes as ec', 'sm.exam_id', '=', 'ec.exam_id')
                ->select('se.section_name', 'c.class_name', 's.first_name', 's.middle_name', 's.last_name', 's.enrollment_number', 'ern.prefix', 'ern.roll_number', 'ec.subject_marks_criteria', 'ce.exam_name', 'smd.obtained_marks', 'smd.percentage', 'sm.exam_id', 'sm.result_in', 'sm.publish_status', 'smd.student_id', 'smd.rank', 'smd.grade', 's.dob')
//		    ->where('smd.student_id', $student_id)
                ->addSelect(DB::raw("CONCAT(ern.prefix,'',ern.roll_number) as roll_number"), DB::raw("CONCAT(s.first_name,' ',s.middle_name,' ',s.last_name) as student_name"))
                ->addSelect(DB::raw("CONCAT(sp.father_first_name,' ',sp.father_middle_name,' ',sp.father_last_name) as father_name"))
                ->addSelect(DB::raw("CONCAT(sp.mother_first_name,' ',sp.mother_middle_name,' ',sp.mother_last_name) as mother_name"))
                ->addSelect("sp.father_first_name", "sp.father_middle_name", "sp.father_last_name")
                ->addSelect("sp.mother_first_name", "sp.mother_middle_name", "sp.mother_last_name")
                ->where('sm.session_id', $session_id)
                ->where('sm.class_id', $class_id)
                ->where('sm.section_id', $section_id)
                ->where('ec.session_id', $session_id)
                ->where('ec.class_id', $class_id)
                ->where('ec.section_id', $section_id)
                ->where('ern.class_id', $class_id)
                ->where('sm.exam_id', $exam_id)
                ->where('ce.exam_id', $exam_id)
                ->where('ec.exam_id', $exam_id)
//                ->where('s.student_id',272) // Testing data
                ->where(function ($q) use ($type, $arr_checked_student) {
                    if ($type == 'print' && !empty($arr_checked_student)) {
                        $q->whereIn('smd.student_id', $arr_checked_student);
                        $q->whereIn('s.student_id', $arr_checked_student);
                    }
                })
                ->where('sm.publish_status', 1)
                ->orderBy('s.first_name', 'ASC')
                ->orderBy('s.middle_name', 'ASC')
                ->orderBy('s.last_name', 'ASC')
                ->get();
            $arr_subject_type_data = [];
            $arr_subject_marks_criteria = [];
            if (count($arr_marks_data) > 0) {
                $arr_subject_type_data = isset($arr_marks_data[0]->obtained_marks) ? json_decode($arr_marks_data[0]->obtained_marks, true) : [];
                $arr_subject_marks_criteria = isset($arr_marks_data[0]->subject_marks_criteria) ? json_decode($arr_marks_data[0]->subject_marks_criteria, true) : [];
            }
            $data = array(
                'arr_marks' => $arr_marks_data,
                'marks_criteria' => $arr_subject_marks_criteria,
                'marks_criteria_p' => $arr_subject_marks_criteria,
                'arr_subjet_type_data' => $arr_subject_type_data, //subject and subject type id
                'arr_subject' => get_class_section_subject($session_id, $class_id, $section_id),
                'arr_subject_type' => get_all_exam_type(),
                'arr_grade' => get_marks_grade($session_id),
            );
        }
        if ($type == 'print' && !empty($arr_checked_student)) {
            // p($data);
            $return_data = View::make('backend.report.exam-report-print-data')->with($data)->render();
//            return $return_data;
        } else {
            $return_data = View::make('backend.report.exam-report-data')->with($data)->render();
        }
        return response()->json(array('status' => 'success', 'data' => $return_data));
    }

    /*
     * Student Exam Report
     */

    public function examMarksheet()
    {
        $data = array(
            'arr_class' => add_blank_option(get_all_classes(), '--select class--'),
            'arr_section' => add_blank_option(get_class_section(null, null, null), '--select section--'),
            'arr_exam' => add_blank_option(get_all_exam(null, null, null), '--select exam --'),
            'session' => db_current_session(),
        );
        return view('backend.report.exam-marksheet')->with($data);
    }

    /*
     * Student Marks Report Data
     */

    public function examMarksheetData(Request $request)
    {
        $final_marks_data = [];
        $class_id = Input::get('class_id');
        $session_id = Input::get('session_id');
        $section_id = Input::get('section_id');

        if (!empty($class_id) && !empty($session_id) && !empty($section_id)) {
            $table = get_student_table($session_id);
            $arr_marks_data = DB::table('student_mark_details as smd')
                ->join('student_marks as sm', 'smd.student_mark_id', '=', 'sm.student_mark_id')
                ->join('exam_roll_numbers as ern', 'smd.student_id', '=', 'ern.student_id')
                ->join("$table", 'smd.student_id', '=', 's.student_id')
                ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
                ->join('create_exams as ce', 'sm.exam_id', '=', 'ce.exam_id')
                ->join('exam_classes as ec', 'sm.exam_id', '=', 'ec.exam_id')
                ->join('classes as c', 'sm.class_id', '=', 'c.class_id')
                ->join('sections as se', 'sm.section_id', '=', 'se.section_id')
                ->select('c.class_name', 'se.section_name', 's.first_name', 's.middle_name', 'sp.father_contact_number', 's.enrollment_number', 'ern.prefix', 'ern.roll_number', 'ec.subject_marks_criteria', 'ce.exam_name', 'smd.obtained_marks', 'smd.percentage', 'sm.exam_id', 'sm.result_in', 'sm.publish_status', 'smd.student_id', 'smd.rank', 'smd.grade', 's.dob')
                ->addSelect(DB::raw("CONCAT(ern.prefix,'',ern.roll_number) as roll_number"), DB::raw("CONCAT(COALESCE(s.first_name,''),' ',COALESCE(s.middle_name,''),' ', COALESCE(s.last_name,'')) as student_name"))
                ->addSelect(DB::raw("CONCAT(COALESCE(sp.father_first_name,''),' ',COALESCE(sp.father_middle_name,''),' ',COALESCE(sp.father_last_name,'')) as father_name"))
                ->addSelect(DB::raw("CONCAT(COALESCE(sp.mother_first_name,''),' ',COALESCE(sp.mother_middle_name,''),' ',COALESCE(sp.mother_last_name,'')) as mother_name"))
                ->where('sm.session_id', $session_id)
                ->where('sm.class_id', $class_id)
                ->where('sm.section_id', $section_id)
                ->where('ec.session_id', $session_id)
                ->where('ec.class_id', $class_id)
                ->where('ec.section_id', $section_id)
                ->where('ern.class_id', $class_id)
                ->where('sm.publish_status', 1)
                ->where('ce.in_final_marksheet', 1)
//                ->orderBy('sm.exam_id')
//                ->orderBy('smd.student_id')
                ->orderBy('student_name')
                ->get();
            $keys = [];
            $arr_grade = get_marks_grade();

            $classes = Classes::get()->pluck("class_name","class_id")->toArray();

            foreach ($arr_marks_data as $marks_data) {
                $marks_data = (array)$marks_data;
                if (!in_array($marks_data['student_id'], $keys)) {
                    $arr_exam = [];
                }

                $subject_marks_criteria = json_decode($marks_data['subject_marks_criteria'], true);
                $obtained_marks = json_decode($marks_data['obtained_marks'], true);
                foreach ($obtained_marks as $key => &$obtained_mark) {
                    if(gettype($key) == "integer"){
                        $marks_criteria = $subject_marks_criteria[$key];
                        $subject_grade = "-";
                        if ($obtained_mark['sub_tot_obtained'] > 0) {
                            $per = round($obtained_mark['sub_tot_obtained'] * 100 / $marks_criteria['total_max_marks']);
                            foreach ($arr_grade as $grade) {
                                if (in_array($per, explode(',', $grade['range']))) {
                                    $subject_grade = $grade['grade_name'];
                                }
                            }
                        }
                        $obtained_mark['subject_grade'] = $subject_grade;
                    }
                }

                $promoted_to = !empty(array_search($marks_data['class_name'], $classes)) ? array_search($marks_data['class_name'], $classes) : NULL;

                $arr_exam[$marks_data['exam_id']] = array(
                    'exam_name' => $marks_data['exam_name'],
                    'subject_marks_criteria' => $subject_marks_criteria,
                    'obtained_marks' => $obtained_marks,
                    'grade' => $marks_data['grade'],
                    'percentage' => $marks_data['percentage'],
                );
                $final_marks_data[$marks_data['student_id']] = array(
                    'student_id' => $marks_data['student_id'],
                    'student_name' => $marks_data['student_name'],
                    'father_name' => $marks_data['father_name'],
                    'mother_name' => $marks_data['mother_name'],
                    'roll_number' => $marks_data['roll_number'],
                    'class_name' => $marks_data['class_name'],
                    'section_name' => $marks_data['section_name'],
                    'dob' => $marks_data['dob'],
                    'enrollment_number' => $marks_data['enrollment_number'],
                    'father_contact_number' => $marks_data['father_contact_number'],
//                    'arr_exam' => $arr_exam,
                    'promoted_to' => !empty($promoted_to) && isset($classes[$promoted_to + 1]) ? $classes[$promoted_to + 1] : "-"
                );
                $keys = array_keys($final_marks_data);
            }
        }
        return Datatables::of($final_marks_data)->make(true);
    }

    /*
     * Student Marks Sheet Print Data
     */

    public function examMarksheetPrint(Request $request)
    {
        $return_data = [];
//        $data['data'] = $request->input();
        $data['session_id'] = $request->input('session_id');
        $data['class_id'] = $request->input('class_id');
        $data['section_id'] = $request->input('section_id');
        $data['print_type'] = $request->input('print_type');
        $result_date = isset($request->result_date) ? str_replace("/","-",$request->input('result_date')) : date("d-m-Y");
        $reopen_date = isset($request->reopen_date) ? str_replace("/","-",$request->input('reopen_date')) : date("d-m-Y");
        $result_date_obj = date_create($result_date);
        $data['result_date'] = date_format($result_date_obj,'jS F, Y');
        $reopen_date_obj = date_create($reopen_date);
        $data['reopen_date'] = date_format($reopen_date_obj,'jS F, Y');
        $data['data'] = [];

        if($request->input('session_id') != "" && $request->input('class_id') != "" && $request->input('section_id') != ""){
            $table = get_student_table($data['session_id']);
            $arr_marks_data = DB::table('student_mark_details as smd')
                ->join('student_marks as sm', 'smd.student_mark_id', '=', 'sm.student_mark_id')
                ->join('exam_roll_numbers as ern', 'smd.student_id', '=', 'ern.student_id')
                ->join("$table", 'smd.student_id', '=', 's.student_id')
                ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
                ->join('create_exams as ce', 'sm.exam_id', '=', 'ce.exam_id')
                ->join('exam_classes as ec', 'sm.exam_id', '=', 'ec.exam_id')
                ->join('classes as c', 'sm.class_id', '=', 'c.class_id')
                ->join('sections as se', 'sm.section_id', '=', 'se.section_id')
                ->select('c.class_name','c.class_order', 'se.section_name', 's.first_name', 's.middle_name', 'sp.father_contact_number', 's.enrollment_number', 'ern.prefix', 'ern.roll_number', 'ec.subject_marks_criteria', 'ce.exam_name', 'ce.exam_alias', 'smd.obtained_marks', 'smd.percentage', 'sm.exam_id', 'sm.result_in', 'sm.publish_status', 'smd.student_id', 'smd.rank', 'smd.grade', 's.dob')
                ->addSelect(DB::raw("CONCAT(ern.prefix,'',ern.roll_number) as roll_number"), DB::raw("CONCAT(COALESCE(s.first_name,''),' ',COALESCE(s.middle_name,''),' ', COALESCE(s.last_name,'')) as student_name"))
                ->addSelect(DB::raw("CONCAT(COALESCE(sp.father_first_name,''),' ',COALESCE(sp.father_middle_name,''),' ',COALESCE(sp.father_last_name,'')) as father_name"))
                ->addSelect(DB::raw("CONCAT(COALESCE(sp.mother_first_name,''),' ',COALESCE(sp.mother_middle_name,''),' ',COALESCE(sp.mother_last_name,'')) as mother_name"))
                ->where('sm.session_id', $data['session_id'])
                ->where('sm.class_id', $data['class_id'])
                ->where('sm.section_id', $data['section_id'])
                ->where('ec.session_id', $data['session_id'])
                ->where('ec.class_id', $data['class_id'])
                ->where('ec.section_id', $data['section_id'])
                ->where('ern.class_id', $data['class_id'])
//                ->where('sm.publish_status', 1)
                ->where('ce.in_final_marksheet', 1)
                ->whereIn('smd.student_id', $request->input('data'))
//                ->orderBy('sm.exam_id')
//                ->orderBy('smd.student_id')
                ->orderBy('student_name')
                ->orderBy('ce.sequence')
                ->get();
            $keys = [];
            $arr_grade = get_marks_grade();

            $classes = Classes::get()->pluck("class_name","class_id")->toArray();
            $classes_order = Classes::get()->pluck("class_name","class_order")->toArray();
            $student_data = [];
            foreach ($arr_marks_data as $marks_data) {
                $marks_data = (array)$marks_data;
                if (!in_array($marks_data['student_id'], $keys)) {
                    $arr_exam = [];
                }

                $subject_marks_criteria = json_decode($marks_data['subject_marks_criteria'], true);
                $obtained_marks = json_decode($marks_data['obtained_marks'], true);
                foreach ($obtained_marks as $key => &$obtained_mark) {
                    if(gettype($key) == "integer"){
                        $marks_criteria = $subject_marks_criteria[$key];
                        $subject_grade = "-";
                        if ($obtained_mark['sub_tot_obtained'] > 0) {
                            $per = round($obtained_mark['sub_tot_obtained'] * 100 / $marks_criteria['total_max_marks']);
                            foreach ($arr_grade as $grade) {
                                if (in_array($per, explode(',', $grade['range']))) {
                                    $subject_grade = $grade['grade_name'];
                                }
                            }
                        }
                        $obtained_mark['subject_grade'] = $subject_grade;
                    }
                }

                $promoted_to = !empty(array_search($marks_data['class_name'], $classes)) ? array_search($marks_data['class_name'], $classes) : NULL;

                $arr_exam[$marks_data['exam_id']] = array(
                    'exam_name' => $marks_data['exam_name'],
                    'exam_alias' => $marks_data['exam_alias'],
                    'subject_marks_criteria' => $subject_marks_criteria,
                    'obtained_marks' => $obtained_marks,
                    'grade' => $marks_data['grade'],
                    'percentage' => $marks_data['percentage'],
                );
                $student_data[$marks_data['student_id']] = array(
                    'student_id' => $marks_data['student_id'],
                    'student_name' => $marks_data['student_name'],
                    'father_name' => $marks_data['father_name'],
                    'mother_name' => $marks_data['mother_name'],
                    'roll_number' => $marks_data['roll_number'],
                    'class_name' => $marks_data['class_name'],
                    'section_name' => $marks_data['section_name'],
                    'dob' => $marks_data['dob'],
                    'enrollment_number' => $marks_data['enrollment_number'],
                    'father_contact_number' => $marks_data['father_contact_number'],
                    'arr_exam' => $arr_exam,
//                    'promoted_to' => !empty($promoted_to) && isset($classes[$promoted_to + 1]) ? $classes[$promoted_to + 1] : "-"
                    'promoted_to' => !empty($marks_data['class_order']) && isset($classes_order[$marks_data['class_order'] + 1]) ? $classes_order[$marks_data['class_order'] + 1] : "-"
                );
                $keys = array_keys($student_data);
            }
            $data['data']['data'] = $student_data;

            if (!empty($data) && $data['print_type'] == "GradeOnly") {
                $return_data = View::make('backend.report.exam-marksheet-print-data')->with($data)->render();
            }elseif (!empty($data) && $data['print_type'] == "MarksOnly") {
                $return_data = View::make('backend.report.exam-marksheet-print-marks-data')->with($data)->render();
            }elseif (!empty($data) && $data['print_type'] == "Both") {
                $return_data = View::make('backend.report.exam-marksheet-print-both-data')->with($data)->render();
            }else{
                return response()->json(array('status' => 'error', 'message' => "Invalid print type"));
            }
            return response()->json(array('status' => 'success', 'data' => $return_data));
        }else{
            return response()->json(array('status' => 'error', 'message' => "Invalid data selection type"));
        }
    }
}