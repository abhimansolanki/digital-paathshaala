<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\Fee;
use App\Model\backend\FeeCircular;
use App\Model\backend\StudentFeeReceiptDetail;
use App\Model\backend\FeeDetail;
use Validator;
use Illuminate\Support\Facades\Input;
use Datatables;
use Illuminate\Support\Facades\View as View;
use Illuminate\Support\Facades\DB;

class FeeController extends Controller
{

    //
    public function __construct()
    {

    }

    /*
     * Show the fee list
     */

    public function index()
    {

        $data = array(
            'redirect_url' => url('admin/fee-parameter/add'),
            'page_title' => trans('language.list_school'),
            'arr_class' => add_blank_option(get_all_classes(), '-- Select  --'),
            'arr_session' => add_blank_option(get_session(), '-- Select  --'),
            'session' => db_current_session(),
        );

        $session = db_current_session();
        $class_id = null;
        $session_id = $session['session_id'];
        $data['disabled'] = true;
        $data['class_data'] = $this->ClassWiseFee($session_id, $class_id);
//	p($data['class_data']);
        return view('backend.fee-parameter.index')->with($data);
    }

    /* Get class wise fee for index/listing page */

    public function ClassWiseFee($session_id = null, $class_id = null)
    {
        $data = [];

        if (!empty($session_id)) {
            $student_table = get_student_table($session_id, 'table');
            $arr_class_student_fee = DB::table('classes')->select('class_id', 'class_name')
                ->where(function ($q) use ($class_id) {
                    if (!empty($class_id)) {
                        $q->where('class_id', $class_id);
                    }
                })
                ->orderBy('class_order')
                ->get();
//		DB::table($student_table)
//		->join('classes as c', 's.current_class_id', '=', 'c.class_id')
//		->where('current_session_id', $session_id)
//		->where(function($q) use ($class_id)
//		{
//		    if (!empty($class_id))
//		    {
//		        $q->where('current_class_id', $class_id);
//		    }
//		})
//		->groupBy('current_class_id')
//		->select('current_class_id', 'class_name', DB::raw('SUM(previous_due_fee) as total_previous_due')
//		        , DB::raw('COUNT(*) AS total_student'), DB::raw('SUM(CASE WHEN new_student = 1 THEN 1 ELSE 0 END )AS new_student'), DB::raw('SUM(CASE WHEN new_student = 0 THEN 1 ELSE 0 END )AS old_student'))
//		->get();
            $arr_class_student_fee = json_decode($arr_class_student_fee, true);
            $arr_class_student_fee = array_column($arr_class_student_fee, null, 'class_id');

            $arr_fee_data = DB::table('fees as f')
                ->join('fee_types as ft', 'f.fee_type_id', '=', 'ft.fee_type_id')
                ->join('fee_circulars as fc', 'f.fee_circular_id', '=', 'fc.fee_circular_id')
                ->select('f.is_fixed', 'f.fee_id', 'f.fee_type_id', 'f.fee_circular_id', 'f.apply_to', 'f.student_type_id', 'f.total_fee', 'f.class_id', 'ft.fee_type', 'fc.fee_circular')
                ->where(function ($q) use ($class_id) {
                    if (!empty($class_id)) {
                        $q->where('class_id', $class_id);
                        $q->orWhere('class_id', NULL);
                    }
                })
                ->where('f.fee_type_id', '!=', 2)
                ->where('session_id', $session_id)->get()->toArray();
//	    p($arr_fee_data);
            $other_fee = [];
            $arr_fee_type = [];
            $arr_class_fee = [];
            $app_access_fee = \Config::get('custom.app_access_fee');
            $fee_apply = \Config::get('custom.fee_apply_to');
            $fee_circular = \Config::get('custom.fee_circular');
            foreach ($arr_fee_data as $key => $fee_data) {
                $fee_data = (array)$fee_data;
                if (!empty($fee_data['class_id'])) {
                    if (!in_array($fee_data['class_id'], array_keys($arr_class_fee))) {
                        $arr_fee_type = [];
                    }
                    $fee_data['apply_to'] = $fee_apply[$fee_data['apply_to']];
                    $fee_data['fee_circular'] = $fee_circular[$fee_data['fee_circular']];
                    $encrypted_fee_id = get_encrypted_value($fee_data['fee_id'], true);
                    $action = '';
//		p($fee_data['fee_type_id']);

                    if ($fee_data['is_fixed'] == 1) {
                        $action = '<a title="Edit" id="deletebtn1" href="fee-parameter/add/' . $encrypted_fee_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>';
                    } else {
                        $action = '<a title="Edit" id="deletebtn1" href="fee-parameter/add/' . $encrypted_fee_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . '<button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $fee_data['fee_id'] . '"><i class="fa fa-trash"></i></button>';
                    }

                    $fee_data['action'] = $action;
                    $arr_fee_type[] = $fee_data;
                    $arr_class_fee[$fee_data['class_id']] = $arr_fee_type;
                } else {
                    $other_fee[] = $fee_data;
                }
            }
//p($other_fee);
            $arr_class_final_fee = [];
            if (!empty($arr_class_student_fee)) {
                foreach ($arr_class_student_fee as $class_id => $class_data) {
                    $arr_other_fee_data = [];
                    foreach ($other_fee as $other_fee_data) {
                        if ($other_fee_data['fee_type_id'] == 1) {
//		        $other_fee_data['total_fee'] = $app_access_fee * $class_data['total_student'];
                            $other_fee_data['total_fee'] = $app_access_fee;
                        } else if ($other_fee_data['apply_to'] == 'all_student') {
//		        * $class_data['total_student']
                            $other_fee_data['total_fee'] = $other_fee_data['total_fee'];
                        } else if ($other_fee_data['apply_to'] == 'new_student') {
//		        * $class_data['new_student']
                            $other_fee_data['total_fee'] = $other_fee_data['total_fee'];
                        }

                        $other_fee_data['apply_to'] = $fee_apply[$other_fee_data['apply_to']];
                        $other_fee_data['fee_circular'] = $fee_circular[$other_fee_data['fee_circular']];
                        $encrypted_fee_id = get_encrypted_value($other_fee_data['fee_id'], true);
                        $action = '';
                        if ($other_fee_data['fee_type_id'] != 1) {
                            if ($other_fee_data['is_fixed'] == 1) {
                                $action = '<a title="Edit" id="deletebtn1" href="fee-parameter/add/' . $encrypted_fee_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>';
                            } else {
                                $action = '<a title="Edit" id="deletebtn1" href="fee-parameter/add/' . $encrypted_fee_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                                    . '<button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $other_fee_data['fee_id'] . '"><i class="fa fa-trash"></i></button>';
                            }
                        }
                        $other_fee_data['action'] = $action;

                        $arr_other_fee_data[] = $other_fee_data;
                    }
//		p($arr_other_fee_data);
                    $main_fee_class = isset($arr_class_fee[$class_id]) ? $arr_class_fee[$class_id] : [];
                    $class_all_fee = array_merge($main_fee_class, $arr_other_fee_data);
                    $arr_class_final_fee['class_fee'][$class_id] = array(
                        'class_name' => $class_data['class_name'],
                        'arr_fee' => $class_all_fee,
                    );
                }
            }
//	p($arr_class_final_fee);
            $data = View::make('backend.fee-parameter.index-data')->with($arr_class_final_fee)->render();
        }
        return $data;
    }

    /* Get class wise fee for filter */

    public function getClassFee(Request $request)
    {
        $session_id = Input::get('session_id');
        $class_id = Input::get('class_id');
        $data = $this->ClassWiseFee($session_id, $class_id);
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    /*
     * load fee form in add and edit case
     */

    public function add(Request $request, $id = NULL)
    {

        $fee = [];
        $data = [];
        $fee_circular_id = null;
        if (!empty($id)) {
            $decrypted_fee_id = get_decrypted_value($id, true);
            $fee = $this->getFeeData($decrypted_fee_id);
            $fee = isset($fee[0]) ? $fee[0] : [];
            if (!$fee) {
                return redirect('admin/fee-parameter')->withError('Fee Parameter not found!');
            }
            $encrypted_fee_id = get_encrypted_value($fee['fee_id'], true);
            $page_title = 'Edit Fee Parameter';
            $save_url = url('admin/fee-parameter/save/' . $encrypted_fee_id);
            $submit_button = 'Update';
            if ($fee['fee_type_id'] == 1 || $fee['fee_type_id'] == 2) {
                $fee_circular_id = 1;
            }
        } else {
            $page_title = 'Create Fee Parameter';
            $save_url = url('admin/fee-parameter/save');
            $submit_button = 'Save';
        }
        $arr_fee_apply_to = \Config::get('custom.fee_apply_to');
        $arr_fine_type = \Config::get('custom.fine_type');
        $fee['arr_fee_apply_to'] = $arr_fee_apply_to;
        $fee['arr_fine_type'] = $arr_fine_type;


        $arr_class = get_all_classes();
        $root_id = null;
        $arr_fee_type = get_fee_type($root_id);
        $arr_session = get_session();
        $arr_months = get_current_session_months();
        $arr_fee_circular = get_fee_circular($fee_circular_id);
        $fee['arr_session'] = add_blank_option($arr_session, '--Select session --');
        $fee['arr_class'] = add_blank_option($arr_class, '--Select class --');
        $fee['arr_fee_type'] = add_blank_option($arr_fee_type, '--Select fee type --');
        $fee['arr_fee_circular'] = add_blank_option($arr_fee_circular, '--Select fee circular --');
        $fee['arr_months'] = add_blank_option($arr_months, '--Select month--');
        $arr_student_type = get_student_type();
        $fee['session'] = db_current_session();
        if (isset($arr_student_type[3])) {
            unset($arr_student_type[3]);
        }
        $add_custom_type[0] = 'Both';
        $arr_student_type = $add_custom_type + $arr_student_type;
        $fee['arr_student_type'] = add_blank_option($arr_student_type, '--Select student type --');
        $data = array(
            'page_title' => $page_title,
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'fee' => $fee,
            'redirect_url' => url('admin/fee-parameter'),
        );
        return view('backend.fee-parameter.add')->with($data);
    }

    /*
     * Add/Edit fee and fee details table
     */

    public function save(Request $request, $id = NULL)
    {
        $user_type_id = null;
        $decrypted_fee_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $fee = Fee::find($decrypted_fee_id);
            if (!$fee) {
                return redirect('/admin/fee-parameter/')->withError('Fee not found!');
            }
            $success_msg = 'Fee updated successfully!';
        } else {
            $fee = new Fee;
            $success_msg = 'Fee saved successfully!';
        }

        $session_id = Input::get('session_id');
        $class_id = Input::get('class_id');
        $arr_input = [
            'session_id' => 'required',
            'fee_circular_id' => 'required',
            'fine_date' => 'required',
            'apply_to' => 'required',
        ];
        if (!empty($class_id)) {
            $arr_input['fee_type_id'] = 'required|unique:fees,fee_type_id,' . $decrypted_fee_id . ',fee_id,session_id,' . $session_id . ',class_id,' . $class_id;
        } else {
            $arr_input['fee_type_id'] = 'required|unique:fees,fee_type_id,' . $decrypted_fee_id . ',fee_id,session_id,' . $session_id;
        }
        $validatior = Validator::make($request->all(), $arr_input);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction(); //Start transaction!
            try {
                $arr_fee_amount = $request->has('fee_amount') ? Input::get('fee_amount') : [];
                // insert into fee table
                $fee->class_id = Input::get('class_id');
                $fee->session_id = Input::get('session_id');
                $fee->fee_type_id = Input::get('fee_type_id');
                $fee->fee_circular_id = Input::get('fee_circular_id');
                $fee->apply_to = Input::get('apply_to');
                $fee->fine_amount = Input::get('fine_amount');
                $fee->fine_type = Input::get('fine_type');
                $fee->student_type_id = Input::get('student_type_id');
                $fee->total_fee = array_sum($arr_fee_amount);
                $fee->save();

                // insert into fee detail table
                $arr_sub_circular = Input::get('fee_sub_circular') ? Input::get('fee_sub_circular') : [];
                $arr_start_month = $request->has('start_month') ? Input::get('start_month') : [];
                $arr_end_month = $request->has('end_month') ? Input::get('end_month') : [];
                $arr_fine_date = $request->has('fine_date') ? Input::get('fine_date') : [];
                $arr_grace_period = $request->has('grace_period') ? Input::get('grace_period') : [];
                $arr_fee_detail_id = $request->has('fee_detail_id') ? Input::get('fee_detail_id') : [];

                $new_added = false;
                if (!empty($arr_sub_circular)) {
                    foreach ($arr_sub_circular as $key => $sub_circular) {
                        $fee_detail_id = $arr_fee_detail_id[$key];

//                        if ($request->has('circular_edit') && Input::get('circular_edit') == 'not_allowed') { // not_allowed
                        if ($request->has('circular_edit') && ((!empty($fee_detail_id) && Input::get('circular_edit') == 'allowed') ||  (Input::get('circular_edit') == 'not_allowed'))) { // not_allowed
                            $fee_detail = FeeDetail::find($fee_detail_id); // If edit case, then do only partial edit
                        } else {
                            $fee_detail = new FeeDetail;
                            $new_added = true;
                        }

                        $fee_detail->fee_sub_circular = $sub_circular;

                        if ($request->has('circular_edit') && Input::get('circular_edit') == 'allowed'){ // not_allowed
                            if (isset($arr_start_month[$key])) {
                                $fee_detail->start_month = $arr_start_month[$key];
                            }
                            if (isset($arr_end_month[$key])) {
                                $fee_detail->end_month = $arr_end_month[$key];
                            }
                            if (isset($arr_fee_amount[$key])) {
                                $fee_detail->fee_amount = $arr_fee_amount[$key];
                            }
                        }

                        if (isset($arr_grace_period[$key])) {
                            $fee_detail->grace_period = $arr_grace_period[$key];
                        }
                        if (isset($arr_fine_date[$key]) && !empty($arr_fine_date[$key])) {
                            $fee_detail->fine_date = get_formatted_date($arr_fine_date[$key], 'database');
                        }
                        $save_fee_detail[] = $fee_detail;
                    }
                }
                /*
                 *  delete perious record and then insert new record in fee_details while editing fee master
                 */

                if (!empty($decrypted_fee_id) && ($request->has('circular_edit') && Input::get('circular_edit') == 'not_allowed') && $new_added) { // allowed
                    $fee_detail = FeeDetail::where('fee_id', $decrypted_fee_id);
                    $fee_detail->delete();
                }
                $fee->feeDetail()->saveMany($save_fee_detail);
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getTraceAsString();
                p($error_message);
                return redirect()->back()->withInput()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin/fee-parameter')->withSuccess($success_msg);
    }

    /*
     * Delete data  from fee and  its details table
     */

    public function destroy(Request $request)
    {
        $fee_id = Input::get('fee_id');
        $fee = Fee::find($fee_id);
        if ($fee) {
            DB::beginTransaction(); //Start transaction!
            try {
                $fee->delete();
                $return_arr = array(
                    'status' => 'success',
                    'message' => 'Fee deleted successfully!'
                );
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                $return_arr = array(
                    'status' => 'used',
                    'message' => trans('language.delete_message')
                );
            }
            DB::commit();
        } else {
            $return_arr = array(
                'status' => 'error',
                'message' => 'Fee not found!'
            );
        }
        return response()->json($return_arr);
    }

    /*
     * List data in datatables
     */

    public function anyData()
    {
        $fee = [];
        $arr_fee = $this->getFeeData();
        foreach ($arr_fee as $key1 => $fee_data) {
            $fee[$key1] = (object)$fee_data;
        }

        return Datatables::of($fee)
            ->addColumn('action', function ($fee) {
                $delete = '';
                if ($fee->is_fixed != 1) {
                    $delete = '<button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $fee->fee_id . '"><i class="fa fa-trash"></i></button>';
                }
                $encrypted_fee_id = get_encrypted_value($fee->fee_id, true);
                return '<a title="Edit" id="deletebtn1" href="fee-parameter/add/' . $encrypted_fee_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                    . $delete;
            })->make(true);
    }

    /*
     * Get  data from fee and all its relation table's
     */

    public function getFeeData($fee_id = null)
    {
        $fee = [];
        $fee_array = [];

        $arr_fee = Fee::where(function ($query) use ($fee_id) {
            if (!empty($fee_id)) {
                $query->where('fee_id', $fee_id);
            }
        })
            ->with('getFeeType')->with('getFeeCircular')
            ->with('getFeeClass')->with('getFeeSession')->get();
        if (!empty($arr_fee)) {
            $arr_fee_circular = get_fee_circular_custom();
            $arr_fee_apply_to = \Config::get('custom.fee_apply_to');
            foreach ($arr_fee as $key1 => $relation_data) {
                $fee = array(
                    'fee_id' => $relation_data['fee_id'],
                    'is_fixed' => $relation_data['is_fixed'],
                    'class_id' => $relation_data['class_id'],
                    'session_id' => $relation_data['session_id'],
                    'fee_type_id' => $relation_data['fee_type_id'],
                    'fee_circular_id' => $relation_data['fee_circular_id'],
                    'apply_to' => $relation_data['apply_to'],
                    'fine_type' => $relation_data['fine_type'],
                    'student_type_id' => $relation_data['student_type_id'],
                    'fine_amount' => $relation_data['fine_amount'],
                    'apply_to_student' => $arr_fee_apply_to[$relation_data['apply_to']],
                    'class_name' => $relation_data['getFeeClass']['class_name'],
                    'session_year' => $relation_data['getFeeSession']['session_year'],
                    'fee_circular' => $arr_fee_circular[$relation_data['getFeeCircular']['fee_circular']],
                    'fee_type' => $relation_data['getFeeType']['fee_type'],
                );
                $fee_array[] = $fee;
            }
        }
        return $fee_array;
    }

    /*
     * Get Fee sub circular installments according to fee circular
     */

    function getFeeSubCircular(Request $request)
    {
        $fee_circular_id = Input::get('fee_circular_id');
        $fee_id = Input::get('fee_id');
        $installments_list = [];
        $message = 'Result not found!';
        $status = 'failed';
        $fee_detail_id_exist = 'allowed';
        if (!empty($fee_circular_id)) {
            $fee_sub_circular['arr_fee_detail'] = [];
            $arr_fee_circular = get_fee_circular_custom();
            $fee_circular = FeeCircular::where('fee_circular_id', $fee_circular_id)->pluck('fee_circular')->first();
            $fee_circular = isset($arr_fee_circular[$fee_circular]) ? $fee_circular : '';
            if (!empty($fee_circular)) {
                $arr_fee_sub_circular = \Config::get('custom.fee_sub_circular');
                $fee_sub_circular['arr_sub_circular'] = isset($arr_fee_sub_circular[$fee_circular]) ? $arr_fee_sub_circular[$fee_circular] : [];
                /*
                 * reordered months according to session months if fee circular is monthly
                 */
                if ($fee_circular == 'monthly' || $fee_circular == 'installment') {
                    $arr_session_months = get_current_session_months();
                    if (!empty($arr_session_months)) {
                        foreach ($arr_session_months as $key => $session_months) {
                            $reordered_months[lcfirst($session_months)] = $fee_sub_circular['arr_sub_circular'][lcfirst($session_months)];
                        }
                    }
                    $fee_sub_circular['arr_sub_circular'] = $reordered_months;
                }
                $fee_sub_circular['fee_circular'] = $fee_circular;
                $custom_months = \config::get('custom.month');
                $fee_sub_circular['custom_months'] = array_flip($custom_months);
            }
            if (isset($request->required_drop_down) && $request->required_drop_down == true) {
                if (!empty($fee_sub_circular['arr_sub_circular'])) {
                    return array("status" => "success", "data" => add_blank_option($fee_sub_circular['arr_sub_circular'], '-- Select sub fee circular --'));
                } else {
                    return array("status" => "success", "data" => add_blank_option([], '-- Select sub fee circular --'));
                }
            }
            if (!empty($fee_sub_circular['arr_sub_circular'])) {
                $arr_months = get_current_session_months();
                $fee_sub_circular['arr_months'] = add_blank_option($arr_months, '--Select month--');
                if (!empty($fee_id)) {
                    $fee_sub_circular['arr_fee_detail'] = $this->getFeeDetail($fee_id);
                }
                if (!empty($fee_sub_circular['arr_fee_detail'])) {
                    $fee_receipt_data = StudentFeeReceiptDetail::where('fee_id', $fee_id)->first();
                    if (!empty($fee_receipt_data->fee_detail_id)) {
                        // exist child data of detail id, so we can't change its circular
                        $fee_detail_id_exist = 'not_allowed';
                    }
                }
                $fee_sub_circular['circular_edit_readonly'] = ($fee_detail_id_exist == "not_allowed") ? "readonly" : ""; // allowed =  FULL Edit or  not_allowed = PARTIAL Edit
                $installments_list = View::make('backend.fee-parameter.fee-sub-circular')->with($fee_sub_circular)->render();
                $message = 'Got result successfully';
                $status = 'success';
            }
        }
        return response()->json($result = array('status' => $status, 'message' => $message, 'data' => $installments_list, 'circular_edit' => $fee_detail_id_exist));
    }

    /*
     * Get fee details according to fee id
     */

    public function getFeeDetail($fee_id = null)
    {
        $fee_detail_array = [];

        $arr_fee_detail = Fee::where(function ($query) use ($fee_id) {
            if (!empty($fee_id)) {
                $query->where('fee_id', $fee_id);
            }
        })->with('feeDetail')->first();
        if (!empty($arr_fee_detail['feeDetail'])) {
            foreach ($arr_fee_detail['feeDetail'] as $key => $fee_detail) {
                $detail_array = array(
                    'fee_detail_id' => $fee_detail['fee_detail_id'],
                    'fee_id' => $fee_detail['fee_id'],
                    'fee_amount' => $fee_detail['fee_amount'],
                    'fee_sub_circular' => $fee_detail['fee_sub_circular'],
                    'start_month' => $fee_detail['start_month'],
                    'end_month' => $fee_detail['end_month'],
                    'fine_date' => get_formatted_date($fee_detail['fine_date'], 'display'),
                    'grace_period' => $fee_detail['grace_period'],
                );
                $fee_detail_array[$key] = $detail_array;
            }
        }
        return $fee_detail_array;
    }

}
    