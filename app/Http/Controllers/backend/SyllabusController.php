<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Syllabus;
    use App\Model\backend\CreateExam;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class SyllabusController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index()
        {
            $data = array(
                'redirect_url' => url('admin/syllabus/add'),
            );
            return view('backend.syllabus.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {
            $syllabus    = [];
            $syllabus_id = null;
            $class_id    = null;
            $session_id  = null;
            $section_id  = null;
            if (!empty($id))
            {
                $decrypted_syllabus_id = get_decrypted_value($id, true);
                $syllabus              = $this->getSyllabusData($decrypted_syllabus_id);
                $syllabus              = isset($syllabus[0]) ? $syllabus[0] : [];
                $class_id              = $syllabus['class_id'];
                $session_id            = $syllabus['session_id'];
                $section_id            = $syllabus['section_id'];
                if (!$syllabus)
                {
                    return redirect('admin/syllabus')->withError('Create Syllabus not found!');
                }
                $encrypted_syllabus_id = get_encrypted_value($syllabus['syllabus_id'], true);
                $save_url              = url('admin/syllabus/save/' . $encrypted_syllabus_id);
                $submit_button         = 'Update';
                $syllabus_id           = $decrypted_syllabus_id;
            }
            else
            {
                $save_url      = url('admin/syllabus/save');
                $submit_button = 'Save';
            }
            $class_data                    = $this->getClassExam($session_id,$class_id, $section_id);
            $syllabus['arr_class_subject'] = $class_data['subject'];
            $syllabus['arr_exam']          = $class_data['exam'];
            $syllabus['arr_syllabus_type'] = \Config::get('custom.syllabus_type');
            $arr_months                    = get_current_session_months();
            $syllabus['arr_months']        = add_blank_option($arr_months, '--Select month--');
            $syllabus['arr_session']       = add_blank_option(get_session(), '-- Academic year --');
            $arr_class                     = get_all_classes();
            $syllabus['arr_class']         = add_blank_option($arr_class, '--Select class --');
            $syllabus['arr_section']       = add_blank_option(get_class_section($class_id, $session_id), '--Select Section --');
            $data                          = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'syllabus'      => $syllabus,
                'redirect_url'  => url('admin/syllabus/'),
            );
            return view('backend.syllabus.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_syllabus_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $syllabus = Syllabus::find($decrypted_syllabus_id);

                if (!$syllabus)
                {
                    return redirect('/admin/syllabus/')->withError('Syllabus not found!');
                }
                $success_msg = 'Syllabus updated successfully!';
            }
            else
            {
                $syllabus    = New Syllabus;
                $success_msg = 'Syllabus saved successfully!';
            }

            $class_id         = Input::get('class_id');
            $section_id       = Input::get('section_id');
            $exam_id          = Input::get('exam_id');
            $month            = Input::get('month');
            $subject_id       = Input::get('subject_id');
            $session_id       = Input::get('session_id');
            $arr_input_fields = [
                'class_id'         => 'required',
                'section_id'       => 'required',
                'session_id'       => 'required',
                'subject_id'       => 'required',
                'syllabus_content' => 'required',
            ];
            if (!empty(Input::get('exam_id')) && Input::get('syllabus_type') == 1)
            {
                $arr_input_fields['syllabus_type'] = 'required|unique:syllabi,syllabus_type,' . $decrypted_syllabus_id . ',syllabus_id,class_id,' . $class_id . ',session_id,' . $session_id . ',subject_id,' . $subject_id . ',exam_id,' . $exam_id.',deleted_at,NULL';
            }
            else if (!empty(Input::get('month')) && Input::get('syllabus_type') == 2)
            {
                $arr_input_fields['syllabus_type'] = 'required|unique:syllabi,syllabus_type,' . $decrypted_syllabus_id . ',syllabus_id,class_id,' . $class_id . ',session_id,' . $session_id . ',subject_id,' . $subject_id . ',month,' . $month.',deleted_at,NULL';
            }
            else
            {
                $arr_input_fields['syllabus_type'] = 'required|unique:syllabi,syllabus_type,' . $decrypted_syllabus_id . ',syllabus_id,class_id,' . $class_id . ',session_id,' . $session_id . ',subject_id,' . $subject_id.',deleted_at,NULL';
            }
            $validatior = Validator::make($request->all(), $arr_input_fields);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $syllabus->exam_id = null;
                    $syllabus->month   = null;
                    if (!empty(Input::get('exam_id')) && Input::get('syllabus_type') == 1)
                    {
                        $syllabus->exam_id = $exam_id;
                    }
                    else if (!empty(Input::get('month')) && Input::get('syllabus_type') == 2)
                    {
                        $syllabus->month = Input::get('month');
                    }

                    $syllabus->class_id         = $class_id;
                    $syllabus->section_id       = $section_id;
                    $syllabus->subject_id       = $subject_id;
                    $syllabus->session_id       = $session_id;
                    $syllabus->syllabus_type    = Input::get('syllabus_type');
                    $syllabus->syllabus_content = Input::get('syllabus_content');
                    $syllabus->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }
                DB::commit();
            }
            return redirect('admin/syllabus')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $syllabus_id = Input::get('syllabus_id');
            $syllabus    = Syllabus::find($syllabus_id);
            if ($syllabus)
            {

                DB::beginTransaction(); //Start transaction!
                try
                {
                    $syllabus->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Syllabus deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Syllabus not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $syllabus     = [];
            $syllabus_id  = null;
            $offset       = Input::get('start');
            $limit        = Input::get('length');
            $arr_syllabus = $this->getSyllabusData($syllabus_id, $offset, $limit);
            foreach ($arr_syllabus as $key => $syllabus_data)
            {
                $arr_modified_data = [];
                $string            = $syllabus_data['syllabus_content'];
                if (strlen($string) > 400)
                {
                    $syllabus_content = substr($string, 0, 100) . '...';
                }
                else
                {
                    $syllabus_content = $string;
                }
                $syllabus_content  = wordwrap($syllabus_content);
                $arr_modified_data = array(
                    'syllabus_id'        => $syllabus_data['syllabus_id'],
                    'syllabus_type'      => $syllabus_data['syllabus_type'],
                    'class_name'         => $syllabus_data['class_name'],
                    'subject_name'       => $syllabus_data['subject_name'],
                    'syllabus_type_name' => $syllabus_data['syllabus_type_name'],
                    'syllabus_content'   => "", //$syllabus_content
                );
                if ($arr_modified_data['syllabus_type'] == 1)
                {
                    $arr_modified_data['name'] = $syllabus_data['exam_name'];
                }
                else
                {
                    $arr_modified_data['name'] = $syllabus_data['month_name'];
                }
                $syllabus[$key] = (object) $arr_modified_data;
            }
            return Datatables::of($syllabus)
                    ->addColumn('action', function ($syllabus)
                    {
                        $encrypted_syllabus_id = get_encrypted_value($syllabus->syllabus_id, true);

                        return '<a title="Edit" id="deletebtn1" href="syllabus/add/' . $encrypted_syllabus_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $syllabus->syllabus_id . '"><i class="fa fa-trash"></i></button>';
                    })
                    ->rawColumns(['syllabus_content', 'action'])->make(true);
        }

        public function getSyllabusData($syllabus_id = null, $offset = null, $limit = null)
        {
            $syllabus_return = [];
            $session_id      = null;
            $session         = get_current_session();
            if (!empty($session))
            {
                $session_id = $session['session_id'];
            }
            $arr_syllabus_data = Syllabus::where('session_id', $session_id)->where(function($query) use ($syllabus_id)
                {
                    if (!empty($syllabus_id))
                    {
                        $query->where('syllabus_id', $syllabus_id);
                    }
                })->with(['syllabusClass' => function($q)
                    {
                        $q->addSelect(['class_id', 'class_name']);
                        $q->where('class_status', 1);
                    }])
                ->with(['syllabusSubject' => function($q)
                    {
                        $q->addSelect(['subject_id', 'subject_name']);
                        $q->where('subject_status', 1);
                    }])
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
                ->get();

            if (!empty($arr_syllabus_data))
            {
                $arr_month     = \Config::get('custom.month');
                $syllabus_type = \Config::get('custom.syllabus_type');
                $syllabus      = [];
                foreach ($arr_syllabus_data as $key => $syllabus_data)
                {
                    $syllabus               = array(
                        'syllabus_id'        => $syllabus_data['syllabus_id'],
                        'session_id'         => $syllabus_data['session_id'],
                        'class_id'           => $syllabus_data['class_id'],
                        'section_id'         => $syllabus_data['section_id'],
                        'class_name'         => $syllabus_data['syllabusClass']['class_name'],
                        'exam_id'            => $syllabus_data['exam_id'],
                        'subject_id'         => $syllabus_data['subject_id'],
                        'subject_name'       => $syllabus_data['syllabusSubject']['subject_name'],
                        'month'              => $syllabus_data['month'],
                        'syllabus_type'      => $syllabus_data['syllabus_type'],
                        'syllabus_type_name' => $syllabus_type[$syllabus_data['syllabus_type']],
                        'syllabus_content'   => $syllabus_data['syllabus_content'],
                    );
                    $syllabus['month_name'] = '';
                    $syllabus['exam_name']  = '';
                    if (!empty($syllabus['month']))
                    {
                        $syllabus['month_name'] = $arr_month[$syllabus_data['month']];
                    }
                    if (!empty($syllabus['exam_id']))
                    {
                        $exam                  = CreateExam::where('exam_id', $syllabus['exam_id'])->first();
                        $syllabus['exam_name'] = $exam['exam_name'];
                    }
                    $syllabus_return[] = $syllabus;
                }
            }
            return $syllabus_return;
        }

        function getClassExam($session_id, $class_id, $section_id)
        {
            $syllabus['subject'] = [];
            $syllabus['exam']    = [];
            $subject             = [];
            $status              = 'failed';
            if (!empty($class_id))
            {
                $exam_id             = null;
                //get class section's subject
                $syllabus['subject'] = get_class_section_subject($session_id,$class_id,$section_id);
                $syllabus['exam']    = get_all_exam($exam_id, $class_id, $session_id, $section_id);
                $status              = 'success';
            }
            $request = request();
            if ($request->ajax())
            {
                return response()->json(array('status' => $status, 'data' => $syllabus));
            }
            return $syllabus;
        }

    }
    