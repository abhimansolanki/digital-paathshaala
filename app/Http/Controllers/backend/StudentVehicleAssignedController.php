<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\StudentVehicleAssigned;
    use App\Model\backend\StudentVehicleAssignedDetail;
    use App\Model\backend\StopPoint;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\View as View;

    class StudentVehicleAssignedController extends Controller
    {

        public function __construct()
        {
	$this->Route = new RouteController;
        }

        public function index()
        {
	$data = array(
	    'redirect_url' => url('admin/student-vehicle-assigned/add'),
	);
	return view('backend.student-vehicle-assigned.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {

	$vehicle_assigned = [];
	$arr_student_id = [];
	$arr_employee_id = [];
	$vehicle_id = null;
	$stop_point_id = null;
	$student_checked = FALSE;
	$employee_checked = FALSE;
	$decrypted_vehicle_assigned_id = get_decrypted_value($id, true);
	if (!empty($id))
	{
	    $vehicle_assigned = $this->getStudentVehicleAssignedData($decrypted_vehicle_assigned_id);
	    $vehicle_assigned = isset($vehicle_assigned[0]) ? $vehicle_assigned[0] : [];
//p($vehicle_assigned);
	    if (!$vehicle_assigned)
	    {
	        return redirect('admin/student-vehicle-assigned')->withError('Assigned Vehicle not found!');
	    }
	    $encrypted_student_vehicle_assigned_id = get_encrypted_value($vehicle_assigned['student_vehicle_assigned_id'], true);
	    $save_url = url('admin/student-vehicle-assigned/save/' . $encrypted_student_vehicle_assigned_id);
	    $submit_button = 'Update';
	    $vehicle_id = $vehicle_assigned['vehicle_id'];
	    $stop_point_id = $vehicle_assigned['stop_point_id'];
	} else
	{
	    $save_url = url('admin/student-vehicle-assigned/save');
	    $submit_button = 'Save';
	}

	$vehicle_assigned['employee_checked'] = $employee_checked;

	// get all classes 
	$arr_class = get_all_classes();
	$vehicle_assigned['arr_class'] = add_blank_option($arr_class, '--Select class --');

	// get all stop points 
	$arr_stop_point = $this->Route->getRouteStopPointData($stop_point_id);
	$vehicle_assigned['arr_route_stop'] = add_blank_option($arr_stop_point, '-- Select stop point --');

	// get all vehicle list
	$all_vehicle = get_vehicle_list($vehicle_id);
	$vehicle_assigned['arr_vehicle'] = add_blank_option($all_vehicle, '-- Select vehicle --');
	$arr_session = get_session();
	$vehicle_assigned['arr_session'] = add_blank_option($arr_session, '-- Academic year --');
	$current_db = db_current_session();
	$vehicle_assigned['start_date'] = get_formatted_date($current_db['start_date'], 'display');
	$vehicle_assigned['end_date'] = get_formatted_date($current_db['end_date'], 'display');
	$data = array(
	    'save_url' => $save_url,
	    'submit_button' => $submit_button,
	    'vehicle_assigned' => $vehicle_assigned,
	    'redirect_url' => url('admin/student-vehicle-assigned'),
	);

	return view('backend.student-vehicle-assigned.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
	//p($request->input());
	$decrypted_student_vehicle_assigned_id = get_decrypted_value($id, true);
	if (!empty($id))
	{
	    $student_vehicle_assigned = StudentVehicleAssigned::find($decrypted_student_vehicle_assigned_id);

	    if (!$student_vehicle_assigned)
	    {
	        return redirect('/admin/student-vehicle-assigned/')->withError('Vehicle Assigned not found!');
	    }
	    $success_msg = 'Vehicle Assigned updated successfully!';
	} else
	{
	    $student_vehicle_assigned = New StudentVehicleAssigned;
	    $success_msg = 'Vehicle Assigned saved successfully!';
	}

	$session_id = Input::get('session_id');
	$vehicle_id = Input::get('vehicle_id');

	$validatior = Validator::make($request->all(), [
		'stop_point_id' => 'required|unique:student_vehicle_assigned,stop_point_id,' . $decrypted_student_vehicle_assigned_id . ',student_vehicle_assigned_id,vehicle_id,' . $vehicle_id . ',session_id,' . $session_id,
		'vehicle_id' => 'required',
		'pickup_time' => 'required',
		'return_time' => 'required',
		'session_id' => 'required',
	]);

	if ($validatior->fails())
	{
	    return redirect()->back()->withInput()->withErrors($validatior);
	} else
	{
	    DB::beginTransaction(); //Start transaction!

	    try
	    {
	        $student_assigned_detail = [];
	        $employee_assigned_detail = [];
	        $student_vehicle_assigned->stop_point_id = Input::get('stop_point_id');
	        $student_vehicle_assigned->vehicle_id = Input::get('vehicle_id');
	        $student_vehicle_assigned->session_id = $session_id;
	        $student_vehicle_assigned->pickup_time = Input::get('pickup_time');
	        $student_vehicle_assigned->return_time = Input::get('return_time');
	        $student_vehicle_assigned->save();

	        $arr_student = !empty(Input::get('student_id')) ? array_unique(Input::get('student_id')) : [];
	        $student_assigned_detail_id = Input::get('student_assigned_detail_id');
	        if (!empty($arr_student))
	        {
		foreach ($arr_student as $key => $student_id)
		{
		    $assigned_detail = isset($student_assigned_detail_id[$student_id]) ? StudentVehicleAssignedDetail::find($student_assigned_detail_id[$student_id]) : new StudentVehicleAssignedDetail;
		    $arr_joined_date = Input::get('joined_date');
		    $assigned_detail->student_id = $student_id;
		    $assigned_detail->join_date = get_formatted_date($arr_joined_date[$student_id], 'database');
		    $assigned_detail->assigned_to = 1;
		    $assigned_detail->assigned_status = 1;
		    $student_assigned_detail[] = $assigned_detail;
		}
	        }

//                    $arr_employee = Input::get('employee_id');
//                    if (!empty($arr_employee))
//                    {
//                        foreach ($arr_employee as $key => $employee_id)
//                        {
//                            $assigned_detail_id = Input::get('employee_assigned_detail_id_' . $employee_id);
//                            if ($request->has('employee_assigned_detail_id_' . $employee_id) && !empty($assigned_detail_id))
//                            {
//                                $assigned_detail = StudentVehicleAssignedDetail::Find($assigned_detail_id);
//                            }
//                            else
//                            {
//                                $assigned_detail = new StudentVehicleAssignedDetail;
//                            }
//                            $assigned_detail->employee_id = $employee_id;
//                            $assigned_detail->join_date   = get_formatted_date(Input::get('join_date'), 'database');
//                            $assigned_detail->assigned_to = 2;
//                            $employee_assigned_detail []  = $assigned_detail;
//                        }
//                    }
//                    $save_assigned_detail = array_merge($student_assigned_detail, $employee_assigned_detail);
//                    p($save_assigned_detail);
	        if (!empty($student_assigned_detail))
	        {
		$student_vehicle_assigned->vehicleAssignedStudentDetail()->saveMany($student_assigned_detail);
	        }
	    } catch (\Exception $e)
	    {
	        //failed logic here
	        DB::rollback();
	        $error_message = $e->getMessage();
	        p($error_message);
	        return redirect()->back()->withInput()->withErrors($error_message);
	    }

	    DB::commit();
	}
	return redirect('admin/student-vehicle-assigned')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
	$student_vehicle_assigned_id = Input::get('student_vehicle_assigned_id');
	$vehicle_assigned = StudentVehicleAssigned::find($student_vehicle_assigned_id);
	if ($vehicle_assigned)
	{
	    DB::beginTransaction(); //Start transaction!
	    try
	    {
	        $vehicle_assigned->delete();
	        $return_arr = array(
		'status' => 'success',
		'message' => 'Assigned Vehicle deleted successfully!'
	        );
	    } catch (\Exception $e)
	    {
	        //failed logic here
	        DB::rollback();
	        $error_message = $e->getMessage();
	        $return_arr = array(
		'status' => 'used',
		'message' => trans('language.delete_message')
	        );
	    }
	    DB::commit();
	} else
	{
	    $return_arr = array(
	        'status' => 'error',
	        'message' => 'Assigned Vehicle not found!'
	    );
	}
	return response()->json($return_arr);
        }

        public function anyData()
        {
	$vehicle_assigned = [];
	$limit = Input::get('length');
	$offset = Input::get('start');

	$vehicle_assigned = $this->getStudentVehicleAssignedData($vehicle_assigned_id = null, $offset, $limit, 'object');
//	foreach ($arr_vehicle_assigned as $key => $vehicle_assigned_data)
//	{
//	    $vehicle_assigned[$key] = (object) $vehicle_assigned_data;
//	}
	return Datatables::of($vehicle_assigned)
		    ->addColumn('action', function ($vehicle_assigned)
		    {
		        $encrypted_vehicle_assigned_id = get_encrypted_value($vehicle_assigned->student_vehicle_assigned_id, true);
		        return '<a title="Edit" id="deletebtn1" href="student-vehicle-assigned/add/' . $encrypted_vehicle_assigned_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
			    . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $vehicle_assigned->student_vehicle_assigned_id . '"><i class="fa fa-trash"></i></button>';
		    })->make(true);
        }

        public function getStudentVehicleAssignedData($vehicle_assigned_id = null, $offset = null, $limit = null, $return_type = 'array')
        {
	$vehicle_assigned_return = [];
	$session = get_current_session();
	$session_id = null;
	if (!empty($session))
	{
	    $session_id = $session['session_id'];
	}
	$arr_vehicle_assigned = StudentVehicleAssigned::
	        where(function($query) use ($vehicle_assigned_id)
	        {
		if (!empty($vehicle_assigned_id))
		{
		    $query->where('student_vehicle_assigned_id', $vehicle_assigned_id);
		}
	        })->where(function($query) use ($session)
	        {
		if (!empty($session['session_id']))
		{
		    $query->where('session_id', $session['session_id']);
		}
	        })
	        ->with(['vehicleAssignedStudentDetail' => function($q) use ($vehicle_assigned_id)
		{
		    $q->select(['student_vehicle_assigned_detail_id', 'join_date', 'assigned_to',
		        'assigned_id', 'student_id', 'employee_id', 'assigned_status', 'stop_date'
		    ]);
		    if (!empty($vehicle_assigned_id))
		    {
		        $q->where('assigned_status', 1);
		    }
		}
	        ])
	        ->with(['vehicleAssignedStopPoint' => function($q)
		{
		    $q->select(['stop_point_id', 'stop_point', 'route_id', 'stop_fair']);
		}
	        ])
	        ->with(['vehicleAssignedVehicle' => function($q)
		{
		    $q->select(['vehicle_id', 'vehicle_number', 'total_seats', 'strength']);
		}
	        ])
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
	        ->get();
//p($arr_vehicle_assigned);
	foreach ($arr_vehicle_assigned as $key => $assigned)
	{
	    $vehicle_assigned = array(
	        'student_vehicle_assigned_id' => $assigned['student_vehicle_assigned_id'],
	        'pickup_time' => $assigned['pickup_time'],
	        'return_time' => $assigned['return_time'],
	        'session_id' => $assigned['session_id'],
	        'vehicle_id' => $assigned['vehicle_id'],
	        'stop_point_id' => $assigned['stop_point_id'],
	        'stop_point' => $assigned['vehicleAssignedStopPoint']['stop_point'],
	        'stop_fair' => $assigned['vehicleAssignedStopPoint']['stop_fair'],
	        'vehicle_number' => $assigned['vehicleAssignedVehicle']['vehicle_number'],
	    );
	    foreach ($assigned['vehicleAssignedStudentDetail'] as $key => $assigned_detail)
	    {
	        $assigned_detail_id = $assigned_detail['student_vehicle_assigned_detail_id'];
	        $vehicle_assigned['assigned_detail'][$assigned_detail_id] = array(
		'assigned_detail_id' => $assigned_detail_id,
		'join_date' => get_formatted_date($assigned_detail['join_date'], 'display'),
		'stop_date' => !empty($assigned_detail['stop_date']) ? get_formatted_date($assigned_detail['stop_date'], 'display') : '',
		'assigned_to' => $assigned_detail['assigned_to'],
		'type' => $assigned_detail['assigned_to'] == 1 ? 'Student' : 'Staff',
		'status' => $assigned_detail['assigned_status'] == 1 ? 'Assigned' : 'Unassigned',
		'student_id' => $assigned_detail['student_id'],
		'employee_id' => $assigned_detail['employee_id'],
		'student_name' => $assigned_detail['vehicleAssignedStudent']['first_name'],
		'enrollment_number' => $assigned_detail['vehicleAssignedStudent']['enrollment_number'],
	        );
	    }
	    if ($return_type == 'object')
	    {
	        $vehicle_assigned_return[] = (object) $vehicle_assigned;
	    } else
	    {
	        $vehicle_assigned_return[] = $vehicle_assigned;
	    }
	}
	return $vehicle_assigned_return;
        }

        function getStudent(Request $request)
        {
	$class_id = Input::get('class_id');
	$arr_student_id = Input::get('student_id');
	$student_list = '';
	$arr_student_data['arr_student_list'] = [];
	$message = 'Students not found!';
	$status = 'failed';
	if (!empty($class_id))
	{
	    $session = get_current_session();
	    $query = DB::table('students as s')
		->join('classes as c', 'c.class_id', '=', 's.current_class_id')
		->leftJoin('sections as sec', 'sec.section_id', '=', 's.current_section_id')
		->where('s.current_session_id', $session['session_id'])
		->select('s.enrollment_number', 's.student_id', 's.first_name', 's.last_name', 's.middle_name', 'c.class_name', 'sec.section_name')
		->where(function($query) use ($class_id, $arr_student_id)
	    {
	        $query->where('s.current_class_id', $class_id);
	        $query->whereNotIn('s.student_id', function($q)
	        {
		$q->select('svad.student_id')->from('student_vehicle_assigned_details as svad')->where('assigned_status', 1);
	        });

	        if (!empty($arr_student_id))
	        {
		$query->whereNotIn('s.student_id', $arr_student_id);
	        }
	    });

	    $arr_student = $query->get();
	    foreach ($arr_student as $student)
	    {
	        $student = (array) $student;
	        $class = $student['class_name'];
	        if (!empty($student['section_name']))
	        {
		$class = $student['class_name'] . '(' . $student['section_name'] . ')';
	        }
	        $arr_student_data['arr_student_list'][] = array(
		'student_id' => $student['student_id'],
		'enrollment_number' => $student['enrollment_number'],
		'student_name' => $student['first_name'] . ' ' . $student['middle_name'] . ' ' . $student['last_name'],
		'class' => $class
	        );
	    }
//	    p($arr_student_data);
	    $message = 'Got result successfully';
	    $status = 'success';
	    $student_list = View::make('backend.student-vehicle-assigned.student')->with($arr_student_data)->render();
	}
	return response()->json($result = array('status' => $status, 'message' => $message, 'data' => $student_list));
        }

        function getStopFair()
        {
	$stop_point_id = Input::get('stop_point_id');
	$stop_fair = null;
	$message = 'Stop Fair not found!';
	$status = 'failed';
	if (!empty($stop_point_id))
	{
	    $arr_stop_fair = StopPoint::where('stop_point_id', $stop_point_id)->pluck('stop_fair');
	    $stop_fair = $arr_stop_fair[0];
	    $message = 'Got result successfully';
	    $status = 'success';
	}
	return response()->json($result = array('status' => $status, 'message' => $message, 'data' => $stop_fair));
        }

        function getEmployee()
        {

	$arr_employee_id = (array) json_decode(Input::get('arr_employee_id'));
	$message = 'Employee not found!';
	$status = 'failed';
	$employee_list = '';

	$arr_employee_data['vehicle_assigned_employee_id'] = $arr_employee_id;
	if (true)
	{
	    $query = DB::table('employees as e')
		->select('e.employee_id', 'e.employee_code', 'e.full_name')
		->whereNotIn('e.employee_id', function($q)
	    {
	        $q->select('svad.employee_id')->from('student_vehicle_assigned_details as svad')->where('status', 1);
	    });

	    $arr_student = $query->get();
	    $arr_employee = get_employee($arr_employee_id);
	    foreach ($arr_employee as $employee)
	    {
	        $arr_employee_data['arr_employee_list'][] = array(
		'employee_id' => $employee['employee_id'],
		'employee_code' => $employee['employee_code'],
		'full_name' => $employee['full_name'],
	        );
	    }
	    $message = 'Got result successfully';
	    $status = 'success';
	    $employee_list = View::make('backend.student-vehicle-assigned.employee')->with($arr_employee_data)->render();
	}
	return response()->json($result = array('status' => $status, 'message' => $message, 'data' => $employee_list));
        }

        public function vehicleUnassigned(Request $request)
        {
	$assigned_detail_id = Input::get('assigned_detail_id');
	$stop_date = Input::get('stop_date');
//	p($request->input());
	$assigned_detail = StudentVehicleAssignedDetail::find($assigned_detail_id);
	if ($assigned_detail)
	{
	    $assigned_detail->assigned_status = 0;
	    $assigned_detail->stop_date = get_formatted_date($stop_date, 'database');
	    $assigned_detail->save();
	    $return_arr = array(
	        'status' => 'success',
	        'message' => 'Vehicle Un-assigned successfully!',
	    );
	} else
	{
	    $return_arr = array(
	        'status' => 'error',
	        'message' => 'Vehicle assigned not found!'
	    );
	}
	return response()->json($return_arr);
        }

//         query to get report of student bu route and class wise
//        SELECT
//  s.current_class_id, sp.route_id,COUNT(svad.student_id)
//FROM
//  student_vehicle_assigned AS svs
//  INNER JOIN student_vehicle_assigned_details AS svad ON svs.student_vehicle_assigned_id = svad.assigned_id
//  INNER JOIN students AS s ON s.student_id = svad.student_id
//  INNER JOIN stop_points AS sp ON sp.stop_point_id = svs.stop_point_id
//GROUP BY
// sp.route_id, s.current_class_id;


        public function checkVehicleJoinDate(Request $request)
        {
	$return = true;
	$arr_joined_date = Input::get('joined_date');
	$arr_student_id = Input::get('student_id');
	$arr_unassigned_date = Input::get('unassigned_date');
	$arr_student_assigned_detail_id = Input::get('student_assigned_detail_id');
	if(!empty($arr_student_assigned_detail_id))
	{
	foreach ($arr_student_id as $student_id)
	{
	    $join_date = isset($arr_joined_date[$student_id]) ? get_formatted_date($arr_joined_date[$student_id], 'databse') : null;
	    if (!empty($join_date) && !empty($student_id))
	    {
	        $date_exist = DB::table('student_vehicle_assigned_details')
		    ->where(function($q) use ($join_date)
		    {
		        $q->where('join_date', '>=', $join_date);
		        $q->where('stop_date', '<=', $join_date);
		    })
		    ->where('assigned_status', 0)
		    ->where('student_id', $student_id)
		    ->first();

	        if (!empty($date_exist->student_vehicle_assigned_detail_id))
	        {
		$return = false;
	        }
	    }
	}
	}
	return response()->json($return);
        }

    }
    