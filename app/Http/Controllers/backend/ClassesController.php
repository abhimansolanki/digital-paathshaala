<?php

	namespace App\Http\Controllers\backend;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Model\backend\Classes;
	use App\Model\backend\ClassSection;
	use App\Model\backend\Student;
	use Validator;
	use Illuminate\Support\Facades\Input;
	use Datatables;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\View as View;

	class ClassesController extends Controller
	{

		public function __construct()
		{

		}

		public function add(Request $request, $id = NULL)
		{
			$classes = [];
			$arr_selected_class_subject = [];
			$selected_class_section = [];
			$class_id = null;
			if (!empty($id))
			{
				$session = get_current_session();
				$session_id = $session['session_id'];
				$decrypted_class_id = get_decrypted_value($id, true);
				$classes = Classes::where('class_id', $decrypted_class_id)
				->with(['classSection' => function($q) use ($session_id)
				{
					$q->where('session_id', $session_id);
				}])
				->first();
				if (isset($classes['classSection']))
				{
					foreach ($classes['classSection'] as $key => $class_section)
					{
						$selected_class_section[] = array(
							'class_section_id' => $class_section['class_section_id'],
							'section_id' => $class_section['section_id'],
							'session_id' => $class_section['session_id'],
							'seats' => $class_section['seats'],
							'strength' => $class_section['strength'],
						);
					}
				}
				if (!$classes)
				{
					return redirect('admin/classes')->withError('Class not found!');
				}
				$page_title = 'Edit Class';
				$encrypted_class_id = get_encrypted_value($classes->class_id, true);
				$save_url = url('admin/classes/save/' . $encrypted_class_id);
				$submit_button = 'Update';
				$class_id = $decrypted_class_id;
				$arr_session = get_session();
			} else
			{
				$arr_session = get_session('current');
				$page_title = 'Create Class';
				$save_url = url('admin/classes/save');
				$submit_button = 'Save';
			}

			$classes['arr_session'] = add_blank_option($arr_session, '-- Section session --');
			$classes['arr_section'] = add_blank_option(get_section(), '--Section--');
			$classes['selected_section_id'] = $selected_class_section;
			$data = array(
				'page_title' => $page_title,
				'save_url' => $save_url,
				'submit_button' => $submit_button,
				'classes' => $classes,
				'redirect_url' => url('admin/classes/'),
			);
			return view('backend.classes.add')->with($data);
		}

		public function save(Request $request, $id = NULL)
		{
			$decrypted_class_id = get_decrypted_value($id, true);
			if (!empty($id))
			{
				$classes = Classes::find($decrypted_class_id);

				if (!$classes)
				{
					return redirect('/admin/classes/')->withError('Classes not found!');
				}
				$success_msg = 'Class updated successfully!';
			} else
			{
				$classes = New Classes;
				$success_msg = 'Class saved successfully!';
			}
			$validatior = Validator::make($request->all(), [
				'class_name' => 'required|unique:classes,class_name,' . $decrypted_class_id . ',class_id',
				'session_id' => 'required',
			]);
			if ($validatior->fails())
			{
				return redirect()->back()->withInput()->withErrors($validatior);
			} else
			{
		    DB::beginTransaction(); //Start transaction!

		    try
		    {
		    	$classes->class_name = Input::get('class_name');
		    	if (empty($id))
		    	{
		    		$class_order = Classes::max('class_order');
		    		$classes->class_order = ($class_order + 1);
		    	}
		    	$classes->save();
		    	$class_section_id = Input::get('class_section_id');
		    	$arr_section_id = Input::get('section_id');
		    	$arr_seats = Input::get('seats');
		    	$arr_strength = Input::get('strength');
		    	$session_id = Input::get('session_id');
		    	if (!empty($arr_section_id))
		    	{
		    		$arr_section = [];
		    		foreach ($arr_section_id as $key => $section_id)	
		    		{
		    			if (!empty($section_id))
		    			{
		    				if (!empty($class_section_id[$key]))
		    				{
		    					$class_section_data = ClassSection::find($class_section_id[$key]);
		    				} else
		    				{
		    					$class_section_data = new ClassSection;
		    				}
		    				$class_section_data->session_id = $session_id;
		    				$class_section_data->section_id = $section_id;
		    				$class_section_data->seats = $arr_seats[$key];
		    				$class_section_data->strength = $arr_strength[$key];
		    				$arr_section[] = $class_section_data;
		    			}
		    		}
		    		if (!empty($arr_section))
		    		{
	//                            p($arr_section);
		    			$classes->classSection()->saveMany($arr_section);
		    		}
		    	}
		    } catch (\Exception $e)
		    {
		        //failed logic here
		    	DB::rollback();
		    	$error_message = $e->getMessage();
		    	return redirect()->back()->withErrors($error_message);
		    }

		    DB::commit();
		}

		return redirect('admin/classes')->withSuccess($success_msg);
	}

	public function destroy(Request $request)
	{
		$class_id = Input::get('class_id');
		$class = Classes::find($class_id);
		if ($class)
		{
		    DB::beginTransaction(); //Start transaction!
		    try
		    {
		    	$class->delete();
		    	$return_arr = array(
		    		'status' => 'success',
		    		'message' => 'Class deleted successfully!'
		    	);
		    } catch (\Exception $e)
		    {
		        //failed logic here
		    	DB::rollback();
		    	$error_message = $e->getMessage();
		    	$return_arr = array(
		    		'status' => 'used',
		    		'message' => trans('language.delete_message')
		    	);
		    }
		    DB::commit();
		} else
		{
			$return_arr = array(
				'status' => 'error',
				'message' => 'Class not found!'
			);
		}
		return response()->json($return_arr);
	}

	public function anyData()
	{
		$classes = [];
		$offset = Input::get('start');
		$limit = Input::get('length');
		$session = get_current_session();

		if (!empty($session['session_id']))
		{
			$session_id = $session['session_id'];
			$arr_classes = Classes::orderBy('class_order', 'ASC')
			->whereHas('classSection', function($q) use ($session_id)
			{
				$q->where('session_id', $session_id);
			})
			->with(['classSection' => function($q) use ($session_id)
			{

				$q->select('class_section_id', 'class_id', 'section_id', 'seats', 'strength');
			}])
			->select('class_id', 'class_name', 'class_status', 'class_order')->get();

			$check_current_session = check_current_session();
			foreach ($arr_classes as $key => $value)
			{
				$class_data['class_id'] = $value['class_id'];
				$class_data['class_name'] = $value['class_name'];
				$class_data['class_order'] = $value['class_order'];
				$class_data['current_session'] = $check_current_session;
				$class_data['class_section'] = [];
				foreach ($value['classSection'] as $key => $classSection)
				{
					$class_data['class_section'][$classSection['section_id']] = array(
						'section_id' => $classSection['section_id'],
						'section_name' => $classSection['Section']['section_name'],
						'seats' => $classSection['seats'],
						'strength' => $classSection['strength'],
					);
				}
				$classes[] = (object) $class_data;
			}
		}
		return Datatables::of($classes)
		->addColumn('action', function ($classes)
		{
			$disabled = 'disabled';
			if ($classes->current_session == true)
			{
				$disabled = '';
			}
			$encrypted_class_id = get_encrypted_value($classes->class_id, true);
			$delete = '';
			if ($classes->class_id != 1)
			{
				$delete = '<button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $classes->class_id . '" "' . $disabled . '"><i class="fa fa-trash"></i></button>';
			}
			return '<a title="Edit" id="deletebtn1" href="' . url('admin/classes/' . $encrypted_class_id) . '" class="btn btn-success" "' . $disabled . '"><i class="fa fa-edit" ></i></a>'
			. $delete;
		})->make(true);
	}

	function getClassOrder(Request $request)
	{
		$classes = Classes::where('class_status', 1)->orderBy('class_order', 'ASC')->get();
		$data['status'] = 'success';
		$data['arr_classes'] = $classes;
		return response()->json($data);
	}

	function setClassOrder(Request $request)
	{
		if ($request->ajax())
		{
			$arr_class_order = Input::get('item');
			foreach ($arr_class_order as $key => $class_id)
			{
				$class_order = $key + 1;
				$classes = Classes::find($class_id);
				$classes->class_order = $class_order;
				$classes->save();
			}
			return response()->json('success');
		}
		$classes = Classes::where('class_status', 1)->orderBy('class_order', 'ASC')->get();
		$data['arr_classes'] = $classes;
		return view('backend.classes.class-order')->with($data);
	}

	function getClassSectionStrength(Request $request)
	{
		$section_id = Input::get('section_id');
		$class_id = Input::get('class_id');
		$session_id = Input::get('session_id');
		$seats_strength = ClassSection::where('class_id', $class_id)->where('section_id', $section_id)->first();
		$students = Student::where('current_session_id', $session_id)
		->where('current_class_id', $class_id)
		->where('current_section_id', $section_id)
		->where('student_status', 1)->count();
		$status = 'failed';
		$return = [];
		if (!empty($seats_strength))
		{
			$available = $seats_strength->strength - $students;
			$return = array(
				'available' => $available > 0 ? $available : 0,
				'strength' => $seats_strength->strength,
				'admission' => $students,
			);
			$status = 'success';
		}
		return response()->json(array('status' => $status, 'data' => $return));
	}

	public function getClassExam($class_id)
	{
		$class_exam = [];
		$arr_class_exam = Classes::where('class_id', $class_id)->with('classExam')->first();
		if (!empty($arr_class_exam['classExam']))
		{
			foreach ($arr_class_exam['classExam'] as $key => $data)
			{
				$class_exam [$data['exam_id']] = $data['exam_name'];
			}
		}
		return $class_exam;
	}

	public function deleteSectionClass(Request $request)
	{
		$class_section_id = Input::get('class_section_id');
		$class_section = ClassSection::find($class_section_id);
		if ($class_section)
		{
		    DB::beginTransaction(); //Start transaction!

		    try
		    {
		    	$class_section->delete();
		    	$return_arr = array(
		    		'status' => 'success',
		    		'message' => 'Class section unmapped successfully!'
		    	);
		    } catch (\Exception $e)
		    {
		        //failed logic here
		    	DB::rollback();
		    	$error_message = $e->getMessage();
		    	$return_arr = array(
		    		'status' => 'used',
		    		'message' => trans('language.delete_message')
		    	);
		    }
		    DB::commit();
		} else
		{
			$return_arr = array(
				'status' => 'error',
				'message' => 'Class section mapping not found!'
			);
		}
		return response()->json($return_arr);
	}

	}
