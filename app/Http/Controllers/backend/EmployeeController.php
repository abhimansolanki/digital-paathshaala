<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Employee;
    use \App\Model\backend\EmployeeDocument;
    use App\Model\backend\AdminUser;
    use App\Model\backend\UserType;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Hash;
     use Illuminate\Support\Facades\Mail;
    use App\Mail\SendMailable;

    class EmployeeController extends Controller
    {

        public function __construct()
        {
            
        }

        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $data = array(
                'redirect_url' => url('admin/employee/add'),
                'page_title'   => trans('language.list_employee'),
                'import_url'   => url('admin/import-employee'),
            );
            return view('backend.employee.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {
            $employee = [];
            if (!empty($id))
            {
                $decrypted_employee_id = get_decrypted_value($id, true);
                $employee              = $this->getEmployeeData($decrypted_employee_id);
                $employee              = isset($employee[0]) ? $employee[0] : array();
                if (!$employee)
                {
                    return redirect('admin/employee')->withError('Employee not found!');
                }
                $encrypted_employee_id = get_encrypted_value($employee['employee_id'], true);
                $save_url              = url('admin/employee/save/' . $encrypted_employee_id);
                $submit_button         = 'Update';
            }
            else
            {
                $save_url      = url('admin/employee/save');
                $submit_button = 'Save';

                $employee['employee_code'] = $this->getEmployeeCode();
            }

            $arr_gender                    = \Config::get('custom.employee_gender');
            $arr_marital                   = \Config::get('custom.arr_marital_status');
            $arr_session                   = get_session();
            $arr_bank                      = get_bank();
            $arr_department                = get_employee_department();
            $arr_category                  = get_caste_category();
            $arr_document_type             = get_document_type();
            $employee['arr_gender']        = add_blank_option($arr_gender, '-- Select gender --');
            
	$employee['arr_marital']       = add_blank_option($arr_marital, '-- Select gender --');
//            p($employee);
	$employee['arr_department']    = add_blank_option($arr_department, '--Select department --');
            $employee['arr_user_role']     = add_blank_option(get_user_role(), '--Select designation --');
            $employee['arr_staff_type']     = add_blank_option(\Config::get('custom.arr_staff_type'), '--Select Staff Type --');
            $employee['arr_session']       = add_blank_option($arr_session, '--Select session --');
            $employee['arr_bank']          = add_blank_option($arr_bank, '--Select bank --');
            $employee['arr_category']      = add_blank_option($arr_category, '-- Select category --');
            $employee['arr_document_type'] = add_blank_option($arr_document_type, '--Select document --');

	
            $data = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'employee'      => $employee,
                'redirect_url'  => url('admin/employee/'),
            );
            return view('backend.employee.add')->with($data);
        }

        public function getEmployeeCode()
        {
            $employee_code_prefix = 'EMP';
            $employee_code        = $employee_code_prefix . '01';
            $data_employee_code   = Employee::orderBy('employee_id', 'desc')->first();
            if (!empty($data_employee_code->employee_id))
            {
                $employee_id   = ($data_employee_code->employee_id) + 1;
                $employee_id   = str_pad($employee_id, 2, '0', STR_PAD_LEFT);
                $employee_code = $employee_code_prefix . $employee_id;
            }
            return $employee_code;
        }

        public function save(Request $request, $id = NULL)
        {
            $admin_user_id         = null;
            $user_type_id          = null;
            $decrypted_employee_id = null;
            if (!empty($id))
            {
                $decrypted_employee_id = get_decrypted_value($id, true);

                $employee      = Employee::Find($decrypted_employee_id);
                $user          = AdminUser::Find($employee['admin_user_id']);
                $admin_user_id = $user->admin_user_id;

                if (!$employee)
                {
                    return redirect('/admin/employee/')->withError('Employee not found!');
                }
                $success_msg = 'Employee updated successfully!';
            }
            else
            {
                $employee     = New Employee;
                $user         = new AdminUser();
                $user_type    = UserType::with('userRole')
                        ->where('user_type', 'Staff')->select('user_type_id')->first();
                $user_type_id = $user_type['user_type_id'];

                $success_msg = 'Employee saved successfully!';
            }
            $arr_input_fields = [
                'employee_code'     => 'required|unique:employees,employee_code,' . $decrypted_employee_id . ',employee_id',
                'full_name'         => 'required',
                'father_name'       => 'required',
                'gender'            => 'required',
                'marital_status'    => 'required',
                'dob'               => 'required',
                'email'             => 'required|unique:employees,email,' . $decrypted_employee_id . ',employee_id',
                'contact_number'    => 'required',
                'address'           => 'required',
                'user_role_id'      => 'required',
                'staff_type_id'     => 'required',
                'department_id'     => 'required',
                'caste_category_id' => 'required',
                'join_date'         => 'required',
                'aadhaar_number'    => 'required|unique:employees,aadhaar_number,' . $decrypted_employee_id . ',employee_id',
                'qualification'     => 'required',
            ];

            $validatior = Validator::make($request->all(), $arr_input_fields);
            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction(); //Start transaction!

                try
                {
                    // employee document folder path
                    $config_upload_path = \Config::get('custom.employee_document');

                    //inserting data in multiples tables

                    $user->email = Input::get('email');
                    if (empty($id))
                    {
                        $employee_password    = \Config::get('custom.employee_password');
                        $user->password       = Hash::make($employee_password);
                        $session              = db_current_session();
                        $session_id           = $session['session_id'];
                        $employee->session_id = $session_id;
                    }
                    if (!empty($user_type_id))
                    {
                        $user->user_type_id = $user_type_id;
                    }
                    $user_role_id = Input::get('user_role_id');
                    if (!empty($user_role_id))
                    {
                        $user->user_role_id = $user_role_id;
                    }
                    if ($user->save())
                    {
                        $contact_number = Input::get('contact_number');
                        $email = Input::get('email');
                        $employee->employee_code     = Input::get('employee_code');
                        $employee->full_name         = Input::get('full_name');
                        $employee->father_name       = Input::get('father_name');
                        $employee->gender            = Input::get('gender');
                        $employee->marital_status    = Input::get('marital_status');
                        $employee->dob               = get_formatted_date(Input::get('dob'), 'database');
                        $employee->contact_number    = $contact_number;
                        $employee->address           = Input::get('address');
                        $employee->email             = $email;
                        $employee->department_id     = Input::get('department_id');
                        $employee->caste_category_id = Input::get('caste_category_id');
                        $employee->join_date         = get_formatted_date(Input::get('join_date'), 'database');
                        $employee->pan_number        = Input::get('pan_number');
                        $employee->aadhaar_number    = Input::get('aadhaar_number');
                        $employee->bank_name         = Input::get('bank_name');
                        $employee->ifsc_code         = Input::get('ifsc_code');
                        $employee->qualification     = Input::get('qualification');
                        $employee->specialization    = Input::get('specialization');
                        $employee->experience        = Input::get('experience');
                        $employee->reference         = Input::get('reference');
                        $employee->tds_deducation    = Input::get('tds_deducation');
                        $employee->medical_leave     = Input::get('medical_leave');
                        $employee->casual_leave_pm   = Input::get('casual_leave_pm');
                        $employee->paid_leave_pm     = Input::get('paid_leave_pm');
                        $employee->account_number    = Input::get('account_number');
                        $employee->qualification     = Input::get('qualification');
                        $employee->specialization    = Input::get('specialization');
                        $employee->experience        = Input::get('experience');
                        $employee->reference         = Input::get('reference');
                        $employee->staff_type_id     = Input::get('staff_type_id');

                        // upload employee profile photo 
                        if (!empty($request->hasFile('profile_photo')))
                        {
                            $file                    = $request->file('profile_photo');
                            $destinationPath         = public_path() . $config_upload_path['upload_path'];
                            $filename                = $file->getClientOriginalName();
                            $rand_string             = quick_random();
                            $filename                = $rand_string . '-' . $filename;
                            $file->move($destinationPath, $filename);
                            $employee->profile_photo = $filename;
                        }

                        // save employee
                        $user->userEmployee()->save($employee);

                        // upload employee document
                        $arr_document_type_id = Input::get('document_type_id');
                        $employee_document_id = Input::get('employee_document_id');
                        $arr_document         = [];
                        if (!empty($arr_document_type_id))
                        {
                            foreach ($arr_document_type_id as $key => $document_type_id)
                            {
                                if (!empty($document_type_id) && $request->hasFile('document_url' . $key))
                                {
                                    if (!empty($employee_document_id[$key]))
                                    {
                                        $employee_document = EmployeeDocument::Find($employee_document_id[$key]);
                                    }
                                    else
                                    {
                                        $employee_document = new EmployeeDocument;
                                    }
                                    $file               = $request->file('document_url' . $key);
                                    $config_upload_path = \Config::get('custom.employee_document');
                                    $destinationPath    = public_path() . $config_upload_path['upload_path'];
                                    $filename           = $file->getClientOriginalName();
                                    $rand_string        = quick_random();
                                    $filename           = $rand_string . '-' . $filename;
                                    $file->move($destinationPath, $filename);

                                    $employee_document->document_type_id = $document_type_id;
                                    $employee_document->document_url     = $filename;

                                    $arr_document[] = $employee_document;
                                }
                            }
                        }
                        if (!empty($arr_document))
                        {
                            $employee->employeeDocument()->saveMany($arr_document);
                        }
                        
                            // send sms  and mail to employee
                            if (!empty($contact_number)  && !empty($email) && empty($id))
                            {
                                $employee_password    = \Config::get('custom.employee_password');
                                $data['view_blade'] = 'employee-registration';
                                $data['subject']    = trans('language.create_empployee');
                                $message_text_mail  = trans('language.emp_registration_message');
                                $data['body']       = array(
                                    'message'           => $message_text_mail,
                                    'employee_code' => $employee->employee_code,
                                    'user_name' => $email,
                                    'password' => $employee_password,
                                    'registration_date'    => date('d-m-Y'),
                                );
//                                $mail_response      = Mail::to($email)->send(new SendMailable($data));
                                
                                $sms_mobile_number = $contact_number;
                                $message_text      = "Hello, registration has been created successfully. ";
                                $message_text      .= "Your login credentials are : ";
                                $message_text      .= "user name = " . $email;
                                $message_text      .= " and password = " . $employee_password;
                                $message_text      .= " Thank you. ";
                                $response          = send_sms($sms_mobile_number, $message_text);
                            }
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                    return redirect()->back()->withInput()->withErrors($error_message);
                }
                DB::commit();
            }
            return redirect('admin/employee/')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $employee_id = Input::get('employee_id');
            $employee    = Employee::find($employee_id);
            if ($employee)
            {
                DB::beginTransaction();
                try
                {
                    $employee->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Employee deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Employee not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $employee     = [];
            $employee_id  = null;
            $offset       = Input::get('start');
            $limit        = Input::get('length');
            $arr_employee = $this->getEmployeeData($employee_id, $offset, $limit);
            foreach ($arr_employee as $key => $employee_data)
            {
                $employee[$key] = (object) $employee_data;
            }
            return Datatables::of($employee)
                    ->addColumn('action', function ($employee)
                    {
                        $encrypted_employee_id = get_encrypted_value($employee->employee_id, true);
                        return '<a title="Edit" id="deletebtn1" href="employee/add/' . $encrypted_employee_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $employee->employee_id . '"><i class="fa fa-trash"></i></button>';
                    })
                    ->rawColumns(['checkbox', 'action', 'view_print'])->make(true);
        }

        function getEmployeeData($employee_id = null, $offset = null, $limit = null)
        {
            $employee_return   = [];
            $employee          = [];
            $arr_employee_data = Employee::where('employee_status', 1)
                ->where(function($query) use ($employee_id)
                {
                    if (!Empty($employee_id))
                    {
                        $query->where('employee_id', $employee_id);
                    }
                })
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->skip($limit);
//                    }
//                })
                ->with('employeeUser.userRole')
                ->with('employeeDepartment')
                ->with('employeeDocument')
                ->get();
            foreach ($arr_employee_data as $key => $employee_data)
            {
                $employee = array(
                    'employee_id'        => $employee_data['employee_id'],
                    'admin_user_id'      => $employee_data['admin_user_id'],
                    'employee_code'      => $employee_data['employee_code'],
                    'bank_name'          => $employee_data['bank_name'],
                    'ifsc_code'          => $employee_data['ifsc_code'],
                    'join_date'          => get_formatted_date($employee_data['join_date'], 'display'),
                    'session_id'         => $employee_data['session_id'],
                    'caste_category_id'  => $employee_data['caste_category_id'],
                    'department_id'      => $employee_data['department_id'],
                    'full_name'          => $employee_data['full_name'],
                    'father_name'        => $employee_data['father_name'],
                    'dob'                => get_formatted_date($employee_data['dob'], 'display'),
                    'gender'             => $employee_data['gender'],
                    'marital_status'     => $employee_data['marital_status'],
                    'gender_name'        => get_gender($employee_data['gender'], 'employee_gender'),
                    'address'            => $employee_data['address'],
                    'email'              => $employee_data['email'],
                    'contact_number'     => $employee_data['contact_number'],
                    'pan_number'         => $employee_data['pan_number'],
                    'aadhaar_number'     => $employee_data['aadhaar_number'],
                    'account_number'     => $employee_data['account_number'],
                    'paid_leave_pm'      => $employee_data['paid_leave_pm'],
                    'casual_leave_pm'    => $employee_data['casual_leave_pm'],
                    'medical_leave'      => $employee_data['medical_leave'],
                    'tds_deducation'     => $employee_data['tds_deducation'],
                    'tds_deducation'     => $employee_data['tds_deducation'],
                    'qualification'      => $employee_data['qualification'],
                    'specialization'     => $employee_data['specialization'],
                    'experience'         => $employee_data['experience'],
                    'reference'          => $employee_data['reference'],
                    'staff_type_id'      => $employee_data['staff_type_id'],
                    'profile_photo'      => check_file_exist($employee_data['profile_photo'], 'employee_document'),
                    'document_type_data' => [],
                );
                if (isset($employee_data['employeeDocument']))
                {
                    foreach ($employee_data['employeeDocument'] as $key => $document_type)
                    {
                        $employee['document_type_data'][] = array(
                            'employee_document_id' => $document_type['employee_document_id'],
                            'document_type_id'     => $document_type['document_type_id'],
                            'document_url'         => check_file_exist($document_type['document_url'], 'employee_document'),
                        );
                    }
                }
                if (isset($employee_data['employeeUser']['userRole']))
                {
                    $employee['user_role']    = $employee_data['employeeUser']['userRole']['user_role_name'];
                    $employee['user_role_id'] = $employee_data['employeeUser']['userRole']['user_role_id'];
                }
                if (isset($employee_data['employeeDepartment']))
                {
                    $employee['department_name'] = $employee_data['employeeDepartment']['department_name'];
                }
                $employee_return[] = $employee;
            }
//            p($employee_return);
            return $employee_return;
        }

        function getEmployee()
        {
            $department_id     = Input::get('department_id');
            $arr_employee_list = [];
            $message           = 'Employee not found!';
            $status            = 'failed';
            if (!empty($department_id))
            {
                $arr_employee = get_employee_by_department($department_id);
                foreach ($arr_employee as $employee)
                {
                    $arr_employee_list[$employee['employee_id']] = $employee['employee_code'] . ' : ' . $employee['full_name']; // . '(' . $care_of_name . ')';
                }
                $message = 'Got result successfully';
                $status  = 'success';
            }
            return response()->json($result = array('status' => $status, 'message' => $message, 'data' => $arr_employee_list));
        }

        /*
         * Delete employee document
         */

        public function deleteEmployeeDocument(Request $request)
        {
            $employee_document_id = Input::get('employee_document_id');
            $employee_document    = EmployeeDocument::find($employee_document_id);
            if ($employee_document)
            {
                DB::beginTransaction(); //Start transaction!

                try
                {
                    $config_upload_path = \Config::get('custom.employee_document');
                    $employee_document->delete();
                    unlink($config_upload_path['display_path'] . $employee_document['document_url']);
                    $return_arr         = array(
                        'status'  => 'success',
                        'message' => 'Employee Document deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Employee Document not found!'
                );
            }
            return response()->json($return_arr);
        }

        /*
         * Import Employee  List 
         */

        public function importStudent(Request $request)
        {
            if ($request->hasFile('import_employee'))
            {
                $arr_input_fields = [
                    'import_employee' => 'required',
                ];

                $validatior = Validator::make($request->all(), $arr_input_fields);
                if ($validatior->fails())
                {
                    return redirect()->back()->withInput()->withErrors($validatior);
                }
                else
                {
                    $destinationPath = public_path() . '/uploads/employee-import/';
                    $file            = $request->File('import_employee');
                    $filename        = $file->getClientOriginalName();
                    $path            = $destinationPath . $filename;
                    $file->move($destinationPath, $filename);
                    //file permission
                    $status          = 'failed';
                    chmod($path, 0777);
                    $data            = \Excel::load($path)->get();

                    if ($data->count())
                    {
                        $session = db_current_session();
                        foreach ($data as $key => $value)
                        {
                            if (!empty($value->emailaddress))
                            {
                                DB::beginTransaction(); //Start transaction!
                                try
                                {
                                    $isUserExist = Employee::where('contact_number', $value->contactno)->whereOr('email', $value->emailaddress)->get()->count();
                                    if ($isUserExist == 0)
                                    {
                                        $user              = new AdminUser();
                                        $user_type         = UserType::with('userRole')
                                                ->where('user_type', 'Staff')->select('user_type_id')->first();
                                        $user_type_id      = $user_type['user_type_id'];
                                        $user->email       = $value->emailaddress;
                                        $employee_password = \Config::get('custom.employee_password');
                                        $user->password    = Hash::make($employee_password);

                                        if (!empty($user_type_id))
                                        {
                                            $user->user_type_id = $user_type_id;
                                        }
                                        $user_role_id = $value->userroleid;
                                        if (!empty($user_role_id))
                                        {
                                            $user->user_role_id = $user_role_id;
                                        }
                                        if ($user->save())
                                        {
                                            $employee_data                    = New Employee;
                                            $employee_data->session_id        = $session['session_id'];
                                            $employee_data->department_id     = $value->departmentid;
                                            $employee_data->caste_category_id = $value->categoryid;
                                            $employee_data->employee_code     = $this->getEmployeeCode();
                                            $employee_data->full_name         = $value->fullname;
                                            $employee_data->caste_category_id = $value->categoryid;
                                            $employee_data->father_name       = $value->fathername;
                                            $employee_data->marital_status    = strtolower($value->maritalstatus) == 'yes' ? 1 : 0;
                                            $employee_data->gender            = $value->gender == 'M' ? 1 : 0;
                                            $employee_data->dob               = $value->dob;
                                            $employee_data->email             = $value->emailaddress;
                                            $employee_data->address           = $value->fathername;
                                            $employee_data->contact_number    = $value->contactno;
                                            $employee_data->pan_number        = $value->panno;
                                            $employee_data->aadhaar_number    = $value->aadhaarno;
                                            $employee_data->join_date         = $value->joindate;
                                            $employee_data->bank_name         = $value->bankname;
                                            $employee_data->ifsc_code         = $value->ifsccode;
                                            $employee_data->account_number    = $value->accountno;
                                            $employee_data->paid_leave_pm     = $value->paidleave;
                                            $employee_data->casual_leave_pm   = $value->fathername;
                                            $employee_data->medical_leave     = $value->medicalleave;
                                            $employee_data->tds_deducation    = $value->tdsdeduction;
                                            $employee_data->qualification     = $value->qualification;
                                            $employee_data->specialization    = $value->specialization;
                                            $employee_data->experience        = $value->experience;
                                            $employee_data->reference         = $value->reference;
                                            $employee_data->staff_type_id     = $value->staff_type_id;
                                            // save employee
                                            $user->userEmployee()->save($employee_data);
                                        }
                                    }
                                }
                                catch (\Exception $e)
                                {
                                    //failed logic here
                                    DB::rollback();
                                    $error_message = $e->getMessage();
                                    return redirect()->back()->withErrors($error_message);
                                }
                                DB::commit();
                                $status = 'success';
                            }
                        }
                    }
                    // remove imported file
                    unlink($path);
                    if ($status == 'success')
                    {
                        return redirect('admin/employee/')->withSuccess('Employee imported successfully.');
                    }
                    else
                    {
                        return redirect('admin/employee/')->withError('Something went wrong, please try again');
                    }
                }
            }
            else
            {
                return redirect()->back()->withErrors('Something went wrong');
            }
        }

    }
    