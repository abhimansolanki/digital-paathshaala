<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Room;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class RoomController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index()
        {
            $data = array(
                'redirect_url' => url('admin/room/add'),
            );
            return view('backend.room.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {
            $room    = [];
            $room_id = null;
            if (!empty($id))
            {
                $decrypted_room_id = get_decrypted_value($id, true);
                $room              = $this->getRoomData($decrypted_room_id);
                $room              = isset($room[0]) ? $room[0] : [];
                $room              = (object) $room;
                if (!$room)
                {
                    return redirect('admin/room')->withError('Room not found!');
                }
                $encrypted_room_id = get_encrypted_value($room->room_id, true);
                $save_url          = url('admin/room/save/' . $encrypted_room_id);
                $submit_button     = 'Update';
                $room_id           = $decrypted_room_id;
            }
            else
            {
                $save_url      = url('admin/room/save');
                $submit_button = 'Save';
            }

            $data = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'room'          => $room,
                'redirect_url'  => url('admin/room/'),
            );
            return view('backend.room.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_room_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $room = Room::find($decrypted_room_id);

                if (!$room)
                {
                    return redirect('/admin/room/')->withError('Room not found!');
                }
                $success_msg = 'Room updated successfully!';
            }
            else
            {
                $room        = New Room;
                $success_msg = 'Room saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'room_number' => 'required|unique:rooms,room_number,' . $decrypted_room_id . ',room_id',
                    'total_seats' => 'required',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $room->room_number = Input::get('room_number');
                    $room->total_seats = Input::get('total_seats');
                    $room->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withInput()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/room')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $room_id = Input::get('room_id');
            $room    = Room::find($room_id);
            if ($room)
            {
                $room->delete();
                $return_arr = array(
                    'status'  => 'success',
                    'message' => 'Room deleted successfully!'
                );
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Room not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $room     = [];
            $arr_room = $this->getRoomData();
            foreach ($arr_room as $key => $room_data)
            {
                $room[$key] = (object) $room_data;
            }
            return Datatables::of($room)
                    ->addColumn('action', function ($room)
                    {
                        $encrypted_room_id = get_encrypted_value($room->room_id, true);
                        return '<a title="Edit" id="deletebtn1" href="room/add/' . $encrypted_room_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $room->room_id . '"><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

        public function getRoomData($room_id = array())
        {
            $room_return   = [];
            $arr_room_data = Room::where(function($query) use ($room_id)
                {
                    if (!empty($room_id))
                    {
                        $query->where('room_id', $room_id);
                    }
                })->get();
            if (!empty($arr_room_data))
            {
                foreach ($arr_room_data as $key => $room_data)
                {
                    $room_return[] = array(
                        'room_id'     => $room_data['room_id'],
                        'room_number' => $room_data['room_number'],
                        'total_seats' => $room_data['total_seats'],
                    );
                }
            }
            return $room_return;
        }

    }
    