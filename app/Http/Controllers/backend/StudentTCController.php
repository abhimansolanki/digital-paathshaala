<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\CronController;
use App\Model\backend\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\StudentTc;
use Validator;
use Illuminate\Support\Facades\Input;
use Datatables;
use Illuminate\Support\Facades\DB;
use PDF;
use View;

class StudentTCController extends Controller
{

    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = array(
            'redirect_url' => url('admin/student-tc/add'),
            'page_title' => trans('language.list_student_tc'),
            'arr_class' => add_blank_option(get_all_classes(), '--Select Class--'),
        );
        return view('backend.student-tc.index')->with($data);
    }

    public function add(Request $request, $id = NULL, $student_id = '')
    {
        $student_tc = [];
        $class_id = null;
        $session_id = null;
        $section_id = null;
        $student_id = null;
        $tc_number = 1;
        $session = db_current_session();
        if (!empty($id)) {
            $decrypted_student_tc_id = get_decrypted_value($id, true);
            $student_tc = StudentTc::Find($decrypted_student_tc_id)->toArray();
            $session_id = $student_tc['session_id'];
            $class_id = $student_tc['class_id'];
            $student_tc['tc_date'] = get_formatted_date($student_tc['tc_date'], 'display');
            $data = $this->getStudentDetailsData($session_id, $class_id, $student_tc['section_id'], $student_tc['student_id'], 1);
            $student_tc = array_merge($student_tc, $data);
            if (!$student_tc) {
                return redirect('admin/student')->withError('Student not found!');
            }
            $student = Student::find($student_tc['student_id']);
            $encrypted_student_tc_id = get_encrypted_value($student_tc['student_tc_id'], true);
            $page_title = 'Edit Student TC';
            $save_url = url('admin/student-tc/save/' . $encrypted_student_tc_id);
            $submit_button = 'Update';
            $edit_student_url = url('admin/student/add/' . get_encrypted_value($student_tc['student_id'], true));
            $arr_student = get_student_list_by_class_section($session_id, $class_id);
            $student_tc['last_session'] = $student->getSession->session_year;
            $student_tc['last_class'] = $student->getClass->class_name;
            $student_tc['tc_application_date'] = get_formatted_date($student_tc['tc_application_date'], 'display');
            $student_tc['final_date'] = get_formatted_date($student_tc['final_date'], 'display');
        } else {
            $page_title = 'Create Student TC';
            $save_url = url('admin/student-tc/save');
            $submit_button = 'Save';

            $tc = StudentTc::orderBy('student_tc_id', 'DESC')->first();
            if (!empty($tc->student_tc_id)) {
                $tc_number = $tc->student_tc_id + 1;
            }
            $student_tc['tc_number'] = $tc_number;
            $student_tc['session_id'] = $session['session_id'];
            $edit_student_url = url('admin/student');
            $arr_student = [];
        }

        $student_tc['session'] = $session;
        $arr_class = get_all_classes();
        $arr_section = get_class_section($class_id, $session_id);
        $student_tc['arr_section'] = add_blank_option($arr_section, '--Select section --');
        $student_tc['arr_student'] = add_blank_option($arr_student, '--Select Student --');
        $student_tc['arr_class'] = add_blank_option($arr_class, '--Select class --');
        $student_tc['status'] = add_blank_option(\Config::get('custom.status'), '-- Select --');
        $student_tc['arr_tc_status'] = add_blank_option(\Config::get('custom.tc_status'), '-- Select --');

        $data = array(
            'page_title' => $page_title,
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'student_tc' => $student_tc,
            'edit_student_url' => $edit_student_url,
            'redirect_url' => url('admin/student-tc'),
        );
        return view('backend.student-tc.add')->with($data);
    }

    public function save(Request $request, $id = NULL)
    {
        $decrypted_student_tc_id = null;
        $session_id = Input::get('session_id');
        $student_id = Input::get('student_id');
        $class_id = Input::get('class_id');
        $section_id = Input::get('section_id');
        if (!empty($id)) {
            $decrypted_student_tc_id = get_decrypted_value($id, true);
            $student_tc = StudentTc::find($decrypted_student_tc_id);
            if (!$student_tc) {
                return redirect('/admin/student-tc/')->withError('Student TC not found!');
            }
            $success_msg = 'Student TC updated successfully!';
            $student_id = $student_tc->student_id;
            $session_id = $student_tc->session_id;
            $class_id = $student_tc->class_id;
            $section_id = $student_tc->section_id;
        } else {
            $student_tc = new StudentTc;
            $success_msg = 'Student TC saved successfully!';
            $student_id = Input::get('student_id');
            $session_id = Input::get('session_id');
            $class_id = Input::get('class_id');
            $section_id = Input::get('section_id');
        }
        $arr_input_fields = [
            'tc_number' => 'required|unique:student_tcs,tc_number,' . $decrypted_student_tc_id . ',student_tc_id',
            'print_tc_number' => 'required|unique:student_tcs,print_tc_number,' . $decrypted_student_tc_id . ',student_tc_id',
            'tc_date' => 'required',
            'tc_application_date' => 'required',
            'final_date' => 'required',
            'exam_result' => 'required',
            'student_conduct' => 'required',
            'total_working_day' => 'required',
            'total_attendance' => 'required',
            'tc_reason' => 'required',
        ];
        if($id = NULL){
            $arr_input_fields['class_id'] = 'required';
            $arr_input_fields['session_id'] = 'required';
            $arr_input_fields['section_id'] = 'required';
        }

        $validatior = Validator::make($request->all(), $arr_input_fields);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            $obj = new StudentFeeReceiptNewController();
            $request->request->add(['arr_student' => [$student_id], 'fee_receipt_date' => date('d/m/Y'), 'fee_type_id' => NULL, 'request_type' => 'API']);
            $pendingFeeDetails = $obj->studentFeeDetail();
            if(!empty($pendingFeeDetails) && $pendingFeeDetails['fee_detail']['payable_fee'] > 0){
                return redirect()->back()->with("info","Student fee is due. Can't generate the TC. Please submit pending fee first.");
            }
            DB::beginTransaction(); //Start transaction!
            try {
                $student_tc->session_id = $session_id;
                $student_tc->student_id = $student_id;
                $student_tc->class_id = $class_id;
                $student_tc->section_id = $section_id;
                $student_tc->tc_number = Input::get('tc_number');
                $student_tc->print_tc_number = Input::get('print_tc_number');
                $student_tc->tc_application_date = get_formatted_date(Input::get('tc_application_date'), 'database');
                $student_tc->tc_date = get_formatted_date(Input::get('tc_date'), 'database');
                $student_tc->final_date = get_formatted_date(Input::get('final_date'), 'database');
                $student_tc->exam_result_remark = Input::get('exam_result');
                $student_tc->tc_reason = Input::get('tc_reason');
                $student_tc->student_conduct = Input::get('student_conduct');
                $student_tc->total_working_day = Input::get('total_working_day');
                $student_tc->total_attendance = Input::get('total_attendance');
                $student_tc->tc_reason = Input::get('tc_reason');
                $student_tc->tc_status = Input::get('tc_status');
                if ($student_tc->save()) {
                    if(Input::get('tc_status') == 1){
                        DB::table('students as s')->where('student_id', $student_tc->student_id)->update(['student_left' => 'Yes', 'app_accessbility_status' => 2]);
                    }else{
                        DB::table('students as s')->where('student_id', $student_tc->student_id)->update(['student_left' => 'No']);
                    }
                    $cron = new CronController;
                    $cron->pushNotificationStudentTC($student_tc);
                }
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                p($error_message);
                return redirect()->back()->withInput()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin/student-tc/')->withSuccess($success_msg);
    }

    public function anyData(Request $request)
    {
        $class_id = Input::get('class_id');
        $section_id = Input::get('section_id');
        $session_id = Input::get('session_id');
        if (empty($session_id)) {
            $session = get_current_session();
            $session_id = $session['session_id'];
        }
//        $table = get_student_table($session_id, 'table');
        $student = DB::table("students as s")
            ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
            ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
            ->join('student_tcs as st', 's.student_id', '=', 'st.student_id')
//            ->select('sp.father_contact_number', 's.admission_date', 'st.student_tc_id', 's.address_line1', 'st.tc_date', 'st.tc_number', DB::raw("(select student_promote_id from student_promotes where session_id='" . $session_id . "' AND class_id='" . $class_id . "' AND promote_status=1) as student_promote_id")
//                , 'st.tc_status', 'c.class_name', 's.previous_class_name', 's.previous_school_name', 's.join_date', 's.dob', 's.enrollment_number', DB::raw("CONCAT(sp.father_first_name,' ',sp.father_middle_name,' ',sp.father_last_name) as father_name"), DB::raw("CONCAT(s.first_name,' ',s.middle_name,' ',s.last_name) as student_name"), DB::raw('CASE WHEN st.tc_status = 1 THEN "Okay" ELSE "Cancel" END AS tc_status'))
            ->select('sp.father_contact_number', 's.admission_date', 'st.student_tc_id', 's.address_line1', 'st.tc_date', 'st.tc_number', DB::raw("(select student_promote_id from student_promotes where session_id='" . $session_id . "' AND class_id='" . $class_id . "' AND promote_status=1) as student_promote_id")
                , 'st.tc_status', 'c.class_name', 's.previous_class_name', 's.previous_school_name', 's.join_date', 's.dob', 's.enrollment_number', DB::raw("CONCAT(sp.father_first_name,' ',sp.father_last_name) as father_name"), DB::raw("CONCAT(s.first_name,' ',s.last_name) as student_name"), DB::raw('CASE WHEN st.tc_status = 1 THEN "Okay" ELSE "Cancel" END AS tc_status'))
            ->where(function ($q) use ($session_id, $class_id, $section_id) {
                if (!empty($session_id)) {
                    $q->where('s.current_session_id', $session_id);
                    $q->where('st.session_id', $session_id);
                }
                if (!empty($class_id)) {
                    $q->where('s.current_class_id', $class_id);
                    $q->where('st.class_id', $class_id);
                }
            })
            ->get();
        return Datatables::of($student)
            ->addColumn('action', function ($student) {
                $encrypted_student_tc_id = get_encrypted_value($student->student_tc_id, true);
                return '<a title="Edit" id="deletebtn1" href="' . url('admin/student-tc/add/' . $encrypted_student_tc_id) . '" class="btn btn-success centerAlign"><i class="fa fa-edit" ></i></a>';
            })
            ->addColumn('view_print', function ($student) {
                $disabled = '';
                $encrypted_student_tc_id = get_encrypted_value($student->student_tc_id, true);
                return '<a title="TC Print" href="' . url('admin/student-tc-print/' . $encrypted_student_tc_id) . '" class="btn btn-info prinr_r centerAlign"  target="_blank" style="text-align:center;" ' . $disabled . '><i class="fa fa-print" ></i></a>';
            })
            ->rawColumns(['checkbox', 'action', 'view_print'])->make(true);
    }

    public function studentTCPrint($student_tc_id)
    {
        $decrypted_student_tc_id = get_decrypted_value($student_tc_id, true);
        $student_tc = StudentTc::Find($decrypted_student_tc_id)->toArray();
        $data = $this->getStudentDetailsData($student_tc['session_id'], $student_tc['class_id'], $student_tc['section_id'], $student_tc['student_id'], $tc_print = 1);
        $student_tc = array_merge($student_tc, $data);
        if (!empty($student_tc['student_tc_id'])) {
            $data['tc_data'] = $student_tc;
//	    $pdf = View::make('backend.student-tc.tc-print')->with($data)->render();
//	    p($pdf);
            $pdf = PDF::loadView('backend.student-tc.tc-print', $data);
            return $pdf->stream('student-tc.pdf');
        }
    }

    function getStudentDetailsData($session_id, $class_id, $section_id, $student_id, $tc_print = 0)
    {
        $student_data = [];
        if (!empty($session_id) && !empty($class_id) && !empty($section_id) && !empty($student_id)) {
            $table = get_student_table($session_id, 'table');
            $student_data = DB::table($table)
                ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
                ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
                ->join('sessions as sec', 'sec.session_id', '=', 's.current_session_id')
                ->select('s.address_line1', DB::raw("(select student_promote_id from student_promotes where session_id='" . $session_id . "' AND class_id='" . $class_id . "' AND section_id='" . $section_id . "' AND promote_status=1) as student_promote_id")
                    ,'s.admission_class_name', 's.religion', 'c.class_name', 's.previous_class_name', 's.previous_school_name', 's.join_date','s.admission_date', 's.dob', 's.religion', 's.enrollment_number',
//                    DB::raw("CONCAT(sp.mother_first_name,' ',sp.mother_middle_name,' ',sp.mother_last_name) as mother_name"),
//                    DB::raw("CONCAT(sp.father_first_name,' ',sp.father_middle_name,' ',sp.father_last_name) as father_name"),
//                    DB::raw("CONCAT(s.first_name,' ',s.middle_name,' ',s.last_name) as student_name"))
                    'sp.mother_first_name','sp.mother_middle_name','sp.mother_last_name',
                    'sp.father_first_name','sp.father_middle_name','sp.father_last_name',
                    's.first_name','s.middle_name','s.last_name','s.previous_school_name','sec.session_year')
                ->where('s.current_session_id', $session_id)
                ->where('s.current_class_id', $class_id)
                ->where('s.current_section_id', $section_id)
                ->where('s.student_id', $student_id)
                ->where(function ($q) use ($tc_print) {
//                    if ($tc_print == 1) {
//                        $q->where('s.student_left', 'Yes');
//                    } else {
//                        $q->where('s.student_left', 'No');
//                    }
                })
                ->first();
            $student_data = (array)$student_data;

            $student_data['student_name'] = $student_data['first_name'].' '.$student_data['middle_name'].' '.$student_data['last_name'];
            $student_data['mother_name'] = $student_data['mother_first_name'].' '.$student_data['mother_middle_name'].' '.$student_data['mother_last_name'];
            $student_data['father_name'] = $student_data['father_first_name'].' '.$student_data['father_middle_name'].' '.$student_data['father_last_name'];

            if (!empty($student_data['join_date'])) {
                $student_data['join_date'] = get_formatted_date($student_data['join_date'], 'display');
            }
            if (!empty($student_data['admission_date'])) {
                $student_data['admission_date'] = get_formatted_date($student_data['admission_date'], 'display');
            }
            if (!empty($student_data['dob'])) {
                $student_data['dob'] = get_formatted_date($student_data['dob'], 'display');
            }
        }
        return $student_data;
    }

    function getStudentDetails(Request $request)
    {
        $class_id = Input::get('class_id');
        $section_id = Input::get('section_id');
        $session_id = Input::get('session_id');
        $student_id = Input::get('student_id');
        $status = 'failed';
        $student_data = $this->getStudentDetailsData($session_id, $class_id, $section_id, $student_id);
        if (!empty($student_data)) {
            $status = 'success';
        }
        return response()->json(['status' => $status, 'data' => $student_data]);
    }

    public function leftStudents(Request $request)
    {
        $data = array(
            'redirect_url' => url('admin/student-tc/add'),
            'page_title' => trans('language.list_student_tc'),
            'arr_class' => add_blank_option(get_all_classes(), '--Select Class--'),
        );
        return view('backend.student-tc.left-student')->with($data);
    }

    public function leftStudentData(Request $request)
    {
        $class_id = Input::get('class_id');
        $session_id = Input::get('session_id');
        if (empty($session_id)) {
            $session = get_current_session();
            $session_id = $session['session_id'];
        }
        $table = get_student_table($session_id, 'table');
        //        $table = get_student_table($session_id, 'table');
        $student = DB::table("students as s")
            ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
            ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
            ->select('s.student_id','sp.father_contact_number', 's.admission_date', 's.address_line1',DB::raw("(select student_promote_id from student_promotes where session_id='" . $session_id . "' AND class_id='" . $class_id . "' AND promote_status=1) as student_promote_id")
                , 'c.class_name', 's.previous_class_name', 's.previous_school_name', 's.join_date', 's.dob', 's.enrollment_number', DB::raw("CONCAT(sp.father_first_name,' ',sp.father_last_name) as father_name"), DB::raw("CONCAT(s.first_name,' ',s.last_name) as student_name"))
            ->where(function ($q) use ($session_id, $class_id) {
                if (!empty($session_id)) {
                    $q->where('s.current_session_id', $session_id);
                }
                if (!empty($class_id)) {
                    $q->where('s.current_class_id', $class_id);
                }
            })
            ->where('s.student_left',"Yes")
            ->get();
        foreach($students as $key => $student){
            $tc = StudentTc::where("student_id",$student->student_id)->get()->count();
            if($tc > 0){
                $students->forget($key);
            }
        }
        return Datatables::of($students)
            ->addColumn('revert', function ($student) {
                $encrypted_student_id = get_encrypted_value($student->student_id, true);
                return '<a title="Revert" href="' . route('student.left.recall',[$encrypted_student_id]) . '" class="btn btn-success centerAlign"><i class="fa fa-undo" ></i></a>';
            })
            ->rawColumns(['checkbox', 'revert'])->make(true);
    }

    public function recallLeftStudent($student_id){
        $decrypted_student_id = get_decrypted_value($student_id, true);
        $student = Student::findOrFail($decrypted_student_id);
        $student->student_left = "No";
        $student->app_accessbility_status = 1;
        $student->save();
        return redirect()->back()->withSuccess("Student Reverted Successfully.");
    }
}
    