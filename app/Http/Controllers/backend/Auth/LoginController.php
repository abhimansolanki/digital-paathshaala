<?php

    namespace App\Http\Controllers\backend\Auth;

    use App\Model\backend\AdminUser;
    use App\Model\backend\School;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Foundation\Auth\AuthenticatesUsers;
    use Illuminate\Support\Facades\Auth;
    //use Illuminate\Support\Facades\Session;
    use Illuminate\Support\Facades\Redirect;
    use Session as LSession;

    class LoginController extends Controller
    {
        /*
          |--------------------------------------------------------------------------
          | Login Controller
          |--------------------------------------------------------------------------
          |
          | This controller handles authenticating users for the application and
          | redirecting them to your home screen. The controller uses a trait
          | to conveniently provide its functionality to your applications.
          |
         */

use AuthenticatesUsers;

        /**
         * Where to redirect users after login.
         *
         * @var string
         */
        protected $redirectTo = '/admin/dashboard';

        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {
            $this->middleware('guest', ['except' => 'logout']);
        }

        public function getLoginForm()
        {
            $data = array(
                'page_title' => 'Admin Login'
            );

            return view('admin_panel.login')->with($data);
        }

        protected function guard()
        {
            return Auth::guard('admin');
        }

        public function getLogout()
        {
            LSession::forget('set_session_year');
            auth()->guard('admin')->logout();
            return redirect()->intended('admin/login');
        }

        public function privacyPolicy()
        {
            $school = School::first();
            return view("admin_panel.privacy_policy", compact("school"));
        }

    }
    