<?php

    namespace App\Http\Controllers\backend\Auth;

    use App\Http\Controllers\Controller;
    use App\Model\backend\AdminUser;
    use Illuminate\Foundation\Auth\ResetsPasswords;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Password;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;
    use Validator;

    class ResetPasswordController extends Controller
    {
        /*
          |--------------------------------------------------------------------------
          | Password Reset Controller
          |--------------------------------------------------------------------------
          |
          | This controller is responsible for handling password reset requests
          | and uses a simple trait to include this behavior. You're free to
          | explore this trait and override any methods you wish to tweak.
          |
         */

use ResetsPasswords;

        protected $redirectTo = 'admin/dashboard';

        /**
         * Create a new controller instance.
         *
         * @return void
         */
//        public $redirectTo = '/admin/dashboard';

        public function __construct()
        {
            
        }

        public function showChangePasswordForm(Request $request, $token = null)
        {
            return view('admin_panel.change-password');
        }

        public function showResetForm(Request $request, $token = null, $email = null)
        {
            return view('admin_panel.reset-password')->with(
                    ['token' => $token, 'email' => $email]
            );
        }

        /**
         * Get the broker to be used during password reset.
         *
         * @return \Illuminate\Contracts\Auth\PasswordBroker
         */
        public function broker()
        {
            return Password::broker();
        }

        /**
         * Get the guard to be used during password reset.
         *
         * @return \Illuminate\Contracts\Auth\StatefulGuard
         */
        protected function guard()
        {
            return Auth::guard('admin');
        }

        public function changePassword(Request $request)
        {

            if (!(Hash::check($request->get('password'), Auth::guard('admin')->user()->password)))
            {
                // The passwords matches
                return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
            }

            if (strcmp($request->get('password'), $request->get('password_confirm')) == 0)
            {
                //Current password and new password are same
                return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
            }

            $validatior = Validator::make($request->all(), [
                    'password'         => 'required',
                    'password_new'     => 'required|min:6',
                    'password_confirm' => 'required_with:password_new|same:password_new|min:6'
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                //Change Password
                $user           = Auth::guard('admin')->user();
                $user->password = bcrypt($request->get('password_confirm'));
                $user->save();
                return redirect()->back()->with("success", "Password changed successfully !");
            }
        }

    }
    