<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\WorkDiary;
use Validator;
use Illuminate\Support\Facades\Input;
use Datatables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\CronController;

class WorkDiaryController extends Controller
{

    public function __construct()
    {

    }

    public function index()
    {
        $data = array(
            'redirect_url' => url('admin/work-diary/add'),
        );
        return view('backend.work-diary.index')->with($data);
    }

    public function add(Request $request, $id = NULL)
    {
        $work_diary = [];
        $work_diary_id = null;
        $class_id = null;
        $session_id = null;
        $session = db_current_session();
        if (!empty($id)) {
            $decrypted_work_diary_id = get_decrypted_value($id, true);
            $work_diary = $this->getWorkDiaryData($decrypted_work_diary_id);
//            p($work_diary);
            $work_diary = isset($work_diary[0]) ? $work_diary[0] : [];
            $class_id = $work_diary['class_id'];
            $session_id = $work_diary['session_id'];
            $section_id = $work_diary['section_id'];
            if (!$work_diary) {
                return redirect('admin/work-diary')->withError('Work Diary not found!');
            }
            $encrypted_work_diary_id = get_encrypted_value($work_diary['work_diary_id'], true);
            $save_url = url('admin/work-diary/save/' . $encrypted_work_diary_id);
            $submit_button = 'Update';
            $work_diary_id = $decrypted_work_diary_id;
            $arr_student = get_student_list_by_class_section($session_id, $class_id, $section_id);
        } else {
            $save_url = url('admin/work-diary/save');
            $submit_button = 'Save';
            $arr_student = [];
            $session_id = $session['session_id'];
        }
        $work_diary['arr_class_subject'] = add_blank_option(get_class_subject($class_id, $session_id), '-- Select--');
        $work_diary['arr_work_type'] = \Config::get('custom.arr_work_type');
        $work_diary['arr_class'] = add_blank_option(get_all_classes(), '--Select--');
        $work_diary['arr_section'] = add_blank_option(get_class_section($class_id, $session_id), '--Select section --');
        $work_diary['arr_student'] = add_blank_option($arr_student, '--Select Student--');

        $work_diary['session_start_date'] = $session['session_start_date'];
        $work_diary['session_end_date'] = $session['session_end_date'];

        $data = array(
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'work_diary' => $work_diary,
            'redirect_url' => url('admin/work-diary/'),
            'session_id' => $session_id
        );
        return view('backend.work-diary.add')->with($data);
    }

    public function save(Request $request, $id = NULL)
    {
        $decrypted_work_diary_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $work_diary = WorkDiary::find($decrypted_work_diary_id);

            if (!$work_diary) {
                return redirect('/admin/work-diary/')->withError('WorkDiary not found!');
            }
            $success_msg = 'Work Diary updated successfully!';
        } else {
            $work_diary = new WorkDiary;
            $success_msg = 'Work Diary saved successfully!';
        }

        $class_id = Input::get('class_id');
        $work_type = Input::get('work_type');
        $section_id = Input::get('section_id');
        $work_start_date = get_formatted_date(Input::get('work_start_date'), 'database');
        $work_end_date = get_formatted_date(Input::get('work_end_date'), 'database');
        $session = db_current_session();
        $session_id = $session['session_id'];
        $arr_input_fields = [
            'class_id' => 'required',
            'work' => 'required',
            'work_type' => 'required',
            'work_start_date' => 'required',
            'work_end_date' => 'required',
        ];
//            if (!empty(Input::get('section_id')))
//            {
//                $arr_input_fields['subject_id'] = 'required|unique:work_diaries,subject_id,' . $decrypted_work_diary_id . ',work_diary_id,class_id,' . $class_id . ',session_id,' . $session_id . ',work_type,' . $work_type . ',section_id,' . $section_id.',work_start_date,'.$work_start_date.',work_end_date'.$work_end_date;
//            }
//            else
//            {
//                $arr_input_fields['subject_id'] = 'required|unique:work_diaries,subject_id,' . $decrypted_work_diary_id . ',work_diary_id,class_id,' . $class_id . ',session_id,' . $session_id . ',work_type,' . $work_type;
//            }

        $validatior = Validator::make($request->all(), $arr_input_fields);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction(); //Start transaction!
            try {
                $work_diary->class_id = Input::get('class_id');
                $work_diary->subject_id = Input::get('subject_id');
                $work_diary->section_id = Input::get('section_id');
                $work_diary->session_id = $session_id;
                $work_diary->work_type = Input::get('work_type');
                $work_diary->work = Input::get('work');
                $work_diary->student_id = Input::get('student_id');
                $work_diary->work_start_date = get_formatted_date(Input::get('work_start_date'), 'database');
                $work_diary->work_end_date = get_formatted_date(Input::get('work_end_date'), 'database');
                // if ($request->hasFile('work_doc'))
                // {
                //     $file                 = $request->file('work_doc');
                //     $config_upload_path   = \Config::get('custom.work_doc');
                //     $destinationPath      = public_path() . $config_upload_path['upload_path'];
                //     $filename             = $file->getClientOriginalName();
                //     $rand_string          = quick_random();
                //     $filename             = $rand_string . '-' . $filename;
                //     $file->move($destinationPath, $filename);
                //     $work_diary->work_doc = $filename;
                // }

                $work_diary->work_doc_file_title = $request->work_doc_image_title;
                if ($request->hasFile('work_doc_image')) {
                    $file = $request->file('work_doc_image');
                    $config_upload_path = \Config::get('custom.work_doc');
                    $destinationPath = public_path() . $config_upload_path['upload_path'];
                    $filename = $file->getClientOriginalName();
                    $rand_string = quick_random();
                    $filename = $rand_string . '-' . $filename;
                    $file->move($destinationPath, $filename);
                    $work_diary->work_doc_file = $filename;
                }
                $work_diary->work_doc_file_download_permission = ($request->download_permission == 'on') ? 1 : 2;
                if ($request->check_for_video == 1) {
                    $work_diary->work_doc_video_title = $request->work_doc_video_title;
                    if ($request->has('work_doc_video')) {
                        $file = $request->file('work_doc_video');
                        $config_upload_path = \Config::get('custom.work_doc');
                        $destinationPath = public_path() . $config_upload_path['upload_path'];
                        $filename = $file->getClientOriginalName();
                        $rand_string = quick_random();
                        $filename = $rand_string . '-' . $filename;
                        $file->move($destinationPath, $filename);
                        $work_diary->work_doc_video = $filename;
                    }

                    $work_diary->work_doc_link_title = NULL;
                    $work_diary->work_doc_link = NULL;
                }
                if ($request->check_for_video == 2) {
                    $work_diary->work_doc_link_title = $request->work_doc_link_title;
                    $work_diary->work_doc_link = $request->work_doc_link;

                    $work_diary->work_doc_video_title = NULL;
                    $work_diary->work_doc_video = NULL;
                }
                $work_diary->save();
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
            /**
             * Send notification for work diary
             *
             */
            if (!empty($work_diary) && $id == null) {
                $cron = new CronController;
                $cron->pushNotificationWorkDiary($work_diary);
            }
        }
        return redirect('admin/work-diary')->withSuccess($success_msg);
    }

    public function destroy(Request $request)
    {
        $work_diary_id = Input::get('work_diary_id');
        $work_diary = WorkDiary::find($work_diary_id);
        if ($work_diary) {

            DB::beginTransaction(); //Start transaction!
            try {
                $work_diary->delete();
                $return_arr = array(
                    'status' => 'success',
                    'message' => 'Work Diary deleted successfully!'
                );
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                $return_arr = array(
                    'status' => 'used',
                    'message' => trans('language.delete_message')
                );
            }
            DB::commit();
        } else {
            $return_arr = array(
                'status' => 'error',
                'message' => 'Work Diary not found!'
            );
        }
        return response()->json($return_arr);
    }

    public function anyData()
    {
        $work_diary = [];
        $work_diary_id = null;
        $offset = Input::get('start');
        $limit = Input::get('length');
        $search_start_date = get_formatted_date(Input::get('search_start_date'), 'database');
        $arr_work_diary = $this->getWorkDiaryData($work_diary_id, $offset, $limit, $search_start_date);
        foreach ($arr_work_diary as $key => $work_diary_data) {
            $work_diary[$key] = (object)$work_diary_data;
        }
        return Datatables::of($work_diary)
            ->addColumn('action', function ($work_diary) {
                $encrypted_work_diary_id = get_encrypted_value($work_diary->work_diary_id, true);
                return '<a title="Edit" id="deletebtn1" href="work-diary/add/' . $encrypted_work_diary_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                    . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $work_diary->work_diary_id . '"><i class="fa fa-trash"></i></button>';
            })
            ->rawColumns(['action'])->make(true);
    }

    public function getWorkDiaryData($work_diary_id = null, $offset = null, $limit = null, $search_start_date = null)
    {
        $work_diary_return = [];
        $session_id = null;
        $session = get_current_session();
        if (!empty($session)) {
            $session_id = $session['session_id'];
        }
        $arr_work_diary_data = WorkDiary::where('session_id', $session_id)->where(function ($query) use ($work_diary_id, $search_start_date) {
            if (!empty($work_diary_id)) {
                $query->where('work_diary_id', $work_diary_id);
            }
            if(!empty($search_start_date)){
                $query->where('work_start_date', $search_start_date);
            }
        })->with(['workClass' => function ($q) {
            $q->addSelect(['class_id', 'class_name']);
            $q->where('class_status', 1);
        }])
            ->with(['workSubject' => function ($q) {
                $q->addSelect(['subject_id', 'subject_name']);
                $q->where('subject_status', 1);
            }])
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
            ->orderBy("work_start_date","desc")
            ->get();
        if (!empty($arr_work_diary_data)) {
            $work_type = \Config::get('custom.arr_work_type');
            $work_diary = [];
            foreach ($arr_work_diary_data as $key => $work_diary_data) {
                $work_diary = array(
                    'work_diary_id' => $work_diary_data['work_diary_id'],
                    'session_id' => $work_diary_data['session_id'],
                    'class_id' => $work_diary_data['class_id'],
                    'section_id' => $work_diary_data['section_id'],
                    'class_name' => $work_diary_data['workClass']['class_name'],
                    'subject_id' => $work_diary_data['subject_id'],
                    'subject_name' => $work_diary_data['workSubject']['subject_name'],
                    'work_type' => $work_diary_data['work_type'],
                    'work_type_name' => $work_type[$work_diary_data['work_type']],
                    'work' => $work_diary_data['work'],
                    'student_id' => $work_diary_data['student_id'],
                    'work_start_date' => get_formatted_date($work_diary_data['work_start_date'], 'display'),
                    'work_end_date' => get_formatted_date($work_diary_data['work_end_date'], 'display'),
                    //'work_doc'        => check_file_exist($work_diary_data['work_doc'], 'work_doc'),
                );
                $work_diary['download_permission'] = $work_diary_data['work_doc_file_download_permission'];
                $work_diary['work_doc_file'] = [];
                if ($work_diary_data['work_doc_file_title'] != "") {
                    $work_diary['work_doc_file'] = array(
                        'title' => $work_diary_data['work_doc_file_title'],
                        'doc' => check_file_exist($work_diary_data['work_doc_file'], 'work_doc'),
                    );
                }
                $work_diary['work_doc_video'] = [];
                if ($work_diary_data['work_doc_video_title'] != "") {
                    $work_diary['work_doc_video'] = array(
                        'title' => $work_diary_data['work_doc_video_title'],
                        'doc' => check_file_exist($work_diary_data['work_doc_video'], 'work_doc'),
                    );
                }
                $work_diary['work_doc_link'] = [];
                if ($work_diary_data['work_doc_link_title'] != "") {
                    $work_diary['work_doc_link'] = array(
                        'title' => $work_diary_data['work_doc_link_title'],
                        'doc' => $work_diary_data['work_doc_link'],
                    );
                }
                $work_diary_return[] = $work_diary;
            }
        }
//        p($work_diary_return);
        return $work_diary_return;
    }

    function getClassSubject($class_id, $section_id)
    {
        $work_diary['subject'] = [];
        $status = 'failed';
        if (!empty($class_id)) {
            $session = db_current_session();
            $session_id = $session['session_id'];
            $work_diary['subject'] = get_class_section_subject($session_id, $class_id, $section_id);
            $status = 'success';
        }
        return response()->json(array('status' => $status, 'data' => $work_diary));
    }

}
    