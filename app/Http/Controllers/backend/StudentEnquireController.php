<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\StudentEnquire;
use Validator;
use Illuminate\Support\Facades\Input;
use Datatables;
use Illuminate\Support\Facades\DB;
use PDF;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class StudentEnquireController extends Controller
{

    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array(
            'redirect_url' => url('admin/student-enquire/add'),
            'page_title' => trans('language.list_school'),
        );
        return view('backend.student-enquire.index')->with($data);
    }

    public function add(Request $request, $id = null)
    {
        $student_enquire = [];
        if (!empty($id)) {
            $decrypted_student_enquire_id = get_decrypted_value($id, true);
            $student_enquire = $this->getEnquireData($decrypted_student_enquire_id);
            $student_enquire = isset($student_enquire[0]) ? $student_enquire[0] : array();

            if (!$student_enquire) {
                return redirect('admin/student')->withError('Student Enquiry not found!');
            }
            $encrypted_student_enquire_id = get_encrypted_value($student_enquire['student_enquire_id'], true);
            $page_title = 'Edit Student Enquiry';
            $save_url = url('admin/student-enquire/save/' . $encrypted_student_enquire_id);
            $submit_button = 'Update';
        } else {
            $page_title = 'Create Student Enquiry';
            $save_url = url('admin/student-enquire/save');
            $submit_button = 'Save';

            // set enquire form number
            $last_form_number = 1;
            $form_number_data = StudentEnquire::orderBy('student_enquire_id', 'desc')->first();
            if (!empty($form_number_data->student_enquire_id)) {
                $form_number = $form_number_data->student_enquire_id;
                $last_form_number = $form_number + 1;
            }
            $student_enquire['form_number'] = $last_form_number;
            $student_enquire['form_fee_paid'] = 0;
        }
        $current_session = db_current_session();
        $school = DB::table('schools')->select('form_fee')->first();
        $arr_gender = \Config::get('custom.student_gender');
        $arr_caste_category = get_caste_category();
        $student_enquire['arr_class'] = add_blank_option(get_all_classes(), '--Select class --');
        $student_enquire['arr_gender'] = add_blank_option($arr_gender, '-- Select gender --');
        $student_enquire['arr_caste_category'] = add_blank_option($arr_caste_category, '--Select category --');
        $student_enquire['arr_session'] = get_session('current');
        $student_enquire['start_date'] = get_formatted_date($current_session['start_date'], 'display');
        $student_enquire['end_date'] = get_formatted_date($current_session['end_date'], 'display');
        $student_enquire['form_fee'] = 0;

        if (!empty($school)) {
            $form_fee = $school->form_fee; //\Config::get('custom.form_fee');
            $student_enquire['form_fee'] = $form_fee;
        }
        $data = array(
            'page_title' => $page_title,
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'student_enquire' => $student_enquire,
            'redirect_url' => url('admin/student-enquire/'),
        );
        return view('backend.student-enquire.add')->with($data);
    }

    public function save(Request $request, $id = null)
    {
        $decrypted_student_enquire_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $student_enquire = StudentEnquire::find($decrypted_student_enquire_id);

            if (!$student_enquire) {
                return redirect('/admin/student-enquire/')->withError('Student Enquire not found!');
            }
            $success_msg = 'Student Enquiry updated successfully!';
        } else {
            $student_enquire = new StudentEnquire;
            $success_msg = 'Student Enquiry saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
            'form_number' => 'required|unique:student_enquires,form_number,' . $decrypted_student_enquire_id . ',student_enquire_id',
            'first_name' => 'required',
            'enquire_date' => 'required',
            'session_id' => 'required',
//                    'last_name'             => 'required',
            'gender' => 'required',
            'caste_category_id' => 'required',
            'father_first_name' => 'required',
//                    'father_occupation'     => 'required',
            'father_contact_number' => 'required',
            'mother_first_name' => 'required',
            'mother_contact_number' => 'required',
//                    'mother_occupation'     => 'required',
            'address_line1' => 'required',
            'class_id' => 'required',
        ]);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction(); //Start transaction!
            try {


                $enquire_date = get_formatted_date(Input::get('enquire_date'), 'database');
                $student_enquire->form_number = Input::get('form_number');
                $student_enquire->class_id = Input::get('class_id');
                $student_enquire->first_name = Input::get('first_name');
                $student_enquire->middle_name = Input::get('middle_name');
                $student_enquire->last_name = Input::get('last_name');
                if (!empty(Input::get('dob'))) {
                    $dob = get_formatted_date(Input::get('dob'), 'database');
                    $student_enquire->dob = $dob;
                }
                $form_fee_paid = Input::get('form_fee_paid');
                $student_enquire->gender = Input::get('gender');
                $student_enquire->caste_category_id = Input::get('caste_category_id');
                $student_enquire->enquire_date = $enquire_date;
                $student_enquire->session_id = Input::get('session_id');
                $student_enquire->form_fee = Input::get('form_fee');
                $student_enquire->form_fee_paid = $form_fee_paid;
                $student_enquire->aadhaar_number = Input::get('aadhaar_number');
                $student_enquire->previous_school_name = Input::get('previous_school_name');
                $student_enquire->previous_class_name = Input::get('previous_class_name');
                $student_enquire->body_sign = Input::get('body_sign');
                $student_enquire->address_line1 = Input::get('address_line1');
                $student_enquire->address_line2 = Input::get('address_line2');
                $student_enquire->percentage = Input::get('percentage');
                $student_enquire->remark = Input::get('remark');
                $student_enquire->birth_place = Input::get('birth_place');

                $student_enquire->father_income = Input::get('father_income');
                $student_enquire->father_email = Input::get('father_email');
                $student_enquire->father_aadhaar_number = Input::get('father_aadhaar_number');
                $student_enquire->father_first_name = Input::get('father_first_name');
                $student_enquire->father_middle_name = Input::get('father_middle_name');
                $student_enquire->father_last_name = Input::get('father_last_name');
                $student_enquire->father_occupation = Input::get('father_occupation');
                $student_enquire->father_contact_number = Input::get('father_contact_number');

                $student_enquire->mother_email = Input::get('mother_email');
                $student_enquire->mother_aadhaar_number = Input::get('mother_aadhaar_number');
                $student_enquire->mother_first_name = Input::get('mother_first_name');
                $student_enquire->mother_middle_name = Input::get('mother_middle_name');
                $student_enquire->mother_last_name = Input::get('mother_last_name');
                $student_enquire->mother_occupation = Input::get('mother_occupation');
                $student_enquire->mother_contact_number = Input::get('mother_contact_number');

                $student_enquire->guardian_email = Input::get('guardian_email');
                $student_enquire->guardian_aadhaar_number = Input::get('guardian_aadhaar_number');
                $student_enquire->guardian_first_name = Input::get('guardian_first_name');
                $student_enquire->guardian_middle_name = Input::get('guardian_middle_name');
                $student_enquire->guardian_last_name = Input::get('guardian_last_name');
                $student_enquire->guardian_occupation = Input::get('guardian_occupation');
                $student_enquire->guardian_contact_number = Input::get('guardian_contact_number');
                $student_enquire->guardian_relation = Input::get('guardian_relation');
//                   $return =  $student_enquire->save();
//                   p($return);
                if ($student_enquire->save()) {
                    // send mail
                    if (empty($id)) {
                        $email = '';
                        if (!empty(Input::get('father_email'))) {
                            $email = Input::get('father_email');
                            $data['cc_address'] = Input::get('mother_email');
                        } else {
                            $email = Input::get('mother_email');
                            $data['cc_address'] = Input::get('father_email');
                        }
                        if (!empty($email)) {
                            $data['view_blade'] = 'enquire-mail';
                            $data['cc_name'] = Input::get('mother_first_name');
                            $data['subject'] = trans('language.enquire_subject');
                            $data['body'] = array(
                                'message' => trans('language.enquire_mail_message'),
                                'form_number' => $student_enquire->form_number,
                                'enquire_date' => $student_enquire->enquire_date,
                            );
//                            $mail_response = Mail::to($email)->send(new SendMailable($data));
                        }

                        // send sms to parents
                        $mobile_number = [];
                        $father_no = Input::get('father_contact_number');
                        if (!empty($father_no)) {
                            array_push($mobile_number, $father_no);
                        }
                        $mother_no = Input::get('mother_contact_number');
                        if (!empty($mother_no)) {
                            array_push($mobile_number, $mother_no);
                        }
                        $guardian_no = Input::get('guardian_contact_number');
                        if (!empty($guardian_no)) {
                            array_push($mobile_number, $guardian_no);
                        }

                        if (!empty($mobile_number)) {
                            $sms_mobile_number = implode(',',$mobile_number);
                            $message_text = trans('language.enquire_message');
                            $response = send_sms($sms_mobile_number, $message_text);
                        }
                    }
                }
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withInput()->withErrors($error_message);
            }
            DB::commit();
        }
        if ($form_fee_paid == 1 && empty($id)) {
            $enquire_id = get_encrypted_value($student_enquire->student_enquire_id, true);
            return redirect('admin/student-enquire/view-print/' . $enquire_id);
        } else {
            return redirect('admin/student-enquire/add')->withSuccess($success_msg);
        }
    }

    public function destroy(Request $request)
    {
        $student_enquire_id = Input::get('student_enquire_id');
        $student_enquire = StudentEnquire::find($student_enquire_id);
        if ($student_enquire) {
            $student_enquire->delete();
            $return_arr = array(
                'status' => 'success',
                'message' => 'Student Enquiry deleted successfully!'
            );
        } else {
            $return_arr = array(
                'status' => 'error',
                'message' => 'Student Enquiry not found!'
            );
        }
        return response()->json($return_arr);
    }

    public function anyData()
    {
        $student_enquire = [];
        $offset = Input::get('start');
        $limit = Input::get('length');
        $student_enquire_id = [];
        $arr_student_enquire = $this->getEnquireData($student_enquire_id, $offset, $limit);
//        p($arr_student_enquire);
        foreach ($arr_student_enquire as $key => $enquire_data) {
            $student_enquire[$key] = (object)$enquire_data;
        }
        return Datatables::of($student_enquire)
            ->addColumn('action', function ($student_enquire) {
                $encrypted_student_enquire_id = get_encrypted_value($student_enquire->student_enquire_id, true);
                return '<a title="Edit" id="deletebtn1" href="' . url('admin/student-enquire/add/' . $encrypted_student_enquire_id) . '" class="btn btn-success centerAlign"><i class="fa fa-edit" ></i></a>';
            })
            ->addColumn('view_print', function ($student_enquire) {
                $disabled = '';
                if ($student_enquire->form_fee_paid == 0) {
                    $disabled = 'disabled="disabled"';
                }
                $encrypted_student_enquire_id = get_encrypted_value($student_enquire->student_enquire_id, true);
                return '<a title="View & Print" href="student-enquire/view-print/' . $encrypted_student_enquire_id . '" class="btn btn-info prinr_r centerAlign"  target="_blank" style="text-align:center;" ' . $disabled . '><i class="fa fa-print" ></i></a>';
            })
            ->addColumn('admission', function ($student_enquire) {
                $encrypted_student_enquire_id = get_encrypted_value($student_enquire->student_enquire_id, true);
                return '<a title="Convert into Admission" id="deletebtn1" href="' . url('admin/student/add/' . $encrypted_student_enquire_id . '/enquiry') . '" class="btn btn-success centerAlign"><i class="glyphicon glyphicon-export" ></i></a>';
            })
            ->rawColumns(['checkbox', 'action', 'view_print', 'admission'])->make(true);
    }

    public function checkParentContact(Request $request)
    {
        $father_number = Input::get('father_contact_number');
        $mother_number = Input::get('mother_contact_number');
        $student_enquire_id = Input::get('student_enquire_id');
        if ((!empty($father_number) && $request->has('father_contact_number')) || (!empty($mother_number) && $request->has('mother_contact_number'))) {
            $enquire_contact = StudentEnquire::where('status', 0)
                ->where(function ($query) use ($father_number, $mother_number, $student_enquire_id) {
                    if (!empty($father_number)) {
                        $query->where('father_contact_number', $father_number);
                    } elseif (!empty($mother_number)) {
                        $query->where('mother_contact_number', $mother_number);
                    }
                    if (!empty($student_enquire_id)) {
                        $query->where('student_enquire_id', '!=', $student_enquire_id);
                    }
                })
                ->first();
            if (!empty($enquire_contact)) {
                return response()->json(false);
            }
        }
        return response()->json(true);
    }

    public function enquireData(Request $request)
    {
        $form_number = Input::get('form_number');
        $student_enquire = [];
        $message = "Form number doesn't match with any record";
        $status = 'failed';
        if (!empty($form_number)) {
            $session = get_current_session();
            $enquire_data = StudentEnquire::where(['status' => 0, 'form_number' => $form_number])
                ->where(function ($query) use ($session) {
                    if (!empty($session['start_date']) && !empty($session['end_date'])) {
                        $query->whereBetween('enquire_date', array($session['start_date'], $session['end_date']));
                    }
                })->first();
            if (!empty($enquire_data)) {
                $enquire_data['dob'] = get_formatted_date($enquire_data['dob'], 'display');
                $enquire_data['start_date'] = get_formatted_date($session['start_date'], 'display');
                $enquire_data['end_date'] = get_formatted_date($session['end_date'], 'display');
                $enquire_data['enrollment_number'] = create_scholar_number($enquire_data['class_id']);
                if (!empty($enquire_data['student_enquire_id'])) {
                    $student_enquire = $enquire_data;
                    $message = 'Enquiry found and applied successfully.';
                    $status = 'success';
                }
            } else {
                $student_enquire = $enquire_data;
                $message = 'Enquiry not found.';
                $status = 'error';
            }
        }
        return response()->json($result = array('status' => $status, 'message' => $message, 'data' => $student_enquire));
    }

    public function viewPrint(Request $request, $id = null)
    {
        $form_number = '';
        $student_enquire['arr_enquire_data'] = [];
        $decrypted_student_enquire_id = get_decrypted_value($id, true);
        $student_enquire['arr_enquire_data'] = $this->getEnquireData($decrypted_student_enquire_id);

        if (!empty($student_enquire['arr_enquire_data'])) {
            $form_number = '-' . $student_enquire['arr_enquire_data'][0]['form_number'];
        }
        $pdf = PDF::loadView('backend.student-enquire.enquire_form_print', $student_enquire);
        return $pdf->stream('enquire-form' . $form_number . '.pdf');
    }

    public function getEnquireData($student_enquire_id = array(), $offset = null, $limit = null)
    {
        $arr_student_enquire_id = $student_enquire_id;
        if (!is_array($arr_student_enquire_id)) {
            $arr_student_enquire_id = array($arr_student_enquire_id);
        }
        $student_enquire_return = [];
        $session = get_current_session();
        $arr_enquire_data = StudentEnquire::where('status', 0)->where(function ($query) use ($arr_student_enquire_id) {
            if (!empty($arr_student_enquire_id)) {
                foreach ($arr_student_enquire_id as $student_enquire_id) {
                    $query->where('student_enquire_id', $student_enquire_id);
                }
            }
        })->where(function ($query) use ($session) {
            if (!empty($session['start_date']) && !empty($session['end_date'])) {
                $query->whereBetween('enquire_date', array($session['start_date'], $session['end_date']));
            }
        })->with('studentEnquireClass')
            ->with(['getCaste' => function ($q) {
                $q->addSelect(['caste_category_id', 'caste_name']);
            }])
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
            ->get();

        foreach ($arr_enquire_data as $key => $enquire_data) {
            $student_enquire_return[] = array(
                'student_enquire_id' => $enquire_data['student_enquire_id'],
                'caste_category_id' => $enquire_data['caste_category_id'],
                'class_id' => $enquire_data['class_id'],
                'class_name' => $enquire_data['studentEnquireClass']['class_name'],
                'caste_name' => $enquire_data['getCaste']['caste_name'],
                'session_id' => $enquire_data['session_id'],
                'enquire_date' => get_formatted_date($enquire_data['enquire_date'], 'display'),
                'dob' => get_formatted_date($enquire_data['dob'], 'display'),
                'gender_name' => get_gender($enquire_data['gender'], 'student_gender'),
                'form_fee' => $enquire_data['form_fee'],
                'form_fee_paid' => $enquire_data['form_fee_paid'],
                'form_number' => $enquire_data['form_number'],
                'first_name' => $enquire_data['first_name'],
                'middle_name' => $enquire_data['middle_name'],
                'last_name' => $enquire_data['last_name'],
                'gender' => $enquire_data['gender'],
                'birth_place' => $enquire_data['birth_place'],
                'father_income' => $enquire_data['father_income'],
                'father_email' => $enquire_data['father_email'],
                'father_aadhaar_number' => $enquire_data['father_aadhaar_number'],
                'father_first_name' => $enquire_data['father_first_name'],
                'father_middle_name' => $enquire_data['father_middle_name'],
                'father_last_name' => $enquire_data['father_last_name'],
                'father_contact_number' => $enquire_data['father_contact_number'],
                'father_occupation' => $enquire_data['father_occupation'],
                'mother_email' => $enquire_data['mother_email'],
                'mother_aadhaar_number' => $enquire_data['mother_aadhaar_number'],
                'mother_first_name' => $enquire_data['mother_first_name'],
                'mother_middle_name' => $enquire_data['mother_middle_name'],
                'mother_last_name' => $enquire_data['mother_last_name'],
                'mother_contact_number' => $enquire_data['mother_contact_number'],
                'mother_occupation' => $enquire_data['mother_occupation'],
                'guardian_email' => $enquire_data['guardian_email'],
                'guardian_aadhaar_number' => $enquire_data['guardian_aadhaar_number'],
                'guardian_first_name' => $enquire_data['guardian_first_name'],
                'guardian_middle_name' => $enquire_data['guardian_middle_name'],
                'guardian_last_name' => $enquire_data['guardian_last_name'],
                'guardian_contact_number' => $enquire_data['guardian_contact_number'],
                'guardian_occupation' => $enquire_data['guardian_occupation'],
                'guardian_relation' => $enquire_data['guardian_relation'],
                'aadhaar_number' => $enquire_data['aadhaar_number'],
                'address_line1' => $enquire_data['address_line1'],
                'address_line2' => $enquire_data['address_line2'],
                'previous_class_name' => $enquire_data['previous_class_name'],
                'previous_school_name' => $enquire_data['previous_school_name'],
                'percentage' => $enquire_data['percentage'],
                'remark' => $enquire_data['remark'],
                'student_name' => $enquire_data['first_name'] . ' ' . $enquire_data['middle_name'] . ' ' . $enquire_data['last_name'],
                'father_name' => $enquire_data['father_first_name'] . ' ' . $enquire_data['father_middle_name'] . ' ' . $enquire_data['father_last_name'],
                'mother_name' => $enquire_data['mother_first_name'] . ' ' . $enquire_data['mother_middle_name'] . ' ' . $enquire_data['mother_last_name'],
                'guardian_name' => $enquire_data['guardian_first_name'] . ' ' . $enquire_data['guardian_middle_name'] . ' ' . $enquire_data['guardian_last_name'],
                'address' => $enquire_data['address_line1'],
                'address' => $enquire_data['address_line1'],
            );
        }
        return $student_enquire_return;
    }

    public function studentEnquireReport()
    {
        $data = array(
            'arr_class' => add_blank_option(get_all_classes(), '-- Select class --'),
            'arr_enquire_type' => \Config::get('custom.enquire_type'),
        );
        return view('backend.report.student-enquire-report')->with($data);
    }

    public function studentEnquireReportData()
    {
        $arr_enquire_student = [];
        $class_id = Input::get('class_id');
        $session_id = Input::get('session_id');
        $enquire_type = Input::get('enquire_type');
        $session = get_current_session();
        if (!empty($session) || !empty($session_id)) {
            $arr_enquire_data = StudentEnquire::where(function ($query) use ($enquire_type) {
                if ($enquire_type == 1) {
                    $query->where('status', 1);
                } elseif ($enquire_type == 2) {
                    $query->where('status', 0);
                }
            })
                ->where(function ($query) use ($class_id) {
                    if (!empty($class_id)) {
                        $query->where('class_id', $class_id);
                    }
                })
                ->where(function ($query) use ($session_id, $session) {
                    if (!empty($session_id)) {
                        $query->where('session_id', $session_id);
                    } else {
                        $query->whereBetween('enquire_date', array($session['start_date'], $session['end_date']));
                    }
                })
                ->with('studentEnquireClass')->get();
            $count = 1;
            foreach ($arr_enquire_data as $key => $enquire_data) {
                $student_enquire = array(
                    'student_enquire_id' => $enquire_data['student_enquire_id'],
                    's_no' => $count,
                    'class_name' => $enquire_data['studentEnquireClass']['class_name'],
                    'form_number' => $enquire_data['form_number'],
                    'enquire_date' => get_formatted_date($enquire_data['enquire_date'], 'display'),
                    'dob' => get_formatted_date($enquire_data['dob'], 'display'),
                    'gender_name' => get_gender($enquire_data['gender'], 'student_gender'),
                    'father_contact_number' => $enquire_data['father_contact_number'],
                    'student_name' => $enquire_data['first_name'] . ' ' . $enquire_data['middle_name'] . ' ' . $enquire_data['last_name'],
                    'father_name' => $enquire_data['father_first_name'] . ' ' . $enquire_data['father_middle_name'] . ' ' . $enquire_data['father_last_name'],
                    'mother_name' => $enquire_data['mother_first_name'] . ' ' . $enquire_data['mother_middle_name'] . ' ' . $enquire_data['mother_last_name'],
                    'address' => $enquire_data['address_line1'],
                );

                $arr_enquire_student[$key] = (object)$student_enquire;
                $count++;
            }
        }
        return Datatables::of($arr_enquire_student)->make(true);
    }

    public function sendEnquireSMS()
    {
        $message_text = trim(Input::get('enquire_sms_message'));
        $arr_student = Input::get('arr_student');
        $arr_father_no = [];
        $arr_mother_no = [];
        $arr_guardian_no = [];
        $response = '';
        $status = 'failed';
        if (!empty($arr_student)) {
            $student_data = StudentEnquire::select('student_enquire_id', 'father_contact_number', 'mother_contact_number', 'guardian_contact_number')
                ->where('status', 0)->whereIn('student_enquire_id', $arr_student)->get();
            foreach ($student_data as $student) {
                $arr_father_no[] = $student['father_contact_number'];
                $arr_mother_no[] = $student['mother_contact_number'];
                $arr_guardian_no[] = $student['guardian_contact_number'];
            }
        }
        $arr_contact_no = array_merge($arr_father_no, $arr_guardian_no, $arr_mother_no);
        $all_contact_no = implode($arr_contact_no, ',');
        if (!empty($all_contact_no) && !empty($message_text)) {
            $response = send_sms($all_contact_no, $message_text);
            $status = 'success';
        }
        $return_arr = array(
            'status' => $status,
            'message' => $response
        );
        return response()->json($return_arr);
    }

}
    