<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\StopPoint;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class StopPointController extends Controller
    {

        public function __construct()
        {
            $this->RouteController = new RouteController;
        }

        public function index()
        {
            $data = array(
                'redirect_url' => url('admin/stop-point/add'),
            );
            return view('backend.stop-point.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {
            $stop_point    = [];
            $stop_point_id = null;
            if (!empty($id))
            {
                $decrypted_stop_point_id = get_decrypted_value($id, true);
                $stop_point              = $this->getStopPointData($decrypted_stop_point_id);
                $stop_point              = isset($stop_point[0]) ? $stop_point[0] : [];
                if (!$stop_point)
                {
                    return redirect('admin/stop-point')->withError('StopPoint not found!');
                }
                $encrypted_stop_point_id = get_encrypted_value($stop_point['stop_point_id'], true);
                $save_url                = url('admin/stop-point/save/' . $encrypted_stop_point_id);
                $submit_button           = 'Update';
                $stop_point_id           = $decrypted_stop_point_id;
            }
            else
            {
                $save_url      = url('admin/stop-point/save');
                $submit_button = 'Save';
            }

            $arr_route = $this->RouteController->getRouteData();
            $all_route = [];
            foreach ($arr_route as $route)
            {
                $all_route[$route['route_id']] = $route['route'];
            }

            $stop_point['arr_route'] = add_blank_option($all_route, '-- Select route --');
            $data                    = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'stop_point'    => $stop_point,
                'redirect_url'  => url('admin/stop-point/'),
            );
            return view('backend.stop-point.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_stop_point_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $stop_point = StopPoint::find($decrypted_stop_point_id);

                if (!$stop_point)
                {
                    return redirect('/admin/stop-point/')->withError('StopPoint not found!');
                }
                $success_msg = 'Stop Point updated successfully!';
            }
            else
            {
                $stop_point  = New StopPoint;
                $success_msg = 'Stop Point saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'route_id'    => 'required',
                    'destination' => 'required',
                    'cost'        => 'required',
                    'route_order' => 'required',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction(); //Start transaction!

                try
                {
                    $stop_point->route_id    = Input::get('route_id');
                    $stop_point->destination = Input::get('destination');
                    $stop_point->cost        = Input::get('cost');
                    $stop_point->route_order = Input::get('route_order');
                    $stop_point->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/stop-point')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $stop_point_id = Input::get('stop_point_id');
            $stop_point    = StopPoint::find($stop_point_id);
            if ($stop_point)
            {
                $stop_point->delete();
                $return_arr = array(
                    'status'  => 'success',
                    'message' => 'StopPoint deleted successfully!'
                );
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'StopPoint not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $stop_point     = [];
            $limit          = Input::get('length');
            $offset         = Input::get('start');
            $arr_stop_point = $this->getStopPointData($stop_point_id  = null, $offset, $limit);
            foreach ($arr_stop_point as $key => $stop_point_data)
            {
                $stop_point[$key] = (object) $stop_point_data;
            }
            return Datatables::of($stop_point)
                    ->addColumn('action', function ($stop_point)
                    {
                        $encrypted_stop_point_id = get_encrypted_value($stop_point->stop_point_id, true);
                        return '<a title="Edit" id="deletebtn1" href="stop-point/add/' . $encrypted_stop_point_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $stop_point->stop_point_id . '"><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

        public function getStopPointData($stop_point_id = null, $offset = null, $limit = null)
        {
            $stop_point_return   = [];
            $arr_stop_point_data = StopPoint::where(function($query) use ($stop_point_id)
                {
                    if (!empty($stop_point_id))
                    {
                        $query->where('stop_point_id', $stop_point_id);
                    }
                })->with('stopPointRoute')
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
                ->get();

            if (!empty($arr_stop_point_data))
            {
                foreach ($arr_stop_point_data as $key => $stop_point_data)
                {
                    $stop_point_return[] = array(
                        'stop_point_id' => $stop_point_data['stop_point_id'],
                        'route_id'      => $stop_point_data['route_id'],
                        'route'         => $stop_point_data['stopPointRoute']['route'],
                        'cost'          => $stop_point_data['cost'],
                        'destination'   => $stop_point_data['destination'],
                        'route_order'   => $stop_point_data['route_order'],
                    );
                }
            }
            return $stop_point_return;
        }

    }
    