<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\ClassCoScholastic;
    use Illuminate\Support\Facades\DB;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;

    class ClassCoScholasticController extends Controller
    {

        public function __construct()
        {
	
        }

        public function add(Request $request, $id = NULL)
        {
	$class_co_scholastic_id = null;
	$class_id = null;
	$session_id = null;
	if (!empty($id))
	{
	    $decrypted_class_co_scholastic_id = get_decrypted_value($id, true);
	    $class_coscholastic = $this->getClassCoScholasticData($decrypted_class_co_scholastic_id);
	    $class_coscholastic = isset($class_coscholastic[0]) ? $class_coscholastic[0] : [];
	    if (!$class_coscholastic)
	    {
	        return redirect('admin/class-coscholastic')->withError('Student Marks mapping not found!');
	    }
	    $encrypted_class_co_scholastic_id = get_encrypted_value($class_coscholastic['class_co_scholastic_id'], true);
	    $save_url = url('admin/class-coscholastic/save/' . $encrypted_class_co_scholastic_id);
	    $submit_button = 'Update';
	    $class_co_scholastic_id = $decrypted_class_co_scholastic_id;
	    $class_id = $class_coscholastic['class_id'];
	    $session_id = $class_coscholastic['session_id'];
	} else
	{
	    $class_coscholastic['class_co_scholastic_id'] = null;
	    $save_url = url('admin/class-coscholastic/save/');
	    $submit_button = 'Save';
	}
	$class_coscholastic['arr_class'] = add_blank_option(get_all_classes(), '--Select class --');
	$class_coscholastic['arr_section'] = add_blank_option(get_class_section($class_id, $session_id), '--Select Section--');
	$class_coscholastic['arr_exam_category'] = add_blank_option(get_all_exam_category(), '-- Select category --');
	$class_coscholastic['arr_session'] = add_blank_option(get_session(), '-- Academic year --');
	$class_coscholastic['arr_coscholastic'] = get_all_coscholastic();
	$data = array(
	    'save_url' => $save_url,
	    'submit_button' => $submit_button,
	    'class_coscholastic' => $class_coscholastic,
	    'redirect_url' => url('admin/class-coscholastic/'),
	);
//            p($data);
	return view('backend.class-coscholastic.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {

	$decrypted_class_co_scholastic_id = get_decrypted_value($id, true);
	if (!empty($id))
	{
	    $class_coscholastic = ClassCoScholastic::find($decrypted_class_co_scholastic_id);

	    if (!$class_coscholastic)
	    {
	        return redirect('/admin/class-coscholastic/')->withError('Student Marks not found!');
	    }
	    $success_msg = 'Class Co Scholastic mapping  updated successfully!';
	} else
	{
	    $class_coscholastic = New ClassCoScholastic;
	    $success_msg = 'Class Co Scholastic mapping saved successfully!';
	}

	$class_id = Input::get('class_id');
	$section_id = Input::get('section_id');
	$exam_category_id = Input::get('exam_category_id');
	$session_id = Input::get('session_id');

	$validatior = Validator::make($request->all(), [
		'session_id' => 'required',
//		'class_id' => 'required|unique:class_co_scholastics,class_id,' . $decrypted_class_co_scholastic_id.',class_co_scholastic_id,session_id,'.$session_id.',class_id,'.$class_id.',section_id,'.$section_id.',exam_category_id,'.$exam_category_id,
		'exam_category_id' => 'required|unique:class_co_scholastics,exam_category_id,' . $decrypted_class_co_scholastic_id.',class_co_scholastic_id,session_id,'.$session_id.',class_id,'.$class_id.',section_id,'.$section_id,
		'section_id' => 'required',
		'class_id' => 'required',
	]);
	if ($validatior->fails())
	{
	    return redirect()->back()->withInput()->withErrors($validatior);
	} else
	{
	    DB::beginTransaction(); //Start transaction!
	    try
	    {
	        $class_coscholastic->class_id = $class_id;
	        $class_coscholastic->section_id = $section_id;
	        $class_coscholastic->exam_category_id = $exam_category_id;
	        $class_coscholastic->session_id = $session_id;
	        $arr_co_scholastic = Input::get('arr_co_scholastic');
	        $arr_co_scholastic_id = [];
	        if (!empty($arr_co_scholastic))
	        {
		foreach ($arr_co_scholastic as $co_scholastic_id)
		{
		    $sub_co_scholastic = [];
		    if ($request->has('sub_co_scholastic_' . $co_scholastic_id))
		    {
		        $sub_co_scholastic = Input::get('sub_co_scholastic_' . $co_scholastic_id);
		    }
		    $arr_co_scholastic_id[$co_scholastic_id] = $sub_co_scholastic;
		}
		$class_coscholastic->co_scholastic_id = json_encode($arr_co_scholastic_id);
	        }
	        $class_coscholastic->save();
	    } catch (\Exception $e)
	    {
	        //failed logic here
	        DB::rollback();
	        $error_message = $e->getMessage();
	        p($error_message);
	        return redirect()->back()->withErrors($error_message);
	    }
	    DB::commit();
	}
	return redirect('admin/class-coscholastic/')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
	$class_co_scholastic_id = Input::get('class_co_scholastic_id');
	$class_co_scholastic = ClassCoScholastic::find($class_co_scholastic_id);
	if ($class_co_scholastic)
	{
	    DB::beginTransaction(); //Start transaction!
	    try
	    {
	        $class_co_scholastic->delete();
	        $return_arr = array(
		'status' => 'success',
		'message' => 'Class Co Scholastic mapping deleted successfully!'
	        );
	    } catch (\Exception $e)
	    {
	        //failed logic here
	        DB::rollback();
	        $error_message = $e->getMessage();
	        $return_arr = array(
		'status' => 'used',
		'message' => trans('language.delete_message')
	        );
	    }
	    DB::commit();
	} else
	{
	    $return_arr = array(
	        'status' => 'error',
	        'message' => 'Class Co Scholastic mapping not found!'
	    );
	}
	return response()->json($return_arr);
        }

        public function anyData(Request $request)
        {

	$class_cosch_data = [];
	$arr_section_list = get_section();
//            $arr_root_scholastic = get_root_coscholastic();
	$limit = Input::get('length');
	$offset = Input::get('start');
	$arr_class_cosch = $this->getClassCoScholasticData($class_co_scholastic_id = null, $offset, $limit);
	foreach ($arr_class_cosch as $key => $class_cosch)
	{


	    $class_cosch_data[$key] = (object) $class_cosch;
	}
	return Datatables::of($class_cosch_data)
		    ->addColumn('action', function ($class_cosch_data)
		    {
		        $encrypted_class_co_scholastic_id = get_encrypted_value($class_cosch_data->class_co_scholastic_id, true);
		        return '<a title="Edit" id="deletebtn1" href="' . url('admin/class-coscholastic/' . $encrypted_class_co_scholastic_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
			    . '<button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $class_cosch_data->class_co_scholastic_id . '"><i class="fa fa-trash"></i></button>';
		    })
		    ->rawColumns(['action'])->make(true);
        }

        function getClassCoScholasticData($class_co_scholastic_id = null, $offset = null, $limit = null)
        {
	$arr_class_coschol_data = [];
	$arr_class_coschol = ClassCoScholastic ::where('status', 1)
	        ->where(function($query) use ($class_co_scholastic_id)
	        {
		if (!empty($class_co_scholastic_id))
		{
		    $query->where('class_co_scholastic_id', $class_co_scholastic_id);
		}
	        })
	        ->with(['CoScholasticClass' => function($q)
		{
		    $q->select(['class_id', 'class_name']);
		}])
	        ->with(['CoScholasticSection' => function($q)
		{
		    $q->select(['section_id', 'section_name']);
		}])
	        ->with(['CoScholasticExamCategory' => function($q)
		{
		    $q->select(['exam_category_id', 'exam_category']);
		}])
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
	        ->get();
	if (!empty($arr_class_coschol))
	{
	    foreach ($arr_class_coschol as $key => $class_coschol)
	    {
	        $class_coscholastic = array(
		'class_co_scholastic_id' => $class_coschol['class_co_scholastic_id'],
		'session_id' => $class_coschol['session_id'],
		'class_id' => $class_coschol['class_id'],
		'section_id' => $class_coschol['section_id'],
		'exam_category_id' => $class_coschol['exam_category_id'],
		'exam_category' => $class_coschol['CoScholasticExamCategory']['exam_category'],
		'class_name' => $class_coschol['CoScholasticClass']['class_name'],
		'section_name' => $class_coschol['CoScholasticSection']['section_name'],
		'selected_co_id' => json_decode(json_encode(json_decode($class_coschol['co_scholastic_id'])), true),
	        );
	        $arr_class_coschol_data[] = $class_coscholastic;
	    }
	}
	return $arr_class_coschol_data;
        }

    }
    