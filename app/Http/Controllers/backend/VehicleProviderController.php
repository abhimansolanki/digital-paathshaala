<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\VehicleProvider;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class VehicleProviderController extends Controller
    {

        public function __construct()
        {
            $this->RouteController = new RouteController;
        }

        public function index()
        {
            $data = array(
                'redirect_url' => url('admin/vehicle-provider/add'),
            );
            return view('backend.vehicle-provider.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {
            $vehicle_provider    = [];
            $vehicle_provider_id = null;
            if (!empty($id))
            {
                $decrypted_vehicle_provider_id = get_decrypted_value($id, true);
                $vehicle_provider              = $this->getVehicleProviderData($decrypted_vehicle_provider_id);
                $vehicle_provider              = isset($vehicle_provider[0]) ? $vehicle_provider[0] : [];
                if (!$vehicle_provider)
                {
                    return redirect('admin/vehicle-provider')->withError('Vehicle Provider not found!');
                }
                $encrypted_vehicle_provider_id = get_encrypted_value($vehicle_provider['vehicle_provider_id'], true);
                $save_url                      = url('admin/vehicle-provider/save/' . $encrypted_vehicle_provider_id);
                $submit_button                 = 'Update';
                $vehicle_provider_id           = $decrypted_vehicle_provider_id;
            }
            else
            {
                $save_url      = url('admin/vehicle-provider/save');
                $submit_button = 'Save';
            }

            $data = array(
                'save_url'         => $save_url,
                'submit_button'    => $submit_button,
                'vehicle_provider' => $vehicle_provider,
                'redirect_url'     => url('admin/vehicle-provider/'),
            );
            return view('backend.vehicle-provider.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_vehicle_provider_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $vehicle_provider = VehicleProvider::find($decrypted_vehicle_provider_id);

                if (!$vehicle_provider)
                {
                    return redirect('/admin/vehicle-provider/')->withError('VehicleProvider not found!');
                }
                $success_msg = 'Vehicle Provider updated successfully!';
            }
            else
            {
                $vehicle_provider = New VehicleProvider;
                $success_msg      = 'Vehicle Provider saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'full_name'      => 'required',
                    'email_address'  => 'required|unique:vehicle_providers,email_address,' . $decrypted_vehicle_provider_id . ',vehicle_provider_id',
                    'address'        => 'required',
                    'contact_number' => 'required|unique:vehicle_providers,contact_number,' . $decrypted_vehicle_provider_id . ',vehicle_provider_id',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction(); //Start transaction!

                try
                {
                    $vehicle_provider->full_name      = Input::get('full_name');
                    $vehicle_provider->email_address  = Input::get('email_address');
                    $vehicle_provider->address        = Input::get('address');
                    $vehicle_provider->contact_number = Input::get('contact_number');
                    $vehicle_provider->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/vehicle-provider')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $vehicle_provider_id = Input::get('vehicle_provider_id');
            $vehicle_provider    = VehicleProvider::find($vehicle_provider_id);
            if ($vehicle_provider)
            {
                $vehicle_provider->delete();
                $return_arr = array(
                    'status'  => 'success',
                    'message' => 'Vehicle Provider deleted successfully!'
                );
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Vehicle Provider not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $vehicle_provider     = [];
            $arr_vehicle_provider = $this->getVehicleProviderData();
            foreach ($arr_vehicle_provider as $key => $vehicle_provider_data)
            {
                $vehicle_provider[$key] = (object) $vehicle_provider_data;
            }
            return Datatables::of($vehicle_provider)
                    ->addColumn('action', function ($vehicle_provider)
                    {
                        $encrypted_vehicle_provider_id = get_encrypted_value($vehicle_provider->vehicle_provider_id, true);
                        return '<a title="Edit" id="deletebtn1" href="vehicle-provider/add/' . $encrypted_vehicle_provider_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $vehicle_provider->vehicle_provider_id . '"><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

        public function getVehicleProviderData($vehicle_provider_id = array())
        {
            $vehicle_provider_return   = [];
            $arr_vehicle_provider_data = VehicleProvider::where(function($query) use ($vehicle_provider_id)
                {
                    if (!empty($vehicle_provider_id))
                    {
                        $query->where('vehicle_provider_id', $vehicle_provider_id);
                    }
                })->get();
            if (!empty($arr_vehicle_provider_data))
            {
                foreach ($arr_vehicle_provider_data as $key => $vehicle_provider_data)
                {
                    $vehicle_provider_return[] = array(
                        'vehicle_provider_id' => $vehicle_provider_data['vehicle_provider_id'],
                        'full_name'           => $vehicle_provider_data['full_name'],
                        'email_address'       => $vehicle_provider_data['email_address'],
                        'address'             => $vehicle_provider_data['address'],
                        'contact_number'      => $vehicle_provider_data['contact_number'],
                    );
                }
            }
            return $vehicle_provider_return;
        }

    }
    