<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\FeeCircular;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class FeeCircularController extends Controller
    {

        public function __construct()
        {
            
        }

        /*
         * Load or show fee circular form
         */

        public function add(Request $request, $id = NULL)
        {
            $fee_circular    = [];
            $fee_circular_id = null;
            if (!empty($id))
            {
                $decrypted_fee_circular_id = get_decrypted_value($id, true);
                $fee_circular              = $this->getFeeCircularData($decrypted_fee_circular_id);
                $fee_circular              = isset($fee_circular[0]) ? $fee_circular[0] : [];
                if (!$fee_circular)
                {
                    return redirect('admin/fee-circular')->withError('FeeCircular not found!');
                }
                $encrypted_fee_circular_id = get_encrypted_value($fee_circular['fee_circular_id'], true);
                $save_url                  = url('admin/fee-circular/save/' . $encrypted_fee_circular_id);
                $submit_button             = 'Update';
                $fee_circular_id           = $decrypted_fee_circular_id;
            }
            else
            {
                $save_url      = url('admin/fee-circular/save');
                $submit_button = 'Save';
            }

            $arr_fee_circular                 = get_fee_circular_custom();
            $fee_circular['arr_fee_circular'] = add_blank_option($arr_fee_circular, '-- Select circular --');
            $data                             = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'fee_circular'  => $fee_circular,
                'redirect_url'  => url('admin/fee-circular/'),
            );
            return view('backend.fee-circular.add')->with($data);
        }

        /*
         * Insert/editinto fee circular table
         */

        public function save(Request $request, $id = NULL)
        {
            $decrypted_fee_circular_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $fee_circular = FeeCircular::find($decrypted_fee_circular_id);

                if (!$fee_circular)
                {
                    return redirect('/admin/fee-circular/')->withError('Fee Circular not found!');
                }
                $success_msg = 'Fee Circular updated successfully!';
            }
            else
            {
                $fee_circular = New FeeCircular;
                $success_msg  = 'Fee Circular saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'fee_circular' => 'required|unique:fee_circulars,fee_circular,' . $decrypted_fee_circular_id . ',fee_circular_id',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction(); //Start transaction!

                try
                {
                    $fee_circular->fee_circular = Input::get('fee_circular');
                    $fee_circular->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withInput()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/fee-circular')->withSuccess($success_msg);
        }

        /*
         * Delete fee circular and its relation's data 
         */

        public function destroy(Request $request)
        {
            $fee_circular_id = Input::get('fee_circular_id');
            $fee_circular    = FeeCircular::find($fee_circular_id);
            if ($fee_circular)
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $fee_circular->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Fee Circular deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Fee Circular not found!'
                );
            }
            return response()->json($return_arr);
        }

        /*
         * Get all fee data to show in datatable list
         */

        public function anyData()
        {
            $fee_circular     = [];
            $offset           = Input::get('start');
            $limit            = Input::get('length');
            $arr_fee_circular = $this->getFeeCircularData($fee_circular_id  = null, $offset, $limit);
            foreach ($arr_fee_circular as $key => $fee_circular_data)
            {
                $fee_circular[$key] = (object) $fee_circular_data;
            }
            return Datatables::of($fee_circular)
                    ->addColumn('action', function ($fee_circular)
                    {

                        if ($fee_circular->is_fixed != 1)
                        {
                            $encrypted_fee_circular_id = get_encrypted_value($fee_circular->fee_circular_id, true);
                            return '<a title="Edit" id="deletebtn1" href="' . url('admin/fee-circular/' . $encrypted_fee_circular_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                                . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $fee_circular->fee_circular_id . '"><i class="fa fa-trash"></i></button>';
                        }
                        else
                        {
                            return '';
                        }
                    })->make(true);
        }

        /*
         * Get Fee detail data according to fee id 
         */

        public function getFeeCircularData($fee_circular_id = null, $offset = null, $limit = null)
        {
            $fee_circular_return   = [];
            $arr_fee_circular_data = FeeCircular::
                where(function($query) use ($fee_circular_id)
                {
                    if (!empty($fee_circular_id))
                    {
                        $query->where('fee_circular_id', $fee_circular_id);
                    }
                })
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
                ->get();

            $arr_fee_circular = get_fee_circular_custom();
            if (!empty($arr_fee_circular_data))
            {
                foreach ($arr_fee_circular_data as $key => $fee_circular_data)
                {
                    $fee_circular_return[] = array(
                        'fee_circular_id'   => $fee_circular_data['fee_circular_id'],
                        'fee_circular'      => $fee_circular_data['fee_circular'],
                        'is_fixed'          => $fee_circular_data['is_fixed'],
                        'fee_circular_name' => $arr_fee_circular[$fee_circular_data['fee_circular']],
                    );
                }
            }
            return $fee_circular_return;
        }

    }
    