<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Subject;
    use App\Model\backend\SubjectGroup;
    use App\Model\backend\Classes;
    use App\Model\backend\ClassSubject;
    use App\Model\backend\StudentMark;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\View as View;
    use Session as LSession;
    use Form;

    class SubjectController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index()
        {
            $data = array(
                'redirect_url'  => url('admin/subject/add'),
                'arr_class'     => add_blank_option(get_all_classes(), '-- Select  --'),
                'arr_session'   => add_blank_option(get_session(), '-- Select  --'),
                'arr_new_group' => \Config::get('custom.arr_new_subject'),
                'session_id'    => db_current_session(),
                'arr_section'   => add_blank_option([], '-- Select  --'),
                'arr_group'     => get_all_subject_group(),
            );

            $session                  = db_current_session();
            $all_data['class_id']     = null;
            $all_data['section_id']   = null;
            $all_data['session_id']   = $session['session_id'];
            $total_record             = count(get_class_subject_mapping($all_data['session_id'], null));
            $data['class_data_count'] = $total_record;
            $data['class_data']       = '';
            $data['disabled']         = true;
            if ($total_record > 0)
            {
                $data['class_data'] = View::make('backend.subject.index-data')->with($all_data)->render();
                $data['disabled']   = false;
            }
            return view('backend.subject.index')->with($data);
        }

        /* Subject view report
         *  get all records of class subject data according  to filter like session, class and section
         */

        public function getClassSubjectData(Request $request)
        {
            $session    = db_current_session();
            $session_id = Input::get('session_id');
            if (empty($session_id))
            {
                $session_id = $session['session_id'];
            }
            $all_data['class_id']   = Input::get('class_id');
            $all_data['section_id'] = Input::get('section_id');
            $all_data['session_id'] = $session_id;
            $class_subject_data = View::make('backend.subject.index-data')->with($all_data)->render();
            $return = array(
                'status'             => 'success',
                'class_subject_data' => $class_subject_data,
            );
            return response()->json($return);
        }

        public function add()
        {
            $this->initializeGroupSession();
            $data = array(
                'redirect_url'  => url('admin/subject/add'),
                'arr_class'     => add_blank_option(get_all_classes(), '-- Select  --'),
                'arr_session'   => add_blank_option(get_session('current'), '-- Select  --'),
                'arr_new_group' => \Config::get('custom.arr_new_subject'),
                'session_id'    => db_current_session(),
                'arr_section'   => add_blank_option([], '-- Select  --'),
                'arr_group'     => get_all_subject_group(),
            );
            return view('backend.subject.subject')->with($data);
        }

        public function initializeGroupSession()
        {
            $group_session = [];
            LSession::put('subject_temp_group', $group_session);
        }

        public function saveSubjectGroup(Request $request, $id = NULL)
        {
            $new_group        = Input::get('new_group');
            $group_name       = Input::get('group_name');
            $group_type       = Input::get('group_type');
            $class_id         = Input::get('class_id');
            $session_id       = Input::get('session_id');
            $section_id       = Input::get('section_id');
            $subject_group_id = Input::get('e_subject_group_id');

            $status = 'failed';
            if (!empty($group_name))
            {
                if ($group_type == 'edit' && !empty($subject_group_id))
                {
                    $subject_group = SubjectGroup::Find($subject_group_id);
                    DB::beginTransaction();
                    try
                    {
                        $subject_group->group_name = Input::get('group_name');
                        $subject_group->save();
                    }
                    catch (\Exception $e)
                    {
                        //failed logic here
                        DB::rollback();
                        $error_message = $e->getMessage();
                        p($error_message);
                    }
                    DB::commit();
                    $status = 'success';
                }
                else
                {
                    $group_name = trim($group_name);
                    $group      = array(
                        'group_name'       => $group_name,
                        'subject_group_id' => $subject_group_id,
                        'include_in_card'  => 0,
                        'include_in_rank'  => 0,
                        'group_temp_id'    => quick_random(3),
                    );
                    $group_list = get_all_subject_group($session_id, $class_id, $section_id, null);

                    // get previous session
                    $group_session = LSession::get('subject_temp_group');
                    if (!in_array($group_name, array_column($group_session, 'group_name')) && !in_array($group_name, $group_list))
                    {
                        array_push($group_session, $group);
                        LSession::put('subject_temp_group', $group_session);
                        $status = 'success';
                    }
                    else
                    {
                        $status = 'used';
                    }
                    // put data in session
                }
            }
            return response()->json($status);
        }

        /* Add subject
         * save subject into class-subject mapping and also adding/updating in subject table */

        public function saveSubject(Request $request, $id = NULL)
        {
//            p($request->input());
            $subject_id       = Input::get('subject_id');
            $subject_name     = Input::get('subject_name');
            $subject_code     = Input::get('subject_code');
            $root_id          = Input::get('root_id');
            $class_id         = Input::get('class_id');
            $section_id       = Input::get('section_id');
            $session_id       = Input::get('session_id');
            $subject_group_id = Input::get('subject_group_id');
            $group_temp_id    = Input::get('group_temp_id');
//p($request->input());

            $status = 'failed';
            if (!empty($subject_name) && !empty($subject_code))
            {
                DB::beginTransaction();
                try
                {
                    // add subject group 
                    if (empty($subject_group_id))
                    {
                        $subject_group             = New SubjectGroup;
                        $subject_group->group_name = Input::get('subject_group_name');
                        $subject_group->save();
                        $subject_group_id          = $subject_group->subject_group_id;
                    }
                    // remove saved group from session list
                    $this->removeGroup($group_temp_id);
                    $subject_order = 1;
                    $order         = Subject::max('subject_order');
                    if (!empty($order))
                    {
                        $subject_order = $order + 1;
                    }
                    if (!empty($subject_id))
                    {
                        $subject = Subject::Find($subject_id);
                    }
                    else
                    {
                        $subject = New Subject;
                    }
                    $subject->subject_name  = $subject_name;
                    $subject->root_id       = $root_id;
                    $subject->subject_code  = $subject_code;
                    $subject->exclude_cgpa  = $request->has('exclude_cgpa') ? 1 : 0;
                    $subject->subject_order = $subject_order;
                    $subject->save();

                    //save subject into class subject mapping table
                    // subject will be added according to class, session, section.
                    if (!empty($class_id) && !empty($subject_group_id) && !empty($section_id))
                    {
                        $class_subject_data = ClassSubject::where('session_id', $session_id)
                            ->where('class_id', $class_id)->where('subject_id', $subject->subject_id)
                            ->where('subject_group_id', $subject_group_id)
                            ->where('section_id', $section_id)
                            ->first();
                        if (empty($class_subject_data->class_subject_id))
                        {
                            $class_subject = New ClassSubject;

                            // get existing group status like include in rank, result
                            $group_data = ClassSubject::where('session_id', $session_id)->where('class_id', $class_id)
                                ->where('subject_group_id', $subject_group_id)->where('section_id', $section_id)
                                ->first();

                            $class_subject->class_id         = $class_id;
                            $class_subject->session_id       = $session_id;
                            $class_subject->section_id       = $section_id;
                            $class_subject->subject_id       = $subject->subject_id;
                            $class_subject->subject_group_id = $subject_group_id;


                            if (!empty($group_data))
                            {
                                $class_subject->include_in_card = $group_data->include_in_card;
                                $class_subject->include_in_rank = $group_data->include_in_rank;
                            }
                            $class_subject->save();
                            if ($root_id != 0)
                            {
                                $parent_class_subject = ClassSubject::where('session_id', $session_id)->where('class_id', $class_id)
                                    ->where('subject_group_id', $subject_group_id)
                                    ->where('section_id', $section_id)->where('subject_id', $root_id)
                                    ->first();
                            }
                        }
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                }
                DB::commit();
                $status = 'success';
            }
            return response()->json($status);
        }

        /* Remove Group/temprary-group from temp session 
         */

        function removeGroup($group_temp_id)
        {
            $arr_group_session = LSession::get('subject_temp_group');
            $key               = array_search($group_temp_id, array_column($arr_group_session, 'group_temp_id'));
            if (isset($arr_group_session[$key]))
            {
                unset($arr_group_session[$key]);
                //update session data
                LSession::put('subject_temp_group', $arr_group_session);
            }
        }

        // Edit subject in  subject table 
        public function editSubject(Request $request, $id = NULL)
        {
//            p($request->input());
            $subject_id         = Input::get('e_subject_id');
            $e_class_subject_id = Input::get('e_class_subject_id');
            $subject_request    = Input::get('s_request_type');
            $class_subject_id   = Input::get('e_class_subject_id');
            $status             = 'failed';
            $subject_type       = '';
            $subject_data       = [];
            if ($subject_request == 'save_edit')
            {
                $subject_name = Input::get('e_subject_name');
                $subject_code = Input::get('e_subject_code');
                $subject_fee  = Input::get('e_subject_fee');
                if (!empty($subject_name) && !empty($subject_code))
                {
                    $check_existance_subject = Subject::where('subject_name', $subject_name)->where('subject_id', '!=', $subject_id)->first();
                    if (!empty($check_existance_subject->subject_id))
                    {
                        $status = 'name_used';
                    }
                    else
                    {
                        $subject = Subject::find($subject_id);
                        DB::beginTransaction();
                        try
                        {
                            // add subject group 
                            $subject->subject_name = $subject_name;
                            $subject->subject_code = $subject_code;
                            $subject->subject_fee  = $subject_fee;
                            $subject->exclude_cgpa = $request->has('exclude_cgpa') ? 1 : 0;
                            $subject->save();
                        }
                        catch (\Exception $e)
                        {
                            //failed logic here
                            DB::rollback();
                            $error_message = $e->getMessage();
                            p($error_message);
                        }
                        DB::commit();
                        $status = 'success';
                    }
                }
            }
            else
            {
                $subject      = Subject::find($subject_id);
                $subject_data = array(
                    'subject_name' => $subject->subject_name,
                    'subject_code' => $subject->subject_code,
                );
                $status       = 'success';

                // get dropdown only for main subject
//                if ($subject->root_id == 0)
//                {
//                    $selected_subject_type_id = [];
//                    $subejct_type             = ClassSubject::select('subject_type_id')->where('class_subject_id', $class_subject_id)->first();
//                    if (!empty($subejct_type['subject_type_id']))
//                    {
//                        $selected_subject_type_id = explode(',', $subejct_type['subject_type_id']);
//                    }
//                    $arr_subject_type = get_all_exam_type();
//
//                    $subject_type .= Form::select('e_subject_type_id[]', $arr_subject_type, $selected_subject_type_id, ['class' => 'form-control select2', 'id' => 'e_subject_type_id', 'multiple' => true, 'required' => true]);
//                }
            }
            $return = array(
                'status'       => $status,
                'data'         => $subject_data,
                'subject_type' => $subject_type,
            );
            return response()->json($return);
        }

        // import subject and their groups from existing session, class and section
        public function saveImportedSubject(Request $request, $id = NULL)
        {
            DB::beginTransaction();
            try
            {
                $arr_subject_group_id = Input::get('subject_group_id');
                $from_session_id      = Input::get('import_from_session_id');
                $from_class_id        = Input::get('import_from_class_id');
                $from_section_id      = Input::get('import_from_section_id');
                $class_id             = Input::get('class_id');
                $session_id           = Input::get('session_id');
                $section_id           = Input::get('section_id');
                foreach ($arr_subject_group_id as $key => $subject_group_id)
                {
                    if ($request->has('subject_id_' . $subject_group_id))
                    {
                        $arr_subject_id = Input::get('subject_id_' . $subject_group_id);
                        if (!empty($arr_subject_id))
                        {
                            foreach ($arr_subject_id as $key => $subject_id)
                            {
                                if (!empty($subject_id))
                                {
                                    $class_subject_exist = ClassSubject::
                                        where('session_id', $session_id)
                                        ->where('subject_group_id', $subject_group_id)
                                        ->where('class_id', $class_id)
                                        ->where('section_id', $section_id)
                                        ->where('subject_id', $subject_id)
                                        ->first();
                                    //if subject is not already exist then insert. otherwise skip it.
                                    if (empty($class_subject_exist->class_subject_id))
                                    {
                                        $class_subject                   = new ClassSubject;
                                        $class_subject->session_id       = $session_id;
                                        $class_subject->class_id         = $class_id;
                                        $class_subject->section_id       = $section_id;
                                        $class_subject->subject_group_id = $subject_group_id;
                                        $class_subject->subject_id       = $subject_id;

                                        // get status, order of existing data 
                                        $existed_data = ClassSubject::
                                            where('session_id', $from_session_id)
                                            ->where('subject_group_id', $subject_group_id)
                                            ->where('class_id', $from_class_id)
                                            ->where('section_id', $from_section_id)
                                            ->where('subject_id', $subject_id)
                                            ->first();

                                        // put data into new row
                                        if (!empty($existed_data->class_subject_id))
                                        {
                                            $class_subject->unassigned_student_id = $existed_data->unassigned_student_id;
                                            $class_subject->include_in_card       = $existed_data->include_in_card;
                                            $class_subject->include_in_rank       = $existed_data->include_in_rank;
                                            $class_subject->class_subject_order   = $existed_data->class_subject_order;
                                        }

                                        //inser sub subject of this subject
                                        if ($class_subject->save())
                                        {
                                            $existed_sub_subject = DB::table('class_subjects as cs')
                                                ->join('subjects as s', 'cs.subject_id', '=', 's.subject_id')
                                                ->where('cs.session_id', $from_session_id)
                                                ->where('cs.subject_group_id', $subject_group_id)
                                                ->where('cs.class_id', $from_class_id)
                                                ->where('cs.section_id', $from_section_id)
                                                ->where('s.root_id', $subject_id)
                                                ->select('cs.subject_id')
                                                ->get();
                                            if (count($existed_sub_subject) > 0)
                                            {
                                                foreach ($existed_sub_subject as $sub_subject)
                                                {
                                                    $sub_subject                              = (array) $sub_subject;
                                                    $class_sub_subject                        = new ClassSubject;
                                                    $class_sub_subject->session_id            = $session_id;
                                                    $class_sub_subject->class_id              = $class_id;
                                                    $class_sub_subject->section_id            = $section_id;
                                                    $class_sub_subject->subject_group_id      = $subject_group_id;
                                                    $class_sub_subject->subject_id            = $sub_subject['subject_id'];
                                                    $class_sub_subject->include_in_card       = $existed_data->include_in_card;
                                                    $class_sub_subject->include_in_rank       = $existed_data->include_in_rank;
                                                    $class_sub_subject->unassigned_student_id = $existed_data->unassigned_student_id;
                                                    $class_sub_subject->class_subject_order   = $existed_data->class_subject_order;
                                                    $class_sub_subject->save();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                p($error_message);
            }
            DB::commit();
            $status = 'success';
            return response()->json($status);
        }

        // get subject data of selected group,class, section and session 
        public function subjectGroupData()
        {
            $group_subject    = [];
            $subject_group_id = Input::get('subject_group_id');
            $class_id         = Input::get('class_id');
            $session_id       = Input::get('session_id');
            $section_id       = Input::get('section_id');
            if (!empty($subject_group_id) && !empty($class_id))
            {
                $arr_subject_data = DB::table('class_subjects as cs')
                    ->join('subjects as s', 's.subject_id', '=', 'cs.subject_id')
                    ->where('cs.class_id', $class_id)
                    ->where('cs.session_id', $session_id)
                    ->where('cs.subject_group_id', $subject_group_id)
                    ->where('cs.section_id', $section_id)
                    ->orderBy('cs.class_subject_order')
                    ->orderBy('s.root_id')
                    ->select('s.subject_id', 's.root_id', 's.subject_name', 's.subject_order', 's.subject_code'
                        , 'cs.class_subject_id', 'cs.subject_group_id', 'cs.unassigned_student_id')
                    ->get();

                if (!empty($arr_subject_data))
                {
                    $keys               = [];
                    $arr_subject_detail = [];
                    $arr_subject_type   = get_all_exam_type();
                    foreach ($arr_subject_data as $key => $subject)
                    {
                        $subject             = (array) $subject;
//                        $select_subject_type = '';
//                        $arr_subject_type_id = explode(',', $subject['subject_type_id']);
//                        if (!empty($subject['subject_type_id']) && !empty($arr_subject_type_id))
//                        {
//                            foreach ($arr_subject_type_id as $subject_type_id)
//                            {
//                                $select_subject_type .= $arr_subject_type[$subject_type_id] . ',';
//                            }
//                            $select_subject_type = rtrim($select_subject_type, ',');
//                        }
                        if (!in_array($subject['subject_id'], $keys) && $subject['root_id'] == 0)
                        {
                            $arr_subject_detail[$subject['subject_id']] = array(
                                'class_subject_id' => $subject['class_subject_id'],
                                'subject_id'       => $subject['subject_id'],
                                'subject_group_id' => $subject_group_id,
                                'subject_name'     => $subject['subject_name'],
                                'subject_code'     => $subject['subject_code'],
                                'subject_order'    => $subject['subject_order'],
                                'root_id'          => $subject['root_id'],
//                                'subject_type_id'  => $select_subject_type,
                            );
                        }
                        $keys = array_keys($arr_subject_detail);
                        if ($subject['root_id'] != 0 && in_array($subject['root_id'], $keys))
                        {
                            $arr_subject_detail[$subject['root_id']]['arr_sub_subect'][$subject['subject_id']] = array(
                                'class_subject_id' => $subject['class_subject_id'],
                                'subject_id'       => $subject['subject_id'],
                                'subject_group_id' => $subject_group_id,
                                'root_id'          => $subject['root_id'],
                                'subject_name'     => $subject['subject_name'],
                                'subject_code'     => $subject['subject_code'],
                                'subject_order'    => $subject['subject_order'],
                            );
                        }
                    }

                    if (!empty($arr_subject_detail))
                    {
                        foreach ($arr_subject_detail as $key => $value)
                        {
                            $group_subject[] = (object) $value;
                        }
                    }
                }
            }
            return Datatables::of($group_subject)
                    ->addColumn('sub_subject', function ($group_subject)
                    {
                        return '<div class="text-center addpusintable"><button  data-toggle="modal" class="open_subject_model" data-backdrop="static" data-keyboard="false" class="btn btn-primary" type="button" title="Add" subject_group_id="' . $group_subject->subject_group_id . '" subject_id="' . $group_subject->subject_id . '" subject_name="' . $group_subject->subject_name . '"  class_subject_id="' . $group_subject->class_subject_id . '" root_id="' . $group_subject->subject_id . '"><i class="fas fa-plus-square"></i></button></div>';
                    })
                    ->addColumn('action', function ($group_subject)
                    {
                        return '<div class="text-center"><button class="btn btn-primary open_edit_subject_model" type="button" title="Edit" subject_id="' . $group_subject->subject_id . '" request_type="open_edit" class_subject_id="' . $group_subject->class_subject_id . '" root_id="' . $group_subject->root_id . '"><i class="fas fa-edit"></i></button>'
                            . '<button type="button" class="btn btn-primary subject-data-delete" title="Remove" delete_type="subject" subject_id="' . $group_subject->subject_id . '" subject_group_id="' . $group_subject->subject_group_id . '"  class_subject_id="' . $group_subject->class_subject_id . '"><i class="fa fa-trash"></i></button>'
                            . '<button type="button" class="custombtnnm btn btn-success student-assign-unassign" request_type="get_student"  subject_name="' . $group_subject->subject_name . '" class_subject_id="' . $group_subject->class_subject_id . '">Unassign Student</buttno></div>';
                    })
                    ->rawColumns(['checkbox', 'sub_subject', 'action'])->make(true);
        }

        // get total subject group of selected class, session and section 
        public function getSubjectGroup()
        {
            $temp_group = LSession::get('subject_temp_group');
            $arr_group  = [];
            $status     = 'failed';
            $class_id   = Input::get('class_id');
            $session_id = Input::get('session_id');
            $section_id = Input::get('section_id');
            if (!empty($class_id))
            {
                $arr_group_data = DB::table('class_subjects as cs')
                    ->join('subject_groups as sg', 'sg.subject_group_id', '=', 'cs.subject_group_id')
                    ->where('cs.class_id', $class_id)
                    ->where('cs.session_id', $session_id)
                    ->where('cs.section_id', $section_id)
                    ->distinct('cs.subject_group_id')
                    ->select('sg.subject_group_id', 'sg.group_name', 'cs.include_in_card', 'cs.include_in_rank')
                    ->get();
                if (!empty($arr_group_data))
                {
                    foreach ($arr_group_data as $key => $group)
                    {
                        $group                                 = (array) $group;
                        $group_subject                         = array(
                            'subject_group_id' => $group['subject_group_id'],
                            'group_name'       => $group['group_name'],
                            'include_in_card'  => $group['include_in_card'],
                            'include_in_rank'  => $group['include_in_rank'],
                            'group_temp_id'    => null,
                        );
                        $arr_group[$group['subject_group_id']] = $group_subject;
                    }
                }
                $status = 'success';
            }

            $temp_group_include = [];
            if (!empty($temp_group))
            {
                foreach ($temp_group as $key => $t_group)
                {
                    if (!in_array($t_group['group_name'], array_column($arr_group, 'group_name')))
                    {
                        array_push($temp_group_include, $t_group);
                    }
                }
            }
            $all_groups['arr_group']        = array_merge($arr_group, $temp_group_include);
            $all_groups['class_id']         = $class_id;
            $all_groups['arr_new_subject']  = \Config::get('custom.arr_new_subject');
            $all_groups['arr_subject_type'] = get_all_exam_type();
            $arr_group_id                   = json_encode($all_groups['arr_group']);
            $html_tables                    = View::make('backend.subject.subject-html')->with($all_groups)->render();
            $return                         = array(
                'status'       => $status,
                'html_tables'  => $html_tables,
                'arr_group_id' => $arr_group_id,
            );
            return response()->json($return);
        }

        public function studentUnAssignedSubject(Request $request)
        {
            $return['status']      = 'failed';
            $unassigned_student_id = [];
            $request_type          = Input::get('request_type');
            $session_id            = Input::get('session_id');
            $class_id              = Input::get('class_id');
            $section_id            = Input::get('section_id');
            $class_subject_id      = Input::get('class_subject_id');
            if (!empty($class_subject_id))
            {
                $class_subject = ClassSubject::where('class_subject_id', $class_subject_id)->first();
                if ($request_type == 'update_student')
                {
                    $arr_unassigned_student_id = Input::get('unassigned_student_id'); // updated studetn id
                    $arr_student_id            = array();
                    if (!empty($class_subject->unassigned_student_id))
                    {
                        $arr_student_id = json_decode(json_encode(json_decode($class_subject->unassigned_student_id)), true); //explode(',', $class_subject->unassigned_student_id);
                    }
                    if (!empty($arr_unassigned_student_id))
                    {
                        $arr_student_id = array_unique(array_merge($arr_student_id, $arr_unassigned_student_id));
                    }
                    $unassigned_student_id                = $arr_student_id;
                    $class_subject->unassigned_student_id = json_encode($arr_student_id, JSON_FORCE_OBJECT);
                    $class_subject->save();

                    $arr_assigned_student_id = Input::get('assigned_student_id'); // updated studetn id
                    if (!empty($arr_assigned_student_id))
                    {
                        $class_subject         = ClassSubject::where('class_subject_id', $class_subject_id)->first();
                        $student_unassigned_id = [];
                        $arr_student_id        = '';
                        if (!empty($class_subject->unassigned_student_id))
                        {
                            $student_unassigned_id = json_decode(json_encode(json_decode($class_subject->unassigned_student_id)), true);
                        }
                        $final_student_id      = array_diff($student_unassigned_id, $arr_assigned_student_id);
                        $unassigned_student_id = $final_student_id;
                        if (!empty($final_student_id))
                        {
                            $arr_student_id = $final_student_id;
                        }
                        $class_subject->unassigned_student_id = json_encode($arr_student_id, JSON_FORCE_OBJECT);
                        $class_subject->save();
                    }
                }
                elseif ($request_type == 'get_student')
                {
                    if (!empty($class_subject->unassigned_student_id))
                    {
                        $unassigned_student_id = json_decode(json_encode(json_decode($class_subject->unassigned_student_id)), true); //  studetn id
                    }
                }
            }
            $student_mark_exist      = DB::table('student_marks')->where('class_id', $class_id)->where('section_id', $section_id)->where('session_id', $session_id)->count();
            $action_enable           = $student_mark_exist > 0 ? true : false;
            $return['student_list']  = $this->studentAssigendUnAssigned($session_id, $class_id, $section_id, $student_mark_exist, $unassigned_student_id);
            $return['status']        = 'success';
            $return['action_enable'] = $action_enable;
            return response()->json($return);
        }

        public function studentAssigendUnAssigned($session_id, $class_id, $section_id, $action_enable, $arr_unassigned_student_id = [])
        {
            $student_list = '';
            if (!empty($session_id) && !empty($class_id) && !empty($section_id))
            {
                $student['student'] = [];
                $arr_student        = [];
                $type               = 'table';
                $table              = get_student_table($session_id, $type);
                $arr_student_data   = DB::table($table)
                    ->where('s.current_class_id', $class_id)
                    ->where('s.current_section_id', $section_id)
                    ->where('s.student_status', 1)
                    ->where('s.current_session_id', $session_id)
                    ->select('s.student_id', 's.enrollment_number', 's.first_name', 's.middle_name', 's.last_name')
                    ->get();
                if (!empty($arr_student_data))
                {
                    foreach ($arr_student_data as $key => $student_data)
                    {
                        $student_data = (array) $student_data;
                        $student_info = [];
                        $student_info = array(
                            'student_id'        => $student_data['student_id'],
                            'student_name'      => $student_data['first_name'] . ' ' . $student_data['middle_name'] . '' . $student_data['last_name'],
                            'enrollment_number' => $student_data['enrollment_number'],
                        );
                        if (in_array($student_data['student_id'], $arr_unassigned_student_id))
                        {
                            $arr_student['unassigned'][] = $student_info;
                        }
                        else
                        {
                            $arr_student['assigned'][] = $student_info;
                        }
                    }
                }
                $student['action_enable'] = $action_enable;
                $student['student']       = $arr_student;
                $student_list             = View::make('backend.subject.subject-assigned-unassigned')->with($student)->render();
            }
            return $student_list;
        }

        public function getGroupList()
        {
            $group_subject                     = [];
            $return_group['arr_group_subject'] = [];
            $status                            = 'failed';
            $import_into_class_id              = Input::get('class_id'); // import into
            $import_into_section_id            = Input::get('section_id'); // import into
            $session_id                        = Input::get('session_id'); // import into
            $import_from_session_id            = Input::get('import_from_session_id'); // import from
            $import_from_class_id              = Input::get('import_from_class_id'); // import from
            $import_from_section_id            = Input::get('import_from_section_id'); // import from
            if (!empty($import_from_class_id))
            {
                $arr_group_data = SubjectGroup:: orderBy('group_name')
                    ->where('group_status', 1)
                    ->whereHas('groupClassSubject', function($query) use ($import_from_class_id, $import_from_section_id, $import_from_session_id)
                    {
                        $query->where('session_id', $import_from_session_id);
                        $query->where('class_id', $import_from_class_id);
                        $query->where('section_id', $import_from_section_id);
                    })
                    ->with(['groupClassSubject' => function($query) use ($import_from_class_id, $import_from_section_id, $import_from_session_id)
                        {
                            $query->where('session_id', $import_from_session_id);
                            $query->where('class_id', $import_from_class_id);
                            $query->where('section_id', $import_from_section_id);
                        }])
                    ->get();
                if (!empty($arr_group_data))
                {
                    foreach ($arr_group_data as $key => $group)
                    {
                        $arr_group   = array(
                            'subject_group_id' => $group['subject_group_id'],
                            'group_name'       => $group['group_name'],
                            'group_subject'    => [],
                        );
                        $arr_subject = [];
                        if (!empty($group['groupClassSubject']))
                        {
                            foreach ($group['groupClassSubject'] as $key => $class_subject)
                            {
                                /* //Insecure// We can get relation data through internal relation */
                                $subject = $class_subject['getSub'];
                                // show only subjects
                                if ($subject['root_id'] == 0)
                                {
                                    $arr_subject[$subject['subject_id']] = $subject['subject_name'] . '(' . $subject['subject_code'] . ')';
                                }
                            }
                        }
                        if (!empty($arr_subject))
                        {
                            $arr_group['group_subject'] = $arr_subject;
                        }
                        $group_subject[] = $arr_group;
                    }
                    $return_group['arr_group_subject'] = $group_subject;
                }
            }

            $return_group['arr_class']   = $this->importClass($import_into_class_id);
            $return_group['arr_session'] = get_session();
            if (!empty($return_group['arr_class']))
            {
                $status = 'success';
            }
            $import_group_subject = View::make('backend.subject.import-subject')->with($return_group)->render();
            $return_data          = array(
                'status'               => $status,
                'import_group_subject' => $import_group_subject,
            );
            return response()->json($return_data);
        }

        public function getSubjectList(Request $request)
        {
            $class_id         = Input::get('class_id');
            $session_id       = Input::get('session_id');
            $section_id       = Input::get('section_id');
            $subject_group_id = Input::get('subject_group_id');
            $root_id          = Input::get('root_id');
            $arr_subject      = [];
            $status           = 'failed';
            $arr_subject_data = DB::table('subjects as s')->where(function($q) use ($root_id)
                {
                    if ($root_id == 0)
                    {
                        $q->where('s.root_id', 0);
                    }
                    else
                    {
                        $q->where('s.root_id', '=', $root_id);
                    }
                })
//                    ->whereNOTIn('subject_id', function($query) use ($class_id, $session_id, $arr_section_id,$subject_group_id)
//                    {
//                        $query->select('cs.subject_id')
//                        ->from('class_subjects as cs')
//                        ->where('cs.class_id', $class_id)
//                        ->where('cs.section_id', $section_id)
//                        ->where('cs.session_id', $session_id);
//                    })
                ->get();

            if (!empty($arr_subject_data))
            {
                foreach ($arr_subject_data as $key => $subject)
                {
                    $subject       = (array) $subject;
                    $arr_subject[] = array(
                        'subject_id'   => $subject['subject_id'],
                        'subject_name' => $subject['subject_name'] . '(' . $subject['subject_code'] . ')',
                        'name'         => $subject['subject_name'],
                        'subject_code' => $subject['subject_code'],
                        'exclude_cgpa' => $subject['exclude_cgpa'],
                    );
                }
                $status = 'success';
            }
            $return_data = array(
                'status'      => $status,
                'arr_subject' => $arr_subject,
            );
            return response()->json($return_data);
        }

        public function getGroup()
        {
            $arr_group   = get_all_subject_group();
            $status      = 'success';
            $return_data = array(
                'status'    => $status,
                'arr_group' => $arr_group,
            );
            return response()->json($return_data);
        }

        // class list for showing in import modal
        function importClass($import_into_class_id)
        {
            //$import_into_class_id commented this because class may repeat but section not
            $arr_class        = [];
            $arr_import_class = DB:: table('classes as c')
                    ->join('class_subjects as cs', 'cs.class_id', '=', 'c.class_id')
//                    ->where('cs.class_id', '!=', $import_into_class_id)
                    ->where('class_status', 1)->select('c.class_id', 'c.class_name')->orderBy('class_order', 'ASC')->get();
            if (!empty($arr_import_class))
            {
                foreach ($arr_import_class as $class)
                {
                    $class                         = (array) $class;
                    $arr_class[$class['class_id']] = $class['class_name'];
                }
            }
            return $arr_class;
        }

        // Include group in marksheet or in rank in class-subject mapping table
        function includeGroupInResult(Request $request)
        {
            $status           = 'falied';
            $subject_group_id = Input::get('subject_group_id');
            $include_in_card  = Input::get('include_in_card');
            $include_in_rank  = Input::get('include_in_rank');
            $class_id         = Input::get('class_id');
            $session_id       = Input::get('session_id');
            $section_id       = Input::get('section_id');
            $type             = Input::get('type');
            if (!empty($subject_group_id))
            {
                if (!empty($subject_group_id) && !empty($class_id))
                {
                    if ($type == 'card')
                    {
                        DB::table('class_subjects')->where('subject_group_id', $subject_group_id)->where('session_id', $session_id)
                            ->where('class_id', $class_id)->where('section_id', $section_id)
                            ->update(['include_in_card' => $include_in_card]);
                    }
                    elseif ($type == 'rank')
                    {
                        DB::table('class_subjects')->where('subject_group_id', $subject_group_id)->where('session_id', $session_id)
                            ->where('class_id', $class_id)->where('section_id', $section_id)
                            ->update(['include_in_rank' => $include_in_rank]);
                    }
                    $status = 'success';
                }
            }
            return response()->json($status);
        }

        // Remove mapping of group/subject and sub-subject from class-subject table of selected session  
        public function destroy(Request $request)
        {
            $subject_id       = Input::get('subject_id');
            $class_id         = Input::get('class_id');
            $session_id       = Input::get('session_id');
            $section_id       = Input::get('section_id');
            $subject_group_id = Input::get('subject_group_id');
            $delete_type      = Input::get('delete_type');
            $group_temp_id    = Input::get('group_temp_id');
            $arr_subject_id   = [];
            if (!empty($delete_type) && !empty($class_id))
            {
                DB::beginTransaction();
                try
                {
                    if ($delete_type == 'subject' && !empty($subject_id) && !empty($subject_group_id))
                    {
                        $arr_subject_id[] = $subject_id;
                        $subject_status   = $this->checkSubjectResultStatus($session_id, $class_id, $arr_subject_id);
                        if ($subject_status === true)
                        {
                            // check  sub-subject if exist then first delete them and then delete main subject from class subject mapping.
                            $arr_subject_id = Subject::select('subject_id')->where('root_id', $subject_id)->orWhere('subject_id', $subject_id)->get()->toArray();
                            if (!empty($arr_subject_id))
                            {

                                $arr_subject_id = array_column($arr_subject_id, 'subject_id');
                                $class_subject  = DB::table('class_subjects')->where('class_id', $class_id)
                                        ->where('session_id', $session_id)
                                        ->whereIn('subject_id', $arr_subject_id)
                                        ->where('section_id', $section_id)
                                        ->where('subject_group_id', $subject_group_id)->delete();
                            }
                            $return_arr = array(
                                'status'  => 'success',
                                'message' => 'Unmapped successfully!'
                            );
                        }
                        else
                        {
                            $return_arr = array(
                                'status'  => 'used',
                                'message' => trans('language.delete_message')
                            );
                        }
                    }
                    elseif ($delete_type == 'subject_group' && (!empty($subject_group_id) || $group_temp_id != ''))
                    {
                        if (!empty($group_temp_id))
                        {
                            $this->removeGroup($group_temp_id);
                        }
                        if (!empty($subject_group_id))
                        {
                            $class_subject = ClassSubject::where('class_id', $class_id)
                                    ->where('session_id', $session_id)
                                    ->where('section_id', $section_id)
                                    ->where('subject_group_id', $subject_group_id)->get();
                            foreach ($class_subject as $key => $subject)
                            {
                                $arr_subject_id[] = $subject['subject_id'];
                            }
                            $subject_status = $this->checkSubjectResultStatus($session_id, $class_id, $arr_subject_id);
                            if ($subject_status == true)
                            {
                                DB::table('class_subjects')->where('class_id', $class_id)
                                    ->where('session_id', $session_id)
                                    ->where('section_id', $section_id)
                                    ->where('subject_group_id', $subject_group_id)->delete();
                                $return_arr = array(
                                    'status'  => 'success',
                                    'message' => 'Unmapped successfully!'
                                );
                            }
                            else
                            {
                                $return_arr = array(
                                    'status'  => 'used',
                                    'message' => trans('language.delete_message')
                                );
                            }
                        }
                    }
                    else
                    {
                        $return_arr = array(
                            'status'  => 'error',
                            'message' => 'Subject Data not found!'
                        );
                    }
                }
                catch (Exception $ex)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                }
                DB::commit();
            }
            return response()->json($return_arr);
        }

        /* Check subject status while deleting 
         * If subject already used in exam, marks then it cann't be deleted
         */

        function checkSubjectResultStatus($session_id, $class_id, $arr_subject_id)
        {
            $arr_subject_mark = StudentMark :: where('mark_status', 1)->where('publish_status', 1)
                ->where('session_id', $session_id)->where('class_id', $class_id)
                ->with(['markDetails' => function($q)
                    {
                        $q->select(['student_mark_id', 'student_mark_detail_id', 'obtained_marks']);
                    }])
                ->get();
            $arr_marks_subject_id = [];
            foreach ($arr_subject_mark as $key => $subject_mark)
            {
                foreach ($subject_mark['markDetails'] as $key => $mark)
                {
                    $sub_with_tot         = json_decode(json_encode(json_decode($mark['obtained_marks'])), true);
                    // remove tot_obtained index from subject marks list
                    unset($sub_with_tot['tot_obtained']);
                    $arr_marks_subject_id = array_merge($arr_marks_subject_id, array_keys($sub_with_tot));
                }
            }
            $arr_marks_subject_id = array_unique($arr_marks_subject_id);
            foreach ($arr_subject_id as $key => $subject_id)
            {
                if (in_array($subject_id, $arr_marks_subject_id))
                {
                    $return = false;
                    break;
                }
                else
                {
                    $return = true;
                    continue;
                }
            }
            return $return;
        }

        // Sort subject order according to session, class and section 
        function setClassSubjectOrder(Request $request)
        {
            if ($request->ajax())
            {
                DB::beginTransaction();
                try
                {
                    $arr_class_subject_order = Input::get('item');
                    foreach ($arr_class_subject_order as $key => $class_subject_id)
                    {
                        $subject_order                      = $key + 1;
                        $class_subject                      = ClassSubject::find($class_subject_id);
                        $class_subject->class_subject_order = $subject_order;
                        $class_subject->save();

                        /*
                         *  Set same order to sub-subject as subject(parent subject) 
                         */
                        if (!empty($class_subject->class_subject_id))
                        {
                            DB::table('class_subjects as cs')
                                ->join('subjects as s', 'cs.subject_id', '=', 's.subject_id')
                                ->where('cs.session_id', $class_subject->session_id)
                                ->where('cs.class_id', $class_subject->class_id)
                                ->where('cs.section_id', $class_subject->section_id)
                                ->where('cs.subject_group_id', $class_subject->subject_group_id)
                                ->where('s.root_id', $class_subject->subject_id)
                                ->update(['class_subject_order' => $subject_order]);
                        }
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                }
                DB::commit();
                return response()->json('success');
            }
        }

        // get subject list to sort according  to session, class and section
        function getClassSubject(Request $request)
        {
            $class_id          = Input::get('class_id');
            $session_id        = Input::get('session_id');
            $section_id        = Input::get('section_id');
            $arr_subject       = [];
            $status            = 'failed';
            $arr_class_subject = DB::table('class_subjects as cs')
                ->join('subjects as s', 's.subject_id', '=', 'cs.subject_id')
                ->where('cs.section_id', $section_id)
                ->where('cs.class_id', $class_id)
                ->where('cs.session_id', $session_id)
                ->where('s.root_id', 0)
                ->select('cs.class_subject_id', 's.subject_name')
                ->orderBy('cs.class_subject_order')
                ->get();

            if (!empty($arr_class_subject))
            {
                foreach ($arr_class_subject as $key => $class_subject)
                {
                    $class_subject = (array) $class_subject;
                    $arr_subject[] = array(
                        'class_subject_id' => $class_subject['class_subject_id'],
                        'subject_name'     => $class_subject['subject_name'],
                    );
                    $status        = 'success';
                }
            }
            return response()->json(array('status' => $status, 'data' => $arr_subject));
        }

        /* Check Group name for uniquness 
         */

        public function checkGroupName(Request $request)
        {
            $group_name = Input::get('group_name');
            $group_id   = Input::get('subject_group_id');
            $group      = SubjectGroup::where('group_name', $group_name)
                ->where(function($query) use ($group_id)
                {
                    if (!empty($group_id))
                    {
                        $query->where('subject_group_id', '!=', $group_id);
                    }
                })
                ->first();
            if (!empty($group) && !empty($group->group_name))
            {
                return response()->json(false);
            }
            else
            {
                return response()->json(true);
            }
        }

        /* Check subject name for uniquness 
         */

        public function checkSubjectName(Request $request)
        {
            $return       = [];
            $subject_name = Input::get('subject_name');
            $subject_code = Input::get('subject_code');
            $subject_id   = Input::get('subject_id');
            $subject      = Subject::
                where(function($query) use ($subject_id)
                {
                    if (!empty($subject_id))
                    {
                        $query->where('subject_id', '!=', $subject_id);
                    }
                })
                ->where(function($query) use ($subject_name)
                {
                    if (!empty($subject_name))
                    {
                        $query->where('subject_name', $subject_name);
                    }
                })
//                ->where(function($query) use ($subject_code)
//                {
//                    if (!empty($subject_code))
//                    {
//                        $query->where('subject_code', $subject_code);
//                    }
//                })
                ->first();
            if (!empty($subject) && !empty($subject->subject_name || !empty($subject->subject_code)))
            {

                $return = array(
                    'status'  => false,
                    'message' => 'Subject name already exist in master, please reuse it',
                );
            }
            else
            {
                $return = array(
                    'status'  => true,
                    'message' => '',
                );
            }
            return response()->json($return);
        }
    }
    