<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\CoScholastic;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class CoScholasticController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index()
        {
            $data = array(
                'redirect_url' => url('admin/coscholastic/add'),
            );
            return view('backend.coscholastic.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {
            $coscholastic     = [];
            $co_scholastic_id = null;
            if (!empty($id))
            {
                $decrypted_co_scholastic_id = get_decrypted_value($id, true);
                $coscholastic               = $this->getCoScholasticData($decrypted_co_scholastic_id);
                $coscholastic               = isset($coscholastic[0]) ? $coscholastic[0] : [];
                if (!$coscholastic)
                {
                    return redirect('admin/coscholastic')->withError('CoScholastic not found!');
                }
                $encrypted_co_scholastic_id = get_encrypted_value($coscholastic['co_scholastic_id'], true);
                $save_url                   = url('admin/coscholastic/save/' . $encrypted_co_scholastic_id);
                $submit_button              = 'Update';
                $co_scholastic_id           = $decrypted_co_scholastic_id;
            }
            else
            {
                $save_url      = url('admin/coscholastic/save');
                $submit_button = 'Save';
            }
            $arr_class                 = get_all_classes();
            $coscholastic['arr_class'] = add_blank_option($arr_class, '--Select class --');
            $data                      = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'coscholastic'  => $coscholastic,
                'redirect_url'  => url('admin/coscholastic/'),
            );
            return view('backend.coscholastic.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_co_scholastic_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $coscholastic = CoScholastic::find($decrypted_co_scholastic_id);

                if (!$coscholastic)
                {
                    return redirect('/admin/coscholastic/')->withError('CoScholastic not found!');
                }
                $success_msg = 'Co Scholastic updated successfully!';
            }
            else
            {
                $coscholastic = New CoScholastic;
                $success_msg  = 'Co Scholastic saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'co_scholastic_name' => 'required|unique:co_scholastics,co_scholastic_name,' . $decrypted_co_scholastic_id . ',co_scholastic_id',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $coscholastic->co_scholastic_name = Input::get('co_scholastic_name');
                    $coscholastic->co_scholastic_head = Input::get('co_scholastic_head');
                    $coscholastic->description        = Input::get('description');
                    $coscholastic->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/coscholastic')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $co_scholastic_id = Input::get('co_scholastic_id');
            $coscholastic     = CoScholastic::find($co_scholastic_id);
            if ($coscholastic)
            {
                $coscholastic->delete();
                $return_arr = array(
                    'status'  => 'success',
                    'message' => 'CoScholastic deleted successfully!'
                );
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'CoScholastic not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $coscholastic     = [];
            $root             = get_root_coscholastic();
            $limit            = Input::get('length');
            $offset           = Input::get('start');
            $arr_coscholastic = $this->getCoScholasticData($co_scholastic_id = null, $offset, $limit);
            foreach ($arr_coscholastic as $key => $coscholastic_data)
            {
                if ($coscholastic_data['root_id'] != 0)
                {
                    $coscholastic_data['co_scholastic_name'] = $root[$coscholastic_data['root_id']] . '->' . $coscholastic_data['co_scholastic_name'];
                }
                $coscholastic[$key] = (object) $coscholastic_data;
            }
            return Datatables::of($coscholastic)
                    ->addColumn('action', function ($coscholastic)
                    {
                        $encrypted_co_scholastic_id = get_encrypted_value($coscholastic->co_scholastic_id, true);
                        return '<a title="Edit" id="deletebtn1" href="' . url('admin/coscholastic/' . $encrypted_co_scholastic_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>';
                    })
                    ->addColumn('sub_coscholastic', function ($coscholastic)
                    {
                        if ($coscholastic->root_id == 0)
                        {
                            return '<button type="button" title="Add Sub CoScholastic" class="btn btn-info add_sub_coscholastic" co_scholastic_id="' . $coscholastic->co_scholastic_id . '" style="text-align:center;padding: 1px 6px !important;border-radius: 4px !important;margin-left: 40px;">'
                                . '<i class="fa fa-plus"></i></button>';
                        }
                    })
                    ->rawColumns(['action', 'sub_coscholastic'])
                    ->make(true);
        }

        public function getCoScholasticData($co_scholastic_id = null, $offset = null, $limit = null)
        {
            $coscholastic_return   = [];
            $coscholastic          = [];
            $arr_coscholastic_data = CoScholastic::where('status', 1)
                ->where(function($query) use ($co_scholastic_id)
                {
                    if (!empty($co_scholastic_id))
                    {
                        $query->where('co_scholastic_id', $co_scholastic_id);
                    }
                })
//                ->where(function($query) use ($offset, $limit)
//                {
//                    if (!empty($offset))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
                ->get();
            if (!empty($arr_coscholastic_data))
            {
                foreach ($arr_coscholastic_data as $key => $coscholastic_data)
                {
                    $coscholastic = array(
                        'co_scholastic_id'   => $coscholastic_data['co_scholastic_id'],
                        'root_id'            => $coscholastic_data['root_id'],
                        'co_scholastic_name' => $coscholastic_data['co_scholastic_name'],
                        'co_scholastic_head' => $coscholastic_data['co_scholastic_head'],
                        'description'        => $coscholastic_data['description'],
                    );

                    $coscholastic_return[] = $coscholastic;
                }
            }
            return $coscholastic_return;
        }

        /*
         * Add new coscholastic from class screen
         */

        public function addSubCoScholastic(Request $request)
        {
            $co_scholastic_name = Input::get('co_scholastic_name');
            $coscholastic       = CoScholastic::where('co_scholastic_name', trim($co_scholastic_name))->first();
            if (!empty($coscholastic->co_scholastic_name))
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Co Scholastic already exist, please try new',
                );
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $add_coscholastic                     = new CoScholastic;
                    $add_coscholastic->root_id            = Input::get('root_id');
                    $add_coscholastic->co_scholastic_name = $co_scholastic_name;
                    $add_coscholastic->co_scholastic_head = Input::get('co_scholastic_head');
                    $add_coscholastic->description        = Input::get('description');
                    $add_coscholastic->save();

                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Sub Co Scholastic saved successfully!',
                    );
                }
                catch (\Exception $e)
                {
                    $return_arr = array(
                        'status'  => 'failed',
                        'message' => 'Something went wrong, please try again!',
                    );
                }

                DB::commit();
            }
            return response()->json($return_arr);
        }

    }
    