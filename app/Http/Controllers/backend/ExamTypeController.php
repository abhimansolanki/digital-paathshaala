<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\ExamType;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class ExamTypeController extends Controller
    {

        public function __construct()
        {
            
        }

        public function add(Request $request, $id = NULL)
        {
            $exam_type    = [];
            $exam_type_id = null;
            if (!empty($id))
            {
                $decrypted_exam_type_id = get_decrypted_value($id, true);
                $exam_type              = $this->getExamTypeData($decrypted_exam_type_id);
                $exam_type              = isset($exam_type[0]) ? $exam_type[0] : [];

                if (!$exam_type)
                {
                    return redirect('admin/exam-type')->withError('Exam Type not found!');
                }
                $encrypted_exam_type_id = get_encrypted_value($exam_type['exam_type_id'], true);
                $save_url               = url('admin/exam-type/save/' . $encrypted_exam_type_id);
                $submit_button          = 'Update';
                $exam_type_id           = $decrypted_exam_type_id;
            }
            else
            {
                $save_url      = url('admin/exam-type/save');
                $submit_button = 'Save';
            }

            $data = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'exam_type'     => $exam_type,
            );
            return view('backend.exam-type.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_exam_type_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $exam_type = ExamType::find($decrypted_exam_type_id);

                if (!$exam_type)
                {
                    return redirect('/admin/exam-type/')->withError('Exam Type not found!');
                }
                $success_msg = 'Exam Type updated successfully!';
            }
            else
            {
                $exam_type   = New ExamType;
                $success_msg = 'Exam Type saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'exam_type' => 'required|unique:exam_types,exam_type,' . $decrypted_exam_type_id . ',exam_type_id',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $exam_type->exam_type       = Input::get('exam_type');
                    $exam_type->exam_type_alias = Input::get('exam_type_alias');
                    $exam_type->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/exam-type')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $exam_type_id = Input::get('exam_type_id');
            $exam_type    = ExamType::find($exam_type_id);
            if ($exam_type)
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $exam_type->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Exam Type deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Exam Type not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $exam_type = [];
            $arr_grade = $this->getExamTypeData();
            foreach ($arr_grade as $key => $exam_type_data)
            {
                $exam_type[$key] = (object) $exam_type_data;
            }
            return Datatables::of($exam_type)
                    ->addColumn('action', function ($exam_type)
                    {
                        $encrypted_exam_type_id = get_encrypted_value($exam_type->exam_type_id, true);
                        return '<a title="Edit" id="deletebtn1" href="exam-type/' . $encrypted_exam_type_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $exam_type->exam_type_id . '"><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

        public function getExamTypeData($exam_type_id = array())
        {
            $exam_type_return   = [];
            $arr_exam_type_data = ExamType::where(function($query) use ($exam_type_id)
                {
                    if (!empty($exam_type_id))
                    {
                        $query->where('exam_type_id', $exam_type_id);
                    }
                })->get();
            if (!empty($arr_exam_type_data))
            {
                foreach ($arr_exam_type_data as $key => $exam_type_data)
                {
                    $exam_type_return[] = array(
                        'exam_type_id'    => $exam_type_data['exam_type_id'],
                        'exam_type'       => $exam_type_data['exam_type'],
                        'exam_type_alias' => $exam_type_data['exam_type_alias'],
                    );
                }
            }
            return $exam_type_return;
        }

    }
    