<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\api\ApiController;
use App\Model\backend\ChapterTopic;
use App\Model\backend\ClassSubject;
use App\Model\backend\EClassRoom;
use App\Model\backend\Subject;
use App\Model\backend\SubjectChapter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Validator;
use Illuminate\Support\Facades\Input;
use Datatables;
use Illuminate\Support\Facades\View as View;
use Illuminate\Support\Facades\DB;

class EClassRoomController extends Controller
{
    public function index()
    {
        $session = db_current_session();
        $data = array(
            'redirect_url' => url('admin/e-class-room/add'),
            'session' => $session,
            'arr_session' => add_blank_option(get_session(), '-- Select  --'),
            'arr_class' => add_blank_option(get_all_classes(), '-- Select  --'),
        );
        return view('backend.e-class-room.index')->with($data);
    }

    public function save(Request $request)
    {
        $validatior = Validator::make($request->all(), [
            'data' => 'required',
        ]);
        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        DB::beginTransaction(); //Start transaction!
        try {
            foreach ($request['data'] as $eclass) {
                if (!empty($eclass['e_class_room_id'])) {
                    $eclass_room = EClassRoom::find($eclass['e_class_room_id']);
                    $success_msg = 'Fee updated successfully!';
                } else {
                    $eclass_room = new EClassRoom;
                    $eclass_room->subject_id = $eclass['subject_id'];
                    $eclass_room->session_id = $eclass['session_id'];
                    $eclass_room->class_id = $eclass['class_id'];
                    $eclass_room->section_id = $eclass['section_id'];
                    $success_msg = 'Fee saved successfully!';
                }
                $eclass_room->teacher_name = $eclass['teacher_name'];
                $eclass_room->meeting_link = $eclass['meeting_link'];
                $eclass_room->meeting_start_time = $eclass['meeting_start_time'];
                $eclass_room->meeting_end_time = $eclass['meeting_end_time'];
                $meeting_days = "";
                if(!empty($eclass['meeting_day'])){
                    $meeting_days = implode(",", array_keys($eclass['meeting_day']));
                }
                $eclass_room->meeting_day = $meeting_days;
                $eclass_room->save();
            }
        } catch (\Exception $e) {
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withInput()->withErrors($error_message);
        }
        DB::commit();
        return redirect('admin/e-class-room')->withSuccess($success_msg);
    }

    public function getClassData(Request $request)
    {
        $session_id = Input::get('session_id');
        $class_id = Input::get('class_id');
        $section_id = Input::get('section_id');
        $result = "";
        $api_obj = new ApiController();
        $arr_subject_data = $api_obj->get_class_section_subject($session_id, $class_id, $section_id);
        if (!empty($arr_subject_data)) {
            foreach ($arr_subject_data as &$subject) {
                $eclass_room = EClassRoom::where([['session_id', '=', $session_id], ['class_id', '=', $class_id], ['section_id', '=', $section_id], ['subject_id', '=', $subject['subject_id']]])->first();
                if (!empty($eclass_room)) {
                    $subject['e_class_room_id'] = $eclass_room['e_class_room_id'];
                    $subject['session_id'] = $eclass_room['session_id'];
                    $subject['class_id'] = $eclass_room['class_id'];
                    $subject['section_id'] = $eclass_room['section_id'];
                    $subject['teacher_name'] = $eclass_room['teacher_name'];
                    $subject['meeting_link'] = $eclass_room['meeting_link'];
                    $subject['meeting_start_time'] = $eclass_room['meeting_start_time'];
                    $subject['meeting_end_time'] = $eclass_room['meeting_end_time'];
                    $subject['meeting_day'] = explode(",",$eclass_room['meeting_day']);
                } else {
                    $subject['e_class_room_id'] = '';
                    $subject['session_id'] = $session_id;
                    $subject['class_id'] = $class_id;
                    $subject['section_id'] = $section_id;
                    $subject['teacher_name'] = null;
                    $subject['meeting_link'] = null;
                    $subject['meeting_start_time'] = null;
                    $subject['meeting_end_time'] = null;
                    $subject['meeting_day'] = [];
                }
            }
            $data['arr_subject_data'] = $arr_subject_data;
            $data['days'] = Config::get('custom.days');
            $result = View::make('backend.e-class-room.subject-data')->with($data)->render();
        }
        return $result;
    }
}
    