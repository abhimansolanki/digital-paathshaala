<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Notice;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;
    use App\CustomClass\ImgCompressor;
    use App\Http\Controllers\CronController;

    class NoticeController extends Controller
    {

        public function __construct()
        {
            
        }

        public function add(Request $request, $id = NULL)
        {
	$notice = [];
	$arr_student = [];
	$arr_employee = [];
	$notice_id = null;
	 $class_id = null;
	    $session_id = null;
	if (!empty($id))
	{
	    $decrypted_notice_id = get_decrypted_value($id, true);
	    $notice = $this->getNoticeData($decrypted_notice_id);
	    $notice = isset($notice[0]) ? $notice[0] : [];
	    if (!$notice)
	    {
	        return redirect('admin/notice')->withError('Notice not found!');
	    }
	    $encrypted_notice_id = get_encrypted_value($notice['notice_id'], true);
	    $save_url = url('admin/notice/save/' . $encrypted_notice_id);
	    $submit_button = 'Update';
	    $notice_id = $decrypted_notice_id;
	    $class_id = $notice['class_id'];
	    $session_id = $notice['session_id'];
	    if (!empty($class_id))
	    {
	        $student_data = get_student_list_by_class_id($class_id);
	        foreach ($student_data as $key => $value)
	        {
		$arr_student[$value['student_id']] = $value['first_name'] . ' ' . $value['middle_name'] . ' ' . $value['last_name'];
	        }
	    }
	    $department_id = $notice['department_id'];
	    if (!empty($department_id))
	    {
	        $employee_data = get_employee_by_department($department_id);
	        foreach ($employee_data as $key => $value)
	        {
		$arr_employee[$value['employee_id']] = $value['full_name'];
	        }
	    }
	} else
	{
	    $save_url = url('admin/notice/save');
	    $submit_button = 'Save';
	}
	$notice['arr_student'] = $arr_student;
	$notice['arr_employee'] = $arr_employee;
	$notice['arr_class'] = add_blank_option(get_all_classes(), '--Select class --');
	$notice['arr_department'] = add_blank_option(get_employee_department(), '--Select department --');
	$notice['arr_notice_for'] = \Config::get('custom.notice_for');
	$notice['arr_section']       = add_blank_option(get_class_section($class_id,$session_id), '--Select section --');
	$session  =  db_current_session();
	$notice['session_start_date'] = $session['session_start_date'];
	$notice['session_end_date'] = $session['session_end_date'];
	$data = array(
	    'save_url' => $save_url,
	    'submit_button' => $submit_button,
	    'notice' => $notice,
	    'redirect_url' => url('admin/notice/'),
	);
	return view('backend.notice.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
	$decrypted_notice_id = get_decrypted_value($id, true);
	if (!empty($id))
	{
	    $notice = Notice::find($decrypted_notice_id);

	    if (!$notice)
	    {
	        return redirect('/admin/notice/')->withError('Notice not found!');
	    }
	    $success_msg = 'Notice updated successfully!';
	} else
	{
	    $notice = New Notice;
	    $success_msg = 'Notice saved successfully!';
	}
	$session  =  db_current_session();
	$session_id = $session['session_id'];
	$all_input = [
//	    'notice_title' => 'required|unique:notices,notice_title,' . $decrypted_notice_id . ',notice_id,session_id,' . $session_id,
	    'notice_title' => 'required',
	    'start_date' => 'required',
	    'end_date' => 'required',
	    'notice_for' => 'required',
	];

	$validatior = Validator::make($request->all(), $all_input);

	if ($validatior->fails())
	{
	    return redirect()->back()->withInput()->withErrors($validatior);
	} else
	{
	    DB::beginTransaction();
	    try
	    {
	        $arr_student_id  =Input::get('student_id');
	        if ($arr_student_id)
	        {
		$notice->student_id = implode(',', $arr_student_id);
	        }
	        if (Input::get('employee_id'))
	        {
		$notice->employee_id = implode(',', Input::get('employee_id'));
	        }
	        $notice_for = Input::get('notice_for');
	        $class_id = Input::get('class_id');
	        $section_id = Input::get('section_id');
	        $notice->start_date = get_formatted_date(Input::get('start_date'), 'database');
	        $notice->end_date = get_formatted_date(Input::get('end_date'), 'database');
	        $notice->notice_title = Input::get('notice_title');
	        $notice->notice_for = $notice_for;
	        $notice->notice = Input::get('notice');
	        $notice->class_id = $class_id;
            $notice->section_id = ($notice_for != 7) ? $section_id : NULL;
	        $notice->department_id = Input::get('department_id');
	        $notice->session_id = $session_id;
	        if ($request->hasFile('notice_image'))
	        {
		$file = $request->file('notice_image');
		$destinationPath = public_path() . '/uploads/notice';
		$filename = $file->getClientOriginalName();
		$rand_string = quick_random();
		$filename = $rand_string . '-' . $filename;
		$file->move($destinationPath, $filename);
		// create object
		$setting = array(
		    'directory' => $destinationPath, // directory file compressed output
		    'file_type' => array(// file format allowed
		        'image/jpeg',
		        'image/jpg',
		        'image/png',
		        'image/gif'
		    )
		);

		$ImgCompressor = new ImgCompressor($setting);
		// run('STRING original file path', 'output file type', INTEGER Compression level: from 0 (no compression) to 9);
		$comp_file = $ImgCompressor->run($destinationPath . '/' . $filename, 'jpg', 5); // example level = 2 same quality 80%, level = 7 same quality 30% etc
		if ($comp_file['status'] == 'success')
		{
		    unlink($destinationPath . '/' . $filename);
		    $notice->notice_image = $comp_file['data']['compressed']['name'];
		}
	        }
	        $notice->save();
	    } catch (\Exception $e)
	    {
	        //failed logic here
	        DB::rollback();
	        $error_message = $e->getMessage();
	        return redirect()->back()->withErrors($error_message);
	    }
	    DB::commit();
	    if(!empty($session_id) && $id == null){
		$cron = new CronController;
		$cron->pushNotificationNotice($notice,$notice_for,$arr_student_id,$class_id, $section_id,$session_id);
	        }
	}

	return redirect('admin/notice')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
	$notice_id = Input::get('notice_id');
	$notice = Notice::find($notice_id);
	if ($notice)
	{

	    DB::beginTransaction(); //Start transaction!
	    try
	    {
	        $notice->delete();
	        $return_arr = array(
		'status' => 'success',
		'message' => 'Notice deleted successfully!'
	        );
	    } catch (\Exception $e)
	    {
	        //failed logic here
	        DB::rollback();
	        $error_message = $e->getMessage();
	        $return_arr = array(
		'status' => 'used',
		'message' => trans('language.delete_message')
	        );
	    }
	    DB::commit();
	} else
	{
	    $return_arr = array(
	        'status' => 'error',
	        'message' => 'Notice not found!'
	    );
	}
	return response()->json($return_arr);
        }

        public function anyData()
        {
	$notice = [];
	$notice_id = array();
	$offset = Input::get('start');
	$limit = Input::get('length');
	$arr_notice = $this->getNoticeData($notice_id, $offset, $limit);
	foreach ($arr_notice as $key => $notice_data)
	{
	    $notice[$key] = (object) $notice_data;
	}
	return Datatables::of($notice)
		    ->addColumn('action', function ($notice)
		    {
		        $encrypted_notice_id = get_encrypted_value($notice->notice_id, true);
		        return '<a title="Edit" id="deletebtn1" href="' . url('admin/notice/' . $encrypted_notice_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
			    . '<button type = "button" title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $notice->notice_id . '"><i class="fa fa-trash"></i></button>';
		    })
		    ->addColumn('notice_image', function ($notice)
		    {
		        $return = '';
		        if (!empty($notice->notice_image))
		        {
			$return = '<img src="' . url($notice->notice_image) . '" style="width:50px;">';
		        }
		        return $return;
		    })->rawColumns(['action', 'notice_image'])
		    ->make(true);
        }

        public function getNoticeData($notice_id = null, $offset = null, $limit = null)
        {
	$notice_return = [];
	$session_id = null;
	$session = get_current_session();
	if (!empty($session))
	{
	    $session_id = $session['session_id'];
	    $arr_notice_data = Notice::where(function($query) use ($notice_id)
		{
		    if (!empty($notice_id))
		    {
		        $query->where('notice_id', $notice_id);
		    }
		})->where(function($query) use ($session_id)
		{
		    if (!empty($session_id))
		    {
		        $query->where('session_id', $session_id);
		    }
		})
//                        ->where(function($query) use ($limit, $offset)
//                    {
//                        if (!empty($limit))
//                        {
//                            $query->skip($offset);
//                            $query->take($limit);
//                        }
//                    })
		->get();

	    if (!empty($arr_notice_data))
	    {
	        $arr_notice_for = \Config::get('custom.notice_for');
	        foreach ($arr_notice_data as $key => $notice_data)
	        {
		$notice = array(
		    'notice_id' => $notice_data['notice_id'],
		    'session_id' => $notice_data['session_id'],
		    'notice' => $notice_data['notice'],
		    'class_id' => $notice_data['class_id'],
		    'department_id' => $notice_data['department_id'],
		    'notice_for' => $notice_data['notice_for'],
		    'start_date' => get_formatted_date($notice_data['start_date'], 'display'),
		    'end_date' => get_formatted_date($notice_data['end_date'], 'display'),
		    'student_id' => explode(',', $notice_data['student_id']),
		    'employee_id' => explode(',', $notice_data['employee_id']),
		    'notice_title' => $notice_data['notice_title'],
		    'notice_for_name' => $arr_notice_for[$notice_data['notice_for']],
		    'notice_image' => check_file_exist($notice_data['notice_image'], 'notice'),
		);
		$notice_return[] = $notice;
	        }
	    }
	}
//            p($notice_return);
	return $notice_return;
        }

    }
    