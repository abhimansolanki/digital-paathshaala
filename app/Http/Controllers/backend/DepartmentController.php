<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Department;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class DepartmentController extends Controller
    {

        public function __construct()
        {
            
        }

        public function add(Request $request, $id = NULL)
        {
            $department    = [];
            $department_id = null;
            if (!empty($id))
            {
                $decrypted_department_id = get_decrypted_value($id, true);
                $department              = $this->getDepartmentData($decrypted_department_id);
                $department              = isset($department[0]) ? $department[0] : [];
                $department              = (object) $department;
                if (!$department)
                {
                    return redirect('admin/department')->withError('Department not found!');
                }
                $encrypted_department_id = get_encrypted_value($department->department_id, true);
                $save_url                = url('admin/department/save/' . $encrypted_department_id);
                $submit_button           = 'Update';
                $department_id           = $decrypted_department_id;
            }
            else
            {
                $save_url      = url('admin/department/save');
                $submit_button = 'Save';
            }

            $data = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'department'    => $department,
                'redirect_url'  => url('admin/department/'),
            );
            return view('backend.department.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_department_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $department = Department::find($decrypted_department_id);

                if (!$department)
                {
                    return redirect('/admin/department/')->withError('Department not found!');
                }
                $success_msg = 'Department updated successfully!';
            }
            else
            {
                $department  = New Department;
                $success_msg = 'Department saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'department_name' => 'required|unique:departments,department_name,' . $decrypted_department_id . ',department_id',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction(); //Start transaction!

                try
                {
                    $department->department_name = Input::get('department_name');
                    $department->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withInput()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/department')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $department_id = Input::get('department_id');
            $department    = Department::find($department_id);
            if ($department)
            {
                 DB::beginTransaction(); //Start transaction!
                try
                {
                    $department->delete();
                $return_arr = array(
                    'status'  => 'success',
                    'message' => 'Department deleted successfully!'
                );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
               
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Department not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $department     = [];
            $department_id = null;
            $offset    = Input::get('start');
            $limit     = Input::get('length');
            $arr_department = $this->getDepartmentData($department_id,$offset,$limit);
            foreach ($arr_department as $key => $department_data)
            {
                $department[$key] = (object) $department_data;
            }
            return Datatables::of($department)
                    ->addColumn('action', function ($department)
                    {
                        $encrypted_department_id = get_encrypted_value($department->department_id, true);
                        return '<a title="Edit" id="deletebtn1" href="' . url('admin/department/'.$encrypted_department_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $department->department_id . '"><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

        public function getDepartmentData($department_id = null, $offset = null, $limit = null)
        {
            $department_return   = [];
            $arr_department_data = Department::where(function($query) use ($department_id)
                {
                    if (!empty($department_id))
                    {
                        $query->where('department_id', $department_id);
                    }
                })
//                 ->where(function($query) use ($limit, $offset)
//                    {
//                        if (!empty($limit))
//                        {
//                            $query->skip($offset);
//                            $query->take($limit);
//                        }
//                    })
                    ->get();
            if (!empty($arr_department_data))
            {
                foreach ($arr_department_data as $key => $department_data)
                {
                    $department_return[] = array(
                        'department_id'   => $department_data['department_id'],
                        'department_name' => $department_data['department_name'],
                    );
                }
            }
            return $department_return;
        }

    }
    