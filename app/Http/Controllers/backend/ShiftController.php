<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Shift;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class ShiftController extends Controller
    {

        public function __construct()
        {
            
        }

        public function add(Request $request, $id = NULL)
        {
            $shift    = [];
            $shift_id = null;
            if (!empty($id))
            {
                $decrypted_shift_id = get_decrypted_value($id, true);
                $shift              = $this->getShiftData($decrypted_shift_id);
                $shift              = isset($shift[0]) ? $shift[0] : [];
                if (!$shift)
                {
                    return redirect('admin/shift')->withError('Shift not found!');
                }
                $encrypted_shift_id = get_encrypted_value($shift['shift_id'], true);
                $save_url           = url('admin/shift/save/' . $encrypted_shift_id);
                $submit_button      = 'Update';
                $shift_id           = $decrypted_shift_id;
            }
            else
            {
                $save_url      = url('admin/shift/save');
                $submit_button = 'Save';
            }

            $data = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'shift'         => $shift,
                'redirect_url'  => url('admin/shift/'),
            );
            return view('backend.shift.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_shift_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $shift = Shift::find($decrypted_shift_id);

                if (!$shift)
                {
                    return redirect('/admin/shift/')->withError('Shift not found!');
                }
                $success_msg = 'Shift updated successfully!';
            }
            else
            {
                $shift       = New Shift;
                $success_msg = 'Shift saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'effective_date' => 'required',
                    'shift_name'     => 'required|unique:shifts,shift_name,' . $decrypted_shift_id . ',shift_id',
                    'login_time'     => 'required',
                    'logout_time'    => 'required',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction(); //Start transaction!

                try
                {
                    $shift->shift_name     = Input::get('shift_name');
                    $shift->effective_date = get_formatted_date(Input::get('effective_date'), 'database');
                    $shift->description    = Input::get('description');
                    $shift->login_time     = Input::get('login_time');
                    $shift->logout_time    = Input::get('logout_time');
                    $shift->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withInput()->withErrors($error_message);
                }
                DB::commit();
            }

            return redirect('admin/shift')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $shift_id = Input::get('shift_id');
            $shift    = Shift::find($shift_id);
            if ($shift)
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $shift->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Shift deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Shift not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $shift     = [];
            $offset    = Input::get('start');
            $limit     = Input::get('length');
            $shift_id  = null;
            $arr_shift = $this->getShiftData($shift_id, $offset, $limit);
            foreach ($arr_shift as $key => $shift_data)
            {
                $shift[$key] = (object) $shift_data;
            }
            return Datatables::of($shift)
                    ->addColumn('action', function ($shift)
                    {
                        $encrypted_shift_id = get_encrypted_value($shift->shift_id, true);
                        return '<a title="Edit" id="deletebtn1" href="' . url('admin/shift/' . $encrypted_shift_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $shift->shift_id . '"><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

        public function getShiftData($shift_id = null, $offset = null, $limit = null)
        {
            $shift_return   = [];
            $arr_shift_data = Shift::where(function($query) use ($shift_id)
                    {
                        if (!empty($shift_id))
                        {
                            $query->where('shift_id', $shift_id);
                        }
                    })
//                    ->where(function($query) use ($limit, $offset)
//                    {
//                        if (!empty($limit))
//                        {
//                            $query->skip($offset);
//                            $query->take($limit);
//                        }
//                    })
                        ->get();
            if (!empty($arr_shift_data))
            {
                foreach ($arr_shift_data as $key => $shift_data)
                {
                    $shift_return[] = array(
                        'shift_id'       => $shift_data['shift_id'],
                        'shift_name'     => $shift_data['shift_name'],
                        'effective_date' => get_formatted_date($shift_data['effective_date'], 'display'),
                        'description'    => $shift_data['description'],
                        'login_time'     => $shift_data['login_time'],
                        'logout_time'    => $shift_data['logout_time'],
                    );
                }
            }
            return $shift_return;
        }

    }
    