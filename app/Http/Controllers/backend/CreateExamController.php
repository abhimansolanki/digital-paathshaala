<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\CreateExam;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Input;
use Datatables;
use Illuminate\Support\Facades\DB;

class CreateExamController extends Controller
{

    public function __construct()
    {

    }

    public function index(Request $request, $id = NULL)
    {
        $data = array(
            'redirect_url' => url('admin/create-exam/'),
        );
        return view('backend.create-exam.index')->with($data);
    }

    public function add(Request $request, $id = NULL)
    {
        $exam_id = null;
        if (!empty($id)) {
            $decrypted_exam_id = get_decrypted_value($id, true);
            $exam = $this->getExamData($decrypted_exam_id);
            $exam = isset($exam[0]) ? $exam[0] : [];
            if (!$exam) {
                return redirect('admin/create-exam')->withError('Create Exam not found!');
            }
            $encrypted_exam_id = get_encrypted_value($exam['exam_id'], true);
            $save_url = url('admin/create-exam/save/' . $encrypted_exam_id);
            $submit_button = 'Update';
            $exam_id = $decrypted_exam_id;
        } else {
            $save_url = url('admin/create-exam/save');
            $submit_button = 'Save';
            $current = db_current_session();
            $exam['session_id'] = $current['session_id'];
        }

        $exam['arr_exam_nature'] = \Config::get('custom.arr_exam_nature');
        $exam['arr_status'] = \Config::get('custom.status');
        $exam['arr_exam_category'] = add_blank_option(get_all_exam_category(), '-- Select category --');
        $exam['exam_type'] = \Config::get('custom.exam_type');
        $arr_session = get_current_next_session();
        $exam['arr_session'] = add_blank_option($arr_session, '-- Academic year --');
        $data = array(
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'exam' => $exam,
            'redirect_url' => url('admin/create-exam/'),
        );
        return view('backend.create-exam.add')->with($data);
    }

    public function save(Request $request, $id = NULL)
    {
        $decrypted_exam_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $exam = CreateExam::find($decrypted_exam_id);

            if (!$exam) {
                return redirect('/admin/create-exam/')->withError('CreateExam not found!');
            }
            $success_msg = 'Create Exam updated successfully!';
        } else {
            $exam = New CreateExam;
            $success_msg = 'Create Exam saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
//            'exam_name' => 'required|unique:create_exams,exam_name,' . $decrypted_exam_id . ',exam_id',
            'session_id' => 'required',
            'exam_name' => ['required',Rule::unique('create_exams')->where(function ($query) use ($request) {
                return $query->where('session_id', $request->session_id);
            })->ignore($decrypted_exam_id,'exam_id')],
        ]);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction(); //Start transaction!
            try {

                $exam->exam_name = Input::get('exam_name');
                $exam->exam_alias = Input::get('exam_alias');
                $exam->in_final_marksheet = $request->has(('in_final_marksheet')) ? 1 : 0;
                $exam->session_id = Input::get('session_id');
                $exam->exam_category_id = Input::get('exam_category_id');
                $exam->exam_nature = Input::get('exam_nature');
                $exam->sequence = Input::get('sequence');
                $exam->save();
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withInput()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin/create-exam')->withSuccess($success_msg);
    }

    public function destroy(Request $request)
    {
        $exam_id = Input::get('exam_id');
        $exam = CreateExam::find($exam_id);
        if ($exam) {
            DB::beginTransaction(); //Start transaction!
            try {
                $exam->delete();
                $return_arr = array(
                    'status' => 'success',
                    'message' => 'Exam deleted successfully!'
                );
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                $return_arr = array(
                    'status' => 'used',
                    'message' => trans('language.delete_message')
                );
            }
            DB::commit();
        } else {
            $return_arr = array(
                'status' => 'error',
                'message' => 'Exam not found!'
            );
        }
        return response()->json($return_arr);
    }

    public function classSectionExam(Request $request)
    {
        $session_id = $request->input('session_id');
        $class_id = $request->input('class_id');
        $section_id = $request->input('section_id');
        $return_arr = get_all_exam(null, $class_id, $session_id, $section_id);
        return response()->json($return_arr);
    }

    public function anyData()
    {
        $arr_exam = [];
        $limit = Input::get('length');
        $offset = Input::get('start');
        $exam_return = [];
        $arr_nature = \Config::get('custom.arr_exam_nature');
        $session = get_current_session();
        $arr_exam_data = CreateExam::with('examCategory')
            ->with(['Examclass' => function ($query) use ($session) {
            }])
            ->where('session_id', $session['session_id'])
            ->get();
//		p($arr_exam_data);
        $arr_exam_type = get_all_exam_type();
        foreach ($arr_exam_data as $key => $exam_data) {
            $arr_data = array(
                'exam_id' => $exam_data['exam_id'],
                'exam_name' => $exam_data['exam_name'],
                'exam_category' => $exam_data['examCategory']['exam_category'],
                'exam_nature_name' => $arr_nature[$exam_data['exam_nature']],
                'sequence' => $exam_data['sequence'],
            );
            $arr_exam_class = $exam_data['Examclass'];
            foreach ($exam_data['Examclass'] as $key => $exam_class_data) {
//	        
//                    $exam_type = '';
//	        $arr_exam_type_id =json_decode($exam_class_data['exam_type_id'],true);
//	        foreach ($arr_exam_type_id as $key => $exam_type_id)
//	        {
//		if (isset($arr_exam_type[$exam_type_id]))
//		{
//		    $exam_type .= $arr_exam_type[$exam_type_id] . ', ';
//		}
//	        }
//	        p($exam_class_data);
                $encrypted_exam_class_id = get_encrypted_value($exam_class_data['exam_class_id'], true);
                $arr_data['exam_class_data'][] = array(
                    'class_name' => $exam_class_data['examClasses']['class_name'],
                    'section_name' => $exam_class_data['examSection']['section_name'],
//		'exam_type' => $exam_type,
                    'exam_class_id' => $exam_class_data['exam_class_id'],
                    'url' => url('admin/exam-class/' . $arr_data['exam_id'] . '/' . $encrypted_exam_class_id),
                );
            }

            $arr_exam[] = (object)$arr_data;
        }
//	p($arr_exam);
        return Datatables::of($arr_exam)
            ->addColumn('action', function ($arr_exam) {
                $encrypted_exam_id = get_encrypted_value($arr_exam->exam_id, true);

                return '<a title="Edit" id="deletebtn1" href="' . url('admin/create-exam/add/' . $encrypted_exam_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                    . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $arr_exam->exam_id . '"><i class="fa fa-trash"></i></button>'
                    . '<a title="Link with class" id="deletebtn1" href="' . url('admin/exam-class/' . $arr_exam->exam_id . '/' . null) . '" class="btn btn-success"><i class="fa fa-link" ></i></a>';
            })
            ->rawColumns(['sub_action', 'action'])->make(true);
    }

    public function getExamData($exam_id = null, $offset = null, $limit = null)
    {
        $exam_return = [];
        $arr_exam_data = CreateExam::where(function ($query) use ($exam_id) {
            if (!empty($exam_id)) {
                $query->where('exam_id', $exam_id);
            }
        })->with('examCategory')
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
            ->get();

        if (!empty($arr_exam_data)) {
            $exam = [];

            foreach ($arr_exam_data as $key => $exam_data) {
                $exam = array(
                    'exam_id' => $exam_data['exam_id'],
                    'session_id' => $exam_data['session_id'],
                    'in_final_marksheet' => $exam_data['in_final_marksheet'],
                    'exam_category_id' => $exam_data['exam_category_id'],
                    'exam_name' => $exam_data['exam_name'],
                    'exam_alias' => $exam_data['exam_alias'],
                    'exam_nature' => $exam_data['exam_nature'],
                    'sequence' => $exam_data['sequence'],
                    'exam_category' => $exam_data['examCategory']['exam_category'],
                );
                $exam_return[] = $exam;
            }
        }
        return $exam_return;
    }

}
    