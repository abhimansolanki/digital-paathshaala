<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\CronController;
use App\Model\backend\FeeCircular;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\StudentParent;
use App\Model\backend\StudentFeeReceipt;
use App\Model\backend\StudentFeeReceiptDetail;

//    use App\Model\backend\StudentVehicleAssignedDetail;
use App\Model\backend\TransportFee;
use App\Model\backend\TransportFeeDetail;
use App\Model\backend\Vehicle;
use App\Model\backend\Fee;
use App\Model\backend\Student;
use Validator;
use Illuminate\Support\Facades\Input;
use Datatables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View as View;
use Carbon\Carbon;
use DateTime;
use DateInterval;
use DatePeriod;
use Illuminate\Support\Arr;

class StudentFeeReceiptController extends Controller
{

    public function index()
    {

        $data = array(
            'arr_class' => add_blank_option(get_all_classes(), '-- Select class --'),
            'arr_student' => add_blank_option([], '-- Select student --'),
            'redirect_url' => 'admin/student-fee-receipt/add',
            'arr_payment_status' => add_blank_option(arr_payment_status(), '-- Payment status --'),
        );
        return view('backend.student-fee-receipt.index')->with($data);
    }

    //get fee form
    public function add(Request $request, $id = NULL)
    {

        $fee_receipt = [];
        $data = [];
        if (!empty($id)) {
            $decrypted_fee_id = get_decrypted_value($id, true);
            $fee_receipt = StudentFeeReceipt::Find($decrypted_fee_id);
            if (!$fee_receipt) {
                return redirect('admin/fee-parameter')->withError('StudentFeeReceipt Parameter not found!');
            }
            $encrypted_fee_id = get_encrypted_value($fee_receipt['fee_id'], true);
            $save_url = url('admin/student-fee-receipt/save/' . $encrypted_fee_id);
            $submit_button = 'Update';
        } else {
            $save_url = url('admin/student-fee-receipt/save');
            $submit_button = 'Save';
        }
        $arr_fee_apply_to = \Config::get('custom.fee_apply_to');
        $arr_is_special_case = \Config::get('custom.is_special_case');
        $fee_receipt['arr_fee_apply_to'] = $arr_fee_apply_to;
        $fee_receipt['arr_is_special_case'] = $arr_is_special_case;
        $fee_receipt['arr_session'] = get_current_session();
        if (!empty($fee_receipt['arr_session'])) {
            $fee_receipt['start_date'] = get_formatted_date($fee_receipt['arr_session']['start_date'], 'display');
            $fee_receipt['end_date'] = get_formatted_date($fee_receipt['arr_session']['end_date'], 'display');
        }
        $arr_class = get_all_classes();
        $arr_fee_type = get_fee_type();
        $arr_fee_circular = get_fee_circular();
        $arr_bank = get_bank();
        $arr_payment_mode = get_payment_mode();
        $fee_receipt['arr_class'] = add_blank_option($arr_class, '--Select class --');
        $fee_receipt['arr_fee_type'] = add_blank_option($arr_fee_type, '--Select fee type --');
        $fee_receipt['arr_fee_circular'] = add_blank_option($arr_fee_circular, '--Select fee circular --');
        $fee_receipt['arr_bank'] = add_blank_option($arr_bank, '--Select bank --');
        $fee_receipt['arr_payment_mode'] = add_blank_option($arr_payment_mode, '--Select mode --');
        $fee_receipt['arr_student'] = add_blank_option(array(), '--Select student --');

        $data = array(
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'fee_receipt' => $fee_receipt,
            'redirect_url' => url('admin/student-fee-receipt'),
        );
        return view('backend.student-fee-receipt.add')->with($data);
    }

    public function save(Request $request, $id = NULL)
    {
        $validatior = Validator::make($request->all(), [
        ]);
        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            $student_receipt_print = [];
            $paid_receipt = '';
            DB::beginTransaction(); //Start transaction!
            try {
                $session = get_current_session();
                $arr_student = [];
                $session_id = $session['session_id'];
                $receipt_number = 1;
                if ($request->has('checked_student')) {
                    $arr_student = Input::get('checked_student');
                } else {
                    $arr_student[0] = Input::get('selected_student_id');
                }

                $st_id = null; // student id form
                $arr_total_transport_fee = [];
                $arr_total_academic_fee = [];
                $payment_mode_id = Input::get('payment_mode_id');
                if (!empty($arr_student)) {
                    foreach ($arr_student as $student_id) {
                        $st_id = $student_id;
                        $arr_receipt['paid_fee_receipt_id'] = null;
                        if ($request->has('paid_amount_' . $student_id) && array_sum(Input::get('paid_amount_' . $student_id)) > 0) {

                            $fee_receipt_number = StudentFeeReceipt::orderBy('student_fee_receipt_id', 'desc')->first();
                            if (!empty($fee_receipt_number->receipt_number)) {
                                $last_receipt_number = $fee_receipt_number->receipt_number;
                                $receipt_number = $last_receipt_number + 1;
                            }
                            $student_fee_receipt = new StudentFeeReceipt;
                            $student_fee_receipt->student_id = $student_id;
                            $student_fee_receipt->class_id = Input::get('student_class_id_' . $student_id);
                            $student_fee_receipt->session_id = $session_id;
                            $student_fee_receipt->receipt_number = $receipt_number;
                            $student_fee_receipt->receipt_date = get_formatted_date(Input::get('receipt_date'), 'database');
                            $student_fee_receipt->is_receipt_print = $request->has('is_receipt_print') ? 1 : 0;
                            $student_fee_receipt->is_sms_send = $request->has('is_sms_send') ? 1 : 0;
                            $student_fee_receipt->payment_mode_id = $payment_mode_id;
                            $student_fee_receipt->bank_id = Input::get('bank_id');
                            $student_fee_receipt->cheque_number = Input::get('cheque_number');

                            if (!empty(Input::get('cheque_date'))) {
                                $student_fee_receipt->cheque_date = get_formatted_date(Input::get('cheque_date'), 'database');
                            }
                            if (!empty(Input::get('transaction_date'))) {
                                $student_fee_receipt->transaction_date = get_formatted_date(Input::get('transaction_date'), 'database');
                            }
                            $student_fee_receipt->transaction_id = Input::get('transaction_id');
                            $disount_amount = 0;
                            if ($request->has('discount_apply')) {
                                $disount_amount = Input::get('discount_amount_' . $student_id);
                                $student_fee_receipt->discount_amount = $disount_amount;
                            }
                            $fine_amount = 0;
                            if ($request->has('fine_apply')) {
                                $fine_amount = Input::get('fine_amount_' . $student_id);
                                $student_fee_receipt->fine_amount = $fine_amount;
                            }

                            $student_paid_amount = Input::get('paid_amount_' . $student_id);
                            $total_amount = array_sum($student_paid_amount);
                            $student_fee_receipt->total_amount = $total_amount;
                            $student_fee_receipt->net_amount = ($total_amount + $fine_amount) - $disount_amount;
                            $arr_total_academic_fee[] = $student_fee_receipt->net_amount;
                            if ($student_fee_receipt->save()) {
                                $arr_receipt['paid_fee_receipt_id'] = $student_fee_receipt->student_fee_receipt_id;
                                $actual_fee_id = Input::get('fee_id_' . $student_id);
                                $student_fee_amount = Input::get('fee_amount_' . $student_id);
                                $student_fee_type = Input::get('fee_type_id_' . $student_id);
                                $student_fee_circular = Input::get('fee_circular_id_' . $student_id);
                                $fee_circular_name = Input::get('fee_circular_name_' . $student_id);
                                $arr_fee_installments = [];
                                if (!empty($student_paid_amount)) {
                                    foreach ($student_paid_amount as $key => $value) {
                                        if (!empty($value)) {
                                            $installment_payment_status = '';
                                            if ($value < $student_fee_amount[$key]) {
                                                $installment_payment_status = 0;
                                            } elseif ($value == $student_fee_amount[$key]) {
                                                $installment_payment_status = 1;
                                            } elseif ($value > $student_fee_amount[$key]) {
                                                $installment_payment_status = 2;
                                            }
                                            $arr_fee_id = explode('_', $actual_fee_id[$key]);
                                            $fee_id = $arr_fee_id[0];
                                            $fee_detail_id = $arr_fee_id[1];
                                            $arr_fee_installments[] = array(
                                                'fee_id' => $fee_id,
                                                'fee_detail_id' => $fee_detail_id,
                                                'installment_paid_amount' => $value,
                                                'installment_amount' => $student_fee_amount[$key],
                                                'fee_circular_name' => $fee_circular_name[$key],
                                                'fee_type_id' => $student_fee_type[$key],
                                                'fee_circular_id' => $student_fee_circular[$key],
                                                'installment_payment_status' => $installment_payment_status,
                                            );
                                        }
                                    }
                                }
                                if (!empty($arr_fee_installments)) {
                                    foreach ($arr_fee_installments as $key => $value) {
                                        if ($value['fee_type_id'] == 2) {
                                            $student_previous_due = $value['installment_amount'] - $value['installment_paid_amount'];
                                            $update_student = Student::find($student_id);
                                            $update_student->previous_due_fee = $student_previous_due > 0 ? $student_previous_due : 0;
                                            $update_student->save();
                                        }
                                        $student_fee_receipt_detail = new StudentFeeReceiptDetail;
                                        $student_fee_receipt_detail->fee_id = $value['fee_id'];
                                        $student_fee_receipt_detail->fee_detail_id = $value['fee_detail_id'];
                                        $student_fee_receipt_detail->installment_paid_amount = $value['installment_paid_amount'];
                                        $student_fee_receipt_detail->installment_amount = $value['installment_amount'];
                                        $student_fee_receipt_detail->fee_circular_name = $value['fee_circular_name'];
                                        $student_fee_receipt_detail->fee_type_id = $value['fee_type_id'];
                                        $student_fee_receipt_detail->fee_circular_id = $value['fee_circular_id'];
                                        $student_fee_receipt_detail->installment_payment_status = $value['installment_payment_status'];
                                        $student_fee_receipt->feeReceiptDetails()->save($student_fee_receipt_detail);
                                    }
                                }
                            }
                        }
                        // transport fee
                        $arr_receipt['paid_transport_fee_id'] = null;
                        if ($request->has('transport_fee_amount_' . $student_id) && array_sum(Input::get('transport_fee_amount_' . $student_id)) > 0) {
                            $receipt_number = 1;
                            $arr_transport_amount = Input::get('transport_fee_amount_' . $student_id);
                            $class_id = Input::get('student_class_id_' . $student_id);
                            $total_paid_amount = array_sum($arr_transport_amount);

                            $transport_receipt_number = TransportFee::orderBy('transport_fee_id', 'desc')->first();
                            if (!empty($transport_receipt_number->receipt_number)) {
                                $last_receipt_number = $transport_receipt_number->receipt_number;
                                $receipt_number = $last_receipt_number + 1;
                            }
                            $student_transport_fee = new TransportFee;
                            $student_transport_fee->student_id = $student_id;
                            $student_transport_fee->class_id = $class_id;
                            $student_transport_fee->receipt_number = $receipt_number;
                            $student_transport_fee->receipt_date = get_formatted_date(Input::get('receipt_date'), 'database');
                            $student_transport_fee->session_id = $session_id;
                            $student_transport_fee->payment_mode_id = $payment_mode_id;
                            $student_transport_fee->total_paid_amount = $total_paid_amount;
                            //		        $student_transport_fee->student_vehicle_assigned_detail_id = Input::get('assigned_detail_id_' . $student_id);
                            $arr_total_transport_fee[] = $student_transport_fee->total_paid_amount;
                            if ($student_transport_fee->save()) {
                                $arr_receipt['paid_transport_fee_id'] = $student_transport_fee->transport_fee_id;
                                $arr_transport_fee_original = Input::get('transport_fee_original_' . $student_id);
                                $arr_month_id = Input::get('month_id_' . $student_id);
                                $arr_transport_detail = [];
                                foreach ($arr_transport_amount as $key => $transport_amount) {
                                    $transport_detail = [];
                                    if (!empty($transport_amount)) {
                                        $amount_status = 0;
                                        if ($transport_amount < $arr_transport_fee_original[$key]) {
                                            $amount_status = 2;
                                        } elseif ($transport_amount == $arr_transport_fee_original[$key]) {
                                            $amount_status = 1;
                                        }
                                        $transport_detail = array(
                                            'month_id' => $arr_month_id[$key],
                                            'fee_amount' => $arr_transport_fee_original[$key],
                                            'paid_fee_amount' => $transport_amount,
                                            'amount_status' => $amount_status,
                                        );
                                        $arr_transport_detail[] = $transport_detail;
                                    }
                                }

                                if (!empty($arr_transport_detail)) {
                                    foreach ($arr_transport_detail as $key => $data_transport_detail) {
                                        $transport_fee_detail = new TransportFeeDetail;
                                        $transport_fee_detail->month_id = $data_transport_detail['month_id'];
                                        $transport_fee_detail->fee_amount = $data_transport_detail['fee_amount'];
                                        $transport_fee_detail->paid_fee_amount = $data_transport_detail['paid_fee_amount'];
                                        $transport_fee_detail->amount_status = $data_transport_detail['amount_status'];
                                        $student_transport_fee->transportFeeDetail()->save($transport_fee_detail);
                                    }
                                }
                            }
                        }
                        $arr_receipt['student_id'] = $student_id;
                        $student_receipt_print[$student_id] = $arr_receipt;
                    }
                }
                $status = 'success';
                $message = trans('language.fee_success_message');
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                $message = trans('language.fee_failed_message');
                p($error_message);
                //return redirect()->back()->withErrors($error_message);
                $status = 'failed';
            }

            DB::commit();
            if ($request->has('is_sms_send') && !empty($st_id)) {
                $arr_parent_contact = DB::table('students as s')
                    ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
                    ->where('s.student_id', $st_id)
                    ->select('sp.father_contact_number', 'sp.mother_contact_number', 'sp.guardian_contact_number')
                    ->first();
                if ($arr_parent_contact->father_contact_number) {
                    $mobile_number = $arr_parent_contact->father_contact_number;
                }
                if ($arr_parent_contact->mother_contact_number) {
                    $mobile_number = $mobile_number . ',' . $arr_parent_contact->mother_contact_number;
                }
                if ($arr_parent_contact->guardian_contact_number) {
                    $mobile_number = $mobile_number . ',' . $arr_parent_contact->guardian_contact_number;
                }
                $academic_amount = array_sum($arr_total_transport_fee);
                $transport_amount = array_sum($arr_total_academic_fee);
                $tot_amount_msg = $academic_amount + $transport_amount;
                $fee_message = 'Dear Parent, we received ' . $tot_amount_msg;
                $paid_sms = send_sms($mobile_number, $fee_message);
            }
            if (!empty($student_receipt_print) && $request->has('is_receipt_print')) {
                $paid_receipt = $this->printPaidFeeReceipt($student_receipt_print);
            }
        }
        $arr_return = array(
            'arr_student' => $arr_student,
            'paid_receipt' => $paid_receipt,
        );
        return response()->json(array('status' => $status, 'data' => $arr_return, 'message' => $message));
    }

    // fee view list
    public function anyData()
    {
        $student = [];
        $class_id = Input::get('class_id');
        $student_id = Input::get('student_id');
        $payment_status = Input::get('payment_status');
        $limit = Input::get('length');
        $offset = Input::get('start');
        $session = get_current_session();
        $session_id = $session['session_id'];
        if (!empty($session_id)) {

            $table = get_student_table($session_id);
            $query = DB::table('student_fee_receipts as sfr')
                ->join('student_fee_receipt_details as sfrd', 'sfrd.student_fee_receipt_id', '=', 'sfr.student_fee_receipt_id')
                ->join($table . ' as s', 'sfr.student_id', '=', 's.student_id')
                ->join('classes as c', 's.current_class_id', '=', 'c.class_id')
                ->join('sections as sec', 's.current_section_id', '=', 'sec.section_id')
                ->groupBy('sfr.student_id')
                ->select('sfr.student_id', 's.first_name', 's.middle_name', 's.last_name', 's.enrollment_number', 'c.class_name', 'sec.section_name', 's.student_type_id', 's.new_student', 's.current_session_id', 's.current_class_id',
                    //                    DB::raw('SUM(sfrd.installment_amount) as fee_amount'),
                    DB::raw('SUM(sfrd.installment_paid_amount) as paid_amount'))
                ->where('sfr.session_id', $session_id);
            if (!empty($class_id)) {
                $query->where('sfr.class_id', $class_id);
            }
            $arr_student_fee = $query->get();
            $arr_student_fee = json_decode($arr_student_fee, true);
            //                p($arr_student_fee);
            $count = 1;
            foreach ($arr_student_fee as $key => $student_fee) {
                $arr_condition = array(
                    'session_id' => $student_fee['current_session_id'],
                    'class_id' => $student_fee['current_class_id'],
                    'student_type_id' => $student_fee['student_type_id'],
                    'new_student' => $student_fee['new_student'],
                );
                $student_total_fee = get_student_academic_total_fee($arr_condition);
                $pending_amount = $student_total_fee - $student_fee['paid_amount'];
                $pending_amount = $pending_amount > 0 ? $pending_amount : 0;
                $arr_fee_data = array(
                    'student_id' => $student_fee['student_id'],
                    'total_amount' => $student_fee['first_name'],
                    'enrollment_number' => $student_fee['enrollment_number'],
                    'student_name' => $student_fee['first_name'],
                    'class_name' => $student_fee['class_name'],
                    'section_name' => $student_fee['section_name'],
                    'paid_amount' => $student_fee['paid_amount'],
                    'total_amount' => $student_total_fee,
                    'pending_amount' => $pending_amount,
                    'care_of_name' => '',
                    'care_of_contact' => '',
                    'class_name' => $student_fee['class_name'],
                    'payment_status' => ''
                );
                $student[$key] = (object)$arr_fee_data;
                $count++;
            }
        }

        return Datatables::of($student)
            ->addIndexColumn()
            ->addColumn('pay', function ($student) {
                $encrypted_student_id = get_encrypted_value($student->student_id, true);
                return '<a title="Edit" id="deletebtn1" href="' . url('admin/student/add/' . $encrypted_student_id) . '" class="btn btn-success centerAlign"><i class="fa fa-edit" ></i></a>';
            })
            ->rawColumns(['pay'])
            ->make(true);
    }

    function printPaidFeeReceipt($student_receipt_print)
    {
        $arr_student_id = array_column($student_receipt_print, 'student_id');
        $arr_student = [];
        $student_return_data = [];
        $status = 'failed';
        if (!empty($arr_student_id)) {
            /** get Student all data */
            $arr_student = get_student_data($arr_student_id);
            if (!empty($arr_student)) {
                $session = get_current_session();
                foreach ($arr_student as $key => $student) {
                    $fee_data = [];
                    $student_id = $student['student_id'];
                    $class_id = $student['current_class_id'];
                    $new_student = $student['new_student'];
                    $student_type = $student['student_type_id'];
                    $session_id = $session['session_id'];
                    $app_access = $student['app_access_fee_status'];
                    $fee_calculate_month = $student['fee_calculate_month'];
                    $previous_due_fee = (int)$student['previous_due_fee_original'];
                    $fee_data['student_name'] = $student['student_name'];
                    $fee_data['father_name'] = $student['father_name'];
                    $fee_data['class_name'] = $student['class_name'];
                    $fee_data['care_of_name'] = $student['care_of_name'];
                    $fee_data['enrollment_number'] = $student['enrollment_number'];
                    $fee_data['session_year'] = $session['session_year'];
                    $academic_paid_fee = [];
                    if (!empty($student_receipt_print[$student_id]['paid_fee_receipt_id'])) {
                        $fee_type_id = null;
                        $paid_fee_receipt_id = $student_receipt_print[$student_id]['paid_fee_receipt_id'];
                        $academic_paid_fee = $this->FeeTable($student_id, $class_id, $session_id, $fee_calculate_month, $new_student, $student_type, $fee_type_id, $previous_due_fee, $app_access, $paid_fee_receipt_id);
                        $academic_paid_fee['recently_paid_fee'] = isset($academic_paid_fee['paid'][$paid_fee_receipt_id]) ? $academic_paid_fee['paid'][$paid_fee_receipt_id] : [];
                    }
                    $fee_data['student_fee'] = $academic_paid_fee;
                    $transport_paid_fee = [];
                    if (!empty($student_receipt_print[$student_id]['paid_transport_fee_id'])) {
                        $paid_fee_receipt_id = $student_receipt_print[$student_id]['paid_transport_fee_id'];
                        $transport_paid_fee = $this->studentTransportFee($student_id);
                        $transport_paid_fee['recently_paid_fee'] = isset($transport_paid_fee['paid'][$paid_fee_receipt_id]) ? $transport_paid_fee['paid'][$paid_fee_receipt_id] : [];
                    }
                    $fee_data['transport_fee'] = $transport_paid_fee;
                    $student_return_data['fee'][] = $fee_data;
                }
            }
            $status = 'success';
        }
        $make_paid_fee_tables = View::make('backend.student-fee-receipt.paid-fee-receipt')->with($student_return_data)->render();
        //            $result          = array(
        //                'status'            => $status,
        //                'data'              => $make_paid_fee_tables,
        //                );
        return $make_paid_fee_tables; //response()->json($result);
    }

    function studentFeeDetail()
    {
        $arr_student_id = Input::get('arr_student');
        $fee_type_id = Input::get('fee_type_id');
        $arr_student = [];
        $student_return_data = [];
        $class_id = null;
        $session_id = null;
        $status = 'failed';
        if (!empty($arr_student_id)) {
            /** get Student all data */
            $arr_student = get_student_data($arr_student_id);
            if (!empty($arr_student)) {
                $session = get_current_session();
                foreach ($arr_student as $key => $student) {
                    $fee_data = [];
                    $student_id = $student['student_id'];
                    $class_id = $student['current_class_id'];
                    $new_student = $student['new_student'];
                    $student_type = $student['student_type_id'];
                    $session_id = $session['session_id'];
                    $fee_calculate_month = $student['fee_calculate_month'];
                    $app_access = $student['app_access_fee_status'];
                    $previous_due_fee = (int)$student['previous_due_fee_original'];
                    $fee_data['student_id'] = $student_id;
                    $fee_data['student_name'] = $student['student_name'];
                    $fee_data['class_id'] = $class_id;
                    $fee_data['class_name'] = $student['class_name'];
                    $fee_data['care_of_name'] = $student['care_of_name'];
                    $fee_data['enrollment_number'] = $student['enrollment_number'];

                    $profile = 'public/st.jpg';
                    if (!empty($student['profile'])) {
                        $profile = $student['profile'];
                    }
                    $fee_data['remark'] = $student['remark'];
                    $fee_data['session_id'] = $session_id;
                    $fee_data['student_id'] = $student_id;
                    $paid_fee_receipt_id = null;
                    $fee_data['student_fee'] = $this->FeeTable($student_id, $class_id, $session_id, $fee_calculate_month, $new_student, $student_type, $fee_type_id, $previous_due_fee, $app_access, $paid_fee_receipt_id);
                    $fee_data['transport_fee'] = $this->studentTransportFee($student_id);
                    $student_return_data['fee'][] = $fee_data;
                }
            }
            $status = 'success';
        }

        $make_fee_tables = View::make('backend.student-fee-receipt.student_fee')->with($student_return_data)->render();
        $result = array(
            'status' => $status,
            'data' => $make_fee_tables,
            'care_of_name' => $fee_data['care_of_name'],
            'enrollment_number' => $fee_data['enrollment_number'],
            'class_name' => $fee_data['class_name'],
            'profile' => url($profile),
            'student_name' => $fee_data['student_name'],
            'remark' => $fee_data['remark']);
        return response()->json($result);
    }

    public function FeeTable($student_id, $class_id, $session_id, $fee_calculate_month, $new_student, $student_type, $fee_type_id = null, $previous_due_fee = 0, $app_access = null, $paid_fee_receipt_id = null)
    {
        $fee_receipt = [];
        $fee_installment_array = [];
        $arr_return = [];
        $arr_class_fee = [];
        $arr_paid_fee = [];
        $installment_paid_amount_sum = 0;
        $total_paid_fine = 0;
        $total_paid_discount = 0;
        $deposited_amount = 0;

        $arr_condition = array(
            'class_id' => $class_id,
            'new_student' => $new_student,
        );

        /** paid fee */
        $fee_paid_list = $this->getPaidFee($class_id, $session_id, $student_id, $fee_type_id, $app_access = null, $paid_fee_receipt_id = null);
        if (!empty($fee_paid_list)) {
            $deposited_amount = array_sum(array_column($fee_paid_list, 'net_amount'));
            $installment_paid_amount_sum = array_sum(array_column($fee_paid_list, 'total_amount'));
            $total_paid_fine = array_sum(array_column($fee_paid_list, 'fine_amount'));
            $total_paid_discount = array_sum(array_column($fee_paid_list, 'discount_amount'));
            /*
             *  used to make sigle array of paid fee and its fee detail,
             *  it will be used to check paid fee in pending fee
             */
            if (!empty($fee_paid_list)) {
                foreach ($fee_paid_list as $key => $value) {
                    $paid_fee = $value['fee_receipt_detail']; //isset($value['fee_receipt_detail']) ? $value['fee_receipt_detail'] : [];
                    $arr_paid_fee = array_merge($arr_paid_fee, $paid_fee);
                }
            }
        }
        /**  get fee data according to student current class and session id */
        if (!empty($class_id) && !empty($session_id)) {
            $arr_class_fee = Fee::where('session_id', $session_id)
                ->where(function ($query) use ($arr_condition) {
                    if (!empty($arr_condition['class_id'])) {
                        $query->where('class_id', $arr_condition['class_id']);
                    }
                    if ($arr_condition['new_student'] == 1) {
                        $query->orWhereIn('apply_to', array('all_student', 'new_student'));
                    } else {
                        $query->orWhere('apply_to', 'all_student');
                    }
                })
                ->where(function ($query) use ($student_type) {
                    if (!empty($student_type)) {
                        $query->orWhereIn('student_type_id', array(0, $student_type));
                    }
                })
                ->where(function ($query) use ($fee_type_id) {
                    if (!empty($fee_type_id)) {
                        $query->where('fee_type_id', $fee_type_id);
                    }
                })
                ->with(['getFeeType' => function ($query) {

                }])->with('getFeeCircular')->with('feeDetail')
                ->get();

            $arr_fee_circular = get_fee_circular_custom();
            $installment_fee_original_amount = [];
            $installment_fine_amount = [];
            foreach ($arr_class_fee as $class_fee) {
                $fee_circular = $class_fee['getFeeCircular']['fee_circular'];
                $fee_installment_array = array(
                    'fee_id' => $class_fee['fee_id'],
                    'fee_date' => get_formatted_date($class_fee['fee_date'], 'display'),
                    'fine_amount' => $class_fee['fine_amount'],
                    'fine_type' => $class_fee['fine_type'],
                    'fee_circular_id' => $class_fee['getFeeCircular']['fee_circular_id'],
                    'fee_circular' => $arr_fee_circular[$fee_circular],
                    'fee_type_id' => $class_fee['getFeeType']['fee_type_id'],
                    'order' => $class_fee['getFeeType']['order'],
                    'fee_type' => $class_fee['getFeeType']['fee_type'],
                );
                if (isset($class_fee['feeDetail'])) {
                    $fee_installment_array['arr_fee_detail'] = [];
                    $arr_fee_sub_circular = \Config::get('custom.fee_sub_circular');
                    $fee_sub_circular = isset($arr_fee_sub_circular[$fee_circular]) ? $arr_fee_sub_circular[$fee_circular] : [];

                    $eight_installment_period = [];
                    if ($fee_circular == 'installment') {
                        $get_installment = $this->getEigthInstallmentPeriod($fee_calculate_month, $class_fee['feeDetail']);
                        $eight_installment_period = array_keys($get_installment);
                    }
                    $fee_start_status = false;
                    foreach ($class_fee['feeDetail'] as $key => $fee_detail) {
                        /*
                         * Calculate fee based on studen fee calculate fee month
                         */
                        if ($fee_circular == 'installment') {
                            $fee_start_status = true;
                            if (!in_array($fee_detail['start_month'], $eight_installment_period)) {
                                /* skip the current itration and keep the loop continue */
                                continue;
                            }
                        } elseif ($fee_installment_array['fee_type_id'] == 3) {
                            // if fee is tuition then it will check fee calculate month
                            $start_month = $fee_detail['start_month'];
                            $end_month = $fee_detail['end_month'];
                            if (!empty($start_month) || !empty($end_month)) {
                                $arr_interval = $this->getMonthInterval($start_month, $end_month);
                                if (!empty($arr_interval) && in_array($fee_calculate_month, array_keys($arr_interval))) {
                                    $fee_start_status = true;
                                }
                            }
                        } else {
                            $fee_start_status = true;
                        }
                        // $app_access = 2:Expired or Unpaid
                        if ((($fee_start_status == true) && (($fee_detail['fee_amount'] > 0) || ($fee_installment_array['fee_type_id'] == 2 && $previous_due_fee > 0) || ($fee_installment_array['fee_type_id'] == 1 && $app_access == 2)))) {

                            /** fine data calculation,if fine is in days */
                            $fine_days = 0;
                            $installment_fine = 0;
                            //		        if ($fee_installment_array['fine_type'] == 0)
                            //		        {
                            $fine_days = $this->getFineDays($fee_detail['fine_date'], $fee_detail['grace_period']);
                            //		        }
                            //p($fine_days,0);
                            //p($fee_sub_circular[$fee_detail['fee_sub_circular']],0);
                            $pending_status = true;
                            $installment_paid_amount = 0;
                            $fee_detail_data = array(
                                'fee_id' => $fee_detail['fee_id'],
                                'fee_detail_id' => $fee_detail['fee_detail_id'],
                                'fee_sub_circular' => $fee_sub_circular[$fee_detail['fee_sub_circular']],
                                'fee_sub_circular_name' => $fee_detail['fee_sub_circular'],
                                'start_month' => $fee_detail['start_month'],
                                'end_month' => $fee_detail['end_month'],
                                'fee_amount' => $fee_detail['fee_amount'],
                                'fine_date' => $fee_detail['fine_date'],
                                'grace_period' => $fee_detail['grace_period'],
                                'fine_days' => $fine_days,
                            );
                            //p($fee_detail_data,0);
                            /* fee_type_id == 2 (Previous Due fee)
                             * update fee amount in case of previous due fee,
                             * this fee amount will depends on student previous amount
                             */
                            if (($fee_installment_array['fee_type_id'] == 2 && $previous_due_fee > 0)) {
                                $fee_detail_data['fee_amount'] = $previous_due_fee;
                                $fee_detail['fee_amount'] = $previous_due_fee;
                            }
                            /* fee_type_id == 1 (App access fee)
                             * this fee  is dependent on session,
                             * this fee amount will renew when academic year change
                             */
                            if (($fee_installment_array['fee_type_id'] == 1 && $app_access = 2)) {
                                //                                    $fee_detail_data['fee_amount'] = $previous_due_fee;
                                $app_fee = \Config::get('custom.app_access_fee');
                                $fee_detail_data['fee_amount'] = $app_fee;
                                $fee_detail['fee_amount'] = $app_fee;
                            }
                            /** checked paid fee installments, and if paid fee exist then removed from display list */
                            if (!empty($arr_paid_fee)) {
                                $arr_partial_paid_amount = array();
                                $fee_id = $fee_detail_data['fee_id'];
                                $fee_detail_id = $fee_detail_data['fee_detail_id'];
                                foreach ($arr_paid_fee as $key => $paid_fee) {
                                    $paid_fee_id = $paid_fee['fee_id'];
                                    $paid_fee_detail_id = $paid_fee['fee_detail_id'];
                                    $paid_status = $paid_fee['installment_payment_status'];
                                    if (($fee_id == $paid_fee_id) && ($fee_detail_id == $paid_fee_detail_id)) {
                                        /*
                                         * $paid_status == 1,fully paid installment
                                         * $paid_status == 0, partial paid
                                         */
                                        if ($paid_status == 1) {
                                            $pending_status = false;
                                            $fee_detail_data = [];
                                        } elseif ($paid_status == 0) {
                                            $pending_status = true;
                                            $arr_partial_paid_amount[] = $paid_fee['installment_paid_amount'];
                                        }
                                    }
                                }
                                /** calculate sum of installment partial paid amount */
                                $installment_paid_amount = array_sum($arr_partial_paid_amount);
                            }
                            /* check install pending or not */
                            if ($pending_status == true) {
                                /* partial_paid_amount : partially paid amount of that inatllment  */
                                $fee_detail_data['partial_paid_amount'] = $installment_paid_amount;
                                //                                    if ($fee_installment_array['fee_type_id'] == 2)
                                //                                    {
                                //                                        /* fee type 2 means previous and in this case pre due amount is already updated */
                                //                                        $fee_detail_data['final_pending_amount'] = $fee_detail_data['fee_amount'];
                                //                                    }
                                //                                    else
                                //                                    {
                                $fee_detail_data['final_pending_amount'] = $fee_detail_data['fee_amount'] - $fee_detail_data['partial_paid_amount'];
                                //                                    }
                                //p($fine_days,0);
                                if ($fine_days > 0 && $fee_installment_array['fine_amount'] > 0 && $fee_detail_data['final_pending_amount'] > 0) {
                                    if ($fee_installment_array['fine_type'] == 0) {
                                        $installment_fine = $fine_days * $fee_installment_array['fine_amount'];
                                    } else {
                                        $installment_fine = floor($fee_detail_data['final_pending_amount'] * $fee_installment_array['fine_amount'] / 100);
                                    }
                                }
                                $fee_detail_data['installment_fine_amount'] = $installment_fine;
                                $fee_installment_array['arr_fee_detail'][] = $fee_detail_data;
                                // will be use in calculate fine sum
                                $installment_fine_amount[] = $fee_detail_data['installment_fine_amount'];
                            }
                            $installment_fee_original_amount[] = $fee_detail['fee_amount'];
                        }
                    }
                }
                if (!empty($fee_installment_array['arr_fee_detail'])) {
                    $fee_receipt[] = $fee_installment_array;
                }
            }

            // sort fee by fee type
            $name = 'order';
            usort($fee_receipt, function ($a, $b) use (&$name) {
                return $a[$name] - $b[$name];
            });
            /** return pending, paid fee and list */
            $arr_return['pending'] = $fee_receipt;
            $arr_return['paid'] = $fee_paid_list;

            /* orginal fee amount */
            $installment_fine_sum = 0;
            //	    $installment_amount_sum = 0;
            $installment_amount_sum = array_sum($installment_fee_original_amount);
            $installment_fine_sum = array_sum($installment_fine_amount);
            $pending_amount = $installment_amount_sum - $installment_paid_amount_sum;
            $arr_return['installment_fine_amount'] = $installment_fine_sum > 0 ? $installment_fine_sum : 0;
            $arr_return['pending_amount'] = $pending_amount > 0 ? $pending_amount : 0;
            $arr_return['deposit_amount'] = $deposited_amount > 0 ? $deposited_amount : 0;
            $arr_return['total_paid_amount'] = $installment_paid_amount_sum > 0 ? $installment_paid_amount_sum : 0;
            $arr_return['total_paid_fine'] = $total_paid_fine > 0 ? $total_paid_fine : 0;
            $arr_return['total_paid_discount'] = $total_paid_discount > 0 ? $total_paid_discount : 0;
        }
        return $arr_return;
    }

    public function getPaidFee($class_id, $session_id, $student_id, $fee_type_id = null, $app_access = null, $paid_fee_receipt_id = null)
    {
        /** paid fee from receipt table */
        $arr_paid_list = [];
        $arr_student_paid_installments = StudentFeeReceipt::where('class_id', $class_id)
            ->where('session_id', $session_id)
            ->where('student_id', $student_id)
            ->where('fee_status', 1)
            ->with('feeReceiptDetails.getFeeType', 'feeReceiptDetails.getFeeCircular')
            ->with(['feeReceiptDetails' => function ($query) use ($fee_type_id) {
                if (!empty($fee_type_id)) {
                    $query->where('fee_type_id', $fee_type_id);
                }
            }])->get();

        if (!empty($arr_student_paid_installments)) {
            $fee_detail = false;
            foreach ($arr_student_paid_installments as $key => $student_paid_installments) {
                $student_fee_receipt_id = $student_paid_installments['student_fee_receipt_id'];
                $arr_paid_list[$student_fee_receipt_id] = array(
                    'student_fee_receipt_id' => $student_fee_receipt_id,
                    'receipt_number' => $student_paid_installments['receipt_number'],
                    'receipt_date' => $student_paid_installments['receipt_date'],
                    'net_amount' => $student_paid_installments['net_amount'],
                    'discount_amount' => $student_paid_installments['discount_amount'],
                    'fine_amount' => $student_paid_installments['fine_amount'],
                    'total_amount' => $student_paid_installments['total_amount'],
                );
                $arr_paid_list_details = [];
                foreach ($student_paid_installments['feeReceiptDetails'] as $paid_installment_details) {
                    $fee_circular_name = $paid_installment_details['getFeeCircular']['fee_circular'];
                    $arr_fee_sub_circular = \Config::get('custom.fee_sub_circular');
                    $fee_sub_circular = isset($arr_fee_sub_circular[$fee_circular_name]) ? $arr_fee_sub_circular[$fee_circular_name] : [];
                    $fee_detail = true;
                    $arr_paid_list_details[] = array(
                        'fee_id' => $paid_installment_details['fee_id'],
                        'fee_detail_id' => $paid_installment_details['fee_detail_id'],
                        'fee_type_id' => $paid_installment_details['fee_type_id'],
                        'fee_circular_id' => $paid_installment_details['fee_circular_id'],
                        'fee_circular_name' => $fee_sub_circular[$paid_installment_details['fee_circular_name']],
                        'installment_amount' => $paid_installment_details['installment_amount'],
                        'installment_paid_amount' => $paid_installment_details['installment_paid_amount'],
                        'fee_type' => $paid_installment_details['getFeeType']['fee_type'],
                        'fee_circular' => $paid_installment_details['getFeeCircular']['fee_circular'],
                        'installment_payment_status' => $paid_installment_details['installment_payment_status'],
                    );
                }
                $arr_paid_list[$student_fee_receipt_id]['fee_receipt_detail'] = $arr_paid_list_details;
            }

            /** if fee_type_id not exist */
            if ($fee_detail == false) {
                $arr_paid_list = [];
            }
        }
        return $arr_paid_list;
    }

    private function getFineDays($fine_date, $grace_period)
    {
        $current_date = date('y-m-d');
        $fine_days = 0;
        if (!empty($fine_date) && strtotime($current_date) > strtotime($fine_date)) {
            $begin = new DateTime($fine_date);
            $end = new DateTime($current_date);
            //              $end          = $end->modify('+1 day');
            $interval_day = new DateInterval('P1D');
            $date_range = new DatePeriod($begin, $interval_day, $end);

            foreach ($date_range as $key => $date) {
                $fine_period[] = $date->format('d/m/Y');
            }
            $fine_days = count($fine_period);
        }
        if ($fine_days > 0) {
            $fine_days = $fine_days - $grace_period;
        }
        return $fine_days;
    }

    public function revertPaidFee(Request $request)
    {
        $student_fee_receipt_id = Input::get('student_fee_receipt_id');
        $student_fee = StudentFeeReceipt::find($student_fee_receipt_id);
        if ($student_fee) {
            $student_fee->fee_status = 2;
            $student_fee->save();
            $return_arr = array(
                'status' => 'success',
                'message' => 'Student Fee Receipt deleted successfully!',
                'student_id' => $student_fee['student_id'],
            );
        } else {
            $return_arr = array(
                'status' => 'error',
                'message' => 'Student Fee Receipt not found!',
                'student_id' => null,
            );
        }
        return response()->json($return_arr);
    }

    public function getMonthInterval($start_month, $end_month)
    {
        $interval = [];
        $arr_months = get_current_session_months(); // current session months list
        foreach ($arr_months as $key => $months) {
            if ($start_month == $key || !empty($interval)) {
                $interval[$key] = $months;
                if ($key == $end_month) {
                    break;
                }
            }
        }
        return $interval;
    }

    public function getEigthInstallmentPeriod($fee_calculate_month, $installment_fee_detail)
    {

        $interval = [];
        $arr_exclude_month = [];
        $installment_count = \Config::get('custom.installment_count');
        $arr_session_months = get_current_session_months(); // current session months list
        // check if fee amount is 0
        if (!empty($installment_fee_detail)) {
            foreach ($installment_fee_detail as $key => $fee_detail) {
                if ($fee_detail['fee_amount'] <= 0) {
                    $arr_exclude_month[$fee_detail['start_month']] = $fee_detail['fee_sub_circular'];
                }
            }
        }
        // exculde months from session months which has fee amount is 0
        if (!empty($arr_exclude_month)) {
            foreach ($arr_exclude_month as $key => $value) {
                unset($arr_session_months[$key]);
            }
        }
        foreach ($arr_session_months as $key => $months) {
            if ($fee_calculate_month == $key || !empty($interval)) {
                $interval[$key] = $months;
                if (count($interval) == $installment_count) {
                    break;
                }
            }
        }
        return $interval;
    }

    function parentStudentList()
    {
        $contact_no = Input::get('parent_contact');
        $session = get_current_session();
        $return_data = [];
        $status = 'failed';
        if (!empty($contact_no) && !empty($session)) {
            $parent_data = [];
            $arr_parent = StudentParent::addSelect(['student_parent_id', 'guardian_first_name', 'father_first_name', 'father_middle_name', 'father_last_name', 'guardian_middle_name', 'mother_first_name', 'mother_middle_name', 'mother_last_name', 'guardian_last_name', 'care_of'])
                ->where('student_parent_status', 1)
                ->where(function ($query) use ($contact_no) {
                    if (!empty($contact_no)) {
                        $query->where('father_contact_number', $contact_no)
                            ->orWhere('mother_contact_number', $contact_no)
                            ->orWhere('guardian_contact_number', $contact_no);
                    }
                })
                ->with(['getParentStudent.getClass' => function ($q) {
                    $q->addSelect(['class_id', 'class_name']);
                }])
                ->with(['getParentStudent' => function ($q) use ($session) {
                    $q->addSelect(['student_id', 'student_parent_id', 'current_class_id', 'enrollment_number', 'first_name', 'middle_name', 'last_name', 'remark']);
                    $q->where('student_status', 1);
                    $q->where('current_session_id', $session['session_id']);
                }])->first();

            if (!empty($arr_parent)) {
                $name = '';
                if ($arr_parent['care_of'] == 1) {
                    $name = $arr_parent['father_first_name'] . ' ' . $arr_parent['father_middel_name'] . ' ' . $arr_parent['father_last_name'];
                } elseif ($arr_parent['care_of'] == 2) {
                    $name = $arr_parent['mother_first_name'] . ' ' . $arr_parent['mother_middel_name'] . ' ' . $arr_parent['mother_last_name'];
                } elseif ($arr_parent['care_of'] == 3) {
                    $name = $arr_parent['guaridan_first_name'] . ' ' . $arr_parent['guaridan_middel_name'] . ' ' . $arr_parent['guaridan_last_name'];
                }
                $parent_data['parent'] = array(
                    'care_of_name' => $name,
                );
                foreach ($arr_parent['getParentStudent'] as $studentDetail) {
                    $parent_data['parent']['student_detail'][] = array(
                        'student_name' => $studentDetail['first_name'] . ' ' . $studentDetail['middle_name'] . ' ' . $studentDetail['last_name'],
                        'student_id' => $studentDetail['student_id'],
                        'enrollment_number' => $studentDetail['enrollment_number'],
                        'class_id' => $studentDetail['current_class_id'],
                        'class_name' => $studentDetail['getClass']['class_name'],
                        'remark' => $studentDetail['remark'],
                        'profile' => check_file_exist($studentDetail['profile_photo'], 'student_profile'),
                    );
                }
            }
            //                p($parent_data);
            $return_data = View::make('backend.student-fee-receipt.parent_student')->with($parent_data)->render();
            $status = 'success';
        }
        return response()->json($result = array('status' => $status, 'data' => $return_data));
    }

    public function studentTransportFee($student_id)
    {
        $session = get_current_session();
        $arr_transport_fee = [];
        if (!empty($session) && !empty($student_id)) {
            $session_id = $session['session_id'];
            $arr_data = array('student_id' => $student_id, 'session_id' => $session_id);
            $arr_month_name = \Config::get('custom.month'); // list of month name and id
            $arr_transport_fee['paid'] = $this->transportPaidFee($student_id, $session_id, $arr_month_name);

            //get monthly fee of student  according to stop fair
            $arr_monthly_fair = get_student_transport_fee($arr_data);
            $arr_transport_fee['pending'] = [];
            if (!empty($arr_monthly_fair)) {
                // all monthly fee
                foreach ($arr_monthly_fair as $month_id => $fee_months) {
                    $month_id = (int)$month_id; // remove heading zero from month id
                    $pending_status = true;
                    $installment_partial_paid_amount = 0;

                    /** checked paid fee installments, and if paid fee exist then removed from display list */
                    if (!empty($arr_transport_fee['paid'])) {
                        $arr_partial_paid_amount = [];

                        //$month_id = $key;
                        foreach ($arr_transport_fee['paid'] as $key => $arr_paid_fee) {
                            if (isset($arr_paid_fee['paid_fee_detail'])) {
                                foreach ($arr_paid_fee['paid_fee_detail'] as $key => $paid_fee) {
                                    $paid_month_id = $paid_fee['month_id'];
                                    $amount_status = $paid_fee['amount_status'];
                                    if (($paid_month_id == $month_id)) {
                                        /*
                                         * $paid_status == 1,fully paid installment
                                         * $paid_status == 2, partial paid
                                         */
                                        if ($amount_status == 1) {
                                            $pending_status = false;
                                            $fee_detail_data = [];
                                        } elseif ($amount_status == 2) {
                                            $pending_status = true;
                                            $arr_partial_paid_amount[] = $paid_fee['paid_fee_amount'];
                                        }
                                    }
                                }
                            }
                        }

                        /** calculate sum of installment partial paid amount */
                        $installment_partial_paid_amount = array_sum($arr_partial_paid_amount);
                    }

                    /* check install pending or not */
                    if ($pending_status == true) {
                        /* partial_paid_amount : partially paid amount of that inatllment  */
                        $transport_pending_amount = $fee_months - $installment_partial_paid_amount;
                        $arr_transport_fee['pending'][] = array(
                            'transport_fee_amount' => $fee_months,
                            'pending_amount' => $transport_pending_amount,
                            'month' => $arr_month_name[$month_id],
                            'month_id' => $month_id,
                            //	                    'assigned_detail_id' => $vehicle_assigned_detail['student_vehicle_assigned_detail_id'],
                        );
                    }
                }
                $arr_transport_fee['total_pending_amount'] = array_sum(array_column($arr_transport_fee['pending'], 'pending_amount'));
            }
        }
        return $arr_transport_fee;
    }

    public function transportPaidFee($student_id, $session_id, $arr_months = [])
    {
        $data_transport_paid_fee = [];
        if (!empty($session_id) && !empty($student_id)) {
            $all_transport_paid_fee = TransportFee::addSelect('receipt_number', 'receipt_date', 'total_paid_amount', 'transport_fee_id')
                ->where('student_id', $student_id)
                ->where('session_id', $session_id)
                ->where('status', 1)
                ->where('fee_status', 1)
                ->with(['transportFeeDetail' => function ($query) {
                    $query->where('fee_detail_status', 1);
                }])
                ->get();
            //                        p($all_transport_paid_fee);
            if (!empty($all_transport_paid_fee)) {
                $transport_paid_fee_sum = [];
                foreach ($all_transport_paid_fee as $key => $transport_paid_fee) {
                    $arr_transport_paid_fee = array(
                        'transport_fee_id' => $transport_paid_fee['transport_fee_id'],
                        'receipt_number' => $transport_paid_fee['receipt_number'],
                        'receipt_date' => $transport_paid_fee['receipt_date'],
                        'deposit_amount' => 0,
                    );
                    foreach ($transport_paid_fee['transportFeeDetail'] as $key => $paid_fee) {
                        $arr_transport_paid_fee['paid_fee_detail'][] = array(
                            'transport_fee_detail_id' => $paid_fee['transport_fee_detail_id'],
                            'month' => $arr_months[$paid_fee['month_id']],
                            'month_id' => $paid_fee['month_id'],
                            'paid_fee_amount' => $paid_fee['paid_fee_amount'],
                            'amount_status' => $paid_fee['amount_status'],
                        );
                    }
                    if (isset($arr_transport_paid_fee['paid_fee_detail'])) {
                        $arr_transport_paid_fee['deposit_amount'] = array_sum(array_column($arr_transport_paid_fee['paid_fee_detail'], 'paid_fee_amount'));
                    }
                    $data_transport_paid_fee[$transport_paid_fee['transport_fee_id']] = $arr_transport_paid_fee;
                }
            }
        }
        return $data_transport_paid_fee;
    }

    public function revertTransportPaidFee(Request $request)
    {
        $transport_fee_id = Input::get('transport_fee_id');
        $student_transport_fee = TransportFee::find($transport_fee_id);
        if ($student_transport_fee) {
            // reverted fee installment
            $student_transport_fee->fee_status = 2;
            $student_transport_fee->save();
            $return_arr = array(
                'status' => 'success',
                'message' => 'Transport fee installment reverted successfully!',
                'student_id' => $student_transport_fee['student_id'],
            );
        } else {
            $return_arr = array(
                'status' => 'error',
                'message' => 'Transport fee installment not found!',
                'student_id' => null,
            );
        }
        return response()->json($return_arr);
    }

    function studentClass()
    {
        $class_id = Input::get('class_id');
        $arr_student_list = [];
        $message = 'Students not found!';
        $status = 'failed';
        if (!empty($class_id)) {
            $arr_student = get_student_list_by_class_id($class_id);
            if (!empty($arr_student)) {
                foreach ($arr_student as $student) {
                    $arr_student_list[$student['student_id']] = $student['enrollment_number'] . ' : ' . $student['first_name'] . ' ' . $student['last_name'];
                }
                $message = 'Got result successfully';
                $status = 'success';
            }
        }
        return response()->json($result = array('status' => $status, 'message' => $message, 'data' => $arr_student_list));
    }

    public function studentFeeReport()
    {
        $data = array(
            'arr_class' => add_blank_option(get_all_classes(), '-- Select class --'),
            'arr_student' => add_blank_option([], '-- Select student --'),
            'arr_payment_mode' => add_blank_option(get_payment_mode(), '-- Payment mode --'),
        );
        return view('backend.report.student-fee-report')->with($data);
    }

    public function studentFeeReportData()
    {
        $student = [];
        $class_id = Input::get('class_id');
        $student_id = Input::get('student_id');
        $receipt_number = Input::get('receipt_number');
        $from_date = get_formatted_date(Input::get('from_date'), 'database');
        $to_date = get_formatted_date(Input::get('to_date'), 'database');
        $payment_mode_id = Input::get('payment_mode_id');
        $limit = Input::get('length');
        $offset = Input::get('start');
        $session = get_current_session();
        if (!empty($session)) { // !empty($class_id) &&
            $session_id = $session['session_id'];
            $arr_student_fee = StudentFeeReceipt::where('session_id', $session_id)->where('fee_status', 1)
                ->where(function ($q) use ($receipt_number) {
                    if (!empty($receipt_number)) {
                        $q->where('receipt_number', $receipt_number);
                    }
                })
                ->where(function ($q) use ($class_id) {
                    if (!empty($class_id)) {
                        $q->where('class_id', $class_id);
                    }
                })
                ->where(function ($q) use ($student_id) {
                    if (!empty($student_id)) {
                        $q->where('student_id', $student_id);
                    }
                })
                ->where(function ($q) use ($payment_mode_id) {
                    if (!empty($payment_mode_id)) {
                        $q->where('payment_mode_id', $payment_mode_id);
                    }
                })
                ->where(function ($query) use ($from_date, $to_date) {
                    if (!empty($from_date) && !empty($to_date)) {
                        $query->whereBetween('receipt_date', array($from_date, $to_date));
                    } else if (!empty($from_date)) {
                        $query->where('receipt_date', '>=', $from_date);
                    } else if (!empty($to_date)) {
                        $query->where('receipt_date', '<=', $to_date);
                    }
                })
                ->with(['getFeeStudent' => function ($q) {
                    $q->addSelect(['student_id', 'student_parent_id', 'current_class_id', 'current_session_id', 'caste_category_id', 'enrollment_number', 'first_name', 'middle_name', 'last_name',
                        DB::raw("CONCAT(first_name,' ',middle_name,' ',last_name)  AS student_name")]);
                    $q->where('student_status', 1);
                }])
                ->with(['getFeeStudent.getParent' => function ($q) {
                    $q->select(['student_parent_id', 'father_first_name', 'father_middle_name', 'father_last_name', 'mother_first_name',
                        'mother_middle_name', 'mother_last_name', 'care_of', 'father_contact_number', 'mother_contact_number',
                        'guardian_first_name', 'guardian_middle_name', 'guardian_last_name', 'guardian_contact_number',
                        DB::raw("CONCAT(father_first_name,' ',father_middle_name,' ',father_last_name)  AS father_name"),
                        DB::raw("CONCAT(mother_first_name,' ',mother_middle_name,' ',mother_last_name)  AS mother_name"),
                        DB::raw("CONCAT(guardian_first_name,' ',guardian_middle_name,' ',guardian_last_name)  AS guardian_name"),
                    ]);
                }])
                ->with(['getFeeStudent.getClass' => function ($q) {
                    $q->addSelect(['class_id', 'class_name']);
                }])
                ->with(['getFeeStudent.getSection' => function ($q) {
                    $q->addSelect(['section_id', 'section_name']);
                }])
                ->with(['getPaymentMode' => function ($q) {
                    $q->addSelect(['payment_mode_id', 'payment_mode']);
                }])
                ->get();
            $count = 1;
            foreach ($arr_student_fee as $key => $arr_student) {
                $arr_fee_data = array(
                    'student_id' => $arr_student['student_id'],
                    's_no' => $count,
                    'enrollment_number' => $arr_student['getFeeStudent']['enrollment_number'],
                    'receipt_number' => $arr_student['receipt_number'],
                    'receipt_id' => $arr_student['student_fee_receipt_id'],
                    'receipt_date' => get_formatted_date($arr_student['receipt_date'], 'display'),
                    'class_name' => $arr_student['getFeeStudent']['getClass']['class_name'] . '(' . $arr_student['getFeeStudent']['getSection']['section_name'] . ')',
                    'student_name' => $arr_student['getFeeStudent']['first_name'] . ' ' . $arr_student['getFeeStudent']['middle_name'] . ' ' . $arr_student['getFeeStudent']['last_name'],
                    'father_name' => $arr_student['getFeeStudent']['getParent']['father_first_name'] . ' ' . $arr_student['getFeeStudent']['getParent']['father_middle_name'] . ' ' . $arr_student['getFeeStudent']['getParent']['father_last_name'],
                    'father_contact_number' => $arr_student['getFeeStudent']['getParent']['father_contact_number'],
                    'payment_mode' => ucfirst($arr_student['getPaymentMode']['payment_mode']),
                    'total_amount' => (int)$arr_student['total_amount'],
                    'fine_amount' => (int)$arr_student['fine_amount'],
                    'cheque_bounce_charges_received' => (int)$arr_student['cheque_bounce_charges_received'],
                    'discount_amount' => (int)$arr_student['discount_amount'],
                    'net_amount' => (int)$arr_student['net_amount'],
                );
                $student[$key] = (object)$arr_fee_data;
                $count++;
            }
        }
        return Datatables::of($student)->make(true);
    }

    public function transportFeeReport()
    {
        $arr_vehicle = [];
        $arr_vehicle_data = Vehicle::where('vehicle_status', 1)->get();
        foreach ($arr_vehicle_data as $key => $vehicle) {
            $arr_vehicle[$vehicle['vehicle_id']] = $vehicle['vehicle_name'] . '(' . $vehicle['vehicle_number'] . ')';
        }
        $data = array(
            'arr_class' => add_blank_option(get_all_classes(), '-- Select class --'),
            'arr_student' => add_blank_option([], '-- Select student --'),
            'arr_vehicle' => add_blank_option($arr_vehicle, '-- Select vehicle --'),
        );
        return view('backend.report.student-transport-fee-report')->with($data);
    }

    public function transportFeeReportData()
    {
        $student = [];
        $class_id = Input::get('class_id');
        $student_id = Input::get('student_id');
        $receipt_number = Input::get('receipt_number');
        $from_date = get_formatted_date(Input::get('from_date'), 'database');
        $to_date = get_formatted_date(Input::get('to_date'), 'database');
        $vehicle_id = Input::get('vehicle_id');
        $limit = Input::get('length');
        $offset = Input::get('start');
        $session = get_current_session();
        if (!empty($class_id) && !empty($session)) {
            $session_id = $session['session_id'];
            $table = get_student_table($session_id, 'table');
            $transfer_fee = DB::table('transport_fees as tf')
                ->join('student_vehicle_assigned_details as svad', 'svad.student_id', '=', 'tf.student_id')
                ->join('student_vehicle_assigned as sva', 'sva.student_vehicle_assigned_id', '=', 'svad.assigned_id')
                ->join('stop_points as spo', 'sva.stop_point_id', '=', 'spo.stop_point_id')
                ->join('vehicles as v', 'v.vehicle_id', '=', 'sva.vehicle_id')
                ->join($table, 's.student_id', '=', 'tf.student_id')
                ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
                ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
                ->join('sections as sec', 'sec.section_id', '=', 's.current_section_id')
                ->join('routes as r', 'spo.route_id', '=', 'r.route_id')
                ->select('tf.receipt_number', 'tf.receipt_date', 'tf.total_paid_amount', 'v.vehicle_number', 'v.vehicle_id', 'sva.student_vehicle_assigned_id', 's.enrollment_number', 's.student_id', 'sp.father_first_name', 'sp.father_last_name', 'sp.father_middle_name', 'sp.father_contact_number', 's.first_name', 's.last_name', 's.middle_name', 'c.class_name', 'sec.section_name', 'sp.care_of', 'spo.stop_point', 'r.route'
                )
                ->where('tf.session_id', $session_id)
                ->where('sva.session_id', $session_id)
                ->where('tf.fee_status', 1)
                ->where('s.student_status', 1)
                ->where('svad.assigned_to', 1);

            if (!empty($receipt_number)) {
                $transfer_fee->where('tf.receipt_number', $receipt_number);
            }
            if (!empty($class_id)) {
                $transfer_fee->where('tf.class_id', $class_id);
            }
            if (!empty($student_id)) {
                $transfer_fee->where('tf.student_id', $student_id);
            }
            if (!empty($vehicle_id)) {
                $transfer_fee->where('sva.vehicle_id', $vehicle_id);
            }
            if (!empty($from_date) && !empty($to_date)) {
                $transfer_fee->whereBetween('receipt_date', array($from_date, $to_date));
            } else if (!empty($from_date)) {
                $transfer_fee->where('receipt_date', '>=', $from_date);
            } else if (!empty($to_date)) {
                $transfer_fee->where('receipt_date', '<=', $to_date);
            }
            $arr_student_transport_fee = //		$transfer_fee->skip($offset)->take($limit)
                $transfer_fee->get();

            $count = 1;
            foreach ($arr_student_transport_fee as $key => $arr_student) {
                $arr_student = (array)$arr_student;
                $arr_fee_data = array(
                    'student_id' => $arr_student['student_id'],
                    's_no' => $count,
                    'receipt_number' => $arr_student['receipt_number'],
                    'receipt_date' => get_formatted_date($arr_student['receipt_date'], 'display'),
                    'total_amount' => (int)$arr_student['total_paid_amount'],
                    'enrollment_number' => $arr_student['enrollment_number'],
                    'student_name' => $arr_student['first_name'] . ' ' . $arr_student['middle_name'] . ' ' . $arr_student['last_name'],
                    'father_name' => $arr_student['father_first_name'] . ' ' . $arr_student['father_middle_name'] . ' ' . $arr_student['father_last_name'],
                    'father_contact_number' => $arr_student['father_contact_number'],
                    'class_name' => $arr_student['class_name'] . '(' . $arr_student['section_name'] . ')',
                    'vehicle' => $arr_student['vehicle_number'],
                    'stop_point' => $arr_student['stop_point'],
                    'route' => $arr_student['route'],
                    'class_name' => $arr_student['class_name'] . '(' . $arr_student['section_name'] . ')'
                );
                $student[$key] = (object)$arr_fee_data;
                $count++;
            }
        }
        return Datatables::of($student)->make(true);
    }

    /*
     * Student Due fee report
     */

    public function studentDueFee()
    {
        $data = array(
            'arr_class' => add_blank_option(get_all_classes(), '-- Select class --'),
            'arr_student' => add_blank_option([], '-- Select student --'),
            'arr_fee_type' => add_blank_option(get_fee_type(), '-- Select fee type --'),
            'redirect_url' => 'admin/student-fee-receipt/add',
            'arr_fee_circular' => add_blank_option(get_fee_circular(), '-- Select fee circular --'),
            'arr_sub_fee_circular' => add_blank_option([], '-- Select sub fee circular --'),
            // 'arr_payment_status' => add_blank_option(arr_payment_status(), '-- Payment status --'),
        );
        return view('backend.report.student-due-fee-report')->with($data);
    }

    /*
     * Student Due fee report  Data
     */

    public function studentDueFeeData(Request $request)
    {
        $student = [];
        $class_id = Input::get('class_id');
        $student_id = Input::get('student_id');
        $fee_type_id = Input::get('fee_type_id');
        $fee_circular_id = Input::get('fee_circular_id');
        $fee_circular_name = Input::get('sub_fee_circular_id');
        $limit = Input::get('length');
        $offset = Input::get('start');
        $session = get_current_session();
        $session_id = $session['session_id'];
        $fee_circular_between = [];
        if(!empty($fee_circular_id) && !empty($fee_circular_name)){
            $fee_circular_between = get_fee_sub_circluar_between($fee_circular_id, $fee_circular_name);
        }
        if (!empty($session_id)) {
            $table = get_student_table($session_id);
            $query = DB::table($table)
                ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
                ->join('classes as c', 's.current_class_id', '=', 'c.class_id')
                ->join('sections as sec', 's.current_section_id', '=', 'sec.section_id')
                ->groupBy('s.student_id')
                ->select(
                    's.student_id', 's.first_name', 's.middle_name', 's.last_name', 's.enrollment_number',
                    's.app_accessbility_status', 'c.class_name', 'sec.section_name', 's.student_type_id', 's.new_student', 's.current_session_id', 's.previous_due_fee',
                    's.current_class_id', 'sp.father_first_name', 'sp.father_middle_name', 'sp.father_last_name', 'sp.father_contact_number', 'sp.mother_contact_number',
                    's.fee_calculate_month','s.previous_due_fee_original','s.app_access_fee_status'
                )
                ->where('s.student_type_id', 2) // Only paid students
                ->where('s.student_left','No')
//                ->where('s.current_class_id', 18) // For testing
//                ->where('s.student_id', 404) // For testing
//                ->where('s.current_class_id', 12) // For testing
//                ->where('s.student_id', 286) // For testing
                ->where('s.current_session_id', $session_id);
            if (!empty($class_id)) {
                $query->where('c.class_id', $class_id);
            }
            if (!empty($student_id)) {
                $query->where('s.student_id', $student_id);
            }
            $arr_student_fee = $query->orderBy('c.class_order')->orderBy('s.first_name')->get();
            $arr_student_fee = json_decode($arr_student_fee, true);
            $count = 1;
            $tt_total = 0;
            $tt_deposit = 0;
            $tt_pending = 0;
            foreach ($arr_student_fee as $key => $student_fee) {
                $arr_condition = array(
                    'session_id' => $student_fee['current_session_id'],
                    'class_id' => $student_fee['current_class_id'],
                    'student_type_id' => $student_fee['student_type_id'],
                    'new_student' => $student_fee['new_student'],
                    'fee_type_id' => $fee_type_id,
                    'fee_circular_id' => $fee_circular_id,
                    'fee_circular_between' => $fee_circular_between,
                );
                $academy_fee = get_student_total_academic_fee($arr_condition);
                $obj = new StudentFeeReceiptNewController();
//                $request->request->add([
//                    'arr_student' => [$student_fee['student_id']],
//                    'fee_receipt_date' => date('d/m/Y'),
//                    'fee_type_id' => NULL,
//                    'request_type' => 'API',
//                    'fee_circular_id' => $fee_circular_id,
//                    'fee_circular_between' => $fee_circular_between,
//                ]);
//                $response = $obj->studentFeeDetail();
                $arr_params = array(
                    "student_id" => $student_fee['student_id'],
                    'session_id' => $student_fee['current_session_id'],
                    'class_id' => $student_fee['current_class_id'],
                    "fee_calculate_month" => $student_fee['fee_calculate_month'],
                    "new_student" => $student_fee['new_student'],
                    "student_type" => $student_fee['student_type_id'],
                    "fee_type_id" => $fee_type_id,
                    "previous_due_fee" => (int)$student_fee['previous_due_fee'],
                    "app_access" => $student_fee['app_access_fee_status'],
                    "paid_fee_receipt_id" => null,
                    "receipt_date" => date("Y-m-d"), // "2021-11-25",
                    "fee_circular_id" => $fee_circular_id,
                    "fee_circular_name" => $fee_circular_name,
                    "fee_circular_between" => $fee_circular_between,
                );
//                p($arr_params);
                $student_fee_details = $obj->dueReportStudentFeeDetails($arr_params);
//                p($student_fee_details);
//                $fine_amount = 0;
//                $deposite_fee = 0;
//                if (!empty($response) && isset($response['fee'])) {
//                    foreach ($response['fee'] as $stu) {
//                        if (isset($stu['student_fee']['pending']) && !empty($stu['student_fee']['pending'])) {
//                            foreach ($stu['student_fee']['pending'] as $pending_fees) {
//                                if (!empty($pending_fees)) {
//                                    if (!empty($fee_circular_id)) {
//                                        if ($pending_fees['fee_circular_id'] == $fee_circular_id) {
//                                            foreach ($pending_fees['arr_fee_detail'] as $fee_detail) {
//                                                if (!empty($fee_circular_between)) {
//                                                    if (in_array($fee_detail['fee_sub_circular_name'],$fee_circular_between))
//                                                        $fine_amount += $fee_detail['installment_fine_amount'];
//                                                } else {
//                                                    $fine_amount += $fee_detail['installment_fine_amount'];
//                                                }
//                                            }
//                                        }
//                                    } else {
//                                        foreach ($pending_fees['arr_fee_detail'] as $fee_detail) {
//                                            $fine_amount += $fee_detail['installment_fine_amount'];
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                        if (isset($stu['student_fee']['paid']) && !empty($stu['student_fee']['paid'])) {
//                            foreach ($stu['student_fee']['paid'] as $paid_fees) {
//                                if (!empty($paid_fees['fee_receipt_detail'])) {
//                                    foreach ($paid_fees['fee_receipt_detail'] as $fee_detail) {
//                                        if (!empty($fee_circular_id) || !empty($fee_circular_name)) {
//                                            if ($fee_detail['fee_circular_id'] == $fee_circular_id) {
//                                                if (!empty($fee_circular_name)) {
//                                                    if ($fee_circular_name == strtolower($fee_detail['fee_circular_name'])){
//                                                        $deposite_fee += $fee_detail['installment_paid_amount'];
//                                                    }
//                                                } else {
//                                                    $deposite_fee += $fee_detail['installment_paid_amount'];
//                                                }
//                                            }
//                                        } else {
//                                            $deposite_fee += $fee_detail['installment_paid_amount'];
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//                $chq_bounce_amount = !empty($response) && isset($response['fee_detail']) ? $response['fee_detail']['cheque_bounce_charges'] : 0;
                $previous_due_fee_original = ($fee_type_id == 2 || $fee_type_id == '') && $student_fee['previous_due_fee'] > 0 ? $student_fee['previous_due_fee'] : 0;
//                $student_total_fee = $academy_fee + $fine_amount + $chq_bounce_amount + $previous_due_fee_original;
//                $student_due_fee = $student_total_fee - $deposite_fee;
//                p("Academic : " . $academy_fee, 0);
//                p("new_Previous Due : " . $previous_due_fee_original, 0);
//                p("Fine : " . $fine_amount,0);
//                p("Deposite : " . $deposite_fee,0);
//                p("Due Fee : " . $student_due_fee, 0);
//                p("$$$$$$$$", 0);

                $student_total_fee = $academy_fee + $student_fee_details['installment_fine_amount'] + $student_fee_details['cheque_bounce_charges'] + $previous_due_fee_original;
                $student_due_fee = $student_total_fee - $student_fee_details['total_paid_amount'];
                $fine_amount = $student_fee_details['installment_fine_amount'];
                $chq_bounce_amount = $student_fee_details['cheque_bounce_charges'];
                $deposite_fee = $student_fee_details['total_paid_amount'];
//                p("new_Academic : " . $academy_fee, 0);
//                p("new_Previous Due : " . $previous_due_fee_original, 0);
//                p("new_Fine : " . $fine_amount,0);
//                p("new_Deposite : " . $deposite_fee,0);
//                p("new_Due Fee : " . $student_due_fee);

                $arr_fee_data = array(
                    'student_id' => $student_fee['student_id'],
                    'enrollment_number' => $student_fee['enrollment_number'],
                    'student_name' => $student_fee['first_name'] . ' ' . $student_fee['middle_name'] . ' ' . $student_fee['last_name'],
                    'father_name' => $student_fee['father_first_name'] . ' ' . $student_fee['father_middle_name'] . ' ' . $student_fee['father_last_name'],
                    'father_contact_number' => $student_fee['father_contact_number'],
                    'mother_contact_number' => $student_fee['mother_contact_number'],
                    'class_name' => $student_fee['class_name'] . '(' . $student_fee['section_name'] . ')',
                    'academy_fee' => (int)$academy_fee,
                    'fine_amount' => $fine_amount,
                    'chq_bounce_amount' => $chq_bounce_amount,
                    'previous_due_fee' => $previous_due_fee_original,
                    'total_fee' => $student_total_fee,
                    'deposite_fee' => $deposite_fee,//$student_paid_fees['paid_net_amount'] > 0 ? $student_paid_fees['paid_net_amount'] : 0,
                    'total_due_fee' => $student_due_fee,// $pending_amount,
                    'app_accessbility_status' => $student_fee['app_accessbility_status'],
                    'fee_due_date' => $student_fee_details['fee_due_date']
                );
                $student[$key] = (object)$arr_fee_data;
                $count++;
            }
        }
//        p($student);
        return Datatables::of($student)
            ->addIndexColumn()->make(true);
    }

    /*
     * Student Due fee report  Data
     */

    public function studentDueFeeDataOld(Request $request)
    {
        $student = [];
        $class_id = Input::get('class_id');
        $student_id = Input::get('student_id');
        $fee_type_id = Input::get('fee_type_id');
        $fee_circular_id = Input::get('fee_circular_id');
        $fee_circular_name = Input::get('sub_fee_circular_id');
        $limit = Input::get('length');
        $offset = Input::get('start');
        $session = get_current_session();
        $session_id = $session['session_id'];
        $fee_circular_between = [];
        if(!empty($fee_circular_id) && !empty($fee_circular_name)){
            $fee_circular_between = get_fee_sub_circluar_between($fee_circular_id, $fee_circular_name);
        }
        if (!empty($session_id)) {
            $table = get_student_table($session_id);
            $query = DB::table($table)
                ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
                ->join('classes as c', 's.current_class_id', '=', 'c.class_id')
                ->join('sections as sec', 's.current_section_id', '=', 'sec.section_id')
                ->groupBy('s.student_id')
                ->select(
                    's.student_id', 's.first_name', 's.middle_name', 's.last_name', 's.enrollment_number',
                    's.app_accessbility_status', 'c.class_name', 'sec.section_name', 's.student_type_id', 's.new_student', 's.current_session_id', 's.previous_due_fee',
                    's.current_class_id', 'sp.father_first_name', 'sp.father_middle_name', 'sp.father_last_name', 'sp.father_contact_number', 'sp.mother_contact_number',
                )
                ->where('s.current_session_id', $session_id);
            if (!empty($class_id)) {
                $query->where('c.class_id', $class_id);
            }
//            if (!empty($fee_type_id)) {
//                $query->where('sfrd.fee_type_id', $fee_type_id);
//            }
//            if (!empty($fee_circular_id)) {
//                $query->where('sfrd.fee_circular_id', $fee_circular_id);
//            }
            if (!empty($student_id)) {
                $query->where('s.student_id', $student_id);
            }
            $arr_student_fee = $query->orderBy('c.class_order')->orderBy('s.first_name')->get();
            $arr_student_fee = json_decode($arr_student_fee, true);
            $count = 1;
            $tt_total = 0;
            $tt_deposit = 0;
            $tt_pending = 0;
            foreach ($arr_student_fee as $key => $student_fee) {
//                $student_paid_fees = StudentFeeReceipt::whereHas('feeReceiptDetails', function ($q) use ($fee_type_id, $fee_circular_id, $fee_circular_name) {
//                    if (!empty($fee_type_id)) {
//                        $q->where('fee_type_id', $fee_type_id);
//                    }
//                    if (!empty($fee_circular_id)) {
//                        $q->where('fee_circular_id', $fee_circular_id);
//                    }
//                    if (!empty($fee_circular_name)) {
//                        $q->where('fee_circular_name', $fee_circular_name);
//                    }
//                })->with(['feeReceiptDetails' => function ($q) use ($fee_type_id, $fee_circular_id, $fee_circular_name) {
//                    if (!empty($fee_type_id)) {
//                        $q->where('fee_type_id', $fee_type_id);
//                    }
//                    if (!empty($fee_circular_id)) {
//                        $q->where('fee_circular_id', $fee_circular_id);
//                    }
//                    if (!empty($fee_circular_name)) {
//                        $q->where('fee_circular_name', $fee_circular_name);
//                    }
//                }])
//                    ->select(
//                        DB::raw('sum(net_amount) as paid_net_amount')
//                    )
//                    ->where('student_id', $student_fee['student_id'])->first()->toArray();
//                $student_fee['paid_amount'] = $student_paid_fees['paid_net_amount'];
                $arr_condition = array(
                    'session_id' => $student_fee['current_session_id'],
                    'class_id' => $student_fee['current_class_id'],
                    'student_type_id' => $student_fee['student_type_id'],
                    'new_student' => $student_fee['new_student'],
                    'fee_type_id' => $fee_type_id,
                    'fee_circular_id' => $fee_circular_id,
                    'fee_circular_between' => $fee_circular_between,
                );
                $obj = new StudentFeeReceiptNewController();
                $request->request->add([
                    'arr_student' => [$student_fee['student_id']],
                    'fee_receipt_date' => date('d/m/Y'),
                    'fee_type_id' => NULL,
                    'request_type' => 'API',
                    'fee_circular_id' => $fee_circular_id,
                    'fee_circular_between' => $fee_circular_between,
                ]);
//                p($fee_circular_name, 0);
                $response = $obj->studentFeeDetail();
//                p($response);
                $academy_fee = get_student_total_academic_fee($arr_condition);
                $fine_amount = 0;
                $deposite_fee = 0;
                if (!empty($response) && isset($response['fee'])) {
                    foreach ($response['fee'] as $stu) {
                        if (isset($stu['student_fee']['pending']) && !empty($stu['student_fee']['pending'])) {
                            foreach ($stu['student_fee']['pending'] as $pending_fees) {
                                if (!empty($pending_fees)) {
                                    if (!empty($fee_circular_id)) {
                                        if ($pending_fees['fee_circular_id'] == $fee_circular_id) {
                                            foreach ($pending_fees['arr_fee_detail'] as $fee_detail) {
                                                if (!empty($fee_circular_between)) {
                                                    if (in_array($fee_detail['fee_sub_circular_name'],$fee_circular_between))
                                                        $fine_amount += $fee_detail['installment_fine_amount'];
                                                } else {
                                                    $fine_amount += $fee_detail['installment_fine_amount'];
                                                }
                                            }
                                        }
                                    } else {
                                        foreach ($pending_fees['arr_fee_detail'] as $fee_detail) {
                                            $fine_amount += $fee_detail['installment_fine_amount'];
                                        }
                                    }
                                }
                            }
                        }
                        if (isset($stu['student_fee']['paid']) && !empty($stu['student_fee']['paid'])) {
                            foreach ($stu['student_fee']['paid'] as $paid_fees) {
                                if (!empty($paid_fees['fee_receipt_detail'])) {
                                    foreach ($paid_fees['fee_receipt_detail'] as $fee_detail) {
                                        if (!empty($fee_circular_id) || !empty($fee_circular_name)) {
                                            if ($fee_detail['fee_circular_id'] == $fee_circular_id) {
                                                if (!empty($fee_circular_name)) {
                                                    if ($fee_circular_name == strtolower($fee_detail['fee_circular_name'])){
                                                        $deposite_fee += $fee_detail['installment_paid_amount'];
                                                    }
                                                } else {
                                                    $deposite_fee += $fee_detail['installment_paid_amount'];
                                                }
                                            }
                                        } else {
                                            $deposite_fee += $fee_detail['installment_paid_amount'];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
//                $fine_amount = !empty($response) && isset($response['fee_detail']) ? $response['fee_detail']['late_fee'] : 0;
                $chq_bounce_amount = !empty($response) && isset($response['fee_detail']) ? $response['fee_detail']['cheque_bounce_charges'] : 0;
                $previous_due_fee_original = ($fee_type_id == 2 || $fee_type_id == '') && $student_fee['previous_due_fee'] > 0 ? $student_fee['previous_due_fee'] : 0;
                $student_total_fee = $academy_fee + $fine_amount + $chq_bounce_amount + $previous_due_fee_original;
//                $student_due_fee = !empty($response) && isset($response['fee_detail']) ? $response['fee_detail']['payable_fee'] : 0;
                $student_due_fee = $student_total_fee - $deposite_fee;
//                p("Academic : " . $academy_fee, 0);
//                p("Fine : " . $fine_amount,0);
//                p("Deposite : " . $deposite_fee,0);
//                p("Due Fee : " . $student_due_fee);

                $arr_fee_data = array(
                    'student_id' => $student_fee['student_id'],
                    'enrollment_number' => $student_fee['enrollment_number'],
                    'student_name' => $student_fee['first_name'] . ' ' . $student_fee['middle_name'] . ' ' . $student_fee['last_name'],
                    'father_name' => $student_fee['father_first_name'] . ' ' . $student_fee['father_middle_name'] . ' ' . $student_fee['father_last_name'],
                    'father_contact_number' => $student_fee['father_contact_number'],
                    'mother_contact_number' => $student_fee['mother_contact_number'],
                    'class_name' => $student_fee['class_name'] . '(' . $student_fee['section_name'] . ')',
                    'academy_fee' => (int)$academy_fee,
                    'fine_amount' => $fine_amount,
                    'chq_bounce_amount' => $chq_bounce_amount,
                    'previous_due_fee' => $previous_due_fee_original,
                    'total_fee' => $student_total_fee,
                    'deposite_fee' => $deposite_fee,//$student_paid_fees['paid_net_amount'] > 0 ? $student_paid_fees['paid_net_amount'] : 0,
                    'total_due_fee' => $student_due_fee,// $pending_amount,
                    'app_accessbility_status' => $student_fee['app_accessbility_status'],
                );
                $student[$key] = (object)$arr_fee_data;
                $count++;
            }
        }
        return Datatables::of($student)
            ->addIndexColumn()->make(true);
    }

    /*
     * Send due fee sms
     */

    public function sendDueFeeSMS()
    {
        $arr_fee_data = Input::get('arr_fee_data');
        $response = '';
        $arr_message = \Config('custom.due_fee_message');
        foreach ($arr_fee_data as $key => $fee_data) {
            $message_text = '';
            $father_contact_no = $fee_data['father_contact_number'];
            $mother_contact_no = $fee_data['mother_contact_number'];
            $due_fee = $fee_data['due_fee'];
            $enrollment_number = $fee_data['enrollment_number'];
            $arr_contact_no = '';
            if (!empty($father_contact_no)) {
                $arr_contact_no = $father_contact_no;
            }
            if (!empty($mother_contact_no)) {
                $arr_contact_no = !empty($father_contact_no) ? $father_contact_no . ', ' . $mother_contact_no : $mother_contact_no;
            }
            if ($due_fee > 0 && !empty($arr_contact_no)) {
                $message_text = str_replace("enrollment_number", $enrollment_number, $arr_message[1]) . ' ' . $due_fee . '. ' . $arr_message[2];  // message lines for due fee
                $response = send_sms($arr_contact_no, $message_text);
            }
        }
        $return_arr = array(
            'status' => 'success',
            'message' => $response
        );
        return response()->json($return_arr);
    }

    /*
     * Student Transport Due fee report
     */

    public function studentTransportDueFee()
    {
        $data = array(
            'arr_class' => add_blank_option(get_all_classes(), '-- Select class --'),
            'arr_month' => add_blank_option(get_transport_fee_months(), '-- Select month --'),
            'arr_vehicle' => add_blank_option(get_vehicle_list(), '-- Select vehicle --'),
            'redirect_url' => 'admin/student-fee-receipt/add',
            'arr_payment_status' => add_blank_option(arr_payment_status(), '-- Payment status --'),
        );
        return view('backend.report.student-transport-due-fee-report')->with($data);
    }

    /*
     * Student Transport Due fee report  Data
     */

    public function studentTransportDueFeeData()
    {
        $student = [];
        $class_id = Input::get('class_id');
        $month_id = Input::get('month_id');
        $vehicle_id = Input::get('vehicle_id');
        $session = get_current_session();
        $session_id = $session['session_id'];
        if (!empty($session_id)) {
            $arr_student = [];
            if (!empty($vehicle_id)) {
                $arr_vehicle_student = DB::table('student_vehicle_assigned as sva')
                    ->join('student_vehicle_assigned_details as svad', 'sva.student_vehicle_assigned_id', '=', 'svad.assigned_id')
                    ->where('sva.vehicle_id', $vehicle_id)
                    ->where('sva.session_id', $session_id)
                    ->select('student_id')
                    ->get()->toArray();

                $arr_student = array_unique(array_column($arr_vehicle_student, 'student_id'));
            }

            $table = get_student_table($session_id);
            $query = DB::table('transport_fee_details as tfd')
                ->join('transport_fees as tf', 'tf.transport_fee_id', '=', 'tfd.transport_fee_id')
                ->join($table . ' as s', 'tf.student_id', '=', 's.student_id')
                ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
                ->join('classes as c', 's.current_class_id', '=', 'c.class_id')
                ->join('sections as sec', 's.current_section_id', '=', 'sec.section_id')
                ->groupBy('tf.student_id')
                ->select('tf.student_id', 's.first_name', 's.middle_name', 's.last_name', 's.enrollment_number', 'c.class_name', 'sec.section_name', 's.student_type_id', 's.new_student', 's.current_session_id', 's.current_class_id', 'sp.father_first_name', 'sp.father_middle_name', 'sp.father_last_name', 'sp.father_contact_number', 'sp.mother_contact_number'
                    , DB::raw("SUM(tfd.paid_fee_amount) as transport_paid_amount"))
                ->where('tf.session_id', $session_id);
            if (!empty($class_id)) {
                $query->where('tf.class_id', $class_id);
            }
            if (!empty($month_id)) {
                $query->where('tfd.month_id', $month_id);
            }
            if (!empty($vehicle_id)) {
                $query->whereIn('tf.student_id', $arr_student);
            }
            $arr_student_fee = $query->get();
            //	    p($arr_student_fee);
            $arr_student_fee = json_decode($arr_student_fee, true);
            $count = 1;
            foreach ($arr_student_fee as $key => $student_fee) {
                $arr_data = array(
                    'session_id' => $student_fee['current_session_id'],
                    'student_id' => $student_fee['student_id'],
                    'month_id' => $month_id,
                );


                $all_month_fee = get_student_transport_fee($arr_data);
                $transport_total_fee = !empty($all_month_fee) ? array_sum($all_month_fee) : 0;
                //	        p($transport_total_fee);
                $pending_amount = $transport_total_fee - $student_fee['transport_paid_amount'];
                $pending_amount = $pending_amount > 0 ? $pending_amount : 0;
                $arr_fee_data = array(
                    'student_id' => $student_fee['student_id'],
                    'enrollment_number' => $student_fee['enrollment_number'],
                    'student_name' => $student_fee['first_name'] . ' ' . $student_fee['middle_name'] . ' ' . $student_fee['last_name'],
                    'father_name' => $student_fee['father_first_name'] . ' ' . $student_fee['father_middle_name'] . ' ' . $student_fee['father_last_name'],
                    'deposite_fee' => $student_fee['transport_paid_amount'],
                    'father_contact_number' => $student_fee['father_contact_number'],
                    'mother_contact_number' => $student_fee['mother_contact_number'],
                    'total_fee' => $transport_total_fee,
                    'due_fee' => $pending_amount,
                    'class_name' => $student_fee['class_name'] . '(' . $student_fee['section_name'] . ')',
                );
                $student[$key] = (object)$arr_fee_data;
                $count++;
            }
        }
        return Datatables::of($student)->addIndexColumn()->make(true);
    }

    /*
     * Total fee out standing
     */

    public function totalFeeOutStanding()
    {

        $data = array(
            'arr_class' => add_blank_option(get_all_classes(), '-- Select class --'),
            'arr_month' => add_blank_option(get_transport_fee_months(), '-- Select month --'),
        );
        return view('backend.report.total-fee-outstanding')->with($data);
    }

    // fee view list
    public function totalFeeOutStandingData(Request $request)
    {
        $student = [];
        $class_id = Input::get('class_id');
        $month_id = Input::get('month_id');
        $session = get_current_session();
        $session_id = $session['session_id'];
        if (!empty($session_id)) {
            $table = get_student_table($session_id);
            $student_query = DB::table($table)
                ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
                ->join('classes as c', 's.current_class_id', '=', 'c.class_id')
                ->join('sections as sec', 's.current_section_id', '=', 'sec.section_id')
                ->select(
                    's.student_id', 's.first_name', 's.middle_name', 's.last_name', 's.enrollment_number',
                    's.app_accessbility_status', 'c.class_name', 'sec.section_name', 's.student_type_id', 's.new_student', 's.current_session_id', 's.previous_due_fee',
                    's.current_class_id', 'sp.father_first_name', 'sp.father_middle_name', 'sp.father_last_name', 'sp.father_contact_number', 'sp.mother_contact_number'
                )
                ->where('s.current_session_id', $session_id);
            if (!empty($class_id)) {
                $student_query->where('c.class_id', $class_id);
            }
            if (!empty($student_id)) {
                $student_query->where('s.student_id', $student_id);
            }
            $arr_student = $student_query->orderBy('c.class_order')->orderBy('s.first_name')->get();
            $arr_student = json_decode($arr_student, true);
//            $query = DB::table('student_fee_receipts as sfr')
//                ->join('student_fee_receipt_details as sfrd', 'sfrd.student_fee_receipt_id', '=', 'sfr.student_fee_receipt_id')
//                ->join($table . ' as s', 'sfr.student_id', '=', 's.student_id')
//                ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
//                ->join('classes as c', 's.current_class_id', '=', 'c.class_id')
//                ->join('sections as sec', 's.current_section_id', '=', 'sec.section_id')
//                ->groupBy('sfr.student_id')
//                ->select('sfr.student_id', 's.first_name', 's.middle_name', 's.last_name', 's.enrollment_number', 'c.class_name', 'sec.section_name', 's.student_type_id', 's.new_student', 's.current_session_id', 's.current_class_id', 'sp.father_contact_number', 'sp.mother_contact_number', 'sp.father_first_name', 'sp.father_middle_name', 'sp.father_last_name'
//                    , DB::raw('SUM(sfrd.installment_paid_amount) as paid_amount')
//                    , DB::raw("(SELECT SUM(tf.total_paid_amount) FROM transport_fees as tf WHERE tf.student_id = s.student_id AND tf.fee_status = 1 AND tf.session_id = " . $session_id . " group by tf.student_id ) as transport_paid_amount")
//                )
//                ->where('sfr.session_id', $session_id)
//                ->where('sfr.fee_status', 1);
//
//            if (!empty($class_id)) {
//                $query->where('sfr.class_id', $class_id);
//            }
//
//            $arr_student_fee = $query->get();
//            //	    p($arr_student_fee);
//            $arr_student_fee = json_decode($arr_student_fee, true);

            $sr_n = 1;
            $return_data = [];
            foreach ($arr_student as $key => $student) {
                $student_paid_fees = StudentFeeReceipt::whereHas('feeReceiptDetails')
                    ->with('feeReceiptDetails')
                    ->select(DB::raw('sum(net_amount) as paid_net_amount'))
                    ->where('student_id', $student['student_id'])->first()->toArray();
                // academic total paid
                $academic_paid = $student_paid_fees['paid_net_amount'];

                $arr_condition = array(
                    'student_id' => $student['student_id'],
                    'session_id' => $student['current_session_id'],
                    'class_id' => $student['current_class_id'],
                    'student_type_id' => $student['student_type_id'],
                    'new_student' => $student['new_student'],
                );
                $student_total_fee = get_student_total_academic_fee($arr_condition);

                $obj = new StudentFeeReceiptNewController();
                $request->request->add(['arr_student' => [$student['student_id']], 'fee_receipt_date' => date('d/m/Y'), 'fee_type_id' => NULL, 'request_type' => 'API']);
                $response = $obj->studentFeeDetail();
                $fine_amount = !empty($response) && isset($response['fee_detail']) ? $response['fee_detail']['late_fee'] : 0;
                $chq_bounce_amount = !empty($response) && isset($response['fee_detail']) ? $response['fee_detail']['cheque_bounce_charges'] : 0;
                $previous_due_fee = $student['previous_due_fee'] > 0 ? $student['previous_due_fee'] : 0;
                // academic total fee
                $academic_fee = $student_total_fee + $fine_amount + $chq_bounce_amount + $previous_due_fee;
                // academic total due
                $academic_due = !empty($response) && isset($response['fee_detail']) ? $response['fee_detail']['payable_fee'] : 0;

                // transport total fee
//                $all_month_fee = get_student_transport_fee($arr_condition);
//                $transport_total_fee = !empty($all_month_fee) ? array_sum($all_month_fee) : 0;

                // academic pending
//                $academic_pending = $student_academic_fee - $student['paid_amount'];
//                $academic_pending = $academic_pending > 0 ? $academic_pending : 0;

                // transport pending
//                $transport_pending = $transport_total_fee - $student['transport_paid_amount'];
//                $transport_pending = $transport_pending > 0 ? $transport_pending : 0;
                // total fee and paid fee
//                $transport_total_fee = 0;
//                $transport_pending = 0;
//                $total_paid = round($student['paid_amount'] + $student['transport_paid_amount']);
//                $total_fee = round($transport_total_fee + $student_academic_fee);
//                // total pending fee
//                $total_pending = $total_fee - $total_paid;
//                $total_pending = $total_pending > 0 ? $total_pending : 0;
                $transport_fee = 0;
                $transport_paid = 0;
                $transport_due = 0;

                $total_fee = $academic_fee + $transport_fee;
                $total_paid = $academic_paid + $transport_paid;
                $total_due = $academic_due + $transport_due;

                $arr_fee_data = array(
                    'student_id' => $student['student_id'],
                    'sr_n' => $sr_n,
                    'enrollment_number' => $student['enrollment_number'],
                    'class_name' => $student['class_name'] . '(' . $student['section_name'] . ')',
                    'student_name' => $student['first_name'] . ' ' . $student['middle_name'] . ' ' . $student['last_name'],
                    'father_name' => $student['father_first_name'] . ' ' . $student['father_middle_name'] . ' ' . $student['father_last_name'],
                    'father_contact_number' => $student['father_contact_number'],
                    'mother_contact_number' => $student['mother_contact_number'],
                    'academic_amount' => (int)$academic_fee,
                    'academic_paid' => (int)$academic_paid,
                    'academic_pending' => (int)$academic_due,
                    'transport_total' => (int)$transport_fee,
                    'transport_paid' => (int)$transport_paid,
                    'transport_pending' => (int)$transport_due,
                    'total_fee' => (int)$total_fee,
                    'total_paid' => (int)$total_paid,
                    'due_fee' => (int)$total_due,
                );
                $sr_n++;
                $return_data[] = (object)$arr_fee_data;
            }
        }
        return Datatables::of($return_data)
            ->addIndexColumn()
            ->make(true);
    }

    //        DayBook Report
    public function dayBookReport()
    {
        $data = [];
        return view('backend.report.day-book-report')->with($data);
    }

    public function dayBookReportData()
    {
        $arr_daybook_data = [];
        $from_date = get_formatted_date(Input::get('from_date'), 'database');
        $to_date = get_formatted_date(Input::get('to_date'), 'database');
        $session = get_current_session();
        if (!empty($session)) {
            //academic fee
            $session_id = $session['session_id'];
            $table = get_student_table($session_id);
//            p($session_id,0);
//            p($table,0);
            $arr_daybook_academic = DB::table('student_fee_receipts as sfr')
                ->join($table . ' as s', 'sfr.student_id', '=', 's.student_id')
                ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
                ->join('payment_modes as pm', 'sfr.payment_mode_id', '=', 'pm.payment_mode_id')
                ->join('classes as c', 's.current_class_id', '=', 'c.class_id')
                ->join('sections as sec', 's.current_section_id', '=', 'sec.section_id')
                ->select('sfr.student_fee_receipt_id', 'sfr.net_amount', 'pm.payment_mode', 'sfr.student_id', 'sfr.receipt_number', 'sfr.receipt_date', 's.enrollment_number', 'c.class_name', 'sec.section_name', 'sp.father_contact_number')
//                ->addSelect(DB::raw("CONCAT(s.first_name,' ',s.middle_name,' ',s.last_name) as student_name"))
//                ->addSelect(DB::raw("CONCAT(sp.father_first_name,' ',sp.father_middle_name,' ',sp.father_last_name) as father_name"))
//                ->addSelect(DB::raw("CONCAT(sp.mother_first_name,' ',sp.mother_middle_name,' ',sp.mother_last_name) as mother_name"))
                ->addSelect("s.first_name", "s.middle_name", "s.last_name")
                ->addSelect("sp.father_first_name", "sp.father_middle_name", "sp.father_last_name")
                ->addSelect("sp.mother_first_name", "sp.mother_middle_name", "sp.mother_last_name")
                ->where('sfr.session_id', $session_id)
                ->where('s.current_session_id', $session_id)
                ->where(function ($query) use ($from_date, $to_date) {
                    if (!empty($from_date) && !empty($to_date)) {
                        $query->whereBetween('receipt_date', array($from_date, $to_date));
                    } else if (!empty($from_date)) {
                        $query->where('receipt_date', '>=', $from_date);
                    } else if (!empty($to_date)) {
                        $query->where('receipt_date', '<=', $to_date);
                    }
                })
                ->where('sfr.fee_status', 1)->orderBy('sfr.created_at')->get()->toArray();

            // transport fee
            $arr_daybook_transport = DB::table('transport_fees as tf')
                ->join($table . ' as s', 'tf.student_id', '=', 's.student_id')
                ->join('student_parents as sp', 's.student_parent_id', '=', 'sp.student_parent_id')
                ->join('payment_modes as pm', 'tf.payment_mode_id', '=', 'pm.payment_mode_id')
                ->join('classes as c', 's.current_class_id', '=', 'c.class_id')
                ->join('sections as sec', 's.current_section_id', '=', 'sec.section_id')
                ->select('tf.transport_fee_id', 'tf.total_paid_amount as net_amount', 'pm.payment_mode', 'tf.student_id', 'tf.receipt_number', 'tf.receipt_date', 's.enrollment_number', 'c.class_name', 'sec.section_name', 'sp.father_contact_number')
//                ->addSelect(DB::raw("CONCAT(s.first_name,' ',s.middle_name,' ',s.last_name) as student_name"))
//                ->addSelect(DB::raw("CONCAT(sp.father_first_name,' ',sp.father_middle_name,' ',sp.father_last_name) as father_name"))
//                ->addSelect(DB::raw("CONCAT(sp.mother_first_name,' ',sp.mother_middle_name,' ',sp.mother_last_name) as mother_name"))
                ->addSelect("s.first_name", "s.middle_name", "s.last_name")
                ->addSelect("sp.father_first_name", "sp.father_middle_name", "sp.father_last_name")
                ->addSelect("sp.mother_first_name", "sp.mother_middle_name", "sp.mother_last_name")
                ->where('tf.session_id', $session_id)
                ->where('s.current_session_id', $session_id)
                ->where(function ($query) use ($from_date, $to_date) {
                    if (!empty($from_date) && !empty($to_date)) {
                        $query->whereBetween('receipt_date', array($from_date, $to_date));
                    } else if (!empty($from_date)) {
                        $query->where('receipt_date', '>=', $from_date);
                    } else if (!empty($to_date)) {
                        $query->where('receipt_date', '<=', $to_date);
                    }
                })
                ->where('tf.fee_status', 1)->orderBy('tf.created_at')->get()->toArray();

            $arr_daybook = array_merge($arr_daybook_academic, $arr_daybook_transport);
            //	    p($arr_daybook);
            $count = 1;
            foreach ($arr_daybook as $key => $daybook) {
                $daybook = (array)$daybook;
                $daybook_data = array(
                    'student_id' => $daybook['student_id'],
                    'receipt_number' => $daybook['receipt_number'],
                    'sn' => $count,
                    'receipt_date' => get_formatted_date($daybook['receipt_date'], 'display'),
                    'amount' => (int)$daybook['net_amount'],
                    'enrollment_number' => $daybook['enrollment_number'],
                    'student_name' => $daybook['first_name'] . " " . $daybook['middle_name'] . " " . $daybook['last_name'],
                    'father_name' => $daybook['father_first_name'] . " " . $daybook['father_middle_name'] . " " . $daybook['father_last_name'],
                    'father_contact_number' => $daybook['father_contact_number'],
                    'mother_name' => $daybook['mother_first_name'] . " " . $daybook['mother_middle_name'] . " " . $daybook['mother_last_name'],
                    'class_name' => $daybook['class_name'] . '(' . $daybook['section_name'] . ')',
                    'payment_mode' => ucfirst($daybook['payment_mode']),
                    'type' => isset($daybook['student_fee_receipt_id']) ? 'Academic' : 'Transport',
                );
                $arr_daybook_data[$key] = (object)$daybook_data;
                $count++;
            }
        }
        return Datatables::of($arr_daybook_data)->addIndexColumn()->make(true);
    }

    public function studentSetAccessbilityStatus(Request $request)
    {
        try {
            $student_ids = Arr::pluck(Input::get('arr_student_data'), 'student_id');
            $student = Student::whereIn('student_id', $student_ids)->where("student_left", "No")->update(["app_accessbility_status" => $request->status]);
            $cron = new CronController;
            $cron->pushNotificationInactiveStudent($student_ids);
            return array('result' => 'success');
        } catch (\Exception $e) {
            return array('result' => 'error', 'message' => $e->getMessage());
        }
    }

    /*
     * Send due fee Notification
     */

    public function sendDueFeeNotification()
    {
        $arr_fee_data = Input::get('arr_fee_data');
        $reminder_button = Input::get('reminder_button');
        $response = '';
        $arr_message = \Config('custom.due_fee_message');
        $notification = new CronController();
        foreach ($arr_fee_data as $key => $fee_data) {
            $message_text = '';
            $father_contact_no = $fee_data['father_contact_number'];
            $mother_contact_no = $fee_data['mother_contact_number'];
            $due_fee = $fee_data['total_due_fee'];
            $enrollment_number = $fee_data['enrollment_number'];
            $arr_contact_no = '';
            if (!empty($father_contact_no)) {
                $arr_contact_no = $father_contact_no;
            }
            if (!empty($mother_contact_no)) {
                $arr_contact_no = !empty($father_contact_no) ? $father_contact_no . ', ' . $mother_contact_no : $mother_contact_no;
            }
            if ($due_fee > 0 && !empty($arr_contact_no)) {
                $message_text = str_replace("enrollment_number", $enrollment_number, $arr_message[1]) . ' ' . $due_fee . '. ' . $arr_message[2];  // message lines for due fee
//                 $response = send_sms($arr_contact_no, $message_text);
                $notification->pushNotificationStudentFee($fee_data['student_id'], $due_fee, $reminder_button);
            }
        }
        $return_arr = array(
            'status' => 'success',
            'message' => ""
        );
        return response()->json($return_arr);
    }
}
