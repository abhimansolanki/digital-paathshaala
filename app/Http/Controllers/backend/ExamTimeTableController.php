<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\ExamTimeTable;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class ExamTimeTableController extends Controller
    {

        public function __construct()
        {
            
        }
        
        public function index()
        {
            $data                = array(
                'redirect_url'   => url('admin/exam-time-table/'),
            );
            return view('backend.exam-time-table.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {
            $exam_time_table_id = null;
            if (!empty($id))
            {
                $decrypted_exam_time_table_id = get_decrypted_value($id, true);
                $exam_time_table              = $this->getExamTimeTable($decrypted_exam_time_table_id);
                $exam_time_table              = isset($exam_time_table[0]) ? $exam_time_table[0] : [];
                if (!$exam_time_table)
                {
                    return redirect('admin/exam-time-table')->withError('Create Exam not found!');
                }
                $encrypted_exam_time_table_id = get_encrypted_value($exam_time_table['exam_time_table_id'], true);
                $save_url                     = url('admin/exam-time-table/save/' . $encrypted_exam_time_table_id);
                $submit_button                = 'Update';
                $exam_time_table_id           = $decrypted_exam_time_table_id;
            }
            else
            {
                $save_url      = url('admin/exam-time-table/save');
                $submit_button = 'Save';
            }

            $exam_time_table['arr_exam'] = add_blank_option(get_all_exam(), '-- Select Exam --');
            $data                        = array(
                'save_url'        => $save_url,
                'submit_button'   => $submit_button,
                'exam_time_table' => $exam_time_table,
                'redirect_url'    => url('admin/exam-time-table/'),
            );
            return view('backend.exam-time-table.add')->with($data);
        }

        /*
         * Add data for exam schedule 
         */

        public function save(Request $request, $id = NULL)
        {
            $decrypted_exam_time_table_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $exam_time_table = ExamTimeTable::find($decrypted_exam_time_table_id);

                if (!$exam_time_table)
                {
                    return redirect('/admin/exam-time-table/')->withError('ExamTimeTable not found!');
                }
                $success_msg = 'Exam Time Table updated successfully!';
            }
            else
            {
                $exam_time_table = New ExamTimeTable;
                $success_msg     = 'Exam Time Table saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'title'   => 'required|unique:exam_time_tables,title,' . $decrypted_exam_time_table_id . ',exam_time_table_id',
                    'exam_id' => 'required',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction(); //Start transaction!
                try
                {

                    $session                     = db_current_session();
                    $exam_time_table->title      = Input::get('title');
                    $exam_time_table->exam_id    = Input::get('exam_id');
                    $exam_time_table->publish    = $request->has(('publish')) ? 1 : 0;
                    $exam_time_table->session_id = $session['session_id'];
                    $exam_time_table->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withInput()->withErrors($error_message);
                }
                DB::commit();
            }
            return redirect('admin/exam-time-table')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $exam_time_table_id = Input::get('exam_time_table_id');
            $exam               = ExamTimeTable::find($exam_time_table_id);
            if ($exam)
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $exam->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Exam deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Exam not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $arr_time_table     = [];
            $limit              = Input::get('length');
            $offset             = Input::get('start');
            $arr_exam_data      = $this->getExamTimeTable($exam_time_table_id = null, $offset, $limit);
            foreach ($arr_exam_data as $key => $exam_data)
            {
                $arr_time_table[] = (object) $exam_data;
            }
            return Datatables::of($arr_time_table)
                    ->addColumn('action', function ($arr_time_table)
                    {
                        $encrypted_exam_time_table_id = get_encrypted_value($arr_time_table->exam_time_table_id, true);
                        return '<a title="Edit" id="deletebtn1" href="' . url('admin/exam-time-table/add/' . $encrypted_exam_time_table_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $arr_time_table->exam_time_table_id . '"><i class="fa fa-trash"></i></button>'
                            . '<a title="Map with class" id="deletebtn1" href="' . url('admin/exam-time-table-detail/add/' . $encrypted_exam_time_table_id) . '" class="btn btn-success"><i class="fa fa-link" ></i></a>';
                    })->make(true);
        }

        public function getExamTimeTable($exam_time_table_id = null, $offset = null, $limit = null)
        {
            $return        = [];
            $arr_exam_data = ExamTimeTable::where(function($query) use ($exam_time_table_id)
                    {
                        if (!empty($exam_time_table_id))
                        {
                            $query->where('exam_time_table_id', $exam_time_table_id);
                        }
                    })->with('timeTableExam')->with(['examTimeTableDetails' => function($query){
                        $query->with('examTimeTableClass');
                        $query->with('examTimeTableSection');
                    }])->select('*', DB::raw('CASE WHEN publish = 1 THEN "Yes" ELSE "No" END AS publish_status'))->get();
            foreach ($arr_exam_data as $key => $time_table)
            {
                $arr = [];
                if(!empty($time_table['examTimeTableDetails']))
                {
                    $counter = 0;
                    foreach( $time_table['examTimeTableDetails'] as $key => $value)
                    {
                        $arr[$counter]['id'] = get_encrypted_value($value['detail_id'], true);
                        $arr[$counter]['details'] = $value;
                        $counter++;
                    }
                }
                $return[] = array(
                    'exam_time_table_id' => $time_table['exam_time_table_id'],
                    'encrypted_exam_time_table_id' => get_encrypted_value($time_table['exam_time_table_id'], true),
                    'title'              => $time_table['title'],
                    'publish'            => $time_table['publish'],
                    'publish_status'     => $time_table['publish_status'],
                    'exam_id'            => $time_table['exam_id'],
                    'exam_name'          => $time_table['timeTableExam']['exam_name'],
                    'exam_details'       => $arr,
                );
            }
            // p($return);
            return $return;
        }

    }
    