<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Datatables;
     use Illuminate\Support\Facades\Input;

    class BirthdayController extends Controller
    {

        public function student()
        {
            $arr_student_type = \Config::get('custom.birthday_student_type');
            $data['birthday_student_type'] = $arr_student_type;
            return view('backend.birthday.student',$data);
        }

        public function studentBirthday(Request $request)
        {
            
            $student      = [];
            $birthday_stu = $request->get('student_type');
            $arr_students = get_student_data(array(),null, null,$birthday_stu);
               
            foreach ($arr_students as $key => $arr_student)
            {
                $student[$key] = (object) $arr_student;
            }
            return Datatables::of($student)
                    ->addColumn('checkbox', function ($student)
                    {
                        return'<input type="checkbox" name="student_birthday" value="'.$student->student_id.'" class="send_sms_student">';
                    })
//                    ->addColumn('checkbox', function ($student)
//                    {
//                        return '<a title="Edit" id="deletebtn1" href="student/add/' . $encrypted_student_id . '" class="btn btn-success centerAlign"><i class="fa fa-edit" ></i></a>';
//                    })
                    ->rawColumns(['checkbox'])->make(true);
        }
    }
    