<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Route;
    use App\Model\backend\StopPoint;
    use App\Model\backend\StudentVehicleAssigned;
    use App\Model\backend\Student;
    use App\Model\backend\StudentVehicleAssignedDetail;
    use Illuminate\Support\Facades\DB;

    class HistoryControllerOld extends Controller
    {

        public function __construct()
        {
            
        }

        function getSession()
        {
            $current_date = date('Y-m-d');
            $current      = db_current_session();
            $previous     = (array) DB::table('sessions')
                    ->where('session_status', '!=', 1)
                    ->where('end_date', '<', $current_date)
                    ->select('session_id', 'session_year', 'start_date', 'end_date', 'session_status')
                    ->orderBy('end_date', 'DESC')->first();
            $return  = [];
            if (!empty($current['session_id']) && !empty($previous['session_id']))
            {
                $return = array(
                    'current'  => $current,
                    'previous' => $previous,
                );
            }
            return $return;
        }

        function vehicleHistory()
        {
            $session           = $this->getSession();
            $arr_stop_point_id = [];
            if (!empty($session))
            {
                //
                DB::beginTransaction(); //Start transaction!
                try
                {
                    // history of route and its stop point assign
                    $arr_route_data = Route::where('session_id', $session['previous']['session_id'])->with('routeStopPoint')->get();
                    foreach ($arr_route_data as $key => $route_data)
                    {
                        $route               = new Route;
                        $route->session_id   = $session['current']['session_id'];
                        $route->route        = $route_data['route'];
                        $route->previous_id  = $route_data['route_id'];
                        $route->route_status = $route_data['route_status'];
                        if ($route->save())
                        {
                            $route_details = [];
                            foreach ($route_data['routeStopPoint'] as $key => $stop_point_data)
                            {
                                $stop_point                  = new StopPoint;
                                $stop_point->session_id      = $session['current']['session_id'];
                                $stop_point->stop_point      = $stop_point_data['stop_point'];
                                $stop_point->stop_fair       = $stop_point_data['stop_fair'];
                                $stop_point->main_stop_point = $stop_point_data['main_stop_point'];
                                $stop_point->stop_status     = $stop_point_data['stop_status'];
                                $stop_point->previous_id     = $stop_point_data['stop_point_id'];
                                $route_details[]             = $stop_point;
                            }
                            if (!empty($route_details))
                            {
                                $route->routeStopPoint()->saveMany($route_details);
                                // last inserted id of stop point
                                foreach ($route_details as $key => $value)
                                {
                                    $arr_stop_point_id[$value['previous_id']] = $value['stop_point_id'];
                                }
                            }
                        }
                    }

                    // history of vehicle assign
                    $arr_assigned_id = [];
                    if (!empty($arr_stop_point_id))
                    {
                        $arr_vehicle_assigned = StudentVehicleAssigned::where('session_id', $session['previous']['session_id'])->with('vehicleAssignedStudentDetail')->get();
                        foreach ($arr_vehicle_assigned as $key => $assigned)
                        {
                            $vehicle_assigned                = new StudentVehicleAssigned;
                            $vehicle_assigned->session_id    = $session['current']['session_id'];
                            $vehicle_assigned->vehicle_id    = $assigned['vehicle_id'];
                            $vehicle_assigned->stop_point_id = $arr_stop_point_id[$assigned['stop_point_id']];
                            $vehicle_assigned->pickup_time   = $assigned['pickup_time'];
                            $vehicle_assigned->return_time   = $assigned['return_time'];

                            if ($vehicle_assigned->save())
                            {
                                $arr_assigned_detail = [];
                                foreach ($assigned['vehicleAssignedStudentDetail'] as $key => $detail_data)
                                {
                                    $assigned_detail              = new StudentVehicleAssignedDetail;
                                    $assigned_detail->join_date   = $detail_data['join_date'];
                                    $assigned_detail->join_date   = $detail_data['join_date'];
                                    $assigned_detail->assigned_to = $detail_data['assigned_to'];
                                    $assigned_detail->student_id  = $detail_data['student_id'];
                                    $assigned_detail->employee_id = $detail_data['employee_id'];
                                    $arr_assigned_detail[]        = $assigned_detail;
                                }
                                if (!empty($arr_assigned_detail))
                                {
                                    $vehicle_assigned->vehicleAssignedStudentDetail()->saveMany($arr_assigned_detail);
                                }
                            }
                        }
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                }
                DB::commit();
            }
            p('done');
        }

        function studentHistory()
        {
            $session             = $this->getSession();
            $arr_student_history = [];
            if (!empty($session))
            {
                //
                DB::beginTransaction(); //Start transaction!
                try
                {
                    // history of route and its stop point assign
                    $arr_student_data = Student::where('current_session_id', $session['previous']['session_id'])->get();
                    foreach ($arr_student_data as $key => $student_data)
                    {
                        $student_history                              = [];
                        $student_history['student_id']                = $student_data['student_id'];
                        $student_history['form_number']               = $student_data['form_number'];
                        $student_history['student_parent_id']         = $student_data['student_parent_id'];
                        $student_history['enrollment_number']         = $student_data['enrollment_number'];
                        $student_history['current_session_id']        = $session['current']['session_id'];
                        $student_history['current_class_id']          = $student_data['current_class_id'];
                        $student_history['current_section_id']        = $student_data['current_section_id'];
                        $student_history['student_type_id']           = $student_data['student_type_id'];
                        $student_history['caste_category_id']         = $student_data['caste_category_id'];
                        $student_history['first_name']                = $student_data['first_name'];
                        $student_history['middle_name']               = $student_data['middle_name'];
                        $student_history['last_name']                 = $student_data['last_name'];
                        $student_history['address_line1']             = $student_data['address_line1'];
                        $student_history['address_line2']             = $student_data['address_line2'];
                        $student_history['dob']                       = $student_data['dob'];
                        $student_history['gender']                    = $student_data['gender'];
                        $student_history['birth_place']               = $student_data['birth_place'];
                        $student_history['contact_number']            = $student_data['contact_number'];
                        $student_history['blood_group']               = $student_data['blood_group'];
                        $student_history['body_sign']                 = $student_data['body_sign'];
                        $student_history['email']                     = $student_data['email'];
                        $student_history['aadhaar_number']            = $student_data['aadhaar_number'];
                        $student_history['previous_section_name']     = $student_data['previous_section_name'];
                        $student_history['previous_class_name']       = $student_data['previous_class_name'];
                        $student_history['previous_school_name']      = $student_data['previous_school_name'];
                        $student_history['previous_result']           = $student_data['previous_result'];
                        $student_history['profile_photo']             = $student_data['profile_photo'];
                        $student_history['tc_number']                 = $student_data['tc_number'];
                        $student_history['tc_date']                   = $student_data['tc_date'];
                        $student_history['admission_date']            = $student_data['admission_date'];
                        $student_history['join_date']                 = $student_data['join_date'];
                        $student_history['fee_discount']              = $student_data['fee_discount'];
                        $student_history['previous_due_fee']          = $student_data['previous_due_fee'];
                        $student_history['previous_due_fee_original'] = $student_data['previous_due_fee'];
                        $student_history['remark']                    = $student_data['remark'];
                        $student_history['fee_calculate_month']       = $student_data['fee_calculate_month'];
                        $student_history['student_left']              = $student_data['student_left'];
                        $student_history['reason']                    = $student_data['reason'];
                        $student_history['new_student']               = $student_data['new_student'];
                        $student_history['discount_date']             = $student_data['discount_date'];
                        $student_history['created_at']                = date('Y-m-d H:i:s');
                        $student_history['updated_at']                = date('Y-m-d H:i:s');
                        $arr_student_history[]                        = $student_history;
                    }
                    if (!empty($arr_student_history))
                    {
                        App\Model\backend\StudentHistory::insert($arr_student_history);
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                }
                DB::commit();
            }
            p('done');
        }

        function classSubjectHistory()
        {
            $session               = $this->getSession();
            $class_subject_history = [];
            if (!empty($session))
            {
                //
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $arr_class_subject = \App\Model\backend\ClassSubject::where('session_id', $session['previous']['session_id'])->get();
                    foreach ($arr_class_subject as $key => $class_subject)
                    {
                        $class_subject_data                          = [];
                        $class_subject_data['class_id']              = $class_subject['class_id'];
                        $class_subject_data['session_id']            = $session['current']['session_id'];
                        $class_subject_data['subject_group_id']      = $class_subject['subject_group_id'];
                        $class_subject_data['subject_id']            = $class_subject['subject_id'];
                        $class_subject_data['sub_subject_id']        = $class_subject['sub_subject_id'];
                        $class_subject_data['unassigned_student_id'] = $class_subject['unassigned_student_id'];
                        $class_subject_data['include_in_card']       = $class_subject['include_in_card'];
                        $class_subject_data['include_in_rank']       = $class_subject['include_in_rank'];
                        $class_subject_data['class_subject_order']   = $class_subject['class_subject_order'];
                        $class_subject_data['created_at']            = date('Y-m-d H:i:s');
                        $class_subject_data['updated_at']            = date('Y-m-d H:i:s');
                        $class_subject_history[]                     = $class_subject_data;
                    }
                    if (!empty($class_subject_history))
                    {
                        \App\Model\backend\ClassSubject::insert($class_subject_history);
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                }
                DB::commit();
            }
            p('done');
        }

        function classSectionHistory()
        {
            $session               = $this->getSession();
            $class_section_history = [];
            if (!empty($session))
            {
                //
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $arr_class_section = \App\Model\backend\ClassSection::where('session_id', $session['previous']['session_id'])->get();
                    foreach ($arr_class_section as $key => $class_section)
                    {
                        $class_section_data               = [];
                        $class_section_data['class_id']   = $class_section['class_id'];
                        $class_section_data['section_id'] = $session['current']['section_id'];
                        $class_section_data['session_id'] = $session['current']['session_id'];
                        $class_section_data['created_at'] = date('Y-m-d H:i:s');
                        $class_section_data['updated_at'] = date('Y-m-d H:i:s');
                        $class_section_history[]          = $class_section_data;
                    }
                    if (!empty($class_section_history))
                    {
                        \App\Model\backend\ClassSection::insert($class_section_history);
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                }
                DB::commit();
            }
            p('done');
        }

        function gradeHistory()
        {
            $session       = $this->getSession();
            $grade_history = [];
            if (!empty($session))
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $arr_grade = \App\Model\backend\Grade::where('session_id', $session['previous']['session_id'])->get();
                    foreach ($arr_grade as $key => $grade)
                    {
                        $grade_data                = [];
                        $grade_data['previous_id'] = $grade['grade_id'];
                        $grade_data['grade_name']  = $grade['grade_name'];
                        $grade_data['grade_alias'] = $grade['grade_alias'];
                        $grade_data['maximum']     = $grade['maximum'];
                        $grade_data['minimum']     = $grade['minimum'];
                        $grade_data['session_id']  = $session['current']['session_id'];
                        $grade_data['created_at']  = date('Y-m-d H:i:s');
                        $grade_data['updated_at']  = date('Y-m-d H:i:s');
                        $grade_history[]           = $grade_data;
                    }
                    if (!empty($grade_history))
                    {
                        \App\Model\backend\Grade::insert($grade_history);
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                }
                DB::commit();
            }
            p('done');
        }

    }
    