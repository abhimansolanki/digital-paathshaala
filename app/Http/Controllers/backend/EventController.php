<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Event;
    use App\Model\backend\EventGallery;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class EventController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index()
        {
            $data = array(
                'redirect_url' => url('admin/event/add'),
            );
            return view('backend.event.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {
            $event    = [];
            $event_id = null;
            if (!empty($id))
            {
                $decrypted_event_id = get_decrypted_value($id, true);
                $event              = $this->getEventData($decrypted_event_id);
                $event              = isset($event[0]) ? $event[0] : [];
                if (!$event)
                {
                    return redirect('admin/event')->withError('Event not found!');
                }
                $encrypted_event_id = get_encrypted_value($event['event_id'], true);
                $save_url           = url('admin/event/save/' . $encrypted_event_id);
                $submit_button      = 'Update';
                $event_id           = $decrypted_event_id;
            }
            else
            {
                $save_url      = url('admin/event/save');
                $submit_button = 'Save';
            }

            $arr_session            = get_session();
            $event['arr_session']   = add_blank_option($arr_session, '--Select session --');
            $event['arr_event_for'] = \Config::get('custom.arr_event_for');
            $data                   = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'event'         => $event,
                'redirect_url'  => url('admin/event/'),
            );
            return view('backend.event.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_event_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $event = Event::find($decrypted_event_id);

                if (!$event)
                {
                    return redirect('/admin/event/')->withError('Event not found!');
                }
                $success_msg = 'Event updated successfully!';
            }
            else
            {
                $event       = New Event;
                $success_msg = 'Event saved successfully!';
            }
            $session_id = Input::get('session_id');
            $validatior = Validator::make($request->all(), [
                    'event_name'       => 'required|unique:events,event_name,' . $decrypted_event_id . ',event_id,session_id,' . $session_id,
                    'session_id'       => 'required',
//                    'event_venue'      => 'required',
                    'event_start_date' => 'required',
//                    'event_end_date'   => 'required',
                    'event_for'        => 'required',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $event->session_id        = Input::get('session_id');
                    $event->event_name        = Input::get('event_name');
//                    $event->event_venue       = Input::get('event_venue');
                    $event->event_start_date  = get_formatted_date(Input::get('event_start_date'), 'database');
//                    $event->event_end_date    = get_formatted_date(Input::get('event_end_date'), 'database');
//                    $event->event_start_time  = Input::get('event_start_time');
//                    $event->event_end_time    = Input::get('event_end_time');
                    $event->event_for         = Input::get('event_for');
                    $event->event_description = Input::get('event_description');
                    $event->save();

                    if ($request->hasFile('image_name'))
                    {
                        EventGallery::where('event_id', $event->event_id)->delete();
                        $files = $request->file('image_name');
                        foreach ($files as $file)
                        {
                            $destinationPath     = public_path() . '/uploads/event-gallery';
                            $filename            = $file->getClientOriginalName();
                            $rand_string         = quick_random();
                            $filename            = $rand_string . '-' . $filename;
                            $file->move($destinationPath, $filename);
                            $event_gallery       = new EventGallery;
                            $event_gallery->name = $filename;
                            $event_gallery->getEvent()->associate($event);
                            $event_gallery->save();
                        }
                    }
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }
                DB::commit();
            }

            return redirect('admin/event')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $event_id = Input::get('event_id');
            $event    = Event::find($event_id);
            if ($event)
            {

                DB::beginTransaction(); //Start transaction!
                try
                {
                    $event->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Event deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Event not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $event     = [];
            $event_id  = array();
            $offset    = Input::get('start');
            $limit     = Input::get('length');
            $arr_event = $this->getEventData($event_id, $offset, $limit);
            foreach ($arr_event as $key => $event_data)
            {
                $event[$key] = (object) $event_data;
            }
            return Datatables::of($event)
                    ->addColumn('action', function ($event)
                    {
                        $encrypted_event_id = get_encrypted_value($event->event_id, true);
                        return '<a title="Edit" id="deletebtn1" href="event/add/' . $encrypted_event_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $event->event_id . '"><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

        public function getEventData($event_id = null, $offset = null, $limit = null)
        {
            $event_return = [];
            $session_id   = null;
            $session      = get_current_session();
            if (!empty($session))
            {
                $session_id = $session['session_id'];

                $arr_event_data = Event::where(function($query) use ($event_id)
                    {
                        if (!empty($event_id))
                        {
                            $query->where('event_id', $event_id);
                        }
                    })->where(function($query) use ($session_id)
                    {
                        if (!empty($session_id))
                        {
                            $query->where('session_id', $session_id);
                        }
                    })->with('getEventGallery')
//                    ->where(function($query) use ($limit, $offset)
//                    {
//                        if (!empty($limit))
//                        {
//                            $query->skip($offset);
//                            $query->take($limit);
//                        }
//                    })
                    ->get();

                if (!empty($arr_event_data))
                {
                    $arr_event_for = \Config::get('custom.arr_event_for');
                    foreach ($arr_event_data as $key => $event_data)
                    {
                        $event = array(
                            'event_id'          => $event_data['event_id'],
                            'session_id'        => $event_data['session_id'],
                            'event_name'        => $event_data['event_name'],
                            'event_venue'       => $event_data['event_venue'],
                            'event_start_time'  => $event_data['event_start_time'],
                            'event_end_time'    => $event_data['event_end_time'],
                            'event_for'         => $event_data['event_for'],
                            'event_for_name'    => $arr_event_for[$event_data['event_for']],
                            'event_start_date'  => get_formatted_date($event_data['event_start_date'], 'display'),
                            'event_end_date'    => get_formatted_date($event_data['event_end_date'], 'display'),
                            'event_description' => $event_data['event_description'],
                        );

                        if (isset($event_data['getEventGallery']))
                        {
                            $arr_images = [];
                            foreach ($event_data['getEventGallery'] as $key => $event_images)
                            {
                                $arr_images [] = array(
                                    'name'  => check_file_exist($event_images['name'], 'event_gallery'),
                                    'title' => $event_images['title'],
                                );
                            }
                            $event['event_images'] = $arr_images;
                        }
                        $event_return[] = $event;
                    }
                }
            }
            return $event_return;
        }

    }
    