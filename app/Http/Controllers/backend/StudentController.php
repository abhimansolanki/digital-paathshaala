<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\Student;
use App\Model\backend\StudentPromote;
use App\Model\backend\UserType;
use App\Model\backend\AdminUser;
use App\Model\backend\StudentEnquire;
use App\Model\backend\StudentParent;
use Validator;
use Illuminate\Support\Facades\Input;
use Datatables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PDF;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use DateTime;
use DateInterval;
use DatePeriod;
use Illuminate\Support\Facades\View as View;
use Illuminate\Support\Facades\Route;
use App\Model\backend\Session;
use App\Model\backend\StudentHistory;

//    use MaddHatter\LaravelFullcalendar\Facades\Calendar;
//    use Illuminate\Support\Facades\View as View;

class StudentController extends Controller
{

    public function __construct()
    {
        //            ->setTotalRecords(100)

        /*
         * get current url or route name
         */
        //        $currentPath= Route::getFacadeRoot()->current()->uri();
        //        p($currentPath);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = array(
            'redirect_url' => url('admin/student/add'),
            'page_title' => trans('language.list_school'),
            'import_url' => url('admin/import-student'),
        );
        return view('backend.student.index')->with($data);
    }

    public function add(Request $request, $id = NULL, $type = '')
    {
        $student = [];
        $data = [];
        $class_id = null;
        $session_id = null;
        if (!empty($id) && $type != 'enquiry') {
            $decrypted_student_id = get_decrypted_value($id, true);
            $student = get_student_data($decrypted_student_id);
            $student = isset($student[0]) ? $student[0] : [];
            $user_detail = AdminUser:: select('user_name')->where('admin_user_id', $student['admin_user_id'])->first();
            $student['user_name'] = $user_detail->user_name;
            $class_id = $student['current_class_id'];
            $session_id = $student['current_session_id'];
            if (!$student) {
                return redirect('admin/student')->withError('Student not found!');
            }
            $encrypted_student_id = get_encrypted_value($student['student_id'], true);
            $page_title = 'Edit Student';
            $save_url = url('admin/student/save/' . $encrypted_student_id);
            $submit_button = 'Update';
        } elseif (!empty($id) && $type == 'enquiry') {
            // convert enquiry into admission
            $decrypted_student_enquire_id = get_decrypted_value($id, true);
            $student_enquiry = new StudentEnquireController;
            $student = $student_enquiry->getEnquireData($decrypted_student_enquire_id);
            $student = isset($student[0]) ? $student[0] : array();
            $student['current_class_id'] = $class_id = $student['class_id'];
            $student['current_session_id'] = $session_id = $student['session_id'];
            if (!$student) {
                return redirect('admin/student')->withError('enquiry not found!');
            }
            $page_title = 'Create Student';
            $save_url = url('admin/student/save');
            $submit_button = 'Save';
            $student['enrollment_number'] = create_scholar_number($class_id);
        } else {
            $page_title = 'Create Student';
            $save_url = url('admin/student/save');
            $submit_button = 'Save';
            $student['enrollment_number'] = '';
        }
        $arr_gender = \Config::get('custom.student_gender');
        $arr_new_student = \Config::get('custom.new_student');
        $arr_student_left = \Config::get('custom.student_left');
        $arr_month = get_current_session_months(); //
        $student['arr_care_of'] = \Config::get('custom.care_of');

        $student['arr_new_student'] = $arr_new_student;
        $student['arr_student_left'] = $arr_student_left;
        $student['arr_gender'] = $arr_gender;

        $arr_class = get_all_classes();
        $arr_caste_category = get_caste_category();
        $arr_student_type = get_student_type();
        $arr_section = get_class_section($class_id, $session_id);
        $student['arr_section'] = add_blank_option($arr_section, '--Select section --');
        $arr_session = get_session();
        $student['arr_session'] = add_blank_option($arr_session, '-- Academic year --');
        $student['arr_class'] = add_blank_option($arr_class, '--Select class --');
        $student['arr_student_type'] = add_blank_option($arr_student_type, '--Select student type --');
        $student['arr_caste_category'] = add_blank_option($arr_caste_category, '--Select category --');
        $student['arr_month'] = add_blank_option($arr_month, '--Select month --');
        $student['student_document'] = \Config::get('custom.student_document');
        $data = array(
            'page_title' => $page_title,
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'student' => $student,
            'redirect_url' => url('admin/student'),
        );
        //p($data['student_document']);
        return view('backend.student.add')->with($data);
    }

    function createScholarNo()
    {
        $class_id = Input::get('class_id');
        $enrollment_no = create_scholar_number($class_id);
        return response()->json(array('status' => 'success', 'scholar_no' => $enrollment_no));
    }

    function getAge()
    {
        $dob = Input::get('dob');
        $status = 'failed';
        $age = '';
        if (!empty($dob)) {
            $status = 'success';
            $age = age_calculator($dob);
        }
        return response()->json(array('status' => $status, 'age' => $age));
    }

    public function save(Request $request, $id = NULL)
    {

        $user_type_id = null;
        $user_role_id = null;
        $student_id = null;
        $decrypted_student_id = null;
        $admin_user_id = null;

        //**in case of sibling, find $student_parent_id*/

        $student_parent_id = null;
        if ($request->has('student_parent_id') && !empty(Input::get('student_parent_id'))) {
            $student_parent_id = Input::get('student_parent_id');
        }

        if (!empty($id)) {
            $decrypted_student_id = get_decrypted_value($id, true);
            $student = Student::find($decrypted_student_id);
            $student_parent = StudentParent::Find($student->student_parent_id);
            $user = AdminUser::Find($student_parent->admin_user_id);
            $admin_user_id = $student_parent->admin_user_id;
            if (!$student) {
                return redirect('/admin/student/')->withError('Student not found!');
            }
            $success_msg = 'Student updated successfully!';
        } else {
            $student = new Student;
            $user_type = UserType::with('userRole')
                ->where('user_type', 'Parent')->select('user_type_id')->first();
            $user_type_id = $user_type['user_type_id'];
            $user_role_id = $user_type['userRole']['user_role_id'];

            if (!empty($student_parent_id)) {
                $student_parent = StudentParent::Find($student_parent_id);
                $user = AdminUser::Find($student_parent->admin_user_id);
            } else {
                $student_parent = new StudentParent();
                $user = new AdminUser();
            }

            $success_msg = 'Student saved successfully!';
        }

        $care_of = null;
        $mobile_number = '';
        if ($request->has('care_of') && !empty(Input::get('care_of'))) {
            $care_of = Input::get('care_of');
        }


        $arr_input_fields = [
            'enrollment_number' => 'required|unique:students,enrollment_number,' . $decrypted_student_id . ',student_id',
            'first_name' => 'required',
            'gender' => 'required',
            'student_type_id' => 'required',
            'caste_category_id' => 'required',
            'admission_date' => 'required',
            'join_date' => 'required',
            'fee_calculate_month' => 'required',
            'address_line1' => 'required',
            'student_left' => 'required',
            'new_student' => 'required',
            'care_of' => 'required',
            'current_section_id' => 'required',
            'session_id' => 'required',
            'admission_class_name' => 'required',
        ];
        if ($care_of == 1) {
            /* set user mobile no and added validation of father * */
            $mobile_number = Input::get('father_contact_number');
            $arr_input_fields['father_contact_number'] = 'required|unique:student_parents,father_contact_number,' . $student_parent_id . ',student_parent_id';
            $arr_input_fields['father_first_name'] = 'required';
            //   $arr_input_fields['father_last_name']      = 'required';
        } elseif ($care_of == 2) {
            /* set user mobile no and added validation of mpother* */
            $mobile_number = Input::get('mother_contact_number');
            $arr_input_fields['mother_first_name'] = 'required';
            //                $arr_input_fields['mother_last_name']      = 'required';
            $arr_input_fields['mother_contact_number'] = 'required|unique:student_parents,mother_contact_number,' . $student_parent_id . ',student_parent_id';
        } elseif ($care_of == 3) {
            /* set user mobile no and added validation of guardian* */
            $mobile_number = Input::get('guardian_contact_number');
            $arr_input_fields['guardian_first_name'] = 'required';
            //                $arr_input_fields['guardian_last_name']      = 'required';
            $arr_input_fields['guardian_contact_number'] = 'required|unique:student_parents,guardian_contact_number,' . $student_parent_id . ',student_parent_id';
        }

        $validatior = Validator::make($request->all(), $arr_input_fields);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction(); //Start transaction!
            try {
                //inserting data in multiples tables
                $arr_mobile_num = [];
                $father_no = Input::get('father_contact_number');
                $mother_no = Input::get('mother_contact_number');
                $guardian_no = Input::get('guardian_contact_number');
                if (!empty($father_no)) {
                    array_push($arr_mobile_num, $father_no);
                }
                if (!empty($mother_no)) {
                    array_push($arr_mobile_num, $mother_no);
                }
                if (!empty($guardian_no)) {
                    array_push($arr_mobile_num, $guardian_no);
                }

                $parent_password = \Config::get('custom.parent_password');
                $user->mobile_number = $mobile_number;
                if (empty($id)) {
                    $user->password = Hash::make($parent_password);
                }
                if (!empty($user_type_id)) {
                    $user->user_type_id = $user_type_id;
                }
                if (!empty($user_role_id)) {
                    $user->user_role_id = $user_role_id;
                }

                if ($user->save()) {
                    // student parent table data
                    $student_parent->father_email = Input::get('father_email');
                    $student_parent->father_aadhaar_number = Input::get('father_aadhaar_number');
                    $student_parent->father_first_name = Input::get('father_first_name');
                    $student_parent->father_middle_name = Input::get('father_middle_name');
                    $student_parent->father_last_name = Input::get('father_last_name');
                    $student_parent->father_occupation = Input::get('father_occupation');
                    $student_parent->father_contact_number = $father_no;
                    $student_parent->father_income = Input::get('father_income');

                    $student_parent->mother_email = Input::get('mother_email');
                    $student_parent->mother_aadhaar_number = Input::get('mother_aadhaar_number');
                    $student_parent->mother_first_name = Input::get('mother_first_name');
                    $student_parent->mother_middle_name = Input::get('mother_middle_name');
                    $student_parent->mother_last_name = Input::get('mother_last_name');
                    $student_parent->mother_occupation = Input::get('mother_occupation');
                    $student_parent->mother_contact_number = $mother_no;
                    $student_parent->mother_income = Input::get('mother_income');

                    $student_parent->guardian_email = Input::get('guardian_email');
                    $student_parent->guardian_aadhaar_number = Input::get('guardian_aadhaar_number');
                    $student_parent->guardian_first_name = Input::get('guardian_first_name');
                    $student_parent->guardian_middle_name = Input::get('guardian_middle_name');
                    $student_parent->guardian_last_name = Input::get('guardian_last_name');
                    $student_parent->guardian_occupation = Input::get('guardian_occupation');
                    $student_parent->guardian_relation = Input::get('guardian_relation');
                    $student_parent->guardian_contact_number = $guardian_no;
                    $student_parent->care_of = Input::get('care_of');
                    if (empty($id)) {
                        $student_parent->sibling_exist = $request->has('sibling_exist') ? 1 : 0;

                        $student->app_access_fee_status = 2;
                    }

                    // student table data
                    $student->form_number = Input::get('form_number');
                    $student->student_parent_id = $student_parent['student_parent_id'];
                    $student->enrollment_number = Input::get('enrollment_number');
                    $student->current_session_id = Input::get('session_id');
                    $student->current_class_id = Input::get('current_class_id');
                    $student->current_section_id = Input::get('current_section_id');
                    $student->student_type_id = Input::get('student_type_id');
                    $student->caste_category_id = Input::get('caste_category_id');
                    $student->admission_date = get_formatted_date(Input::get('admission_date'), 'database');
                    $student->join_date = get_formatted_date(Input::get('join_date'), 'database');
                    //                        $student->fee_discount                 = Input::get('fee_discount');
                    $student->previous_due_fee = Input::get('previous_due_fee');
                    if (empty($id)) {
                        $student->previous_due_fee_original = Input::get('previous_due_fee');
                    }
                    $student->remark = Input::get('remark');
                    $student->fee_calculate_month = Input::get('fee_calculate_month');
                    $student->student_left = Input::get('student_left');
                    $student->reason = Input::get('reason');
                    $student->new_student = Input::get('new_student');
                    //                        if (!empty(Input::get('discount_date')))
                    //                        {
                    //                            $student->discount_date = get_formatted_date(Input::get('discount_date'), 'database');
                    //                        }
                    // student details table data
                    $student->first_name = Input::get('first_name');
                    $student->middle_name = Input::get('middle_name');
                    $student->last_name = Input::get('last_name');

                    if (!empty(Input::get('dob'))) {
                        $student->dob = get_formatted_date(Input::get('dob'), 'database');
                    }
                    $student->gender = Input::get('gender');
                    $student->aadhaar_number = Input::get('aadhaar_number');
                    $student->previous_school_name = Input::get('previous_school_name');
                    $student->previous_class_name = Input::get('previous_class_name');
                    $student->previous_result = Input::get('previous_result');
                    $student->tc_number = Input::get('tc_number');
                    $student->address_line1 = Input::get('address_line1');
                    $student->address_line2 = Input::get('address_line2');
                    $student->birth_place = Input::get('birth_place');
                    $student->religion = Input::get('religion');
                    $student->admission_class_name = Input::get('admission_class_name');
                    $student->student_doc = !empty(Input::get('student_doc')) ? implode(',', Input::get('student_doc')) : '';
                    if (!empty(Input::get('tc_date'))) {
                        $student->tc_date = get_formatted_date(Input::get('tc_date'), 'database');
                    }
                    if ($request->hasFile('profile_photo')) {
                        $file = $request->file('profile_photo');
                        $config_upload_path = \Config::get('custom.student_profile');
                        $destinationPath = public_path() . $config_upload_path['upload_path'];
                        $filename = $file->getClientOriginalName();
                        $rand_string = quick_random();
                        $filename = $rand_string . '-' . $filename;
                        $file->move($destinationPath, $filename);
                        $student->profile_photo = $filename;
                    }
                    $inserted_data = $user->adminStudentParent()->save($student_parent)->getParentStudent()->save($student);
                    $student_id = $inserted_data->student_id;

                    // updated enquire table
                    if (!empty($student_id) && empty($id) && !empty($student->form_number)) {
                        $enquire_data = StudentEnquire::where('form_number', $student->form_number)->where('status', 0)->first();
                        if (!empty($enquire_data)) {
                            $enquire_data->status = 1;
                            $enquire_data->save();
                        }
                    }
                    if (!empty($student_id) && empty($id)) {
                        // send mail
                        $email = '';
                        if (!empty(Input::get('father_email'))) {
                            $email = Input::get('father_email');
                            $data['cc_address'] = Input::get('mother_email');
                        } else {
                            $email = Input::get('mother_email');
                            $data['cc_address'] = Input::get('father_email');
                        }
                        if (!empty($email)) {
                            $data['view_blade'] = 'admission-mail';
                            $data['cc_name'] = $student_parent->mother_first_name;
                            $data['subject'] = trans('language.admission_subject');
                            $message_text_mail = trans('language.admission_mail_message');
                            $message_text_mail .= "user name =" . $mobile_number;
                            $message_text_mail .= "password =" . $parent_password;
                            $data['body'] = array(
                                'message' => $message_text_mail,
                                'enrollment_number' => $student->enrollment_number,
                                'admission_date' => $student->admission_date,
                            );
//                            $mail_response = Mail::to($email)->send(new SendMailable($data));
                        }

                        // send sms  to parents
                        if (!empty($mobile_number)) {
                            $sms_mobile_number = implode(',', $arr_mobile_num);
                            $message_text = "Dear Parent, registration has been created successfully. ";
                            $message_text .= "Your app login credentials are : ";
                            $message_text .= "user name = " . $mobile_number;
                            $message_text .= " and password = " . $parent_password;
                            $message_text .= " Thank you. ";
                            $response = send_sms($sms_mobile_number, $message_text);
                        }
                    }
                }
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withInput()->withErrors($error_message);
            }

            DB::commit();
        }
        if ($request->has('savePrint') && (Input::get('savePrint') == 'Save&Print')) {
            //                 $url = 'admin/admission-pdf/' . $student_id;
            //                return  "<script>window.open('".$url."', '_blank')</script>";
            return redirect('admin/admission-pdf/' . $student_id);
        } else {
            return redirect('admin/student/add')->withSuccess($success_msg);
        }
    }

    function getParent()
    {
        $care_of_contact = Input::get('care_of_contact');
        $care_of = Input::get('care_of');
        $session = get_current_session();
        $parent_data = [];
        $status = 'failed';
        if (!empty($care_of) && !empty($session)) {
            $parent_data = [];
            $student_parent = StudentParent::addSelect([
                'student_parent_id', 'father_email', 'father_first_name', 'father_middle_name', 'father_last_name', 'father_contact_number', 'father_aadhaar_number',
                'mother_occupation', 'mother_contact_number', 'mother_email', 'mother_aadhaar_number', 'mother_first_name', 'mother_middle_name', 'mother_last_name',
                'guardian_occupation', 'guardian_contact_number', 'guardian_email', 'guardian_aadhaar_number', 'guardian_first_name', 'guardian_middle_name', 'guardian_last_name'
            ])
                ->where('student_parent_status', 1)
                ->where(function ($query) use ($care_of, $care_of_contact) {
                    if ($care_of == 1) {
                        $query->where('father_contact_number', $care_of_contact)
                            ->orWhere('father_aadhaar_number', $care_of_contact);
                    } elseif ($care_of == 2) {
                        $query->where('mother_contact_number', $care_of_contact)
                            ->orWhere('mother_aadhaar_number', $care_of_contact);
                    } elseif ($care_of == 3) {
                        $query->where('guardian_contact_number', $care_of_contact)
                            ->orWhere('guardian_aadhaar_number', $care_of_contact);
                    }
                })->first();

            if (!empty($student_parent)) {
                $parent_data = array(
                    'student_parent_id' => $student_parent['student_parent_id'],
                    'father_first_name' => $student_parent['father_first_name'],
                    'father_middel_name' => $student_parent['father_middel_name'],
                    'father_last_name' => $student_parent['father_last_name'],
                    'father_contact_number' => $student_parent['father_contact_number'],
                    'father_email' => $student_parent['father_email'],
                    'father_occupation' => $student_parent['father_occupation'],
                    'mother_first_name' => $student_parent['mother_first_name'],
                    'mother_middle_name' => $student_parent['mother_middle_name'],
                    'mother_last_name' => $student_parent['mother_last_name'],
                    'mother_email' => $student_parent['mother_email'],
                    'mother_occupation' => $student_parent['mother_occupation'],
                    'mother_contact_number' => $student_parent['mother_contact_number'],
                    'guardian_first_name' => $student_parent['guardian_first_name'],
                    'guardian_middle_name' => $student_parent['guardian_middle_name'],
                    'guardian_last_name' => $student_parent['guardian_last_name'],
                    'guardian_email' => $student_parent['guardian_email'],
                    'guardian_occupation' => $student_parent['guardian_occupation'],
                    'guardian_contact_number' => $student_parent['guardian_contact_number'],
                );
                $status = 'success';
            }
        }
        return response()->json($result = array('status' => $status, 'data' => $parent_data));
    }

    public function anyData(Request $request)
    {
        $student = [];
        $arr_student_id = [];
        $class_id = null;
        $offset = Input::get('start');
        $limit = Input::get('length');
        //            $session_id     = Input::get('session_id');
        $arr_students = get_student_data($arr_student_id, $class_id, null, $offset, $limit);
        foreach ($arr_students as $key => $arr_student) {
            $student[$key] = (object)$arr_student;
        }
        return Datatables::of($student)
            ->addColumn('action', function ($student) {
                $encrypted_student_id = get_encrypted_value($student->student_id, true);
                return '<a title="Edit" id="deletebtn1" href="' . url('admin/student/add/' . $encrypted_student_id) . '" class="btn btn-success centerAlign"><i class="fa fa-edit" ></i></a>';
            })
            ->addColumn('view_print', function ($student) {
                $disabled = '';
                $encrypted_student_id = get_encrypted_value($student->student_id, true);
                return '<a title="View & Print" href="admission-pdf/' . $student->student_id . '" class="btn btn-info prinr_r centerAlign"  target="_blank" style="text-align:center;" ' . $disabled . '><i class="fa fa-print" ></i></a>';
            })
            ->rawColumns(['checkbox', 'action', 'view_print'])->make(true);
    }

    public function checkParentContact(Request $request)
    {
        $student_parent_id = Input::get('student_parent_id');
        if ($request->has('father_contact_number')) {
            $father_contact_number = Input::get('father_contact_number');
            $father = StudentParent::where('father_contact_number', $father_contact_number)
                ->where(function ($query) use ($student_parent_id) {
                    if (!empty($student_parent_id)) {
                        $query->where('student_parent_id', '!=', $student_parent_id);
                    }
                })
                ->first();
            if (!empty($father) && !empty($father->father_contact_number)) {
                return response()->json(false);
            }
        } elseif ($request->has('mother_contact_number')) {
            $mother_contact_number = Input::get('mother_contact_number');
            $mother = StudentParent::where('mother_contact_number', $mother_contact_number)
                ->where(function ($query) use ($student_parent_id) {
                    if (!empty($student_parent_id)) {
                        $query->where('student_parent_id', '!=', $student_parent_id);
                    }
                })
                ->first();
            if (!empty($mother) && !empty($mother->mother_contact_number)) {
                return response()->json(false);
            }
        } elseif ($request->has('guardian_contact_number')) {
            $guardian_contact_number = Input::get('guardian_contact_number');
            $guardian = StudentParent::where('guardian_contact_number', $guardian_contact_number)
                ->where(function ($query) use ($student_parent_id) {
                    if (!empty($student_parent_id)) {
                        $query->where('student_parent_id', '!=', $student_parent_id);
                    }
                })
                ->first();
            if (!empty($guardian) && !empty($guardian->guardian_contact_number)) {
                return response()->json(false);
            }
        }
        return response()->json(true);
    }

    public function admissionPdfPrint($student_id)
    {
        $arr_student_id = is_array($student_id) ? $student_id : array($student_id);

        if (!empty($arr_student_id)) {
            $arr_student_data['arr_student_data'] = get_student_data($arr_student_id);
            $pdf = PDF::loadView('backend.student.admission', $arr_student_data);
            return $pdf->stream('admission-form.pdf');
        }
    }

    function studentList()
    {
        $class_id = Input::get('class_id');
        $section_id = Input::get('section_id');
        $session_id = Input::get('session_id');
        if (empty($session_id)) {
            $session = db_current_session();
            $session_id = $session['session_id'];
        }
        $arr_student_list = [];
        $message = 'Students not found!';
        $status = 'failed';
        if (!empty($class_id)) {
            $arr_student_list = get_student_list_by_class_section($session_id, $class_id, $section_id = null);

            $message = 'Got result successfully';
            $status = 'success';
        }
        return response()->json($result = array('status' => $status, 'message' => $message, 'data' => $arr_student_list));
    }

    // dummy datatable
    public function test()
    {
        $data = array(
            'redirect_url' => url('admin/student/add'),
            'page_title' => trans('language.list_school'),
        );
        return view('backend.test')->with($data);
    }

    public function testData()
    {
        $student = [];
        $arr_students = get_student_data();
        foreach ($arr_students as $key => $arr_student) {
            $student[$key] = (object)$arr_student;
        }
        return Datatables::of($student)
            ->rawColumns(['checkbox'])->make(true);
    }

    public function studentSibling($sibling_student_id = null)
    {
        $class_id = null;
        $student_id = null;
        $arr_student = [];
        if (!empty($sibling_student_id)) {
            $student = Student::select('student_parent_id', 'current_class_id')->where('student_id', $sibling_student_id)->first();
            $class_id = $student['current_class_id'];
            $student_id = $student['student_parent_id'] . '_' . $sibling_student_id;
        }
        $data = array(
            'class_id' => $class_id,
            'student_id' => $student_id,
            'arr_class' => add_blank_option(get_all_classes(), '--select class--'),
            'arr_student' => add_blank_option($arr_student, '--select student--'),
        );
        return view('backend.report.student-sibling')->with($data);
    }

    public function studentSiblingReport(Request $request)
    {
//        p($request->all());
        $student_sibling = [];
        $parent_id = Input::get('parent_id');
        $class_id = Input::get('class_id');
        $sibling_student_id = Input::get('sibling_student_id');
        //	if (!empty($parent_id) && !empty($sibling_student_id))
        //	{
        $arr_students = DB::table('students as s')
            ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
            ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
            ->join('sections as sec', 'sec.section_id', '=', 's.current_section_id')
            ->select('s.enrollment_number', 's.dob', 's.gender', 's.address_line1', 's.admission_date', 'sp.sibling_exist', 'sp.care_of', 's.student_id', 'sp.father_first_name', 'sp.father_last_name', 'sp.father_middle_name', 'sp.father_contact_number', 'sp.mother_contact_number', 's.first_name', 'sp.mother_first_name', 'sp.mother_middle_name', 'sp.mother_last_name', 'sp.guardian_contact_number', 'sp.guardian_first_name', 'sp.guardian_middle_name', 'sp.guardian_last_name', 's.last_name', 's.middle_name', 'c.class_name', 'sec.section_name')
            ->where('student_status', 1)
            ->where('sp.sibling_exist', 1)
            ->where(function ($q) use ($parent_id, $class_id, $sibling_student_id) {
                if (!empty($parent_id)) {
                    $q->where('sp.student_parent_id', $parent_id);
                }
//                if (!empty($class_id)) {
//                    $q->where('s.current_class_id', $class_id);
//                }
                if (!empty($sibling_student_id)) {
                    $q->where('s.student_id', '!=', $sibling_student_id);
                }
            })
            ->orderBy('s.enrollment_number', 'ASC')
            ->get();
        $arr_care_of = \Config::get('custom.care_of');
        foreach ($arr_students as $key => $student) {
            $student = (array)$student;
            $student_data = array(
                'student_id' => $student['student_id'],
                'enrollment_number' => $student['enrollment_number'],
                'admission_date' => get_formatted_date($student['admission_date'], 'display'),
                'sibling_exist' => $student['sibling_exist'],
                'care_of' => $student['care_of'],
                'care_of_rel' => $arr_care_of[$student['care_of']],
                'dob' => get_formatted_date($student['dob'], 'display'),
                'gender_name' => get_gender($student['gender'], 'student_gender'),
                'address_line1' => $student['address_line1'],
                'student_name' => $student['first_name'] . ' ' . $student['middle_name'] . ' ' . $student['last_name'],
                'class_name' => $student['class_name'] . '(' . $student['section_name'] . ')',
                'father_name' => $student['father_first_name'] . ' ' . $student['father_middle_name'] . ' ' . $student['father_last_name'],
                'father_contact_number' => $student['father_contact_number'],
                'mother_name' => $student['mother_first_name'] . ' ' . $student['mother_middle_name'] . ' ' . $student['mother_last_name'],
                'mother_contact_number' => $student['mother_contact_number']
            );

            $student_sibling[$key] = (object)$student_data;
        }
        //	}
        return Datatables::of($student_sibling)->make(true);
    }

    /*
     * Student Detail Report
     */

    public function studentDetail()
    {
        $data = array(
            'arr_class' => add_blank_option(get_all_classes(), '--Select class--'),
            'arr_student_presence' => add_blank_option(\Config::get('custom.arr_student_presence'), '--Select Student type--'),
        );
        return view('backend.report.student-details')->with($data);
    }

    /*
     * Student Detail Report Data
     */

    public function studentDetailReport(Request $request)
    {
        $arr_student_data = [];
        $arr_student_detail = [];
        $class_id = Input::get('class_id');
        $session_id = Input::get('sessionid');
        $student_type_id = Input::get('student_type_id');
        if (empty($session_id)) {
            $session = get_current_session();
            $session_id = $session['session_id'];
        }
        $limit = Input::get('length');
        $offset = Input::get('start');
        if (!empty($session_id)) {
            $type = 'table';
            $table = get_student_table($session_id, $type);
            $arr_care_of = \Config::get('custom.care_of');
// p($session_id);
            $query = DB::table($table)
                ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
                // ->leftJoin('exam_roll_numbers as ern', 's.student_id', '=', 'ern.student_id')
                ->join('caste_categories as cc', 'cc.caste_category_id', '=', 's.caste_category_id')
                ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
                ->join('sections as sec', 'sec.section_id', '=', 's.current_section_id')
                 //'ern.prefix', 'ern.roll_number',
                ->select('s.address_line2', 'sp.father_income', 's.address_line2', 's.profile_photo', 'cc.caste_name', 's.enrollment_number', 's.dob', 's.gender', 's.address_line1', 's.admission_date', 'sp.sibling_exist', 'sp.care_of', 's.student_id', 'sp.father_first_name', 'sp.father_last_name', 'sp.father_middle_name', 'sp.father_contact_number', 'sp.mother_contact_number', 's.first_name', 'sp.mother_first_name', 'sp.mother_middle_name', 'sp.mother_last_name', 'sp.guardian_contact_number', 'sp.guardian_first_name', 'sp.guardian_middle_name', 'sp.guardian_last_name', 's.last_name', 's.middle_name', 'c.class_name', 'sec.section_name')
                ->where('student_status', 1)
                ->where('s.current_session_id', $session_id);
                // ->where('ern.session_id', $session_id);
            if ($student_type_id == 2 || $student_type_id == 3) {
                $query->where('s.student_left', 'Yes');
                if ($student_type_id == 2) {
                    $query->whereNotIn('s.student_id', function ($query) use ($session_id, $class_id) {
                        $query->select('stc.student_id')->from('student_tcs as stc')
                            ->where(function ($q) use ($session_id) {
                                if (!empty($session_id)) {
                                    $q->where('stc.session_id', $session_id);
                                }
                            })
                            ->where(function ($q) use ($class_id) {
                                if (!empty($class_id)) {
                                    $q->where('stc.class_id', $class_id);
                                }
                            });
                    });
                }
            } else {
                $query->where('s.student_left', 'No');
            }
            if (!empty($class_id)) {
                $query->where('c.class_id', $class_id);
            }
            $query->orderBy('enrollment_number');
            $arr_student_detail = $query->get();
            $sr_no = 1;
            foreach ($arr_student_detail as $key => $student) {
                $student = (array)$student;
                $profile = '';
                $img = check_file_exist($student['profile_photo'], 'student_profile');
                if ($img) {
                    $profile = '<img src="' . url($img) . '" height="50">';
                }
                $exam_roll_number = DB::table('exam_roll_numbers')->select("prefix", "roll_number")->where(["student_id" => $student['student_id'], 'session_id' => $session_id])->first();
                $student_data = array(
                    'student_id' => $student['student_id'],
                    'enrollment_number' => $student['enrollment_number'],
                    'profile' => $profile,
                    'admission_date' => get_formatted_date($student['admission_date'], 'display'),
                    'sibling_exist' => $student['sibling_exist'],
                    'care_of' => $student['care_of'],
                    'care_of_rel' => $arr_care_of[$student['care_of']],
                    'dob' => get_formatted_date($student['dob'], 'display'),
                    'dob_word' => covert_dob_words(get_formatted_date($student['dob'], 'display')),
                    'gender_name' => get_gender($student['gender'], 'student_gender'),
                    'address_line1' => $student['address_line1'],
                    'address_line2' => $student['address_line2'],
                    'student_name' => $student['first_name'] . ' ' . $student['middle_name'] . ' ' . $student['last_name'],
                    'caste_name' => $student['caste_name'],
                    'father_income' => $student['father_income'],
                    'class_name' => $student['class_name'] . '(' . $student['section_name'] . ')',
                    'father_name' => $student['father_first_name'] . ' ' . $student['father_middle_name'] . ' ' . $student['father_last_name'],
                    'father_contact_number' => $student['father_contact_number'],
                    'mother_contact_number' => $student['mother_contact_number'],
                    'mother_name' => $student['mother_first_name'] . ' ' . $student['mother_middle_name'] . ' ' . $student['mother_last_name'],
                    'sr_no' => $sr_no,
                    'roll_number' => !empty($exam_roll_number) ? $exam_roll_number->prefix . $exam_roll_number->roll_number : "--",
                );
                $sr_no++;
                $arr_student_data[$key] = (object)$student_data;
            }
        }
        return Datatables::of($arr_student_data)->rawColumns(['checkbox', 'profile'])->make(true);
    }

    public function promoteStudent()
    {
        $data = array(
            'arr_class' => get_all_classes(),
            'arr_session' => get_session('no'),
        );
        $session = Session::where('session_status', 1)->first();
        $data['promote_sessions'] = Session::Where('start_date', '>', $session->end_date)->get()->pluck("session_year", "session_id")->toArray();
        return view('backend.report.promote-student')->with($data);
    }

    public function promoteStudentData()
    {
        $arr_student_data = [];
        $arr_student_detail = [];
        $class_id = Input::get('class_id');
        $session_id = Input::get('session_id');
        $section_id = Input::get('section_id');
        $limit = Input::get('length');
        $offset = Input::get('start');
        if (!empty($class_id) && !empty($session_id)) {
            $arr_care_of = \Config::get('custom.care_of');
            $arr_student_detail = DB::table('students as s')
                ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
                ->join('caste_categories as cc', 'cc.caste_category_id', '=', 's.caste_category_id')
                ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
                ->leftJoin('sections as sec', 'sec.section_id', '=', 's.current_section_id')
                ->select('cc.caste_name', 's.enrollment_number', 's.dob', 's.gender', 's.address_line1', 's.admission_date', 'sp.sibling_exist', 'sp.care_of', 's.student_id', 'sp.father_first_name', 'sp.father_last_name', 'sp.father_middle_name', 'sp.father_contact_number', 'sp.mother_contact_number', 's.first_name', 'sp.mother_first_name', 'sp.mother_middle_name', 'sp.mother_last_name', 'sp.guardian_contact_number', 'sp.guardian_first_name', 'sp.guardian_middle_name', 'sp.guardian_last_name', 's.last_name', 's.middle_name', 'c.class_name', 'sec.section_name'
                )
                ->where('student_status', 1)
                ->where('s.current_session_id', $session_id)
                ->where('s.current_class_id', $class_id)
                ->where('s.current_section_id', $section_id)
                ->where('s.student_left', "No")
                ->orderBy('s.enrollment_number', 'ASC')
                ->get();

            $arr_student_history_detail = DB::table('student_histories as sh')
                ->join('student_parents as sp', 'sp.student_parent_id', '=', 'sh.student_parent_id')
                ->join('caste_categories as cc', 'cc.caste_category_id', '=', 'sh.caste_category_id')
                ->join('classes as c', 'c.class_id', '=', 'sh.current_class_id')
                ->leftJoin('sections as sec', 'sec.section_id', '=', 'sh.current_section_id')
                ->select('cc.caste_name', 'sh.enrollment_number', 'sh.dob', 'sh.gender', 'sh.address_line1', 'sh.admission_date', 'sp.sibling_exist', 'sp.care_of', 'sh.student_id', 'sp.father_first_name', 'sp.father_last_name', 'sp.father_middle_name', 'sp.father_contact_number', 'sp.mother_contact_number', 'sh.first_name', 'sp.mother_first_name', 'sp.mother_middle_name', 'sp.mother_last_name', 'sp.guardian_contact_number', 'sp.guardian_first_name', 'sp.guardian_middle_name', 'sp.guardian_last_name', 'sh.last_name', 'sh.middle_name', 'c.class_name', 'sec.section_name'
                )
                ->where('student_status', 1)
                ->where('sh.current_session_id', $session_id)
                ->where('sh.current_class_id', $class_id)
                ->where('sh.current_section_id', $section_id)
                ->orderBy('sh.enrollment_number', 'ASC')
                ->get();

            $arr_student_detail = $arr_student_detail->merge($arr_student_history_detail);

            foreach ($arr_student_detail as $key => $student) {
                $student = (array)$student;
                $student_data = array(
                    'student_id' => $student['student_id'],
                    'enrollment_number' => $student['enrollment_number'],
                    'student_id' => $student['student_id'],
                    'admission_date' => get_formatted_date($student['admission_date'], 'display'),
                    'sibling_exist' => $student['sibling_exist'],
                    'care_of' => $student['care_of'],
                    'care_of_rel' => $arr_care_of[$student['care_of']],
                    'dob' => get_formatted_date($student['dob'], 'display'),
                    'gender_name' => get_gender($student['gender'], 'student_gender'),
                    'address_line1' => $student['address_line1'],
                    'student_name' => $student['first_name'] . ' ' . $student['middle_name'] . ' ' . $student['last_name'],
                    'caste_name' => $student['caste_name'],
                    'class_name' => $student['class_name'],
                );
                if ($student_data['care_of'] == 1) {
                    $student_data['care_of_name'] = $student['father_first_name'] . ' ' . $student['father_middle_name'] . ' ' . $student['father_last_name'];
                    $student_data['care_of_contact'] = $student['father_contact_number'];
                } elseif ($student_data['care_of'] == 2) {
                    $student_data['care_of_name'] = $student['mother_first_name'] . ' ' . $student['mother_middle_name'] . ' ' . $student['mother_last_name'];
                    $student_data['care_of_contact'] = $student['mother_contact_number'];
                } else {
                    $student_data['care_of_name'] = $student['guardian_first_name'] . ' ' . $student['guardian_middle_name'] . ' ' . $student['guardian_last_name'];
                    $student_data['care_of_contact'] = $student['guardian_contact_number'];
                }

                $arr_student_data[$key] = (object)$student_data;
            }
        }
        return Datatables::of($arr_student_data)->make(true);
    }

    public function promoteStudentSave(Request $request)
    {
        $arr_student_id = explode(',', Input::get('student_id'));
        $class_id = Input::get('class_id');
        $section_id = Input::get('section_id');
        $session_id = Input::get('session_id');
        $promote_class_id = Input::get('promote_class_id');
        $promote_section_id = Input::get('promote_section_id');
        $promote_session_id = Input::get('promote_session_id');

        DB::beginTransaction(); //Start transaction!
        try {
            // Move Student to student history
            if (isset($arr_student_id) && is_array($arr_student_id)) {
                foreach ($arr_student_id as $key => $student_id) {
                    $student = Student::find($student_id);
                    $is_student_exist_in_history =
                        StudentHistory::where('student_id', $student_id)
                            ->where('current_class_id', $class_id)
                            ->where('current_section_id', $section_id)
                            ->where('current_session_id', $session_id)->first();
                    if (empty($is_student_exist_in_history)) {
                        $student_history = $student->toArray();
                        unset($student_history['created_at']);
                        unset($student_history['updated_at']);
                        StudentHistory::insert($student_history);
                    }
                }
            }
//            if ($class_id == 1 && $promote_class_id != 1) {
                foreach ($arr_student_id as $key => $student_id) {
                    $obj = new StudentFeeReceiptNewController();
                    $request->request->add(['arr_student' => [$student_id], 'fee_receipt_date' => date('d/m/Y'), 'fee_type_id' => NULL, 'request_type' => 'API']);
                    $pendingFeeDetails = $obj->studentFeeDetail();
                    $pending_amount = 0;
                    if(!empty($pendingFeeDetails) && $pendingFeeDetails['fee_detail']['payable_fee'] > 0){
                        $pending_amount = $pendingFeeDetails['fee_detail']['payable_fee'];
                    }

                    $stu = Student::where('current_class_id', $class_id)->where('current_section_id', $section_id)->where('current_session_id', $session_id)->findOrFail($student_id);
                    $stu->current_class_id = $promote_class_id;
                    $stu->current_section_id = $promote_section_id;
                    $stu->current_session_id = $promote_session_id;
                    if ($class_id == 1 && $promote_class_id != 1) {
                        $stu->enrollment_number = create_scholar_number($promote_class_id);
                    }
                    $stu->new_student = 0;
                    $stu->previous_due_fee = $pending_amount;
                    $stu->previous_due_fee_original = $pending_amount;
                    $stu->save();
//                    DB::table('students as s')->where('student_id', $student_id)
//                        ->where('current_class_id', $class_id)
//                        ->where('current_section_id', $section_id)
//                        ->where('current_session_id', $session_id)
//                        ->update(['current_class_id' => $promote_class_id,
//                            'current_section_id' => $promote_section_id,
//                            'current_session_id' => $promote_session_id,
//                            'enrollment_number' => create_scholar_number($promote_class_id),
//                        ]);
                }
//            } else {
//                DB::table('students as s')->whereIn('student_id', $arr_student_id)
//                    ->where('current_class_id', $class_id)
//                    ->where('current_section_id', $section_id)
//                    ->where('current_session_id', $session_id)
//                    ->update(['current_class_id' => $promote_class_id,
//                            'current_section_id' => $promote_section_id,
//                            'current_session_id' => $promote_session_id]
//                    );
//            }


            $promote = StudentPromote::where('session_id', $promote_session_id)->where('class_id', $promote_class_id)->where('section_id', $promote_section_id)->first();
            if (empty($promote->student_promote_id)) {
                $promote = new StudentPromote;
            }
            $promote->session_id = $promote_session_id;
            $promote->class_id = $promote_class_id;
            $promote->section_id = $promote_section_id;
            $promote->promote_status = 1;
            $promote->save();
            $return = array('status' => 'success', 'message' => 'Student(s) Promoted successfully');
            DB::commit();
            return response()->json($return);
        } catch (\Exception $e) {
            //failed logic here
            DB::rollback();
            $error_message = $e->getMessage();
            $return = array('status' => 'failed', 'message' => $error_message);
            return response()->json($return);
        }
    }

    function studentClass()
    {
        $class_id = Input::get('class_id');
        $arr_student_list = [];
        $message = 'Students not found!';
        $status = 'failed';
        $session_id = null;
        $session = get_current_session();
        if (!empty($session) && !empty($class_id)) {
            $session_id = $session['session_id'];
            $arr_student = DB::table('students as s')
                ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
                ->select('current_class_id', 's.student_id', 's.student_parent_id', 'enrollment_number', 'first_name', 'last_name')
                ->where('s.current_class_id', $class_id)
                ->where('s.student_status', 1)
                ->where('s.current_session_id', $session_id)
//                ->where('sp.sibling_exist', 1)
                ->get();
            if (!empty($arr_student)) {
                foreach ($arr_student as $student) {
                    $student = (array)$student;

                    $arr_student_list[$student['student_parent_id'] . '_' . $student['student_id']] = $student['enrollment_number'] . ' : ' . $student['first_name'] . ' ' . $student['last_name'];
                }
                $message = 'Got result successfully';
                $status = 'success';
            }
        }
        return response()->json($result = array('status' => $status, 'message' => $message, 'data' => $arr_student_list));
    }

    function resetAppAccessStatus()
    {
        $status = 'failed';
        $message = '';
        $return = [];
        $current_session = db_current_session();
        $affected_rows = null;
        DB::beginTransaction(); //Start transaction!

        if ($current_session['end_date'] == date('Y-m-d')) {
            try {
                $affected_rows = DB::table('student_parents')->update(['app_access_fee_status' => 2]);
                if ($affected_rows > 0) {
                    $status = 'success';
                    $message = 'App access status reset successfully.';
                }
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $message = $e->getMessage();
                return redirect()->back()->withInput()->withErrors($error_message);
            }

            DB::commit();
            $return = array('status' => $status, 'message' => $message, 'affected_rows' => $affected_rows);
            Log::info($return);
        }
    }

    public function getProgressHeader($class_id, $section_id)
    {
        $session = get_current_session();
        $session_id = $session['session_id'];
        $arr_col1[0] = array('key' => 'student_id', 'val' => '');
        $arr_col1[1] = array('key' => 'serial_no', 'val' => 'S.No');
        $arr_col1[2] = array('key' => 'enrollment_number', 'val' => 'SR.No');
        $arr_col1[3] = array('key' => 'student_name', 'val' => ' Student Name ');
        $arr_col1[4] = array('key' => 'father_name', 'val' => ' Father Name ');
        $arr_col1[5] = array('key' => 'father_contact', 'val' => ' Father Conatct No');

        $arr_col2 = [];
        $arr_subject = get_class_section_subject($session_id, $class_id, $section_id);
        if (!empty($arr_subject)) {

            foreach ($arr_subject as $key => $subject) {
                $arr_col2[] = array(
                    'key' => 'subject_' . $key,
                    'val' => $subject,
                    'type' => 1,
                    'subject_id' => $key);
            }
        }
        $arr_col = array_merge($arr_col1, $arr_col2);
        return $arr_col;
    }

    public function studentProgress($class_id = null, $section_id = null)
    {
        if (empty($class_id)) {
            $class_id = 1;
            $section_id = 1;
        }
        $session = get_current_session();
        $session_id = $session['session_id'];
        $data = array(
            'arr_class' => get_all_classes(),
            'arr_section' => add_blank_option(get_class_section($class_id, $session_id, null), '--select section--'),
            'arr_month' => get_current_session_months(),
            'arr_th' => $this->getProgressHeader($class_id, $section_id),
            'arr_progress_setting' => \Config::get('custom.progress_message'),
        );
        return view('backend.report.student-progress')->with($data);
    }

    public function studentProgressReport(Request $request)
    {
        $arr_student_data = [];
        $arr_student_detail = [];
        $class_id = Input::get('class_id');
        $section_id = Input::get('section_id');
        $month = Input::get('month');
        $session = get_current_session();
        $session_id = $session['session_id'];
        $header = $this->getProgressHeader($class_id, $section_id);
        if (!empty($session_id)) {
            $type = 'model';
            $table = get_student_table($session_id, $type);
            $query = $table
                ->with(['getParent' => function ($q) {
                    $q->select('student_parent_id', 'father_first_name', 'father_last_name', 'father_middle_name', 'father_contact_number'
                    );
                }])
                ->with(['getProgress' => function ($q) use ($session_id, $class_id, $month, $section_id) {
                    $q->where('session_id', $session_id);
                    $q->where('class_id', $class_id);
                    $q->where('section_id', $section_id);
                    $q->where('month', $month);
                    $q->addSelect('student_id', 'subject_id', 'month', 'progress', 'type');
                }])
                ->addSelect('student_id', 'student_parent_id', 'first_name', 'middle_name', 'last_name', 'enrollment_number')
                ->where('current_session_id', $session_id)
                ->where('current_section_id', $section_id)
                ->where('current_class_id', $class_id);
            $arr_student_detail = $query->get();
            $sn = 1;
            foreach ($arr_student_detail as $key => $student) {
                $student_data = array(
                    'student_id' => $student['student_id'],
                    'enrollment_number' => $student['enrollment_number'],
                    'serial_no' => $sn,
                    'student_name' => $student['first_name'] . ' ' . $student['middle_name'] . ' ' . $student['last_name'],
                    'father_name' => $student['getParent']['father_first_name'] . ' ' . $student['getParent']['father_middle_name'] . ' ' . $student['getParent']['father_last_name'],
                    'father_contact' => $student['getParent']['father_contact_number'],
                );

                $arr_subject = [];
                foreach ($student['getProgress'] as $key => $progress) {
                    $arr_subject[$progress['subject_id']] = array(
                        'subject_id' => $progress['subject_id'],
                        'progress' => $progress['progress'],
                    );
                }
                $subject_header = array_slice($header, 6);
                foreach ($subject_header as $key => $subj_header) {
                    $student_data[$subj_header['key']] = '';
                    if (isset($arr_subject[$subj_header['subject_id']])) {
                        $student_data[$subj_header['key']] = $arr_subject[$subj_header['subject_id']]['progress'];
                    }
                }
                $arr_student_data[] = (object)$student_data;
                $sn++;
            }
        }
        return Datatables::of($arr_student_data)->make(true);
    }

    public function importStudent(Request $request)
    {
        $admin_password = Input::get('admin_password');
        $admin = get_loggedin_user_data();
        if (!Hash::check($admin_password, $admin['password'])) {
            return redirect()->back()->withErrors('Invalid Admin Password');
        }
        if ($request->hasFile('import_student')) {
            $arr_input_fields = [
                'import_student' => 'required',
                'admin_password' => 'required',
            ];

            $validatior = Validator::make($request->all(), $arr_input_fields);
            if ($validatior->fails()) {
                return redirect()->back()->withInput()->withErrors($validatior);
            } else {
                $destinationPath = public_path() . '/uploads/student-import/';
                $file = $request->File('import_student');
                $filename = $file->getClientOriginalName();
                $path = $destinationPath . $filename;
                $file->move($destinationPath, $filename);
                //file permission
                $status = 'failed';
                chmod($path, 0777);
                $mobile_number = '';
                $data = \Excel::load($path)->get();
                if ($data->count()) {
                    $session = db_current_session();
                    foreach ($data as $key => $value) {
                        //		    p($value);
                        if (!empty($value->current_session_id)) {
                            DB::beginTransaction(); //Start transaction!
                            try {

                                if ($value->care_of == 1) {
                                    $mobile_number = $value->father_contact_number;
                                } elseif ($value->care_of == 2) {
                                    $mobile_number = $value->mother_contact_number;
                                } elseif ($value->care_of == 3) {
                                    $mobile_number = $value->guardian_contact_number;
                                }
                                $arr_mobile_num = [];
                                $password = \Config::get('custom.parent_password');
                                $student_parent = StudentParent::where('father_contact_number', $value->father_contact_number)->first();
                                if (empty($student_parent->admin_user_id)) {
                                    $user = new AdminUser();
                                    $user_type = DB::table('user_types as ut')
                                        ->join('user_roles as ur', 'ut.user_type_id', '=', 'ur.user_type_id')
                                        ->where('user_type', 'Parent')->select('ut.user_type_id', 'ur.user_role_id')->first();
                                    $user_type_id = $user_type->user_type_id;
                                    $user_role_id = $user_type->user_role_id;
                                    $user->mobile_number = $mobile_number;

                                    $user->password = Hash::make($password);

                                    if (!empty($user_type_id)) {
                                        $user->user_type_id = $user_type_id;
                                    }
                                    if (!empty($user_role_id)) {
                                        $user->user_role_id = $user_role_id;
                                    }
                                    $father_no = $value->father_contact_number;
                                    $mother_no = $value->mother_contact_number;
                                    $guardian_no = $value->guardian_contact_number;
                                    if (!empty($father_no)) {
                                        array_push($arr_mobile_num, $father_no);
                                    }
                                    if (!empty($mother_no)) {
                                        array_push($arr_mobile_num, $mother_no);
                                    }
                                    if (!empty($guardian_no)) {
                                        array_push($arr_mobile_num, $guardian_no);
                                    }

                                    if ($user->save()) {
                                        $student_parent = new StudentParent;
                                        // student parent table data
                                        $student_parent->father_email = $value->father_email;
                                        $student_parent->father_aadhaar_number = $value->father_aadhaar_number;
                                        $student_parent->father_first_name = $value->father_first_name;
                                        $student_parent->father_middle_name = $value->father_middle_name;
                                        $student_parent->father_last_name = $value->father_last_name;
                                        $student_parent->father_occupation = $value->father_occupation;
                                        $student_parent->father_contact_number = $father_no;
                                        $student_parent->father_income = $value->father_income;

                                        $student_parent->mother_email = $value->mother_email;
                                        $student_parent->mother_aadhaar_number = $value->mother_aadhaar_number;
                                        $student_parent->mother_first_name = $value->mother_first_name;
                                        $student_parent->mother_middle_name = $value->mother_middle_name;
                                        $student_parent->mother_last_name = $value->mother_last_name;
                                        $student_parent->mother_occupation = $value->mother_occupation;
                                        $student_parent->mother_contact_number = $mother_no;
                                        $student_parent->mother_income = $value->mother_income;

                                        $student_parent->guardian_email = $value->guardian_email;
                                        $student_parent->guardian_aadhaar_number = $value->guardian_aadhaar_number;
                                        $student_parent->guardian_first_name = $value->guardian_first_name;
                                        $student_parent->guardian_middle_name = $value->guardian_middle_name;
                                        $student_parent->guardian_last_name = $value->guardian_last_name;
                                        $student_parent->guardian_occupation = $value->guardian_occupation;
                                        $student_parent->guardian_relation = $value->guardian_relation;
                                        $student_parent->guardian_contact_number = $guardian_no;
                                        $student_parent->care_of = $value->care_of;
                                        $student_parent->sibling_exist = $value->sibling_exist;

                                    }
                                }
                                else{
                                    $user = AdminUser::where('mobile_number',$mobile_number)->first();
                                    array_push($arr_mobile_num, $mobile_number);
                                }
                                $student_data = new Student;
                                $student_data->current_session_id = $value->current_session_id;
                                $student_data->current_class_id = $value->current_class_id;
                                $student_data->current_section_id = $value->current_section_id;
                                $student_data->student_type_id = $value->student_type_id;
                                $student_data->enrollment_number = $value->enrollment_number;
                                $student_data->student_parent_id = $student_parent->student_parent_id;
                                $student_data->caste_category_id = $value->caste_category_id;
                                $student_data->first_name = $value->first_name;
                                $student_data->last_name = $value->last_name;
                                $student_data->middle_name = $value->middle_name;
                                $student_data->admission_date = date('Y-m-d', strtotime($value->admission_date));
                                $student_data->join_date = date('Y-m-d', strtotime($value->join_date));//date('Y-m-d'); //get_formatted_date($value->join_date, 'database');
                                $student_data->address_line1 = $value->address_line1;
                                $student_data->address_line2 = $value->address_line2;
                                $student_data->gender = $value->gender;
                                $student_data->dob = date('Y-m-d', strtotime($value->dob)); //$value->dob;
                                $student_data->remark = $value->remark;
                                $student_data->birth_place = $value->birth_place;
                                $student_data->fee_calculate_month = $value->fee_calculate_month;
                                $student_data->previous_class_name = $value->previous_class_name;
                                $student_data->previous_school_name = $value->previous_school_name;
                                $student_data->previous_due_fee = $value->previous_due_fee;
                                $student_data->previous_due_fee_original = $value->previous_due_fee;
                                $student_data->previous_school_name = $value->previous_school_name;
                                $student_data->previous_result = $value->previous_result;
                                $student_data->tc_number = $value->tc_number;
                                $student_data->tc_date = date('Y-m-d', strtotime($value->tc_date)); //!empty($value->tc_date) ? get_formatted_date($value->tc_date) : '';
                                $student_data->new_student = $value->new_student;
                                $student_data->student_left = $value->student_left;
                                $student_data->app_access_fee_status = 2;

                                $inserted_data = $user->adminStudentParent()->save($student_parent)->getParentStudent()->save($student_data);

                                $student_id = $inserted_data->student_id;
                                if (!empty($student_id)) {
                                    // send mail
                                    $email = '';
                                    if (!empty(Input::get('father_email'))) {
                                        $email = $value->father_email;
                                        $data['cc_address'] = $value->mother_email;
                                    } else {
                                        $email = $value->mother_email;
                                        $data['cc_address'] = $value->father_email;
                                    }
                                    if (!empty($email)) {
                                        $data['view_blade'] = 'admission-mail';
                                        $data['cc_name'] = $student_parent->mother_first_name;
                                        $data['subject'] = trans('language.admission_subject');
                                        $message_text_mail = trans('language.admission_mail_message');
                                        $message_text_mail .= "user name =" . $mobile_number;
                                        $message_text_mail .= "password =" . $password;
                                        $data['body'] = array(
                                            'message' => $message_text_mail,
                                            'enrollment_number' => $student_data->enrollment_number,
                                            'admission_date' => $student_data->admission_date,
                                        );
//                                        $mail_response = Mail::to($email)->send(new SendMailable($data));
                                    }
                                    // send sms  to parents
                                    if (!empty($mobile_number)) {
                                        $sms_mobile_number = implode($arr_mobile_num, ',');
                                        $message_text = "Dear Parent, registration has been created successfully. ";
                                        $message_text .= "Your app login credentials are : ";
                                        $message_text .= "user name = " . $mobile_number;
                                        $message_text .= " and password = " . $password;
                                        $message_text .= " Thank you. ";
                                        $response = send_sms($sms_mobile_number, $message_text);
                                    }

                                }
                            } catch (\Exception $e) {
                                //failed logic here
                                DB::rollback();
                                $error_message = $e->getMessage();
                                return redirect()->back()->withErrors($error_message);
                            }
                            DB::commit();
                            $status = 'success';
                        }
                    }
                }
                // remove imported file
                unlink($path);
                if ($status == 'success') {
                    return redirect('admin/student/')->withSuccess('Student imported successfully.');
                } else {
                    return redirect('admin/student/')->withError('Something went wrong, please try again');
                }
            }
        } else {
            return redirect()->back()->withErrors('Invalid File');
        }
    }
}