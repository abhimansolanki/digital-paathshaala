<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Model\backend\School;
use App\Model\backend\AdminUser;
use App\Model\backend\UserType;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class SchoolController extends Controller
{

    public function __construct()
    {

    }

    public function add(Request $request)
    {

        $school = School::where('status', 1)->first();
        $school['school_logo'] = check_file_exist($school['school_logo'], 'school_profile');
        $school['arr_session_month'] = get_current_session_months();
        $save_url = url('admin/school/save');
        $submit_button = 'Save';
        $data = array(
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'school' => $school,
        );
        return view('backend.school.add')->with($data);
    }

    public function save(Request $request)
    {
        if (!empty(Input::get('school_id'))) {
            $school = School::find(Input::get('school_id'));
            if (!empty($school->school_id)) {
                $success_msg = 'School profile updated successfully!';
            }
        } else {
            $school = new School;
            $success_msg = 'School profile saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'school_name' => 'required|min:3|max:250',
//                    'alias'               => 'required|min:3|max:250',
            'address' => 'required|min:3',
            'contact_number' => 'required|min:3|max:50',
            'mobile_number' => 'required|min:3|max:50',
            'email' => 'required|min:3|max:50|email',
            'started_year' => 'required',
            'registration_number' => 'required|min:3|max:250',
            'form_fee' => 'required',
        ]);

        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
//	    p($request->input());
            DB::beginTransaction(); //Start transaction!
            try {
                $user = get_loggedin_user_data();
                $school->admin_user_id = $user['admin_user_id'];
                $school->school_name = Input::get('school_name');
                $school->alias = Input::get('alias');
                $school->email = Input::get('email');
                $school->address = Input::get('address');
                $school->form_fee = Input::get('form_fee');
                $school->mobile_number = Input::get('mobile_number');
                $school->contact_number = Input::get('contact_number');
                $school->fax_number = Input::get('fax_number');
                $school->tin_number = Input::get('tin_number');
                $school->pan_number = Input::get('pan_number');
                $school->pin_code = Input::get('pin_code');
                $school->website_url = Input::get('website_url');
                $school->started_year = Input::get('started_year');
                $school->dies_code = Input::get('dies_code');
                $school->effilated_number = Input::get('effilated_number');
                $school->effilated_to = Input::get('effilated_to');
                $school->registration_number = Input::get('registration_number');
                $school->principal_name = Input::get('principal_name');
                $school->principal_contact_no = Input::get('principal_contact_no');
                $school->summer_start_time = Input::get('summer_start_time');
                $school->summer_end_time = Input::get('summer_end_time');
                $school->winter_start_time = Input::get('winter_start_time');
                $school->winter_end_time = Input::get('winter_end_time');
                $school->psp_code = Input::get('psp_code');
                $school->transport_fee_month = json_encode(Input::get('transport_fee_month'), JSON_FORCE_OBJECT);

                if ($request->hasFile('school_logo')) {
                    $file = $request->file('school_logo');
                    $config_upload_path = \Config::get('custom.school_profile');
                    $destinationPath = public_path() . $config_upload_path['upload_path'];
                    $filename = $file->getClientOriginalName();
                    $file->move($destinationPath, $filename);
                    $school->school_logo = $filename;
                }
                if ($request->hasFile('school_logo_color')) {
                    $file = $request->file('school_logo_color');
                    $config_upload_path = \Config::get('custom.school_profile');
                    $destinationPath = public_path() . $config_upload_path['upload_path'];
                    $filename = $file->getClientOriginalName();
                    $file->move($destinationPath, $filename);
                    $school->school_logo_color = $filename;
                }
                $school->whatsapp_phone = Input::get('whatsapp_phone');
                $school->facebook_url = Input::get('facebook_url');
                $school->instagram_url = Input::get('instagram_url');
                $school->twitter_url = Input::get('twitter_url');
                $school->youtube_url = Input::get('youtube_url');
                $school->linkedin_url = Input::get('linkedin_url');
                $school->app_version = Input::get('app_version');
                $school->mandatory_update = Input::get('mandatory_update');
                $school->save();
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
            return redirect('admin/school')->withSuccess($success_msg);
        }
    }

}
    