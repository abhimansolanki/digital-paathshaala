<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\AdminUser;
use App\Model\backend\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class SubAdminController extends Controller
{
    /**
     * Add and view for Sub Admin
     * @Bhuvanesh on 09 Apr 2021
     */
    public function add(Request $request, $id = null)
    {
        $sub_admin = [];
        if (!empty($id)) {
            $decrypted_sub_admin_id = get_decrypted_value($id, true);
            $sub_admin = AdminUser::where('admin_user_id', $decrypted_sub_admin_id)->first();
            if (empty($sub_admin)) {
                return redirect()->back()->withError('Sub Admin not found!');
            }
            $encrypted_user_role_id = get_encrypted_value($sub_admin['user_role_id'], true);
            $save_url = route('admin.sub-admin.save', [$encrypted_user_role_id]);
            $submit_button = 'Update';
        } else {
            $save_url = route('admin.sub-admin.save');
            $submit_button = 'Save';
        }
        $user_roles = UserRole::orderBy('user_role_name')->get()->pluck("user_type_id", "user_type_name")->toArray();
        $user_roles = add_blank_option($user_roles, "Select");
        $data = array(
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'user_role' => $user_roles,
            'sub_admin' => $sub_admin,
            'redirect_url' => route('admin.sub-admin.index'),
        );
        return view('backend.sub-admin.add')->with($data);
    }

    /**
     * Any Data for Sub Admin
     * @Bhuvanesh on 09 Apr 2021
     */
    public function anyData(Request $request)
    {
        $sub_admins = UserRole::orderBy('user_role_name')->get()->toArray();
        return Datatables::of($sub_admins)
            ->addColumn('user_role_type', function ($user_roles) {
                $user_type = \Config::get('custom.user_type');
                return $user_type[$user_roles['user_type_id']];
            })
            ->addColumn('user_role_status', function ($user_roles) {
                if ($user_roles['user_role_status'] == '1') {
                    return 'Active';
                } else {
                    return 'Inactive';
                }
            })
            ->addColumn('action', function ($user_roles) {
                $delete = '';
                if ($user_roles['user_role_id'] != 1) {
                    $delete = ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $user_roles['user_role_id'] . '"><i class="fa fa-trash"></i></button>';
                }
                $encrypted_user_role_id = get_encrypted_value($user_roles['user_role_id'], true);
                //p($encrypted_user_role_id);
                return '<a title="Edit" id="deletebtn1" href="' . url('admin/user-role/' . $encrypted_user_role_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                    . $delete;
            })->rawColumns(['user_role_type' => 'user_role_type', 'user_role_status' => 'user_role_status', 'action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     * Save Sub Admin
     * @Bhuvanesh on 09 Apr 2021
     */
    public function save(Request $request, $id = null)
    {
        $decrypted_sub_admin_id = ($id != null) ? get_decrypted_value($id, true) : null;
        $validatior = Validator::make($request->all(), [
            'user_role_name' => 'required|unique:user_roles,user_role_name,' . $decrypted_sub_admin_id . ',user_role_id',
        ]);
        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $sub_admin = UserRole::find($decrypted_sub_admin_id);
                if (!$sub_admin) {
                    return redirect('/admin/user-role/')->withError('User Role not found!');
                }
                $success_msg = 'Sub Admin updated successfully!';
            } else {
                $sub_admin = new UserRole;
                $success_msg = 'Sub Admin saved successfully!';
            }
            $sub_admin->user_role_name = Input::get('user_role_name');
            $sub_admin->user_type_id = Input::get('user_role_type');
            $sub_admin->save();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
        DB::commit();
        return redirect()->to("admin.sub-admin.index")->withSuccess($success_msg);
    }

    /**
     * Delete Sub Admin
     * @Bhuvanesh on 09 Apr 2021
     */
    public function destroy(Request $request)
    {
        $sub_admin_id = Input::get('sub_admin_id');
        $sub_admin = UserRole::find($sub_admin_id);
        if ($sub_admin) {
            DB::beginTransaction(); //Start transaction!
            try {
                $sub_admin->delete();
                $return_arr = array(
                    'status' => 'success',
                    'message' => 'Sub Admin deleted successfully!'
                );
            } catch (\Exception $e) {
                DB::rollback();
                $return_arr = array(
                    'status' => $e->getMessage(),
                    'message' => trans('language.delete_message')
                );
                return response()->json($return_arr, 200);
            }
            DB::commit();
        } else {
            $return_arr = array(
                'status' => 'error',
                'message' => 'Sub Admin not found!'
            );
        }
        return response()->json($return_arr, 200);
    }
}
