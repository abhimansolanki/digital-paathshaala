<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\ExamRollNumber;
use App\Model\backend\Student;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Datatables;
use Illuminate\Support\Facades\View as View;

class ExamRollNumberController extends Controller
{

    public function __construct()
    {

    }

    public function add(Request $request)
    {
        $exam_roll_number = [];
        $last_roll_no = ExamRollNumber::max('roll_number');
        $save_url = url('admin/exam-roll-no/save');
        $submit_button = 'Save';
        $exam_roll_number['arr_session'] = add_blank_option(get_session(), '-- Academic year --');
        $exam_roll_number['arr_class'] = add_blank_option(get_all_classes(), '--Select class --');
        $data = array(
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'exam_roll_number' => $exam_roll_number,
            'last_roll_no' => !empty($last_roll_no) ? $last_roll_no + 1 : 1,
            'redirect_url' => url('admin/exam-roll-number/'),
        );
        return view('backend.exam-roll-number.add')->with($data);
    }

    public function save(Request $request)
    {
        $class_id = Input::get('class_id');
        $session_id = Input::get('session_id');
        $status = 'failed';
        $message = "Parameters cann't be empty";
//            $arr_input_fields = [
//                'class_id'   => 'required',
//                'session_id' => 'required',
//                'prefix'     => 'required',
//                'start_no'   => 'required',
//            ];
//            $validatior       = Validator::make($request->all(), $arr_input_fields);
        if ($request->input()) {
//                return redirect()->back()->withInput()->withErrors($validatior);
//            }
//            else
//            {
            DB::beginTransaction(); //Start transaction!
            try {
                $prefix = Input::get('prefix');
                $start_no = Input::get('start_no');
//                    $arr_student_id = Input::get('student_id');
                $arr_student_id = Student::select('student_id')->where('current_class_id', $class_id)
                    ->where('current_session_id', $session_id)->where('student_status', 1)
                    ->orderBy('first_name', 'ASC')->get();
                $arr_student_id = json_decode($arr_student_id, true);
                foreach ($arr_student_id as $key => $student_id) {
                    $exam_roll_number = ExamRollNumber::where(['session_id' => $session_id, 'class_id' => $class_id, 'student_id' => $student_id['student_id']])->first();
                    if(empty($exam_roll_number)){
                        $exam_roll_number = new ExamRollNumber;
                        $exam_roll_number->session_id = $session_id;
                        $exam_roll_number->class_id = $class_id;
                        $exam_roll_number->student_id = $student_id['student_id'];
                    }
                    $exam_roll_number->prefix = $prefix;
                    $exam_roll_number->roll_number = $start_no;
                    $exam_roll_number->save();
                    $start_no++;
                }
//                if (!empty($exam_roll_number)) {
//                    ExamRollNumber::insert($exam_roll_number);
//                }
                $message = 'Exam Roll Number saved successfully!';
                $status = 'success';
            } catch (\Exception $e) {
                //failed logic here
                DB::rollback();
                $message = $e->getMessage();
                $status = 'error';
//                    return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return response()->json(array('status' => $status, 'message' => $message));
//            return redirect('admin/exam-roll-no')->withSuccess($success_msg);
    }

    function StudentExamRollNo()
    {
        $class_id = Input::get('class_id');
        $session_id = Input::get('session_id');
        $status = 'failed';
        $student_exist = 'no';
        if (!empty($class_id) && !empty($session_id)) {
            $arr_roll_no = DB::table('exam_roll_numbers as ern')
                ->where('ern.class_id', $class_id)
                ->where('ern.session_id', $session_id)
                ->orderBy('ern.exam_roll_number_id', 'ASC')
                ->select('ern.roll_number', 'ern.exam_roll_number_id', 'ern.prefix', 'ern.student_id')
                ->first();
            $last_roll_no = ExamRollNumber::max('roll_number');
            $last_roll_no = !empty($last_roll_no) ? $last_roll_no + 1 : 1;
            $prefix = '';
            $start_no = $last_roll_no;
            if (!empty($arr_roll_no->roll_number)) {
                $student_exist = 'yes';
                $prefix = $arr_roll_no->prefix;
                $start_no = $arr_roll_no->roll_number;
            }
            $status = 'success';
        }
        return response()->json(array('status' => $status, 'prefix' => $prefix, 'start_no' => $start_no, 'student_status' => $student_exist));
    }

    function getStudentExamRollNo(Request $request)
    {
        $class_id = Input::get('class_id');
        $session_id = Input::get('session_id');
        $data = [];
        if (!empty($class_id) && !empty($session_id)) {
            $arr_roll_no = DB::table('students as s')
                ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
                ->leftJoin('exam_roll_numbers as ern', 'ern.student_id', '=', 's.student_id')
                ->where('s.current_class_id', $class_id)
                ->where('s.current_session_id', $session_id)
                ->where('ern.class_id', $class_id)
                ->where('ern.session_id', $session_id)
                ->where('s.student_status', 1)
                ->where('s.student_left', "No")
                ->orderBy('s.first_name', 'ASC')
                ->select('ern.roll_number', 'ern.exam_roll_number_id', 'ern.prefix', 's.student_id', 's.first_name', 's.middle_name', 's.last_name', 's.enrollment_number', 'sp.father_first_name', 'sp.father_middle_name', 'sp.father_last_name', 'sp.father_contact_number')
                ->get();
            if(count($arr_roll_no) == 0){
                $arr_roll_no = DB::table('students as s')
                ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
                ->where('s.current_class_id', $class_id)
                ->where('s.current_session_id', $session_id)
                ->where('s.student_status', 1)
                ->where('s.student_left', "No")
                ->orderBy('s.first_name', 'ASC')
                ->select('s.student_id', 's.first_name', 's.middle_name', 's.last_name', 's.enrollment_number', 'sp.father_first_name', 'sp.father_middle_name', 'sp.father_last_name', 'sp.father_contact_number')
                ->get();
            }    
            foreach ($arr_roll_no as $roll_no) {
                $roll_no = (array)$roll_no;
                $data[] = array(
                    'student_id' => $roll_no['student_id'],
                    'roll_number' => isset($roll_no['prefix']) ? $roll_no['prefix'] . $roll_no['roll_number'] : "",
                    'exam_roll_number_id' => isset($roll_no['exam_roll_number_id']) ? $roll_no['exam_roll_number_id'] : "",
                    'enrollment_number' => $roll_no['enrollment_number'],
                    'father_contact_number' => $roll_no['father_contact_number'],
                    'student_name' => $roll_no['first_name'] . ' ' . $roll_no['middle_name'] . ' ' . $roll_no['last_name'],
                    'father_name' => $roll_no['father_first_name'] . ' ' . $roll_no['father_middle_name'] . ' ' . $roll_no['father_last_name'],
                );
            }
        }
        return datatables::of($data)->make(true);
    }

    function checkRollNo(Request $request)
    {
        $session_id = Input::get('session_id');
        $class_id = Input::get('class_id');
        $check_edit = Input::get('check_edit');
        if (!empty($session_id)) {
            $query = DB::table('exam_roll_numbers as ern')
                ->where('ern.session_id', $session_id)
                ->select('ern.roll_number', 'ern.exam_roll_number_id', 'ern.prefix');
            if ($check_edit == 'yes') {
                $query->where('class_id', '!=', $class_id);
            }

            if ($request->has('start_no')) {
                $start_no = Input::get('start_no');
                $query->where('roll_number', $start_no);
            } else {
                $prefix = Input::get('prefix');
                $query->where('prefix', $prefix);
            }
            $data = $query->first();
//                p($data);
            if (!empty($data->roll_number)) {
                return response()->json(false);
            }
        }
        return response()->json(true);
    }

}