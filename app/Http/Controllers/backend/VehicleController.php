<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Vehicle;
    use App\Model\backend\Student;
    use App\Model\backend\Employee;
    use App\Model\backend\Route;
    use App\Model\backend\VehicleProvider;
    use App\Model\backend\VehicleDriver;
    use App\Model\backend\VehicleDriverHelper;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;
    use PDF;
    use Illuminate\Support\Facades\View as View;

    class VehicleController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index()
        {
            $data = array(
                'redirect_url' => url('admin/vehicle/add'),
            );
            return view('backend.vehicle.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {
            $vehicle    = [];
            $vehicle_id = null;
            if (!empty($id))
            {
                $decrypted_vehicle_id = get_decrypted_value($id, true);
                $vehicle              = $this->getVehicleData($decrypted_vehicle_id);
                $vehicle              = isset($vehicle[0]) ? $vehicle[0] : [];
                if (!$vehicle)
                {
                    return redirect('admin/vehicle')->withError('Vehicle not found!');
                }
                $encrypted_vehicle_id = get_encrypted_value($vehicle['vehicle_id'], true);
                $save_url             = url('admin/vehicle/save/' . $encrypted_vehicle_id);
                $submit_button        = 'Update';
                $vehicle_id           = $decrypted_vehicle_id;
            }
            else
            {
                $save_url      = url('admin/vehicle/save');
                $submit_button = 'Save';
            }
            $this->RouteController = new RouteController;
            $arr_route             = $this->RouteController->getRouteData();
            $all_route             = [];
            foreach ($arr_route as $route)
            {
                $all_route[$route['route_id']] = $route['route'];
            }

            $arr_session                   = get_session();
            $vehicle['arr_session']        = add_blank_option($arr_session, '-- Academic year --');
            $vehicle['arr_vehicle_status'] = \Config::get('custom.vehicle_status');
            $vehicle['arr_route']          = add_blank_option($all_route, '-- Select route --');
            $data                          = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'vehicle'       => $vehicle,
                'redirect_url'  => url('admin/vehicle/'),
            );
            return view('backend.vehicle.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $vehicle_provider_id  = null;
            $vehicle_helper_id    = null;
            $vehicle_driver_id    = null;
            $decrypted_vehicle_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $vehicle = Vehicle::find($decrypted_vehicle_id);

                $vehicle_provider_id = $vehicle->vehicle_provider_id;
                $vehicle_helper_id   = $vehicle->vehicle_helper_id;
                $vehicle_driver_id   = $vehicle->vehicle_driver_id;

                $vehicle_provider = VehicleProvider::find($vehicle_provider_id);
                $vehicle_helper   = VehicleDriverHelper::find($vehicle_helper_id);
                $vehicle_driver   = VehicleDriver::find($vehicle_driver_id);

                if (!$vehicle)
                {
                    return redirect('/admin/vehicle/')->withError('Vehicle not found!');
                }
                $success_msg = 'Vehicle updated successfully!';
            }
            else
            {
                $vehicle             = New Vehicle;
                $vehicle_provider_id = Input::get('vehicle_provider_id');
                $vehicle_driver_id   = Input::get('vehicle_driver_id');
                $vehicle_helper_id   = Input::get('vehicle_helper_id');
                if ($request->has('vehicle_provider_id') && !empty($vehicle_provider_id))
                {
                    $vehicle_provider = VehicleProvider::Find($vehicle_provider_id);
                }
                else
                {
                    $vehicle_provider = New VehicleProvider;
                }

                if ($request->has('vehicle_driver_id') && !empty($vehicle_driver_id))
                {
                    $vehicle_driver = VehicleDriver::Find($vehicle_driver_id);
                }
                else
                {
                    $vehicle_driver = New VehicleDriver;
                }

                if ($request->has('vehicle_helper_id') && !empty($vehicle_helper_id))
                {
                    $vehicle_helper = VehicleDriverHelper::Find($vehicle_helper_id);
                }
                else
                {
                    $vehicle_helper = New VehicleDriverHelper;
                }
                $success_msg = 'Vehicle saved successfully!';
            }
            $validatior = Validator::make($request->all(), [
                    'vehicle_number'          => 'required|unique:vehicles,vehicle_number,' . $decrypted_vehicle_id . ',vehicle_id',
                    'driver_contact_number'   => 'required|unique:vehicle_drivers,driver_contact_number,' . $vehicle_driver_id . ',vehicle_driver_id',
                    'helper_contact_number'   => 'required|unique:vehicle_driver_helpers,helper_contact_number,' . $vehicle_helper_id . ',vehicle_helper_id',
                    'provider_contact_number' => 'required|unique:vehicle_providers,provider_contact_number,' . $vehicle_provider_id . ',vehicle_provider_id',
                    'route_id'                => 'required',
                    'vehicle_status'          => 'required',
                    'vehicle_name'            => 'required',
                    'vehicle_color'           => 'required',
//                    'gps_device_id'           => 'required',
                    'total_seats'             => 'required',
                    'strength'                => 'required',
                    'purchase_date'           => 'required',
                    'insurance_date'          => 'required',
                    'ins_expired_date'        => 'required',
                    'driver_full_name'        => 'required',
                    'driver_aadhaar_number'   => 'required',
//                    'driver_aadhaar'            => 'required',
                    'driver_license_number'   => 'required',
//                    'driver_licence'          => 'required',
                    'provider_full_name'      => 'required',
                    'provider_aadhaar_number' => 'required',
//                    'provider_aadhaar'          => 'required',
                    'helper_full_name'        => 'required',
                    'helper_aadhaar_number'   => 'required',
//                    'helper_aadhaar'            => 'required',
                    'helper_license_number'   => 'required',
//                    'helper_license'          => 'required',
                    'helper_address'          => 'required',
                    'session_id'              => 'required',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction(); //Start transaction!

                try
                {
                    /*
                     * Add vehicle provider
                     */

                    $vehicle_provider->provider_full_name      = Input::get('provider_full_name');
                    $vehicle_provider->provider_email_address  = Input::get('provider_email_address');
                    $vehicle_provider->provider_address        = Input::get('provider_address');
                    $vehicle_provider->provider_contact_number = Input::get('provider_contact_number');
                    $vehicle_provider->provider_aadhaar_number = Input::get('provider_aadhaar_number');
                    if ($request->hasFile('provider_aadhaar'))
                    {
                        $file                               = $request->file('provider_aadhaar');
                        $config_upload_path                 = \Config::get('custom.provider_doc');
                        $destinationPath                    = public_path() . $config_upload_path['upload_path'];
                        $filename                           = $file->getClientOriginalName();
                        $rand_string                        = quick_random();
                        $filename                           = $rand_string . '-' . $filename;
                        $file->move($destinationPath, $filename);
                        $vehicle_provider->provider_aadhaar = $filename;
                    }
                    $vehicle_provider->save();

                    /*
                     * Add vehicle driver
                     */

                    $vehicle_driver->driver_full_name      = Input::get('driver_full_name');
                    $vehicle_driver->driver_email_address  = Input::get('driver_email_address');
                    $vehicle_driver->driver_address        = Input::get('driver_address');
                    $vehicle_driver->driver_contact_number = Input::get('driver_contact_number');
                    $vehicle_driver->driver_aadhaar_number = Input::get('driver_aadhaar_number');
                    $vehicle_driver->driver_license_number = Input::get('driver_license_number');

                    $config_upload_path_driver = \Config::get('custom.driver_doc');
                    $destinationPath           = public_path() . $config_upload_path_driver['upload_path'];
                    if ($request->hasFile('driver_aadhaar'))
                    {
                        $file                           = $request->file('driver_aadhaar');
                        $filename                       = $file->getClientOriginalName();
                        $rand_string                    = quick_random();
                        $filename                       = $rand_string . '-' . $filename;
                        $file->move($destinationPath, $filename);
                        $vehicle_driver->driver_aadhaar = $filename;
                    }
                    if ($request->hasFile('driver_license'))
                    {
                        $file                           = $request->file('driver_license');
                        $filename                       = $file->getClientOriginalName();
                        $rand_string                    = quick_random();
                        $filename                       = $rand_string . '-' . $filename;
                        $file->move($destinationPath, $filename);
                        $vehicle_driver->driver_license = $filename;
                    }

                    $vehicle_driver->save();

                    /*
                     * Add vehicle driver helper
                     */

                    $vehicle_helper->helper_full_name      = Input::get('helper_full_name');
                    $vehicle_helper->helper_email_address  = Input::get('helper_email_address');
                    $vehicle_helper->helper_address        = Input::get('helper_address');
                    $vehicle_helper->helper_contact_number = Input::get('helper_contact_number');
                    $vehicle_helper->helper_aadhaar_number = Input::get('helper_aadhaar_number');
                    $vehicle_helper->helper_license_number = Input::get('helper_license_number');

                    $config_upload_path_helper = \Config::get('custom.helper_doc');
                    $destinationPath1          = public_path() . $config_upload_path_helper['upload_path'];
                    if ($request->hasFile('helper_aadhaar'))
                    {

                        $file                           = $request->file('helper_aadhaar');
                        $filename                       = $file->getClientOriginalName();
                        $rand_string                    = quick_random();
                        $filename                       = $rand_string . '-' . $filename;
                        $file->move($destinationPath1, $filename);
                        $vehicle_helper->helper_aadhaar = $filename;
                    }
                    if ($request->hasFile('helper_license'))
                    {
                        $file                           = $request->file('helper_license');
                        $filename                       = $file->getClientOriginalName();
                        $rand_string                    = quick_random();
                        $filename                       = $rand_string . '-' . $filename;
                        $file->move($destinationPath1, $filename);
                        $vehicle_helper->helper_license = $filename;
                    }
                    $vehicle_helper->save();

                    /*
                     * Add vehicle with provider, driver and helper details
                     */
                    $vehicle->route_id            = Input::get('route_id');
                    $vehicle->session_id          = Input::get('session_id');
                    $vehicle->vehicle_provider_id = $vehicle_provider->vehicle_provider_id;
                    $vehicle->vehicle_driver_id   = $vehicle_driver->vehicle_driver_id;
                    $vehicle->vehicle_helper_id   = $vehicle_helper->vehicle_helper_id;
                    $vehicle->vehicle_number      = Input::get('vehicle_number');
                    $vehicle->vehicle_name        = Input::get('vehicle_name');
                    $vehicle->vehicle_color       = Input::get('vehicle_color');
                    $vehicle->gps_device_id       = Input::get('gps_device_id');
                    $vehicle->total_seats         = Input::get('total_seats');
                    $vehicle->strength            = Input::get('strength');
                    $vehicle->vehicle_status      = Input::get('vehicle_status');
                    $vehicle->purchase_date       = get_formatted_date(Input::get('purchase_date'), 'database');
                    $vehicle->insurance_date      = get_formatted_date(Input::get('insurance_date'), 'database');
                    $vehicle->ins_expired_date    = get_formatted_date(Input::get('ins_expired_date'), 'database');
                    $vehicle->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withInput()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/vehicle')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $vehicle_id = Input::get('vehicle_id');
            $vehicle    = Vehicle::find($vehicle_id);
            if ($vehicle)
            {
                DB::beginTransaction(); //Start transaction!
                try
                {
                    $vehicle->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Vehicle deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Vehicle not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $vehicle     = [];
            $limit       = Input::get('length');
            $offset      = Input::get('start');
            $arr_vehicle = $this->getVehicleData($vehicle_id  = null, $offset, $limit);
            foreach ($arr_vehicle as $key => $vehicle_data)
            {
                $vehicle[$key] = (object) $vehicle_data;
            }
            return Datatables::of($vehicle)
                    ->addColumn('action', function ($vehicle)
                    {
                        $encrypted_vehicle_id = get_encrypted_value($vehicle->vehicle_id, true);
                        return '<a title="Edit" id="deletebtn1" href="vehicle/add/' . $encrypted_vehicle_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>';
//                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $vehicle->vehicle_id . '"><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

        public function getVehicleData($vehicle_id = null, $offset = null, $limit = null)
        {
            $vehicle_return   = [];
            $arr_vehicle_data = Vehicle::
                where(function($query) use ($vehicle_id)
                {
                    if (!empty($vehicle_id))
                    {
                        $query->where('vehicle_id', $vehicle_id);
                    }
                })
                ->with('vehicleRoute')
                ->with('vehicleProvider')
                ->with('vehicleDriver')
                ->with('vehicleHelper')
//                ->where(function($query) use ($limit, $offset)
//                {
//                    if (!empty($limit))
//                    {
//                        $query->skip($offset);
//                        $query->take($limit);
//                    }
//                })
                ->get();

            if (!empty($arr_vehicle_data))
            {
                $vehicle = [];
                foreach ($arr_vehicle_data as $key => $vehicle_data)
                {
                    $vehicle          = array(
                        'vehicle_id'              => $vehicle_data['vehicle_id'],
                        'session_id'              => $vehicle_data['session_id'],
                        'route_id'                => $vehicle_data['route_id'],
                        'vehicle_provider_id'     => $vehicle_data['vehicle_provider_id'],
                        'vehicle_driver_id'       => $vehicle_data['vehicle_driver_id'],
                        'vehicle_helper_id'       => $vehicle_data['vehicle_helper_id'],
                        'vehicle_number'          => $vehicle_data['vehicle_number'],
                        'vehicle_name'            => $vehicle_data['vehicle_name'],
                        'vehicle_color'           => $vehicle_data['vehicle_color'],
                        'gps_device_id'           => $vehicle_data['gps_device_id'],
                        'total_seats'             => $vehicle_data['total_seats'],
                        'strength'                => $vehicle_data['strength'],
                        'vehicle_status'          => $vehicle_data['vehicle_status'],
                        'route'                   => $vehicle_data['vehicleRoute']['route'],
                        'purchase_date'           => get_formatted_date($vehicle_data['purchase_date'], 'display'),
                        'insurance_date'          => get_formatted_date($vehicle_data['insurance_date'], 'display'),
                        'ins_expired_date'        => get_formatted_date($vehicle_data['ins_expired_date'], 'display'),
                        'provider_full_name'      => $vehicle_data['vehicleProvider']['provider_full_name'],
                        'provider_contact_number' => $vehicle_data['vehicleProvider']['provider_contact_number'],
                        'provider_email_address'  => $vehicle_data['vehicleProvider']['provider_email_address'],
                        'provider_address'        => $vehicle_data['vehicleProvider']['provider_address'],
                        'provider_aadhaar_number' => $vehicle_data['vehicleProvider']['provider_aadhaar_number'],
                        'provider_aadhaar'        => check_file_exist($vehicle_data['vehicleProvider']['provider_aadhaar'], 'provider_doc'),
                        'driver_full_name'        => $vehicle_data['vehicleDriver']['driver_full_name'],
                        'driver_contact_number'   => $vehicle_data['vehicleDriver']['driver_contact_number'],
                        'driver_email_address'    => $vehicle_data['vehicleDriver']['driver_email_address'],
                        'driver_address'          => $vehicle_data['vehicleDriver']['driver_address'],
                        'driver_aadhaar_number'   => $vehicle_data['vehicleDriver']['driver_aadhaar_number'],
                        'driver_aadhaar'          => check_file_exist($vehicle_data['vehicleDriver']['driver_aadhaar'], 'driver_doc'),
                        'driver_license_number'   => $vehicle_data['vehicleDriver']['driver_license_number'],
                        'driver_license'          => check_file_exist($vehicle_data['vehicleDriver']['driver_license'], 'driver_doc'),
                        'helper_full_name'        => $vehicle_data['vehicleHelper']['helper_full_name'],
                        'helper_address'          => $vehicle_data['vehicleHelper']['helper_address'],
                        'helper_email_address'    => $vehicle_data['vehicleHelper']['helper_email_address'],
                        'helper_contact_number'   => $vehicle_data['vehicleHelper']['helper_contact_number'],
                        'helper_aadhaar_number'   => $vehicle_data['vehicleHelper']['helper_aadhaar_number'],
                        'helper_aadhaar'          => check_file_exist($vehicle_data['vehicleHelper']['helper_aadhaar'], 'helper_doc'),
                        'helper_license_number'   => $vehicle_data['vehicleHelper']['helper_license_number'],
                        'helper_license'          => check_file_exist($vehicle_data['vehicleHelper']['helper_license'], 'helper_doc'),
                    );
                    $vehicle_return[] = $vehicle;
                }
            }
            return $vehicle_return;
        }

        public function getVehicleMasterData()
        {
            $vehicle_data = [];
            $status       = 'failed';
            $request_type = Input::get('request_type');
            if (!empty($request_type))
            {
                if (trim($request_type) == 'provider')
                {
                    $provider_contact_number = Input::get('provider_contact_number');
                    $vehicle_provider_data   = VehicleProvider::where('provider_contact_number', $provider_contact_number)->first();
                    if(!empty($vehicle_provider_data)){
                        $vehicle_data            = array(
                            'vehicle_provider_id'     => $vehicle_provider_data['vehicle_provider_id'],
                            'provider_full_name'      => $vehicle_provider_data['provider_full_name'],
                            'provider_contact_number' => $vehicle_provider_data['provider_contact_number'],
                            'provider_email_address'  => $vehicle_provider_data['provider_email_address'],
                            'provider_address'        => $vehicle_provider_data['provider_address'],
                            'provider_aadhaar_number' => $vehicle_provider_data['provider_aadhaar_number'],
                        );
                    }
                }
                elseif (trim($request_type) == 'driver')
                {
                    $driver_contact_number = Input::get('driver_contact_number');
                    $vehicle_driver_data   = VehicleDriver::where('driver_contact_number', $driver_contact_number)->first();
                    if(!empty($vehicle_driver_data)){
                        $vehicle_data = array(
                            'vehicle_driver_id'     => $vehicle_driver_data['vehicle_driver_id'],
                            'driver_full_name'      => $vehicle_driver_data['driver_full_name'],
                            'driver_contact_number' => $vehicle_driver_data['driver_contact_number'],
                            'driver_email_address'  => $vehicle_driver_data['driver_email_address'],
                            'driver_address'        => $vehicle_driver_data['driver_address'],
                            'driver_aadhaar_number' => $vehicle_driver_data['driver_aadhaar_number'],
                            'driver_license_number' => $vehicle_driver_data['driver_license_number'],
                        );
                    }
                }
                elseif (trim($request_type) == 'helper')
                {
                    $helper_contact_number = Input::get('helper_contact_number');
                    $vehicle_helper_data   = VehicleDriverHelper::where('helper_contact_number', $helper_contact_number)->first();
                    if(!empty($vehicle_helper_data)){
                        $vehicle_data          = array(
                            'vehicle_helper_id'     => $vehicle_helper_data['vehicle_helper_id'],
                            'helper_full_name'      => $vehicle_helper_data['helper_full_name'],
                            'helper_address'        => $vehicle_helper_data['helper_address'],
                            'helper_email_address'  => $vehicle_helper_data['helper_email_address'],
                            'helper_contact_number' => $vehicle_helper_data['helper_contact_number'],
                            'helper_aadhaar_number' => $vehicle_helper_data['helper_aadhaar_number'],
                            'helper_license_number' => $vehicle_helper_data['helper_license_number'],
                        );
                    }
                }
                $status = 'success';
            }
            return response()->json(array('status' => $status, 'data' => $vehicle_data));
        }

        public function transportReport()
        {
            $arr_vehicle      = [];
            $arr_route        = [];
            // vehicle
            $arr_vehicle_data = $this->getVehicleData();
            foreach ($arr_vehicle_data as $key => $vehicle_data)
            {
                $arr_vehicle[$vehicle_data['vehicle_id']] = $vehicle_data['vehicle_name'] . '(' . $vehicle_data['vehicle_number'] . ')';
            }
            $data['arr_vehicle'] = add_blank_option($arr_vehicle, '--Select Vehicle--');

            // route
            $arr_route_data = Route::where('route_status', 1)->get();
            foreach ($arr_route_data as $key => $route_data)
            {
                $arr_route[$route_data['route_id']] = $route_data['route'];
            }
            $data['arr_route'] = add_blank_option($arr_route, '--Select Route--');

            $class_id               = null;
            $session_id             = null;
            $arr_class              = get_all_classes();
            $arr_section            = get_class_section($class_id, $session_id);
            $data['arr_class']      = add_blank_option($arr_class, '--Select class --');
            $data['arr_section']    = add_blank_option($arr_section, '--Select section --');
            $data['arr_stop_point'] = add_blank_option([], '--Select stop point --');

            return view('backend.report.transport-report')->with($data);
        }

        public function transportReportData(Request $request)
        {

            $arr_transport_data = [];
            $arr_employee       = [];
            $arr_student        = [];
            $vehicle_id         = Input::get('vehicle_id');
            $route_id           = Input::get('route_id');
            $stop_point_id      = Input::get('stop_point_id');
            $class_id           = Input::get('class_id');
            $section_id         = Input::get('section_id');
            $first_name         = Input::get('first_name');
            $middle_name        = Input::get('middle_name');
            $last_name          = Input::get('last_name');
            $type               = Input::get('type');
            $session            = get_current_session();

            /*
             * get students by vehicle or route , name and class
             */
            if (!empty($session))
            {
                $vehicle_query_student = DB::table('student_vehicle_assigned_details as svad')
                    ->join('student_vehicle_assigned as sva', 'sva.student_vehicle_assigned_id', '=', 'svad.assigned_id')
                    ->join('vehicles as v', 'v.vehicle_id', '=', 'sva.vehicle_id')
                    ->join('stop_points as stop_p', 'stop_p.stop_point_id', '=', 'sva.stop_point_id')
                    ->join('routes as r', 'r.route_id', '=', 'stop_p.route_id')
                    ->join('students as s', 's.student_id', '=', 'svad.student_id')
                    ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
                    ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
                    ->leftJoin('sections as sec', 'sec.section_id', '=', 's.current_section_id')
                    ->select('v.vehicle_number', 'v.vehicle_id', 'svad.assigned_to', 'sva.student_vehicle_assigned_id', 'stop_p.stop_point_id', 'stop_p.stop_point', 'stop_p.stop_fair', 'svad.student_vehicle_assigned_detail_id', 's.enrollment_number', 's.student_id', 'sp.father_first_name', 'sp.father_last_name', 'sp.father_middle_name', 'sp.father_contact_number', 's.first_name', 's.last_name', 's.middle_name', 'c.class_name', 'sec.section_name'
                        , DB::raw("CONCAT(s.first_name, ' ', s.middle_name, ' ', s.last_name) as student_name")
                        , DB::raw("CONCAT(sp.father_first_name, ' ', sp.father_middle_name, ' ', sp.father_last_name) as father_name")
                    )
                    ->where('sva.session_id', $session['session_id'])
                    ->where('svad.assigned_to', 1)
                    ->where('svad.assigned_status', 1);

                if ($type == 'vehicle')
                {
                    if (!empty($vehicle_id))
                    {
                        $vehicle_query_student->where('sva.vehicle_id', $vehicle_id);
                    }
                    $vehicle_query_student->orderBy('v.vehicle_id');
                }
                if ($type == 'route')
                {
                    if (!empty($route_id))
                    {
                        $vehicle_query_student->where('r.route_id', $route_id);
                    }
                    if (!empty($stop_point_id))
                    {
                        $vehicle_query_student->where('sva.stop_point_id', $stop_point_id);
                    }
                    $vehicle_query_student->orderBy('r.route_id');
                }

                if ($type == 'class')
                {
                    if (!empty($class_id))
                    {
                        $vehicle_query_student->where('s.current_class_id', $class_id);
                    }
                    if (!empty($section_id))
                    {
                        $vehicle_query_student->where('s.current_section_id', $section_id);
                    }
                }
                if ($type == 'student')
                {
                    if (!empty($first_name))
                    {
                        $vehicle_query_student->where('s.first_name', 'LIKE', "%" . $first_name . "%");
                    }
                    if (!empty($middle_name))
                    {
                        $vehicle_query_student->where('s.middle_name', 'LIKE', "%" . $middle_name . "%");
                    }
                    if (!empty($last_name))
                    {
                        $vehicle_query_student->where('s.last_name', 'LIKE', "%" . $last_name . "%");
                    }
                    $vehicle_query_student->orderBy('s.first_name');
                }
                $arr_student_transport_data = $vehicle_query_student->get();
                foreach ($arr_student_transport_data as $key => $student_data)
                {
                    $student_data = (array) $student_data;
                    $from         = '';
                    if (!empty($student_data['section_name']))
                    {
                        $from = $student_data['class_name'] . '(' . $student_data['section_name'] . ')';
                    }
                    else
                    {
                        $from = $student_data['class_name'];
                    }
                    $arr_student[] = array(
                        'type'           => 'Student',
                        'id'             => $student_data['student_vehicle_assigned_detail_id'],
                        'user_id'        => $student_data['student_id'],
                        'code'           => $student_data['enrollment_number'],
                        'assigned_to'    => $student_data['assigned_to'],
                        'name'           => $student_data['first_name'] . ' ' . $student_data['middle_name'] . ' ' . $student_data['last_name'],
                        'father_name'    => $student_data['father_first_name'] . ' ' . $student_data['father_middle_name'] . ' ' . $student_data['father_last_name'],
                        'from'           => $from,
                        'contact_number' => $student_data['father_contact_number'],
                        'vehicle_number' => $student_data['vehicle_number'],
                        'stop_point'     => $student_data['stop_point'],
                        'stop_fair'      => $student_data['stop_fair'],
                    );
                }

                /*
                 * get staff by vehicle or route
                 */

                if ($type == 'vehicle' || $type == 'route')
                {
                    if (!empty($session['session_id']))
                    {
                        $vehicle_query_employee = DB::table('student_vehicle_assigned_details as svad')
                            ->join('student_vehicle_assigned as sva', 'sva.student_vehicle_assigned_id', '=', 'svad.assigned_id')
                            ->join('vehicles as v', 'v.vehicle_id', '=', 'sva.vehicle_id')
                            ->join('stop_points as stop_p', 'stop_p.stop_point_id', '=', 'sva.stop_point_id')
                            ->join('routes as r', 'r.route_id', '=', 'stop_p.route_id')
                            ->join('employees as e', 'e.employee_id', '=', 'svad.employee_id')
                            ->join('departments as d', 'd.department_id', '=', 'e.department_id')
                            ->select('v.vehicle_number', 'v.vehicle_id', 'svad.assigned_to', 'sva.student_vehicle_assigned_id', 'stop_p.stop_point_id', 'stop_p.stop_point', 'stop_p.stop_fair', 'svad.student_vehicle_assigned_detail_id', 'e.employee_code', 'e.employee_id', 'e.father_name', 'e.email', 'e.full_name', 'e.contact_number', 'd.department_name'
                            )->where('sva.session_id', $session['session_id'])
                            ->where('svad.assigned_to', 2)
                            ->where('svad.assigned_status', 1);

                        if ($type == 'vehicle')
                        {
                            if (!empty($vehicle_id))
                            {
                                $vehicle_query_employee->where('sva.vehicle_id', $vehicle_id);
                            }
                            $vehicle_query_employee->orderBy('v.vehicle_id');
                        }
                        if ($type == 'route')
                        {
                            if (!empty($route_id))
                            {
                                $vehicle_query_employee->where('r.route_id', $route_id);
                            }
                            if (!empty($stop_point_id))
                            {
                                $vehicle_query_employee->where('sva.stop_point_id', $stop_point_id);
                            }
                            $vehicle_query_employee->orderBy('v.route_id');
                        }
                        $arr_employee_transport_data = $vehicle_query_employee->get();
                        foreach ($arr_employee_transport_data as $key => $employee_data)
                        {
                            $employee_data  = (array) $employee_data;
                            $arr_employee[] = array(
                                'type'           => 'Staff',
                                'id'             => $employee_data['student_vehicle_assigned_detail_id'],
                                'user_id'        => $employee_data['employee_id'],
                                'code'           => $employee_data['employee_code'],
                                'assigned_to'    => $employee_data['assigned_to'],
                                'name'           => $employee_data['full_name'],
                                'contact_number' => $employee_data['contact_number'],
                                'vehicle_number' => $employee_data['vehicle_number'],
                                'father_name'    => $employee_data['father_name'],
                                'from'           => $employee_data['department_name'],
                                'stop_point'     => $employee_data['stop_point'],
                                'stop_fair'      => $employee_data['stop_fair'],
                            );
                        }
                    }
                }
                if (!empty($arr_employee))
                {
                    $arr_transport_data = array_merge($arr_employee, $arr_student);
                }
                else
                {
                    $arr_transport_data = $arr_student;
                }
            }
            return Datatables::of($arr_transport_data)->rawColumns(['checkbox'])->make(true);
        }

        function vehicleDriver()
        {
            $return_arr = [];
            $vehicle_id = Input::get('vehicle_id');
            if (!empty($vehicle_id))
            {
                $vehicle = $this->getVehicleData($vehicle_id);
                $vehicle = isset($vehicle[0]) ? $vehicle[0] : [];
                if ($vehicle)
                {
                    $return_arr = array(
                        'status'                => 'success',
                        'message'               => 'Vehicle found successfully!',
                        'route'                 => $vehicle['route'],
                        'driver_name'           => $vehicle['driver_full_name'],
                        'driver_contact_number' => $vehicle['driver_contact_number'],
                    );
                }
                else
                {
                    $return_arr = array(
                        'status'  => 'error',
                        'message' => 'Vehicle not found!'
                    );
                }
            }

            return response()->json($return_arr);
        }

        public function printTransportReceipt($student_id)
        {
            $session = get_current_session();
            if (!empty($session['session_id']) && !empty($student_id))
            {
                $query_result                            = DB::table('student_vehicle_assigned as sva')
                    ->join('student_vehicle_assigned_details as svad', 'svad.assigned_id', '=', 'sva.student_vehicle_assigned_id')
                    ->join('vehicles as v', 'v.vehicle_id', '=', 'sva.vehicle_id')
                    ->join('vehicle_drivers as vd', 'vd.vehicle_driver_id', '=', 'v.vehicle_driver_id')
                    ->join('vehicle_driver_helpers as vdh', 'vdh.vehicle_helper_id', '=', 'v.vehicle_helper_id')
                    ->join('stop_points as stop_p', 'stop_p.stop_point_id', '=', 'sva.stop_point_id')
                    ->join('routes as r', 'r.route_id', '=', 'stop_p.route_id')
                    ->join('students as s', 's.student_id', '=', 'svad.student_id')
                    ->join('student_parents as sp', 'sp.student_parent_id', '=', 's.student_parent_id')
                    ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
                    ->join('sections as sec', 'sec.section_id', '=', 's.current_section_id')
                    ->select('v.vehicle_number', 'vehicle_color', 'v.vehicle_id', 'sva.student_vehicle_assigned_id', 'sva.pickup_time', 'sva.return_time', 'stop_p.stop_point_id', 'stop_p.stop_point', 'stop_p.stop_fair', 'svad.student_vehicle_assigned_detail_id', 'svad.join_date', 's.enrollment_number', 's.student_id', 'address_line1', 'address_line2', 'sp.father_first_name', 'sp.father_last_name', 'sp.father_middle_name', 'sp.father_contact_number', 's.first_name', 's.last_name', 's.middle_name', 'c.class_name', 'sec.section_name', 'r.route', 'vd.driver_full_name', 'vd.driver_contact_number', 'vdh.helper_full_name', 'vdh.helper_contact_number'
                        , DB::raw("CONCAT(s.first_name, ' ', s.middle_name, ' ', s.last_name) as student_name")
                        , DB::raw("CONCAT(sp.father_first_name, ' ', sp.father_middle_name, ' ', sp.father_last_name) as father_name")
                        , DB::raw("CONCAT(c.class_name, '(', sec.section_name,')') as class_section")
                    )
                    ->where('sva.session_id', $session['session_id'])
                    ->where('svad.student_id', $student_id)
                    ->where('svad.assigned_to', 1);
                $arr_transport_data['transport_receipt'] = (array) $query_result->first();
//            p($arr_transport_data);
//            return  View::make('backend.student-vehicle-assigned.transport-receipt')->with($arr_transport_data)->render();
                $arr_transport_data['school_data']       = get_school_data();
                $pdf                                     = PDF::loadView('backend.student-vehicle-assigned.transport-receipt', $arr_transport_data);
                return $pdf->stream('transport-receipt.pdf');
            }
        }

        public function sendTransportSMS()
        {
            $message_text            = Input::get('transport_sms_message');
            $arr_selected_user       = Input::get('arr_student');
            $session                 = get_current_session();
            $arr_student             = [];
            $arr_employee            = [];
            $arr_student_contact_no  = [];
            $arr_employee_contact_no = [];
            $response                = '';
            foreach ($arr_selected_user as $key => $selected_user)
            {
                if ($selected_user['assigned_to'] == 1)
                {
                    $arr_student[] = $selected_user['id'];
                }
                elseif ($selected_user['assigned_to'] == 2)
                {
                    $arr_employee[] = $selected_user['id'];
                }
            }
            if (!empty($arr_student) && !empty($session))
            {
                if ($session['session_status'] == 1)
                {
                    $query = Student::select('student_parent_id', 'student_id');
                }
                else
                {
                    $query = \App\Model\backend\StudentHistory::select('student_parent_id', 'student_id');
                }
                $student_data = $query->where('current_session_id', $session['session_id'])->whereIn('student_id', $arr_student)
                        ->with(['getParent' => function ($query)
                            {
                                $query->select(['student_parent_id', 'father_contact_number']);
                            }])->get();
                foreach ($student_data as $key => $student)
                {
                    $arr_student_contact_no[] = $student['getParent']['father_contact_number'];
                }
            }
            if (!empty($arr_employee) && !empty($session))
            {
                $employee_data = Employee::select('employee_id', 'contact_number')->where('employee_status', 1)->whereIn('employee_id', $arr_employee)->get();
                foreach ($employee_data as $key => $employee)
                {
                    $arr_employee_contact_no[] = $employee['contact_number'];
                }
            }
            $arr_contact_no = [];
            if (!empty($arr_student_contact_no) || !empty($arr_employee_contact_no))
            {
                $arr_contact_no = array_merge($arr_student_contact_no, $arr_employee_contact_no);
            }
            $all_contact_no = implode($arr_contact_no, ',');
            if (!empty($all_contact_no))
            {
                $response = send_sms($all_contact_no, $message_text);
            }
            $return_arr = array(
                'status'  => 'success',
                'message' => $response
            );
            return response()->json($return_arr);
        }

        // transport route report class wise
        function classHeader()
        {
            $return           = [];
            $arr_classes_list = DB::table('classes')->where('class_status', 1)->select('class_name', 'class_id')
                    ->orderBy('class_order', 'ASC')->get();
            foreach ($arr_classes_list as $arr_class)
            {
                $arr_class = (array) $arr_class;
                $return[]  = array('key' => $arr_class['class_id'], 'val' => $arr_class['class_name']);
            }
            return $return;
        }

        public function routeReport()
        {
            $data['arr_col'] = $this->classHeader();
            return view('backend.report.route-report-classwise')->with($data);
        }

        public function routeReportData(Request $request)
        {
            $session         = get_current_session();
            $arr_route_count = [];
            if (!empty($session))
            {
                $arr_data   = DB::table('student_vehicle_assigned_details as svad')
                    ->join('student_vehicle_assigned as sva', 'sva.student_vehicle_assigned_id', '=', 'svad.assigned_id')
                    ->join('stop_points as stop_p', 'stop_p.stop_point_id', '=', 'sva.stop_point_id')
                    ->join('routes as r', 'r.route_id', '=', 'stop_p.route_id')
                    ->join('students as s', 's.student_id', '=', 'svad.student_id')
                    ->join('classes as c', 'c.class_id', '=', 's.current_class_id')
                    ->where('sva.session_id', $session['session_id'])
                    ->where('svad.assigned_to', 1)
                    ->where('svad.assigned_status', 1)
                    ->groupBy('s.current_class_id')
//                    ->orderBy('c.class_order', 'ASC')
                    ->groupBy('stop_p.route_id')
                    ->groupBy('r.route_id')
                    ->groupBy('r.route')
                    ->select('stop_p.route_id', 'r.route', 's.current_class_id', DB::raw('count(svad.student_id) as num'))
                    ->get();
                $route_data = [];
                foreach ($arr_data as $key => $data)
                {
                    $data = (array) $data;

                    $route_data[$data['route_id']][$data['current_class_id']] = $data;
                }
                $sn = 1;
                foreach ($route_data as $route_id =>  $route)
                {
                    $total_count         = 0;
                    $route_count['route_id'] = $route_id;
                    $route_count['s_no'] = $sn;
                    $arr_class           = $this->classHeader();
                    foreach ($arr_class as $class)
                    {
                        $route_count['class' . $class['key']] = 0;
                        if (isset($route[$class['key']]))
                        {
                            $route_count['route']                 = $route[$class['key']]['route'];
                            $route_count['class' . $class['key']] = $route[$class['key']]['num'];
                            $total_count                          = $total_count + $route[$class['key']]['num'];
                        }
                    }
                    $route_count['total'] = $total_count;
                    $arr_route_count[]    = (object) $route_count;
                    $sn++;
                }
            }
            return Datatables::of($arr_route_count)->make(true);
        }

    }
    