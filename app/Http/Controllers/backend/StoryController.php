<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\Story;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;
    use App\CustomClass\ImgCompressor;

    class StoryController extends Controller
    {

        public function __construct()
        {
            
        }

        public function add(Request $request, $id = NULL)
        {
            $story    = [];
            $story_id = null;
            if (!empty($id))
            {
                $decrypted_story_id = get_decrypted_value($id, true);
                $story              = $this->getStoryData($decrypted_story_id);
                $story              = isset($story[0]) ? $story[0] : [];
                if (!$story)
                {
                    return redirect('admin/story')->withError('Story not found!');
                }
                $encrypted_story_id = get_encrypted_value($story['story_id'], true);
                $save_url           = url('admin/story/save/' . $encrypted_story_id);
                $submit_button      = 'Update';
                $story_id           = $decrypted_story_id;
            }
            else
            {
                $save_url      = url('admin/story/save');
                $submit_button = 'Save';
            }
	$session  =  db_current_session();
	$story['session_start_date'] = $session['session_start_date'];
	$story['session_end_date'] = $session['session_end_date'];
            $data                 = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'story'         => $story,
                'redirect_url'  => url('admin/story/'),
            );
//	p($data);
            return view('backend.story.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_story_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $story = Story::find($decrypted_story_id);

                if (!$story)
                {
                    return redirect('/admin/story/')->withError('Story not found!');
                }
                $success_msg = 'Story updated successfully!';
            }
            else
            {
                $story       = New Story;
                $success_msg = 'Story saved successfully!';
            }
            $session_id = Input::get('session_id');

            $all_input = [
                'story_title' => 'required|unique:stories,story_title,' . $decrypted_story_id . ',story_id,session_id,' . $session_id,
                'start_date'  => 'required',
                'end_date'    => 'required',
            ];
            if (empty($decrypted_story_id))
            {
                $all_input['story_url'] = 'required';
            }

            $validatior = Validator::make($request->all(), $all_input);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
	        
                    if(empty($id))
	        {
		$session  = db_current_session();
		$story->session_id  = $session['session_id'];
	        }
                    $story->start_date  = get_formatted_date(Input::get('start_date'), 'database');
                    $story->end_date    = get_formatted_date(Input::get('end_date'), 'database');
                    $story->story_title = Input::get('story_title');
                    if ($request->hasFile('story_url'))
                    {
                        $file            = $request->file('story_url');
                        $destinationPath = public_path() . '/uploads/story';
                        $filename        = $file->getClientOriginalName();
                        $rand_string     = quick_random();
                        $filename        = $rand_string . '-' . $filename;
                        $file->move($destinationPath, $filename);
                        // create object
                        $setting         = array(
                            'directory' => $destinationPath, // directory file compressed output
                            'file_type' => array(// file format allowed
                                'image/jpeg',
                                'image/jpg',
                                'image/png',
                                'image/gif'
                            )
                        );

                        $ImgCompressor = new ImgCompressor($setting);
                        // run('STRING original file path', 'output file type', INTEGER Compression level: from 0 (no compression) to 9);
                        $comp_file     = $ImgCompressor->run($destinationPath . '/' . $filename, 'jpg', 5); // example level = 2 same quality 80%, level = 7 same quality 30% etc
                        if ($comp_file['status'] == 'success')
                        {
                            unlink($destinationPath . '/' . $filename);
                            $story->story_url = $comp_file['data']['compressed']['name'];
                        }
                    }
                    $story->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }
                DB::commit();
            }

            return redirect('admin/story')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $story_id = Input::get('story_id');
            $story    = Story::find($story_id);
            if ($story)
            {

                DB::beginTransaction(); //Start transaction!
                try
                {
                    $story->delete();
                    $return_arr = array(
                        'status'  => 'success',
                        'message' => 'Story deleted successfully!'
                    );
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    $return_arr    = array(
                        'status'  => 'used',
                        'message' => trans('language.delete_message')
                    );
                }
                DB::commit();
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Story not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $story     = [];
            $story_id  = array();
            $offset    = Input::get('start');
            $limit     = Input::get('length');
            $arr_story = $this->getStoryData($story_id, $offset, $limit);
            foreach ($arr_story as $key => $story_data)
            {
                $story[$key] = (object) $story_data;
            }
            return Datatables::of($story)
                    ->addColumn('action', function ($story)
                    {
                        $encrypted_story_id = get_encrypted_value($story->story_id, true);
                        return '<a title="Edit" id="deletebtn1" href="' . url('admin/story/' . $encrypted_story_id) . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . '<button type = "button" title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $story->story_id . '"><i class="fa fa-trash"></i></button>';
                    })
                    ->addColumn('story_url', function ($story)
                    {
                        $return = '';
                        if (!empty($story->story_url))
                        {
                            $return = '<img src="' . url($story->story_url) . '" style="width:50px;">';
                        }
                        return $return;
                    })->rawColumns(['action', 'story_url'])
                    ->make(true);
        }

        public function getStoryData($story_id = null, $offset = null, $limit = null)
        {
            $story_return = [];
            $session_id   = null;
            $session      = get_current_session();
            if (!empty($session))
            {
                $session_id     = $session['session_id'];
                $arr_story_data = Story::where(function($query) use ($story_id)
                    {
                        if (!empty($story_id))
                        {
                            $query->where('story_id', $story_id);
                        }
                    })->where(function($query) use ($session_id)
                    {
                        if (!empty($session_id))
                        {
                            $query->where('session_id', $session_id);
                        }
                    })
//                        ->where(function($query) use ($limit, $offset)
//                    {
//                        if (!empty($limit))
//                        {
//                            $query->skip($offset);
//                            $query->take($limit);
//                        }
//                    })
                        ->get();

                if (!empty($arr_story_data))
                {
                    foreach ($arr_story_data as $key => $story_data)
                    {
                        $story          = array(
                            'story_id'    => $story_data['story_id'],
                            'session_id'  => $story_data['session_id'],
                            'start_date'  => get_formatted_date($story_data['start_date'], 'display'),
                            'end_date'    => get_formatted_date($story_data['end_date'], 'display'),
                            'story_title' => $story_data['story_title'],
                            'story_url'   => check_file_exist($story_data['story_url'], 'story'),
                        );
                        $story_return[] = $story;
                    }
                }
            }
            return $story_return;
        }

    }
    