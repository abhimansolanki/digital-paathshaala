<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\SmsTemplate;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class SmsTemplateController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index()
        {
            $data = array(
                'redirect_url' => url('admin/sms-template/add'),
            );
            return view('backend.sms-template.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {
            $sms_template    = [];
            $sms_template_id = null;
            if (!empty($id))
            {
                $decrypted_sms_template_id = get_decrypted_value($id, true);
                $sms_template              = $this->getSmsTemplateData($decrypted_sms_template_id);
                $sms_template              = isset($sms_template[0]) ? $sms_template[0] : [];
                $sms_template              = (object) $sms_template;
                if (!$sms_template)
                {
                    return redirect('admin/sms-template')->withError('Sms Template not found!');
                }
                $encrypted_sms_template_id = get_encrypted_value($sms_template->sms_template_id, true);
                $save_url                  = url('admin/sms-template/save/' . $encrypted_sms_template_id);
                $submit_button             = 'Update';
                $sms_template_id           = $decrypted_sms_template_id;
            }
            else
            {
                $save_url      = url('admin/sms-template/save');
                $submit_button = 'Save';
            }

            $data = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'sms_template'  => $sms_template,
                'redirect_url'  => url('admin/sms-template/'),
            );
            return view('backend.sms-template.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_sms_template_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $sms_template = SmsTemplate::find($decrypted_sms_template_id);

                if (!$sms_template)
                {
                    return redirect('/admin/sms-template/')->withError('Sms Template not found!');
                }
                $success_msg = 'Sms Template updated successfully!';
            }
            else
            {
                $sms_template = New SmsTemplate;
                $success_msg  = 'Sms Template saved successfully!';
            }
            $validatior = Validator::make($request->all(), [
                    'sms_name'    => 'required|unique:sms_templates,sms_name,' . $decrypted_sms_template_id . ',sms_template_id',
                    'sms_heading' => 'required',
                    'sms_body'    => 'required',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $sms_template->sms_key     = Input::get('sms_name');
                    $sms_template->sms_name    = Input::get('sms_name');
                    $sms_template->sms_heading = Input::get('sms_heading');
                    $sms_template->sms_body    = Input::get('sms_body');
                    $sms_template->sms_status  = $request->has('sms_status') ? 1 : 0;
                    $sms_template->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/sms-template')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $sms_template_id = Input::get('sms_template_id');
            $sms_template    = SmsTemplate::find($sms_template_id);
            if ($sms_template)
            {
                $sms_template->delete();
                $return_arr = array(
                    'status'  => 'success',
                    'message' => 'Sms Template deleted successfully!'
                );
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Sms Template not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $sms_template     = [];
            $arr_sms_template = $this->getSmsTemplateData();
            foreach ($arr_sms_template as $key => $sms_template_data)
            {
                $sms_template[$key] = (object) $sms_template_data;
            }
            return Datatables::of($sms_template)
                    ->addColumn('action', function ($sms_template)
                    {
                        $encrypted_sms_template_id = get_encrypted_value($sms_template->sms_template_id, true);
                        return '<a title="Edit" id="deletebtn1" href="sms-template/add/' . $encrypted_sms_template_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $sms_template->sms_template_id . '"><i class="fa fa-trash"></i></button>';
                    })
                    ->rawColumns(['action','sms_body'])->make(true);
                       
        }

        public function getSmsTemplateData($sms_template_id = array())
        {
            $sms_template_return   = [];
            $arr_sms_template_data = SmsTemplate::where(function($query) use ($sms_template_id)
                {
                    if (!empty($sms_template_id))
                    {
                        $query->where('sms_template_id', $sms_template_id);
                    }
                })->get();
            if (!empty($arr_sms_template_data))
            {
                foreach ($arr_sms_template_data as $key => $sms_template_data)
                {
                    $sms_template_return[] = array(
                        'sms_template_id' => $sms_template_data['sms_template_id'],
                        'sms_key'         => $sms_template_data['sms_key'],
                        'sms_name'        => $sms_template_data['sms_name'],
                        'sms_heading'     => $sms_template_data['sms_heading'],
                        'sms_body'        => $sms_template_data['sms_body'],
                        'sms_status'      => $sms_template_data['sms_status'],
                    );
                }
            }
            return $sms_template_return;
        }

    }
    