<?php

    namespace App\Http\Controllers\backend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\VehicleType;
    use Validator;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class VehicleTypeController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index()
        {
            $data = array(
                'redirect_url' => url('admin/vehicle-type/add'),
            );
            return view('backend.vehicle-type.index')->with($data);
        }

        public function add(Request $request, $id = NULL)
        {
            $vehicle_type    = [];
            $vehicle_type_id = null;
            if (!empty($id))
            {
                $decrypted_vehicle_type_id = get_decrypted_value($id, true);
                $vehicle_type              = $this->getVehicleTypeData($decrypted_vehicle_type_id);
                $vehicle_type              = isset($vehicle_type[0]) ? $vehicle_type[0] : [];
                $vehicle_type              = (object) $vehicle_type;
                if (!$vehicle_type)
                {
                    return redirect('admin/vehicle_type')->withError('VehicleType not found!');
                }
                $encrypted_vehicle_type_id = get_encrypted_value($vehicle_type->vehicle_type_id, true);
                $save_url                  = url('admin/vehicle-type/save/' . $encrypted_vehicle_type_id);
                $submit_button             = 'Update';
                $vehicle_type_id           = $decrypted_vehicle_type_id;
            }
            else
            {
                $save_url      = url('admin/vehicle-type/save');
                $submit_button = 'Save';
            }

            $data = array(
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'vehicle_type'  => $vehicle_type,
                'redirect_url'  => url('admin/vehicle-type/'),
            );
            return view('backend.vehicle-type.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            $decrypted_vehicle_type_id = get_decrypted_value($id, true);
            if (!empty($id))
            {
                $vehicle_type = VehicleType::find($decrypted_vehicle_type_id);

                if (!$vehicle_type)
                {
                    return redirect('/admin/vehicle-type/')->withError('VehicleType not found!');
                }
                $success_msg = 'Vehicle Type updated successfully!';
            }
            else
            {
                $vehicle_type = New VehicleType;
                $success_msg  = 'Vehicle Type saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'vehicle_type' => 'required|unique:vehicle_types,vehicle_type,' . $decrypted_vehicle_type_id . ',vehicle_type_id',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction(); //Start transaction!

                try
                {
                    $vehicle_type->vehicle_type = Input::get('vehicle_type');
                    $vehicle_type->save();
                }
                catch (\Exception $e)
                {
                    //failed logic here
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withInput()->withErrors($error_message);
                }

                DB::commit();
            }

            return redirect('admin/vehicle-type')->withSuccess($success_msg);
        }

        public function destroy(Request $request)
        {
            $vehicle_type_id = Input::get('vehicle_type_id');
            $vehicle_type    = VehicleType::find($vehicle_type_id);
            if ($vehicle_type)
            {
                $vehicle_type->delete();
                $return_arr = array(
                    'status'  => 'success',
                    'message' => 'Vehicle Type deleted successfully!'
                );
            }
            else
            {
                $return_arr = array(
                    'status'  => 'error',
                    'message' => 'Vehicle Type not found!'
                );
            }
            return response()->json($return_arr);
        }

        public function anyData()
        {
            $vehicle_type     = [];
            $arr_vehicle_type = $this->getVehicleTypeData();
            foreach ($arr_vehicle_type as $key => $vehicle_type_data)
            {
                $vehicle_type[$key] = (object) $vehicle_type_data;
            }
            return Datatables::of($vehicle_type)
                    ->addColumn('action', function ($vehicle_type)
                    {
                        $encrypted_vehicle_type_id = get_encrypted_value($vehicle_type->vehicle_type_id, true);
                        return '<a title="Edit" id="deletebtn1" href="vehicle-type/add/' . $encrypted_vehicle_type_id . '" class="btn btn-success"><i class="fa fa-edit" ></i></a>'
                            . ' <button title="Delete" id="deletebtn" class="btn btn-danger delete-button" data-id="' . $vehicle_type->vehicle_type_id . '"><i class="fa fa-trash"></i></button>';
                    })->make(true);
        }

        public function getVehicleTypeData($vehicle_type_id = array())
        {
            $vehicle_type_return   = [];
            $arr_vehicle_type_data = VehicleType::where(function($query) use ($vehicle_type_id)
                {
                    if (!empty($vehicle_type_id))
                    {
                        $query->where('vehicle_type_id', $vehicle_type_id);
                    }
                })->get();
            if (!empty($arr_vehicle_type_data))
            {
                foreach ($arr_vehicle_type_data as $key => $vehicle_type_data)
                {
                    $vehicle_type_return[] = array(
                        'vehicle_type_id' => $vehicle_type_data['vehicle_type_id'],
                        'vehicle_type'    => $vehicle_type_data['vehicle_type'],
                    );
                }
            }
            return $vehicle_type_return;
        }

    }
    