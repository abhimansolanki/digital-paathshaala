<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreatePushNotificationTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
	if (!Schema::hasTable('push_notification_devices'))
	{
	    Schema::create('push_notification_devices', function (Blueprint $table)
	    {
	        $table->increments('push_notification_device_id');
	        
	        $table->integer('push_notification_id')->unsigned();
	        $table->foreign('push_notification_id')->references('push_notification_id')->on('push_notifications')->onDelete('cascade');
	        
	        $table->string('device_id');
	        $table->tinyInteger('visited')->default(0)->comment = "1:Yes, 0:No";
            $table->softDeletes();
	        $table->timestamps();
	    });
	}
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
	Schema::dropIfExists('push_notification_devices');
        }

    }
    