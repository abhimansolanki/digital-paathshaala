<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSchoolGalleriesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
	if (!Schema::hasTable('school_galleries'))
	{
	    Schema::create('school_galleries', function (Blueprint $table)
	    {
	        $table->increments('school_gallery_id');
	        
	        $table->integer('album_id')->unsigned();
	        $table->foreign('album_id')->references('album_id')->on('school_gallery_albums')->onDelete('cascade');
	        
//	        $table->integer('album_id')->unsigned();
//	        $table->foreign('album_id')->referances('album_id')->on('school_gallery_albums');
//		    ->onDelete('cascade');
	        $table->text('image_url');
	        $table->tinyInteger('type')->default(1)->comment('1:Photos, 2: Videos');
            $table->softDeletes();
	        $table->timestamps();
	    });
	}
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
	Schema::dropIfExists('school_galleries');
        }

    }
    