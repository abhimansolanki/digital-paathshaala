<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentMarksTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'student_marks';
        protected $primaryKey = 'student_mark_id';

        public function up()
        {
            if (!Schema::hasTable('student_marks'))
            {
                Schema::create('student_marks', function (Blueprint $table)
                {
                    $table->increments('student_mark_id');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                    $table->integer('class_id')->unsigned();
                    $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

                    $table->integer('exam_id')->unsigned();
                    $table->foreign('exam_id')->references('exam_id')->on('create_exams')->onDelete('cascade');

                    $table->integer('section_id')->unsigned();
                    $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');

                    $table->tinyInteger('result_in')->comment               = '1:Grade, 2:Marks 3:Both';
                    $table->tinyInteger('publish_status')->comment          = '1:Yes, 0:No';
                    $table->tinyInteger('mark_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('student_marks');
        }

    }
    