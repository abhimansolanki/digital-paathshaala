<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateFeesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'fees';
        protected $primaryKey = 'fee_id';

        public function up()
        {
            if (!Schema::hasTable('fees'))
            {
                Schema::create('fees', function (Blueprint $table)
                {
                    $table->increments('fee_id');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');
                    
	                $table->integer('class_id')->unsigned()->nullable();
                    $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

                    $table->integer('fee_type_id')->unsigned();
                    $table->foreign('fee_type_id')->references('fee_type_id')->on('fee_types')->onDelete('cascade');

                    $table->integer('fee_circular_id')->unsigned();
                    $table->foreign('fee_circular_id')->references('fee_circular_id')->on('fee_circulars')->onDelete('cascade');

                    $table->string('apply_to')->comment                         = 'All Student, New student';
                    $table->date('fee_date')->nullable()->comment               = 'fee start date';
                    $table->decimal('fine_amount', 10, 2)->nullable()->comment  = 'fine amount';
                    $table->tinyInteger('fine_type')->nullable()->comment       = '1: Percentage, 0: Per day';
                    $table->tinyInteger('student_type_id')->nullable()->comment = '1: Free, 2: Paid';
                    $table->tinyInteger('fee_status')->default(1)->comment      = '1:Active, 0:Blocked'; // simple status for fee row like active or inactive
                    $table->tinyInteger('is_fixed')->default(0)->comment        = '1:Yes, 0:No';
                    $table->integer('total_fee')->default(0)->comment        = 'total fee of that fee type';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }

            DB::table('fees')->insert(
                array(
                    'session_id'      => 1,
                    'fee_type_id'     => 1, // app access fee
                    'fee_circular_id' => 1,
                    'is_fixed'        => 1,
                    'student_type_id' => 2,
                    'apply_to'        => 'all_student',
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s'),
                )
            );

            DB::table('fees')->insert(
                array(
                    'session_id'      => 1,
                    'fee_type_id'     => 2, // previous due fee
                    'fee_circular_id' => 1,
                    'is_fixed'        => 1,
                    'apply_to'        => 'all_student',
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s'),
                )
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('fees');
        }

    }
    