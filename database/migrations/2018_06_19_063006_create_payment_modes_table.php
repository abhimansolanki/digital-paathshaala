<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreatePaymentModesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'payment_modes';
        protected $primaryKey = 'payment_mode_id';

        public function up()
        {
            if (!Schema::hasTable('payment_modes'))
            {
                Schema::create('payment_modes', function (Blueprint $table)
                {
                    $table->increments('payment_mode_id');
                    $table->string('payment_mode', 50);
                    $table->tinyInteger('payment_mode_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
            
             // Insert some stuff
            $payment_mode = \Config::get('custom.payment_mode');
            if (!empty($payment_mode))
            {
                foreach ($payment_mode as $key => $value)
                {
                    DB::table('payment_modes')->insert(
                        array(
                            'payment_mode'        => $key,
                            'payment_mode_status' => 1,
                            'created_at'       => date('Y-m-d H:i:s'),
                            'updated_at'       => date('Y-m-d H:i:s'),
                        )
                    );
                }
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('payment_modes');
        }

    }
    