<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('admin_users')) {
            Schema::create('admin_users', function (Blueprint $table) {
                $table->increments('admin_user_id');

                $table->integer('user_type_id')->unsigned();
                $table->foreign('user_type_id')->references('user_type_id')->on('user_types')->onDelete('cascade');

                $table->integer('user_role_id')->unsigned();
                $table->foreign('user_role_id')->references('user_role_id')->on('user_roles')->onDelete('cascade');

                $table->string('email', 50)->nullable();
                $table->string('user_name', 50)->nullable();
                $table->string('password');
                $table->string('mobile_number', 25)->nullable();
                $table->rememberToken();
                $table->softDeletes();
                $table->timestamps();
            });
        }
            // Insert some stuff
        DB::table('admin_users')->insert(
            array(
                'admin_user_id' => 1,
                'user_type_id' => 1,
                'user_role_id' => 1,
                'user_name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => '$2y$10$NvTUZYU.QmXGHo2aljVEcuv7rFgd/KDs2pA4pjbQMFnEQ6TGaa0bm',
                'remember_token' => 'oJJmPSevfhpZdKdRf2iSfnKLM8boFxrkWkYP1GjANsSkmcU6CMBNHkdMIfcx',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_users');
    }

}
    