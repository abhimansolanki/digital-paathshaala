<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateEmployeeDocumentsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            if (!Schema::hasTable('employee_documents'))
            {
                Schema::create('employee_documents', function (Blueprint $table)
                {
                    $table->increments('employee_document_id');
                    
                    $table->integer('employee_id')->unsigned();
                    $table->foreign('employee_id')->references('employee_id')->on('employees')->onDelete('cascade');
                    
                    $table->integer('document_type_id')->unsigned();
                    $table->foreign('document_type_id')->references('document_type_id')->on('document_types')->onDelete('cascade');
                    
                    $table->text('document_url');
	        $table->tinyInteger('status')->default(1)->comment = "1:Active, 0:Blocked";
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('employee_documents');
        }

    }
    
