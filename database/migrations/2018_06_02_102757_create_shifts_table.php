<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateShiftsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'shifts';
        protected $primaryKey = 'shift_id';

        public function up()
        {
            if (!Schema::hasTable('shifts'))
            {
                Schema::create('shifts', function (Blueprint $table)
                {
                    $table->increments('shift_id');
                    $table->string('shift_name', 255);
                    $table->date('effective_date');
                    $table->text('description')->nullable();
                    $table->string('login_time', 10)->nullable();
                    $table->string('logout_time', 10)->nullable();
                    $table->tinyInteger('shift_status')->default(1)->nullable()->comment = '0: Blocked,1: Active';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('shifts');
        }

    }
    