<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSchoolGalleryAlbumsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
	if (!Schema::hasTable('school_gallery_albums'))
	{
	    Schema::create('school_gallery_albums', function (Blueprint $table)
	    {
	        $table->increments('album_id');
	        $table->text('album_title');
	        $table->text('description');
            $table->softDeletes();
	        $table->timestamps();
	    });
	}
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
	Schema::dropIfExists('school_gallery_albums');
        }

    }
    