<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEClassRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_class_rooms', function (Blueprint $table) {
            $table->increments('e_class_room_id');

            $table->integer('session_id')->unsigned();
            $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

            $table->integer('class_id')->unsigned();
            $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

            $table->integer('subject_id')->unsigned();
            $table->foreign('subject_id')->references('subject_id')->on('subjects')->onDelete('cascade');

            $table->integer('section_id')->unsigned();
            $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');

            $table->string('teacher_name')->nullable();
            $table->string('meeting_link')->nullable();

            $table->time('meeting_start_time')->nullable();
            $table->time('meeting_end_time')->nullable();

            $table->string('meeting_day')->nullable();
            $table->tinyInteger('status')->default('1')->comment('1-active,0-inactive');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('e_class_rooms');
    }
}
