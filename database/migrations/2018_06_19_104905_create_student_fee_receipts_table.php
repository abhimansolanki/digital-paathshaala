<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentFeeReceiptsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'student_fee_receipts';
        protected $primaryKey = 'student_fee_receipt_id';

        public function up()
        {
            if (!Schema::hasTable('student_fee_receipts'))
            {
                Schema::create('student_fee_receipts', function (Blueprint $table)
                {
                    $table->increments('student_fee_receipt_id');

                    $table->integer('student_id')->unsigned();
                    $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                    $table->integer('class_id')->unsigned();
                    $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

                    $table->integer('payment_mode_id')->unsigned();
                    $table->foreign('payment_mode_id')->references('payment_mode_id')->on('payment_modes')->onDelete('cascade');

                    $table->integer('bank_id')->nullable()->default(0);
                      
                    $table->string('receipt_number')->comment          = 'Student Fee deposit receipt';
                    $table->date('receipt_date')->comment              = 'fee receipt date';
                    $table->decimal('total_amount', 10, 2)->comment    = 'total receipt amount';
                    $table->decimal('discount_amount', 10, 2)->nullable()->comment = 'discount receipt amount';
                    $table->decimal('cheque_bounce_charges_received', 10, 2)->nullable();
                    $table->decimal('fine_amount', 10, 2)->nullable()->comment     = 'fine receipt amount';
                    $table->decimal('net_amount', 10, 2)->comment      = 'net receipt amount';

                    $table->string('cheque_number')->nullable()->comment  = 'if mode = cheque';
                    $table->date('cheque_date')->nullable()->comment      = 'if mode = cheque';
                    $table->date('transaction_date')->nullable()->comment = 'if mode = online or paytm';
                    $table->string('transaction_id')->nullable()->comment = 'if mode = online or paytm';

                    $table->tinyInteger('is_receipt_print')->default(0)->comment      = '1 : Yes, 0: No';
                    $table->tinyInteger('is_sms_send')->default(0)->comment               = '1 : Yes, 0: No';
                    $table->tinyInteger('fee_status')->default(1)->comment               = '1 : Yes, 0: No';
                    $table->tinyInteger('auto_adjusted')->default(0)->nullable()->comment = '1:Paid,2:Reverted,3:Refunded';
                    //receipt transaction type
                    $table->tinyInteger('transaction_type')->default(1)->nullable()->comment = '1:Credit,2:Debit';

                    // Fee return details
                    $table->string('return_cheque_number')->nullable()->comment  = 'if mode = cheque';
                    $table->date('return_cheque_date')->nullable()->comment      = 'if mode = cheque';
                    $table->string('return_transaction_id')->nullable()->comment = 'if mode = online or paytm';
                    $table->date('return_transaction_date')->nullable()->comment = 'if mode = online or paytm';

                    // Fee cancel details
                    $table->date('cancel_fee_date')->nullable()->comment  = 'if fee_status = 2';
                    $table->string('cancel_fee_reason')->nullable()->comment      = 'if fee_status = 2';

                    // Fee Cheque Bounce Details
                    $table->date('cheque_bounce_date')->nullable()->comment  = 'if fee_status = 3';
                    $table->decimal('cheque_bounce_charges', 10, 2)->nullable()->comment      = 'if fee_status = 3';
                    $table->tinyInteger('cheque_bounce_charges_status')->nullable()->comment = "1 unpaid, 2 paid";
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('student_fee_receipts');
        }

    }
    