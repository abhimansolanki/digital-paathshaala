<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('users')){
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
        }
        
//        DB::table('users')->insert(
//                array(
//                    'id'   => 1,
//                    'name'           => 'user',
//                    'email'          => 'user@gmail.com',
//                    'password'       => '$2y$10$NvTUZYU.QmXGHo2aljVEcuv7rFgd/KDs2pA4pjbQMFnEQ6TGaa0bm',
//                    'created_at'     => date('Y-m-d H:i:s'),
//                    'updated_at'     => date('Y-m-d H:i:s'),
//                )
//            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
