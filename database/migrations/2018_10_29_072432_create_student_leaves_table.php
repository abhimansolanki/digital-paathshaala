<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentLeavesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'student_leaves';
        protected $primaryKey = 'student_leave_id';

        public function up()
        {
            if (!Schema::hasTable('student_leaves'))
            {
                Schema::create('student_leaves', function (Blueprint $table)
                {
                    $table->increments('student_leave_id');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                    $table->integer('class_id')->unsigned();
                    $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

                    $table->integer('student_id')->unsigned();
                    $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');

                    $table->unsignedInteger('section_id')->nullable();
                    $table->date('leave_start_date');
                    $table->date('leave_end_date');
                    $table->tinyInteger('leave_day');
                    $table->tinyInteger('leave_type')->default(1)->comment   = "1:Full Day, 2:Half Day";
                    $table->longText('reason')->nullable();
                    $table->tinyInteger('leave_status')->default(1)->comment = "1:Pending, 2:Confirmed, 3:Rejected";
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('student_leaves');
        }

    }
    