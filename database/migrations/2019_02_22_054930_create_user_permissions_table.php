<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_permissions'))
        {
            Schema::create('user_permissions', function (Blueprint $table) {
                $table->increments('user_permission_id');
                $table->integer('user_role_id')->unsigned();
                $table->foreign('user_role_id')->references('user_role_id')->on('user_roles')->onDelete('cascade');
                $table->text('user_permission_modules')->nullable()->comment('Modules id store here');
                $table->tinyInteger('user_perission_status')->default('1')->comment('1-Active, 0-Inactive');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_permissions');
    }
}
