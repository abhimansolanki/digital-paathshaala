<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentEnquiresTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'student_enquires';
        protected $primaryKey = 'student_enquire_id';

        public function up()
        {
            Schema::create('student_enquires', function (Blueprint $table)
            {
                $table->increments('student_enquire_id');

                $table->integer('caste_category_id')->unsigned();
                $table->foreign('caste_category_id')->references('caste_category_id')->on('caste_categories')->onDelete('cascade');

                $table->integer('class_id')->unsigned();
                $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');
               
                $table->integer('session_id')->unsigned();
                $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                $table->string('form_number', 150)->unique();
                $table->date('enquire_date');
                $table->decimal('form_fee', 10, 2);
                $table->string('first_name', 100);
                $table->string('middle_name', 100)->nullable();
                $table->string('last_name', 100)->nullable();
                $table->date('dob')->nullable();
                $table->tinyInteger('gender')->comment             = '0:Girl,1:Boy';
                $table->string('birth_place', 20)->nullable();
                $table->string('contact_number', 20)->nullable();
                $table->string('blood_group', 20)->nullable();
                $table->string('body_sign', 20)->nullable();
                $table->string('email', 50)->nullable();
                $table->string('aadhaar_number', 25)->nullable();
                $table->text('remark')->nullable();
                $table->text('address_line1');
                $table->text('address_line2')->nullable();
                $table->text('percentage',20)->nullable();
                $table->string('previous_class_name', 10)->nullable();
                $table->string('previous_school_name', 50)->nullable();
                $table->decimal('father_income', 20, 2)->nullable();
                $table->string('father_email', 50)->nullable();
                $table->string('father_aadhaar_number', 50)->nullable();
                $table->string('father_first_name', 255);
                $table->string('father_middle_name', 255)->nullable();
                $table->string('father_last_name', 255)->nullable();
                $table->string('father_contact_number', 50);
                $table->string('father_occupation', 50)->nullable();
                $table->string('mother_email', 50)->nullable();
                $table->string('mother_aadhaar_number', 50)->nullable();
                $table->string('mother_first_name', 100);
                $table->string('mother_middle_name', 100)->nullable();
                $table->string('mother_last_name', 100)->nullable();
                $table->string('mother_contact_number', 50)->nullable();
                $table->string('mother_occupation', 50)->nullable();
                $table->string('guardian_email', 50)->nullable();
                $table->string('guardian_aadhaar_number', 50)->nullable();
                $table->string('guardian_first_name', 100)->nullable();
                $table->string('guardian_middle_name', 100)->nullable();
                $table->string('guardian_last_name', 100)->nullable();
                $table->string('guardian_contact_number', 50)->nullable();
                $table->string('guardian_occupation', 50)->nullable();
                $table->string('guardian_relation', 50)->nullable();
                $table->tinyInteger('status')->default(0)->comment = '1:Admission, 0:Enquire';
                $table->tinyInteger('form_fee_paid')->default(0)->comment = '1:Yes, 0:No';
                $table->softDeletes();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('student_enquires');
        }

    }
    