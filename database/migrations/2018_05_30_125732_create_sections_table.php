<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'sections';
    protected $primaryKey = 'section_id';

    public function up()
    {
        if (!Schema::hasTable('sections'))
        {
            Schema::create('sections', function (Blueprint $table)
            {
                $table->increments('section_id');
                $table->string('section_name', 20);
                $table->tinyInteger('is_fixed')->default(0)->comment = '1:Yes, 0:No';
                $table->tinyInteger('section_status')->default(1)->nullable()->comment = '1:Active, 0:Blocked';
                $table->softDeletes();
                $table->timestamps();
            });
        }
         DB::table('sections')->insert(
                array(
                    'section_name'   => 'A',
                    'section_status' => 1,
                    'is_fixed' => 1,
                    'created_at'   => date('Y-m-d H:i:s'),
                    'updated_at'   => date('Y-m-d H:i:s'),
                )
            );
        }

    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sections');
    }
}
