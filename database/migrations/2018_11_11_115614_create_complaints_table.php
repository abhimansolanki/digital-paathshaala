<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateComplaintsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'complaints';
        protected $primaryKey = 'complaint_id';

        public function up()
        {
            if (!Schema::hasTable('complaints'))
            {
                Schema::create('complaints', function (Blueprint $table)
                {
                    $table->increments('complaint_id');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');


                    $table->integer('student_id')->unsigned();
                    $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');

                    $table->text('heading');
                    $table->longText('description');
                    $table->tinyInteger('status')->default(1)->comment = "1:Pending, 2:Resolved, 3:Rejected";
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('complaints');
        }

    }
    