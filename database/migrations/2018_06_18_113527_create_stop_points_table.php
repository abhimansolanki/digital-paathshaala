<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStopPointsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'stop_points';
        protected $primaryKey = 'stop_point_id';

        public function up()
        {
            if (!Schema::hasTable('stop_points'))
            {
                Schema::create('stop_points', function (Blueprint $table)
                {
                    $table->increments('stop_point_id');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');
                    
                    $table->integer('route_id')->unsigned();
                    $table->foreign('route_id')->references('route_id')->on('routes')->onDelete('cascade');

                    $table->string('stop_point', 255);
                    $table->decimal('stop_fair', 10, 2);
                    $table->tinyInteger('main_stop_point')->default(0)->comment = '1:Yes, 0:No';
                    $table->tinyInteger('stop_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('stop_points');
        }

    }
    