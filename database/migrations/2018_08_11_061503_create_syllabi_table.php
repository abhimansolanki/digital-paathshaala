<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSyllabiTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('syllabi', function (Blueprint $table)
            {
                $table->increments('syllabus_id');

                $table->integer('session_id')->unsigned();
                $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                $table->integer('class_id')->unsigned();
                $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');
                
	    $table->integer('section_id')->unsigned();
                $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');
	    
	    $table->integer('exam_id')->unsigned()->nullable()->comment = "will not be empty in case of syllabus type is exam";
                $table->foreign('exam_id')->references('exam_id')->on('create_exams')->onDelete('cascade');

                $table->integer('month')->default(0)->nullable()->comment   = "will not be empty in case of syllabus type is monthly";

                $table->tinyInteger('syllabus_type')->comment = '1:Exam, 2:Monthly';

                $table->integer('subject_id')->unsigned();
                $table->foreign('subject_id')->references('subject_id')->on('subjects')->onDelete('cascade');

                $table->longText('syllabus_content')->comment               = "Subject content for syllabus";
                $table->tinyInteger('syllabus_status')->default(1)->comment = '1:Active, 0:Blocked';
                $table->softDeletes();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('syllabi');
        }

    }
    