<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSmsTemplatesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'sms_templates';
        protected $primaryKey = 'sms_template_id';

        public function up()
        {
            if (!Schema::hasTable('sms_templates'))
            {
                Schema::create('sms_templates', function (Blueprint $table)
                {
                    $table->increments('sms_template_id');
                    $table->string('sms_key');
                    $table->string('sms_name');
                    $table->text('sms_heading');
                    $table->text('sms_body');
                    $table->tinyInteger('sms_status')->default(0)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('sms_templates');
        }

    }
    