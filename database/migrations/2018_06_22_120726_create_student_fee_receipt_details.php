<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentFeeReceiptDetails extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'student_fee_receipt_details';
        protected $primaryKey = 'student_fee_receipt_detail_id';

        public function up()
        {
            if (!Schema::hasTable('student_fee_receipt_details'))
            {
                Schema::create('student_fee_receipt_details', function (Blueprint $table)
                {
                    $table->increments('student_fee_receipt_detail_id');

                    $table->integer('student_fee_receipt_id')->unsigned();
                    $table->foreign('student_fee_receipt_id')->references('student_fee_receipt_id')->on('student_fee_receipts')->onDelete('cascade');

                    $table->integer('fee_type_id')->unsigned();
                    $table->foreign('fee_type_id')->references('fee_type_id')->on('fee_types')->onDelete('cascade');

                    $table->integer('fee_circular_id')->unsigned();
                    $table->foreign('fee_circular_id')->references('fee_circular_id')->on('fee_circulars')->onDelete('cascade');

                    $table->integer('fee_id')->unsigned();
                    $table->foreign('fee_id')->references('fee_id')->on('fees')->onDelete('cascade');

                    $table->integer('fee_detail_id')->unsigned();
                    $table->foreign('fee_detail_id')->references('fee_detail_id')->on('fee_details')->onDelete('cascade');

                    $table->string('fee_circular_name', 50);
                    $table->decimal('installment_amount', 10, 2);
                    $table->decimal('installment_paid_amount', 10, 2);
                    $table->decimal('fine_amount', 10, 2)->nullable()->comment     = 'fine receipt amount';
                    $table->tinyInteger('installment_payment_status')->default(1)->comment = '1 : equal, 0: less, 2: greater';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('student_fee_receipt_details');
        }

    }
    