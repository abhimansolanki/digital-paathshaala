<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateClassSubjectsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'class_subjects';
        protected $primaryKey = 'class_subject_id';

        public function up()
        {
            if (!Schema::hasTable('class_subjects'))
            {
                Schema::create('class_subjects', function (Blueprint $table)
                {
                    $table->increments('class_subject_id');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');
                    
                    $table->integer('class_id')->unsigned();
                    $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');
                    
                    $table->integer('section_id')->unsigned();
                    $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');

                    

                    $table->integer('subject_group_id')->unsigned();
                    $table->foreign('subject_group_id')->references('subject_group_id')->on('subject_groups')->onDelete('cascade');

                    $table->integer('subject_id')->unsigned();
                    $table->foreign('subject_id')->references('subject_id')->on('subjects')->onDelete('cascade');
                    
                    $table->string('unassigned_student_id')->nullable();
                    $table->tinyInteger('include_in_card')->default(0)->comment = '1 : Yes, 0:No';
                    $table->tinyInteger('include_in_rank')->default(0)->comment = '1 : Yes, 0:No';
                    $table->integer('class_subject_order')->default(1)->comment = '1 : Yes, 0:No';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('class_class_subjects');
        }

    }
    