<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateExamCategoriesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'exam_categories';
        protected $primaryKey = 'exam_category_id';

        public function up()
        {
            if (!Schema::hasTable('exam_categories'))
            {
                Schema::create('exam_categories', function (Blueprint $table)
                {
                    $table->increments('exam_category_id');
                    $table->string('exam_category', 10);
                    $table->string('exam_category_alias', 25)->nullable();
                    $table->integer('exam_category_order')->nullable();
                    $table->tinyInteger('exam_category_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('exam_categories');
        }

    }
    