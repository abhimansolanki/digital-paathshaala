<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSubjectsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'subjects';
        protected $primaryKey = 'subject_id';

        public function up()
        {
            if (!Schema::hasTable('subjects'))
            {
                Schema::create('subjects', function (Blueprint $table)
                {
                    $table->increments('subject_id');
                    $table->tinyInteger('root_id')->default(0)->comment = '0:Subject, 0:Sub-Subject';
                    $table->string('subject_name', 50);
                    $table->string('subject_code', 25);
                    $table->string('subject_order', 255);
                    $table->decimal('subject_fee', 10,2)->nullable();
                    $table->tinyInteger('exclude_cgpa')->default(0)->comment   = '1:Yes, 0:No';
                    $table->tinyInteger('subject_status')->default(1)->comment = '1 : Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('subjects');
        }

    }
    