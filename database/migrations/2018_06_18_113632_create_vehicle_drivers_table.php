<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateVehicleDriversTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'vehicle_drivers';
        protected $primaryKey = 'vehicle_driver_id';

        public function up()
        {

            if (!Schema::hasTable('vehicle_drivers'))
            {
                Schema::create('vehicle_drivers', function (Blueprint $table)
                {
                    $table->increments('vehicle_driver_id');
                    $table->string('driver_full_name');
                    $table->string('driver_email_address', 50)->nullable();
                    $table->text('driver_address');
                    $table->string('driver_contact_number');
                    $table->string('driver_aadhaar_number');
                    $table->text('driver_aadhaar')->nullable();
                    $table->string('driver_license_number');
                    $table->text('driver_license')->nullable();
                    $table->tinyInteger('vehicle_driver_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('vehicle_drivers');
        }

    }
    