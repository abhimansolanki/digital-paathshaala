<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateClassSectionsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'class_sections';
        protected $primaryKey = 'class_section_id';

        public function up()
        {
            if (!Schema::hasTable('class_sections'))
            {
                Schema::create('class_sections', function (Blueprint $table)
                {
                    $table->increments('class_section_id');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                    $table->integer('class_id')->unsigned();
                    $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

                    $table->integer('section_id')->unsigned();
                    $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');

                    $table->integer('seats')->default(0)->nullable();
                    $table->integer('strength')->default(0)->nullable();
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
	
	DB::table('class_sections')->insert(
                array(
                    'session_id'  => 1,
                    'class_id'  => 1,
                    'section_id' => 1,
                    'created_at'   => date('Y-m-d H:i:s'),
                    'updated_at'   => date('Y-m-d H:i:s'),
                )
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('class_sectionss');
        }

    }
    