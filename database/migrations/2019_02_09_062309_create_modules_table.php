<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table = 'modules';
    protected $primaryKey = 'module_id';

    public function up()
    {
        if (!Schema::hasTable('modules')) {
            Schema::create('modules', function (Blueprint $table) {
                $table->increments('module_id');
                $table->string('key');
                $table->string('module', 50);
                $table->string('module_action', 50);
                $table->text('module_url');
                $table->tinyInteger('module_status')->default(1)->nullable()->comment = '0: Blocked,1: Active';
                $table->softDeletes();
                $table->timestamps();
            });
        }

        // Insert some stuff
        $modules = \Config::get('custom.modules');
        if (!empty($modules)) {
            foreach ($modules as $key => $value) {
                DB::table('modules')->insert(
                    array(
                        'key' => $value['key'],
                        'module' => $value['name'],
                        'module_action' => $value['action'],
                        'module_url' => $value['url'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    )
                );
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
