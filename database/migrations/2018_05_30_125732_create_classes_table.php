<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateClassesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'classes';
        protected $primaryKey = 'class_id';

        public function up()
        {
            if (!Schema::hasTable('classes'))
            {
                Schema::create('classes', function (Blueprint $table)
                {
                    $table->increments('class_id');
                    $table->string('class_name', 20);
                    $table->tinyInteger('class_order')->nullable()->comment              = 'Set class order';
                    $table->tinyInteger('class_status')->default(1)->nullable()->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }

            DB::table('classes')->insert(
                array(
                    'class_name'   => 'Play Group',
                    'class_order'  => 1,
                    'class_status' => 1,
                    'created_at'   => date('Y-m-d H:i:s'),
                    'updated_at'   => date('Y-m-d H:i:s'),
                )
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('classes');
        }

    }
    