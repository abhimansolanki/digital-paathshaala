<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStoriesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'stories';
        protected $primaryKey = 'story_id';

        public function up()
        {
            if (!Schema::hasTable('stories'))
            {
                Schema::create('stories', function (Blueprint $table)
                {
                    $table->increments('story_id');
                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');
                    $table->text('story_title');
                    $table->date('start_date');
                    $table->date('end_date');
                    $table->text('story_url');
                    $table->tinyInteger('story_status')->default(1)->comment = "1:Active, 0:Blocked";
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('stories');
        }

    }
    