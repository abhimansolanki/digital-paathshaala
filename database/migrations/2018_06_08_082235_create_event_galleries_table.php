<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateEventGalleriesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'event_galleries';
        protected $primaryKey = 'event_gallery_id';

        public function up()
        {
            if (!Schema::hasTable('event_galleries'))
            {
                Schema::create('event_galleries', function (Blueprint $table)
                {
                    $table->increments('event_gallery_id');
                    
                    $table->integer('event_id')->unsigned();
                    $table->foreign('event_id')->references('event_id')->on('events')->onDelete('cascade');
                    
                    $table->string('title')->nullable();
                    $table->string('name');
                    $table->Integer('order')->nullable();
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('event_images');
        }

    }
    