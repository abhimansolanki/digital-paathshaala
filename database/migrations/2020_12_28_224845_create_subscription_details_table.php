<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('student_parent_id')->unsigned();
            $table->foreign('student_parent_id')->references('student_parent_id')->on('student_parents')->onDelete('cascade');

            $table->tinyInteger('payment_status')->default(0)->comment('0-not done, 1-done');
            $table->string('order_id')->nullable();
            $table->string('amount')->nullable();
            $table->dateTime('payment_date')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_details');
    }
}
