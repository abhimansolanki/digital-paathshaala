<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_parent_id')->unsigned();
            $table->foreign('student_parent_id')->references('student_parent_id')->on('student_parents')->onDelete('cascade');
            $table->tinyInteger('fee_type')->default(1)->comment('1-Fees, 2-Subscription');

            $table->string('order_id');
            $table->string('receipt_id');
            $table->tinyInteger('payment_status')->default(0)->comment('0-not done, 1-done, 2-failed');
            $table->longText('payment_receipt')->nullable();
            $table->string('amount');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
