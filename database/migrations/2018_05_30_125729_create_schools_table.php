<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSchoolsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table = 'schools';
        protected $primaryKey = 'school_id';

        // protected $fillable =   [ 'email','verification_token'];

        public function up()
        {
	if (!Schema::hasTable('schools'))
	{
	    Schema::create('schools', function (Blueprint $table)
	    {
	        $table->increments('school_id');

	        $table->integer('admin_user_id')->unsigned();
	        $table->foreign('admin_user_id')->references('admin_user_id')->on('admin_users')->onDelete('cascade');
	        $table->string('school_name', 255);
	        $table->string('alias', 255)->nullable();
	        $table->text('address');
	        $table->string('contact_number', 20)->nullable();
	        $table->string('mobile_number', 20)->nullable();
	        $table->string('fax_number', 20)->nullable();
	        $table->string('tin_number', 20)->nullable();
	        $table->string('email', 50)->nullable();
	        $table->string('pan_number', 15)->nullable();
	        $table->string('form_fee', 15);
	        $table->text('website_url')->nullable();
	        $table->year('started_year');
	        $table->string('dies_code', 50)->nullable();
	        $table->string('effilated_number', 255)->nullable();
	        $table->string('effilated_to', 255)->nullable();
	        $table->string('registration_number', 50)->nullable();
	        $table->string('principal_name')->nullable();
	        $table->string('principal_contact_no')->nullable();
	        $table->string('summer_start_time')->nullable();
	        $table->string('summer_end_time')->nullable();
	        $table->string('winter_start_time')->nullable();
	        $table->string('winter_end_time')->nullable();
	        $table->string('psp_code')->nullable();
	        $table->string('pin_code')->nullable();
            $table->text('school_logo')->nullable();
            $table->text('school_logo_color')->nullable();
	        $table->text('transport_fee_month')->nullable();
	        $table->tinyInteger('status')->default(1);
            $table->text('whatsapp_phone')->nullable();
            $table->text('facebook_url')->nullable();
            $table->text('instagram_url')->nullable();
            $table->text('twitter_url')->nullable();
            $table->text('youtube_url')->nullable();
            $table->text('linkedin_url')->nullable();
            $table->string('app_version')->nullable();
            $table->tinyInteger('mandatory_update')->default(1);
            $table->text('payment_gateway_app_id')->nullable();
            $table->text('payment_gateway_secret_key')->nullable();
            $table->softDeletes();
            $table->timestamps();
	    });
	}
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
	Schema::dropIfExists('schools');
        }

    }
    