<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentPromotesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
	if (!Schema::hasTable('student_promotes'))
	{
	    Schema::create('student_promotes', function (Blueprint $table)
	    {
	        $table->increments('student_promote_id');

	        $table->integer('session_id')->unsigned();
	        $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

	        $table->integer('class_id')->unsigned();
	        $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

	        $table->integer('section_id')->unsigned();
	        $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');

	        $table->tinyInteger('promote_status')->default(0)->nullable()->comment = '1:YES, 0:NO';
            $table->softDeletes();
	        $table->timestamps();
	    });
	}
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
	Schema::dropIfExists('student_promotes');
        }

    }
    