<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateExamRollNumbersTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'exam_roll_numbers';
        protected $primaryKey = 'exam_roll_number_id';

        public function up()
        {
            if (!Schema::hasTable('exam_roll_numbers'))
            {
                Schema::create('exam_roll_numbers', function (Blueprint $table)
                {
                    $table->increments('exam_roll_number_id');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                    $table->integer('class_id')->unsigned();
                    $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

                    $table->integer('student_id')->unsigned();
                    $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');

                    $table->string('prefix');
                    $table->integer('roll_number')->unsigned();
                    $table->tinyInteger('status')->default(1)->comment = "1:Active, 0:Blocked";
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('exam_roll_numbers');
        }

    }
    