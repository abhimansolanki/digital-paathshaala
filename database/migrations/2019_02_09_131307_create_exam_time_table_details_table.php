<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateExamTimeTableDetailsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            if (!Schema::hasTable('exam_time_table_details')) {
                Schema::create('exam_time_table_details', function (Blueprint $table)
                {
                    $table->increments('detail_id');

                    $table->integer('exam_time_table_id')->unsigned();
                    $table->foreign('exam_time_table_id')->references('exam_time_table_id')->on('exam_time_tables')->onDelete('cascade');

                    $table->integer('class_id')->unsigned();
                    $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

                    $table->integer('section_id')->unsigned();
                    $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');

                    // $table->integer('subject_id')->unsigned();
                    // $table->foreign('subject_id')->references('subject_id')->on('subjects')->onDelete('cascade');

                    // $table->integer('exam_type_id')->unsigned()->comment('Like: Written, Practical');
                    // $table->foreign('exam_type_id')->references('exam_type_id')->on('exam_types')->onDelete('cascade');

                    // $table->integer('employee_id')->unsigned()->nullable()->comment('Teacher ID');
                    // $table->foreign('employee_id')->references('employee_id')->on('employees')->onDelete('cascade');

                    // $table->date('exam_date');
                    // $table->string('start_time', 50);
                    // $table->string('end_time', 50);
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('exam_time_table_details');
        }

    }
    