<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateVehicleProvidersTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'vehicle_providers';
        protected $primaryKey = 'vehicle_provider_id';

        public function up()
        {
            if (!Schema::hasTable('vehicle_providers'))
            {
                Schema::create('vehicle_providers', function (Blueprint $table)
                {
                    $table->increments('vehicle_provider_id');
                    $table->string('provider_full_name');
                    $table->string('provider_email_address', 50)->nullable();
                    $table->text('provider_address');
                    $table->string('provider_contact_number');
                    $table->string('provider_aadhaar_number');
                    $table->text('provider_aadhaar')->nullable();
                    $table->tinyInteger('vehicle_provider_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('vehicle_providers');
        }

    }
    