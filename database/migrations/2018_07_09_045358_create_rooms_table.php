<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateRoomsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'rooms';
        protected $primaryKey = 'room_id';

        public function up()
        {
            if (!Schema::hasTable('rooms'))
            {
                Schema::create('rooms', function (Blueprint $table)
                {
                    $table->increments('room_id');
                    $table->integer('room_number');
                    $table->integer('total_seats');
                    $table->tinyInteger('room_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('rooms');
        }

    }
    