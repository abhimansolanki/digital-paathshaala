<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleDriverHelpersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     protected $table      = 'vehicle_driver_helpers';
        protected $primaryKey = 'vehicle_helper_id';

    public function up()
    {
         if (!Schema::hasTable('vehicle_driver_helpers'))
            {
                Schema::create('vehicle_driver_helpers', function (Blueprint $table)
                {
                    $table->increments('vehicle_helper_id');
                    $table->string('helper_full_name');
                    $table->string('helper_email_address', 50)->nullable();
                    $table->text('helper_address');
                    $table->string('helper_contact_number');
                    $table->string('helper_aadhaar_number');
                    $table->text('helper_aadhaar')->nullable();
                    $table->string('helper_license_number');
                    $table->text('helper_license')->nullable();
                    $table->tinyInteger('vehicle_helper_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_driver_helpers');
    }
}
