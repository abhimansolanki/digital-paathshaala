<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChapterTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapter_topics', function (Blueprint $table) {
            $table->increments('chapter_topic_id');

            $table->integer('subject_chapter_id')->unsigned();
            $table->foreign('subject_chapter_id')->references('subject_chapter_id')->on('subject_chapters')->onDelete('cascade');

            $table->string('name');
            $table->longText('video_link')->nullable();
            $table->string('video_file')->nullable();
            $table->string('image_file')->nullable();
            $table->longText('reading_text')->nullable();
            $table->tinyInteger('download_permission')->default(1)->comment('1-yes,2-no');
            $table->tinyInteger('status')->default(1)->comment('1-action');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapter_topics');
    }
}
