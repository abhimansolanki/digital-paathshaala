<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{

    protected $table = 'employees';
    protected $primaryKey = 'employee_id';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('employees')) {
            Schema::create('employees', function (Blueprint $table) {
                $table->increments('employee_id');

                $table->integer('admin_user_id')->unsigned();
                $table->foreign('admin_user_id')->references('admin_user_id')->on('admin_users')->onDelete('cascade');

                $table->integer('session_id')->unsigned();
                $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                $table->integer('department_id')->unsigned();
                $table->foreign('department_id')->references('department_id')->on('departments')->onDelete('cascade');

                $table->integer('caste_category_id')->unsigned();
                $table->foreign('caste_category_id')->references('caste_category_id')->on('caste_categories')->onDelete('cascade');

                $table->string('employee_code', 150); //->unique();
                $table->string('full_name', 255);
                $table->string('father_name', 100);
                $table->tinyInteger('gender')->comment = '0:Female, 1:Male';
                $table->tinyInteger('marital_status')->comment = '0:No, 1:Yes';
                $table->tinyInteger('staff_type_id')->comment = '1:teaching, 2:non-teaching';
                $table->date('dob');
                $table->string('email', 50);
                $table->text('address');
                $table->string('contact_number', 50);
                $table->string('pan_number', 50)->nullable();
                $table->string('aadhaar_number', 50);
                $table->date('join_date', 50);
                $table->string('bank_name', 255);
                $table->string('ifsc_code', 25);
                $table->string('account_number', 50);
                $table->string('paid_leave_pm', 50)->nullable();
                $table->string('casual_leave_pm', 50)->nullable();
                $table->string('medical_leave', 50)->nullable();
                $table->string('tds_deducation', 50)->nullable();
                $table->text('profile_photo')->nullable();
                $table->text('qualification');
                $table->text('specialization')->nullable();
                $table->text('experience')->nullable();
                $table->text('reference')->nullable();
                $table->tinyInteger('employee_status')->default(1)->comment = '0:Inactive, 1:Active';
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }

}
    