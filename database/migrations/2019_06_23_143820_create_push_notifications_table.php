<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreatePushNotificationsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
	if (!Schema::hasTable('push_notifications'))
	{
	    Schema::create('push_notifications', function (Blueprint $table)
	    {
	        $table->increments('push_notification_id');
	        $table->string('multicast_id');
	        $table->integer('canonical_ids');
	        $table->text('results');
	        $table->text('notification_data');
	        $table->tinyInteger('notification_type')->default(1)->comment = "1:Event, 2:Notice, 3:Attendance,4: Exam Time table, 5 : Exam Result";
	        $table->integer('success')->default(0);
	        $table->integer('failure')->default(0);
            $table->softDeletes();
	        $table->timestamps();
	    });
	}
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
	Schema::dropIfExists('push_notifications');
        }

    }
    