<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateNoticesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table = 'notices';
        protected $primaryKey = 'notice_id';

        public function up()
        {
	if (!Schema::hasTable('notices'))
	{
	    Schema::create('notices', function (Blueprint $table)
	    {
	        $table->increments('notice_id');
	        $table->integer('session_id')->unsigned();
	        $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

	        $table->integer('class_id')->unsigned()->nullable();
	        $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

	        $table->integer('section_id')->unsigned()->nullable();
	        $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');

	        $table->integer('department_id')->unsigned()->nullable();
	        $table->foreign('department_id')->references('department_id')->on('departments')->onDelete('cascade');

	        $table->date('start_date');
	        $table->date('end_date');
	        $table->text('notice_title');
	        $table->tinyInteger('notice_for')->default(1)->comment = "1:All Student, 2:All Staff, 3:All Student & Staff, 4:Selected Student(s), 5:Selected Staff(s),6:Selected Student & Staff";
	        $table->string('student_id', 255)->nullable();
	        $table->string('employee_id', 255)->nullable();
	        $table->text('notice_image')->nullable();
	        $table->longText('notice')->nullable();
	        $table->tinyInteger('notice_status')->default(1)->comment = "1:Active, 0:Blocked";
            $table->softDeletes();
	        $table->timestamps();
	    });
	}
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
	Schema::dropIfExists('notices');
        }

    }
    