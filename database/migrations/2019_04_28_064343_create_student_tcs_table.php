<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentTcsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
	if (!Schema::hasTable('student_tcs'))
	{
	    Schema::create('student_tcs', function (Blueprint $table)
	    {
	        $table->increments('student_tc_id');
	        
	        $table->integer('tc_number')->unique();
	        
	        $table->integer('student_id')->unsigned();
	        $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');
	        
	        $table->integer('session_id')->unsigned();
	        $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');
	        
//	        $table->integer('promoted_class_id')->unsigned()->nullable();
//	        $table->foreign('promoted_class_id')->references('class_id')->on('classes')->onDelete('cascade');
	        
	        $table->integer('class_id')->unsigned();
	        $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');
	        
	        $table->integer('section_id')->unsigned();
	        $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');


            $table->string('print_tc_number')->unique();
            $table->date('tc_application_date');
            $table->date('tc_date');
            $table->date('final_date')->nullable();
            $table->string('exam_result_remark')->nullable();
            $table->tinyInteger('tc_status')->default(1)->comment="1:Okay, 2:Cancel";
            $table->text('student_conduct')->nullable();
            $table->integer('total_working_day')->nullable();
            $table->integer('total_attendance')->nullable();
            $table->text('tc_reason')->nullable();
            $table->softDeletes();
	        $table->timestamps();
	    });
	}
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
	Schema::dropIfExists('student_tcs');
        }

    }
    