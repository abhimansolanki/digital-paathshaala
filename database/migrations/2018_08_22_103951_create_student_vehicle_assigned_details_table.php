<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentVehicleAssignedDetailsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'student_vehicle_assigned_details';
        protected $primaryKey = 'student_vehicle_assigned_detail_id';

        public function up()
        {
            Schema::create('student_vehicle_assigned_details', function (Blueprint $table)
            {
                $table->increments('student_vehicle_assigned_detail_id');
                
                $table->integer('assigned_id')->unsigned();
                $table->foreign('assigned_id')->references('student_vehicle_assigned_id')->on('student_vehicle_assigned')->onDelete('cascade');
                
	    $table->integer('student_id')->unsigned()->nullable();
                $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');
                
	    $table->integer('employee_id')->unsigned()->nullable();
                $table->foreign('employee_id')->references('employee_id')->on('employees')->onDelete('cascade');
                
                $table->date('join_date');
                $table->date('stop_date')->nullable()->comment              = "Leave assigned vehicle date";
                $table->tinyInteger('assigned_to')->default(1)->comment     = '1:Student, 2:Staff';
                $table->tinyInteger('assigned_status')->comment          = '1:Assigned, 0:Unassigned';
                $table->tinyInteger('status')->default(1)->comment = '1:Active, 0:Blocked';
                $table->softDeletes();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('student_vehicle_assigned_trans');
        }

    }
    