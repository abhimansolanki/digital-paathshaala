<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentTypesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'student_types';
        protected $primaryKey = 'student_type_id';

        public function up()
        {
            if (!Schema::hasTable('student_types'))
            {
                Schema::create('student_types', function (Blueprint $table)
                {
                    $table->increments('student_type_id');
                    $table->string('student_type', 50);
                    $table->tinyInteger('student_type_status')->default(1)->nullable()->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }

            $student_type = \Config::get('custom.student_type');
            if (!empty($student_type))
            {
                foreach ($student_type as $key => $value)
                {
                    DB::table('student_types')->insert(
                        array(
                            'student_type'        => $value,
                            'student_type_status' => 1,
                            'created_at'          => date('Y-m-d H:i:s'),
                            'updated_at'          => date('Y-m-d H:i:s'),
                        )
                    );
                }
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('student_types');
        }

    }
    