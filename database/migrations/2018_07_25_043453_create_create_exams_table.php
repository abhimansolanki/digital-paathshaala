<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateCreateExamsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'create_exams';
        protected $primaryKey = 'exam_id';

        public function up()
        {
            if (!Schema::hasTable('create_exams'))
            {
                Schema::create('create_exams', function (Blueprint $table)
                {
                    $table->increments('exam_id');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                    $table->integer('exam_category_id')->unsigned();
                    $table->foreign('exam_category_id')->references('exam_category_id')->on('exam_categories')->onDelete('cascade');

                    $table->string('exam_name');
                    $table->string('exam_alias')->nullable();
                    $table->tinyInteger('in_final_marksheet')->default(0)->comment = '1:Yes, 0:No';
                    $table->tinyInteger('exam_nature')->comment = '1:Main, 2:suppli,3:other';
                    $table->tinyInteger('exam_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->integer("sequence")->default(1);
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('create_exams');
        }

    }
    