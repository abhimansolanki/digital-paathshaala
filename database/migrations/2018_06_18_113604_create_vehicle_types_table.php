<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateVehicleTypesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'vehicle_types';
        protected $primaryKey = 'vehicle_type_id';

        public function up()
        {
            if (!Schema::hasTable('vehicle_types'))
            {
                Schema::create('vehicle_types', function (Blueprint $table)
                {
                    $table->increments('vehicle_type_id');
                    $table->string('vehicle_type', 100);
                    $table->tinyInteger('vehicle_type_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('vehicle_types');
        }

    }
    