<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateFeeCircularsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'fee_circulars';
        protected $primaryKey = 'fee_circular_id';

        public function up()
        {
            if (!Schema::hasTable('fee_circulars'))
            {
                Schema::create('fee_circulars', function (Blueprint $table)
                {
                    $table->increments('fee_circular_id');
                    $table->string('fee_circular', 50);
                    $table->tinyInteger('fee_circular_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->tinyInteger('is_fixed')->default(0)->comment = '1:Yes, 0:No';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }

            $fee_circular = \Config::get('custom.fee_circular');
            if (!empty($fee_circular))
            {
                foreach ($fee_circular as $key => $circular)
                {
                    if ($key == 'yearly')
                    {
                        DB::table('fee_circulars')->insert(
                            array(
                                'fee_circular' => $key,
                                'is_fixed'        => 1,
                                'created_at'   => date('Y-m-d H:i:s'),
                                'updated_at'   => date('Y-m-d H:i:s'),
                            )
                        );
                    }
                }
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('fee_circulars');
        }

    }
    