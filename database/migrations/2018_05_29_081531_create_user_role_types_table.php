<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateUserRoleTypesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'user_roles';
        protected $primaryKey = 'user_role_id';

        public function up()
        {
            if (!Schema::hasTable('user_roles'))
            {
                Schema::create('user_roles', function (Blueprint $table)
                {
                    $table->increments('user_role_id');

                    $table->integer('user_type_id')->unsigned();
                    $table->foreign('user_type_id')->references('user_type_id')->on('user_types')->onDelete('cascade');

                    $table->string('user_role_name', 50);
                    $table->tinyInteger('user_role_status')->default(1)->nullable()->comment = '0: Blocked,1: Active';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }

            DB::table('user_roles')->insert(
                array(
                    'user_type_id'   => 1,
                    'user_role_name' => 'Admin',
                    'created_at'     => date('Y-m-d H:i:s'),
                    'updated_at'     => date('Y-m-d H:i:s'),
                )
            );
            DB::table('user_roles')->insert(
                array(
                    'user_type_id'   => 2,
                    'user_role_name' => 'Parent',
                    'created_at'     => date('Y-m-d H:i:s'),
                    'updated_at'     => date('Y-m-d H:i:s'),
                )
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('user_roles');
        }

    }
    