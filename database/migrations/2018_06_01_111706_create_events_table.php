<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateEventsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'events';
        protected $primaryKey = 'event_id';

        public function up()
        {
            if (!Schema::hasTable('events'))
            {
                Schema::create('events', function (Blueprint $table)
                {
                    $table->increments('event_id');
                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');
                    $table->string('event_name', 100);
                    $table->text('event_venue');
                    $table->date('event_start_date');
                    $table->date('event_end_date');
                    $table->string('event_start_time')->nullable();
                    $table->string('event_end_time')->nullable();
                    $table->string('event_type')->nullable();
                    $table->text('event_description')->nullable();
                    $table->tinyInteger('event_for')->comment                            = '1:Student,2: Staff, 3: Both';
                    $table->tinyInteger('event_status')->default(1)->nullable()->comment = '0: Blocked,1: Active';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('events');
        }

    }
    