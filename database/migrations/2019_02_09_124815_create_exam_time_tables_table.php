<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateExamTimeTablesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            if (!Schema::hasTable('exam_time_tables')) 
            {
                Schema::create('exam_time_tables', function (Blueprint $table)
                {
                    $table->increments('exam_time_table_id');
                    
                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');
                    
                    $table->integer('exam_id')->unsigned();
                    $table->foreign('exam_id')->references('exam_id')->on('create_exams')->onDelete('cascade');

                    $table->string('title');
                    
                    $table->tinyInteger('status')->default(1)->comment = "1:Active, 0:Blocked";
                    $table->tinyInteger('publish')->default(0)->comment = "1:Yes, 0:No";
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('exam_time_tables');
        }

    }
    