<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'students';
        protected $primaryKey = 'student_id';

        public function up()
        {
            if (!Schema::hasTable('students'))
            {
                Schema::create('students', function (Blueprint $table)
                {
                    $table->increments('student_id')->index();

                    $table->integer('current_session_id')->unsigned();
                    $table->foreign('current_session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                    $table->integer('current_class_id')->unsigned();
                    $table->foreign('current_class_id')->references('class_id')->on('classes')->onDelete('cascade');
                    
                    $table->integer('current_section_id')->unsigned();
                    $table->foreign('current_section_id')->references('section_id')->on('sections')->onDelete('cascade');

                    $table->integer('student_parent_id')->unsigned();
                    $table->foreign('student_parent_id')->references('student_parent_id')->on('student_parents')->onDelete('cascade');

                    $table->integer('student_type_id')->unsigned();
                    $table->foreign('student_type_id')->references('student_type_id')->on('student_types')->onDelete('cascade');

                    $table->integer('caste_category_id')->unsigned();
                    $table->foreign('caste_category_id')->references('caste_category_id')->on('caste_categories')->onDelete('cascade');

                    $table->string('form_number',255)->nullable()->comment = 'form_number = form_number from student_enqires table';
                    $table->unsignedInteger('enrollment_number')->unique();
                    $table->string('first_name', 255)->default('');
                    $table->string('middle_name', 100)->default('')->nullable();
                    $table->string('last_name', 255)->default('')->nullable();
                    $table->date('admission_date');
                    $table->date('join_date');
                    $table->text('address_line1');
                    $table->text('address_line2')->nullable();
                    $table->date('dob')->nullable();
                    $table->tinyInteger('gender')->comment = '0:Girl,1:Boy';
                    $table->string('birth_place', 20)->nullable();
                    $table->string('contact_number', 20)->nullable();
                    $table->string('blood_group', 20)->nullable();
                    $table->string('body_sign', 20)->nullable();
                    $table->string('email', 50)->nullable();
                    $table->string('aadhaar_number', 25)->nullable();
                    $table->decimal('fee_discount', 10, 2)->nullable();
                    $table->date('discount_date')->nullable()->comment ='Discount applicable date';
                    $table->text('remark')->nullable();
                    $table->enum('fee_calculate_month', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])->comment = '1:Jan, 2:Feb,12:Dec';
                    $table->decimal('previous_due_fee', 10, 2)->nullable();
                    $table->decimal('previous_due_fee_original', 10, 2)->nullable();
                    $table->string('previous_section_name', 10)->nullable();
                    $table->string('previous_class_name', 10)->nullable();
                    $table->string('previous_school_name', 50)->nullable();
                    $table->string('previous_result', 50)->nullable();
                    $table->text('profile_photo')->nullable();
                    $table->string('tc_number', 50)->nullable();
                    $table->string('student_doc', 50)->nullable();
                    $table->date('tc_date')->nullable();
                    $table->tinyInteger('new_student')->default(0);
                    $table->enum('student_left', ['Yes', 'No'])->default('No');
                    $table->text('reason')->nullable();
        	        $table->string('religion')->nullable();
        	        $table->tinyInteger('app_access_fee_status')->default(0)->nullable()->comment = '1:Paid, 2:Expired or Unpaid';
                    $table->tinyInteger('student_status')->default(1)->nullable()->comment = '1:Active, 0:Blocked';
                    $table->tinyInteger('app_accessbility_status')->default(1)->nullable()->comment = '0: No Access, 1:Full Access, 2:Partial Access';
                    $table->string('admission_class_name',191)->nullable();
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('students');
        }

    }
    