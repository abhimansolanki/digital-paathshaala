<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentVehicleAssignedTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'student_vehicle_assigned';
        protected $primaryKey = 'student_vehicle_assigned_id';

        public function up()
        {
            if (!Schema::hasTable('student_vehicle_assigned'))
            {
                Schema::create('student_vehicle_assigned', function (Blueprint $table)
                {
                    $table->increments('student_vehicle_assigned_id');

                    $table->integer('vehicle_id')->unsigned();
                    $table->foreign('vehicle_id')->references('vehicle_id')->on('vehicles')->onDelete('cascade');
                    
                    $table->integer('stop_point_id')->unsigned();
                    $table->foreign('stop_point_id')->references('stop_point_id')->on('stop_points')->onDelete('cascade');
                    
                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                    $table->string('pickup_time', 50);
                    $table->string('return_time', 50);
                
                    $table->tinyInteger('status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('student_vehicle_assigned');
        }

    }
    