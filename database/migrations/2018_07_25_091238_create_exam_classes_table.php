<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateExamClassesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'exam_classes';
        protected $primaryKey = 'exam_class_id';

        public function up()
        {
            if (!Schema::hasTable('exam_classes'))
            {
                Schema::create('exam_classes', function (Blueprint $table)
                {
                    $table->increments('exam_class_id');

                    $table->integer('exam_id')->unsigned();
                    $table->foreign('exam_id')->references('exam_id')->on('create_exams')->onDelete('cascade');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                    $table->integer('class_id')->unsigned();
                    $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');
                    
                    $table->integer('section_id')->unsigned();
                    $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');
                    
                    
                    $table->text('subject_marks_criteria')->comment  = "set min and max marks criteria for exam's subject";
                    
                    $table->tinyInteger('status')->default(1)->comment = "1:Active, 0:Blocked";
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('exam_classes');
        }

    }
    