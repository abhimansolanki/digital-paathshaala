<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSessionsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'sessions';
        protected $primaryKey = 'session_id';

        public function up()
        {
            if (!Schema::hasTable('sessions'))
            {
                Schema::create('sessions', function (Blueprint $table)
                {
                    $table->increments('session_id');
                    $table->date('start_date');
                    $table->date('end_date');
                    $table->string('session_year', 100);
                    $table->text('description')->nullable();
                    $table->tinyInteger('session_status')->default(0)->nullable()->comment = '1:Active, 0:Blocked';
                    $table->tinyInteger('is_fixed')->default(0)->comment                   = '1:Yes, 0:No';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
            DB::table('sessions')->insert(
                array(
                    'start_date'   => date('Y-m-d'),
                    'end_date'     => date('Y-m-d', strtotime('next year')),
                    'session_year' => date('Y') . '-' . date('Y', strtotime('next year')),
                    'session_status'     => 1,
                    'is_fixed'     => 1,
                    'created_at'   => date('Y-m-d H:i:s'),
                    'updated_at'   => date('Y-m-d H:i:s'),
                )
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('sessions');
        }

    }
    