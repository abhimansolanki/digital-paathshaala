<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateGradesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table = 'grades';
        protected $primaryKey = 'grade_id';

        public function up()
        {
	if (!Schema::hasTable('grades'))
	{
	    Schema::create('grades', function (Blueprint $table)
	    {
	        $table->increments('grade_id');

	        $table->integer('session_id')->unsigned();
	        $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

	        $table->string('grade_name', 10);
	        $table->string('grade_alias', 25)->nullable();
	        $table->integer('minimum');
	        $table->integer('maximum');
	        $table->integer('previous_id')->nullable();
	        $table->text('grade_comments')->nullable()->comment = 'comments related to grade';
	        $table->tinyInteger('grade_status')->default(1)->comment = '1:Active, 0:Blocked';
            $table->softDeletes();
	        $table->timestamps();
	    });
	}
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
	Schema::dropIfExists('grades');
        }

    }
    