<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
     protected $table      = 'holidays';
        protected $primaryKey = 'holiday_id';

        public function up()
        {
            if (!Schema::hasTable('holidays'))
            {
                Schema::create('holidays', function (Blueprint $table)
                {
                    $table->increments('holiday_id');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                    $table->string('holiday_name', 100);
                    $table->string('holiday_group', 100)->nullable();
                    $table->date('holiday_start_date');
                    $table->date('holiday_end_date');
                    $table->tinyInteger('holiday_status')->default(1)->nullable()->comment='0: Blocked,1: Active';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('holidays');
    }
}
