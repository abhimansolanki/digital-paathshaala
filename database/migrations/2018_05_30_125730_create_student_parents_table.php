<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentParentsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table = 'student_parents';
        protected $primaryKey = 'student_parent_id';

        public function up()
        {
	if (!Schema::hasTable('student_parents'))
	{
	    Schema::create('student_parents', function (Blueprint $table)
	    {
	        $table->increments('student_parent_id');

	        $table->integer('admin_user_id')->unsigned();
	        $table->foreign('admin_user_id')->references('admin_user_id')->on('admin_users')->onDelete('cascade');


	        $table->string('father_email', 50)->default('')->nullable();
	        $table->string('father_aadhaar_number', 50)->nullable();
	        $table->string('father_first_name', 255)->default('')->nullable();
	        $table->string('father_middle_name', 255)->default('')->nullable();
	        $table->string('father_last_name', 255)->default('')->nullable();
	        $table->string('father_contact_number', 50)->nullable();
	        $table->string('father_occupation', 50)->nullable();
	        $table->decimal('father_income', 10, 2)->nullable();
	        $table->string('mother_email', 50)->nullable();
	        $table->string('mother_aadhaar_number', 50)->nullable();
	        $table->string('mother_first_name', 100)->default('')->nullable();
	        $table->string('mother_middle_name', 100)->default('')->nullable();
	        $table->string('mother_last_name', 100)->default('')->nullable();
	        $table->string('mother_contact_number', 50)->nullable();
	        $table->string('mother_occupation', 50)->nullable();
	        $table->decimal('mother_income', 10, 2)->nullable();
	        $table->string('guardian_email', 100)->nullable();
	        $table->string('guardian_aadhaar_number', 50)->nullable();
	        $table->string('guardian_first_name', 100)->default('')->nullable();
	        $table->string('guardian_middle_name', 100)->default('')->nullable();
	        $table->string('guardian_last_name', 100)->default('')->nullable();
	        $table->string('guardian_contact_number', 50)->nullable();
	        $table->string('guardian_occupation', 50)->nullable();
	        $table->string('guardian_relation', 50)->nullable();
	        $table->tinyInteger('care_of')->default(1)->nullable()->comment = '1:Father, 2:Mother, 3:Guardian';
	        $table->tinyInteger('sibling_exist')->default(0)->nullable()->comment = '1:Yes, 0:No';
	        $table->tinyInteger('student_parent_status')->default(1)->nullable()->comment = '1:Active, 0:Blocked';
            $table->softDeletes();
	        $table->timestamps();
	    });
	}
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
	Schema::dropIfExists('student_parents');
        }

    }
    