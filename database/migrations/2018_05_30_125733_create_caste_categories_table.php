<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateCasteCategoriesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'caste_categories';
        protected $primaryKey = 'caste_category_id';

        public function up()
        {
            if (!Schema::hasTable('caste_categories'))
            {
                Schema::create('caste_categories', function (Blueprint $table)
                {
                    $table->increments('caste_category_id')->index();
                    $table->string('caste_name', 255);
                    $table->tinyInteger('caste_category_status')->default(1)->nullable()->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }

            $arr_caste_category = \Config::get('custom.arr_caste_category');
            if (!empty($arr_caste_category))
            {
                foreach ($arr_caste_category as $key => $caste_category)
                {
                    DB::table('caste_categories')->insert(
                        array(
                            'caste_name'            => $caste_category,
                            'caste_category_status' => 1,
                            'created_at'            => date('Y-m-d H:i:s'),
                            'updated_at'            => date('Y-m-d H:i:s'),
                        )
                    );
                }
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('caste_categories');
        }

    }
    