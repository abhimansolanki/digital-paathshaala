<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentProgressesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            if (!Schema::hasTable('student_progresses'))
            {
                Schema::create('student_progresses', function (Blueprint $table)
                {
                    $table->increments('student_progress_id');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                    $table->integer('class_id')->unsigned();
                    $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

                    $table->integer('section_id')->unsigned();
                    $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');

                    $table->integer('student_id')->unsigned();
                    $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');

                    $table->integer('subject_id')->unsigned();
                    $table->foreign('subject_id')->references('subject_id')->on('subjects')->onDelete('cascade');

                    $table->integer('month')->comment                = "Name of report month";
                    $table->string('progress');
                    $table->tinyInteger('type')->default(1)->comment = "1:Subject, 2:SubSubject";
                    $table->tinyInteger('status')->default(1);
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('student_progresses');
        }

    }
    