<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateFeeTypesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'fee_types';
        protected $primaryKey = 'fee_type_id';

        public function up()
        {
            if (!Schema::hasTable('fee_types'))
            {
                Schema::create('fee_types', function (Blueprint $table)
                {
                    $table->increments('fee_type_id');
                    $table->integer('root_id')->default(0)->comment             = 'parent fee type id';
                    $table->string('fee_type', 50);
                    $table->tinyInteger('is_root')->default(1)->comment         = '1:Yes, 0:No';
                    $table->tinyInteger('is_fixed')->default(0)->comment        = '1:Yes, 0:No';
                    $table->tinyInteger('fee_type_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->tinyInteger('order')->comment                       = 'fee type order';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }

            $fixed_fee_type = \Config::get('custom.fixed_fee_type');
            if (!empty($fixed_fee_type))
            {
                foreach ($fixed_fee_type as $key => $fee_type)
                {
                    DB::table('fee_types')->insert(
                        array(
                            'root_id'         => 0,
                            'fee_type'        => $fee_type,
                            'is_root'         => 1,
                            'is_fixed'        => 1,
                            'fee_type_status' => 1,
                            'order'           => $key,
                            'created_at'      => date('Y-m-d H:i:s'),
                            'updated_at'      => date('Y-m-d H:i:s'),
                        )
                    );
                }
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('fee_types');
        }

    }
    