<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateVehiclesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'vehicles';
        protected $primaryKey = 'vehicle_id';

        public function up()
        {
            if (!Schema::hasTable('vehicles'))
            {
                Schema::create('vehicles', function (Blueprint $table)
                {
                    $table->increments('vehicle_id');

                    $table->integer('route_id')->unsigned();
                    $table->foreign('route_id')->references('route_id')->on('routes');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions');
                    
                    $table->integer('vehicle_provider_id')->unsigned();
                    $table->foreign('vehicle_provider_id')->references('vehicle_provider_id')->on('vehicle_providers');

                    $table->integer('vehicle_driver_id')->unsigned();
                    $table->foreign('vehicle_driver_id')->references('vehicle_driver_id')->on('vehicle_drivers');

                    $table->integer('vehicle_helper_id')->unsigned();
                    $table->foreign('vehicle_helper_id')->references('vehicle_helper_id')->on('vehicle_driver_helpers');

                    $table->string('vehicle_number', 25);
                    $table->string('vehicle_name');
                    $table->string('vehicle_color');
                    $table->string('gps_device_id')->nullable();
                    $table->integer('total_seats');
                    $table->integer('strength');
                    $table->date('purchase_date');
                    $table->date('insurance_date');
                    $table->date('ins_expired_date');
                    $table->tinyInteger('vehicle_status')->default(1)->comment = '1:Active, 0:Inactive';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('vehicles');
        }

    }
    