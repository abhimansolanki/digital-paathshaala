<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateExamTypesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'exam_types';
        protected $primaryKey = 'exam_type_id';

        public function up()
        {
            if (!Schema::hasTable('exam_types'))
            {
                Schema::create('exam_types', function (Blueprint $table)
                {
                    $table->increments('exam_type_id');
                    $table->string('exam_type', 100);
                    $table->string('exam_type_alias', 100)->nullable();
                    $table->integer('exam_type_order')->nullable();
                    $table->tinyInteger('exam_type_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
            $fixed_exam_type = \Config::get('custom.exam_type');
            if (!empty($fixed_exam_type))
            {
                foreach ($fixed_exam_type as $key => $exam_type)
                {
                    DB::table('exam_types')->insert(
                        array(
                            'exam_type'       => $exam_type,
                            'exam_type_alias' => $exam_type,
                            'exam_type_order' => $key,
                            'created_at'      => date('Y-m-d H:i:s'),
                            'updated_at'      => date('Y-m-d H:i:s'),
                        )
                    );
                }
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('exam_types');
        }

    }
    