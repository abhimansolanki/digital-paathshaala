<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateCoScholasticsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'co_scholastics';
        protected $primaryKey = 'co_scholastic_id';

        public function up()
        {
            if (!Schema::hasTable('co_scholastics'))
            {
                Schema::create('co_scholastics', function (Blueprint $table)
                {
                    $table->increments('co_scholastic_id');
                    $table->integer('root_id')->default(0);
                    $table->string('co_scholastic_name', 100);
                    $table->string('co_scholastic_head')->nullable();
                    $table->text('description')->nullable();
                    $table->integer('order')->nullable();
                    $table->tinyInteger('status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('co_scholastics');
        }

    }
    