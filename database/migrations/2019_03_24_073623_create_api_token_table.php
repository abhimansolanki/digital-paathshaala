<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateApiTokenTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
	if (!Schema::hasTable('api_token'))
	{
	    Schema::create('api_token', function (Blueprint $table)
	    {
	        $table->increments('token_id');

	        $table->integer('admin_user_id')->unsigned();
	        $table->foreign('admin_user_id')->references('admin_user_id')->on('admin_users')->onDelete('cascade');

	        $table->string('token');
	        $table->text('device_id')->nullable();
	        $table->tinyInteger('status')->default(1);
	        $table->timestamps();
	    });
	}
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
	Schema::dropIfExists('api_token');
        }

    }
    