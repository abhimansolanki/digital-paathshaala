<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateBanksTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'banks';
        protected $primaryKey = 'bank_id';

        public function up()
        {
            if (!Schema::hasTable('banks'))
            {
                Schema::create('banks', function (Blueprint $table)
                {
                    $table->increments('bank_id');
                    $table->string('bank_name', 255);
                    $table->string('bank_alias', 255)->nullable();
                    $table->text('bank_branch')->nullable();
                    $table->string('ifsc_code', 100)->unique();
                    $table->string('bank_contact_number', 30)->nullable();

                    $table->tinyInteger('bank_status')->default(1)->nullable()->comment = '0: Blocked,1: Active';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('banks');
        }

    }
    