<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateClassCoScholasticsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table = 'class_co_scholastics';
        protected $primaryKey = 'class_co_scholastic_id';

        public function up()
        {
	if (!Schema::hasTable('class_co_scholastics'))
	{
	    Schema::create('class_co_scholastics', function (Blueprint $table)
	    {
	        $table->increments('class_co_scholastic_id');

	        $table->integer('session_id')->unsigned();
	        $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

	        $table->integer('class_id')->unsigned();
	        $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

	        $table->integer('section_id')->unsigned();
	        $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');

	        $table->integer('exam_category_id')->unsigned();
	        $table->foreign('exam_category_id')->references('exam_category_id')->on('exam_categories')->onDelete('cascade');


	        $table->text('co_scholastic_id');
	        $table->tinyInteger('status')->default(1)->comment = "1:Active, 0:Blocked";
            $table->softDeletes();
	        $table->timestamps();
	    });
	}
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
	Schema::dropIfExists('class_co_scholastics');
        }

    }
    