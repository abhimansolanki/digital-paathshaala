<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSubjectGroupsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'subject_groups';
        protected $primaryKey = 'subject_group_id';

        public function up()
        {
            if (!Schema::hasTable('subject_groups'))
            {
                Schema::create('subject_groups', function (Blueprint $table)
                {
                    $table->increments('subject_group_id');
                    $table->string('group_name', 50);
                    $table->tinyInteger('group_status')->default(1)->comment      = '1 : Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
            
            $fixed_subject_group = \Config::get('custom.fixed_subject_group');
            if (!empty($fixed_subject_group))
            {
                foreach ($fixed_subject_group as $key => $subject_group)
                {
                    DB::table('subject_groups')->insert(
                        array(
                            'group_name'        => $subject_group,
                            'created_at'      => date('Y-m-d H:i:s'),
                            'updated_at'      => date('Y-m-d H:i:s'),
                        )
                    );
                }
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('subject_group_groups');
        }

    }
    