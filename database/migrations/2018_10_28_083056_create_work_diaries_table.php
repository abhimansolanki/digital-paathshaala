<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkDiariesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'work_diaries';
    protected $primaryKey = 'work_diary_id';

    public function up()
    {
    	if (!Schema::hasTable('work_diaries'))
    	{
    		Schema::create('work_diaries', function (Blueprint $table)
    		{
    			$table->increments('work_diary_id');

    			$table->integer('session_id')->unsigned();
    			$table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

    			$table->integer('class_id')->unsigned();
    			$table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

    			$table->integer('subject_id')->unsigned();
    			$table->foreign('subject_id')->references('subject_id')->on('subjects')->onDelete('cascade');

    			$table->integer('section_id')->unsigned();
    			$table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');

    			$table->tinyInteger('work_type')->default(1)->comment = "1:CW, 2:HW";

    			$table->longText('work')->nullable();
    			$table->text('work_doc')->nullable();

    			$table->text('work_doc_file_title')->nullable();
    			$table->text('work_doc_file')->nullable();
                $table->tinyInteger('work_doc_file_download_permission')->default(2)->comment = "1:Download, 2:Not Download";
    		

    			$table->text('work_doc_video_title')->nullable();
    			$table->text('work_doc_video')->nullable();
            
    			
    			$table->text('work_doc_link_title')->nullable();
    			$table->text('work_doc_link')->nullable();
    			
    			
    			$table->tinyInteger('for')->default(2)->comment = "1:single_student, 2:all_student";

    			$table->integer('student_id')->unsigned()->nullable();
    			$table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');

    			$table->date('work_start_date');
    			$table->date('work_end_date');
                $table->softDeletes();
    			$table->timestamps();
    		});
    	}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('work_diaries');
    }

}
