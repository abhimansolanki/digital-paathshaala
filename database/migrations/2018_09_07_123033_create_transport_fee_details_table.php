<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateTransportFeeDetailsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'transport_fee_details';
        protected $primaryKey = 'transport_fee_detail_id';

        public function up()
        {
            if (!Schema::hasTable('transport_fee_details'))
            {
                Schema::create('transport_fee_details', function (Blueprint $table)
                {
                    $table->increments('transport_fee_detail_id');

                    $table->integer('transport_fee_id')->unsigned();
                    $table->foreign('transport_fee_id')->references('transport_fee_id')->on('transport_fees')->onDelete('cascade');

                    $table->integer('month_id');
                    $table->decimal('fee_amount', 10, 2)->default(0);
                    $table->decimal('paid_fee_amount', 10, 2)->default(0);
                    $table->tinyInteger('amount_status')->comment     = '1:full amount, 2:Less amount';
                    $table->tinyInteger('fee_detail_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('transport_fee_details');
        }

    }
    