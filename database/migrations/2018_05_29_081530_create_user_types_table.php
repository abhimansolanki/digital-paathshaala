<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTypesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'user_types';
    protected $primaryKey = 'user_type_id';

    public function up()
    {
        if (!Schema::hasTable('user_types')) {
            Schema::create('user_types', function (Blueprint $table) {
                $table->increments('user_type_id');
                $table->string('user_type', 50);
                $table->tinyInteger('user_type_status')->default(1)->nullable()->comment = '0: Blocked,1: Active';
                $table->softDeletes();
                $table->timestamps();
            });
        }

            // Insert some stuff
        $user_type = \Config::get('custom.user_type');
        if (!empty($user_type)) {
            foreach ($user_type as $key => $value) {
                DB::table('user_types')->insert(
                    array(
                        'user_type' => $value,
                        'user_type_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    )
                );
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_types');
    }

}
    