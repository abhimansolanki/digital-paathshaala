<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateUserDevicesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
	if (!Schema::hasTable('user_devices'))
	{
	    Schema::create('user_devices', function (Blueprint $table)
	    {
	        $table->increments('user_device_id');

	        $table->integer('admin_user_id')->unsigned();
	        $table->foreign('admin_user_id')->references('admin_user_id')->on('admin_users')->onDelete('cascade');

	        $table->string('device_id');
	        $table->tinyInteger('notification_status')->default(1)->comment = "1:Yes, 0:No";

	        $table->timestamps();
	    });
	}
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
	Schema::dropIfExists('user_devices');
        }

    }
    