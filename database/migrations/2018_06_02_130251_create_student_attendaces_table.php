<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStudentAttendacesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'student_attendances';
        protected $primaryKey = 'student_attendance_id';

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {

            if (!Schema::hasTable('student_attendances'))
            {
                Schema::create('student_attendances', function (Blueprint $table)
                {
                    $table->increments('student_attendance_id');

                    $table->integer('session_id')->unsigned();
                    $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

                    $table->integer('class_id')->unsigned();
                    $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

                     $table->integer('section_id')->unsigned();
                    $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');

                    $table->integer('teacher_id')->unsigned()->nullable()->comment = 'From Employee Table(teacher who marked attendance)';
                    $table->foreign('teacher_id')->references('employee_id')->on('employees')->onDelete('cascade');

                    $table->integer('total_present')->default(0)->comment = 'Sum of present Students';
                    $table->integer('total_absent')->default(0)->comment  = 'Sum of absent Students';
                    $table->integer('on_leave')->default(0)->comment  = 'Sum of on Leave students';
                    $table->text('student_id')->comment                   = 'ID of absent Students';

                    $table->date('attendance_date');
                    $table->tinyInteger('attendance_status')->comment = '1:Marked, 0:Unmarked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('student_attendaces');
        }

    }
    