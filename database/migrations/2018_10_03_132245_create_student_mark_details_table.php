<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentMarkDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'student_mark_details';
    protected $primaryKey = 'student_mark_detail_id';
    public function up()
    {
         if (!Schema::hasTable('student_mark_details'))
            {
        Schema::create('student_mark_details', function (Blueprint $table) {
            $table->increments('student_mark_detail_id');
            
            $table->integer('student_mark_id')->unsigned();
            $table->foreign('student_mark_id')->references('student_mark_id')->on('student_marks')->onDelete('cascade');
            
            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');
            
            $table->text('obtained_marks');
            $table->float('percentage',10,2)->nullable();
            $table->string('rank',25)->nullable();
            $table->string('grade',25)->nullable();
            
            $table->tinyInteger('status')->default(1)->comment = '1:Active, 0:Blocked';
            $table->softDeletes();
            $table->timestamps();
        });
            }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_mark_details');
    }
}
