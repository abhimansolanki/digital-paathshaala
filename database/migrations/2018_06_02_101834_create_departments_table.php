<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateDepartmentsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'departments';
        protected $primaryKey = 'department_id';

        public function up()
        {
            if (!Schema::hasTable('departments'))
            {
                Schema::create('departments', function (Blueprint $table)
                {
                    $table->increments('department_id');

                    $table->string('department_name', 255);
                    $table->tinyInteger('department_status')->default(1)->nullable()->comment = '0: Blocked,1: Active';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('departments');
        }

    }
    