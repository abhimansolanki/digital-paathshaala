<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateComplaintDetailsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'complaint_details';
        protected $primaryKey = 'complaint_detail_id';

        public function up()
        {
            if (!Schema::hasTable('complaint_details'))
            {
                Schema::create('complaint_details', function (Blueprint $table)
                {
                    $table->increments('complaint_detail_id');

                    $table->integer('complaint_id')->unsigned();
                    $table->foreign('complaint_id')->references('complaint_id')->on('complaints')->onDelete('cascade');

                    $table->integer('admin_user_id')->unsigned();
                    $table->foreign('admin_user_id')->references('admin_user_id')->on('admin_users')->onDelete('cascade');

                    $table->longText('message');
                    $table->tinyInteger('status')->default(1)->comment = "1:Seen, 2:Unseen";
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('complaint_detail_details');
        }

    }
    