<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateDocumentTypesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'document_types';
        protected $primaryKey = 'document_type_id';

        public function up()
        {
            if (!Schema::hasTable('document_types'))
            {
                Schema::create('document_types', function (Blueprint $table)
                {
                    $table->increments('document_type_id');
                    $table->string('document_type', 50);
                    $table->string('column_name', 255);
	        $table->tinyInteger('status')->default(1)->comment = '0:Inactive, 1:Active';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('document_types');
        }

    }
    