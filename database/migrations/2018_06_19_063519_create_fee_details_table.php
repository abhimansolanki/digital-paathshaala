<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateFeeDetailsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        protected $table      = 'fee_details';
        protected $primaryKey = 'fee_detail_id';

        public function up()
        {
            if (!Schema::hasTable('fee_details'))
            {
                Schema::create('fee_details', function (Blueprint $table)
                {
                    $table->increments('fee_detail_id');

                    $table->integer('fee_id')->unsigned();
                    $table->foreign('fee_id')->references('fee_id')->on('fees')->onDelete('cascade');
                    $table->string('fee_sub_circular', 25)->nullable()->comment   = 'Fee sub circular name';
                    $table->integer('start_month')->nullable()->comment           = 'Circular Start Month';
                    $table->integer('end_month')->nullable()->comment             = 'Circular End Month';
                    $table->decimal('fee_amount', 10, 2)->default(0);
                    $table->date('fine_date')->nullable()->comment                = 'fine start date';
                    $table->integer('grace_period')->nullable()->comment          = 'in days';
                    $table->tinyInteger('fee_detail_status')->default(1)->comment = '1:Active, 0:Blocked';
                    $table->softDeletes();
                    $table->timestamps();
                });
            }

            // app fee
            $app_fee = \config('custom.app_access_fee');
            DB::table('fee_details')->insert(
                array(
                    'fee_id'           => 1,
                    'fee_sub_circular' => 'yearly',
                    'fee_amount'       => $app_fee,
                    'created_at'       => date('Y-m-d H:i:s'),
                    'updated_at'       => date('Y-m-d H:i:s'),
                )
            );

            // student previous due fee
            DB::table('fee_details')->insert(
                array(
                    'fee_id'           => 2,
                    'fee_sub_circular' => 'yearly',
                    'fee_amount'       => 0,
                    'created_at'       => date('Y-m-d H:i:s'),
                    'updated_at'       => date('Y-m-d H:i:s'),
                )
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('fee_detail_details');
        }

    }
    