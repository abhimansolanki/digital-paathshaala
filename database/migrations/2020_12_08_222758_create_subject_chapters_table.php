<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectChaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_chapters', function (Blueprint $table) {
            $table->increments('subject_chapter_id');

            $table->integer('session_id')->unsigned();
            $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');

            $table->integer('class_id')->unsigned();
            $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');

            $table->integer('section_id')->unsigned();
            $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');

            $table->integer('subject_id')->unsigned();
            $table->foreign('subject_id')->references('subject_id')->on('subjects')->onDelete('cascade');

            $table->string('name');
            $table->tinyInteger('status')->default(1)->comment('1-active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subject_chapters');
    }
}
