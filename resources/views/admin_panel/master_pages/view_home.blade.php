@extends('admin_panel/layout')
@section('content')

  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="{{ url('/admin-panel/master-pages/add-home') }}">Add More About</a>
          </li>
          <li class="crumb-icon">
            <a href="{{ url('/admin-panel/dashboard') }}">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
          </li>
          <li class="crumb-trail">View More About</li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb35">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span>Add More About</div>
          </div>

          <div class="panel-menu admin-form theme-primary">
            <div class="row">
              {!! Form::open() !!}

                <div class="col-md-2">
                  <label for="blog_category_title" class="field prepend-icon">
                    {!! Form::text('serach_home','',array('class' => 'form-control product','placeholder' => 'Search By Name', 'autocomplete' => 'off' )) !!}
                    <label for="blog_category_title" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>         
                </div>
                <div class="col-md-3">
                  <label for="blog_category_title" class="field prepend-icon">
                    {!! Form::text('serach_desc','',array('class' => 'form-control product','placeholder' => 'Search By Short Description', 'autocomplete' => 'off' )) !!}
                    <label for="blog_category_title" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>         
                </div>
                

                <div class="col-md-1">
                  <button type="submit" name="search" class="button btn-primary"> Search </button>
                </div>
              {!! Form::close() !!}

              {!! Form::open() !!}
                <div class="col-md-1 pull-right">
                    {!! Form::submit('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')) !!}
                </div>
              {!! Form::close() !!}
            </div>
          </div>

          <div class="panel-body pn">
              {!! Form::open(['name'=>'form']) !!}

              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class="">Name</th>
                      <th class="">Short Description</th>
                      <th class="">Description</th>
                      <th class="">Image</th>
                      <th class="text-right">Status/Edit</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                   @foreach($get_record as $get_records)   

                    <tr>
                      <td class="text-left" style="padding-left:18px">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="{{ $get_records->page_id }}">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>
                      
                      <td class="" style="padding-left:20px"> {{ $get_records->page_name }}</td>                     
                      <td class="text-left" style="padding-left:20px"> {{ $get_records->page_short_desc }} </td>
                      <td class="text-left" style="padding-left:20px"> {{ $get_records->page_description }} </td>
                      <td class="text-left" style="padding-left:20px">
                      {{ Html::image($get_records->page_img,'',array('style'=>'width:50px;height:50px;')) }}
                       </td>
                                            
                      <td class="text-right"  style="padding-left: 20px;">
                        <div class="btn-group text-left">
                          <button type="button" class="btn {{ $get_records->page_status == 1 ? 'btn-success' : 'btn-danger' }}  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> {{ $get_records->page_status == 1 ? 'Active' : 'Deactive' }}
                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:125px;">
                            <li>
                              <a href="{{ url('/admin-panel/master-pages/add-home/'.$get_records->page_id)}}">Edit</a>
                            </li>
                            <li class="divider"></li>

                           @if ($get_records->page_status == 0)
                            <li class="{{ $get_records->page_status == 1 ? 'active' : '' }}">
                              <a href="{{ url('/admin-panel/page-status') }}/{{$get_records->page_id}}/1">Active</a>
                            </li>
                           @else
                            <li class=" {{ $get_records->page_status == 0 ? 'active' : '' }} ">
                              <a href="{{ url('/admin-panel/page-status') }}/{{$get_records->page_id}}/0">Deactive</a>
                            </li>
                            @endif
                          </ul>
                        </div>
                      </td>

                     

                    </tr>
                      @endforeach
                  </tbody>
                </table>
              </div>
              {!! Form::close() !!}
          </div>

          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                    <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th>
                    <th>{{ $get_record->links() }}</th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
</style>

@endsection