@extends('admin_panel/layout')

@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/admin-panel/master-pages/view-home') }}">View Home</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/admin-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/admin-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Home</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Home </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">
                  @if ($message != "")
                    <div class="alert alert-danger" role="alert"><i class="fa fa-thumbs-o-down" aria-hidden="true">
                      {{ $message }}</i>
                    </div>
                  @endif   
                  @if ($errors->any())
                    <div id="log_error" class="alert alert-danger"><i class="fa fa-thumbs-o-down"> {{$errors->first()}} </i></div>
                  @endif

                    {!! Form::open(['url'=>'admin-panel/page-insert/'.$get_record->page_id , 'enctype' => 'multipart/form-data' , 'id' => 'validation' ] ) !!}
                      <div class="row">
                        <div class="col-md-5">
                          <div class="section">
                            <label for="blog_category_title" class="field-label" style="font-weight:600;" > Title </label>
                              <label for="blog_category_title" class="field prepend-icon">
                              
                                  {!! Form::text('title',$get_record->page_name, array('class' => 'event-name gui-input br-light light focusss','placeholder' => '','required' => 'ture' )) !!}                            

                                <label for="store-currency" class="field-icon">
                                  <i class="glyphicons glyphicons-pencil"></i>
                                </label>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-3" style="margin-left:150px;">
                          <div class="section">
                            <label for="name" class="field">
                              <div class="fileupload fileupload-new admin-form" data-provides="fileupload" >  
                                @if($get_record != "")
                                  <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important;">
                                    {!! Html::image($get_record['page_img'], 'Upload Image') !!}
                                  </div>
                                  <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                                    <span class="fileupload-new" >Change Image</span>
                                    <span class="fileupload-exists">Change Image</span>  
                                    {!! Form::file('image') !!}
                                  </span>
                                @else
                                  <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important;">
                                    <img data-src="holder.js/Upload Image" alt="Upload Image" src="" data-holder-rendered="true" style="height: 100% !important; width: 100%; display: block;">
                                  </div>
                                  <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                                    <span class="fileupload-new" >Image</span>
                                    <span class="fileupload-exists">Image</span>
                                    {!! Form::file('image') !!}
                                  </span>
                                @endif
                              </div>
                            </label>
                          </div>
                        </div>

                      </div>
                       <div class="row">
                        <div class="col-md-12">   
                          <div class="section">
                            <label for="blog_category_description" class="field-label" style="font-weight:600;" >Short Description </label>
                              <label for="blog_category_description" class="field prepend-icon">
                          
                                  {!! Form::textarea('short_description',$get_record->page_short_desc, array('class' => 'gui-textarea accc jqte-test','placeholder' => 'Short Description','required' => 'ture' )) !!}
                             
                              </label>
                          </div>
                          </div> 

                    <div class="row">
                      <div class="col-md-12">

                        <div class="section">
                            <label for="blog_category_description" class="field-label" style="font-weight:600;" > Description </label>
                              <label for="blog_category_description" class="field prepend-icon">
                           
                                  {!! Form::textarea('description',$get_record->page_description, array('class' => 'gui-textarea accc jqte-test','placeholder' => '' )) !!}
                              
                              </label>
                          </div>

                          <div class="section">
                            <label for="blog_category_title" class="field-label" style="font-weight:600;" >Meta Title </label>
                              <label for="blog_category_title" class="field prepend-icon">
                           
                                  {!! Form::text('meta_title','', array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}
                          
                                <label for="store-currency" class="field-icon">
                                  <i class="glyphicons glyphicons-pencil"></i>
                                </label>
                              </label>
                          </div>


                          <div class="section">
                            <label for="blog_category_order" class="field-label" style="font-weight:600;" > Meta Keyboard </label>
                              <label for="blog_category_order" class="field prepend-icon">
                                
                                  {!! Form::textarea('meta_keywords','', array('class' => 'event-name timmer br-light light focusss','placeholder' => '',  min => '0'  )) !!}
                              
                                <label for="store-currency" class="field-icon">
                                  <i class="glyphicons glyphicons-pencil"></i>
                                </label>
                              </label>
                          </div>

                          <div class="section">
                            <label for="blog_category_description" class="field-label" style="font-weight:600;" > Meta Description </label>
                              <label for="blog_category_description" class="field prepend-icon">
                        
                                  {!! Form::textarea('meta_description','', array('class' => 'gui-textarea accc jqte-test','placeholder' => '' )) !!}
                               
                              </label>
                          </div>

                      </div>
                    </div>


                    <div class="panel-footer">
                      {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                      {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                    </div>
                    
                    {!! Form::close() !!}

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
 <div id="lodding_image" style="width: 100%;height: 100%;position: fixed;z-index: 9999999; top: 0;text-align: center;background: rgba(255,255,255,0.5);display: none;"><img src="{{{ asset('public/admin/img/lodding.gif') }}}"></div>
<script type="text/javascript">
  function change(a){
    
    if(document.getElementById('user_role').value == 1 || document.getElementById('user_role').value == "")
    {
      $('#box01').hide();
    } else {
      $('#box01').show();
    }
  }
</script>

<script type="text/javascript">

    function showTextBox() {

      $('#box01').show();
    }

    function showTextBox2() {
      $('#box01').hide();
    }

  jQuery(document).ready(function() {
  
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        
        blog_category_title: {
          required: true
        },
        
        blog_category_description: {
          required: true,
        },
        blog_category_order: {
          required: true,
          integer: true,
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        
        blog_category_title: {
          required: 'Please enter title'
        },
        blog_category_description: {
          required: 'Please enter description'
        },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });

  function change_city() {
    var st_id = $("#artist_state").val();
    if (st_id!="") {
      $('#lodding_image').show();
      $.ajax({
       url:'{{url("demo-panel/get-city/change_city")}}'+'/'+st_id,  
       success:function(response){
      // alert(response);
       $('#lodding_image').hide();
       $('#artist_city').html(response);
       }
      });
    }
  }

  function change_pin() {
    var ct_id = $("#artist_city").val();
    if (ct_id!="") {
      $('#lodding_image').show();
      $.ajax({
       url:'{{url("demo-panel/get-pin/change_pin")}}'+'/'+ct_id,  
       success:function(response){
       //alert(response);
       $('#lodding_image').hide();
       $('#artist_pincode').html(response);
       }
      });
    }
  }

  </script>



@endsection
