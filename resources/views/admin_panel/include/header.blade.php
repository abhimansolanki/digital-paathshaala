<!DOCTYPE html>
<html>
@php $current_segment2 = Request::segment(2);$current_segment3 = Request::segment(3);
    $combine_segment = $current_segment2.'/'.$current_segment3;
@endphp
<head>
    <script>
        var BASE_URL = "{{ url('/') }}";
    </script>
    <meta charset="utf-8">
    <title>{!! trans('language.project_title') !!}</title>
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>
    <meta name="keywords" content="AdminPanel"/>
    <meta name="description" content="AdminPanel">
    <meta name="author" content="AdminPanel">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!!Html::style('public/admin/css/dashboard-admin.css') !!}
    {!!Html::style('public/admin/css/css.css') !!}
    {!!Html::style('public/admin/css/bootstrap-formhelpers.min.css') !!}
    {!!Html::style('public/admin/js/plugins/datatables/media/css/dataTables.bootstrap.css') !!}
    {!!Html::style('public/admin/js/plugins/datatables/media/css/dataTables.plugins.css') !!}
    {!!Html::style('public/admin/js/plugins/magnific/magnific-popup.css') !!}
    {!!Html::style('public/admin/css/admin-forms.css') !!}
    {!!Html::style('public/admin/js/plugins/select2/css/core.css') !!}
    {!!Html::style('public/admin/css/fonts/glyphicons-pro/glyphicons-pro.css') !!}
    {!!Html::style('public/admin/css/fonts/iconsweets/iconsweets.css') !!}
    {!!Html::style('public/admin/css/jquery-te-1.4.0.css') !!}
    {!!Html::style('public/admin/skin/default_skin/css/theme1.css') !!}
    {!!Html::style('public/admin/js/plugins/summernote/summernote.css') !!}
    {!!Html::style('public/admin/js/fullcalendar.min.css') !!}
    {!!Html::style('public/admin/plugins/select2/dist/css/select2.min.css') !!}
    {!!Html::style('public/css/dataTables.checkboxes.css') !!}
    {!!Html::style('public/css/jquery.multiselect.css') !!}
    {!!Html::script('public/js/jquery-3.3.1.min.js') !!}
    {!!Html::script('public/js/bootstrap-3.3.7.min.js') !!}
    {!!Html::style('public/admin/plugins/fancybox/source/jquery.fancybox.css') !!}


    {!!Html::script('public/admin/plugins/bootbox/bootbox.js') !!}
    {!!Html::script('public/admin/plugins/bootbox/bootbox.js') !!}
    {!!Html::script('public/admin/plugins/select2/dist/js/select2.min.js') !!}
    {!!Html::script('public/admin/plugins/ckeditor/ckeditor.js') !!}

    {{--        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">--}}
    {{--        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">--}}
    {{--        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
{{--    {!!Html::style('public/css/all.css') !!}--}}
    {!!Html::style('public/css/jquery-ui.css') !!}
    {!!Html::script('public/js/jquery-ui.js') !!}
    {!!Html::style('https://cdn.datatables.net/v/dt/dt-1.10.16/r-2.2.1/datatables.min.css') !!}
    {{--    {!!Html::style('public/font/font-awesome/all.min.css') !!}--}}
</head>
@include('backend.partials.popup')
<!--include modal of js in partial view-->
<body class="sb-l-o sb-l-m">
<!-- Start: Main -->
<div id="main">
    <!-- Start: Sidebar -->
    <aside id="sidebar_left" class="nano">
        <div class="">
            <!-- Start: Sidebar Menu -->
            <ul class="nav sidebar-menu">
                <li><a href="{{url('admin/dashboard')}}" title="Dashboard"><span
                                class="glyphicon glyphicon-home"></span></a></li>
                <li @if($current_segment2 == 'student-enquire' || $current_segment2 =='student-enquire-report' ) class="sidebar_active"
                    @else class="" @endif >
                    <a class="accordion-toggle" href="#">
                        <span class="glyphicons glyphicons-pen"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle acti" href="#">
                                Front Desk Enquiry
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="{{url('admin/student-enquire/add')}}"> Add Enquiry </a>
                                </li>
                                <li>
                                    <a href="{{url('admin/student-enquire/')}}"> View Enquiry </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- =================== -->
                <li @if($current_segment2 == 'student') class="sidebar_active" @else class="" @endif>
                    <a class="accordion-toggle" href="#">
                        <span class="glyphicon glyphicon-edit"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle" href="#">
                                Student Manager
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li><a href="{{url('admin/student/add')}}">Add Student </a></li>
                                <li><a href="{{url('admin/student/')}}">View Student </a></li>
                                <li><a href="{{url('admin/student-attendance/add')}}">Add Student Attendance </a></li>
                                <li><a href="{{url('admin/student-attendance-view')}}">View Student Attendance </a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- ================= -->
                <li
                        @if($current_segment2 == 'route' ||
                        $current_segment2 =='vehicle' ||
                        $current_segment2 =='student-vehicle-assigned')
                        class="sidebar_active" @else class="" @endif>
                    <a class="accordion-toggle" href="#">
                        <span class="glyphicons glyphicons-bus"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle" href="#">
                                Transport Manager
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.route')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li>
                                            <a href="{{url('admin/route/add')}}"><span
                                                        class=""></span>{!! trans('language.add_route') !!}</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/route')}}"><span
                                                        class=""></span>{!! trans('language.view_route') !!}</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.vehicle')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li>
                                            <a href="{{url('admin/vehicle/add')}}"><span
                                                        class=""></span>{!! trans('language.add_vehicle')!!}</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/vehicle/')}}"><span
                                                        class=""></span>{!! trans('language.view_vehicle')!!}</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.assign_vehicle')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li>
                                            <a href="{{url('admin/student-vehicle-assigned/add/')}}">Add Assign
                                                Vehicle</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/student-vehicle-assigned/')}}">View Assigned
                                                Vehicle</a>
                                        </li>
                                    </ul>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- ============ -->
                <!-- ======================== -->
                <li @if($current_segment2 == 'student-fee-receipt' || $current_segment2 == 'fee-type' || $current_segment2 == 'fee-circular' || $current_segment2 == 'fee-parameter') class="sidebar_active"
                    @else class="" @endif>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-rupee"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle" href="#">
                                Fee Manager
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
{{--                                <li>--}}
{{--                                    <a href="{{url('admin/student-fee-receipt/add')}}">{{trans('language.pay_fee')}}</a>--}}
{{--                                </li>--}}
                                <li>
                                    <a href="{{url('admin/student-fee-receipt-new/add')}}">Pay Fee</a>
                                </li>
                                <li>
                                    <a href="{{url('admin/student-transport-fee-receipt/add')}}">Pay Transport Fee</a>
                                </li>
                            <!--			    <li>
							<a class="accordion-toggle" href="#">{{trans('language.pay_fee')}}
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav">
                                    <li>
                                        <a href="{{url('admin/student-fee-receipt/add')}}">{{trans('language.fee_receipt')}}</a>
							    </li>
							    <li> 
							        <a href="{{url('admin/student/fee')}}">{{trans('language.view_fee')}}</a> 
							    </li>
							</ul>
						        </li>-->
                                <li>
                                    <a href="{{url('admin/fee-type')}}">{{trans('language.fee_type')}}</a>
                                </li>
                                <li>
                                    <a href="{{url('admin/fee-circular')}}">{{trans('language.fee_circular')}}</a>
                                </li>
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.fee_configuration')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li>
                                            <a href="{{url('admin/fee-parameter/add')}}">{{trans('language.add_fee_parameter')}}</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/fee-parameter')}}">{{trans('language.view_fee_parameter')}}</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!--  -->
                <li @if($current_segment2 == 'employee' || $current_segment2 =='shift' || $current_segment2 =='department') class="sidebar_active"
                    @else class="" @endif>
                    <a class="accordion-toggle" href="#">
                        <span class="glyphicons glyphicons-group"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle" href="#">
                                Staff Management
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a class="accordion-toggle" href="#">
                                        Employee
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li>
                                            <a href="{{url('admin/employee/add')}}">{{trans('language.add_employee')}}</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/employee')}}">{{trans('language.view_employee')}}</a>
                                        </li>
                                    </ul>
                                </li>

                            <!--                                        <li>
                                            <a href="{{url('admin/shift')}}">{{trans('language.shift')}}</a> 
                                        </li>-->
                                <li>
                                    <a href="{{url('admin/department')}}">{{trans('language.department')}}</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- ================= -->
                <li @if($current_segment2 == 'student-mark' || $current_segment2 == 'create-exam' || $current_segment2 == 'exam-type' || $current_segment2 == 'exam-category' || $current_segment2 == 'coscholastic' || $current_segment2 == 'class-coscholastic') class="sidebar_active"
                    @else class="" @endif>
                    <a class="accordion-toggle" href="#">
                        <span class="glyphicons glyphicons-book"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle" href="#">
                                Exam Manager
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <!--student-marks-->
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.marks')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li>
                                            <a href="{{url('admin/student-mark/add/')}}">{{trans('language.add_marks')}}</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/student-mark/')}}">{{trans('language.view_marks')}}</a>
                                        </li>
                                    </ul>
                                </li>
                                <!--create-exam-->
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.exam')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li>
                                            <a href="{{url('admin/create-exam/add')}}">{{trans('language.add_exam')}}</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/create-exam')}}">{{trans('language.view_exam')}}</a>
                                        </li>
                                    </ul>
                                </li>
                                <!--exam time table-->
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.exam_time_table')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li>
                                            <a href="{{url('admin/exam-time-table/add')}}">{{trans('language.add_exam_time_table')}}</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/exam-time-table')}}">{{trans('language.view_exam_time_table')}}</a>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="{{url('admin/exam-roll-no')}}">{{trans('language.exam_roll_no')}}</a>
                                </li>
                                <li>
                                    <a href="{{url('admin/exam-type')}}">{{trans('language.exam_type')}}</a>
                                </li>
                                <li>
                                    <a href="{{url('admin/exam-category')}}">{{trans('language.exam_category')}}</a>
                                </li>
                                <!--exam or daily syllabus-->
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.coscholastic')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li>
                                            <a href="{{url('admin/coscholastic')}}">{{trans('language.add_co_scholastic')}}</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/class-coscholastic')}}">{{trans('language.class_co_scholastic')}}</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- ======================  App manager-->
                <li @if($current_segment2 == 'app-manager' || $current_segment2 == 'event' || $current_segment2 == 'holiday' || $current_segment2 == 'school-gallery-album' || $current_segment2 == 'syllabus') class="sidebar_active" @else class="" @endif>
                    <a class="accordion-toggle" href="#">
                        <span class="glyphicons glyphicons-iphone"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle" href="#">
                                App Manager
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="{{url('admin/work-diary/add')}}">{{trans('language.add_work_diary')}}</a>
                                </li>
                                <li>
                                    <a href="{{url('admin/work-diary')}}">{{trans('language.view_work_diary')}}</a>
                                </li>
                                <li>
                                    <a href="{{url('admin/student-leave')}}">{{trans('language.view_student_leave')}}</a>
                                </li>
                                <li>
                                    <a href="{{url('admin/story')}}">{{trans('language.story')}}</a>
                                </li>
                                <li>
                                    <a href="{{url('admin/notice')}}">{{trans('language.notice')}}</a>
                                </li>
{{--                                <li>--}}
{{--                                    <a href="{{url('admin/student-progress')}}">{{trans('language.student_progress')}}</a>--}}
{{--                                </li>--}}
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.event')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li><a href="{{url('admin/event/add')}}">{{trans('language.add_event')}}</a>
                                        </li>
                                        <li><a href="{{url('admin/event')}}">{{trans('language.view_event')}}</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{url('admin/holiday')}}">{{trans('language.holiday')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/school-gallery-album') }}">Gallery</a>
                                </li>
                                <li>
                                    <a href="{{url('admin/e-book')}}">{{trans('language.ebook')}}</a>
                                </li>
                                <li>
                                    <a href="{{url('admin/e-class-room')}}">{{trans('language.e_class_room')}}</a>
                                </li>
                                <!--exam or daily syllabus-->
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.syllabus')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li>
                                            <a href="{{url('admin/syllabus/add')}}">{{trans('language.add_syllabus')}}</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/syllabus')}}">{{trans('language.view_syllabus')}}</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{url('admin/app-users')}}">App Users</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- ================== -->
                <!--All reports of project-->
                <!-- ================= -->
                <li @if($current_segment2=='student-fee-report' ||
		     $current_segment2=='student-transport-fee-report' || 
		     $current_segment2 =='attendance-register' || 
		     $current_segment2 =='student-detail') class="sidebar_active" @else class="" @endif>
                    <a class="accordion-toggle" href="#">
                        <span class="glyphicons glyphicons-file"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle" href="#">
                                Report
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.student')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li><a href="{{url('admin/student-detail')}}">Student Details</a></li>
                                        <li><a href="{{url('admin/student/sibling')}}">Student Sibling Details</a></li>
                                        <li><a href="{{url('admin/student-fee-report')}}">Student Fee Details</a></li>
                                        <li><a href="{{url('admin/transport-report')}}">Transport Details</a></li>
                                        <li><a href="{{url('admin/student-transport-fee-report')}}">Transport Fee
                                                Details</a></li>
                                        <li><a href="{{url('admin/student-enquire-report')}}">Student Enquiry</a></li>
                                        <li><a href="{{url('admin/attendance-register/month')}}">Attendance Register</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/student-promote')}}">{!! trans('language.promote_student') !!}</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/attendance-summary/month')}}">{!! trans('language.student_attendance_summary') !!}</a>
                                        </li>

                                        <li><a href="{{url('admin/route-report-classwise/')}}">Route Report Class
                                                wise</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.exam')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li><a href="{{url('admin/exam-report')}}">Exam Report</a></li>
                                        <li><a href="{{url('admin/exam-mark-sheet')}}">Exam Marksheet</a></li>
                                        <li><a href="{{url('/')}}">Exam Admin Card</a></li>
                                        <li><a href="{{url('/')}}">Exam Hole Report</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.transaction')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li><a href="{{url('admin/student/daybook')}}">DayBook Report</a></li>
                                        <li><a href="{{url('admin/student-due-fee')}}">Due Fee</a></li>
                                        <li><a href="{{url('admin/student-due-transport-fee')}}">Transport Due
                                                Report</a></li>
                                        <li><a href="{{url('admin/student/total-fee-outstanding')}}">Total OutStanding
                                                Report</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.misc')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li><a href="{{url('/')}}">ID Card</a></li>
                                        <li><a href="{{url('/')}}">Certificate</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- ================== -->
                <li @if($current_segment2 == 'tc-manager') class="sidebar_active" @else class="" @endif>
                    <a class="accordion-toggle" href="#">
                        <span class="glyphicons glyphicons-user_remove"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle" href="#">
                                TC Manager
                                <span class="caret"></span>
                            </a>

                            <ul class="nav sub-nav">
                                <li><a href="{{url('admin/student-tc/add')}}">Add Student TC </a></li>
                                <li><a href="{{url('admin/student-tc/')}}">View Student TC</a></li>
                                <li><a href="{{url('admin/student-left/')}}">Left Students</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <!-- =============== -->
                <li @if($current_segment2 == 'account-manager') class="sidebar_active" @else class="" @endif>
                    <a class="accordion-toggle" href="#">
                        <span class="glyphicons glyphicons-calculator"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle" href="#">
                                Account Manager
                                <span class="caret"></span>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- ================== -->
                <li @if($current_segment2 == 'session' || $current_segment2 == 'classes' || $current_segment2 == 'section' || $current_segment2 == 'subject'|| $current_segment2 == 'sms-template' ||  $current_segment2 == 'bank' || $current_segment2 == 'grade') class="sidebar_active"
                    @else class="" @endif>
                    <a class="accordion-toggle" href="#">
                        <span class="glyphicon glyphicon-cog"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle" href="#">
                                Configuration
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="{{url('admin/session/')}}">{{trans('language.session')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('admin/classes')}}">{{trans('language.class')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('admin/section')}}">{{trans('language.section')}}
                                    </a>
                                </li>
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.subject')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li>
                                            <a href="{{url('admin/subject')}}">{{trans('language.add_subject')}}</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/subject/view')}}">{{trans('language.view_subject')}}</a>
                                        </li>
                                    </ul>
                                </li>
                            <!--                                        <li>
							<a class="accordion-toggle" href="#">{{trans('language.sms_template')}}
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav">
                                    <li><a href="{{url('admin/sms-template/add/')}}">{{trans('language.add_sms_template')}}</a></li>
							    <li><a href="{{url('admin/sms-template')}}">{{trans('language.view_sms_template')}}</a></li>
							</ul>
						        </li>-->
                                <li>
                                    <a href="{{url('admin/bank')}}">{{trans('language.bank')}}
                                    </a>
                                </li>
                                <li>
                                    <a class="accordion-toggle" href="#">{{trans('language.room')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="nav sub-nav">
                                        <li><a href="{{url('admin/room/add')}}">{{trans('language.add_room')}}</a></li>
                                        <li><a href="{{url('admin/room')}}">{{trans('language.view_room')}}</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{url('admin/grade/')}}">{{trans('language.grade')}}
                                    </a>
                                </li>
                                <li>
                                    <a class="" href="{{url('admin/permission')}}">Permissions</a>
                                </li>
                                <li>
                                    <a class="" href="{{url('admin/user-role')}}">User Roles</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </li>
                <!-- ========================== -->
                <li @if($current_segment2 == 'password/change') class="sidebar_active" @else class="" @endif>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-user"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a href="{{ url('admin/school') }}">Profile</a>
                        </li>
                        <li>
                            <a href="{{ url('admin/password/change') }}">Change Password</a>
                        </li>
                        <li>
                            <a href="{{ url('admin/logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                Logout</a>
                        </li>

                    </ul>
                </li>
                <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul>
        </div>
    </aside>
    <section id="content_wrapper">
        <script type="text/javascript">
            $(document).ready(function () {
                $('.sidebar-menu li').click(function () {
                    $('.sidebar-menu li').removeClass("sidebar_active");
                    $(this).addClass("sidebar_active");
                });
            });
        </script>
