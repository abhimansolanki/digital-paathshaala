</section>
  </div>
</body>
  <footer id="content-footer" id="topbar" class="affix" >
    <div class="row">
      <div class="col-md-6">
        <span class="footer-legal"><b>©{{date('Y')}} : {{trans('language.project_title')}}</span></b>
      </div>
      <div class="col-md-2"></div>
      <div class="col-md-4">
        <span class="footer-legal"><b>Academic Session: {{ get_current_session()['session_year'] }} / Current Session  : {{ implode(" ",get_session()) }}</b></span>
      </div>
    </div>
  </footer>
</section>
 </div>   
 <style type="text/css">
  .dt-panelfooter{
    display: none ;
  }
 </style>
<!-- jQuery -->
{!!Html::script('public/admin/js/jquery/jquery-1.11.1.min.js') !!}
{!!Html::script('public/admin/js/jquery/jquery_ui/jquery-ui.min.js') !!}
{!!Html::script('public/admin/js/jquery-te-1.4.0.min.js') !!}
{!!Html::script('public/admin/js/admin-forms/js/jquery-ui-datepicker.min.js') !!}
{!!Html::script('public/admin/js/plugins/select2/select2.min.js') !!}
{!!Html::script('public/admin/js/plugins/magnific/jquery.magnific-popup.js') !!}
<!-- FileUpload JS -->
{!!Html::script('public/admin/js/plugins/fileupload/fileupload.js') !!}
{!!Html::script('public/admin/js/plugins/holder/holder.min.js') !!}
<!-- Datatables -->
{!!Html::script('public/admin/js/plugins/highcharts/highcharts.js') !!}
{!!Html::script('public/admin/js/plugins/circles/circles.js') !!}
<!--removed below widgets.js-->
<!--public/admin/js/plugins/widgets.js-->
{!!Html::script('public/admin/js/plugins/sparkline/jquery.sparkline.min.js') !!}
{!!Html::script('public/admin/js/plugins/jvectormap/jquery.jvectormap.min.js') !!}
{!!Html::script('public/admin/js/plugins/jvectormap/assets/jquery-jvectormap-us-lcc-en.js') !!}
{!!Html::script('public/admin/plugins/fancybox/source/jquery.fancybox.pack.js') !!}
<!-- Tagmanager JS -->
{!!Html::script('public/admin/js/plugins/tagsinput/tagsinput.min.js') !!}
{!!Html::script('public/admin/js/bootstrap-formhelpers.min.js') !!}
<!-- jQuery Validate Plugin-->
{!!Html::script('public/admin/js/admin-forms/js/jquery.validate.min.js') !!}
{!!Html::script('public/admin/js/admin-forms/js/additional-methods.min.js') !!}
<!-- Theme Javascript -->
{!!Html::script('public/admin/js/utility/utility.js') !!}
{!!Html::script('public/admin/js/demo/demo.js') !!}
{!!Html::script('public/admin/js/main.js') !!}
{!!Html::script('public/admin/js/app.js') !!}
{!!Html::script('public/admin/js/plugins/summernote/summernote.min.js') !!}
<!-- AdminForms JS -->
<!-- FullCalendar Plugin + Moment Dependency -->
{!!Html::script('public/admin/js/moment.min.js') !!}
{!!Html::script('public/admin/js/fullcalendar.min.js') !!}
{!!Html::script('public/js/jquery.multiselect.js') !!}
{!!Html::script('public/vendor/pnotify/pnotify.js') !!}

<!--<script src="https://code.jquery.com/jquery-3.3.1.js"></script>-->
<!--{!!Html::script('https://code.jquery.com/jquery-3.3.1.js') !!}-->
{!!Html::script('public/js/jquery.dataTables.min.js') !!}
<!--<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->
{!!Html::script('public/js/dataTables.buttons.min.js') !!}
<!--<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>-->

<!--<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>-->
{!!Html::script('public/js/jszip.min.js') !!}
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>-->

<!--for pdf-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>-->
{!!Html::script('public/js/vfs_fonts.js') !!}
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>-->

<!--for export-->
{!!Html::script('public/js/buttons.html5.min.js') !!}
<!--<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>-->
{!!Html::script('public/js/buttons.print.min.js') !!}
<!--<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>-->
<!--for seleted rows-->
{!!Html::script('public/js/dataTables.select.min.js') !!}
<!--<script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>-->


{!!Html::script('public/js/dataTables.checkboxes.min.js') !!}
<!--<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>-->

{!!Html::script('public/admin/js/plugins/datatables/media/js/dataTables.bootstrap.js') !!}
<!--<script src="https://cdn.rawgit.com/ashl1/datatables-rowsgroup/fbd569b8768155c7a9a62568e66a64115887d7d0/dataTables.rowsGroup.js"></script>-->

{{--{!!Html::script('public/font/font-awesome/all.min.js') !!}--}}

{{--{!!Html::script('https://cdn.datatables.net/v/dt/dt-1.10.16/r-2.2.1/datatables.min.js') !!}--}}

<script>
$(".select2-single").select2();
</script>
<script>
  $('.summernote').summernote({
      height: 255, //set editable area's height
      focus: false, //set focus editable area after Initialize summernote
      oninit: function() {},
      onChange: function(contents, $editable) {},
    });

    // Init Inline Summernote Plugin
    $('.summernote-edit').summernote({
      airMode: true,
      focus: false //set focus editable area after Initialize summernote            
    });

  $('.jqte-test').jqte();
  var jqteStatus = true;
  $(".status").click(function()
  {
    jqteStatus = jqteStatus ? false : true;
    $('.jqte-test').jqte({"status" : jqteStatus})
  });
</script>
<script>
$(document).ready(function() {
  $('img').on('dragstart', function(event) { event.preventDefault(); });
});
</script>
<script type="text/javascript">

$(function(){
  $('.numeric').on('input', function (event) { 
      this.value = this.value.replace(/[^0-9]/g, '');
  });
});

  jQuery(document).ready(function() {
    /* @time  picker
     ------------------------------------------------------------------ */
    $('.inline-tp').timepicker({
                    format: 'LT'
                }
    );

    $('#timepicker1').timepicker({
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });

    $('#timepicker2').timepicker({
      showOn: 'both',
      buttonText: '<i class="fa fa-clock-o"></i>',
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });

    $('#timepicker3').timepicker({
      showOn: 'both',
      disabled: true,
      buttonText: '<i class="fa fa-clock-o"></i>',
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });
    
    /* @date  time picker
    ------------------------------------------------------------------ */
    $('#datetimepicker1').datetimepicker({
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });
    
    $('#datetimepicker2').datetimepicker({
      showOn: 'both',
      buttonText: '<i class="fa fa-calendar-o"></i>',
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    }); 

    $( "#datepicker" ).datepicker( {
      dateFormat: 'd MM yy',
      buttonText: '<i class="fa fa-calendar-o"></i>',
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
    });
    $( "#datepicker1" ).datepicker( {
      dateFormat: 'd MM yy',
      buttonText: '<i class="fa fa-calendar-o"></i>',
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
    });

    $('#datetimepicker3').datetimepicker({
      showOn: 'both',
      buttonText: '<i class="fa fa-calendar-o"></i>',
      disabled: true,
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });

    $('.inline-dtp').datetimepicker({
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
    });

    /* @date  picker
    ------------------------------------------------------------------ */

    $(".date_picker").datepicker({
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      showButtonPanel: false,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: "dd/mm/yy",
      yearRange: "-10:+50"

    });


  $(".dateofbirthseprate").datepicker({
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      showButtonPanel: true,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: "dd/mm/yy",
      maxDate: "-2Y",
      minDate: "-50Y",
      yearRange: "-50:-2"

    });
    
     $(".date_picker1").datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: false,
      changeYear: true,
      prevText: '',
      nextText: '',
      showButtonPanel: false,
    
    });
  
    $("#datepicker1").datepicker({
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      showButtonPanel: false,
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });
    
    $("#datepicker").datepicker({
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      showButtonPanel: false,
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });

    $('#datepicker2').datepicker({
      numberOfMonths: 3,
      showOn: 'both',
      buttonText: '<i class="fa fa-calendar-o"></i>',
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });

    $('#datepicker3').datepicker({
      showOn: 'both',
      disabled: true,
      buttonText: '<i class="fa fa-calendar-o"></i>',
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });

    $('.inline-dp').datepicker({
      numberOfMonths: 1,
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      showButtonPanel: false
    });
  });
</script>
<script type="text/javascript">
jQuery(document).ready(function() {

"use strict";

// Init Theme Core    
Core.init();

// Init Demo JS    
Demo.init();  



// Init Boostrap Multiselects
$('#multiselect2').multiselect({
  includeSelectAllOption: true
});

// Init Boostrap Multiselects
$('#multiselect3').multiselect({
  includeSelectAllOption: true
}); 

// Init Boostrap Multiselects
$('#multiselect4').multiselect({
  includeSelectAllOption: true
}); 
 

// select dropdowns - placeholder like creation
    var selectList = $('.admin-form select');
    selectList.each(function(i, e) {
      $(e).on('change', function() {
        if ($(e).val() == "0") $(e).addClass("empty");
        else $(e).removeClass("empty")
      });
    });
    selectList.each(function(i, e) {
      $(e).change();
    });

    // Init tagsinput plugin
    $("input#tagsinput").tagsinput({
      tagClass: function(item) {
        return 'label label-default';
      }
    });

     // Init DataTables
    $('#datatable').dataTable({
      "sDom": 't<"dt-panelfooter clearfix"ip>',
    });

});
</script>
<script type="text/javascript">
// To delete Selected Records
function go_delete() {
    if($(".check:checked").length) {
        if(confirm("Are you sure to want delete selected records?")) {
            $("[name=form]").submit();
        }
    } else {
        alert("Please select atleast one record to delete");
    }
}
$(function() {
    $("#check_all").on("click", function() {
        $(".check").prop("checked",$(this).prop("checked"));
    });

    $(".check").on("click", function() {
        var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
        $("#check_all").prop("checked", flag);
    });
});

function selected() {
    if($(".check:checked").length) {
        $("[name=form]").submit();
    } else {
        alert("Please select atleast one record to download");
    }
}

function update_record() {
    if(confirm("Are you sure to want update records?")) {
      $("[name=form]").submit();    
    }
}

</script>
<script type="text/javascript">
  function generate_code() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for(var i = 0; i < 6; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    $("#discount_code").val(text);
  }





</script>
 <script type="text/javascript">
  jQuery(document).ready(function() {

    // Init FullCalendar external events
    $('#external-events .fc-event').each(function() {
      // store data so the calendar knows to render an event upon drop
      $(this).data('event', {
        title: $.trim($(this).text()), // use the element's text as the event title
        stick: true, // maintain when user navigates (see docs on the renderEvent method)
        className: 'fc-event-' + $(this).attr('data-event') // add a contextual class name from data attr
      });

      // make the event draggable using jQuery UI
      $(this).draggable({
        zIndex: 999,
        revert: true, // will cause the event to go back to its
        revertDuration: 0 //  original position after the drag
      });

    });

    var Calendar = $('#calendar');
    var Picker = $('.inline-mp');

    // Init FullCalendar Plugin
    Calendar.fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      editable: true,
      viewRender: function(view) {
        // If monthpicker has been init update its date on change
        if (Picker.hasClass('hasMonthpicker')) {
          var selectedDate = Calendar.fullCalendar('getDate');
          var formatted = moment(selectedDate, 'MM-DD-YYYY').format('MM/YY');
          Picker.monthpicker("setDate", formatted);
        }
        // Update mini calendar title
        var titleContainer = $('.fc-title-clone');
        if (!titleContainer.length) {
          return;
        }
        titleContainer.html(view.title);
      },
      droppable: true, // this allows things to be dropped onto the calendar
      drop: function() {
        // is the "remove after drop" checkbox checked?
        if (!$(this).hasClass('event-recurring')) {
          $(this).remove();
        }
      },
      eventRender: function(event, element) {
        // create event tooltip using bootstrap tooltips
        $(element).attr("data-original-title", event.title);
        $(element).tooltip({
          container: 'body',
          delay: {
            "show": 100,
            "hide": 200
          }
        });
        // create a tooltip auto close timer  
        $(element).on('show.bs.tooltip', function() {
          var autoClose = setTimeout(function() {
            $('.tooltip').fadeOut();
          }, 3500);
        });
      }
    });

    // Init MonthPicker Plugin and Link to Calendar
//    Picker.monthpicker({
//      prevText: '<i class="fa fa-chevron-left"></i>',
//      nextText: '<i class="fa fa-chevron-right"></i>',
//      showButtonPanel: false,
//      onSelect: function(selectedDate) {
//        var formatted = moment(selectedDate, 'MM/YYYY').format('MM/DD/YYYY');
//        Calendar.fullCalendar('gotoDate', formatted)
//      }
//    });


    // Init Calendar Modal via "inline" Magnific Popup
    $('#compose-event-btn').magnificPopup({
      removalDelay: 500, //delay removal by X to allow out-animation
      callbacks: {
        beforeOpen: function(e) {
          // we add a class to body indication overlay is active
          // We can use this to alter any elements such as form popups
          // that need a higher z-index to properly display in overlays
          $('body').addClass('mfp-bg-open');
          this.st.mainClass = this.st.el.attr('data-effect');
        },
        afterClose: function(e) {
          $('body').removeClass('mfp-bg-open');
        }
      },
      midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });

    // Calendar Modal Date picker
    $("#eventDate").datepicker({
      numberOfMonths: 1,
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      showButtonPanel: false,
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });
  });
</script>
</body>
</html>
