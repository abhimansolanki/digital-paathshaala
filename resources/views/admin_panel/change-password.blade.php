@extends('admin_panel/layout')
@section('content')
<!-- Start: Content-Wrapper -->
<section id="content_wrapper">
    <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center" style="height:100% !important;">
            <div class="mw1000 center-block">
                <div class="content-header">
                    <h2><b class="text-primary">Change your Password</b> </h2>
                    <p class="lead"></p>
                </div>
                <div class="panel panel-primary panel-border top mt20 mb35">
                    @include('backend.partials.messages')
                    <div class="panel-heading">
                        <span class="panel-title">Change Password</span>
                    </div>            
                    <div class="panel-body bg-light dark">
                        <div class="admin-form">
                            {!! Form::open(['url'=>url('admin/password-change'), 'enctype' => 'multipart/form-data' , 'id' => 'change-password-form' ]) !!}
                            <div class="col-md-12">
                                <div class="tab-content pn br-n admin-form">
                                    <div id="tab1_1" class="tab-pane active">
                                        <div class="section row" style="  margin-bottom: 17px;">
                                            <div class="col-md-12">  
                                                <label for="password" class="field prepend-icon">
                                                    {!! Form::password('password',array('class' => 'gui-input','placeholder' => 'Current Password' )) !!}
                                                    <label for="password" class="field-icon">
                                                        <i class="fa fa-key"></i>
                                                    </label>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="section row" style="  margin-bottom: 17px;">
                                            <div class="col-md-12">
                                                <label for="password_new" class="field prepend-icon">
                                                    {!! Form::password('password_new', array('class' => 'gui-input','id' =>'password_new','placeholder' => 'New Password' )) !!}
                                                    <label for="password_new" class="field-icon">
                                                        <i class="glyphicons glyphicons-keys"></i>
                                                    </label>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="section row" style="  margin-bottom: 17px;"> 
                                            <div class="col-md-12">
                                                <label for="password_confirm" class="field prepend-icon">
                                                    {!! Form::password('password_confirm',array('class' => 'gui-input','placeholder' => 'Confirm Password' )) !!}
                                                    <label for="password_confirm" class="field-icon">
                                                        <i class="glyphicons glyphicons-keys"></i>
                                                    </label>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <div class="input-group" style="float: right; width: 300px;  ">
                                        {!! Form::submit('Change Password', array('class' => 'button btn-primary btn-file btn-block ph5', 'id' => '')) !!}
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        jQuery(document).ready(function () {

            /* @custom validation method (smartCaptcha) 
             ------------------------------------------------------------------ */
            $("#change-password-form").validate({

                /* @validation states + elements 
                 ------------------------------------------- */

                errorClass: "state-error",
                validClass: "state-success",
                errorElement: "em",

                /* @validation rules 
                 ------------------------------------------ */

                rules: {
                    password: {
                        required: true
                    },
                    password_new: {
                        required: true,
                        minlength: 6,
                        maxlength: 16,
                        mypassword: true,
                    },
                    password_confirm: {
                        required: true,
                        minlength: 6,
                        maxlength: 16,
                        equalTo: '#password_new'
                    },
                },

                /* @validation error messages 
                 ---------------------------------------------- */

                messages: {
                    password: {
                        required: 'Please enter password'
                    },
                    password_new: {
                        required: 'Please enter a password',
                        mypassword: 'Password must include one uppercase letter and one alphanumeric character',
                        remote: 'New Password cannot be same as Old Password'
                    },
                    password_confirm: {
                        required: 'Please repeat the above password',
                        equalTo: 'Password mismatch detected'
                    },
                },

                /* @validation highlighting + error placement  
                 ---------------------------------------------------- */

                highlight: function (element, errorClass, validClass) {
                    $(element).closest('.field').addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).closest('.field').removeClass(errorClass).addClass(validClass);
                },
                errorPlacement: function (error, element) {
                    if (element.is(":radio") || element.is(":checkbox")) {
                        element.closest('.option-group').after(error);
                    } else {
                        error.insertAfter(element.parent());
                    }
                }

            });
            $.validator.addMethod('mypassword', function (value, element) {
                return this.optional(element) || (value.match(/[A-Z]/) && value.match(/[0-9]/));
            });
        });
    </script>
    @endsection
