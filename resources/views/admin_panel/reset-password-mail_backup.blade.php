@extends('admin_panel/layout')
@section('content')
<!-- Start: Content-Wrapper -->
<section id="content_wrapper">
    <header id="topbar">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="crumb-icon">
                    <a href="{{ url('/admin/dashboard') }}">
                        <span class="glyphicon glyphicon-home"></span>
                    </a>
                </li>
                <li class="crumb-trail">Forgot Password</li>
            </ol>
        </div>
    </header>
    <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center" style="height:100% !important;">
            <div class="mw1000 center-block">
                @include('backend.partials.messages')
                <div class="content-header">
                    <h2>Send Password Rest Link</h2>
                    <p class="lead"></p>
                </div>

                <div class="panel panel-primary panel-border top mt20 mb35">
                    <div class="panel-heading">
                        <span class="panel-title">Forgot Password</span>
                    </div>            
                    <div class="panel-body bg-light dark">
                        <div class="admin-form">
                            {!! Form::open(['url'=>'password/email','id' =>'reset-password-email']) !!}
                            <div class="col-md-12">
                                <div class="tab-content pn br-n admin-form">
                                    <div id="tab1_1" class="tab-pane active">
                                        <div class="section row" style="  margin-bottom: 17px;">
                                            <div class="col-md-12">  
                                                <label for="email" class="field prepend-icon">
                                                    {!! Form::email('email','', array('class' => 'gui-input','placeholder' => 'Email' )) !!}
                                                    <label for="email" class="field-icon">
                                                        <i class="fa fa-key"></i>
                                                    </label>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <div class="input-group" style="float: right; width: 300px;  ">
                                        {!! Form::submit('Send Password Reset Link', array('class' => 'button btn-primary btn-file btn-block ph5', 'id' => '')) !!}
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            /* @custom validation method (smartCaptcha) 
             ------------------------------------------------------------------ */
            $("#reset-password-email").validate({

                /* @validation states + elements 
                 ------------------------------------------- */
                errorClass: "state-error",
                validClass: "state-success",
                errorElement: "em",

                /* @validation rules 
                 ------------------------------------------ */

                rules: {
                    email: {
                        required: true
                    },
                },

                /* @validation error messages 
                 ---------------------------------------------- */

                messages: {
                    password: {
                        required: 'Please enter password'
                    },
                    password_new: {
                        required: 'Please enter a password'
                    },
                    password_confirm: {
                        required: 'Please repeat the above password',
                        equalTo: 'Password mismatch detected'
                    },
                },

                /* @validation highlighting + error placement  
                 ---------------------------------------------------- */

                highlight: function (element, errorClass, validClass) {
                    $(element).closest('.field').addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).closest('.field').removeClass(errorClass).addClass(validClass);
                },
                errorPlacement: function (error, element) {
                    if (element.is(":radio") || element.is(":checkbox")) {
                        element.closest('.option-group').after(error);
                    } else {
                        error.insertAfter(element.parent());
                    }
                }

            });
        });
    </script>
    @endsection
