<!DOCTYPE html>
<html>
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Login | AdminPanel</title>
    <meta name="keywords" content="AdminPanel" />
    <meta name="description" content="AdminPanel">
    <meta name="author" content="AdminPanel">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!!Html::style('public/admin/css/css.css') !!}
    {!!Html::style('public/admin/css/theme.css') !!}
    {!!Html::style('public/admin/css/fonts/glyphicons-pro/glyphicons-pro.css') !!}
  </head>
  <body class="external-page sb-l-c sb-r-c">
    <div id="main" class="animated fadeIn">
      <section id="content_wrapper">
        <section id="content">
          <div class="admin-form theme-info" id="login1">
            <div class="mt10 br-n">
              {!! Form::open(['url'=>'admin/login', 'id' => 'login_form']) !!}
              {{ csrf_field() }}
              <div class="row" id="loginis">
                <div class="col-sm-12 pr30">
                  <img src="{{ url('public/admin/img/loginlogo.png')}}" class="text-center">
                  @if (session('status'))
                  <div class="alert alert-danger">
                    {{ session('status') }}
                  </div>
                  @endif
                  @if (count($errors)) 
                  <div class="alert alert-danger">
                    @foreach($errors->all() as $error) 
                    <p>{{ $error }}</p>
                    @endforeach 
                  </div>
                  @endif
                  <div class="section">
                    {!! Form::email('email', '', array('class' => 'gui-input','placeholder' => 'Enter Email Address', 'required' => 'required'  )) !!}
                  </div>
                  <div class="section">
                    {!! Form::password('password', array('class' => 'gui-input','placeholder' => 'Enter Password','required' => 'required' )) !!}
                  </div>
                  <input type="submit" class="loginClass12" name="login" id="login" value="Login"/>
{{--                  <a href="{{ url('password/reset') }}" class="forgot_textss" title="Forgot Password?"><span>Forgot Password?</span></a>--}}
                  <a href="{{route("privacy_policy")}}" class="forgot_textss" title="Privacy Policy"><span>Privacy Policy</span></a>
                </div>
              </div>
              {!! Form::close() !!}
            </div>
          </div>
        </section>
      </section>
    </div>
  </body>
</html>
