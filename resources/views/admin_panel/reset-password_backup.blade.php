@extends('admin_panel/layout')
@section('content')
  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="{{ url('/admin/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-trail">Reset Password</li>
          </ol>
        </div>
      </header>
      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center" style="height:100% !important;">
          <div class="mw1000 center-block">
            <div class="content-header">
              <h2> Please fillup below form to  <b class="text-primary">Reset your Password</b> </h2>
              <p class="lead"></p>
            </div>

            <div class="panel panel-primary panel-border top mt20 mb35">
                @include('backend.partials.messages')
              <div class="panel-heading">
                <span class="panel-title">Reset Password</span>
              </div>            
              <div class="panel-body bg-light dark">
                <div class="admin-form">
                  {!! Form::open(['url'=>url('password/reset'),'id' => 'password-reset' ]) !!}
                  <input type="hidden" name="token" value="{{ $token }}">
                    <div class="col-md-12">
                      <div class="tab-content pn br-n admin-form">
                        <div id="tab1_1" class="tab-pane active">
                            <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">
                              <label for="email" class="field prepend-icon">
                                {!! Form::email('email','', array('class' => 'gui-input','id' =>'email','placeholder' => 'Email' )) !!}
                                <label for="email" class="field-icon">
                                  <i class="glyphicons glyphicons-keys"></i>
                                </label>
                              </label>
                            </div>
                          </div>
                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">  
                              <label for="password" class="field prepend-icon">
                                {!! Form::password('password', array('class' => 'gui-input','placeholder' => 'Password','id'=>'password' )) !!}
                                <label for="password" class="field-icon">
                                  <i class="fa fa-key"></i>
                                </label>
                              </label>
                            </div>
                          </div>

                          <div class="section row" style="  margin-bottom: 17px;"> 
                            <div class="col-md-12">
                              <label for="password_confirmation" class="field prepend-icon">
                                {!! Form::password('password_confirmation', array('class' => 'gui-input','placeholder' => 'Confirm Password' )) !!}
                                <label for="password_confirmation" class="field-icon">
                                  <i class="glyphicons glyphicons-keys"></i>
                                </label>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>
                    <div class="form-group">
                    <div class="col-lg-12">
                      <div class="input-group" style="float: right; width: 300px;  ">
                      {!! Form::submit('Reset Password', array('class' => 'button btn-primary btn-file btn-block ph5', 'id' => '')) !!}
                      </div>
                    </div>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
<script type="text/javascript">
  jQuery(document).ready(function() {
  
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#password-reset").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        email: {
          required: true
        },
        password: {
          required: true,
          minlength: 6,
          maxlength: 16
        },
        password_confirmation: {
          required: true,
          minlength: 6,
          maxlength: 16,
          equalTo: '#password'
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        password: {
          required: 'Please enter password'
        },
        email: {
          required: 'Please enter a password'
        },
        password_confirmation: {
          required: 'Please repeat the above password',
          equalTo: 'Password mismatch detected'
        },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>





@endsection
