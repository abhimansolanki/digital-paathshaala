<!DOCTYPE html>
<html>
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Login | AdminPanel</title>
    <meta name="keywords" content="AdminPanel" />
    <meta name="description" content="AdminPanel">
    <meta name="author" content="AdminPanel">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!!Html::style('public/admin/css/css.css') !!}
    {!!Html::style('public/admin/css/theme.css') !!}
    {!!Html::style('public/admin/css/fonts/glyphicons-pro/glyphicons-pro.css') !!}
  </head>
  <body class="external-page sb-l-c sb-r-c">
    <div id="main" class="animated fadeIn">
      <section id="content_wrapper">
        <section id="content">
          <div class="admin-form theme-info" id="login1">
            <div class="mt10 br-n">
              {!! Form::open(['url'=>url('password/reset'),'id' => 'password-reset' ]) !!}
              
              <input type="hidden" name="token" value="{{$token}}">
              
              <div class="row" id="loginis">
                <div class="col-sm-12 pr30">
                  <img src="{{ url('public/admin/img/loginlogo.png')}}" class="text-center">
                  @if(Session::has('success') && !empty(session('status')))
                      <div class="alert alert-success">
                    {{ session('status') }}
                  </div>
                  @endif
                 @if(Session::has('error') && !empty(session('status')))
                  <div class="alert alert-danger">
                    {{ session('status') }}
                  </div>
                  @endif
                  @if (count($errors)) 
                  <div class="alert alert-danger">
                    @foreach($errors->all() as $error) 
                    <p>{{ $error }}</p>
                    @endforeach 
                  </div>
                  @endif
                  <div class="section">
                    {!! Form::hidden('email',urldecode($email), array('class' => 'gui-input','id' =>'email','placeholder' => 'Email','readonly'=>true )) !!}
                  </div>
                  <div class="section">
                    {!! Form::password('password', array('class' => 'gui-input','placeholder' => 'Password','id'=>'password' )) !!}
                  </div>
                  <div class="section">
                    {!! Form::password('password_confirmation', array('class' => 'gui-input','placeholder' => 'Confirm Password' )) !!}
                  </div>
                  {!! Form::submit('Reset Password', array('class' =>'loginClass12','id' => '','style'=>'font-size:15px !important')) !!}
                  <a href="{{ url('admin/login') }}" class="forgot_textss" title="Login"><span>Login</span></a>
                </div>
              </div>
              {!! Form::close() !!}
            </div>
          </div>
        </section>
      </section>
    </div>
  </body>
</html>
<script type="text/javascript">
  jQuery(document).ready(function() {
  
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#password-reset").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
//        email: {
//          required: true
//        },
        password: {
          required: true,
          minlength: 6,
          maxlength: 16
        },
        password_confirmation: {
          required: true,
          minlength: 6,
          maxlength: 16,
          equalTo: '#password'
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        password: {
          required: 'Please enter password'
        },
        email: {
          required: 'Please enter a password'
        },
        password_confirmation: {
          required: 'Please repeat the above password',
          equalTo: 'Password mismatch detected'
        },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>
