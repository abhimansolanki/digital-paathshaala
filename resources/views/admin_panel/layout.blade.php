<!-- Start : Header -->  

    @include('admin_panel/include/header')

<!-- End : Header -->	


<!-- Start: Menu -->

    @include('admin_panel/include/sidebar')

<!-- End: Menu -->

@yield('content')

<!-- Start: Footer -->
  
    @include('admin_panel/include/footer')

<!-- End: Footer -->