@if(!empty($arr_student_list))
@foreach($arr_student_list as $student_list)
@php $student_id = $student_list['student_id'];@endphp
<tr class="" id="assign_{{$student_id}}">
    <td class="text-center assign-student-name"><span class="">{{$student_list['student_name']}}</span> </td>
    <td class="text-center assign-student-eno">{{$student_list['enrollment_number']}}</td>
    <td>
        {!! Form::button('<i class="fa fa-check check-btn"></i>',['class'=>'checked_student assign-student','action-type'=>'assign','rel'=>$student_id]) !!}
    </td>
</tr>
@endforeach
@else
@endif


