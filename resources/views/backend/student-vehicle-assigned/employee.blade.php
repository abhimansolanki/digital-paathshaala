<style type="text/css">
   #employee table tr td{
        border: 0px !important;
    }
    #employee table {
        border: 0px !important;
    }
    #employee table tr{
        border: 0px !important;
    }
    #employee table th{
        border: 0px;
        border-top: 1px solid #ccc !important;
     }
    #employee table th:first-child{
        border-right: 1px solid #ccc !important;
     }
    #employee  table th:nth-child(2){
        border-right: 1px solid #ccc !important;
     }
    #employee  table tr td:first-child{
        border-right: 1px solid #ccc !important;
     }
    #employee  table tr td:nth-child(2){
        border-right: 1px solid #ccc !important;
     }
</style>

<table class="table table-striped" style="color: #231a1a;" class="table-resposive" id="employee">
    @if(!empty($arr_employee_list))
    <thead>
        <tr>
            <th class="text-left">Select</th>
            <th class="text-center">{{trans('language.employee_name')}}</th>
            <th class="text-center">{{trans('language.employee_code')}}</th>
        </tr>
    </thead>
    <tbody>
        @php $vehicle_assigned_id = array_flip($vehicle_assigned_employee_id); @endphp
        @foreach($arr_employee_list as $employee_list)
        @php $employee_id = $employee_list['employee_id'];
        $checked = in_array($employee_id,$vehicle_assigned_employee_id) ? true : false;
        $assigned_detail_id = isset($vehicle_assigned_id[$employee_id]) ? $vehicle_assigned_id[$employee_id] : null; 
        @endphp

        <tr class="">
            <td>
                {!! Form::checkbox('employee_id[]',$employee_id,$checked,['class'=>'employee vehicle-assign assigned_'.$assigned_detail_id,'id'=>'employee_id','assigned-detail-id'=>$assigned_detail_id]) !!}
                {!! Form::hidden('employee_assigned_detail_id_'.$employee_id,$assigned_detail_id,['class'=>'employee','id'=>'assigned_detail_id','readonly'=>true]) !!}
            </td>
            <td class="text-center"><span class="">{{$employee_list['full_name']}}</span> </td>
            <td class="text-center">{{$employee_list['employee_code']}}</td>
        </tr>
        @endforeach
    </tbody>
        @else
    <tbody>
        <tr colspan="3">
            <td style="text-align: center">No Staff available</td>
        </tr>
    </tbody>
@endif
</table>


