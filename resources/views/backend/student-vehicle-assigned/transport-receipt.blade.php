<!doctype html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Transport Information</title>
    </head>
    <style type="text/css">
        body{
            font-size: 12px;
            margin-top: 20px;
        }
        .addtext{
            background: #000;
            width: 50%;
            text-align: left;
            padding: 8px 0px;
            color: #fff;
            text-transform: uppercase;
            position: relative;
            float: right;
            margin-top: 0px;
            padding-left: 30px;
        }
        .studentClass{
            float: right;
            padding: 5px;
            border: 1px solid;
            height: 120px;
            width: 100px;
            z-index: 999999;
            position: absolute;
            background: #ffff;
            right: 0px;
            top: -2%;
        }
        .logo{
            float: left;
            width: 20%;
            margin-top: -30px;
        }
        .addmiNo{
            width:50%;
            float: right;
            margin-top: -82px;
        }
        .foemno{
            float: left;
            width: 70px;
        }
        .inderform{
            float: right;
            width: 140px;
            margin-right: 20px;
            border-bottom: 1px dotted #000;
            text-align: left;
        }
        .admisdate{
            float: right;
            width:220px;
            border-bottom: 1px dotted #000;
            text-align: left;
            margin-right: 20px;
        }
        .admisdate1{
            float: left;
            width:120px;
        }
        .stduenamelabel{
            float: left;
            width: 12%;
            padding-top: 2px;
            font-weight: bold;
        }
        .stduenamelabel345{
            float: left;
            width: 16%;
            padding-top: 2px;
            font-weight: bold; 
        }
        .stduenamelabel3{
            float: left;
            width: 11%;
            padding-top: 2px;
            font-weight: bold;
        }
        .stduenamelabel32{
            float: left;
            width: 20%;
            padding-top: 2px;
            font-weight: bold; 
        }
        .stduenamelabel323{
            float: left;
            width: 15%;
            padding-top: 2px;
            font-weight: bold;
        }
        .stduenName1{
            float: left;
            border-bottom: 1px dotted #000;
            width: 78%;
            margin-left: 2%;
            padding-bottom: 5px;
        }
        .addresss{
            float: left;
            border-bottom: 1px dotted #000;
            width: 79.5%;
            margin-left: 2%;
            padding-bottom: 5px;  
        }
        .stduenName{
            float: left;
            border-bottom: 1px dotted #000;
            width: 86%;
            margin-left: 2%;
            padding-bottom: 5px;
        }
        .genderarea{
            float: left;
            border:1px solid;
            width: 50%;
        }
        .Vehicle{
            float: left;
            width:10%;
            padding-top: 2px;
            font-weight: bold;  
        }
        .genderlabel35623{
            float: left;
            width:15%;
            padding-top: 2px;
            font-weight: bold;
        }
        .genderlabel356{
            float: left;
            width:6%;
            padding-top: 2px;
            font-weight: bold;
        }
        .genderlabel3568{
            float: left;
            margin-left: 10px;
            width:11%;
            padding-top: 2px;
            font-weight: bold;
        }
        .attacment{
            float: left;
            width:18%;
            padding-top: 2px;
            font-weight: bold;
        }
        .lastone{
            float: left;
            width:10%;
            padding-top: 2px;
            font-weight: bold;
        }
        .genderlabel{
            float: left;
            width: 12%;
            padding-top: 2px;
            font-weight: bold;
        }
        .genderlabel1{
            float: left;
            width: 9%;
            padding-top: 2px;
            font-weight: bold;
        }
        .gengerClass2{
            float: left;
            border-bottom: 1px dotted #000;
            width: 36%;
            margin-left: 2%;
            padding-bottom: 5px; 
            margin-right: 10px; 
        }
        .Signature{
            float: left;
            width: 30%;
            text-align: center;
            margin: 0px 20px;
        }
        .Signature2{
            width:70%;
            text-align: center;
            margin: 0px 20px;
        }
        .checkedby{
            float: left;
            margin-top: 15px;
            border-bottom: 1px solid #000;
            width: 20%;
            padding-bottom: 5px; 
        }
        .studneclasss{
            float: left;
            border-bottom: 1px dotted #000;
            width: 19%;
            margin-left: 2%;
            margin-right: 10px;
            padding-bottom: 5px; 
        }
        .gengerClass1265{
            float: left;
            border-bottom: 1px dotted #000;
            width:33.8%;
            margin-right: 10px;
            margin-left: 2%;
            padding-bottom: 5px; 
        }
        .gengerClass{
            float: left;
            border-bottom: 1px dotted #000;
            width: 36.5%;
            margin-right: 10px;
            margin-left: 2%;
            padding-bottom: 5px;  
        }
        .gengerClass235{
            float: left;
            margin-right: 10px;
            border-bottom: 1px dotted #000;
            width:34.6%;
            margin-left: 2%;
            padding-bottom: 5px; 
        }
        .genderlabel2{
            float: left;
            width: 13%;
            padding-top: 2px;
            font-weight: bold;
        }
        .gengerClass1{
            float: left;
            border-bottom: 1px dotted #000;
            width: 33.8%;
            margin-left: 0px;
            padding-bottom: 5px;  
        }
        .gengerClass1235{
            float: left;
            border-bottom: 1px dotted #000;
            width:20%;
            margin-left: 2%;
            padding-bottom: 5px; 
            margin-right: 10px;
        }
        .gengerClass12352{
            float: left;
            margin-right: 10px;
            border-bottom: 1px dotted #000;
            width: 17.5%;
            margin-left: 2%;
            padding-bottom: 5px;    
        }
        .declare{
            font-size: 12px;
            float: left;
            width: 90%;
            line-height: 22px;
            padding-bottom: 15px;
        }
        .centerdive{
            position: relative;
            top: -20px;
            width: 200px;
            background: #000;
            color: #fff;
            padding: 6px 0px;
            left: 35%;
            right: 35%;
            text-align: center;
        }
        .dateoflabel{
            float: right;
        }
        .dateofdate{
            float: right;
            border-bottom: 1px dotted #000;
            padding-bottom: 5px;
            width: 20%;
            margin-left: 10px;
        }
        .footerstudent{
            float: left;
        }
        .footerright{
            float: left;
            border-bottom: 1px dotted #000;
            width: 46%;
            margin-left: 10px;
            margin-top: 12px;
            margin-right: 10px;
        }
        .psign{
            float: left;
            border-top: 1px solid #000;
            width: 31.2%;
            margin: 10px 10px;
            text-align: center;
            padding-top: 10px;
            font-weight: bold;
        }
    </style>
    @if(!empty($transport_receipt))
    <body>
        <div class="logo">
            <img src="../public/css/logo.png" width="200">
        </div>
        <div class="addtext">
            <h3 style="padding: 0px; margin: 0px;">Transport Information</h3>
        </div>
        <div style="clear: both;"></div>
        <br>
        
        <div class="addmiNo">
            <br>
            <div class="foemno"> <label style="font-weight: bold;">Branch :</label></div>
            <div class="admisdate">{{$school_data['school_name'] ?? ''}}</div>
            <div style="clear: both;"></div>
            <br>
            <div  class="admisdate1" > <label style="font-weight: bold;">Tel No.:</label></div>
            <div  class="admisdate" >{{$school_data['school_contact_number'] ?? ''}}</div>
            <div style="clear: both;"></div>
            <br>
            <div  class="admisdate1" > <label style="font-weight: bold;">Transport Manager :</label></div>
            <div  class="admisdate">Jodhpur, Rajasthan</div>
            <div style="clear: both;"></div>
            <br>
            <div  class="admisdate1" > <label style="font-weight: bold;">Contact No. :</label></div>
            <div  class="admisdate" >{{$school_data['school_mobile_number'] ?? ''}}</div>
            <div style="clear: both;"></div>
        </div>
        <div style="clear: both;"></div>
        <!--  <div class="studentClass">
            <img src="../public/css/st.jpg" style="width: 100%" height="100%;">
            </div>
            <div style="clear: both;"></div> -->
        <br>
        <hr style="border:1px solid">
        <br>
        <div class="stduenamelabel">
            Admission No. 
        </div>
        <div class="stduenName">
            {{$transport_receipt['enrollment_number']}}
        </div>
        <div style="clear: both;"></div>
        <br>
        <div class="stduenamelabel">
            Child Name :  
        </div>
        <div class="stduenName">
            {{$transport_receipt['student_name']}}   
        </div>
        <div style="clear: both;"></div>
        <br>
        <div class="genderlabel">
            Father Name:
        </div>
        <div class="gengerClass">
            {{$transport_receipt['father_name']}}   
        </div>
        <div class="genderlabel1">Phone No. :</div>
        <div class="gengerClass">{{$transport_receipt['father_contact_number']}}</div>
        <div style="clear: both;"></div>
        <br>
        <div class="attacment">
            Local Address :
        </div>
        <div class="addresss">
            {{$transport_receipt['address_line1']}}
        </div>
        <div style="clear: both;"></div>
        <br>
        <div class="attacment">
            Permanent Address :
        </div>
        <div class="addresss">
            {{$transport_receipt['address_line2']}}
        </div>
        <div style="clear: both;"></div>
        <br>
        <div class="genderlabel">Driver Name:</div>
        <div class="studneclasss">{{$transport_receipt['driver_full_name']}}</div>
        <div class="genderlabel3568">Phone No. :</div>
        <div class="gengerClass12352">{{$transport_receipt['driver_contact_number']}}</div>
        <div class="genderlabel3568">Helper Name :</div>
        <div class="gengerClass12352">{{$transport_receipt['helper_full_name']}}</div>
        <div style="clear: both;"></div>
        <br>

        <div class="Vehicle">Vehicle No. :</div>
        <div class="gengerClass1235">{{$transport_receipt['vehicle_number']}}</div>
        <div class="genderlabel35623">Color Of Vehicle :</div>
        <div class="gengerClass1235">{{$transport_receipt['vehicle_color']}}</div>
        <div style="clear: both;"></div>
        <br>
        <div class="genderlabel">Route :
        </div>
        <div class="gengerClass1265">{{$transport_receipt['route']}} ({{$transport_receipt['stop_point']}})</div>
        <div class="genderlabel35623">Date of Starting :</div>
        <div class="gengerClass1265">{{$transport_receipt['join_date']}}</div>
        <div style="clear: both;"></div>
        <br>
        <div class="genderlabel">Pick Time :</div>
        <div class="gengerClass1265">{{$transport_receipt['pickup_time']}}</div>
        <div class="genderlabel35623">Drop Time :</div>
        <div class="gengerClass1265">{{$transport_receipt['return_time']}}</div>
        <div style="clear: both;"></div>
        <br>
        <br>  <br>
        <div class="psign">Administrator's Sign</div>
        <div class="psign">Tpt In charge Sign</div>
        <div class="psign">Parent's Sign</div>
        <br>  <br>
        <br>
        <br>
        <hr>
        <br>
        <div class="stduenamelabel">
            Note :
        </div>
        <div class="declare">
            1. I have gone through the rules and regulation unmentioned in the school hand book and i agree to abide by.<br>
            2.  the same with instruction issued from time to time by the school authorities the date of birth mentioned
        </div>
        <div style="clear: both;"></div>
        <br>
    </body>
    @endif    
</html>
