@extends('admin_panel/layout')
@section('content')
<style>
    td.details-control {
        background: url(<?php echo url('/public/details_open.png'); ?>) no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url(<?php echo url('/public/details_close.png'); ?>) no-repeat center center;
    }
    table, th, td {
        border: 1px solid #eeeeee;
    }
    table {
        width: 100%;
        margin-bottom: 20px;
    }  
</style>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_vehicle_assign')}}</span>
                </div>
                <div class="topbar-right">
                    <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right custom-btn">{{trans('language.add_vehicle_assign')}}</a>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-vehicle-assigned-table" cellspacing="0" width="100%"> 
                    <thead>
                        <tr>
		    <th></th>
                            <th>{{trans('language.vehicle_number')}}</th>
                            <th>{{trans('language.stop')}}</th>
                            <th>{{trans('language.fair')}}</th>
                            <th>{{trans('language.pickup_time')}}</th>
                            <th>{{trans('language.return_time')}}</th>
                            <th class="text-center">{{trans('language.action')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#student-vehicle-assigned-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('student-vehicle-assigned/data')}}",
            columns: [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',

                },
                {data: 'vehicle_number', name: 'vehicle_number'},
                {data: 'stop_point', name: 'stop_point'},
                {data: 'stop_fair', name: 'stop_fair'},
                {data: 'pickup_time', name: 'pickup_time'},
                {data: 'return_time', name: 'return_time'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

// detail of assigned students/employee

        $('#student-vehicle-assigned-table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
//            console.log(row.data());
                // Open this row
                var child = format(row.data());
//            console.log(child);
                if (child !== '')
                {
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            }
        });

        function format(d) {
    console.log(d);
            // `d` is the original data object for the row
            var assigned_data = d.assigned_detail;
            var tab_body = '';
            var tab_head = '<table cellpadding="5" cellspacing="0" border="0" style="margin-left: -4px; font-size:11px !important;">';
            tab_head = tab_head + '<tr><th></th><th style="width:24%;">Type</th><th style="width:12%;">Code</th>' +
                    '<th>Name</th>' +
                    '<th>Assigned Date</th>' +
                    '<th>Unassigned Date</th>' +
                    '<th>Status</th>' +
                    '</tr>';
            var count = 1;
            $.each(assigned_data, function (key, value)
            {
                console.log(value.type);
                var name = '';
                var code = '';
                if (value.assigned_to == '1')
                {
                    name = value.student_name;
                    code = value.enrollment_number;
                } else
                {
                    name = value.employee_name;
                    code = value.employee_code;
                }
                tab_body = tab_body + '<tr><td>' + count + '</td>' +
                        '<td>' + value.type + '</td><td>' + code + '</td>' +
                        '<td>' + name + '</td>' +
                        '<td>' + value.join_date + '</td>' +
                        '<td>' + value.stop_date + '</td>' +
                        '<td>' + value.status + '</td>' +
                        '</tr>';

                count++;
            });
            var tab_foot = '</table>';
            var table_data = '';
            if (tab_body !== '')
            {
                table_data = tab_head + tab_body + tab_foot;
            }
            return table_data;
        }

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('student-vehicle-assigned/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'student_vehicle_assigned_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();
                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
    });
</script>
</body>
</html>
@endsection




