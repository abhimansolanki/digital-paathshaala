<style type="text/css">
    .form-horizontal .checkbox, .form-horizontal .radio{
        min-height: 21px !important;
    }
    .total_mar {
        width: 100%;
    }

    #accordionpanel1 {
        padding: 0px !important;
        border-radius: 0px;
    }

    .accordionpanel {
        background: #f9f9f9 !important;
        width: 100%;
        display: block;
        padding: 10px 8px;
        font-weight: bold;
        text-transform: capitalize;
        font-size: 12px;
    }

    .accordionpanel label {
        color: #000;
    }

    /*    .classopen {
            display: none;
        }*/

    table tr td {
        text-transform: capitalize;
    }

    #accordion2 .panel {
        padding: 0px 0px;
    }
    #dispaly_student table{
        width: 100% !important;
        border: 1px solid #dddddd;
        margin: 0px 0px;
    }
    .accordionpanel{

    }
    .panel #spy1{
        margin: 15px 0px;
    }
    #pnl{
        margin: 15px 15px;
    }
    .ui-datepicker-calendar table th:first-child{
        border-right:none !important;
    }
    .state-error{
        display:block !important;
        margin-top: 6px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    }
    .joined_date{
        height: 25px !important;
        width: 80% !important;
        margin:5px  5px 5px 10px !important;
    }
    .unassigned_date{
        height: 25px !important;
        width: 80% !important;
        margin:5px  5px 5px 10px !important;
    }
    .table > thead > tr > th{
        padding:8px !important;
    }
    .table > tbody > tr > td{
        padding:0px !important;
    }
    .assign-student,.assigned-student{
        margin-left: 10px !important;
    }
    #student-table table tr td{
        border: 0px !important;
    }
    #student-table  table {
        border: 0px !important;
    }
    #student-table table tr{
        border: 0px !important;
    }
    #student-table table th{
        border: 0px;
    }
    #student-table table th:first-child{
        border-right: 1px solid #ccc !important;
    }
    #student-table  table th:nth-child(2){
        border-right: 1px solid #ccc !important;
    }
    #student-table  table tr td:first-child{
        border-right: 1px solid #ccc !important;
    }
    #student-table table tr td:nth-child(2){
        border-right: 1px solid #ccc !important;
    }
    .join_date{
        height: 25px !important;
        width: 70% !important;
        margin:5px  5px 5px 22px !important;
    }
    .check-btn{
        color:green;
    }
    .uncheck-btn{
        color:red;
    }
    .admin-form button{
        background-color: transparent;
        border: none;
    }
</style>

<div class="admin-form theme-primary mw10000 center-block" style="padding-bottom: 175px;" id="CustomInput">
    @include('backend.partials.loader')
    <div class="panel heading-border" id="pnl">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}">Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_vehicle_assign')}}</li>
                </ol>
            </div>
            <div class="topbar-right">
                <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right">{{trans('language.view_vehicle_assign')}}</a>
            </div>
        </div>
        <!--@include('backend.partials.messages')-->
        <div class="bg-light panelBodyy">
            <div class="section-divider mv40">
                <span style="padding-left: 9px !important;"><i class="fa fa-bus"></i> &nbsp;Assign Vehicle</span>
            </div>
            {!! Form::hidden('student_vehicle_assigned_id',isset($vehicle_assigned['student_vehicle_assigned_id']) ? $vehicle_assigned['student_vehicle_assigned_id'] : '',['class' => 'gui-input', 'id' => 'student_vehicle_assigned_id', 'readonly' => 'true']) !!}
            {!! Form::hidden('input_hidden_field','',['class' => 'gui-input', 'id' => 'input_hidden_field', 'readonly' => 'true']) !!}
            <div class=" row" id="spy1">
                <div class="col-md-3">
                    <label>
                        <span class="radioBtnpan">{{trans('language.vehicle')}}<span class="asterisk">*</span></span>
                    </label>
                    <label class="field select">
                        {!!Form::select('vehicle_id', $vehicle_assigned['arr_vehicle'],isset($vehicle_assigned['vehicle_id']) ? $vehicle_assigned['vehicle_id'] : '', ['class' => 'form-control','id'=>'vehicle_id'])!!}
                    </label>
                    @if ($errors->has('vehicle_id')) <p class="help-block">{{ $errors->first('vehicle_id') }}</p> @endif                            
                </div>
                <div class="col-md-3">
                    <label>
                        <span class="radioBtnpan">{{trans('language.stop')}}<span class="asterisk">*</span></span>
                    </label>
                    <label for="stop_point_id" class="field select">
                        {!!Form::select('stop_point_id', $vehicle_assigned['arr_route_stop'],isset($vehicle_assigned['stop_point_id']) ? $vehicle_assigned['stop_point_id'] : '', ['class' => 'form-control','id'=>'stop_point_id'])!!}
                    </label>
                    @if ($errors->has('stop_point_id')) <p class="help-block">{{ $errors->first('stop_point_id') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.fair')}}<span class="asterisk">*</span></span></label>
                    <label for="stop_fair" class="field prepend-icon">
                        {!! Form::text('stop_fair', old('stop_fair',isset($vehicle_assigned['stop_fair']) ? $vehicle_assigned['stop_fair'] : ''), ['class' => 'gui-input','id' => 'stop_fair', 'readonly' => 'true']) !!}
                        <label for="stop_fair" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('stop_fair')) <p class="help-block">{{ $errors->first('stop_fair') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $vehicle_assigned['arr_session'],isset($vehicle_assigned['session_id']) ? $vehicle_assigned['session_id'] : null, ['class' => 'form-control','id'=>'session_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('session_id')) 
                    <p class="help-block">{{ $errors->first('session_id') }}</p>
                    @endif
                </div>
            </div>
            <div class=" row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.pickup_time')}}<span class="asterisk">*</span></span></label>
                    <label for="pickup_time" class="field prepend-icon">
                        {!! Form::time("pickup_time", old('pickup_time',isset($vehicle_assigned['pickup_time']) ? $vehicle_assigned['pickup_time'] : ''), ["class" => "gui-input","id" => "pickup_time"]) !!}
                        <label for="pickup_time" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('pickup_time')) <p class="help-block">{{ $errors->first('pickup_time') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.return_time')}}<span class="asterisk">*</span></span></label>
                    <label for="return_time" class="field prepend-icon">
                        {!! Form::time("return_time", old('return_time',isset($vehicle_assigned['return_time']) ? $vehicle_assigned['return_time'] : ''), ["class" => "gui-input","id" => "return_time"]) !!}
                        <label for="return_time" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('return_time')) <p class="help-block">{{ $errors->first('return_time') }}</p> @endif
                </div>
                <!-- end section -->
            </div>
            <div class="section-divider mv40">
                <span style="padding-left: 9px !important;"><i class="fas fa-user-check"></i>&nbsp;Student(s)&nbsp;</span>
            </div>
	<div class="bs-component" id="time_table_error" style="display:none;">
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	        <i class="fa fa-remove pr10"></i>
	        <span id="time_table_error_message"></span>
	    </div>
	</div>
            <div class="col-md-6">
                <div id="accordion2">
                    <div class="panel">
                        <span class="accordionpanel">
                            <label class="option block option-primary" style="text-align:center"> Select Student </label>
                        </span>
                        <div id="classopen_student" class="classopen">
                            <div class=" row" id="spy1">
                                <div  class="col-md-3"> <label style="font-size: 12px; padding: 14px 1px;">Choose Class</label></div>

                                <div class="col-md-6" style="">

                                    <label class="field select">
                                        {!!Form::select('search_class_id', $vehicle_assigned['arr_class'],isset($vehicle_assigned['class_id']) ? $vehicle_assigned['class_id'] : '', ['class' => 'form-control','id'=>'search_class_id'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                    @if ($errors->has('class_id')) 
                                    <p class="help-block">{{$errors->first('class_id')}}</p>
                                    @endif                            
                                </div>
                            </div>
                            <div>
                                <table class="table table-striped"style="color: #231a1a;"  class="table-resposive" id="assign-student-table">
                                    <thead>
                                        <tr>
                                            <th class="text-center">{{trans('language.student_name')}}</th>
                                            <th class="text-center">{{trans('language.enrollment_number')}}</th>
                                            <th class="text-left">Assign</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <div id='dispaly_student'>
                                    </div>
                                    </tbody>
                                </table>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div id="accordion2">
                    <div class="panel">
                        <span class="accordionpanel">
                            <label class="option block option-primary" style="text-align:center">Selected Student</label>
                        </span>
                        <div id="classopen_student" class="">
                            <div id='dispaly_selected_student'>
                                <table class="table table-striped"style="color: #231a1a;"  class="table-resposive" id="assigned-student-table">
                                    <thead>
                                        <tr>
                                            <th class="text-center">{{trans('language.student_name')}}</th>
                                            <th class="text-center">{{trans('language.enrollment_number')}}</th>
                                            <th class="text-center" style="width: 130px;">Joined Date</th>
                                            <th class="text-left">Un Assign</th>
			        <th class="text-center" style="width: 130px;">Unassigned Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($vehicle_assigned['assigned_detail']))
                                        @foreach($vehicle_assigned['assigned_detail'] as $student_list)
			    @if($student_list['assigned_to']  == 1)
                                        @php $student_id = $student_list['student_id']; 
                                        $assigned_detail_id = $student_list['assigned_detail_id']; 
                                        @endphp
                                        <tr id="unassign_{{$student_id}}">
                                            <td class="text-center assigned-student-name"><span class="">{{$student_list['student_name']}}</span> </td>
                                            <td class="text-center assigned-student-eno">{{$student_list['enrollment_number']}}</td>
                                            <td class="text-left ">
                                                <div>
                                                    {!! Form::text('joined_date['.$student_id.']',$student_list['join_date'], ['class' => 'gui-input joined_date', 'id' => 'join_'.$student_id, 'readonly' => 'readonly','required'=>true,'rel'=>$student_id,'date_type'=>'join']) !!}
                                                </div>
                                            </td>
                                            <td>
                                                {!! Form::button('<i class="fa fa-close uncheck-btn"></i>',['class'=>'student assigned-student','assigned-detail-id' =>$assigned_detail_id,'action-type'=>'unassign','rel'=>$student_id]) !!}
                                                {!! Form::hidden('student_assigned_detail_id['.$student_id.']',$assigned_detail_id) !!}
                                                {!! Form::hidden('student_id['.$student_id.']',$student_id,['class'=>'arr_student','id'=>'arr_sudent_id']) !!}
                                            </td>
			        <td class="text-left ">
                                                <div>
                                                    {!! Form::text('unassigned_date['.$student_id.']','', ['class' => 'gui-input unassigned_date', 'readonly' => 'readonly','id'=>'unjoin_'.$student_id,'rel'=>$student_id,'date_type'=>'unjoin']) !!}
                                                </div>
                                            </td>

                                        </tr>
			    @endif
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!--            <div class="section-divider mv40">
                            <span style="padding-left: 9px !important;"><i class="fas fa-user-check"></i>&nbsp;Staff(s) &nbsp;</span>
                        </div>
                        <div class="col-md-6">
                            <div id="accordion2">
                                <div class="panel">
                                    <a class="accordionpanel">
                                        <label class="option block option-primary">
                                            {!! Form::checkbox('assigned_to_employee',2,$vehicle_assigned['employee_checked'], ['class' => 'assigned_to gui-input', 'id' => 'assigned_to_employee']) !!}
                                            <span class="checkbox radioBtnpan"> </span>Employee</label>
                                    </a>
                                    <div id="classopen_employee" class="classopen"  @if($vehicle_assigned['employee_checked']) style="display:block;" @endif>
                                         <div id='dispaly_employee'>
                                        </div>   
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div id="accordion2">
                                <div class="panel">
                                    <a class="accordionpanel">
                                        <label class="option block option-primary">
                                            {!! Form::checkbox('assigned_to_employee',2,$vehicle_assigned['employee_checked'], ['class' => 'assigned_to gui-input', 'id' => 'assigned_to_employee']) !!}
                                            <span class="checkbox radioBtnpan"> </span>Employee</label>
                                    </a>
                                    <div id="" class="">
                                        <div id='dispaly_selected_employee'>
                                            <table class="table table-striped" style="color: #231a1a;" class="table-resposive" id="employee">
                                                @if(!empty($arr_employee_id))
                                                <thead>
                                                    <tr>
                                                        <th class="text-left">Select</th>
                                                        <th class="text-center">{{trans('language.employee_name')}}</th>
                                                        <th class="text-center">{{trans('language.employee_code')}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="">
                                                        <td>
                                                            {!! Form::checkbox('employee_id[]','','',['class'=>'employee vehicle-assign assigned_','id'=>'employee_id','assigned-detail-id'=>'']) !!}
                                                            {!! Form::hidden('employee_assigned_detail_id_','',['class'=>'employee','id'=>'assigned_detail_id','readonly'=>true]) !!}
                                                        </td>
                                                        <td class="text-center"><span class=""></span> </td>
                                                        <td class="text-center"></td>
                                                    </tr>
	
                                                </tbody>
                                                @else
                                                <tbody>
                                                    <tr colspan="3">
                                                        <td style="text-align: center">No Selected Staff available</td>
                                                    </tr>
                                                </tbody>
                                                @endif
                                            </table>
                                        </div>   
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>-->
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::button($submit_button, ['class' => 'button btn-primary','id'=>'submit_form']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
</div>
@include('backend.partials.popup')
<script type="text/javascript">
    jQuery(document).ready(function () {
//        getDateData();

        setDateFormat();
        function setDateFormat()
        {
            $(".joined_date,.unassigned_date").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                minDate: "{!! $vehicle_assigned['start_date'] !!}",
                maxDate: "{!! $vehicle_assigned['end_date'] !!}",
            });
        }
        $("#student-vehicle-assigned-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            ignore: [],
            rules: {
                stop_fair: {
                    required: true,
                },
                'joined_date[]': {
                    required: true,
//	        remote: {
//                        url: "{{url('check-vehicle-join-date')}}",
//                        type: "post",
//                        data: {
//                            student_parent_id: function () {
//                                return $("#student_parent_id").val();
//
//                            }
//                        }
//                    }
                },
                stop_date: {
                    required: true,
                },
                vehicle_id: {
                    required: true,
                },
                stop_point_id: {
                    required: true,
                },
                pickup_time: {
                    required: true,
                    notEqualTo: '#return_time',
                },
                return_time: {
                    required: true,
                    notEqualTo: '#pickup_time',

                },
                assigned_to: {
                    required: true,
                },
                session_id: {
                    required: true,
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
//                if (element.is(":radio") || element.is(":checkbox")) {
                if (element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }


        });
//        $('.joined_date').datepicker().on('change', function (ev) {
//            $(this).valid();
//        });
//        $('.unassigned_date').datepicker().on('change', function (ev) {
//            $(this).valid();
//        });

//        $(".joined_date,.unassigned_date").datepicker({
//            prevText: '<i class="fa fa-chevron-left"></i>',
//            nextText: '<i class="fa fa-chevron-right"></i>',
//            showButtonPanel: true,
//            changeMonth: true,
//            changeYear: true,
//            dateFormat: "dd/mm/yy",
//            onSelect: function (newText) {
//                console.log($(this).val());
//                var date_type = $(this).attr('date_type');
//                console.log('type' + date_type);
//                var rel = $(this).attr('rel');
//                console.log('rel' + rel);
//                var join_date = $("#join_" + rel).val();
//                console.log('join_date' + join_date);
//                var unassign_date = $("#unjoin_" + rel).val();
//                console.log('unassign_date' + unassign_date);
//
//                if (join_date != '')
//                {
//                    if (unassign_date != '')
//                    {
//                        var jdate = new Date(join_date);
//		console.log(jdate);
//                        var sdate = new Date(unassign_date);
//		console.log(sdate);
//                        if (jdate > sdate)
//                        {
//                            alert('Un assign date can not be less than or equal to join date');
//                        }
//		else
//		{
//		    alert('okay');
//		}
//                    }
//                }
//
////       $.ajax({
////                    headers: {
////                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
////                    },
////                    url: "{{url('check-vehicle-join-date')}}",
////                    datatType: 'json',
////                    type: 'GET',
////                    data: {
////                        'join_date': class_id,
////                        'student_id': student_id,
////                        'student_vehicle_assigned_detail_id': $("#student_vehicle_assigned_id").val(),
////                    },
////                    success: function (response) {
////                        var resopose_data = [];
////                        resopose_data = response.data;
////                        if (response.status === 'success')
////                        {
////                            $("#assign-student-table tbody").html('');
////                            $("#assign-student-table tbody").append(resopose_data);
////                        } else
////                        {
////                            $("#assign-student-table tbody").html('');
////                        }
////                    }
////                });
//            }
//        });

        $("#assigned_to_student").change(function () {
            $("#classopen_student").toggle();
        });
        $("#assigned_to_employee").change(function () {
            var arr_employee_id = [];
            employee(arr_employee_id);
            $("#classopen_employee").toggle();
        });



        function student(class_id)
        {
            var student_id = [];
            if (class_id !== null)
            {
                $('input.arr_student').each(function () {
                    student_id.push($(this).val());
                });
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-student')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                        'student_id': student_id,
                        'student_vehicle_assigned_id': $("#student_vehicle_assigned_id").val(),
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $("#assign-student-table tbody").html('');
                            $("#assign-student-table tbody").append(resopose_data);
                        } else
                        {
                            $("#assign-student-table tbody").html('');
                        }
                    }
                });
            }
        }
        function employee()
        {
//        if (arr_employee_id.length > 0)
//        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-employee')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'arr_employee_id': '',
                },
                success: function (response) {
                    var resopose_data = [];
                    resopose_data = response.data;
                    if (response.status == 'success')
                    {
                        $("#dispaly_employee").html(resopose_data);
                    } else
                    {
                        $("#dispaly_employee").html('');
                    }
                }
            });
//        }
        }
        // get student list data 
        $(document).on('change', '#search_class_id', function (e) {
            e.preventDefault();
            var class_id = $(this).val();
            student(class_id);

        });

        $(document).on('change', '#stop_point_id', function (e) {
            e.preventDefault();
            var stop_point_id = $(this).val();
            if (stop_point_id !== null)
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-stop-fair')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'stop_point_id': stop_point_id,
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status == 'success')
                        {
                            $("#stop_fair").val(resopose_data);
                        } else
                        {
                            $("#stop_fair").val(null);
                        }
                    }
                });
            }
        });


//        function getDateData()
//        {
//            var session_id = $("#session_id").val();
//            if (session_id !== '')
//            {
//                $.ajax({
//                    headers: {
//                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
//                    },
//                    url: "{{url('get-session')}}",
//                    datatType: 'json',
//                    type: 'POST',
//                    data: {
//                        'session_id': session_id,
//                    },
//                    beforeSend: function () {
//                        $("#LoadingImage").show();
//                    },
//                    success: function (response) {
//                        if (response.status === 'success')
//                        {
//                            var minDate = response.start_date;
//                            var maxDate = response.end_date;
//                            $("#min_date").val(minDate);
//                            $("#max_date").val(maxDate);
//                            setDate(minDate, maxDate);
//                            $("#LoadingImage").hide();
//                        }
//                    }
//                });
//            }
//        }
//
//        function setDate(minDate, maxDate)
//        {
//            $(".joined_date").datepicker({
//                prevText: '<i class="fa fa-chevron-left"></i>',
//                nextText: '<i class="fa fa-chevron-right"></i>',
//                showButtonPanel: false,
//                changeMonth: true,
//                changeYear: true,
//                dateFormat: "dd/mm/yy",
//                minDate: minDate,
//                maxDate: maxDate
//            });
//        }
//
//        $(document).on('change', '#session_id', function () {
//            $(".joined_date").datepicker('destroy');
//            $(".joined_date").val('');
//            getDateData();
//        });
//        $(document).on('click', '.joined_date', function () {
//            var session = $("#session_id").val();
//            if (session === '')
//            {
//                $('.modal-body').text('First select academic year');
//                $('#alertmsg-student').modal({
//                    backdrop: 'static',
//                    keyboard: false
//                });
//            }
//        });



        $(document).on('click', '.assign-student,.assigned-student', function () {
            var student_id = $(this).attr('rel');
            var action_type = $(this).attr('action-type');
            if (action_type === 'assign')
            {
                var eno = $(this).closest("tr").find(".assign-student-eno").text();
                var s_name = $(this).closest("tr").find(".assign-student-name").text();
                var j_date = $(".j_date_" + student_id).val();
                var assign_tr = '<tr id="unassign_' + student_id + '">'
                        + '<td class="text-center assigned-student-name">' + s_name
                        + '<input type="hidden" name="student_id[' + student_id + ']" class ="arr_student" value="' + student_id + '" readonly>'
                        + '<input type="hidden" name="student_assigned_detail_id[' + student_id + ']" value="" readonly>'
                        + '</td>'
                        + '<td class="text-center assigned-student-eno">' + eno + '</td>'
                        + '<td><div><input type="text" name="joined_date[' + student_id + ']" value="" class="gui-input joined_date date_picker" id="join_' + student_id + '" readonly required rel="' + student_id + '" date_type= "join"></div>'
                        + '</td>'
                        + '<td class="text-left "><div>'
                        + '<button class="student assigned-student" id="student_id" assigned-detail-id="" action-type="unassign" name="student_assigned_detail_id[]" type="button" rel="' + student_id + '" date_type= "unjoin"><i class="fa fa-close uncheck-btn"></i></button>'
                        + '</div></td>'
                        + '<td><div><input type="text" name="unassigned_date[' + student_id + ']" value="" class="gui-input unassigned_date date_picker" id="unjoin_' + student_id + '" readonly rel="' + student_id + '"></div></td>'
                        + '</tr>';

                $("#assigned-student-table tbody").append(assign_tr);
                setDateFormat();
                $(this).closest("tr").remove();
            } else
            {
                var rel = $(this).attr("rel");
                var assigned_detail_id = $(this).attr("assigned-detail-id");
                $("#unjoin_" + rel).attr('required', true);
                var row = $(this);
                if (isNaN(assigned_detail_id))
                {
                    var assigned_detail_id = '';
                }
                if (assigned_detail_id !== '')
                {
//	        alert('hello');
                    if ($("#unjoin_" + rel).valid())
                    {
                        bootbox.confirm({
                            message: "Are you sure to delete ?",
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {
                                if (result)
                                {
                                    $.ajax(
                                            {
                                                url: "{{ url('vehicle-unassigned/delete/') }}",
                                                datatType: 'json',
                                                type: 'GET',
                                                data: {
                                                    'assigned_detail_id': assigned_detail_id,
                                                    'stop_date': $("#unjoin_" + rel).val(),

                                                },
                                                beforeSend: function () {
                                                    $("#LoadingImage").show();
                                                },
                                                success: function (res)
                                                {
                                                    $("#LoadingImage").hide();
                                                    if (res.status === "success")
                                                    {
                                                        var eno = row.closest("tr").find(".assigned-student-eno").text();
                                                        var s_name = row.closest("tr").find(".assigned-student-name").text();
                                                        var unassign_tr = '<tr id="assign_' + student_id + '">'
                                                                + '<td class="text-center assign-student-name">' + s_name + '</td>'
                                                                + '<td class="text-center assign-student-eno">' + eno + '</td>'
                                                                + '<td class="text-left"><div>'
                                                                + '<button class="checked_student assign-student" id="student_id" action-type="assign" type="button" rel="' + student_id + '"><i class="fa fa-check check-btn"></i>'
                                                                + '</div></td>'
                                                                + '</tr>';
                                                        $("#assign-student-table tbody").append(unassign_tr);
                                                        row.closest("tr").remove();
                                                    }
                                                }
                                            });
                                }
                            }
                        });
                    }
                } else
                {
                    var eno = $(this).closest("tr").find(".assigned-student-eno").text();
                    var s_name = $(this).closest("tr").find(".assigned-student-name").text();
                    var unassign_tr = '<tr id="assign_' + student_id + '">'
                            + '<td class="text-center assign-student-name">' + s_name + '</td>'
                            + '<td class="text-center assign-student-eno">' + eno + '</td>'
                            + '<td class="text-left"><div>'
                            + '<button class="checked_student assign-student" id="student_id" action-type="assign" type="button" rel="' + student_id + '"><i class="fa fa-check check-btn"></i>'
                            + '</div></td>'
                            + '</tr>';
                    $("#assign-student-table tbody").append(unassign_tr);
                    $(this).closest("tr").remove();
                }


            }
        });


//        check date
        $(document).on('click', '#submit_form', function () {
            if ($('#student-vehicle-assigned-form').valid()) {
                $('#time_table_error_message').html(null);
                $('#time_table_error').hide();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('check-vehicle-join-date')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: $('#student-vehicle-assigned-form').serialize(),
                    success: function (response) {
		console.log(response)
                        if (response === true)
                        {
                            $('#student-vehicle-assigned-form').submit();
                        } else
                        {
                            $('#time_table_error_message').html('Joined date already used for selected students. So please check join date.');
                            $('#time_table_error').show();
                        }
                    }
                });
            }
        });

    });
</script>