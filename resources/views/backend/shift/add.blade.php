@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<div class="row">
   
    <div class="col-md-12">
       
        <div class="col-md-12">
           
            <div class="box box-info">
                <div class="box-header with-border">
                    
                </div>
                {!! Form::open(['files'=>TRUE,'id' => 'shift-form' , 'class'=>'form-horizontal','url' => $save_url,'autocomplete'=>'off']) !!}
                @include('backend.shift._form',['submit_button' =>$submit_button])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush
