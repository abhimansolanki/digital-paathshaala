<style>
    input[type="time"]{
        height: 25px !important;
    }
</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_shift')}}</li>
                </ol>
            </div>
        </div>
        <div class="bg-light panelBodyy">
            @if(Session::has('success'))
            @include('backend.partials.messages')
            @endif
            <div class="section-divider mv40">
                <span><i class="fa fa-clock-o"></i>&nbsp;Shift Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.shift_name')}}<span class="asterisk">*</span></span></label>
                    <label for="shift_name" class="field prepend-icon">
                        {!! Form::text('shift_name', old('shift_name',isset($shift['shift_name']) ? $shift['shift_name'] : ''), ['class' => 'gui-input', 'id' => 'shift_name']) !!}
                        <label for="shift_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('shift_name')) 
                    <p class="help-block">{{ $errors->first('shift_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.effective_date') !!}<span class="asterisk">*</span></span></label>
                    <label for="effective_date" class="field prepend-icon">
                        {!! Form::text('effective_date', old('effective_date',isset($shift['effective_date']) ? $shift['effective_date'] : ''), ['class' => 'gui-input date_picker', 'id' => 'effective_date','readonly'=>true]) !!}
                        <label for="effective_date" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('effective_date')) <p class="help-block">{{ $errors->first('effective_date') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.login_time') !!}{{trans('language.shift_name')}}</span></label>
                    <label for="login_time" class="field prepend-icon">
                        {!! Form::time('login_time', old('login_time',isset($shift['login_time']) ? $shift['login_time'] : ''), ['class' => 'gui-input', 'id' => 'login_time']) !!}
                        <label for="login_time" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('login_time')) <p class="help-block">{{ $errors->first('login_time') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.logout_time') !!}<span class="asterisk">*</span></span></label>
                    <label for="logout_time" class="field prepend-icon">
                        {!! Form::time('logout_time', old('logout_time',isset($shift['logout_time']) ? $shift['logout_time'] : ''), ['class' => 'gui-input input-time', 'id' => 'logout_time']) !!}
                        <label for="logout_time" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('logout_time')) <p class="help-block">{{ $errors->first('logout_time') }}</p> @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.description') !!}</span></label>
                    <label for="description" class="field prepend-icon">
                        {!! Form::textarea('description', old('description',isset($shift['description']) ? $shift['description'] : ''), ['class' => 'gui-input', 'id' => 'class']) !!}
                        <label for="description" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                        @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                    </label>
                </div>
            </div>
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" title="">{{ trans('language.list_shift')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="shift-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.shift_name') !!}</th>
                            <th>{!!trans('language.effective_date') !!}</th>
                            <th>{!!trans('language.login_time') !!}</th>
                            <th>{!!trans('language.logout_time') !!}</th>
                            <th>{!!trans('language.description') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

        $("#shift-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                logout_time: {
                    required: true
                },
                login_time: {
                    required: true
                },
                effective_date: {
                    required: true
                },
                shift_name: {
                    required: true,
                    lettersonly: true,
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9\s\.]+$/i.test(value);
        }, "Please enter only alphabets(more than 1)");

        var table = $('#shift-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('shift/data')}}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'shift_name', name: 'shift_name'},
                {data: 'effective_date', name: 'effective_date'},
                {data: 'login_time', name: 'login_time'},
                {data: 'logout_time', name: 'logout_time'},
                {data: 'description', name: 'description'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        $.ajax(
                                {
                                    url: "{{ url('shift/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'shift_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();

                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });

    });
</script>