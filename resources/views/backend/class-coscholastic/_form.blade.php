<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.class_co_scholastic')}}</li>
                    {!! Form::hidden('class_co_scholastic_id',isset($class_coscholastic['class_co_scholastic_id']) ? $class_coscholastic['class_co_scholastic_id'] : null, ['class' => 'gui-input','id' => 'class_co_scholastic_id']) !!}
                </ol>
            </div>
        </div>
        @if(Session::has( 'success'))
        @include('backend.partials.messages')
        @endif
        <div class="panel-body bg-light">
            <!-- .section-divider -->
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $class_coscholastic['arr_session'],isset($class_coscholastic['session_id']) ? $class_coscholastic['session_id'] : null, ['class' => 'form-control','id'=>'session_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('session_id')) 
                    <p class="help-block">{{ $errors->first('session_id') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{ trans('language.class')}}<span class="asterisk">*</span></span></label>
                    <label for="class_id" class="field select">
                        {!!Form::select('class_id', $class_coscholastic['arr_class'],isset($class_coscholastic['class_id']) ? $class_coscholastic['class_id'] : '', ['class' => 'form-control','id'=>'class_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('class_id')) 
                    <p class="help-block">{{ $errors->first('class_id') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{ trans('language.section')}}<span class="asterisk">*</span></span></label>
                    <label for="section_id" class="field select">
                        {!!Form::select('section_id', $class_coscholastic['arr_section'],isset($class_coscholastic['section_id']) ? $class_coscholastic['section_id'] : '', ['class' => 'form-control','id'=>'section_id','required'=>true])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('section_id')) 
                    <p class="help-block">{{ $errors->first('section_id') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{ trans('language.exam_category')}}<span class="asterisk">*</span></span></label>
                    <label for="exam_category_id" class="field select">
                        {!!Form::select('exam_category_id', $class_coscholastic['arr_exam_category'],isset($class_coscholastic['exam_category_id']) ? $class_coscholastic['exam_category_id'] : '', ['class' => 'form-control','id'=>'exam_category_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('exam_category_id')) 
                    <p class="help-block">{{ $errors->first('exam_category_id') }}</p>
                    @endif
                </div>
            </div>
            <!-- end .section row section -->
        </div>
        <div id="sbject-data-detail" style="">
            <div class="section-divider mv40">
                <span><i class="glyphicon glyphicon-sound-dolby"></i> {!! trans('language.co_scholastic') !!}</span>
            </div>
            <div class="section row" id="spy1">
                @if(!empty($class_coscholastic['arr_coscholastic']))
                @foreach($class_coscholastic['arr_coscholastic'] as $coscholastic)
                @php $co_checkd = false; @endphp
                @if(isset($class_coscholastic['selected_co_id'][$coscholastic['co_scholastic_id']]))
                @php $co_checkd = true; 
                $selectedco_scholastic = array_flip($class_coscholastic['selected_co_id'][$coscholastic['co_scholastic_id']]);
                @endphp
                @endif
                <div class="col-md-2" style="">
                    <div class="option-group field" id="checkBooxx">
                        <label class="option block option-primary">
                            {!! Form::checkbox('arr_co_scholastic[]',$coscholastic['co_scholastic_id'],$co_checkd, ['class' => 'gui-input checked_all', 'id' =>$coscholastic['co_scholastic_id']]) !!}
                            <span class="checkbox radioBtnpan"></span><em> <strong>{!! $coscholastic['co_scholastic_name'] !!} </strong></em>
                        </label>
                    </div>
                    @foreach($coscholastic['sub_coscholastic'] as $co_scholastic_id=> $sub_coscholastic)
                    @php $co_sub_checkd = false; @endphp
                    @if(isset($selectedco_scholastic[$co_scholastic_id]))
                    @php $co_sub_checkd = true; @endphp
                    @endif
                    <div class="option-group field" id="checkBooxx" style="margin-left: 25px">
                        <label class="option block option-primary">
                            {!! Form::checkbox('sub_co_scholastic_'.$coscholastic['co_scholastic_id'].'[]',$co_scholastic_id,$co_sub_checkd, ['class' => 'gui-input sub_coscholastic each_check'.$coscholastic['co_scholastic_id'], 'id' =>$co_scholastic_id ,'rel'=>$coscholastic['co_scholastic_id']]) !!}
                            <span class="checkbox radioBtnpan"></span><em>{!! $sub_coscholastic !!}</em>
                        </label>
                    </div>
                    @endforeach
                </div>
                @endforeach
                @endif
            </div>
        </div>
        <div class="panel-footer text-right">
	{!! Form::reset('Reset', ['class' => 'button btn-primary', 'id' => 'form-reload']) !!}
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
</div>
</div>
<!--Datatabel of data--> 
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" class="" title="">{{ trans('language.list_class_co_scholastic')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="class-coscholastic-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.class_name') !!}</th>
                            <th>{!!trans('language.section_name') !!}</th>
                            <th>{!!trans('language.exam_category') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
