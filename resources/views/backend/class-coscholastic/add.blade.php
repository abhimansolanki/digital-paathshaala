@extends('admin_panel/layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="box box-info">
                {!! Form::open(['files'=>TRUE,'id' => 'class-coscholastic-form' , 'class'=>'form-horizontal','url' =>$save_url,'autocomplete'=>'off']) !!}
                @include('backend.class-coscholastic._form',[$submit_button])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
         $(document).on('click', '#form-reload', function (e) {
        window.location.href = "{{ url('admin/class-coscholastic') }}";
    });
        var table = $('#class-coscholastic-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{url('class-coscholastic/data')}}",
            },
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'class_name', name: 'class_name'},
                {data: 'section_name', name: 'section_name'},
                {data: 'exam_category', name: 'exam_category'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });
        $("#class-coscholastic-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             * $("#section_id option").length
             ------------------------------------------ */
            ignore: [],
            rules: {
                class_id: {
                    required: true
                },
                session_id: {
                    required: true
                },
                'section_id': {
                    required: true,
                },
                'arr_co_scholastic[]': {
                    required:true
                },
                'exam_category_id': {
                    required: true
                },
                'result_in': {
                    required: true
                },
            },
            messages: {
                'section_id': {
                    remote: "Section already mapped with class and exam_category"
                },
                'arr_co_scholastic': {
                    remote: "Please check atleast one co scholastic "
                }

            },

            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
       
       $(document).on("click", ".delete-button", function (e) {
        e.preventDefault();
        var id = $(this).attr("data-id");

        bootbox.confirm({
            message: "Are you sure to delete ?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result)
                {
                    var token = '{!!csrf_token()!!}';
                    $.ajax(
                            {
                                url: "{{ url('class-coscholastic/delete/') }}",
                                datatType: 'json',
                                type: 'POST',
                                data: {
                                    'class_co_scholastic_id': id,
                                },
                                success: function (res)
                                {
                                    if (res.status === "success")
                                    {
                                        table.ajax.reload();

                                    } 
                                    else if (res.status === "used")
                                    {
                                        $('#server-response-message').text(res.message);
                                        $('#alertmsg-student').modal({
                                            backdrop: 'static',
                                            keyboard: false
                                        });
                                    }
                                }
                            });
                }
            }
        });

    });
       

        $(document).on('change', '#class_id,#session_id', function (e) {
            e.preventDefault();
            var class_id = $('#class_id').val();
            var session_id = $('#session_id').val();
	$('#section_id').empty();
	$('#section_id').append('<option value="">-- Select Section --</option>');
            if (class_id !== '' && session_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                        'session_id': session_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $.each(resopose_data, function (key, value) {
                                $('#section_id').append('<option value="' + key + '">' + value + '</option>');
                            });
                            $("#LoadingImage").hide();
                        }
                    }
                });
            }

        });

        $(document).on('change', '.checked_all', function () {

            var master_rel = $(this).attr('id');
            $('.each_check' + master_rel).prop('checked', $(this).prop("checked"));
        });
        $(document).on('change', '.sub_coscholastic', function () {
            var sub_rel = $(this).attr('rel');
//            if ($('.each_check' + sub_rel + ':checked').length === $('.each_check' + sub_rel).length) 
            if ($('.each_check' + sub_rel + ':checked').length > 0)
            {
                $('#' + sub_rel).prop('checked', true);
            } else {
                $('#' + sub_rel).prop('checked', false);
            }
        });

    });
</script>
@endsection

