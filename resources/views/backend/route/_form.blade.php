<style>
    .state-error{
        display:block !important;
        margin-top: 6px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    }
    .pluss button {
        background: #4a89dc;
        color: #fff;
        font-weight: bold !important;
        padding: 5px;
    }
    .pluss button {
        margin-top: 0px !important;
    }
    .pluss i {
        font-size: 15px;
    }
    .table > tbody > tr > td{
        padding:2px;
    }
    .table > tbody > tr > th{
        padding:2px;
    }
    .admin-form .gui-input{
        height: 30px;
        width: 95%;
        margin-left: 2%;
    }
    .btnDelete{
        margin-left:0px !important;  
    }
</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_route')}}</li>
                </ol>
            </div>
            <div class="topbar-right">
                <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right" style="height: 32px !important;">{{trans('language.view_route')}}</a>
            </div>
        </div>
        <!--@include('backend.partials.messages')-->
        <div class="bg-light panelBodyy">

            <div class="section-divider mv40" id="imgrouute">
                {!! Html::image('public/Routeicon.png') !!}
                <span>Route Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.destination')}}<span class="asterisk">*</span></span></label>
                    <label for="route" class="field prepend-icon">
                        {!! Form::text('route',old('route',isset($route['route']) ? $route['route'] : ''), ['class' => 'gui-input', 'id' => 'route','required'=>true]) !!}
                        <i class="arrow double"></i>
                    </label>
                    @if($errors->has('route')) <p class="help-block">{{ $errors->first('route') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label> 
                        <span class="radioBtnpan">{{trans('language.fair')}} <span class="asterisk">* (per month)</span></span>
                    </label> 
                    <label for="route_fair" class="field prepend-icon">
                        {!! Form::number('route_fair', old('route_fair',isset($route['route_fair']) ? $route['route_fair'] : ''), ['class' => 'gui-input','id' => 'route_fair','min'=>0,'required'=>true]) !!}
                        {!! Form::hidden('main_stop_point_id', old('main_stop_point_id',isset($route['main_stop_point_id']) ? $route['main_stop_point_id'] : ''), ['class' => 'gui-input add-destination','id' => 'main_stop_point_id','readonly'=>true]) !!}
                    </label> 
                    @if ($errors->has('route_fair')) 
                    <p class="help-block">{{ $errors->first('route_fair') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $route['arr_session'],isset($route['session_id']) ? $route['session_id'] : null, ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('session_id')) 
                    <p class="help-block">{{ $errors->first('session_id') }}</p>
                    @endif
                </div>
            </div>
            <div class="row tabledata1">
                <div class="col-md-6 ">
                    <table class="table table-striped" id="tbUser">
                        <tr class="text-center tabledata">
                            <th class="text-center tabledata">S.No.</th>
                            <th class="text-center tabledata">Location</th>
                            <th class="text-center tabledata">Fair</th>
                            <th class=" tabledata">Action
                                <div class="pluss" style="float:right;margin-top:0px;" id="0">
                                    <!--<button class="add-row" type="button" style="font-size:9px !important; "><i class="fa fa-plus-circle"></i>Row</button>-->
                                </div> 
                            </th>
                        </tr>

                        @php $count = 0; @endphp
                        @if(!empty($route['arr_stop_point']))
                        @foreach($route['arr_stop_point'] as $key => $stop_point)
                        @php $rel = $key + 1; @endphp
                        <tr class='stop_point_row' rel='{{$rel}}' id='stop_point_row{{$rel}}'>
                            <td>{!! Form::hidden('stop_point_id['.$rel.']', old('stop_point_id',isset($stop_point['stop_point_id']) ? $stop_point['stop_point_id'] : ''), ['class' => 'gui-input ','id' => 'stop_point_id']) !!}
                                <span class='stop_number' id='stop_number{{$rel}}'></span>
                            </td>
                            <td>
                                <div>
                                    {!! Form::text('stop_point['.$rel.']', old('stop_point',isset($stop_point['stop_point']) ? $stop_point['stop_point'] : ''), ['class' => 'gui-input stop-point','id'=>'stop-point-name'.$rel,'required'=>true]) !!}
                                </div>
                            </td>
                            <td>
                                <div>
                                    {!! Form::text('stop_fair['.$rel.']', old('stop_fair',isset($stop_point['stop_fair']) ? $stop_point['stop_fair'] : ''), ['class' => 'gui-input ','id' => 'stop_fair','required'=>true]) !!}
                                </div>
                            </td> 
                            <td>
                                <div class="col-md-3">
                                    <button type='button' class='btnDelete' rel='{{$rel}}' stop-point-id="{{$stop_point['stop_point_id']}}" id="stop-point-button{{$rel}}" style=""><i class='fa fa-times-circle' title='Remove'></i></button>
                                </div>
                                <div class="col-md-9 pluss" style="float:right;margin-top:0px;" id="{{$rel}}">
                                </div> 
                            </td>
                        </tr>
                        @php $count = $rel; @endphp
                        @endforeach
                        @endif
                    </table>
                </div>
                <div class="col-md-6">
                    <div class="main-timeline">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end .form-body section -->
    <div class="panel-footer text-right">
        {!! Form::submit($submit_button, ['class' => 'button btn_primary1235']) !!}
    </div>
</div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

        assignAddButton();
        $("#vehicle-route-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                route: {
                    required: true,
                    lettersonly: true
                            // nowhitespace: true
                },
                route_fair: {
                    required: true
                },
                session_id: {
                    required: true
                },
                'stop_point[]': {
                    required: true,
                    lettersonly: true
                },
                'stop_fair[]': {
                    required: true
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        assignNumber();
        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || !/[~`!@#$%\^&*()+=\-\_\.\[\]\\';,/{}|\\":<>\?]/.test(value) || /^[a-zA-Z][a-zA-Z0-9\s\.]+$/i.test(value);
        }, "Please enter only alphabets(more than 1)");
    });


    function assignNumber()
    {
        var numb = 1;
        $(".stop_point_row").each(function (e) {
            var rel = $(this).attr('rel');
            $("#stop_number" + rel).text('Stop' + numb);
            numb++;
        });
    }
    function assignAddButton()
    {
       var max_rel = 0;
                $(".pluss").html('');
                $('.pluss').each(function() {
                    max_rel = Math.max(this.id, max_rel);
                });
                 $("#"+max_rel).html("<button class='add-row' type='button' style='font-size:9px !important; '><i class='fa fa-plus-circle'></i>Row</button>");
    }
    $(document).ready(function () {
        var count = '<?php echo $count; ?>';
        count = parseInt(count) + 1;
        $(document).on("click", ".add-row", function () {
            $(".pluss").html('');
            var markup = "<tr class='stop_point_row' rel='" + count + "' id='stop_point_row" + count + "'>\n\
                <td><span class='stop_number' id='stop_number" + count + "'></span> </td> \n\
                <td><div><input name='stop_point[" + count + "]' class='gui-input counttext stop-point' id='stop-point-name" + count + "' required></div></td>\n\
                <td><div><input type='number' name='stop_fair[" + count + "]'  class='gui-input counttext' min='0' required></div></td>\n\
                <td>\n\
                <div class='col-md-3'>\n\
                <button type='button' class='btnDelete' stop-point-id='' rel='" + count + "' id='stop-point-button" + count + "'><i class='fa fa-times-circle' title='Remove'></i></button></div>\n\
                <div class='col-md-9 pluss' style='float:right;margin-top:0px;' id='" + count + "'>\n\
                <button class='add-row' type='button' style='font-size:9px !important; '><i class='fa fa-plus-circle'></i>Row</button>\n\
                </div></td></tr>";
            $("table tbody").append(markup);
            count++;
            assignNumber();
        });

        $("#tbUser").on('click', '.btnDelete', function () {

            var stop_point_id = $(this).attr("stop-point-id");
            var rel = $(this).attr("rel");
            if (stop_point_id !== '')
            {
                bootbox.confirm({
                    message: "Are you sure to delete ?",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result)
                        {
                            var token = '{!!csrf_token()!!}';
                            $.ajax(
                                    {
                                        url: "{{ url('remove-route-stop/delete/') }}",
                                        datatType: 'json',
                                        type: 'POST',
                                        data: {
                                            'stop_point_id': stop_point_id,
                                        },
                                        beforeSend: function () {
                                            $("#LoadingImage").show();
                                        },
                                        success: function (res)
                                        {
                                            $("#LoadingImage").hide();
                                            if (res.status === "success")
                                            {
                                                $("#stop_point_row" + rel).closest('tr').remove();
                                                assignNumber();
                                                drawStopPointGraph();
                                                assignAddButton();
                                            } else if (res.status === "used")
                                            {
                                                $('#server-response-message').text(res.message);
                                                $('#alertmsg-student').modal({
                                                    backdrop: 'static',
                                                    keyboard: false
                                                });
                                            }

                                        }
                                    });
                        }
                    }
                });
            } else
            {
             
                $("#stop_point_row" + rel).closest('tr').remove();
                assignNumber();
                assignAddButton();
            }

        }
        );
        drawStopPointGraph();
        $(document).on('change', '.stop-point,#route', function () {
            drawStopPointGraph();
        });
        $(document).on('click', '.btnDelete', function () {
            drawStopPointGraph();
        });
        function drawStopPointGraph()
        {
            $(".main-timeline").html('');
            $(".stop_point_row").each(function () {
                var rel = $(this).attr('rel');
                var stop_point = $("#stop-point-name" + rel).val();
                if (stop_point !== '')
                {
                    var stop_point_graph = '<div class="timeline">' +
                            '<a href="#" class="timeline-content">' +
                            '<h3 class="title">' + stop_point + '<i class="fa fa-bus"></i></h3>' +
                            '</a></div>';
                    $(".main-timeline").append(stop_point_graph);
                }
            });
            if ($("#route").val() !== '')
            {
                var final_route = '<div class="timeline">' +
                        '<a href="#" class="timeline-content">' +
                        '<h3 class="title">' + $("#route").val() + '<i class="fa fa-bus"></i></h3>' +
                        '</a></div>';
                $(".main-timeline").append(final_route);
            }
        }
    });
</script>
