@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<style>
    td.details-control {
        background: url(<?php echo url('/public/details_open.png'); ?>) no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url(<?php echo url('/public/details_close.png'); ?>) no-repeat center center;
    }
    table, th, td {
        border: 1px solid #eeeeee;
    }
    table {
        width: 100%;
        margin-bottom: 20px;
    }
</style>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_route')}}</span>
                </div>
                <div class="topbar-right">
                    <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right custom-btn">{{trans('language.add_route')}}</a>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="route-table" cellspacing="0" width="100%">                    <thead>
                        <tr>
                            <th></th>
                            <th>{!!trans('language.destination') !!}</th>
                            <th>{!!trans('language.fair') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#route-table').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: "{{url('route/data')}}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {
                    "className": 'details-control id',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',

                },
                {data: 'route', name: 'route'},
                {data: 'route_fair', name: 'route_fair'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });
        function format(d) {
            // `d` is the original data object for the row
            var arr_stop_point = d.arr_stop_point;
            if (arr_stop_point.length > 0)
            {
                var tab_body = '';
                var tab_head = '<table cellpadding="5" cellspacing="0" border="0" style="margin-left: -4px;">';
                $.each(arr_stop_point, function (key, value)
                {
                    tab_body = tab_body + '<tr><td style="width: 11%;"></td>' +
                            '<td style="width: 45%;">' + value.stop_point + '</td>' +
                            '<td style="width: 37%;">' + value.stop_fair + '</td>' +
                            '<td></td>' +
                            '</tr>';
                });
                var tab_foot = '</table>';
                var table_data = tab_head + tab_body + tab_foot;
                return table_data;
            }
        }
        $('#route-table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                console.log(row.data());
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });
        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");
            bootbox.confirm({
                message: "Are you sure to inactive route ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        $.ajax(
                                {
                                    url: "{{ url('route/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'route_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();
                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
    });
</script>
@endsection
@push('scripts')
@endpush

