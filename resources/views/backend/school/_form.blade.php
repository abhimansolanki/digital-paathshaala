<style type="text/css">
    .admin-form .select {
        margin-top: 0px;
    }

    .select2-container .select2-selection--single {
        height: 35px !important;
    }

    .admin-form .select > select {
        height: 35px !important;
    }

    .state-error {
        display: block !important;
        margin-top: 6px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    }

    .fileupload-preview {
        line-height: 70px !important;
    }

    .admin-form .file .button {
        top: 0px !important;
        right: 0px !important;
    }
</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">School Profile</li>
                </ol>
            </div>
        </div>
        @include('backend.partials.messages')
        <div class="bg-light panelBodyy">
            {!! Form::hidden('school_id',old('school_id',isset($school['school_id']) ? $school['school_id'] : ''),['class' => 'gui-input', 'id' => 'school_id', 'readonly' => 'true']) !!}
            <div class="section row" id="spy1">
                <div class="col-md-2">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-preview thumbnail mb20">
                            @if(isset($school['school_logo']) && !empty($school['school_logo']))
                                <img src="{{url($school['school_logo'])}}">
                            @else
                                <img data-src="holder.js/93%x160" alt="holder">
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <span class="btn btn-system btn-file btn-block">
                                    <span class="fileupload-new">Select</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input type="file" class="gui-file" name="school_logo" accept="image/*" id="file1"
                                           onChange="document.getElementById('school_logo').value = this.value;">
                                    @if ($errors->has('school_logo'))
                                        <p class="help-block">{{ $errors->first('school_logo') }}</p>
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="section row" id="spy1">-->
                <div class="col-md-3" style="float:right">
                    <label><span class="radioBtnpan">Admission Form Fee<span class="asterisk">*</span></span></label>
                    <label for="form_fee" class="field prepend-icon">
                        {!! Form::text("form_fee", old('form_fee',isset($school['form_fee']) ? $school['form_fee'] : ''), ["class" => "gui-input","id" => "form_fee"]) !!}
                        <label for="form_fee" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('form_fee'))
                        <p class="help-block">{{ $errors->first('form_fee') }}</p>
                    @endif
                </div>
                <!--            </div>
                                <div class="col-md-4"></div>-->
            </div>
            <div class="section-divider mv40">
                <span><i class="fa fa-home"></i> &nbsp;School details</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Name<span class="asterisk">*</span></span></label>
                    <label for="school_name" class="field prepend-icon">
                        {!! Form::text('school_name', old('school_name',isset($school['school_name']) ? $school['school_name'] : ''), ['class' => 'gui-input', 'id' => 'school_name']) !!}
                        <label for="school_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('school_name'))
                        <p class="help-block">{{ $errors->first('school_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Alias</span></label>
                    <label for="alias" class="field prepend-icon">
                        {!! Form::text('alias', old('school_name',isset($school['alias']) ? $school['alias'] : ''), ['class' => 'gui-input', 'id' => 'alias']) !!}
                        <label for="alias" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('alias'))
                        <p class="help-block">{{ $errors->first('school_name') }}</p>
                    @endif
                </div>
                <div class="col-md-6">
                    <label><span class="radioBtnpan">Address<span class="asterisk">*</span></span></label>
                    <label for="address" class="field prepend-icon">
                        {!! Form::textarea('address', old('address',isset($school['address']) ? $school['address'] : ''), ['class' => 'gui-input','id' => 'address']) !!}
                    </label>
                    @if ($errors->has('address'))
                        <p class="help-block">{{ $errors->first('address') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Pin Code</span></label>
                    <label for="pin_code" class="field prepend-icon">
                        {!! Form::number("pin_code", old('pin_code',isset($school['pin_code']) ? $school['pin_code'] : ''), ["class" => "gui-input","id" => "pin_code"]) !!}
                        <label for="pin_code" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('pin_code'))
                        <p class="help-block">{{ $errors->first('pin_code') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Contact No<span class="asterisk">*</span></span></label>
                    <label for="contact_number" class="field prepend-icon">
                        {!! Form::text("contact_number", old('contact_number',isset($school['contact_number']) ? $school['contact_number'] : ''), ["class" => "gui-input numeric","id" => "contact_number"]) !!}
                        <label for="contact_number" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('contact_number'))
                        <p class="help-block">{{ $errors->first('contact_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Mobile No<span class="asterisk">*</span></span></label>
                    <label for="mobile_number" class="field prepend-icon">
                        {!! Form::text("mobile_number", old('mobile_number',isset($school['mobile_number']) ? $school['mobile_number'] : ''), ["class" => "gui-input numeric","id" => "mobile_number"]) !!}
                        <label for="mobile_number" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('mobile_number'))
                        <p class="help-block">{{ $errors->first('mobile_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Email ID<span class="asterisk">*</span></span></label>
                    <label for="email" class="field prepend-icon">
                        {!! Form::email("email", old('email',isset($school['email']) ? $school['email'] : ''), ["class" => "gui-input","id" => "email"]) !!}
                        <label for="email" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('email'))
                        <p class="help-block">{{ $errors->first('email') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Fax No</span></label>
                    <label for="fax_number" class="field prepend-icon">
                        {!! Form::text("fax_number", old('fax_number',isset($school['fax_number']) ? $school['fax_number'] : ''), ["class" => "gui-input","id" => "fax_number"]) !!}
                        <label for="fax_number" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('fax_number'))
                        <p class="help-block">{{ $errors->first('fax_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Registration No<span class="asterisk">*</span></span></label>
                    <label for="registration_number" class="field prepend-icon">
                        {!! Form::text("registration_number", old('registration_number',isset($school['registration_number']) ? $school['registration_number'] : ''), ["class" => "gui-input","id" => "registration_number"]) !!}
                        <label for="registration_number" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('registration_number'))
                        <p class="help-block">{{ $errors->first('registration_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Tin No</span></label>
                    <label for="tin_number" class="field prepend-icon">
                        {!! Form::text("tin_number", old('tin_number',isset($school['tin_number']) ? $school['tin_number'] : ''), ["class" => "gui-input","id" => "tin_number"]) !!}
                        <label for="tin_number" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('tin_number'))
                        <p class="help-block">{{ $errors->first('tin_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Pan No</span></label>
                    <label for="pan_number" class="field prepend-icon">
                        {!! Form::number("pan_number", old('pan_number',isset($school['pan_number']) ? $school['pan_number'] : ''), ["class" => "gui-input","id" => "pan_number",'pattern'=>'[0-9]*']) !!}
                        <label for="pan_number" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('pan_number'))
                        <p class="help-block">{{ $errors->first('pan_number') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Start Year<span class="asterisk">*</span></span></label>
                    <label for="started_year" class="field prepend-icon">
                        {!! Form::text("started_year", old('started_year',isset($school['started_year']) ? $school['started_year'] : ''), ["class" => "gui-input","id" => "started_year"]) !!}
                        <label for="started_year" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('started_year'))
                        <p class="help-block">{{ $errors->first('started_year') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Dies Code</span></label>
                    <label for="dies_code" class="field prepend-icon">
                        {!! Form::text("dies_code", old('dies_code',isset($school['dies_code']) ? $school['dies_code'] : ''), ["class" => "gui-input","id" => "dies_code"]) !!}
                        <label for="dies_code" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('dies_code'))
                        <p class="help-block">{{ $errors->first('dies_code') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Effilated No</span></label>
                    <label for="effilated_number" class="field prepend-icon">
                        {!! Form::text("effilated_number", old('effilated_number',isset($school['effilated_number']) ? $school['effilated_number'] : ''), ["class" => "gui-input","id" => "effilated_number"]) !!}
                        <label for="effilated_number" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('effilated_number'))
                        <p class="help-block">{{ $errors->first('effilated_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Effilated to</span></label>
                    <label for="effilated_to" class="field prepend-icon">
                        {!! Form::text("effilated_to", old('effilated_to',isset($school['effilated_to']) ? $school['effilated_to'] : ''), ["class" => "gui-input","id" => "effilated_to"]) !!}
                        <label for="effilated_to" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('effilated_to'))
                        <p class="help-block">{{ $errors->first('effilated_to') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Start Timing in summer (am)</span></label>
                    <label for="summer_start_time" class="field prepend-icon">
                        {!! Form::time("summer_start_time", old('summer_start_time',isset($school['summer_start_time']) ? $school['summer_start_time'] : ''), ["class" => "gui-input","id" => "summer_start_time"]) !!}
                        <label for="summer_start_time" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('summer_start_time'))
                        <p class="help-block">{{ $errors->first('summer_start_time') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">End Timing in summer (pm)</span></label>
                    <label for="summer_end_time" class="field prepend-icon">
                        {!! Form::time("summer_end_time", old('summer_end_time',isset($school['summer_end_time']) ? $school['summer_end_time'] : ''), ["class" => "gui-input","id" => "summer_end_time"]) !!}
                        <label for="summer_end_time" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('summer_end_time'))
                        <p class="help-block">{{ $errors->first('summer_end_time') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Start Timing in winter (am)</span></label>
                    <label for="winter_start_time" class="field prepend-icon">
                        {!! Form::time("winter_start_time", old('winter_start_time',isset($school['winter_start_time']) ? $school['winter_start_time'] : ''), ["class" => "gui-input","id" => "winter_start_time"]) !!}
                        <label for="winter_start_time" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('winter_start_time'))
                        <p class="help-block">{{ $errors->first('winter_start_time') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">End Timing in winter (pm)</span></label>
                    <label for="winter_end_time" class="field prepend-icon">
                        {!! Form::time("winter_end_time", old('winter_end_time',isset($school['winter_end_time']) ? $school['winter_end_time'] : ''), ["class" => "gui-input","id" => "winter_end_time"]) !!}
                        <label for="winter_end_time" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('winter_end_time'))
                        <p class="help-block">{{ $errors->first('winter_end_time') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Website Url</span></label>
                    <label for="website_url" class="field prepend-icon">
                        {!! Form::text("website_url", old('website_url',isset($school['website_url']) ? $school['website_url'] : ''), ["class" => "gui-input","id" => "website_url"]) !!}
                        <label for="website_url" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('website_url'))
                        <p class="help-block">{{ $errors->first('website_url') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Principal Name</span></label>
                    <label for="principle_name" class="field prepend-icon">
                        {!! Form::text("principal_name", old('principal_name',isset($school['principal_name']) ? $school['principal_name'] : ''), ["class" => "gui-input","id" => "principal_name"]) !!}
                        <label for="principal_name" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('principal_name'))
                        <p class="help-block">{{ $errors->first('principal_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Principal Contact No</span></label>
                    <label for="principal_contact_no" class="field prepend-icon">
                        {!! Form::text("principal_contact_no", old('principal_contact_no',isset($school['principal_contact_no']) ? $school['principal_contact_no'] : ''), ["class" => "gui-input","id" => "principal_contact_no"]) !!}
                        <label for="principal_contact_no" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('principal_contact_no'))
                        <p class="help-block">{{ $errors->first('principal_contact_no') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">PSP Code</span></label>
                    <label for="psp_code" class="field prepend-icon">
                        {!! Form::text("psp_code", old('psp_code',isset($school['psp_code']) ? $school['psp_code'] : ''), ["class" => "gui-input","id" => "psp_code"]) !!}
                        <label for="psp_code" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('psp_code'))
                        <p class="help-block">{{ $errors->first('psp_code') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Transport Fee Months</span></label>
                    <label for="transport_fee_month" class="field prepend-icon">
                        {!! Form::select("transport_fee_month[]", \Illuminate\Support\Facades\Config::get('custom.month'), old('transport_fee_month',isset($school['transport_fee_month']) ? json_decode($school['transport_fee_month'],true) : ''), ["class" => "gui-input","multiple"=>"multiple","id" => "multiselect2"]) !!}
                        <label for="transport_fee_month" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('website_url'))
                        <p class="help-block">{{ $errors->first('website_url') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">School Color Logo</span></label>
                    <label for="school_logo_color" class="field prepend-icon">
                        {!! Form::file("school_logo_color", ["class" => "", "id" => "school_logo_color"]) !!}
                        <label for="school_logo_color" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('school_logo_color'))
                        <p class="help-block">{{ $errors->first('school_logo_color') }}</p>
                    @endif
                </div>
            </div>

            <div id="sbject-data-detail" style="">
                <div class="section-divider mv40">
                    <span><i class="fa fa-facebook"></i> {!! trans('language.social_media_options') !!}</span>
                </div>
                <div class="section row" id="spy1">
                    <div class="col-md-3">
                        <label><span class="radioBtnpan">Whatsapp</span></label>
                        <label for="website_url" class="field prepend-icon">
                            {!! Form::text("whatsapp_phone", old('whatsapp_phone',isset($school['whatsapp_phone']) ? $school['whatsapp_phone'] : ''), ["class" => "gui-input","id" => "whatsapp_phone"]) !!}
                            <label for="whatsapp_phone" class="field-icon">
                                <i class="fa fa-mobile"></i>
                            </label>
                        </label>
                        @if ($errors->has('whatsapp_phone'))
                            <p class="help-block">{{ $errors->first('whatsapp_phone') }}</p>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <label><span class="radioBtnpan">Facebook</span></label>
                        <label for="principle_name" class="field prepend-icon">
                            {!! Form::text("facebook_url", old('facebook_url',isset($school['facebook_url']) ? $school['facebook_url'] : ''), ["class" => "gui-input","id" => "facebook_url"]) !!}
                            <label for="facebook_url" class="field-icon">
                                <i class="fa fa-mobile"></i>
                            </label>
                        </label>
                        @if ($errors->has('facebook_url'))
                            <p class="help-block">{{ $errors->first('facebook_url') }}</p>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <label><span class="radioBtnpan">Instagram</span></label>
                        <label for="instagram_url" class="field prepend-icon">
                            {!! Form::text("instagram_url", old('instagram_url',isset($school['instagram_url']) ? $school['instagram_url'] : ''), ["class" => "gui-input","id" => "instagram_url"]) !!}
                            <label for="instagram_url" class="field-icon">
                                <i class="fa fa-mobile"></i>
                            </label>
                        </label>
                        @if ($errors->has('instagram_url'))
                            <p class="help-block">{{ $errors->first('instagram_url') }}</p>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <label><span class="radioBtnpan">Twitter</span></label>
                        <label for="twitter_url" class="field prepend-icon">
                            {!! Form::text("twitter_url", old('twitter_url',isset($school['twitter_url']) ? $school['twitter_url'] : ''), ["class" => "gui-input","id" => "twitter_url"]) !!}
                            <label for="twitter_url" class="field-icon">
                                <i class="fa fa-mobile"></i>
                            </label>
                        </label>
                        @if ($errors->has('twitter_url'))
                            <p class="help-block">{{ $errors->first('twitter_url') }}</p>
                        @endif
                    </div>
                </div>
                <div class="section row" id="spy2">
                    <div class="col-md-3">
                        <label><span class="radioBtnpan">Youtube</span></label>
                        <label for="youtube_url" class="field prepend-icon">
                            {!! Form::text("youtube_url", old('youtube_url',isset($school['youtube_url']) ? $school['youtube_url'] : ''), ["class" => "gui-input","id" => "youtube_url"]) !!}
                            <label for="youtube_url" class="field-icon">
                                <i class="fa fa-mobile"></i>
                            </label>
                        </label>
                        @if ($errors->has('youtube_url'))
                            <p class="help-block">{{ $errors->first('youtube_url') }}</p>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <label><span class="radioBtnpan">Linkedin Profile</span></label>
                        <label for="linkedin_url" class="field prepend-icon">
                            {!! Form::text("linkedin_url", old('linkedin_url',isset($school['linkedin_url']) ? $school['linkedin_url'] : ''), ["class" => "gui-input","id" => "linkedin_url"]) !!}
                            <label for="linkedin_url" class="field-icon">
                                <i class="fa fa-mobile"></i>
                            </label>
                        </label>
                        @if ($errors->has('linkedin_url'))
                            <p class="help-block">{{ $errors->first('linkedin_url') }}</p>
                        @endif
                    </div>
                </div>
            </div>

            <div id="sbject-data-detail" style="">
                <div class="section-divider mv40">
                    <span><i class="fa fa-facebook"></i> {!! trans('language.app_settings') !!}</span>
                </div>
                <div class="section row" id="spy1">
                    <div class="col-md-3">
                        <label><span class="radioBtnpan">@lang('language.app_version')</span></label>
                        <label for="app_version" class="field prepend-icon">
                            {!! Form::number("app_version", old('app_version',isset($school['app_version']) ? $school['app_version'] : ''), ["class" => "gui-input","id" => "app_version"]) !!}
                            <label for="app_version" class="field-icon">
                                <i class="fa fa-mobile"></i>
                            </label>
                        </label>
                        @if ($errors->has('app_version'))
                            <p class="help-block">{{ $errors->first('app_version') }}</p>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <label><span class="radioBtnpan">@lang('language.mandatory_update')</span></label>
                        <label for="mandatory_update" class="field prepend-icon">
                            {!!Form::select('mandatory_update', array('' => 'Select', '1' => 'Yes', '0' => 'No'), old('mandatory_update',isset($school['mandatory_update']) ? $school['mandatory_update'] : ''), ['class' => 'form-control','id'=>'mandatory_update'])!!}
                            <i class="arrow double"></i>
                        </label>
                        @if ($errors->has('mandatory_update'))
                            <p class="help-block">{{ $errors->first('mandatory_update') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit('Save', ['class' => 'button btn-primary','name'=>'save']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
</div>
@include('backend.partials.popup')
