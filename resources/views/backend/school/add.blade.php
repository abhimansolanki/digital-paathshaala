@extends('admin_panel/layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="box box-info">
                {!! Form::open(['files'=>TRUE,'id' => 'school-form' , 'class'=>'form-horizontal','url' => $save_url]) !!}
                @include('backend.school._form',['submit_button' =>$submit_button ])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#school-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                form_fee: {
                    required: true
                },
                address: {
                    required: true
                },
                school_logo: {
                    extension: true,
                },
                school_name: {
                    required: true
                },
                contact_number: {
                    required: true
                },
                mobile_number: {
                    required: true
                },
                email: {
                    required: true
                },
                started_year: {
                    required: true
                },
                registration_number: {
                    required: true
                },
                'month_id[]': {
                    required: true
                },
            },
            messages: {

            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        jQuery.validator.addMethod("extension", function (a, b, c) {
            return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
        }, jQuery.validator.format("Only valid image formats are allowed"));

    });

</script>
@endsection

