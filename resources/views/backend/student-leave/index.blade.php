@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
    #session_id {
        padding:0px !important;
        height: 30px !important;
    }
</style>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">

                <div class="panel-title hidden-xs col-md-7">
                    <span class="glyphicon glyphicon-tasks"></span> <span>View Leaves</span>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-leave-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>Stu. Name</th>
                            <th>{{trans('language.class_name')}}</th>
                            <th>Section</th>
                            <th>{{trans('language.leave_type')}}</th>
                            <th>{{trans('language.leave_start_date')}}</th>
                            <th>{{trans('language.leave_end_date')}}</th>
                            <th>{{trans('language.reason')}}</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>{{trans('language.father_contact_number')}}</th>
                            <th>{{trans('language.address')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        getStudents();
        function getStudents()
        {
            $('#student-leave-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
//                bFilter:false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'Student Leave',
                        "filename": 'student-leave',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Student Leave',
                        "filename": 'student-leave',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                            modifier: {
                                selected: true
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: "{{ url('student-leave/data')}}",
                    data: function (f) {
                    }
                },
                columns: [
                    {data: 'student_id', name: 'student_id'},
                    {data: 'enrollment_number', name: 'enrollment_number'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'class_name', name: 'class_name'},
                    {data: 'section_name', name: 'section_name'},
                    {data: 'leave_type', name: 'leave_type'},
                    {data: 'leave_start_date', name: 'leave_start_date'},
                    {data: 'leave_end_date', name: 'leave_end_date'},
                    {data: 'reason', name: 'reason'},
                    {data: 'father_name', name: 'father_name'},
                    {data: 'father_contact_number', name: 'father_contact_number'},
                    {data: 'address_line1', name: 'address_line1'},
                ]
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
        }
        var table = $('#student-leave-table').DataTable();
        table.on('select deselect', function (e) {
            var arr_checked_student = checkedStudent();
            if (arr_checked_student.length > 0)
            {
                $(".buttons-excel").prop('disabled', false);
                $(".buttons-print").prop('disabled', false);
            } else
            {
                $(".buttons-excel").prop('disabled', true);
                $(".buttons-print").prop('disabled', true);
            }
        });

        function checkedStudent()
        {
            var arr_checked_student = [];
            $.each(table.rows('.selected').data(), function () {
                arr_checked_student.push(this["student_id"]);
            });
            return arr_checked_student;
        }
    });

</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



