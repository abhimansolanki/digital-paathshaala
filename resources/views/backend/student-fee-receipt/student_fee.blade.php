<style type="text/css">
    .inner_table {
        height: 190px;
        overflow-y: auto;
    }
    .inner_table1 {
        height: 189px;
        overflow-y: auto;
    }
    .inputtd input{
        width: 106px!important;
        height: 29px !important;
    }
    .table > tbody > tr > td, .table > tfoot > tr > td{
        padding: 5px 9px;
    }
    .fee_installment_paid_amount{
        padding: 0px;
        margin: 4px 0px !important;
    }
    .inputtd input{
        width: 97px!important;
        height: 22px !important;
    }
    .inner_table{
        height: 157px !important;
    }
    .pendingAmountClass{
        border-top: 1px solid #ccc;
        font-size: 11px;
        padding: 7px 10px;
    }
    /*.inner_table::-webkit-scrollbar {
        width: 4px !important;
        background: red !important;
        height: 10px !important;
    
    }
    
    .inner_table::-webkit-scrollbar-track {
       
        background: blue !important;
        height: 10px !important;
    }
    */
    .inner_table::-webkit-scrollbar {
        width: 7px !important;
    }

    .inner_table::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3) !important;
    }

    .inner_table::-webkit-scrollbar-thumb {
        background-color: darkgrey !important;
        outline: 1px solid slategrey !important;
    }

    .margincommm{
        margin:0px !important;
    }
    .margincommm input{
        width: 61% !important;
        height: 25px !important;
    }
    .margincommm span{
        font-size: 13px !important;
    }
    .transport_main_button{
        margin-left: 15px !important;
        margin-top: 12px !important;
    }
    .bordernonetable{
        padding-bottom: 5px;
    }
</style>
<!--border-bottom: none !important; width:0px;
border: 1px solid #c0c0c0 !important;-->
@if(isset($fee) && !empty($fee))
@php $total_pending_amount = 0; $without_fine_total_pending_amount = 0; 
@endphp
@foreach($fee as $student_key=> $display_fee)
@php $student_key = $display_fee['student_id']; @endphp
{!! Form:: hidden('student_class_id_'.$student_key,$display_fee['class_id'],['id'=>'student_class_id','readonly'=>true]) !!}
<fieldset style="border: 1px solid #c0c0c0 !important; margin-bottom: 9px;">
    <legend style="border-bottom: 0px !important; width:auto;font-size: 15.5px !important;padding: 2px !important;"> Name : {{$display_fee['student_name']}} | Class : {{$display_fee['class_name']}} </legend>
    <div class="student_tables" id="student_{!!$student_key!!}" style="margin-bottom:8px;">
        <div class="col-md-5 text-left bordernonetable" id="rightBorderClass">
            <label class="fullwitst colorchange"><b class="radioBtnpan" ><i class="fas fa-rupee-sign"></i>{{trans('language.pending_fees')}}</b></label>
            @if(isset($display_fee['student_fee']['pending']))
            <div class="commbdr" id="student-pending-fee-detail" id="show">
                <table class="table table-striped table-fixed " style="font-size: 14px !important;">
                    <thead>
                        <tr>
                            <th class="text-center">{{trans('language.table_fee_type')}}</th>
                            <th class="text-center">{{trans('language.table_fee_circular')}}</th>

                            <th class="text-center">{{trans('language.table_fee_amount')}}</th>
                            <th class="text-center">+</th>
                            <th class="text-center">fine</th>
                            <th class="text-center">{{trans('language.table_paid_amount')}}</th>
                            <th class="text-center">{{trans('language.add')}}</th>
                            <th class="text-center">{{trans('language.remove')}}</th>
                        </tr>
                    </thead>
                </table>
                <div class="inner_table wrap">
                    <table class="table table-striped"  style="font-size: 12px !important;" >
                        <tbody>
                            @php
                            $counter = 1;
                            @endphp
                            @foreach($display_fee['student_fee']['pending'] as $pending_fee)
                            @if(isset($pending_fee['arr_fee_detail']) && !empty($pending_fee['arr_fee_detail']))
                            @foreach($pending_fee['arr_fee_detail'] as $key2=> $fee_detail)
                            @php
                            $rel = $student_key.'_'.$counter;
                            $fine_amount = 0;
                            $fee_amount = 0;
                            $fine_amount = (int) $pending_fee['fine_amount'];
                            $fee_amount = $fee_detail['fee_amount'];
                            $pending_amount = $fee_detail['final_pending_amount'];
                            @endphp
                            <tr>
                                {!! Form::hidden('fee_id_'.$student_key.'[]',$pending_fee['fee_id'].'_'.$fee_detail['fee_detail_id'], ['class' => '','id' => 'fee_id']) !!}
                                <td style="width: 10px;" >{!! Form::hidden('fee_type_id_'.$student_key.'[]',$pending_fee['fee_type_id'], ['class' => '','id' => 'fee_type']) !!}
                                    {!! $pending_fee['fee_type'] !!}
                                </td>
                                <td>{!! Form::hidden('fee_circular_id_'.$student_key.'[]',$pending_fee['fee_circular_id'], ['class' => '','id' => 'fee_circular_id']) !!}
                                    {!! Form::hidden('fee_circular_name_'.$student_key.'[]',$fee_detail['fee_sub_circular_name'], ['class' => '','id' => 'fee_sub_circular_name']) !!}
                                    {!! $fee_detail['fee_sub_circular'] !!}
                                </td>
                                <td >
                                    {!! Form::hidden('fee_amount_'.$student_key.'[]',$pending_amount, ['class'=>'fee_installment_amount','id' => 'fee_amount_'.$rel,'rel'=>$rel]) !!}
                                    {!! $pending_amount !!}
                                    <!--+{{$fee_detail['installment_fine_amount']}} $fine_amount-->
                                </td>
                                <td>+</td>
                                <td>{{$fee_detail['installment_fine_amount']}}</td>
                                <td style="width:112px;" class="inputtd">
                                    <div>
                                        {!! Form::number('paid_amount_'.$student_key.'[]','', ['class' => 'fee_installment_paid_amount valid_empty_value student_installments_'.$student_key,'min'=>0,'id' => 'paid_amount_'. $rel,'data-fee-amount'=>$pending_amount, 'rel'=>$rel,'fine-type'=>$pending_fee['fine_type'],'fine-days'=>$fee_detail['fine_days'],'fine'=>$fine_amount,'final-fine-amount'=>'','student-id'=>$student_key,'fine-auto'=>'']) !!}
                                    </div>
                                </td>
                                <td class="cbtnn" >
                                    {!! Form::button('<i class="fas fa-plus"></i>',['class' =>'add_amount','id' => 'add_' . $rel, 'data-paid-amount'=>$pending_amount,'rel'=>$rel,'data-clicked-class'=>'add_amount']) !!}

                                    {!! Form::button('<i class="fas fa-minus"></i>',['class' =>' remove_amount btn-primary remove_amount','id' => 'remove_' . $rel,'data-paid-amount'=>$fee_amount,'rel'=>$rel,'data-clicked-class'=>'remove_amount']) !!}
                                </td>
                            </tr>
                            @php  $counter ++; @endphp
                            @endforeach
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="pendingAmountClass " >
                    <div class="col-md-7 paddingrr">
                        @php $each_student_pending = 0;
                        $each_student_pending = $display_fee['student_fee']['pending_amount'] + $display_fee['student_fee']['installment_fine_amount'];
                        $each_student_pending_without_fine = $display_fee['student_fee']['pending_amount'];
                        @endphp
                        {!! Form::hidden('pending_amount_'.$student_key,$each_student_pending, ['class' => 'pending_amount_'.$student_key,'id' => 'student_pending_amount_'. $student_key,'readonly'=>true]) !!}
                        {!! Form::hidden('without_fine_pending_amount_'.$student_key,$each_student_pending_without_fine, ['class' => 'pending_amount_'.$student_key,'id' => 'without_fine_pending_amount_'. $student_key,'readonly'=>true]) !!}
                        {!! Form::hidden('student_assigned_amount_'.$student_key,'', ['class' => 'student_assigned_amount_'.$student_key,'id' => 'student_assigned_amount_'. $student_key,'readonly'=>true]) !!}
                        {{trans('language.pending_amount')}} : {{amount_format($display_fee['student_fee']['pending_amount'])}}  </div>

                    <span class="text-right pull-right">Fine : {{amount_format($display_fee['student_fee']['installment_fine_amount'])}} </span>
                    <div class="clearfix"></div>
                    @php
                    $total_pending_amount = $total_pending_amount + $display_fee['student_fee']['pending_amount'] + $display_fee['student_fee']['installment_fine_amount'];
                    $without_fine_total_pending_amount = $without_fine_total_pending_amount + $display_fee['student_fee']['pending_amount'];
                    @endphp
                </div>
            </div>
            @endif
        </div>
        <!--paid fee table-->
        <div class="col-md-7 text-left bordernonetable" id="rightBorderClass1">
            <label class="fullwitst"><b class="radioBtnpan"><i class="fas fa-rupee-sign"></i>{{trans('language.paid_fees')}}</b></label>
            @if(isset($display_fee['student_fee']['paid']))
            <div id="student-paid-fee-detail" class="commbdr">
                <table class="table table-striped head"  style="font-size: 14px !important;" >
                    <thead>
                        <tr>
                            <th>{{trans('language.table_receipt_date')}}</th>
                            <th>{{trans('language.table_receipt_number')}}</th>
                            <th>{{trans('language.table_fee_type')}}</th>
                            <th>{{trans('language.table_fee_circular')}}</th>
                            <th>{{trans('language.table_fee_amount')}}</th>
                            <th>{{trans('language.table_discount_amount')}}</th>
                            <th>{{trans('language.table_fine_amount')}}</th>
                            <th>{{trans('language.remove')}}</th>
                        </tr>
                    </thead>
                </table >
                <div class="inner_table wrap">
                    <table class="table table-striped"  style="font-size: 12px !important;" >

                        @if(!empty($display_fee['student_fee']['paid']))
		<?php
		    $counter = 1;
		?>
                        @foreach($display_fee['student_fee']['paid'] as  $receipt)
                        @foreach($receipt['fee_receipt_detail'] as $key2 => $receipt_detail)
                        @php $paid_rel = $student_key.'_'.$counter; @endphp
                        <tr class="student_information">
                            {!! Form::hidden('fee_circular_id[]',$receipt_detail['fee_circular_id'], ['class' => '','id' => 'fee_circular_id']) !!}
		    <?php $row_span = count($receipt['fee_receipt_detail']); ?>

                            <td style="width:70px;" rowspan="">{!! $receipt['receipt_date'] !!}</td>
                            <td style="width:59px;" rowspan="">{!! $receipt['receipt_number'] !!}</td>


                            <td style="width:70px;">{!! $receipt_detail['fee_type'] !!}</td>
                            <td style="width:97px;">{!! $receipt_detail['fee_circular_name'] !!}</td>
                            <td style="width:99px;">{!! $receipt_detail['installment_paid_amount'] !!}</td>

                            <td style="width:106px;" rowspan="">{!! $receipt['discount_amount'] !!}</td>
                            <td  style="width:67px;" rowspan="">{!! $receipt['fine_amount'] !!}</td>
                            <td class="removePadding">{!! Form::button('-',['class' =>'tableforpendingremove delete_installment btn-primary remove_amount','id' => 'remove_' . $paid_rel, 'rel'=>$paid_rel,'receipt-id'=>$receipt['student_fee_receipt_id']]) !!}</td>
                        </tr>
		<?php
		    $counter ++;
		?>
                        @endforeach
                        @endforeach
                        @endif
                    </table>
                </div>
                <div class="pendingAmountClass text-center">
                    {{trans('language.deposit_amount')}} :{{amount_format($display_fee['student_fee']['deposit_amount'])}}
                </div>
            </div>
            @endif
        </div>
        <div class="clearfix"></div>
        <div class=" row marginRemove" id="spy1">
            <div class="col-md-2">
                <button class="button btn-primary transport_main_button transport_button" type="button" style="font-size:13px;" rel="{{$student_key}}"><img src="{{url('public/details_open.png')}}" style="" rel="{{$student_key}}" class="transport_button" current-img="details_open" id="trans_img{{$student_key}}"><b>Transport Fee</b></img></button>
            </div>
            <div class="col-md-2 margincommm  " style="margin-left: 17px !important;">
                <label><span class="radioBtnpan">Discount</span></label>
                <span id="discount_status" style="font-size:9px; color:red"></span>
                <label for="discount_amount" class="field prepend-icon">
                    {!! Form::number('discount_amount_'.$student_key, '', ['class' => 'gui-input student_discount',''=>trans('language.discount_amount'), 'id' => 'discount_amount_'.$student_key,'rel'=>$student_key,'min'=>0]) !!}
                    <label for="datepicker1" class="field-icon">
                        <i class="fa fa-eye"></i>
                    </label>
                </label>
            </div>
            <div class="col-md-2 margincommm" style="margin-left: -51PX !important;">
                <label><span class="radioBtnpan">Fine</span></label>
                <label for="fine_amount" class="field prepend-icon">
                    {!! Form::number('fine_amount_'.$student_key,'', ['class' => 'gui-input student_fine','id' => 'fine_amount_'.$student_key,'readonly'=>true,'min'=>0]) !!}
                    <label for="datepicker1" class="field-icon">
                        <i class="fa fa-eye"></i>
                    </label>
                </label>
            </div>

        </div>
    </div>

    <!--transport fee-->
    <!--<a id="button" href="#">Transport</a>-->

    <div class="student_tables tab-pane" id="transport_fee{{$student_key}}" style="margin-bottom:0px;display: none ">
        <div class="col-md-5 text-left bordernonetable" id="rightBorderClass">
            <label class="fullwitst colorchange"><b class="radioBtnpan" ><i class="fas fa-rupee-sign"></i>{{trans('language.transport_pending_fees')}}</b></label>
            @if(isset($display_fee['transport_fee']['pending']))
            <div class="commbdr" id="student-pending-fee-detail" id="show">
                <table class="table table-striped table-fixed " style="font-size: 14px !important;">
                    <thead>
                        <tr>
                            <th class="text-center">{{trans('language.month')}}</th>
                            <th class="text-center">{{trans('language.table_fee_amount')}}</th>
                            <th class="text-center">{{trans('language.table_paid_amount')}}</th>
                            <th class="text-center">{{trans('language.add')}}</th>
                            <th class="tex-center">{{trans('language.remove')}}</th>
                        </tr>
                    </thead>
                </table>
                <div class="inner_table wrap">
                    <table class="table table-striped"  style="font-size: 12px !important;" >
                        <tbody>
                            @php $counter  = 1; @endphp
                            @foreach($display_fee['transport_fee']['pending'] as $t_key=> $transport_pending)
                            @php $transport_pending_amount = $transport_pending['pending_amount'];
                            $t_rel = $student_key.'_'.$counter;;
                            @endphp
                            <tr>
                                <td style="width:80px">
                                    {!! Form::hidden('month_id_'.$student_key.'[]',$transport_pending['month_id'],['class'=>'','id'=>'']) !!}
                                    {!! $transport_pending['month'] !!}
                                </td>
                                <td style="width:80px;">
                                    {!! $transport_pending_amount !!} 
                                </td>
                                <td style="width:112px;" class="inputtd">
                                    <div>
                                        {!! Form::text('transport_fee_amount_'.$student_key.'[]','',['class'=>'valid_empty_value transport_pay_amount','id'=>'transport_pay_amount_'.$t_rel,'rel'=>$t_rel,'trans-data-fee-amount'=>$transport_pending_amount]) !!}  
                                        {!! Form::hidden('transport_fee_original_'.$student_key.'[]',$transport_pending_amount,['class'=>'amount','id'=>'transport_pay_amount_'.$t_rel]) !!}  
                                    </div>
                                </td>
                                <td class="cbtnn" style="padding-left: 20px !important;">
                                    {!! Form::button('<i class="fas fa-plus"></i>',['class' =>'transport_add_amount','id' => 'transport_add_'.$t_rel, 'transport-paid-amount'=>$transport_pending_amount,'rel'=>$t_rel,'data-clicked-class'=>'add_transport_amount']) !!}
                                </td>
                                <td class="cbtnn">
                                    {!! Form::button('<i class="fas fa-minus"></i>',['class' =>'transport_remove_amount btn-primary','id' => 'transport_remove_'.$t_rel,'transport-paid-amount'=>$transport_pending_amount,'rel'=>$t_rel,'data-clicked-class'=>'remove_transport_amount']) !!}
                                </td>
                            </tr>
                            @php  $counter ++; @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="pendingAmountClass " >
                    <div class="col-md-7 paddingrr"> 
                        {{trans('language.pending_amount')}} : {!! isset($display_fee['transport_fee']['total_pending_amount']) ? amount_format($display_fee['transport_fee']['total_pending_amount']) : 0 !!} </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            @endif
        </div>
        <!--paid fee table-->
        <div class="col-md-7 text-left bordernonetable" id="rightBorderClass1">
            <label class="fullwitst"><b class="radioBtnpan"><i class="fas fa-rupee-sign"></i>{{trans('language.transport_paid_fees')}}</b></label>
            @if(isset($display_fee['transport_fee']['paid']))
            <div id="student-paid-fee-detail" class="commbdr">
                <table class="table table-striped head"  style="font-size: 14px !important;" >
                    <thead>
                        <tr>
                            <th>{{trans('language.table_receipt_date')}}</th>
                            <th>{{trans('language.table_receipt_number')}}</th>
                            <th>{{trans('language.month')}}</th>
                            <th>{{trans('language.table_fee_amount')}}</th>
                            <th>{{trans('language.remove')}}</th>
                        </tr>
                    </thead>
                </table >
                <div class="inner_table wrap">
                    <table class="table table-striped"  style="font-size: 12px !important;" >
                        @foreach($display_fee['transport_fee']['paid'] as $arr_transport_paid)
                        @if(isset($arr_transport_paid['paid_fee_detail']))
                        @php $row_span = count($arr_transport_paid['paid_fee_detail']); @endphp
                        @foreach($arr_transport_paid['paid_fee_detail'] as $pt_key => $transport_paid)
                        @php 
                        $pt_rel = $pt_key +1 ;
                        @endphp
                        <tr class="student_information">
                            @if($pt_key == 0)
                            <td style="width:100px;" rowspan="{{$row_span}}">
                                {!! $arr_transport_paid['receipt_date'] !!}
                            </td>
                            <td style="width:80px;" rowspan="{{$row_span}}">
                                {!! $arr_transport_paid['receipt_number'] !!}
                            </td>
                            @endif
                            <td style="width:150px;">
                                {!! $transport_paid['month'] !!}
                            </td>
                            <td style="width:162px;">
                                {!! $transport_paid['paid_fee_amount'] !!}
                            </td>
                            @if($pt_key == 0)
                            <td class="removePadding">
                                {!! Form::button('-',['class'=>'tableforpendingremove revert_transport_installment btn-primary','id'=>'revert_transport_'.$pt_rel, 'rel'=>$pt_rel,'receipt-id'=>$arr_transport_paid['transport_fee_id']]) !!}
                            </td>
                            @endif
                        </tr>
                        @endforeach
                        @endif
                        @endforeach
                    </table>
                </div>
                <div class="pendingAmountClass text-center">
                    {{trans('language.deposit_amount')}} : {!! amount_format(array_sum(array_column($display_fee['transport_fee']['paid'],'deposit_amount')))!!} 
                </div>
            </div>
            @endif
        </div>
        <div class="clearfix"></div>
    </div>
</fieldset>
@endforeach
{!! Form::hidden('total_pending_amount',$total_pending_amount, ['id' => 'total_pending_amount','readonly'=>true]) !!}
{!! Form::hidden('without_fine_total_pending_amount',$without_fine_total_pending_amount, ['id' => 'without_fine_total_pending_amount','readonly'=>true]) !!}
@endif

<script type="text/javascript">
    jQuery(document).ready(function () {

        $(".transport_button").click(function () {
            var rel = $(this).attr('rel');
            var img = $("#trans_img" + rel).attr('current-img');
            if (img === 'details_open')
            {
                $("#trans_img" + rel).attr('current-img', 'details_close');
                $("#trans_img" + rel).attr('src', '<?php echo url('public/details_close.png') ?>');
            } else
            {
                $("#trans_img" + rel).attr('current-img', 'details_open');
                $("#trans_img" + rel).attr('src', '<?php echo url('public/details_open.png') ?>');
            }
            $("#transport_fee" + rel).toggle();
        });
    });
</script>


