<!--  Left Panel start-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<style type="text/css">
    .form-horizontal .checkbox, .form-horizontal .radio{
        min-height: 21px !important;
    }
    .table > thead > tr > th{
        font-weight: bold;
        font-size: 12px !important;
    }
    /* .table {
    background-color: #f3f3f3;
    }*/
    .mv40{
        margin: 20px 0px !important;
    }
    /*    .textareaClass{
    height: 35px !important;
    }*/
    .admin-form .select > select{
        padding: 7px 10px !important;
    }
    .admin-form .select{
        margin-top: 0px;
    }
    .select2-container .select2-selection--single{
        height: 35px;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        line-height: 35px;
    }
    .table > tbody + tbody{
        border-top: 1px solid #eeeeee !important;  
    }
    .option-primary em{
        font-weight: 600 !important;
        color: #333333;
    }
    .fa-user-circle{
        font-size: 65px;
        color: #ccc;
    }
    /*.section, #multiple_student{
        padding-top: 15px;
    }*/
    #submit_form{
        margin-bottom: 20px !important;
    }
    .continebutton {
        width: 142px !important;
        height: 40px !important;
        background: #4a89dc !important;
        color: #fff !important;
        text-align: center !important;
        margin-top: 15px !important;
        font-weight: bold !important;
        font-size: 16px;
    }
    .asterisk{
        padding-top: 8px;
    }
    #topbar{
        padding: 5px 21px !important;
    }
    .admin-form .panel{
        padding-top: 0px;
    }
    #studentDetails_fee{
        padding-top: 0px !important;
        padding-left: 0px !important;
    }
    #highlighid{
        border:0px !important;
        padding: 5px 0px !important;
    }
    .rightbarclass{
        border:0px !important;
        padding: 10px 0px;
        padding-bottom: 0px !important;
    }
    #studentDetails_fee{
        border:0px !important;
        padding-bottom: 0px !important;
    }
    legend{
        margin-bottom: 5px !important;
    }
    .fullwitst{
        padding: 0px 15px !important;
    }
    .fullwitst i{
        padding: 3px 4px;
        border-radius: 100%;
        width: 23px;
        font-size: 13px;
    }
    .fullwitst{
        padding-top: 5px !important;
    }
    .commbdr table th{
        padding: 3px 5px !important;
    }
    .commbdr{
        padding: 0px 0px !important;
    }
    input{
        height: 27px !important;
    }
    #fine_amount,  #bank_id, #payment_mode_id, #search_class_id ,#selected_fee_type_id{
        height: 27px !important;
        line-height: 14px !important;
        padding: 0px 10px !important;
    }
    form{
        margin-top: 0px !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        line-height: 23px;
        height: 27px !important;
    }


    .student_fee_panel{
        margin-bottom: 0px !important;
    }
    #lightpanelBodyy{
        padding-top: 3px !important;
    }
    .select2-container .select2-selection--single{
        height: 27px !important;
    }
    #student-list table th{
        padding: 2px 9px !important;
        font-size: 13px !important;
    }
    #student-list table tr tr{
        padding: 3px 9px !important;
        font-size: 13px !important;
    }
    body.sb-l-m #content-footer.affix{
        display: none !important;
    }
    @media only screen and (max-width: 1366px) and (min-width: 1024px){
        #dummy-table-class{
            padding: 0px;
        }
        #dummy-table-class1{
            padding: 0px 0px 0px 10px;
        }
    }
    /*    .marginRemove{
            margin-bottom: 10px;
        }*/
    @media (min-width: 992px){
        .col-md-2 {
            width: 13.666667% !important;
        }
    }
    .inner_table_parent::-webkit-scrollbar {
        width: 7px !important;
    }

    .inner_table_parent::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3) !important;
    }

    .inner_table_parent::-webkit-scrollbar-thumb {
        background-color: darkgrey !important;
        outline: 1px solid slategrey !important;
    }
</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;" id="CustomInput">
    @include('backend.partials.loader')
    <div class="col-md-12">
        <div class="">
            <div class="panel heading-border">
                <div id="topbar">
                    <div class="topbar-left">
                        <ol class="breadcrumb">
                            <li class="crumb-active">
                                <a href="{{url('admin/dashboard')}}"> <span class="glyphicon glyphicon-home"></span> Home</a>
                            </li>
                            <li class="crumb-trail">{{trans('language.add_fee_receipt')}}</li>
                        </ol>
                    </div>
                </div>
                <div class="lightpanelBodyy" id="lightpanelBodyy">
                    {!! Form:: hidden('fee_request','student',['id'=>'fee_request','readonly'=>true]) !!}
                    {!! Form:: hidden('total_amount_hidden','',['id'=>'total_amount_hidden','readonly'=>true]) !!}

                    <div style="border:1px solid #ccc;padding-bottom: 10px;margin-bottom: 10px">
                        <div class="col-md-12">
                            <div class="student_fee_panel" id="last_modify_ui" style="padding-left: 15px; border:0px !important;">
                                <!--  <div class="col-md-1">
                                     <i class="fas fa-user-circle"></i>
                                 </div> -->
                                <div class="col-md-5 padding_removecl">
                                    <h2> <span style="font-size:18px !important;">Student Info</span>
                                        <label class="option option-primary" style="margin-left: 15%;">
                                            {!! Form::checkbox('sibling_exist','','', ['class' => 'gui-input ', 'id' => 'sibling_exist']) !!}
                                            <span class="checkbox radioBtnpan"></span><em>{{trans('language.sibling_exist')}} </em>
                                        </label>
                                    </h2>
                                </div>
                                <div id='single_student' style="margin-left: 15px;">
                                    <div class="col-md-3">
                                        <small class="asterisk" style="float: left;display: block;width: 10px;">*</small>
                                        <div class="section"style="padding-top: 0px;float: left;width: 94%;">
                                            <label class="field select">
                                                {!!Form::select('search_class_id', $fee_receipt['arr_class'],isset($fee_receipt['class_id']) ? $fee_receipt['class_id'] : '', ['class' => 'form-control','id'=>'search_class_id'])!!} 
                                                <i class="arrow double"></i>
                                            </label>
                                            @if ($errors->has('class_id')) 
                                            <p class="help-block">{{ $errors->first('class_id') }}</p>
                                            @endif                            
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <small class="asterisk" style="float: left;display: block;width: 10px;">*</small>
                                        <div class="section"style="padding-top: 0px;float: left;width: 94%;">

                                            <label class="field select">
                                                {!!Form::select('student_id', $fee_receipt['arr_student'],isset($fee_receipt['student_id']) ? $fee_receipt['student_id'] : '', ['class' => 'form-control','id'=>'student_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                            @if ($errors->has('student_id')) 
                                            <p class="help-block">{{ $errors->first('student_id') }}</p>
                                            @endif                            
                                        </div>
                                        <div class="clearfix"></div>
                                        {!! Form:: hidden('selected_student_id','',['id'=>'selected_student_id','readonly'=>true]) !!}
                                    </div>
                                </div>
                                <div id='multiple_student' style="display:none">

                                    <div class="col-md-3">
                                        <label for="parent_contact_number" class="field prepend-icon">
                                            {!! Form::number("parent_contact_number",'', ["class" => "gui-input","id" => "parent_contact_number",'min'=>0]) !!}
                                            <label for="parent_contact_number" class="field-icon">
                                                <i class="fa fa-eye"></i>
                                            </label>
                                    </div>
                                    <div class="col-md-2">
                                        {!! Form::button('<i class="fas fa-search"></i>Search',['class' =>'button btn-primary  searchInquiry','id' => 'search_parent','style'=>'height: 30px !important;']) !!}  
                                    </div>   
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div style="border-top:1px solid #ccc;"></div>
                        <div class="col-md-8" style="padding-right: 0px;">
                            <div id='parent_student_list'>
                            </div>
                        </div>

                        <!--  Left side bar -->

                        <div class="col-md-4 " style="padding-left: 0px;">
                            <div class=" rightbarclass" >

                                <div class="col-md-6">
                                    <label> <span class="radioBtnpan">{{trans('language.receipt_date')}}</span></label>
                                    <span class="asterisk" style="float: left; width: 10px;">*</span>

                                    <label for="receipt_date" class="field prepend-icon" style="float: left; width: 94%">
                                        {!! Form::text('receipt_date', old('receipt_date',isset($fee_receipt['receipt_date']) ? $fee_receipt['receipt_date'] : ''), ['class' => 'gui-input', 'id' => 'receipt_date',"readonly"=>'readonly']) !!}
                                        <label for="datepicker1" class="field-icon">
                                            <i class="fa fa-calendar-o"></i>
                                        </label>
                                    </label>
                                    @if ($errors->has('receipt_date')) 
                                    <p class="help-block">{{ $errors->first('receipt_date') }}</p>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label><span class="radioBtnpan">{{trans('language.fee_type')}}</span></label>
                                    <label class="field select">
                                        {!!Form::select('fee_type_id', $fee_receipt['arr_fee_type'],isset($fee_receipt['fee_type_id']) ? $fee_receipt['fee_type_id'] : '', ['class' => 'form-control','id'=>'selected_fee_type_id'])!!}
                                        <i class="arrow double"></i>
                                    </label>
                                    @if($errors->has('fee_type_id')) 
                                    <p class="help-block">{{ $errors->first('fee_type_id') }}</p>
                                    @endif
                                </div>
                                <div class="clearfix"></div>
                                <!-- end section -->
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12">
                        <!-- <div class=" mv40 sectionDuibe">
                            <span> <img src="../../public/rs.png" width="2%"> Student's Fee </span>
                        </div> -->
                        <div class="row marginRemove" id="spy1">
                            <span style="color:#DE888A; display: none;font-size: 0.85em;padding-left: 30px;" id="paid_amount_error">Please enter amount in installment(s)</span>
                            <div id="fee-tables">
                                @include("backend.student-fee-receipt.dummy-table")
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
	        <div class="section row" id="spy1">
		<div class="col-md-2">
		    <label><span class="radioBtnpan">{{trans('language.total_amount')}}</span></label>
		    <label for="datepicker1" class="field prepend-icon">
		        {!! Form::number('total_amount', old('total_amount',isset($fee_receipt['total_amount']) ? $fee_receipt['total_amount'] : ''), ['class' => 'gui-input ','id' => 'total_amount','readonly' => 'true','min'=>0]) !!}  
		    </label>
		</div>
		<div class="col-md-2">
		    <div class="option-group field" id="checkBooxx">
		        <label class="option block option-primary" style="margin-top:22px;">
			{!! Form::checkbox('auto_adjusted','','', ['class' => 'gui-input ', 'id' => 'auto_adjusted']) !!}
			<span class="checkbox radioBtnpan"></span><em>{{trans('language.auto_adjust')}} </em>
		        </label>
		    </div>
		</div>
		<div class="col-md-2">
		    <label><span class="radioBtnpan">{{trans('language.total_fine')}}</span></label>
		    <label for="datepicker1" class="field prepend-icon">
		        {!! Form::text('fine_amount', old('fine_amount',isset($fee_receipt['fine_amount']) ? $fee_receipt['fine_amount'] :0), ['class' => 'gui-input ','id' => 'fine_amount','readonly'=>true]) !!}
		    </label>
		</div>
		<div class="col-md-2">
		    <div class="option-group field" id="fine_apply">
		        <label class="option block option-primary" style="margin-top:22px;">
			{!! Form::checkbox('fine_apply','','', ['class' => 'gui-input ', 'id' => 'fine_apply']) !!}
			<span class="checkbox radioBtnpan"></span><em>{{trans('language.fine_apply')}} </em>
		        </label>
		    </div>
		</div>
		<div class="col-md-2">
		    <label><span class="radioBtnpan">{{trans('language.total_discount')}}</span></label>
		    <label for="datepicker1" class="field prepend-icon">
		        {!! Form::number('discount_amount', old('discount_amount',isset($fee_receipt['discount_amount']) ? $fee_receipt['discount_amount'] : ''), ['class' => 'gui-input ','id' => 'discount_amount','readonly'=>true,'min'=>0]) !!}
		    </label>
		</div>
		<div class="col-md-2">
		    <div class="option-group field" id="discount_apply">
		        <label class="option block option-primary" style="margin-top:22px;">
			{!! Form::checkbox('discount_apply','','', ['class' => 'gui-input ', 'id' => 'discount_apply']) !!}
			<span class="checkbox radioBtnpan"></span><em>{{trans('language.discount_apply')}} </em>
		        </label>
		    </div>
		</div>

		<div class="col-md-2">
		    <label><span class="radioBtnpan">{{trans('language.transport_amount')}}</span></label>
		    <label for="datepicker1" class="field prepend-icon">
		        {!! Form::number('transport_total_amount', old('transport_total_amount',isset($fee_receipt['transport_total_amount']) ? $fee_receipt['transport_total_amount'] : ''), ['class' => 'gui-input ', 'id' => 'transport_total_amount','readonly' => 'true','min'=>0]) !!}
		    </label>
		</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="section row" id="spy1">
		<div class="col-md-2">
		    <label><span class="radioBtnpan">{{trans('language.net_amount')}}</span></label>
		    <label for="datepicker1" class="field prepend-icon">
		        {!! Form::number('net_amount', old('net_amount',isset($fee_receipt['net_amount']) ? $fee_receipt['net_amount'] : ''), ['class' => 'gui-input ',''=>trans('language.net_amount'), 'id' => 'net_amount','readonly' => 'true','min'=>0]) !!}
		    </label>
		</div>
		<div class="col-md-2">
		    <label><span class="radioBtnpan">{{trans('language.payment_mode')}}</span></label>
		    <label for="datepicker1" class="field select">
		        {!!Form::select('payment_mode_id', $fee_receipt['arr_payment_mode'],isset($fee_receipt['payment_mode_id']) ? $fee_receipt['payment_mode_id'] : '', ['class' => 'form-control', 'id'=>'payment_mode_id'])!!}
		    </label>
		</div>
		<div class="col-md-2">
		    <label><span class="radioBtnpan">{{trans('language.bank')}}</span></label>
		    <label for="datepicker1" class="field select">
		        {!!Form::select('bank_id', $fee_receipt['arr_bank'],isset($fee_receipt['bank_id']) ? $fee_receipt['bank_id'] : '', ['class' => 'form-control','id'=>'bank_id'])!!}
		    </label>
		</div>
		<div class="col-md-2">
		    <div class="option-group field" id="checkBooxx">
		        <label class="option block option-primary" style="margin-top:22px;">
			{!! Form::checkbox('is_sms_send','',true, ['class' => 'gui-input ', 'id' => 'is_sms_send']) !!}
			<span class="checkbox radioBtnpan"></span><em>{{trans('language.is_sms_send')}} </em>
		        </label>
		    </div>
		</div>
		<div class="col-md-2">
		    <div class="option-group field" id="checkBooxx" style="margin-top:22px;">
		        <label class="option block option-primary">
			{!! Form::checkbox('is_receipt_print','',true, ['class' => 'gui-input ', 'id' => 'is_receipt_print']) !!}
			<span class="checkbox radioBtnpan"></span><em>{{trans('language.is_receipt_print')}} </em>
		        </label>
		    </div>
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-2">
		    {!! Form::submit($submit_button, ['class' => 'button btn-primary','id'=>'submit_form','disabled' =>'disabled','style'=>'width:125px !important;height: 27px !important;line-height: 27px !important;margin-top:12%;']) !!}
		</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="section row" id="spy1">
		<div class='cheque_cash' style="display:none;">
		    <div class="col-md-2">
		        <label><span class="radioBtnpan">{{trans('language.cheque_number')}}</span></label>
		        <label for="cheque_number" class="field prepend-icon">
			{!! Form::text('cheque_number', old('cheque_number',isset($fee_receipt['cheque_number']) ? $fee_receipt['cheque_number'] : ''), ['class' => 'gui-input ', 'id' => 'cheque_number']) !!}
		        </label>

		    </div>
		    <div class="col-md-2">
		        <label><span class="radioBtnpan">{{trans('language.cheque_date')}}</span></label>
		        <label for="cheque_date" class="field prepend-icon">
			{!! Form::text('cheque_date', old('cheque_date',isset($fee_receipt['cheque_date']) ? $fee_receipt['cheque_date'] : ''), ['class' => 'gui-input date_picker',''=>trans('language.cheque_date'), 'id' => 'cheque_date', 'readonly'=>'readonly']) !!}
		        </label>
		    </div>
		</div>
		<div class='online_paytm' style="display:none;">
		    <div class="col-md-2">
		        <label><span class="radioBtnpan">{{trans('language.transaction_id')}}</span></label>
		        <label for="transaction_id" class="field prepend-icon">
			{!! Form::text('transaction_id', old('transaction_id',isset($fee_receipt['transaction_id']) ? $fee_receipt['transaction_id'] : ''), ['class' => 'gui-input ',''=>trans('language.transaction_id'), 'id' => 'transaction_id']) !!}
		        </label>
		    </div>

		    <div class="col-md-2">
		        <label><span class="radioBtnpan">{{trans('language.transaction_date')}}</span></label>
		        <label for="transaction_date" class="field prepend-icon">
			{!! Form::text('transaction_date', old('transaction_date',isset($fee_receipt['transaction_date']) ? $fee_receipt['transaction_date'] : ''), ['class' => 'gui-input date_picker',''=>trans('language.transaction_date'), 'id' => 'transaction_date', 'readonly'=>'readonly']) !!}
		        </label>
		    </div>
		</div>
	        </div>
                </div>
            </div>
            <!-- end .form-footer section -->
        </div>
    </div>
</div>
</div>
<div id='dummy-student-data-table' style="display:none">
    <!-- <div class="col-md-12">
        <div class=" row" id="highlighid">
            <div class="col-md-4">
                <label><b class="hightli">{{trans('language.care_of_name')}} : </b></label>  
                <span id="s_father_name"></span>
            </div>
        </div>
    </div> -->
    <div class="col-md-12" id="studentDetails_fee">
        <table class="table table-striped" id="student-list">
            <thead>
                <tr>
                    <th>S.No.</th>
                    <th>Name</th>
                    <th>{{trans('language.care_of_name')}}</th>
                    <th>Scholar No.</th>
                    <th>Class</th>
                    <th class="text-center"> Action</th>
                </tr>
            </thead>
            <tbody>
                <tr id="student-tr">
                    <td colspan="5" style="text-align:center; color:#DE888A;font-size: 12px;">No student(s) available in table.</td>
                </tr>
            </tbody>
        </table>
        <div class="clearfix"></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var dummy_student = $("#dummy-student-data-table").html();
        $("#parent_student_list").html(dummy_student);

        // receipt date calendar will based on session date
        var minDate = '<?php
    if (!empty($fee_receipt['start_date']))
    {
        echo $fee_receipt['start_date'];
    }
?>';
        var maxDate = '<?php
    if (!empty($fee_receipt['end_date']))
    {
        echo $fee_receipt['end_date'];
    }
?>';
        $("#receipt_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: minDate,
            maxDate: maxDate
        });
        $("#fee-parameter-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                receipt_date: {
                    required: true,
                },
                payment_mode_id: {
                    required: true,
                },
                cheque_number: {
                    required: function (e) {
                        if ($("#payment_mode_id option:selected").text().toLowerCase() == 'cheque')
                        {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                cheque_date: {
                    required: function (e) {
                        if ($("#payment_mode_id option:selected").text().toLowerCase() == 'cheque')
                        {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                transaction_id: {
                    required: function (e) {
                        if (($("#payment_mode_id option:selected").text().toLowerCase() == 'online') || ($("#payment_mode_id option:selected").text().toLowerCase() == 'paytm'))
                        {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                transaction_date: {
                    required: function (e) {
                        if (($("#payment_mode_id option:selected").text().toLowerCase() == 'online') || ($("#payment_mode_id option:selected").text().toLowerCase() == 'paytm')) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                total_amount: {
                    required: function (e) {
                        if ($("#total_amount").val() > 0) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                },
                net_amount: {
                    required: true,
                },
                is_sms_send: {
                    required: true,
                },
                'bank_id': {
                    required: function (e) {
                        if ($("#payment_mode_id option:selected").text().toLowerCase() == 'cheque') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
//                'discount_apply': {
//                    required: function (e) {
//                        if ($("#discount_amount").val() != 0) {
//                            return true;
//                        } else {
//                            return false;
//                        }
//                    }
//                },
//                'fine_apply': {
//                    required: function (e) {
//                        if ($("#fine_amount").val() != 0) {
//                            return true;
//                        } else {
//                            return false;
//                        }
//                    }
//                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        $('#receipt_date').datepicker().on('change', function (ev) {
            $(this).valid();
        });
        // submit form
        $('#submit_form').on('click', function (e) {
            e.preventDefault();
            var status = '';
            status = true;
            $(".valid_empty_value").each(function (e) {
                var entered_paid_amount = $(this).val();
                entered_paid_amount = parseInt(entered_paid_amount);
                if (isNaN(entered_paid_amount))
                {
                    var entered_paid_amount = 0;
                }

                if (entered_paid_amount > 0)
                {
                    status = false;
                }
            });
            if (status == true)
            {
                $("#paid_amount_error").show();
                return false;
            } else
            {
                $("#paid_amount_error").hide();
            }
            if ($("#fee-parameter-form").valid())
            {
                var total_fine_amount = $("#fine_amount").val();
                var total_discount_amount = $("#discount_amount").val();
                var extra_amount_message = '';
                total_fine_amount = parseInt(total_fine_amount);
                if (isNaN(total_fine_amount))
                {
                    var total_fine_amount = 0;
                }
                total_discount_amount = parseInt(total_discount_amount);
                if (isNaN(total_discount_amount))
                {
                    var total_discount_amount = 0;
                }
                if ((total_fine_amount != '' && total_fine_amount > 0) && (total_discount_amount != '' && total_discount_amount > 0))
                {
                    extra_amount_message = 'Rs. ' + total_fine_amount + ' is fine amount and Rs. ' + total_discount_amount + ' is discount amount, Do you want to update net amount and continue?';
                } else if ((total_fine_amount != '' && total_fine_amount > 0))
                {
                    extra_amount_message = 'Rs. ' + total_fine_amount + ' is fine amount, Do you want to apply fine and continue?';
                } else if ((total_discount_amount != '' && total_discount_amount > 0))
                {
                    extra_amount_message = 'Rs. ' + total_discount_amount + ' is discount amount, Do you want to apply discount and continue?';
                }

                if (total_fine_amount > 0 || total_discount_amount > 0)
                {
                    bootbox.confirm({
                        message: extra_amount_message,
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if (result)
                            {
                                $('#fine_apply').prop('checked', true);
                                $('#discount_apply').prop('checked', true);
                                updateTotalNetAmount();
                                submitForm();
                            } else {
                                var arr_student = checkedStudent();
                                var total_pending_without_fine = 0;
                                total_pending_without_fine = parseInt(total_pending_without_fine);
                                var total_checked_student = arr_student.length;
                                for (var i = 0; i < total_checked_student; i++)
                                {
                                    var student_id = arr_student[i];

                                    var student_pending_amount = $("#without_fine_pending_amount_" + student_id).val();
                                    student_pending_amount = parseInt(student_pending_amount);
                                    if (isNaN(student_pending_amount))
                                    {
                                        var student_pending_amount = 0;
                                    }
                                    total_pending_without_fine = total_pending_without_fine + student_pending_amount;
                                }
                                var total_entered_amount = $("#total_amount_hidden").val();
                                total_entered_amount = parseInt(total_entered_amount);
                                if (isNaN(total_entered_amount))
                                {
                                    var total_entered_amount = 0;
                                }
                                if (total_entered_amount > total_pending_without_fine)
                                {
                                    $(".transport_pay_amount").val(null);
                                    $(".fee_installment_paid_amount").val(null);
                                    $('.fee_installment_paid_amount').attr('fine-auto', '');
                                    clearFields();
                                } else
                                {
//                                $('.student_discount').val(0);
//                                $('#discount_amount').val(0);
                                    $('#fine_apply').prop('checked', false);
                                    $('#discount_apply').prop('checked', false);
                                    $(".student_fine").val(null);
                                    $('.fee_installment_paid_amount').attr('fine-auto', '');
                                    if ($("#auto_adjusted").is(":checked"))
                                    {
                                        $(".student_fine").val(null);
                                        var fine_calculate = checkFineApplyStatus();
                                        distributeTotalAmount(fine_calculate);
                                    }
                                    updateTotalNetAmount();
                                    submitForm();
                                }
                            }
                        }
                    });
                } else {
                    submitForm();
                }

            } else
            {
                return false;
            }
        });

        function submitForm()
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('admin/student-fee-receipt/save/')}}",
                datatType: 'json',
                type: 'GET',
                data: $('#fee-parameter-form').serialize(),
                success: function (response) {
                    if (response.status == 'success')
                    {
                        $('#server-response-success').text(response.message);
                        $('#alertmsg-success').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
		 var arr_student = response.data.arr_student;
                            studentFeeTables(arr_student);
//                        $('#alertmsg-success').on('hidden.bs.modal', function (e) {

                            if ($("#is_receipt_print").is(":checked"))
                            {
		        var time = new Date().getTime();
                                var paid_receipt = response.data.paid_receipt;
                                newWin = window.open("");
                                newWin.document.write('<html><head>');
                                newWin.document.title = "Student-fees-"+time;
                                newWin.document.write('<head><style type="text/css">h1,h2,h3,h4,h5,h6{padding: 0px;margin: 0px;}p{padding: 0px;margin: 0px;}.tabletr tr td{line-height: 27px;}.border-left td{border-left: 0px !important; }.pagebreak { page-break-before: always; }</style></head></head><body>');
                                newWin.document.write(paid_receipt);
                                newWin.document.write('</body></html>');
                                setTimeout(function() {
		        newWin.print();
		        newWin.close();
		        }, 100);
                            }
                           
//                        });

                    } else
                    {
                        $('#server-response-message').text('Something went wrong');
                        $('#alertmsg-student').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                }
            });
        }

        // onchange student event
        $("#student_id").select2().on('change', function (e) {
            var dummy_student = $("#dummy-student-data-table").html();
            $("#parent_student_list").html(dummy_student);
            $("#student_tables").html('');
            $("#fee-tables").html('@include("backend.student-fee-receipt.dummy-table")');
            clearFields();
            var arr_student = [];
            var student_id = $(this).val();
            $('#submit_form').prop('disabled', true);
            if (student_id != '')
            {
                $("#selected_student_id").val(student_id);
                arr_student.push(student_id);
                studentFeeTables(arr_student);
            }
        });

        // auto adjust amount
        $('#auto_adjusted').on('click', function (e) {
            $("#total_amount").val(null);
            $("#net_amount").val(null);
            $("#discount_amount").val(null);
            $("#fine_amount").val(null);
            $(".student_fine").val(null);
            $(".student_discount").val(null);
            $(".fee_installment_paid_amount").val(null);

            if ($(this).prop("checked") == true) {
                $('#total_amount').prop('readonly', false);
                $('#fine_apply').prop('checked', true);
                $('.fee_installment_paid_amount').prop('readonly', true);
            } else {

                $('#fine_apply').prop('checked', false);
                $('#total_amount').prop('readonly', true);
                $('.fee_installment_paid_amount').attr('fine-auto', '');
                $('.fee_installment_paid_amount').prop('readonly', false);
                //remove amount 
                clearFields();
                $("#total_amount").val(null);
                $(".fee_installment_paid_amount").val(null);
            }

        });
    });
    function checkTotalEnteredAutoAmount() {

        var total_amount = $('#total_amount').val();
        total_amount = parseInt(total_amount);
        var fine_calculate = checkFineApplyStatus();
        if (isNaN(total_amount))
        {
            var total_amount = 0;
        }
//        alert(total_amount);
        var total_pending_amount = $("#total_pending_amount").val();
        var without_fine_total_pending_amount = $("#without_fine_total_pending_amount").val();
        total_pending_amount = parseInt(total_pending_amount);
        if (isNaN(total_pending_amount))
        {
            var total_pending_amount = 0;
        }
        if (isNaN(without_fine_total_pending_amount))
        {
            var without_fine_total_pending_amount = 0;
        }

        if ((total_amount > 0) &&
                (total_amount <= total_pending_amount && fine_calculate === 'yes') ||
                (total_amount <= without_fine_total_pending_amount && fine_calculate !== 'yes'))
        {
            $("#total_amount_hidden").val(total_amount);
            distributeTotalAmount(fine_calculate);
        } else
        {
            $('.fee_installment_paid_amount').val(null);
            clearFields();
            $('#auto_adjusted').prop('checked', true);
            $('#fine_apply').prop('checked', true);

            if (total_amount > total_pending_amount && fine_calculate === 'yes')
            {
                $(this).val(0);
                $('#server-response-message').text("You can't enter amount more than total pending amount");
                $('#alertmsg-student').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
            if (total_amount > without_fine_total_pending_amount && fine_calculate !== 'yes')
            {
                $(this).val(0);
                $('#server-response-message').text("You can't enter amount more than total pending amount without fine");
                $('#alertmsg-student').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
        }
    }
    //distribute auto adjust amount to student installments equally
    $('#total_amount').on('change', function (e) {
        // check total amount
        $('.fee_installment_paid_amount').attr('fine-auto', '');
        $(".student_fine").val(null);
        $("#fine_amount").val(null);
//         var total_amount = $(this).val();
//        total_amount = parseInt(total_amount);
//        if (isNaN(total_amount))
//        {
//            var total_amount = 0;
//        }
//        $("#total_amount_hidden").val(total_amount);
        checkTotalEnteredAutoAmount();
    });

    function distributeAmountInStudent(fine_calculate)
    {
        var total_entered_amount = $("#total_amount_hidden").val();
        total_entered_amount = parseInt(total_entered_amount);
        var arr_student = checkedStudent();
        var total_checked_student = arr_student.length;
        var each_student_amount = parseInt(total_entered_amount / total_checked_student);
        var arr_student_list = [];
        var new_student_list = [];
        for (var i = 0; i < total_checked_student; i++)
        {
            var student_id = arr_student[i];
            if (fine_calculate == 'yes')
            {
                var student_pending_amount = $("#student_pending_amount_" + student_id).val();
            } else
            {
//                alert(fine_calculate);
                var student_pending_amount = $("#without_fine_pending_amount_" + student_id).val();
//                 alert(student_pending_amount);
            }

            student_pending_amount = parseInt(student_pending_amount);
//            alert(student_pending_amount);
            if (isNaN(student_pending_amount))
            {
                var student_pending_amount = 0;
            }
            arr_student_list.push({student_id: student_id, amount: student_pending_amount});
        }
        var sorted_student = arr_student_list.sort(function (a, b) {
            return a.amount - b.amount;
        });

        // assign amount to students whose pending is less than divided amount
        $.each(sorted_student, function (key, value) {
            var student_id = value.student_id;
            var student_pending_amount = value.amount;
//            alert(student_pending_amount);
            if (each_student_amount > student_pending_amount)
            {
                if (student_pending_amount > 0)
                {
                    $("#student_assigned_amount_" + student_id).val(student_pending_amount);
                    total_entered_amount = (total_entered_amount - student_pending_amount);
                }
            } else
            {
                new_student_list.push(student_id);
            }
        });

        var final_student_list = [];
        if (new_student_list.length > 0)
        {
            final_student_list = new_student_list;
        } else
        {
            final_student_list = arr_student;
        }
        // assign amount to students whose pending is greater than divided amount
//        alert(total_entered_amount);
        var total_pending_student = final_student_list.length;
//        alert(total_pending_student);
        var student_amount = parseInt(total_entered_amount / total_pending_student);
//        alert(student_amount);
        var will_assign_amount = parseInt(student_amount * total_pending_student);
        var amount_diff = parseInt(total_entered_amount - will_assign_amount);
        for (var i = 0; i < total_pending_student; i++)
        {
            var studentid = final_student_list[i];
            if (i === (total_pending_student - 1))
            {
                student_amount = parseInt(student_amount) + parseInt(amount_diff);
            }
            $("#student_assigned_amount_" + studentid).val(student_amount);

        }
    }

    function distributeTotalAmount(fine_calculate)
    {
        distributeAmountInStudent(fine_calculate);
        var total_entered_amount = $("#total_amount_hidden").val();
//        console.log('total amount '+total_entered_amount);
        var arr_student = checkedStudent();
        var total_checked_student = arr_student.length;
        $(".fee_installment_paid_amount").val(null);
        if (total_entered_amount >= 0)
        {
            for (var i = 0; i < total_checked_student; i++)
            {
                var student_id = arr_student[i];
//                console.log('student_id'+student_id);
                var student_amount_assign = $("#student_assigned_amount_" + student_id).val();
//                console.log('assign : ' + student_amount_assign);
                student_amount_assign = parseInt(student_amount_assign);

                // discount in case of auto adjust
//                if ($("#auto_adjusted").is(":checked"))
//                {
//                    var disount = $("#discount_amount_" + student_id).val();
//                    disount = parseInt(disount);
//                    if (isNaN(disount))
//                    {
//                        disount = 0;
//                    }
//                    student_amount_assign = student_amount_assign + disount;
//                }

                $(".student_installments_" + student_id).each(function () {
                    if (student_amount_assign > 0)
                    {
                        var rel = $(this).attr('rel');
                        var fee_amount = $(this).attr('data-fee-amount');
//                        console.log('iffeed_amount :' + fee_amount);
                        fee_amount = parseInt(fee_amount);
                        if (isNaN(fee_amount))
                        {
                            var fee_amount = 0;
                        }
                        if (student_amount_assign > fee_amount)
                        {
                            $(this).val(fee_amount);
                            var paid_amount = $(this).val();
                            console.log('send :' + student_amount_assign);
                            console.log('send rel :' + rel);
                            var fine_amount = calculateFine(rel, paid_amount, fine_calculate, student_amount_assign);
//                             console.log('iffine_amount :' +fine_amount);
                            student_amount_assign = student_amount_assign - fine_amount;
//                            console.log('ifstudent_amount_assign :' +student_amount_assign);
                            if (student_amount_assign > fee_amount)
                            {
                                student_amount_assign = student_amount_assign - fee_amount;
//                                 console.log(rel+'student_amount_assign :' +student_amount_assign);
                            } else
                            {
                                $(this).val(student_amount_assign);
                                student_amount_assign = 0;
                            }
                        } else
                        {
                            $(this).val(student_amount_assign);
                            var paid_amount = $(this).val();
                            // console.log('how to pay :' +paid_amount);
                            var fine_amount = calculateFine(rel, paid_amount, fine_calculate);
//                            console.log('fine_amount :' +fine_amount);
                            student_amount_assign = student_amount_assign - fine_amount;
//                            console.log('student_amount_assign :' +student_amount_assign);
                            $(this).val(student_amount_assign);
                            student_amount_assign = 0;
                        }
                        calculateFeeInstallment(fine_calculate);
                    }
                });
            }
        }
    }
    // filter fee by fee type
    $('#selected_fee_type_id').on('change', function (e) {
        e.preventDefault();
        var arr_student = checkedStudent();
        studentFeeTables(arr_student);
    });

// search students by class id
    $(document).on('change', '#search_class_id', function (e) {
        $('select[name="student_id"]').empty();
        var dummy_student = $("#dummy-student-data-table").html();
        $("#parent_student_list").html(dummy_student);
        $("#student_tables").html('');
//        $("#fee-tables").html('');
        $("#fee-tables").html('@include("backend.student-fee-receipt.dummy-table")');
        clearFields();
        e.preventDefault();
        var class_id = $(this).val();
        if (class_id != '')
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-student-list')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'class_id': class_id,
                },
                beforeSend: function () {
                    $('#LoadingImage').show();
                },
                success: function (response) {
                    var resopose_data = [];
                    resopose_data = response.data;
                    if (response.status == 'success')
                    {
                        $('select[name="student_id"]').append('<option value="">--Select Student--</option>');
                        $.each(resopose_data, function (key, value) {
                            $('select[name="student_id"]').append('<option value="' + key + '">' + value + '</option>');

                        });

                    }
                    $('#LoadingImage').hide();
                }
            });
        }
    });

    // show payment mode according to its master
    $('#payment_mode_id').on('change', function (e) {
        var payment_mode = $("#payment_mode_id option:selected").text().toLowerCase();
        var payment_mode_id = $(this).val();
        if (payment_mode_id != null)
        {
            if (payment_mode == 'cheque')
            {
                $(".cheque_cash").show();
                $(".online_paytm").hide();
            } else if (payment_mode == 'online' || payment_mode == 'paytm')
            {
                $(".cheque_cash").hide();
                $(".online_paytm").show();
            } else
            {
                $(".cheque_cash").hide();
                $(".online_paytm").hide();
            }
        } else
        {
            $(".cheque_cash").hide();
            $(".online_paytm").hide();
        }

    });
    // search parent by contact no 
    $(document).on('click', '#search_parent', function (e) {

        var dummy_student = $("#dummy-student-data-table").html();
        $("#parent_student_list").html(dummy_student);
//        $("#fee-tables").html('');
        $("#fee-tables").html('@include("backend.student-fee-receipt.dummy-table")');
        var parent_contact = $("#parent_contact_number").val();
        if (parent_contact !== '')
        {
            $.ajax(
                    {
                        url: "{{ url('get-parent-student') }}",
                        datatType: 'json',
                        type: 'POST',
                        data: {
                            'parent_contact': parent_contact,
                        },
                        beforeSend: function () {
                            $('#LoadingImage').show();
                        },
                        success: function (res)
                        {
                            if (res.status == "success")
                            {
                                $("#parent_student_list").html(res.data);
                                $('#exampleModalCenter').modal('hide');
                            }
                            $('#LoadingImage').hide();
                        }
                    });
        } else
        {
            $('.modal-body').text('Please enter C/o contact no');
            $('#alertmsg-student').modal({
                backdrop: 'static',
                keyboard: false
            });
        }

    });

    // get all students fee by parent
    $(document).on('click', '#parent_student_fee', function (e) {
        var arr_student = checkedStudent();
        studentFeeTables(arr_student);
    });

    // make fee tables empty if all student unchecked
    $(document).on('click', '.parent_child', function (e) {
        if (!$(this).is(':checked'))
        {
            var arr_student = checkedStudent();
            if (arr_student.length == 0)
            {
                clearFields();
                $('#submit_form').prop('disabled', true);
//                $("#fee-tables").html('');
                $("#fee-tables").html('@include("backend.student-fee-receipt.dummy-table")');
            }
        }
    });

    function checkedStudent()
    {
        var arr_student = [];
        var fee_request = $("#fee_request").val();
        if (fee_request === 'parent')
        {
            $(".parent_child:checked").each(function () {
                var student_id = $(this).attr('student-id');
                arr_student.push(student_id);
            });
        } else if (fee_request === 'student')
        {
            var student_id = $("#selected_student_id").val();
            if (student_id !== '')
            {
                arr_student.push(student_id);
            }
        }
        return arr_student;
    }

    function studentFeeTables(arr_student)
    {
        if (arr_student.length > 0)
        {
            $.ajax(
                    {
                        url: "{{ url('get-student-fee-details') }}",
                        datatType: 'json',
                        type: 'POST',
                        data: {
                            'arr_student': arr_student,
                            'fee_type_id': $("#selected_fee_type_id").val(),
                        },
                        beforeSend: function () {
                            $('#LoadingImage').show();
                        },
                        success: function (res)
                        {
                            if (res.status == "success")
                            {
                                clearFields();
                                $("#fee-tables").html(res.data);
                                var fee_request = $("#fee_request").val();
                                if (fee_request == 'student')
                                {
                                    $("#student-tr").html('<td>1</td><td>' + res.student_name + '</td><td>' + res.care_of_name + '</td><td>' + res.enrollment_number + '</td><td>' + res.class_name + '</td><td><a title="view remark" class="btn btn-info view_remark centerAlign" style="text-align:center;" student-remark="' + res.remark + '"><i class="fa fa-eye" ></i></a></td>');
                                    $("#s_father_name").text(res.care_of_name);
                                }
                                if ($(".valid_empty_value").length > 0)
                                {
                                    $('#submit_form').prop('disabled', false);
                                } else
                                {
                                    $('#submit_form').prop('disabled', true);
                                }

                            }
                            $('#LoadingImage').hide();
                        }
                    });
        } else
        {
            $('.modal-body').text('Please select student(s)');
            $('#alertmsg-student').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    }

    $(document).on('click', '#sibling_exist', function (e) {
        $('#submit_form').prop('disabled', true);
        $("#fee-tables").html('@include("backend.student-fee-receipt.dummy-table")');
        if ($('input[name=sibling_exist]').is(":checked"))
        {
            $("#fee_request").val('parent');
            $("#single_student").hide();
            $("#multiple_student").show();
//            $("#fee-tables").html('');
//            $("#parent_student_list").html('');
        } else
        {
            $("#fee_request").val('student');
            $('select[name="search_class_id"]').val(null);
            $('select[name="student_id"]').val(null).trigger('change');
            $("#single_student").show();
            $("#multiple_student").hide();
//            $("#parent_student_list").html('');
//            $("#fee-tables").html('');
        }
        var dummy_student = $("#dummy-student-data-table").html();
        $("#parent_student_list").html(dummy_student);

    });

    // calculate each student's installment amount
    $(document).on('change', '.fee_installment_paid_amount', function (e) {
        var rel = $(this).attr('rel');
        $('#paid_amount_' + rel).attr('fine-auto', '');
        var fine_calculate = 'yes';
        calculateFeeInstallment(fine_calculate);
    });

    function calculateFeeInstallment(fine_calculate)
    {
        
        var total_paid_amount = 0;
        $("#paid_amount_error").hide();
        $(".fee_installment_paid_amount").each(function () {

            var rel = $(this).attr('rel');
            var paid_amount = $(this).val();

            $('#add_' + rel).prop('disabled', false);
            $('#remove_' + rel).prop('disabled', false);

            paid_amount = parseInt(paid_amount);
            if (isNaN(paid_amount))
            {
                var paid_amount = 0;
            }

            var installment_fee_amount = $(this).attr('data-fee-amount');
            installment_fee_amount = parseInt(installment_fee_amount);
            if (paid_amount > installment_fee_amount)
            {
                $(this).val(0);
                $('#server-response-message').text("Entered amount can't be greater than fee amoun");
                $('#alertmsg-student').modal({
                    backdrop: 'static',
                    keyboard: false
                });

            } else
            {
                total_paid_amount = paid_amount + parseInt(total_paid_amount);
                calculateFine(rel, paid_amount, fine_calculate);

            }
        });
        $("#total_amount").val(total_paid_amount);

        // assign fine amount to total fine amount of student
        var arr_student = checkedStudent();
        var total_checked_student = arr_student.length;
        var total_fine_amount = 0;
//        console.log(total_checked_student);
        for (var i = 0; i < total_checked_student; i++)
        {
            var student_id = arr_student[i];
            var student_total_fine_amount = 0;

            $(".student_installments_" + student_id).each(function () {
                var installment_amount = $(this).val();
                installment_amount = parseInt(installment_amount);
                if (isNaN(installment_amount))
                {
                    var installment_amount = 0;
                }
                var fine_amount = $(this).attr('final-fine-amount');
                fine_amount = parseInt(fine_amount);
                if (isNaN(fine_amount))
                {
                    var fine_amount = 0;
                }
                if (installment_amount > 0)
                {
                    student_total_fine_amount = student_total_fine_amount + fine_amount;
                }
            });
            total_fine_amount = total_fine_amount + student_total_fine_amount;
//	console.log(total_fine_amount);
            $("#fine_amount_" + student_id).val(student_total_fine_amount);
        }
        $("#fine_amount").val(total_fine_amount);

        // update net amount with fine and discount
        updateTotalNetAmount();
    }
    // add installment amount when click on add button
    $(document).on('click', '.add_amount, .remove_amount', function (e) {

        var rel = $(this).attr('rel');
        var class_name = $(this).attr('data-clicked-class');
        //remove auto fine set flag 
        if ($("#auto_adjusted").is(":checked"))
        {
            bootbox.confirm({
                message: "If you click on this, it will cancel auto adjust process",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        clearFields();
                        addRemoveButtonTask(rel, class_name);
                    }
                }
            });
        } else
        {
            addRemoveButtonTask(rel, class_name);
        }
    });
    function addRemoveButtonTask(rel, class_name)
    {
        $('#paid_amount_' + rel).attr('fine-auto', '');
        var paid_amount = $("#add_" + rel).attr('data-paid-amount');
        if (isNaN(paid_amount))
        {
            var paid_amount = 0;
        }
        if (class_name === 'add_amount')
        {
            $('#add_' + rel).prop('disabled', true);
            $('#remove_' + rel).prop('disabled', false);
            $("#paid_amount_" + rel).val(paid_amount);
        } else if (class_name === 'remove_amount')
        {
            $("#paid_amount_" + rel).val(null);
            $('#add_' + rel).prop('disabled', false);
            $('#remove_' + rel).prop('disabled', true);
        }
        // update total, net amount etc
        var fine_calculate = 'yes';
        calculateFeeInstallment(fine_calculate);
    }
    // reverted paid installment
    $(document).on('click', '.delete_installment', function (e) {
        e.preventDefault();
        var receipt_id = $(this).attr("receipt-id");
        if (receipt_id !== '')
        {
            bootbox.confirm({
                message: "Are you sure to revert this installment ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('fee-installment/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'student_fee_receipt_id': receipt_id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status === "success")
                                        {
                                            var arr_student = checkedStudent();
                                            studentFeeTables(arr_student);
                                        }
                                    }
                                });
                    }
                }
            });
        }

    });
    function checkFineApplyStatus()
    {
        var fine_calculate = '';
        if ($("#fine_apply").is(':checked'))
        {
            fine_calculate = 'yes';
        } else
        {
            fine_calculate = 'no';
        }
        return fine_calculate;
    }

    $(document).on('click', '#fine_apply', function (e) {
        $('.fee_installment_paid_amount').attr('fine-auto', '');
        if ($("#auto_adjusted").is(":checked"))
        {
            $(".student_fine").val(null);
//            var fine_calculate = checkFineApplyStatus();
//            distributeTotalAmount(fine_calculate);
        }
        checkTotalEnteredAutoAmount();
        var fine_calculate = checkFineApplyStatus();
        distributeTotalAmount(fine_calculate);
        updateTotalNetAmount();
    });

    $(document).on('change', '#discount_apply', function (e) {

        if ($("#auto_adjusted").is(":checked"))
        {
//            $("#discount_amount").val(null);
//            $(".student_discount").val(null);
//            var fine_calculate = checkFineApplyStatus();
//            distributeTotalAmount(fine_calculate);
        }
        updateTotalNetAmount();

    });
    $(document).on('change', '.student_discount', function (e) {
        $("#discount_amount").val(null);
//        var rel = $(this).attr('rel');
//        var student_discount = $(this).val();
//        student_discount = parseInt(student_discount);
//        if (isNaN(student_discount))
//        {
//            student_discount = 0;
//        }
//        student_installments_1
//        var installment_selected_amount = 0;
//        installment_selected_amount = parseInt(installment_selected_amount);
//        $(".student_installments_" + rel).each(function () {
//            var installment_amount = $(this).val();
//            installment_amount = parseInt(installment_amount);
//            if (isNaN(installment_amount))
//            {
//                var installment_amount = 0;
//            }
//            var fine_amount = $(this).attr('final-fine-amount');
//            fine_amount = parseInt(fine_amount);
//            if (isNaN(fine_amount))
//            {
//                var fine_amount = 0;
//            }
//
//            installment_selected_amount = installment_selected_amount + installment_amount + fine_amount;
//        });
//        var student_pending_amount = $("#student_pending_amount_" + rel).val();
//        student_pending_amount = parseInt(student_pending_amount);
//        if (isNaN(student_pending_amount))
//        {
//            student_pending_amount = 0;
//        }
//        var final_pending = 0;
//        if (student_pending_amount >= installment_selected_amount)
//        {
//            final_pending = student_pending_amount - installment_selected_amount;
//        }
        if ($("#auto_adjusted").is(":checked"))
        {
//            if (student_discount > final_pending)
//            {
//                $(this).val(0);
//                $('.modal-body').text("Discount amount can't be greater student's pending fee amount");
//                $('#alertmsg-student').modal({
//                    backdrop: 'static',
//                    keyboard: false
//                });
//            }
            $("#discount_apply").prop("checked", true);
//            $('.fee_installment_paid_amount').attr('fine-auto', '');
//            var fine_calculate = checkFineApplyStatus();
//            distributeTotalAmount(fine_calculate);
        }
//        else
//        {
//            if (student_discount >= installment_selected_amount)
//            {
//                $(this).val(0);
//                $('.modal-body').text("Discount amount can't be greater student's pending fee amount");
//                $('#alertmsg-student').modal({
//                    backdrop: 'static',
//                    keyboard: false
//                });
//
//            }
//        }

        updateDiscountAmount();
    });

    function updateTotalNetAmount()
    {
        var net_amount = 0;
        var total_amount = $("#total_amount").val();
        total_amount = parseInt(total_amount);
        if (isNaN(total_amount))
        {
            total_amount = 0;
        }
        $("#total_amount").val(total_amount);
        var fine_amount = $("#fine_amount").val();
        fine_amount = parseInt(fine_amount);
        if (isNaN(fine_amount))
        {
            fine_amount = 0;
        }
        if ($('input[name=fine_apply]').is(":checked"))
        {
            net_amount = total_amount + fine_amount;
        } else
        {
            net_amount = total_amount;
        }
        if ($('input[name=discount_apply]').is(":checked"))
        {
            var discount_amount = $("#discount_amount").val();
            discount_amount = parseInt(discount_amount);
            if (isNaN(discount_amount))
            {
                discount_amount = 0;
            }
            if (net_amount > discount_amount)
            {
                net_amount = net_amount - discount_amount;
            }

        }
        var transport_total_amount = $("#transport_total_amount").val();
        transport_total_amount = parseInt(transport_total_amount);
        if (isNaN(transport_total_amount))
        {
            transport_total_amount = 0;
        }
        if (transport_total_amount > 0)
        {
            net_amount = net_amount + transport_total_amount;
        }
        // put sum of total itno total 
        $("#net_amount").val(net_amount);
    }
    function updateDiscountAmount()
    {
        // update discount amount
        var arr_student = checkedStudent();
        var total_checked_student = arr_student.length;
        var total_discount_amount = 0;
        var total_fine_amount = 0;
        for (var i = 0; i < total_checked_student; i++)
        {
            var student_id = arr_student[i];
            var student_discount_amount = $("#discount_amount_" + student_id).val();
            student_discount_amount = parseInt(student_discount_amount);
            var student_fine_amount = $("#fine_amount_" + student_id).val();
            student_fine_amount = parseInt(student_fine_amount);

            if (isNaN(student_discount_amount))
            {
                student_discount_amount = 0;
            }
            if (isNaN(student_fine_amount))
            {
                student_fine_amount = 0;
            }
            total_discount_amount = total_discount_amount + student_discount_amount;
            total_fine_amount = total_fine_amount + student_fine_amount;
        }
        $("#discount_amount").val(total_discount_amount);
        $("#fine_amount").val(total_fine_amount);
        updateTotalNetAmount();
    }

    function clearFields()
    {
        $('#receipt_date').val(null);
        $('#transport_total_amount').val(null);
        $('#total_amount').val(null);
        $('#discount_amount').val(null);
        $('#net_amount').val(null);
        $('#fine_amount').val(null);
        $('#transaction_id').val(null);
        $('#transaction_date').val(null);
        $('#cheque_number').val(null);
        $('#cheque_date').val(null);
        $('#payment_mode_id').val(null);
        $('#bank_id').val(null);
        $('.student_fine').val(null);
        $('.student_discount').val(null);
        $('#auto_adjusted').prop('checked', false);
        $('#fine_apply').prop('checked', false);
        $('#discount_apply').prop('checked', false);
    }
    function calculateFine(rel, paid_amount, fine_calculate, student_amount_assign = 0)
    {
//        console.log('get rel'+rel);
        var fine_amount = 0;
        var fine_type = $('#paid_amount_' + rel).attr('fine-type');
        var fine = $('#paid_amount_' + rel).attr('fine');
        var fine_days = $('#paid_amount_' + rel).attr('fine-days');
        var fine_auto = $('#paid_amount_' + rel).attr('fine-auto');
//        console.log('fine_auto : '+fine_auto);
        fine = parseInt(fine);
        // fine calculation of every installment according to fine type
        if (fine_calculate === 'yes')
        {
	
            // fine auto means (fine calculated automatic its used to prevent multiple fine calculate)
            if (fine_auto != 'yes')
            {
	    
//                console.log('yes in : '+fine_auto);
                if (fine > 0)
                {
	       
//                    console.log('yes in  fine h: '+fine);
                    if (fine_type == 1)
                    {
		if(fine_days > 0)
		{
//                         console.log('yes in  % h: '+fine_type);
                        var fine_val = fine / 100;
                        fine_amount = parseInt(paid_amount * fine_val);
                        var pay_amount = parseInt(paid_amount) + fine_amount;
//                        console.log('get : '+student_amount_assign);
//                        console.log('pay : '+pay_amount);
                        if (pay_amount > student_amount_assign)
                        {
		    
                            // total amount = x + x(fine/100);
                            // get x value and x will be installment amount
                            // the get fine of that x value
		    // this calculation will be used for auto adjust fine 
//                            var fine_x = 1 + fine_val;
//                            var installment_x = parseInt(student_amount_assign / fine_x);
//                            fine_amount = parseInt((installment_x) * fine_val);
//                            console.log('last fine'+fine_amount);
                        }
	        }
		
                    } else
                    {
                        fine_amount = parseInt(fine_days * fine);
                    }
                }
                if (isNaN(fine_amount))
                {
                    var fine_amount = 0;
                }
                $('#paid_amount_' + rel).attr('final-fine-amount', fine_amount);

                if (paid_amount > 0)
                {
                    $('#paid_amount_' + rel).attr('fine-auto', 'yes');
                }
            }
        } else
        {
            fine_amount = 0;
            $('#paid_amount_' + rel).attr('final-fine-amount', fine_amount);
        }
        return fine_amount;
    }


//   Transport Fee Calculation 
    $(document).on('click', '.transport_add_amount, .transport_remove_amount', function (e) {

        var rel = $(this).attr('rel');
        var class_name = $(this).attr('data-clicked-class');

        //remove auto fine set flag 
        var paid_amount = $("#transport_add_" + rel).attr('transport-paid-amount');
        if (isNaN(paid_amount))
        {
            var paid_amount = 0;
        }
        if (class_name == 'add_transport_amount')
        {
            $('#transport_add_' + rel).prop('disabled', true);
            $('#transport_remove_' + rel).prop('disabled', false);
            $("#transport_pay_amount_" + rel).val(paid_amount);
        } else if (class_name == 'remove_transport_amount')
        {
            $("#transport_pay_amount_" + rel).val(null);
            $('#transport_add_' + rel).prop('disabled', false);
            $('#transport_remove_' + rel).prop('disabled', true);
        }
        calculateTransportFeeInstallment();

    });
    $(document).on('change', '.transport_pay_amount', function (e) {
        calculateTransportFeeInstallment();
    });

    function calculateTransportFeeInstallment()
    {
        var transport_total_paid_amount = 0;
        $(".transport_pay_amount").each(function () {

            var rel = $(this).attr('rel');
            var paid_amount = $(this).val();

            $('#transport_add_' + rel).prop('disabled', false);
            $('#transport_remove_' + rel).prop('disabled', false);
            paid_amount = parseInt(paid_amount);
            if (isNaN(paid_amount))
            {
                var paid_amount = 0;
            }
            var installment_fee_amount = $("#transport_pay_amount_" + rel).attr('trans-data-fee-amount');
            installment_fee_amount = parseInt(installment_fee_amount);
            if (paid_amount > installment_fee_amount)
            {
                $(this).val(0);
                $('#server-response-message').text("Entered amount can't  be greater than fee amount");
                $('#alertmsg-student').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else
            {
                transport_total_paid_amount = paid_amount + parseInt(transport_total_paid_amount);
            }
        });
        $("#transport_total_amount").val(transport_total_paid_amount);
        updateTotalNetAmount();
    }
    $(document).on('click', '.revert_transport_installment', function (e) {
        e.preventDefault();
        var receipt_id = $(this).attr("receipt-id");
        if (receipt_id !== '')
        {
            bootbox.confirm({
                message: "Are you sure to revert this installment ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('transport-fee-installment/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'transport_fee_id': receipt_id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status === "success")
                                        {
                                            var arr_student = checkedStudent();
                                            studentFeeTables(arr_student);
                                        }
                                    }
                                });
                    }
                }
            });
        }

    });

    $(document).on('click', '.view_remark', function () {
        var remark = $(this).attr('student-remark');
        $('#student-data-modal').text(remark);
        $('#remark-student-name').text();
        $('#show-remark').modal({
            backdrop: 'static',
            keyboard: false
        });
    });
</script>
