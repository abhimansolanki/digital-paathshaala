@extends('admin_panel/layout')
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
</style>
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span>View Fee</span>
                </div>
            </div>
            <div class="panel" id="transportId">
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('class_id', $arr_class,'', ['class' => 'form-control','id'=>'class_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('student_id', $arr_student,'', ['class' => 'form-control','id'=>'student_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('payment_status',$arr_payment_status,'', ['class' => 'form-control','id'=>'payment_status'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-detail-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>S.No</th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>Stu. Name</th>
                            <th>{{trans('language.class_name')}}</th>
                            <th>{{trans('language.section_name')}}</th>
                            <th>{{trans('language.care_of_name')}}</th>
                            <th>{{trans('language.care_of_contact')}}</th>
                            <th>{{trans('language.total_amount')}}</th>
                            <th>{{trans('language.paid_amount')}}</th>
                            <th>{{trans('language.pending_amount')}}</th>
                            <th>{{trans('language.pay')}}</th>

                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        studentFeeReport();
        function studentFeeReport()
        {
            var table = $('#student-detail-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
//                bFilter:false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'Student Fee Report',
                        "filename": 'student-fee-report',
                        exportOptions: {
//                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Student Fee Report',
                        "filename": 'student-fee-report',
                        exportOptions: {
//                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
                            modifier: {
                                selected: true
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: "{{ url('fee/data')}}",
                    data: function (f) {
                        f.class_id = $('#class_id').val();
                        f.student_id = $('#student_id').val();
                        f.payment_status = $('#payment_status').val();
                    }
                },
                columns: [
                    {data: 'student_id', name: 'student_id'},
                    {data: 'DT_Row_Index', name: 'DT_Row_Index'},
                    {data: 'enrollment_number', name: 'enrollment_number'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'class_name', name: 'class_name'},
                    {data: 'section_name', name: 'section_name'},
                    {data: 'care_of_name', name: 'care_of_name'},
                    {data: 'care_of_contact', name: 'care_of_contact'},
                    {data: 'total_amount', name: 'total_amount'},
                    {data: 'paid_amount', name: 'paid_amount'},
                    {data: 'pending_amount', name: 'pending_amount'},
                    {data: 'pay', name: 'pay'},
                ]
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
            table.on('select deselect', function (e, dt, type, indexes) {
                var arr_checked_student = checkedStudent();
                if (arr_checked_student.length > 0)
                {
                    $(".buttons-excel").prop('disabled', false);
                    $(".buttons-print").prop('disabled', false);
                } else
                {
                    $(".buttons-excel").prop('disabled', true);
                    $(".buttons-print").prop('disabled', true);
                }
            });
        }

        function checkedStudent()
        {
            var arr_checked_student = [];
            $.each($('#student-detail-table').DataTable().rows('.selected').data(), function () {
                arr_checked_student.push(this["student_id"]);
            });
            return arr_checked_student;
        }
        $(document).on('change', '#class_id,#student_id,#receipt_number,#payment_status,#from_date,#to_date', function (e) {
            studentFeeReport();
        });

    });

    $(document).on('change', '#class_id', function ()
    {
        $('select[name="student_id"]').empty();
        $('select[name="student_id"]').append('<option value="">--Select Student--</option>');
        var class_id = $("#class_id").val();
        if (class_id != '')
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-fee-student')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'class_id': class_id,
                },
                beforeSend: function () {
                    $('#LoadingImage').show();
                },
                success: function (response) {
                    var resopose_data = [];
                    resopose_data = response.data;
                    if (response.status == 'success')
                    {
                        $.each(resopose_data, function (key, value) {
                            $('select[name="student_id"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                    $('#LoadingImage').hide();
                }
            });
        }
    });
</script>
</body>
</html>
@endsection



