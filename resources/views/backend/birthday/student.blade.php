@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
            </div>
            <div class="panel-body pn">

                @include('backend.partials.messages')
                <div class="section row" id="spy1">
                    <div class="col-md-6">
                        <label><span class="radioBtnpan">Select Student Type</span></label>
                        <div class="section">
                            <label class="field select">
                                {!!Form::select('birthday_student_type', $birthday_student_type,'', ['class' => 'form-control','id'=>'birthday_student_type'])!!}
                                <i class="arrow double"></i>
                            </label>
                        </div>
                    </div>
                   <div class="col-md-6">
                        <br>
                        {!! Form::button('Send SMS', ['class' => 'gui-input button btn-primary','id' => 'send_sms']) !!}
                    </div>
                </div>
                <table class="table table-bordered table-striped table-hover" id="birthday-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="checkAll">All</th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>{{trans('language.class_name')}}</th>
                            <th>{{trans('language.student_name')}}</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>{{trans('language.mother_name')}}</th>
                            <th>{{trans('language.dob')}}</th>
                            <th>{{trans('language.father_contact_number')}}</th>
                            <th>{{trans('language.caste_category')}}</th>
                            <th>{{trans('language.gender')}}</th>
                            <th>{{trans('language.address')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal hide fade" id="student-birthday-sms">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <div>TITLE</div>
    </div>
    <div class="modal-body">
        Modal Body
    </div>
    <div class="modal-footer">
        <a data-dismiss="modal" class="close">Close</a>
    </div>
</div>
<script>
    $(document).ready(function () {

        getDatatable();
        
        function getDatatable()
        {
            $('#birthday-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url('student-birthday')}}',
                    data: function (f) {
                        f.student_type = $('#birthday_student_type').val();
                    }
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
                    {data: 'enrollment_number', name: 'enrollment_number'},
                    {data: 'class_name', name: 'class_name'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'father_name', name: 'father_name'},
                    {data: 'mother_name', name: 'mother_name'},
                    {data: 'dob', name: 'dob'},
                    {data: 'father_contact_number', name: 'father_contact_number'},
                    {data: 'caste_name', name: 'caste_name'},
                    {data: 'gender_name', name: 'gender_name'},
                    {data: 'address_line1', name: 'address_line1'},
                ]
            });
        }
        
        $(document).on('change', '#birthday_student_type', function () {
            getDatatable();
        });
        $(document).on('click', '#send_sms', function () {
            var arr_student = [];
            $(".send_sms_student:checked").each(function () {
                var student_id = $(this).val();
                arr_student.push(student_id);
            });
            if(arr_student.length > 1)
            {
              $('#student-birthday-sms').modal('show');   
            }
            else
            {
                alert('Plase select student(s)');
            }
        });
        $('.checkAll').change(function () {
            var state = this.checked; //checked ? - true else false
            state ? $(':checkbox').prop('checked', true) : $(':checkbox').prop('checked', false);
            //change text
            state ? $(this).text('Uncheck All') : $(this).text('Check All');
        });
    });
</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



