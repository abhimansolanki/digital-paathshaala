@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{trans('language.view_event')}}</span>
                </div>
                <div class="topbar-right">
                    <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right custom-btn">{{trans('language.add_event')}}</a>
                </div>
            </div>
            @include('backend.partials.messages')
            <div class="panel-body pn">
                <table class="table table-striped table-hover" id="event-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.event_name') !!}</th>
                            <th>{!!trans('language.event_for') !!}</th>
{{--                            <th>{!!trans('language.event_venue') !!}</th>--}}
                            <th>{!!trans('language.event_start_date') !!}</th>
{{--                            <th>{!!trans('language.event_end_date') !!}</th>--}}
{{--                            <th>{!!trans('language.event_start_time') !!}</th>--}}
{{--                            <th>{!!trans('language.event_end_time') !!}</th>--}}
                            <!--<th>{!!trans('language.event_description') !!}</th>-->
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        var table = $('#event-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('event/data')}}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'event_name', name: 'event_name'},
                {data: 'event_for_name', name: 'event_for_name'},
                // {data: 'event_venue', name: 'event_venue'},
                {data: 'event_start_date', name: 'event_start_date'},
                // {data: 'event_end_date', name: 'event_end_date'},
                // {data: 'event_start_time', name: 'event_start_time'},
                // {data: 'event_end_time', name: 'event_end_time'},
//                {data: 'event_description', name: 'event_description'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        $.ajax(
                                {
                                    url: "{{url('event/delete/')}}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'event_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status === "success")
                                        {
                                            table.ajax.reload();
                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
    });
</script>
@endsection
@push('scripts')
@endpush

