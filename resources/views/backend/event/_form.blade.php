<!-- Field Options -->
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_event')}}</li>
                </ol>
            </div>
            <div class="topbar-right">
                <a href="{{$redirect_url}}"
                   class="button btn-primary text-right pull-right">{{trans('language.view_event')}}</a>
            </div>
        </div>
        <div class="bg-light panelBodyy">
            <div class="section-divider mv40">
                <span><i class="material-icons">event</i>Event Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.event_name')}}<span
                                    class="asterisk">*</span></span></label>
                    <label for="event_name" class="field prepend-icon">
                        {!! Form::text('event_name', old('event_name',isset($event['event_name']) ? $event['event_name'] : ''), ['class' => 'gui-input', 'id' => 'event_name']) !!}
                        <label for="event_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('event_name'))
                        <p class="help-block">{{ $errors->first('event_name') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $event['arr_session'],isset($event['session_id']) ? $event['session_id'] : '', ['class' => 'form-control','id'=>'session_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('session_id')) <p class="help-block">{{ $errors->first('session_id') }}</p> @endif
                </div>

                <div class="col-md-2">
                    <label for="event_start_date"><span class="radioBtnpan">{{trans('language.event_start_date')}}<span
                                    class="asterisk">*</span></span></label>
                    <label for="event_start_date" class="field prepend-icon">
                        {!! Form::text('event_start_date', old('event_start_date',isset($event['event_start_date']) ? $event['event_start_date'] : ''), ['class' => 'gui-input','id' => 'event_start_date']) !!}
                        <label for="" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('event_start_date')) <p
                            class="help-block">{{ $errors->first('event_start_date') }}</p> @endif
                </div>
                {{--                <div class="col-md-2">--}}
                {{--                    <label><span class="radioBtnpan">{{trans('language.event_end_date')}}<span class="asterisk">*</span></span></label>--}}
                {{--                    <label for="event_end_date" class="field prepend-icon">--}}
                {{--                        {!! Form::text('event_end_date', old('event_end_date',isset($event['event_end_date']) ? $event['event_end_date'] : ''), ['class' => 'gui-input', 'id' => 'event_end_date', 'readonly' => 'readonly']) !!}--}}
                {{--                        <label for="" class="field-icon">--}}
                {{--                            <i class="fa fa-calendar-o"></i>--}}
                {{--                        </label>--}}
                {{--                    </label>--}}
                {{--                    @if ($errors->has('event_start_date')) <p class="help-block">{{ $errors->first('event_start_date') }}</p> @endif--}}
                {{--                </div>--}}
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.event_for')}}<span
                                    class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('event_for', $event['arr_event_for'],isset($event['event_for']) ? $event['event_for'] : '', ['class' => 'form-control','id'=>'event_for'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('event_for')) <p class="help-block">{{ $errors->first('event_for') }}</p> @endif
                </div>
                {{--                <div class="col-md-2">--}}
                {{--                    <label><span class="radioBtnpan">{!! trans('language.event_venue') !!}<span--}}
                {{--                                    class="asterisk">*</span></span></label>--}}
                {{--                    <label for="event_venue" class="field prepend-icon">--}}
                {{--                        {!! Form::text('event_venue', old('event_venue',isset($event['event_venue']) ? $event['event_venue'] : ''), ['class' => 'gui-input', 'id' => 'event_venue']) !!}--}}
                {{--                        <label for="event_venue" class="field-icon">--}}
                {{--                            <i class="fa fa-eye"></i>--}}
                {{--                        </label>--}}
                {{--                    </label>--}}
                {{--                    @if ($errors->has('event_venue')) <p--}}
                {{--                            class="help-block">{{ $errors->first('event_venue') }}</p> @endif--}}
                {{--                </div>--}}
            </div>
            <div class="section row" id="spy1">
                {{--                <div class="col-md-2">--}}
                {{--                    <label for="event_start_time"><span--}}
                {{--                                class="radioBtnpan">{{trans('language.event_start_time')}}</span></label>--}}
                {{--                    <label for="event_start_time" class="field prepend-icon">--}}
                {{--                        {!! Form::time('event_start_time', old('event_start_time',isset($event['event_start_time']) ? $event['event_start_time'] : ''), ['class' => 'gui-input', 'id' => 'event_start_time']) !!}--}}
                {{--                        <label for="event_start_time" class="field-icon">--}}
                {{--                            <i class="fa fa-eye"></i>--}}
                {{--                        </label>--}}
                {{--                    </label>--}}
                {{--                    @if ($errors->has('event_start_time')) <p--}}
                {{--                            class="help-block">{{ $errors->first('event_start_time') }}</p> @endif--}}
                {{--                </div>--}}
                {{--                <div class="col-md-2">--}}
                {{--                    <label for="event_end_time"><span--}}
                {{--                                class="radioBtnpan">{{trans('language.event_end_time')}}</span></label>--}}
                {{--                    <label for="event_end_time" class="field prepend-icon">--}}
                {{--                        {!! Form::time('event_end_time', old('event_end_time',isset($event['event_end_time']) ? $event['event_end_time'] : ''), ['class' => 'gui-input', 'id' => 'event_end_time']) !!}--}}
                {{--                        <label for="event_end_time" class="field-icon">--}}
                {{--                            <i class="fa fa-eye"></i>--}}
                {{--                        </label>--}}
                {{--                    </label>--}}
                {{--                    @if ($errors->has('event_end_time')) <p--}}
                {{--                            class="help-block">{{ $errors->first('event_end_time') }}</p> @endif--}}
                {{--                </div>--}}
                <div class="col-md-4">
                    <span class="radioBtnpan">{{trans('language.image_name')}}
                        @if(isset($event['event_images']))
                            &nbsp;<a href="" data-toggle="modal" data-target="#img1" data-backdrop="static"
                                     data-keyboard="false">Click to view </a></span>
                    @endif
                    {!!Form::file('image_name[]',['multiple','accept'=>'image/*'])!!}


                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-10">
                    <label><span class="radioBtnpan">{!! trans('language.event_description') !!}</span></label>
                    <label for="class" class="field prepend-icon">
                        {!! Form::textarea('event_description', old('event_description',isset($event['event_description']) ? $event['event_description'] : ''), ['class' => 'gui-input ckeditor', 'id' => 'class','rows'=>3]) !!}
                        <label for="class" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('event_description')) <p
                            class="help-block">{{ $errors->first('event_description') }}</p> @endif
                </div>


            </div>
            <div id="img1" class="modal fade-scale" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            @if(isset($event['event_images']))
                                @foreach($event['event_images'] as $event_image)
                                    <img src="{{url($event_image['name'])}}" style="width: 100%">
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

        var session_id = '<?php
            if (!empty($event['session_id'])) {
                echo $event['session_id'];
            }
            ?>';
        if (session_id !== '') {
            getDateData(session_id);
        }

        $("#event-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                event_name: {
                    required: true,
                    lettersonly: true
                },
                event_venue: {
                    required: true
                },
                session_id: {
                    required: true
                },
                event_for: {
                    required: true
                },
                event_start_date: {
                    required: true
                },
                event_end_date: {
                    required: true
                },
                'image_name[]': {
                    extension: true
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        $('#event_start_date').datepicker().on('change', function (ev) {
            var max_date = $(this).datepicker("option", "maxDate");
            $("#event_end_date").datepicker('destroy');
            $("#event_end_date").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                minDate: $(this).val(),
                maxDate: max_date
            });
            $(this).valid();
        });
        $('#event_start_date').datepicker().on('change', function (ev) {
            $(this).valid();
        });
        $('#event_end_date').datepicker().on('change', function (ev) {
            $(this).valid();
        });
        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || !/[~`!@#$%\^&*()+=\-\_\.\[\]\\';,/{}|\\":<>\?]/.test(value) || /^[a-zA-Z][a-zA-Z0-9\s\.]+$/i.test(value);
        }, "Please enter only alphabets(more than 1)");


        jQuery.validator.addMethod("extension", function (a, b, c) {
            return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
        }, jQuery.validator.format("Only valid image formats are allowed"));


        $(document).on('click', '#event_start_date,#event_end_date', function () {
            var session = $("#session_id").val();
            if (session === '') {
                $('#server-response-message').text('First select academic year');
                $('#alertmsg-student').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
        });

        $(document).on('change', '#session_id', function () {
            var session_id = $("#session_id").val();
            $("#event_sart_date").val('');
            $("#event_end_date").val('');
            getDateData(session_id);

        });

        function getDateData(session_id) {
            if (session_id !== '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-session')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'session_id': session_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        if (response.status === 'success') {
                            var minDate = response.start_date;
                            var maxDate = response.end_date;
                            setDate(minDate, maxDate);
                            $("#LoadingImage").hide();
                        }
                    }
                });
            }
        }

        function setDate(minDate, maxDate) {
            $("#event_end_date").datepicker('destroy');
            $("#event_start_date").datepicker('destroy');
            $("#event_start_date").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                minDate: minDate,
                maxDate: maxDate
            });
            if ($("#event_start_date").val() !== '') {
                $("#event_end_date").datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>',
                    nextText: '<i class="fa fa-chevron-right"></i>',
                    showButtonPanel: false,
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd/mm/yy",
                    minDate: $("#event_start_date").val(),
                    maxDate: maxDate
                });
            }

        }
    });
</script>