@extends('admin_panel/layout')
<style>
    .bootstrap-tagsinput {
        width: 100%;
    }
</style>
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                </div>
                @include('backend.permission._form')
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush
