<!-- Field Options -->
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.permission')}}</li>
                </ol>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages')
        @endif
        <div class="alert alert-success alert-dismissable" style="display:none;" id="successMessage">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check pr10"></i>
            <span id="successMessageText"></span>
        </div>
        <div class="alert alert-danger alert-dismissable"  style="display:none;" id="errorMessage">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-remove pr10"></i>
            <span id="errorMessageText"></span>
        </div>
        <div class="bg-light panelBodyy">
            <div class="section row">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.user_role_type')}}<span class="asterisk">*</span></span></label>
                    <label class="field prepend-icon">
                        {!!Form::select('user_role', $user_roles, '', ['class' => 'form-control','id'=>'user_role'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if($errors->has('user_role')) <p class="help-block">{{ $errors->first('user_role') }}</p> @endif 
                </div>
            </div>
        </div>
        <!-- end .form-body section -->
    </div>
    <!-- end .form-footer section -->
</div>
<div id="permission-table"></div>
<div class="clearfix"></div>
<script>
    $(document).on('click','.markGroup', function(){
        var key = $(this).attr('rel');
        if ($(this).prop("checked"))
        {
            $(".module-" + key).prop("checked", true);
        } else {
            $(".module-" + key).prop("checked", false);
        }
    });
    $(document).on('click', '.groupElement', function(){
        var master_key = $(this).attr('master-key');
        if($('.module-' + master_key + ':checked').length > 0){
            $("#master-" + master_key).prop("checked", true);
        } else {
            $("#master-" + master_key).prop("checked", false);
        }
    });
    $(document).on('click', '.checkAll', function(){
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
    $(document).on('change', '#user_role', function(){
        $('#successMessageText').html(null);
        $('#successMessage').hide();
        $('#errorMessageText').html(null);
        $('#errorMessage').hide();
        $.ajax({
                url: "{!! url('admin/permission/view-table') !!}",
                data: {
                    'user_role_id': $(this).val()
                },
                type: 'get',
            success: function (response) {
                if($.isEmptyObject(response.error)){
                    $('#permission-table').html(response);
                } else {
                    console.log(response.error);
                }
            }
        });
    });
    $(document).on('submit', '#permission-form', function(e){
        var form = $(this);
        $.ajax({
            url: "{!! url('admin/permission/save') !!}",
            data: form.serialize(),
            type: 'get',
            success: function (response) {
                if(response.status == 'success') {
                    $('#successMessageText').html(response.message);
                    $('#successMessage').show();
                } else{
                    $('#errorMessageText').html(response.message);
                    $('#errorMessage').show();
                }
            }
        });
        e.preventDefault(); 
    });
</script>