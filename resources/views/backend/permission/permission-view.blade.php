<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible">
            {{ Form::open(array('url' => '', 'id' => 'permission-form')) }}
                {!! Form::hidden('user_role_id', $user_role_id) !!}                      
                <div class="panel-body pn">
                    <table class="table table-bordered table-striped table-hover" id="user-role-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="checkAll"> &nbsp; &nbsp; All
                                </th>
                                <th>Module Name</th>
                                @foreach ($modules_action as $key => $value)
                                    <th>{{ $value }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($modules as $module)
                                <tr>
                                    @php 
                                        $module_name = $module['module_name'];
                                        $module_key = $module['key'];
                                        $details = $module['details'];
                                    @endphp
                                    <td>
                                        <input 
                                            type="checkbox" 
                                            class="markGroup" 
                                            id="master-{{$module_key}}" 
                                            rel={{$module_key}} 
                                            id="map[{{ $module_key }}]"
                                            @isset($permissions)
                                                @if(!empty(array_intersect($module['existModuleIds'], $permissions['user_permission_modules'])))
                                                    checked
                                                @endif
                                            @endisset
                                        />
                                    </td>
                                    <td>{{ $module_name }}</td>
                                    @foreach ($modules_action as $key => $value)
                                        <td>
                                            @if(isset($details[$key]))
                                                <input 
                                                    type="checkbox" 
                                                    class="module-{{$module_key}} groupElement" 
                                                    master-key="{{ $module_key }}" 
                                                    id="map[{{ $module_key }}][{{ $details[$key]['module_id']}}]" 
                                                    name="map[{{ $module_key }}][{{ $details[$key]['module_id']}}]"
                                                    @isset($permissions)
                                                        @if(in_array($details[$key]['module_id'], $permissions['user_permission_modules']))
                                                            checked
                                                        @endif
                                                    @endisset
                                                    />
                                            @else
                                                --
                                            @endif
                                        </td>
                                    @endforeach
                                </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="panel-footer clearfix">
                        {!! Form::submit('Submit Form', ['class' => 'form-control button btn-primary', 'id' => 'savePermission']) !!}
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>