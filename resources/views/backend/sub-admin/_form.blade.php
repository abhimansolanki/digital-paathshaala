<!-- Field Options -->
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.user_role')}}</li>
                </ol>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages')
        @endif
        <div class="bg-light panelBodyy">

            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.user_role_name') !!}<span class="asterisk">*</span></span></label>
                    <label for="user_role_name" class="field prepend-icon">
                        {!! Form::text('user_role_name', old('user_role_name',isset($user_role['user_role_name']) ? $user_role['user_role_name'] : ''), ['class' => 'gui-input', 'id' => 'user_role_name','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="user_role_name" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('user_role_name')) <p class="help-block">{{ $errors->first('user_role_name') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.user_role_type')}}<span class="asterisk">*</span></span></label>
                    <label class="field prepend-icon">
                        {!!Form::select('user_role_type', $user_type, isset($user_role['user_type_id']) ? $user_role['user_type_id'] : '', ['class' => 'form-control','id'=>'user_role_type'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if($errors->has('user_role_type')) <p class="help-block">{{ $errors->first('user_role_type') }}</p> @endif 
                </div>
                <div class="col-md-3">
                    <label for="submit_user_role" class="field prepend-icon">
                        {!! Form::submit($submit_button, ['class' => 'button btn-primary','style'=>'width:100px; margin-top:25px;']) !!}   
                    </label>

                </div>
            </div>
        </div>
        <!-- end .form-body section -->
    </div>
    <!-- end .form-footer section -->
</div>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.user_role_list')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="user-role-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!! trans('language.user_role_name') !!}</th>
                            <th>{!! trans('language.user_role_type') !!}</th>
                            <th>{!! trans('language.user_role_status') !!}</th>
                            <th>{!! trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#user-role-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'get',
                url: '{{url('user-role/data')}}',
                data: function (d) { 

                }
            },
            //"aaSorting": [[0, "ASC"]],
            columns: [
                //{data: 'DT_Index', name: 'DT_Index'},
                {data: 'user_role_name', name: 'user_role_name'},
                {data: 'user_role_type', name: 'user_role_type'},
                {data: 'user_role_status', name: 'user_role_status'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('admin/user-role/delete') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'user_role_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();

                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function () {

        $("#user-role-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                user_role_name: {
                    required: true
                },
                user_role_type: {
                    required: true,
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });
</script>