<!-- Field Options -->
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_holiday')}}</li>
                </ol>
            </div>
        </div>
        <div class="bg-light panelBodyy">
            <div class="section-divider mv40">
                <span><i class="fa fa-calendar-o"></i>&nbsp;Holiday Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.holiday_name')}}<span class="asterisk">*</span></span></label>
                    <label for="holiday_name" class="field prepend-icon">
                        {!! Form::text('holiday_name', old('holiday_name',isset($holiday['holiday_name']) ? $holiday['holiday_name'] : ''), ['class' => 'gui-input', 'id' => 'holiday_name']) !!}
                        <label for="holiday_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('holiday_name')) 
                    <p class="help-block">{{ $errors->first('holiday_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label for="holiday_start_date"><span class="radioBtnpan">{{trans('language.holiday_start_date')}}<span class="asterisk">*</span></span></label>
                    <label for="holiday_start_date" class="field prepend-icon">
                        {!! Form::text('holiday_start_date', old('holiday_start_date',isset($holiday['holiday_start_date']) ? $holiday['holiday_start_date'] : ''), ['class' => 'gui-input date_picker','id' => 'holiday_start_date']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('holiday_start_date')) <p class="help-block">{{ $errors->first('holiday_start_date') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.holiday_end_date')}}<span class="asterisk">*</span></span></label>
                    <label for="holiday_end_date" class="field prepend-icon">
                        {!! Form::text('holiday_end_date', old('holiday_end_date',isset($holiday['holiday_end_date']) ? $holiday['holiday_end_date'] : ''), ['class' => 'gui-input date_picker', 'id' => 'holiday_end_date', 'readonly' => 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('holiday_end_date')) <p class="help-block">{{ $errors->first('holiday_end_date') }}</p> @endif
                </div>
            </div>
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span>{{ trans('language.list_holiday')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="holiday-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.holiday_name') !!}</th>
                            <th>{!!trans('language.holiday_start_date') !!}</th>
                            <th>{!!trans('language.holiday_end_date') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

        $("#holiday_end_date,#holiday_start_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: 0,
            maxDate: "{!! $holiday['session_end_date'] !!}"
        });
        $("#holiday-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                holiday_name: {
                    required: true,
                    lettersonly: true
                },
                holiday_start_date: {
                    required: true
                },
                holiday_end_date: {
                    required: true
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9\s\.]+$/i.test(value);
        }, "Please enter only alphabets(more than 1)");

        $('#holiday_start_date').datepicker().on('change', function (ev) {
            var max_date = $(this).datepicker("option", "maxDate");
            $("#holiday_end_date").datepicker('destroy');
            $("#holiday_end_date").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                minDate: $(this).val(),
                maxDate: max_date
            });
            $(this).valid();
        });
        $('#holiday_end_date').datepicker().on('change', function (ev) {
            $(this).valid();
        });

        var table = $('#holiday-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('holiday/data')}}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'holiday_name', name: 'holiday_name'},
                {data: 'holiday_start_date', name: 'holiday_start_date'},
                {data: 'holiday_end_date', name: 'holiday_end_date'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");
            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        $.ajax(
                                {
                                    url: "{{url('holiday/delete/')}}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'holiday_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status === "success")
                                        {
                                            table.ajax.reload();

                                        }
                                    }
                                });
                    }
                }
            });

        });
    });
</script>