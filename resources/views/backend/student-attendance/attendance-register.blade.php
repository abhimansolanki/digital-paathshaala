@extends('admin_panel/layout')
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
    .display-icon{
        width: 15px; 
        height: 15px;
        border-radius: 100%;
        margin: 10px;
        float:left;
    }
    .display-text{
        margin-top: 7px;
        float:left;
    }
    .custom-table {
        font-size: 11px !important;   
    }
    .table > thead > tr > th{
        padding:4px !important;
        text-align: center !important;
    }
    .overlay {
        background-color:#EFEFEF;
        /*position: fixed;*/
        width: 100%;
        height: 100%;
        z-index: 1000;
        top: 0px;
        left: 0px;
        opacity: .5; /* in FireFox */ 
        filter: alpha(opacity=50); /* in IE */
    }
    .button{
        background: #2e76d6 !important;
    border-radius: 0px !important;
    border: 0px;
    outline: none;
    box-shadow: 0px;
    color: #fff;
    padding: 7px 10px;
    font-weight: bold;
    font-size: 11px;
    margin-top: 1%;
    z-index: 9999999;
    font-weight: normal;
    }
</style>
@php $current_segment2 = Request::segment(3); @endphp
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel" id="">
                <!--<span style="font-size:18px;">Student Attendance Register</span>-->
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                {!! Form::open(['id' => 'attendance-register-form' , 'class'=>'form-horizontal','url' => '','autocomplete'=>'off']) !!}
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('class_id', $arr_class,$class_id, ['class' => 'form-control','id'=>'class_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('section_id', $arr_section,$section_id, ['class' => 'form-control','id'=>'section_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="field select" style="width: 100%;">
                                        {!! Form::select('table_view',$arr_table_view,$current_segment2, ['class' =>'form-control','id'=>'table-view', 'readonly' =>true]) !!}
                                    </label>
                                </div>
                                {!! Form::hidden('file_title',date('F, Y'), ['class' =>'','id'=>'file-title', 'readonly' =>true]) !!}
                                @if($current_segment2 !='student-wise')
                                <div class="col-md-2">
                                    <label class="field select" style="width: 100%;">
                                        {!! Form::text('attendance_calendar',date('D F d, Y'), ['class' => 'form-control date_piker','id' => 'attendance_calendar', 'readonly' => 'readonly','placeholder'=>'Select Calendar']) !!}
                                    </label>
                                </div>
                                @endif
		        <div class="col-md-2">
			<label class="field select" style="width: 100%;">
			    {!! Form::button('Download Excel', ['class' => 'button btn-primary','style'=>'width:170px;','id'=>'excel-data']) !!}   
			</label>
		        </div>
		        <div class="col-md-2">
			<label class="field select" style="width: 100%;">
			    {!! Form::button('Print', ['class' => 'button btn-primary','style'=>'width:120px;','id'=>'print-data']) !!}   
			</label>
		        </div>
                                <div class="clearfix"></div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2" style="width: 8.666667% !important">
                    <span class="display-icon" style=" background-color:greenyellow; "></span>
                    <span class="display-text">Holiday</span>
                </div>
                <div class="col-md-2" style="width: 8.666667% !important">
                    <span class="display-icon" style=" background-color:blue; "></span>
                    <span class="display-text">Present</span>
                </div>
                <div class="col-md-2" style="width: 8.666667% !important">
                    <span class="display-icon" style=" background-color:red; "></span>
                    <span class="display-text">Absent</span>
                </div>
                <div class="col-md-2" style="width: 8.666667% !important">
                    <span class="display-icon" style=" background-color:yellow; "></span>
                    <span class="display-text">Leave</span>
                </div>
            </div>
	<div id="full-attendance-register">
            <div class="panel-heading" style="font-size: 14px; font-weight: bold">
                <div class="col-md-4">
                    <span style="" id="class-name"></span>
                    <span style="" id="section-name"></span>&nbsp;
                </div>
                <div class="col-md-4">
                    <span style="text-align:center">Student Attendance Register</span>
                </div>
                <div class="col-md-4" style="text-align:right" id="year-name">
                    <span style="" id="month-year">{!! date('F') !!}</span> &nbsp;<span>{!! $session['session_year'] !!}</span>
                </div>
            </div>
            <div class="panel-body pn" style="border-top:1px solid #e5e5e5 !important;">
                <div style="overflow:auto;margin-bottom: 50px;">
	        <div id="student-attendance-div">
		
	        </div>    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        studentAttendance();
        function studentAttendance(c_date = null)
        {
            $("#student-attendance-div").html('');
            var class_id = $('#class_id').val();
            var section_id = $('#section_id').val();
            var view_type = $("#table-view").val();
            if (class_id !== '' && section_id !== '' && view_type !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('attendance_register')}}",
                    datatType: 'json',
                    type: 'GET',
                    data: {
                        'c_date': c_date,
                        'class_id': class_id,
                        'section_id': section_id,
                        'view_type': view_type,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
//		console.log(response.data);
                        $("#LoadingImage").hide();
                        $("#student-attendance-div").html(response.data);
                    }
                });
        }
        }

        /**/
        function getClassSection()
        {
            $('select[name="section_id"]').empty();
            $('select[name="section_id"]').append('<option value="">--Select section--</option>');
            var class_id = $("#class_id").val();
            if (class_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $.each(resopose_data, function (key, value) {
                                $('select[name="section_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                    }
                });
            }
        }
        $("#attendance_calendar").datepicker('destroy');
        $(function () {
            var i = $("#attendance_calendar").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                dateFormat: "D MM dd, yy",
                minDate: '{!! $session["start_date"] !!}',
                maxDate: '{!! $session["end_date"] !!}',
                onChangeMonthYear: function (year, month, inst) {
                    var n = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    var month_num = parseInt(month) - 1;
                    var file_name = n[month_num] + ', ' + year;
                    $("#file-title").val(file_name);
                    $("#month-year").text(n[month_num]);
                    var view = $("#table-view").val();
                    if (view === 'month')
                    {
                        var c_date = year + '-' + month + '-' + '01';
                        var a = new Date(c_date);
                        var weekday = new Array(7);
                        weekday[0] = "Sun";
                        weekday[1] = "Mon";
                        weekday[2] = "Tue";
                        weekday[3] = "Wed";
                        weekday[4] = "Thu";
                        weekday[5] = "Fri";
                        weekday[6] = "Sat";
                        studentAttendance(c_date);
                        var day = weekday[a.getDay()];
                        $("#attendance_calendar").val(day + ' ' + n[month_num] + ' ' + '01, ' + year);
                    }
                },
                onSelect: function (c_date, inst) {
                    var view = $("#table-view").val();
                    if (view === 'week')
                    {
                        studentAttendance(c_date);
                    }
                }
            });
        });

        $(document).on('change', '#class_id,#section_id', function (e) {
            $("#section-name").text('');
            if ($(this).attr("id") === "class_id")
            {
                getClassSection();
                $("#class-name").text('');
                if ($(this).val() !== "")
                {
                    $("#class-name").text('Class - ' + $("#class_id option:selected").text());
                }
            } else
            {
                if ($(this).val() !== "")
                {
                    $("#section-name").text(' / Section - ' + $("#section_id option:selected").text());
                }
            }
            var c_date = $("#attendance_calendar").val();
            studentAttendance(c_date);
        });
        $("#class-name").text('Class - ' + $("#class_id option:selected").text());
        $("#section-name").text(' / Section - ' + $("#section_id option:selected").text());
        $(document).on('change', '#table-view', function (e) {
            var view_type = $(this).val();
            var url = '{!! url("admin/attendance-register/' + view_type + '") !!}';
            window.location.replace(url);
        });

    });
    // print class -subject mapping data
    function printData()
    {
        var divToPrint = document.getElementById("attendance-register-data");
        newWin = window.open("");
        newWin.document.write('<html><head><title></title>');
        newWin.document.write('<style> table, th, td {border: 1px solid black;border-collapse: collapse;}</style>');
        newWin.document.title = $("#class-name").text() +''+ $("#section-name").text() +' '+ $("#year-name").text();
        newWin.document.write('</head><body>');
        newWin.document.write(divToPrint.outerHTML);
        newWin.document.write('</body></html>');
        newWin.print();
        newWin.close();
    }

    $(document).on('click', '#print-data', function () {
        printData();
    });

// download excel of data class-subject mapping
    $(document).on('click', '#excel-data', function () {
        var htmls_data = "";
        var uri = 'data:application/vnd.ms-excel;base64,';
        var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
        var base64 = function (s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        };
        var format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            });
        };
        htmls_data = $("#class-name").text() +''+ $("#section-name").text() +' '+ $("#year-name").text();
        htmls_data += $('#attendance-register-data').html();
        var ctx = {
            worksheet: 'Attendance Register',
            table: htmls_data
        }
        var link = document.createElement("a");
        link.download = "attendance-register.xls";
        link.href = uri + base64(format(template, ctx));
        link.click();
    });
</script>
</body>
</html>
@endsection




