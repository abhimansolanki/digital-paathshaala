@extends('admin_panel/layout')
<style>
    .student_attendance{
        width: 10px;
        height: 10px;
        text-align:center !important;
        border-radius:100% !important;
    }
    .attendance_holiday{
        text-align:center !important;
        color:black;
        background: greenyellow;
    }
    .fc-button-group fc-icon {
        line-height: .1em !important;
    }
    .btn-icon{
        height: 2.1em;padding: 0 .6em;
        background-color: #f0f0f0;
    }
    #student-name{
        padding-right:5px;
        font-size: 16px;
        text-transform: capitalize !important;
    }
</style>
@section('content')

<div class="container">

    <div class="row">

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <p>
                    <a href="javascript:history.go(-1)" title="Return to the previous page">&laquo; Go back</a>
                </p>
                <div class="panel-heading">
                    <div class="col-md-6"><span style="font-size:25px;padding-left: 6px;">Attendance</span></div>
                    <div class="col-md-6">
                        <div class="fc-button-group" style="float:right">
                            <span id="student-name"></span>
                            <button  id="left" style="margin-right:-5px" type="button" class="btn-icon fc-prev-button fc-button fc-state-default fc-corner-left move-student" move-type="left">
                                <span class="fc-icon fc-icon-left-single-arrow"></span>
                            </button>
                            <button  id="right" style="" type="button" class=" btn-icon fc-next-button fc-button fc-state-default fc-corner-right move-student" move-type="right">
                                <span class="fc-icon fc-icon-right-single-arrow"></span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div id='calendar' class="admin-theme"></div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        var student_id = '{!! Request::segment(3) !!}';
        var arr_student = '{!! json_encode($arr_student) !!}';
        arr_student = $.parseJSON(arr_student);
        console.log(arr_student);
        var current_index = 0;
        jQuery.each(arr_student, function (key, val) {
            if (val['id'] == student_id)
            {
                current_index = key;
                $("#student-name").text(val['name']);
            }
        });

        $(document).on('click', '.move-student', function (e) {
            if ($(this).attr('move-type') == 'right') {
                var next_index = parseInt(current_index) + 1;
                var data = arr_student[next_index];
                if (typeof data !== 'undefined') {
                    $("#left").attr('disabled', false);
                    current_index = next_index;
                    $("#student-name").text(data['name']);
                    var c_student_id = data['id'];
                    var url = '{!! url("admin/view-student-attendance/' + c_student_id + '") !!}';
                    window.location.replace(url);

                    // does not exist
                } else {
                    $("#right").attr('disabled', true);
                    // does exist
                }

            } else
            {
                $("#right").attr('disabled', false);
                var pre_index = parseInt(current_index) - 1;
                var data = arr_student[pre_index];
                if (typeof data !== 'undefined') {
                    current_index = pre_index;
                    $("#student-name").text(data['name']);
                    var c_student_id = data['id'];
                    var url = '{!! url("admin/view-student-attendance/' + c_student_id + '") !!}';
                    window.location.replace(url);
                    // does not exist
                } else {
                    $("#left").attr('disabled', true);
                    // does exist
                }
            }
        });
        getStudentCalendar();
        function getStudentCalendar()
        {
            var myCalendar = $('#calendar').fullCalendar({
                events: function (start, end, timezone, callback) {
                    start = getDateFormat(start);
                    end = getDateFormat(end);

                    $.ajax({
                        contentType: "application/json",
                        url: "{!! url('admin/student-attendance-calendar/" + student_id + "/" + start + "/" + end + "') !!}",
                        datatType: 'json',
                        type: 'GET',
                        success: function (calendar_data) {
                            var my_events = [];
                            if (calendar_data.status === 'success') {
                                $.each(calendar_data.data, function (index, val) {
                                    my_events.push({
                                        title: val.title,
                                        start: val.start_date,
                                        end: val.end_date,
                                        className: val.className,
                                        color: val.color,
                                        textColor: val.textColor,
                                    });
                                });
//                                console.log(my_events);
                                callback(my_events);
                            }
                        }
                    });
                }

            });
        }
        function getDateFormat(date)
        {
            var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        }
    });
</script>


