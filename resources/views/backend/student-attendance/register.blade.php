<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    .table-style {
        margin-left:11px;
    }
    .row-heading{
        text-align:center;
    }
</style>
<div class="row">
    @php  $rowspan = 0;  $colspan = 0; $tot_col = count($header);@endphp
    <table id="attendance-register-data" class="table-style" cellpading="10">
        <thead>
	<tr>
	    @foreach($header as $key =>$row1)
	    @if($row1['key'] != 'student_id')
	    @php   $rowspan = ''; $colspan = ''; @endphp
	    @if($row1['key'] == 'serial_no' || $row1['key'] == 'enrollment_number' || $row1['key'] == 'student_name')
	    @php   $rowspan = 'rowspan=2'; @endphp
	    @endif
	    @if($row1['key'] == 'attendance' || $row1['key'] == 'absence' || $row1['key'] == 'leave')
	    @php   $colspan = 'colspan=3'; 
	    @endphp
	    @endif
	    <th {{$rowspan}} {{$colspan}} class="row-heading">{!! $row1['val'] !!}</th>
	    @endif
	    @endforeach
	</tr>
	<tr>
	    @foreach($header as $key =>$row2)
	    @if($row2['key'] != 'student_id' && $row2['key'] != 'serial_no' && $row2['key'] != 'enrollment_number' && $row2['key'] != 'student_name')
	    @if($row2['key'] == 'attendance' || $row2['key'] == 'absence' || $row2['key'] == 'leave')
	    @foreach($row2['child'] as $key =>$row2)
	    <th>{!! $row2['val'] !!}</th>
	    @endforeach
	    @else
	    <th>{!! $row2['key'] !!}</th>
	    @endif	  
	    @endif	  
	    @endforeach
	</tr>
        </thead>
        <tboady>
	@php @endphp
	@if(!empty($body))
	@foreach($body['arr_attendance'] as $row3)
	<tr>
	    @foreach($row3 as $key =>$value)
	    @if($key != 'student_id' && $key !='admission_date')
	    @php  $color = '';$color = ''; $color = ''; $background_color = ''; @endphp
	    @if($value == 'A' || $value == 'L')
	    @php  $color = 'red'; @endphp
	    @elseif($value == 'W')
	    @php  $background_color = 'yellow'; @endphp
	    @elseif($value == 'P')
	    @php  $color = 'blue'; @endphp
	    @elseif($value == 'H')
	    @php  $background_color = 'greenyellow'; @endphp
	    @endif
	    <td style="color:{{$color}};background-color: {{$background_color}}">{!! $value !!}</th>
	        @endif
	        @endforeach
	    </td>
	    @endforeach
	    @endif	  

        </tboady>
        <tfoot>
	<tr>
	    <th colspan="3">Total Attendance</th>
	    @foreach($body['att_summary']['total_present'] as $key =>$row2)
	    <th>{!! $row2 !!}</th>
	    @endforeach
	    <th colspan="9">Sig. of Classteacher </th>
	</tr>
	<tr>
	    <th colspan="3">Total Absence </th>
	    @foreach($body['att_summary']['total_absent'] as $key =>$row2)
	    <th>{!! $row2 !!}</th>
	    @endforeach
	    <th colspan="9">Checked by </th>
	</tr>
	<tr>
	    <th colspan="3">Total Leave </th>
	    @foreach($body['att_summary']['on_leave'] as $key =>$row2)
	    <th>{!! $row2 !!}</th>
	    @endforeach
	    <th colspan="9" rowspan="2">Head of the institution </th>
	</tr>
	<tr>
	    <th colspan="3">Total No. of Scholar's</th>
	    @foreach($body['att_summary']['total_student'] as $key =>$row2)
	    <th>{!! $row2 !!}</th>
	    @endforeach
	</tr>
	@if($view_type  == 'month')
	@php $extra_col = $tot_col+ 5 - 33; @endphp
	<tr>
	    <th colspan="3" class="row-heading">Total No. of Scholar's</th>
	    <th colspan="3" class="row-heading">S.C.</th>
	    <th colspan="3" class="row-heading">S.T.</th>
	    <th colspan="3" class="row-heading">O.B.C.</th>
	    <th colspan="3" class="row-heading">Minority</th>
	    <th colspan="3" class="row-heading">General</th>
	    <th colspan="3" class="row-heading">No. of Teaching Days</th>
	    <th colspan="3" class="row-heading">Average Attendance </th>
	    <th colspan="3" class="row-heading">Promotion </th>
	    <th colspan="3" class="row-heading">New Admission </th>
	    <th colspan="3" class="row-heading">Drop-out</th>
	    <th colspan="{{$extra_col}}"></th>

	</tr>
	<tr>
	    <th>Boys</th>
	    <th>Girls</th>
	    <th>Total</th>

	    <th>Boys</th>
	    <th>Girls</th>
	    <th>Total</th>

	    <th>Boys</th>
	    <th>Girls</th>
	    <th>Total</th>
	    <th>Boys</th>
	    <th>Girls</th>
	    <th>Total</th>

	    <th>Boys</th>
	    <th>Girls</th>
	    <th>Total</th>

	    <th>Boys</th>
	    <th>Girls</th>
	    <th>Total</th>

	    <th>In this Month </th>
	    <th>Upto Last Month </th>
	    <th>Total</th>

	    <th>In this Month </th>
	    <th>Upto Last Month </th>
	    <th>Total</th>

	    <th>Boys</th>
	    <th>Girls</th>
	    <th>Total</th>

	    <th>Boys</th>
	    <th>Girls</th>
	    <th>Total</th>

	    <th>Boys</th>
	    <th>Girls</th>
	    <th>Total</th>
	    <th colspan="{{$extra_col}}"></th>
	</tr>
	<tr>
	    <th>{!! $summary['total_boys'] !!}</th>
	    <th>{!! $summary['total_girls'] !!}</th>
	    <th>{!! $summary['total_students'] !!}</th>

	    <th>{!! $summary['sc_boys'] !!}</th>
	    <th>{!! $summary['sc_girls'] !!}</th>
	    <th>{!! $summary['sc_total'] !!}</th>

	    <th>{!! $summary['st_boys'] !!}</th>
	    <th>{!! $summary['st_girls'] !!}</th>
	    <th>{!! $summary['st_total'] !!}</th>

	    <th>{!! $summary['obc_boys'] !!}</th>
	    <th>{!! $summary['obc_girls'] !!}</th>
	    <th>{!! $summary['obc_total'] !!}</th>

	    <th>{!! $summary['minority_boys'] !!}</th>
	    <th>{!! $summary['minority_girls'] !!}</th>
	    <th>{!! $summary['minority_total'] !!}</th>

	    <th>{!! $summary['gen_boys'] !!}</th>
	    <th>{!! $summary['gen_girls'] !!}</th>
	    <th>{!! $summary['gen_total'] !!}</th>

	    <th>{!! $summary['teaching_this_month'] !!}</th>
	    <th>{!! $summary['teaching_upto_this_month'] !!}</th>
	    <th>{!! $summary['teaching_total'] !!}</th>

	    <th>{!! $summary['average_attendance'] !!}</th>
	    <th>{!! $summary['average_attendance'] !!}</th>
	    <th>{!! $summary['average_attendance'] !!}</th>

	    <th>{!! $summary['promotion_boys'] !!}</th>
	    <th>{!! $summary['promotion_girls'] !!}</th>
	    <th>{!! $summary['promotion_total'] !!}</th>

	    <th>{!! $summary['new_admission_boys'] !!}</th>
	    <th>{!! $summary['new_admission_girls'] !!}</th>
	    <th>{!! $summary['new_admission_total'] !!}</th>

	    <th>{!! $summary['drop_boys'] !!}</th>
	    <th>{!! $summary['drop_girls'] !!}</th>
	    <th>{!! $summary['drop_total'] !!}</th>
	    <th colspan="{{$extra_col}}"></th>
	</tr>
	@endif
        </tfoot>
    </table>
</div>