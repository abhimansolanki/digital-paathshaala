<!--@php  $arr_th = array(
                'total_boys'               => 'Total Boys',
                'total_girls'              => 'Total Girls',
                'total_students'           => 'Total Scholars',
                'obc_boys'                 => 'O.B.C. Boys',
                'obc_girls'                => 'O.B.C. Girls',
                'obc_total'                => 'O.B.C. Total',
                'gen_boys'                 => 'General Boys',
                'gen_girls'                => 'General Girls',
                'gen_total'                 => 'General Total',
                'sc_boys'                  => 'S.C. Boys',
                'sc_girls'                 => 'S.C. Girls',
                'sc_total'                  => 'S.C. Total',
                'st_boys'                  => 'S.T. Boys',
                'st_girls'                 => 'S.T. Girls',
                'st_total'                 => 'S.T. Total',
                'minority_boys'            => 'Minority Boys',
                'minority_girls'           => 'Minority Girls',
                'minority_total'           => 'Minority Total',
                'promotion_boys'           => 'Promotion Boys',
                'promotion_girls'          => 'Promotion Girls',
                'promotion_total'          => 'Promotion Total',
                'new_admission_boys'       => 'New Admission Boys',
                'new_admission_girls'      => 'New Admission Girls',
                'new_admission_total'      => 'New Admission Total',
                'drop_boys'                => 'Drop-out Boys',
                'drop_girls'               => 'Drop-out Girls',
                'drop_total'               => 'Drop-out Total',
                'teaching_this_month'      => 'Teaching day this month',
                'teaching_upto_this_month' => 'Teaching day up to this month',
                'teaching_total'           => 'Teaching day total',
                'average_attendance'       => 'Average Attendance',
            );
            @endphp-->
<table class="table table-bordered table-striped table-hover" id="register-summary" cellspacing="0" width="100%">
    <thead>
        <tr>
<!--            @foreach($arr_th as $th)
            <th>{{$th}}</th>
            @endforeach-->
        </tr>
    </thead>
</table>
<script>
    $(document).ready(function () {
        var arr_col = '{!! json_encode($arr_th) !!}';
        var  arr_col = $.parseJSON(arr_col);
         var summary_dynamic_col = [];
        summaryTableHeader(arr_col);
        function summaryTableHeader(arr_col, c_date = null)
        {
            var table_th = '';
             summary_dynamic_col = [];
            jQuery.each(arr_col, function (key,val) {
                summary_dynamic_col.push({data: '' + key + '', name: '' + key + '', orderable: false, searchable: false});
                table_th = table_th + '<th>' + val + '</th>';
                
            });
            $("#register-summary thead tr").html(table_th);
            RegisterSummary(summary_dynamic_col, c_date);
        }
//RegisterSummary();
        function RegisterSummary(summary_dynamic_col, c_date)
        {
            var table = $('#register-summary').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                bFilter:false,
                bPaginate:false,
                lengthChange: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": $("#class_id option:selected").text() + ',' + $("#file-title").val(),
                        "filename": 'student-attendance-register',
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": $("#class_id option:selected").text() + ',' + $("#file-title").val(),
                        "filename": 'student-attendance-register',
                    }
                ],

                ajax: {
                    url: '{{ url('attendance_register_summary')}}',
                    data: function (f) {
                        f.class_id = $('#class_id').val();
                        f.section_id = $('#section_id').val();
//                        f.c_date = c_date;
                        f.view_type = $("#table-view").val()
                    },
                },
                columns:summary_dynamic_col,
//                    [
//                    {data: 'total_boys', name: 'total_boys'},
//                    {data: 'total_girls', name: 'total_girls'},
//                    {data: 'total_students', name: 'total_students'},
//                    {data: 'obc_boys', name: 'obc_boys'},
//                    {data: 'obc_girls', name: 'obc_girls'},
//                    {data: 'obc_total', name: 'obc_total'},
//                    {data: 'gen_boys', name: 'General Boys'},
//                    {data: 'gen_girls', name: 'General Girls'},
//                    {data: 'gen_total', name: 'General Total'},
//                    {data: 'sc_boys', name: 'S.C. Boys'},
//                    {data: 'sc_girls', name: 'S.C. Girls'},
//                    {data: 'sc_total', name: 'sc_total'},
//                    {data: 'st_boys', name: 'st_boys'},
//                    {data: 'st_girls', name: 'st_boys'},
//                    {data: 'st_total', name: 'st_total'},
//                    {data: 'minority_boys', name: 'minority_boys'},
//                    {data: 'minority_girls', name: 'minority_girls'},
//                    {data: 'minority_total', name: 'minority_total'},
//                    {data: 'promotion_boys', name: 'promotion_boys'},
//                    {data: 'promotion_girls', name: 'promotion_girls'},
//                    {data: 'promotion_total', name: 'promotion_total'},
//                    {data: 'new_admission_boys', name: 'new_admission_boys'},
//                    {data: 'new_admission_girls', name: 'new_admission_girls'},
//                    {data: 'new_admission_total', name: 'new_admission_total'},
//                    {data: 'drop_boys', name: 'drop_boys'},
//                    {data: 'drop_girls', name: 'drop_girls'},
//                    {data: 'drop_total', name: 'drop_total'},
//                    {data: 'teaching_this_month', name: 'teaching_this_month'},
//                    {data: 'teaching_upto_this_month', name: 'teaching_upto_this_month'},
//                    {data: 'teaching_total', name: 'teaching_total'},
//                    {data: 'average_attendance', name: 'average_attendance'},
//                ],

            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

        }
        
        $(document).on('change', '#class_id,#section_id', function (e) {
           summaryTableHeader(arr_col,null);
        });
    });
</script>



