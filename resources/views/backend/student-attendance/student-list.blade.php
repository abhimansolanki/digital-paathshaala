<table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
    <thead>
        <tr>
	<th style="width:10%;">{{trans('language.sr_no')}}</th>
	<th style="width:10%;">{{trans('language.enrollment_number')}}</th>
	<th style="width:30%;">{{trans('language.student_name')}}</th>
	<th style="width:30%;">{{trans('language.attendance')}}</th>
        </tr>
    </thead>
    <tbody>
        @php $counter = 0; @endphp
        @if(!empty($arr_student))
        @foreach ($arr_student as $key => $student)
        @php $counter++; 
        $student = (array)$student;
        @endphp
        <tr>
	<td>{!! $counter !!}</td>
	<td>{!! $student['enrollment_number'] !!}</td>
	<td>{!! $student['first_name'].' '.$student['middle_name'] .' '.$student['last_name'] !!}</td>
	<td>
	    @php $present =''; $absent = '';    @endphp
	    @if(!empty($absent_student_id)) 
	    @if(in_array($student['student_id'],$absent_student_id))
	    @php $present =''; $absent ='checked';    @endphp
	    @else
	    @php $present ='checked'; $absent ='';    @endphp
	    @endif
	    @else
	    @php $present ='checked'; $absent ='';    @endphp
	    @endif
	    {!! Form::hidden('student_id['.$student['student_id'].']',$student['student_id'],['class' => 'gui-input', 'id' => 'attendance_id', 'readonly' => 'true']) !!}
	    <div class="radio-custom radio-primary mb5">
	        <input type="radio" class="attenance-status"   id="attendance{{$counter}}"  value="1" name="arr_student_id_{{$student['student_id']}}" {!! $present !!}>
	        <label for="attendance{{$counter}}">Present</label>

	        <input type="radio"  class="attenance-status"  id="absent{{$counter}}" value="2"  name="arr_student_id_{{$student['student_id']}}" {!! $absent !!}>
	        <label for="absent{{$counter}}">Absent</label>
	    </div>
	</td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>

