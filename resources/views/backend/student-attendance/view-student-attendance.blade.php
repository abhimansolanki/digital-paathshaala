<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    .table-style {
        margin-left:11px;
    }
</style>
<div class="row">
    <table id="attendance-register-data" class="table-style">
        <thead>
	<tr>
	    <th>S.No</th>
	    <th>SR.No.</th>
	    <th>Student Name</th>
	    <th>father Name</th>
	    <th>Action</th>
	</tr>
        </thead>
        <tboady>
	@if(!empty($attendance_view))
	@foreach($attendance_view as $value)
	@php  $value = (array)$value; @endphp
	<tr>
	    <td>{!! $value['serial_no'] !!}</td>
	    <td>{!! $value['enrollment_number'] !!}</td>
	    <td>{!! $value['student_name'] !!}</td>
	    <td>{!! $value['father_name'] !!}</td>
	    <td><a title="View" id="" href="{{url('admin/view-student-attendance/' . $value['student_id'])}}">View Attendance</a></td>
	</tr>
	@endforeach
            @endif	  
        </tboady>
    </table>
</div>