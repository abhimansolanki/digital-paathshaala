@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
</style>
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span>student Absense Summary</span>
                </div>
            </div>
            <div class="panel" id="transportId">
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!! Form::text('attendance_date',date('Y-m-d'), ['class' => 'form-control date_piker','id' => 'attendance_date', 'readonly' => 'readonly','placeholder'=>'Select Date']) !!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-absense-summary" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>S.No</th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>Student Name</th>
                            <th>Class(Section)</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>{{trans('language.father_contact_number')}}</th>
                            <th>Absence From Days</th>
                            <th>Absence in this month</th>
                            <th>{{trans('language.action')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        studentAbsenseSummary();
        function studentAbsenseSummary()
        {
            var table = $('#student-absense-summary').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'student Absense Summary',
                        "filename": 'student-absense-summary',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'student Absense Summary',
                        "filename": 'student-absense-summary',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8],
                            modifier: {
                                selected: true
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: '{{ url('student_absence_summary_data')}}',
                    data: function (f) {
                        f.attendance_date = $("#attendance_date").val();
                    }
                },
                columns: [
                    {data: 'student_id', name: 'student_id'},
                    {data: 's_no', name: 's_no'},
                    {data: 'enrollment_number', name: 'enrollment_number'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'class_name', name: 'class_name'},
                    {data: 'father_name', name: 'father_name'},
                    {data: 'father_contact_number', name: 'father_contact_number'},
                    {data: 'absent_from_days', name: 'absent_from_days'},
                    {data: 'absent_this_month', name: 'absent_this_month'},
                    {data: 'action', name: 'action','orderable':false,'searchable':false},
                    
                ]
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
            table.on('select deselect', function (e, dt, type, indexes) {
                var arr_checked_student = checkedStudent();
                if (arr_checked_student.length > 0)
                {
                    $(".buttons-excel").prop('disabled', false);
                    $(".buttons-print").prop('disabled', false);
                } else
                {
                    $(".buttons-excel").prop('disabled', true);
                    $(".buttons-print").prop('disabled', true);
                }
            });
        }

        function checkedStudent()
        {
            var arr_checked_student = [];
            $.each($('#student-absense-summary').DataTable().rows('.selected').data(), function () {
                arr_checked_student.push(this["student_id"]);
            });
            return arr_checked_student;
        }
        $(document).on('change', '#class_id,#session_id', function (e) {
            if ($("#session_id").val() !== '')
            {
                studentAbsenseSummary();
            }
        });

        $("#attendance_date").datepicker('destroy');
        $(function () {
            var i = $("#attendance_date").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                dateFormat: "yy-mm-dd",
                minDate: '{!! $session["start_date"] !!}',
                maxDate: '{!! $session["end_date"] !!}',
                onSelect: function (c_date, inst) {
                    studentAbsenseSummary();
                }
            });
        });

    });

</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



