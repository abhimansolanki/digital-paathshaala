@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
    .display-icon{
        width: 15px; 
        height: 15px;
        border-radius: 100%;
        margin: 10px;
        float:left;
    }
    .display-text{
        margin-top: 7px;
        float:left;
    }
    .custom-table {
        font-size: 11px !important;   
    }
    .table > thead > tr > th{
        padding:4px !important;
        text-align: center !important;
    }
    .overlay {
        background-color:#EFEFEF;
        /*position: fixed;*/
        width: 100%;
        height: 100%;
        z-index: 1000;
        top: 0px;
        left: 0px;
        opacity: .5; /* in FireFox */ 
        filter: alpha(opacity=50); /* in IE */
    }
</style>
@php $current_segment2 = Request::segment(3); @endphp
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel" id="transportId">
                <!--<span style="font-size:18px;">Student Attendance Register</span>-->
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('class_id', $arr_class,'', ['class' => 'form-control','id'=>'class_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('section_id', $arr_section,'', ['class' => 'form-control','id'=>'section_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('summary_type', $summary_type,$current_segment2, ['class' => 'form-control','id'=>'summary_type'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                 @if($current_segment2 =='month' || $current_segment2 =='')
                                <div class="col-md-2">
                                    <label class="field select" style="width: 100%;">
                                        {!! Form::text('month_year',date('F Y'), ['class' => 'form-control date_piker','id' =>'month_year', 'readonly' => 'readonly','placeholder'=>'Select Calendar']) !!}
                                    </label>
                                </div>
                                 @endif
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body pn" style="border-top:1px solid #e5e5e5 !important;">
                <div style="overflow:auto;margin-bottom: 50px;">
                    <table class="table table-bordered table-striped table-hover" id="attendance-summary-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                            <th>SR. No.</th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>{{trans('language.student_name')}}</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>{{trans('language.father_contact_number')}}</th>
                            <th>{{trans('language.total_days')}}</th>
                            <th>{{trans('language.absent_days')}}</th>
                            <th>{{trans('language.present_days')}}</th>
                            <th>{{trans('language.present_per')}}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        studentAttendanceSummary();
        function studentAttendanceSummary(summary_type = '')
        {
            var table = $('#attendance-summary-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                pageLength: 100,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": $("#class_id option:selected").text() + ',' + $("#file-title").val(),
                        "filename": 'student-attendance-summary',
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": $("#class_id option:selected").text() + ',' + $("#file-title").val(),
                        "filename": 'student-attendance-summary',
                    }
                ],
                ajax: {
                    url: '{{ url('student_attendance_summary_data')}}',
                    data: function (f) {
                        f.class_id = $('#class_id').val();
                        f.section_id = $('#section_id').val();
                        f.summary_type = $("#summary_type").val();
                        f.month_year = $("#month_year").val()
                    },
                },
                columns: [
//                    {data: 'student_id', name: 'student_id'},
                    {data: 'serial_no', name: 'serial_no'},
                    {data: 'enrollment_number', name: 'enrollment_number'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'father_name', name: 'father_name'},
                    {data: 'father_contact_number', name: 'father_contact_number'},
                    {data: 'total_days', name: 'total_days'},
                    {data: 'absent_days', name: 'absent_days'},
                    {data: 'present_days', name: 'present_days'},
                    {data: 'present_per', name: 'present_per'},
                ],
            });
            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

        }



        function getClassSection()
        {
            $('select[name="section_id"]').empty();
            $('select[name="section_id"]').append('<option value="">--Select section--</option>');
            var class_id = $("#class_id").val();
            if (class_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $.each(resopose_data, function (key, value) {
                                $('select[name="section_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                    }
                });
            }
        }
        $("#month_year").datepicker('destroy');
        $(function () {
            var i = $("#month_year").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                dateFormat: "MM yy",
//                minDate: '{!! $session["start_date"] !!}',
//                maxDate: '{!! $session["end_date"] !!}',
                onChangeMonthYear: function (year, month, inst) {
                    var n = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    var month_num = parseInt(month) - 1;
                    var month_year = n[month_num] + ' ' + year;
                    $("#month_year").datepicker("setDate" , month_year);
                    studentAttendanceSummary();
                },
            });
        });

        $(document).on('change', '#class_id,#section_id', function (e) {
            $("#section-name").text('');
            studentAttendanceSummary()
        });
        $(document).on('change', '#summary_type', function (e) {
            var summary_type = $(this).val();
            var url = '{!! url("admin/attendance-summary/' + summary_type + '") !!}';
            window.location.replace(url);
        });

    });
</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



