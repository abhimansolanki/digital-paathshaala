@extends('admin_panel/layout')
@section('content')
<style>
    td.details-control {
        background: url(<?php echo url('/public/details_open.png'); ?>) no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url(<?php echo url('/public/details_close.png'); ?>) no-repeat center center;
    }
</style>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="tray tray-center tableCenter">
                    <div class="panel panel-visible mw1000" id="spy2">
                        <div id="topbar">
                            <div class="topbar-left">
                                <ol class="breadcrumb">
                                    <li class="crumb-active">
                                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                                    </li>
                                    <li class="crumb-trail">View Exam Time Table </li>
                                </ol>
                            </div>
                            <div class="topbar-right">
                                <a  href="{{ url('admin/exam-time-table/add') }}" class="button btn-primary text-right pull-right" title="View Student" style="padding:8px;">Add Exam Time Table</a>
                            </div>
                        </div>
                        <div class="panel-heading">
                            <div class="panel-title hidden-xs col-md-6">
                                <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_exam_time_table')}}</span>
                            </div>
                        </div>
                        <div class="panel-body pn">
                            @if(Session::has( 'success'))
		    @include('backend.partials.messages')
                            @endif
                            <table class="table table-bordered table-striped table-hover" id="exam-time-table-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>{!!trans('language.title') !!}</th>
                                        <th>{!!trans('language.exam') !!}</th>
                                        <th>{!!trans('language.publish') !!}</th>
                                        <th>{!!trans('language.action') !!}</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#exam-time-table-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('exam-time-table/data') }}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                },
                {data: 'title', name: 'title'},
                {data: 'exam_name', name: 'exam_name'},
                {data: 'publish_status', name: 'publish_status'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $('#exam-time-table-table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                //console.log(row.data());
                // Open this row
                var child = format(row.data());
                //console.log(child);
                if (child !== '')
                {
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            }
        });
        function format(d) {
            // `d` is the original data object for the row
            var exam_details = d.exam_details;
            var exam_time_table_id = d.encrypted_exam_time_table_id;
            var tab_body = '';
            var tab_head = '<table cellpadding="5" cellspacing="0" border="0" style="margin-left: -4px; font-size:11px !important;">';
            tab_head = tab_head +
                    '<tr style="font-weight: 600;">' +
                    '<th style="width: 7%;"></th>' +
                    '<th style="width: 36%;">Class Name</th>' +
                    '<th style="width: 36%;">Section Name</th>' +
                    '<th></th>' +
                    '</tr>';
            ;
            $.each(exam_details, function (key, value)
            {
                tab_body = tab_body +
                        '<tr>' +
                        '<td></td>' +
                        '<td>' + value.details.exam_time_table_class.class_name + '</td>' +
                        '<td>' + value.details.exam_time_table_section.section_name + '</td>' +
                        '<td>' +
                        '<a title="Edit" id="deletebtn1" href="{{ url("admin/exam-time-table-detail/add") }}' + '/' + exam_time_table_id + '/' + value.id + '"  class="btn btn-success"><i class="fa fa-edit" ></i></a>' +
                        '<button title="Delete" id="deletebtn" class="btn btn-danger delete-detail-button" data-id="' + value.details.detail_id + '"><i class="fa fa-trash"></i></button>' +
                        '</td>' +
                        '</tr>';
            });
            var tab_foot = '</table>';
            var table_data = '';
            if (tab_body !== '')
            {
                table_data = tab_head + tab_body + tab_foot;
                //table_data = tab_head + tab_foot;
            }
            return table_data;
        }

        $(document).on("click", ".delete-detail-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax({
                            url: "{{ url('exam-time-table-detail/delete') }}",
                            datatType: 'json',
                            type: 'GET',
                            data: {
                                'detail_id': id,
                            },
                            success: function (res)
                            {
                                if (res.status == "success")
                                {
                                    table.ajax.reload();

                                } else if (res.status === "used")
                                {
                                    $('#server-response-message').text(res.message);
                                    $('#alertmsg-student').modal({
                                        backdrop: 'static',
                                        keyboard: false
                                    });
                                }
                            }
                        });
                    }
                }
            });

        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax({
                            url: "{{ url('exam-time-table/delete/') }}",
                            datatType: 'json',
                            type: 'POST',
                            data: {
                                'exam_time_table_id': id,
                            },
                            success: function (res)
                            {
                                if (res.status == "success")
                                {
                                    table.ajax.reload();

                                } else if (res.status === "used")
                                {
                                    $('#server-response-message').text(res.message);
                                    $('#alertmsg-student').modal({
                                        backdrop: 'static',
                                        keyboard: false
                                    });
                                }
                            }
                        });
                    }
                }
            });

        });
    });
</script>
@endsection
