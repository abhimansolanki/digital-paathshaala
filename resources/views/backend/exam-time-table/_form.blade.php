<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.exam_time_table')}}</li>
                </ol>
            </div>
	<div class="topbar-right">
	    <a  href="{{ url('admin/exam-time-table') }}" class="button btn-primary text-right pull-right" title="View Student" style="padding:8px;">View Exam Time Table</a>
            </div>
        </div>
        @if(Session::has( 'success'))
        @include('backend.partials.messages')
        @endif
        <div class="bg-light panelBodyy">
            <div class="section-divider mv40">
                <span><i class="fa fa-graduation-cap"></i> Exam's Time Table</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.title')}}<span class="asterisk">*</span></span></label>
                    <label for="time-table-title" class="field prepend-icon">
                        {!! Form::text('title', old('title',isset($exam_time_table['title']) ? $exam_time_table['title'] : ''), ['class' => 'gui-input',''=>trans('language.title'), 'id' => 'time-table-title','pattern'=>'[a-zA-Z][a-zA-Z0-9\s\.]*']) !!}
                        <label for="time-table-title" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('title'))
                    <p class="help-block">{{ $errors->first('title') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.exam')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('exam_id', $exam_time_table['arr_exam'],isset($exam_time_table['exam_id']) ? $exam_time_table['exam_id'] : null, ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label> @if ($errors->has('exam_id'))
                    <p class="help-block">{{ $errors->first('exam_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2" style="margin-top: 21px;">
                    <div class="option-group field" id="checkBooxx">
                        <label class="option block option-primary">
                            @php $checked = !empty($exam_time_table['publish']) && ($exam_time_table['publish'] == 1) ? true : false; @endphp
                            {!! Form::checkbox('publish','',$checked, ['class' => 'gui-input ', 'id' => 'publish']) !!}
                            <span class="checkbox radioBtnpan"></span><em>{{trans('language.publish')}} </em>
                        </label>
                    </div>
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan"></span></label>
                    <label class="field select">
                        {!! Form::submit($submit_button, ['class' => 'button btn-primary','style'=>'width:100px;margin-top: 6px;']) !!}
                    </label> 
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
