@extends('admin_panel/layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="box box-info">
                {!! Form::open(['files'=>TRUE,'id' => 'exam-time-table-form' , 'class'=>'form-horizontal','url' => $save_url,'autocomplete'=>'off']) !!}
                @include('backend.exam-time-table._form',['submit_button' =>$submit_button])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#exam-time-table-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                title: {
                    required: true,
                    // nowhitespace: true
                },
                exam_id: {
                    required: true
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
    });
</script>
@endsection
