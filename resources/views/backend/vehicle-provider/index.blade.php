@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_subject')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="vehicle-provider-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.full_name') !!}</th>
                            <th>{!!trans('language.email') !!}</th>
                            <th>{!!trans('language.contact_number') !!}</th>
                            <th>{!!trans('language.address') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        var table = $('#vehicle-provider-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ url('vehicle-provider/data') }}',
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'full_name', name: 'full_name'},
                {data: 'email_address', name: 'email_address'},
                {data: 'contact_number', name: 'contact_number'},
                {data: 'address', name: 'address'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('vehicle-provider/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'vehicle_provider_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();

                                        }
                                    }
                                });
                    }
                }
            });

        });
    });
</script>
@endsection
@push('scripts')
@endpush

