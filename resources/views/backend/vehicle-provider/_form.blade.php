<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_vehicle_provider')}}</li>
                </ol>
            </div>
            <div class="topbar-right">
                <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right">{{trans('language.view_vehicle_provider')}}</a>
            </div>
        </div>
        <!--@include('backend.partials.messages')-->
        <div class="bg-light panelBodyy">

            <div class="section-divider mv40">
                <span><i class="fa fa-user"></i>Vehicle Provider Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan"></span>{{trans('language.full_name')}}</label>
                    <label for="full_name" class="field prepend-icon">
                        {!! Form::text('full_name', old('full_name',isset($vehicle_provider['full_name']) ? $vehicle_provider['full_name'] : ''), ['class' => 'gui-input',''=>trans('language.full_name'), 'id' => 'full_name']) !!}
                        <label for="full_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('full_name')) 
                    <p class="help-block">{{ $errors->first('full_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.email') !!}</span></label>
                    <label for="email_address" class="field prepend-icon">
                        {!! Form::text('email_address', old('email_address',isset($vehicle_provider['email_address']) ? $vehicle_provider['email_address'] : ''), ['class' => 'gui-input', 'id' => 'email_address']) !!}
                        <label for="email_address" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('email_address')) <p class="help-block">{{ $errors->first('email_address') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.address') !!}</span></label>
                    <label for="class" class="field prepend-icon">
                        {!! Form::text('address', old('address',isset($vehicle_provider['address']) ? $vehicle_provider['address'] : ''), ['class' => 'gui-input', 'id' => 'class']) !!}
                        <label for="class" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                        @if ($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif
                    </label>
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.contact_number') !!}</span></label>
                    <label for="class" class="field prepend-icon">
                        {!! Form::number('contact_number', old('contact_number',isset($vehicle_provider['contact_number']) ? $vehicle_provider['contact_number'] : ''), ['class' => 'gui-input', 'id' => 'class']) !!}
                        <label for="class" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('contact_number')) <p class="help-block">{{ $errors->first('contact_number') }}</p> @endif
                </div>

            </div>

        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

        $("#vehicle-provider-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                address: {
                    required: true
                },
                contact_number: {
                    required: true
                },
                email_address: {
                    required: true
                },
                full_name: {
                    required: true
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });
</script>