@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{trans('language.view_subject')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-striped table-hover" id="sms-template-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.sms_name') !!}</th>
                            <th>{!!trans('language.sms_heading') !!}</th>
                            <th>{!!trans('language.sms_body') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        var table = $('#sms-template-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ url('sms-template/data') }}',
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'sms_name', name: 'sms_name'},
                {data: 'sms_heading', name: 'sms_heading'},
                {data: 'sms_body', name: 'sms_body'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('sms-template/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'sms_template_id': id,
                                    },
                                   
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();

                                        } 
                                    }
                                });
                    }
                }
            });

        });
    });
</script>
@endsection
@push('scripts')
@endpush

