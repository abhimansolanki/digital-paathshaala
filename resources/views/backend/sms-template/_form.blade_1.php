<!-- Field Options -->
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <a href="{{$redirect_url}}" class="button btn-primary" title="">{{trans('language.view_sms_template')}}</a>
        <div class="panel-body bg-light">
            <div class="section-divider mt20 mb40">
                <span>{{trans('language.add_sms_template')}}</span>
            </div>
            <!-- .section-divider -->
            <div class="section row" id="spy1">
                <div class="col-md-4">
                    <label><span class="radioBtnpan">{!! trans('language.sms_name') !!}</span></label>
                    <label for="sms_name" class="field prepend-icon">
                        {!! Form::text('sms_name', old('sms_name',isset($sms_template->sms_name) ? $sms_template->sms_name : ''), ['class' => 'gui-input','placeholder'=>trans('language.sms_name'), 'id' => 'sms_name']) !!}
                        @if ($errors->has('sms_name')) <p class="help-block">{{ $errors->first('sms_name') }}</p> @endif
                        <label for="sms_name" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                </div>
                <div class="col-md-4">
                    <label><span class="radioBtnpan">{!! trans('language.sms_heading') !!}</span></label>
                    <label for="sms_heading" class="field prepend-icon">
                        {!! Form::text('sms_heading', old('sms_heading',isset($sms_template->sms_heading) ? $sms_template->sms_heading : ''), ['class' => 'gui-input','placeholder'=>trans('language.sms_heading'), 'id' => 'sms_heading']) !!}
                        @if ($errors->has('sms_heading')) <p class="help-block">{{ $errors->first('sms_heading') }}</p> @endif
                        <label for="sms_heading" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                </div>
                </div>
            <div class="section row" id="spy1">
                <div class="col-md-4">
                    <label><span class="radioBtnpan">{!! trans('language.sms_body') !!}</span></label>
                    <label class="field">
                        {!!Form::textarea('sms_body',old('sms_body',isset($sms_template->sms_body) ? $sms_template->sms_body : ''), ['class' => 'gui-input'])!!}
                        @if ($errors->has('sms_body')) <p class="help-block">{{ $errors->first('sms_body') }}</p> @endif
                        <i class="arrow double"></i>
                    </label>
                </div>
                <!-- end section -->
            </div>
            <!-- end section -->
            <!-- end .section row section -->
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
        <!-- end .form-footer section -->
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

        $("#sms-template-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                sms_name: {
                    required: true
                },
                sms_heading: {
                    required: true
                },
                sms_body: {
                    required: true
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });
</script>