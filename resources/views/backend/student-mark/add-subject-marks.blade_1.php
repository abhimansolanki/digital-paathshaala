<style type="text/css">
    .marks {
        height: 25px;
        width: auto;
    }
    .tot_marks {
        height: 25px;
        width: 100%;
    }
    .lock-btn{
        width: 20px;
        height: 25px !important;
        line-height: 0px !important;
        padding: 0px !important;
        background: #fafafa05 !important;
        vertical-align:middle !important;
    }
    .state-error{
        display: block!important;
        margin-top: 5px !important;
        padding: 0;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 9px !important;
        color: #DE888A !important;
    }
    th{
        vertical-align: middle !important;
    }
    input{
        border: 1px solid #ccc !important;
        margin: 5px !important;
    }
    .form-group{
        margin-bottom: 0px;
    }
 table td{
        border: 0px !important;
    }
     table {
        margin-bottom: 0px;
     }
     tr{
          border: 0px !important;
     }
</style>
@if(!empty($arr_student_exam))
<div class="" style="overflow-x: scroll; width: 100%">
    <table class="table table-bordered" style="width:auto;">
        <thead>
            <tr style="font-size: 12px !important; text-align: center;">
                <th >S.No.</th>
                <th >S.R</th>
                <th ><div class="" style="min-width: 50px;">Roll No</div></th>
                <th> <div class="" style="min-width: 100px;"> Student Name</div></th>
                @foreach($arr_student_exam['exam_subject_marks'] as $subject_id => $subject)
                @php $col_count = count($subject); @endphp
                <th  style="text-align:center;">
                    {!! $subject['subject_name'] !!} <!--subject name -->
                    <button type="button" class="button btn-primary lock-btn all_subject_lock_unlock" id="" rel="{{$subject_id}}">
                        <i class="fa fa-lock" id="lock-unlock-{{$subject_id}}" style="color: #575553;"></i>
                    </button>
                    @if(isset($subject['sub_subject']) && !empty($subject['sub_subject']))
                    <table>
                        <tbody>
                            <tr>
                                @foreach($subject['sub_subject'] as  $sub_subject_id =>$sub_subject)
                                <th style="text-align:center">{!! $sub_subject['sub_subject_name'] !!}
                                    @if(isset($subject['exam_type']['sub_subject'][$sub_subject_id]) && !empty($subject['exam_type']['sub_subject'][$sub_subject_id]))
                                    <table>
                                        <tbody>
                                            <tr>
                                                @foreach($subject['exam_type']['sub_subject'][$sub_subject_id] as  $exam_type_id =>$exam_type)
                                                <th>
                                                    {!! $arr_exam_type[$exam_type_id] !!}  
                                                </th>
                                                @endforeach
                                                <th>Tot. Obt.</th>
                                            </tr>
                                        </tbody>
                                    </table>
                                    @endif
                                </th>
                                @endforeach
                                <th>Sub. Tot. Obt.</th>
                            </tr>
                        </tbody>
                    </table>
                    @else
                    <table>
                        <tbody>
                            <tr>
                                @foreach($subject['exam_type'] as  $exam_type_id =>$exam_type)
                                <th>
                                    {!! $arr_exam_type[$exam_type_id] !!}  
                                </th>
                                @endforeach
                                <th>Tot. Obt.</th>
                            </tr>
                        </tbody>
                    </table>
                    @endif
                </th>
                @endforeach
                <th  style="text-align:center;">Tot. Obtained</th>
                <th >%</th>
                <th >Rank</th>
            </tr>
        </thead>
        <tbody>
            @php  $count = 1;@endphp 
            @foreach($arr_student_exam['exam_student_list'] as $key => $student_exam)
            @php $student_id = $student_exam['student_id']; $tot_stu_sub_max_marks = 0;
            $total_obtained_marks = isset($student_exam['obtained_marks']['tot_obtained']) ? $student_exam['obtained_marks']['tot_obtained'] : null;
            @endphp 
            <tr class="student_row_id" rel='{{$student_id}}'>
                <td style="border: 1px solid #eee !important;">
                    {!! $count !!}
                </td>
                <td style="border: 1px solid #eee !important;">
                    {!! $student_exam['enrollment_number'] !!}
                </td>
                <td style="border: 1px solid #eee !important;">
                    {!! Form::hidden('mark_detail_id_'.$student_id,isset($student_exam['student_mark_detail_id']) ? $student_exam['student_mark_detail_id'] : null, ['class' => 'gui-input','id' => 'student_mark_detail_id']) !!}
                    {!! $student_exam['roll_number'] !!} 
                </td>
                <td style="border: 1px solid #eee !important;">
                    <input type="hidden" name="arr_student_id[]" value="{{$student_id}}" id="student_id" class="student_id"  readonly="">
                    {!! $student_exam['student_name'] !!}
                </td>
                <!-- class subject array loop start -->
                @foreach($arr_student_exam['exam_subject_marks'] as $subject_id => $subject)
                @php $tot_sub_max_marks = 0; @endphp
                @php $sub_tot_obtained_marks = isset($student_exam['obtained_marks'][$subject_id]['sub_tot_obtained']) ? $student_exam['obtained_marks'][$subject_id]['sub_tot_obtained'] : null; @endphp
        <input type="hidden" name="arr_subject_id[]" value="{{$subject_id}}" id="subject_id" class="subject_id"  readonly="">
        <td  style="text-align:center; border: 1px solid #eee !important;" >
            @if(isset($subject['sub_subject']) && !empty($subject['sub_subject']))
            <table >
                <tbody>
                    <tr>
                        @foreach($subject['sub_subject'] as  $sub_subject_id =>$sub_subject)
                        @php $sub_sub_max_marks = 0; @endphp
                        @php $sub_sub_tot_obtained_marks = isset($student_exam['obtained_marks'][$subject_id]['sub_subject'][$sub_subject_id]['sub_sub_tot']) ? $student_exam['obtained_marks'][$subject_id]['sub_subject'][$sub_subject_id]['sub_sub_tot'] : null; @endphp
                <input type="hidden" name="arr_sub_subject_id{{$subject_id}}[]" value="{{$sub_subject_id}}" id="sub_subject_id" class="sub_subject_id"  readonly="">
                <td style="text-align:center">
                    @if(isset($subject['exam_type']['sub_subject'][$sub_subject_id]) && !empty($subject['exam_type']['sub_subject'][$sub_subject_id]))
                    <table border="0" style="border: 0px !important;">  
                        <tbody>
                            <tr>
                            @foreach($subject['exam_type']['sub_subject'][$sub_subject_id] as  $exam_type_id =>$exam_type)
                            @php $sub_sub_marks = isset($student_exam['obtained_marks'][$subject_id]['sub_subject'][$sub_subject_id][$exam_type_id]) ? $student_exam['obtained_marks'][$subject_id]['sub_subject'][$sub_subject_id][$exam_type_id] : null; 
                            @endphp
                                <input type="hidden" name="arr_exam_type_id[]" value="{{$exam_type_id}}" id="exam_type_id" class="exam_type_id"  readonly="">
                                <td>
                                    <div class=" padding_remove">
                                        <div class="form-group"> 
                                            @if(!in_array($student_id,$subject['un_assigned_student']))
                                            @php
                                            $sub_sub_max_marks = $sub_sub_max_marks + $exam_type['max_marks'];
                                            @endphp
                                            {!! Form::number('obtained_subject_marks_'.$student_id.'_'.$subject_id.'_'.$sub_subject_id.'_'.$exam_type_id,$sub_sub_marks, ['class' => 'marks subject_lock_unlock'.$subject_id.' student_subject_'.$student_id.' subject_'.$student_id.'_'.$subject_id.' sub_subject_'.$student_id.'_'.$subject_id.'_'.$sub_subject_id,'id' =>'','rel'=>$subject_id,'min'=>'0','max'=>$exam_type['max_marks'],'required'=>true]) !!}
                                            @else
                                             @php
                                            $sub_sub_max_marks = $sub_sub_max_marks + 0;
                                            @endphp
                                            NA
                                            @endif
                                        </div>
                                    </div>
                                </td>
                        @endforeach
                        <td  style="width: 110px;">
                            <div class="" style="min-width: 120px;">
                            M : <span id="sub-subject-marks-text{{$student_id}}{{$subject_id}}{{$sub_subject_id}}">{!! $sub_sub_tot_obtained_marks !!}</span>
                          | G : <span id="sub-subject-grade-text{{$student_id}}{{$subject_id}}{{$sub_subject_id}}"></span>
                            {!! Form::hidden('tot_obtained_sub_subject_marks'.$student_id.'_'.$subject_id,$sub_tot_obtained_marks, ['class' => 'tot_marks'.$subject_id.' st_sub_sub_marks'.$student_id.$subject_id,'id'=>'sub_subject_tot_obt'.$student_id.$subject_id.$sub_subject_id,'rel'=>$sub_subject_id,'min'=>'0','max'=>$sub_sub_max_marks,'readonly'=>true,]) !!}
                            </div>
                        </td>
                        </tr>
                        </tbody>
                    </table>
                    @endif
                </td>
                @php $tot_sub_max_marks = $tot_sub_max_marks + $sub_sub_max_marks; @endphp
                @endforeach
                <td>
                    <div class="" style="min-width: 115px;">
                    M : <span id="subject-marks-text{{$student_id}}{{$subject_id}}">{!! $sub_tot_obtained_marks !!}</span>
                    &nbsp;|&nbsp;
                    G : <span id="subject-grade-text{{$student_id}}{{$subject_id}}"></span>
                    {!! Form::hidden('tot_obtained_subject_marks'.$student_id.'_'.$subject_id,$sub_tot_obtained_marks, ['class' => 'tot_marks '.$subject_id.' st_sub_marks'.$student_id,'id'=>'subject_tot_obt'.$student_id.$subject_id,'rel'=>$subject_id,'min'=>'0','max'=>$tot_sub_max_marks,'readonly'=>true,'sub-subject'=>'yes']) !!}
                </div>
                </td>
                </tr>
                </tbody>
            </table>
            @else
            <table>
                <tbody>
                    <tr>
                        @foreach($subject['exam_type'] as  $exam_type_id =>$exam_type)
                        @php $sub_marks = isset($student_exam['obtained_marks'][$subject_id][$exam_type_id]) ? $student_exam['obtained_marks'][$subject_id][$exam_type_id] : null; @endphp
                <input type="hidden" name="arr_exam_type_id[]" value="{{$exam_type_id}}" id="exam_type_id" class="exam_type_id"  readonly="">
                <td>
                    <div class=" padding_remove">
                        <div class="form-group"> 
                            @if(!in_array($student_id,$subject['un_assigned_student']))
                             @php $tot_sub_max_marks = $tot_sub_max_marks + $exam_type['max_marks']; @endphp
                            {!! Form::number('obtained_subject_marks_'.$student_id.'_'.$subject_id.'_'.$exam_type_id,$sub_marks, ['class' => 'marks subject_lock_unlock'.$subject_id.' student_subject_'.$student_id.' subject_'.$student_id.'_'.$subject_id,'id' =>'','rel'=>$subject_id,'min'=>'0','max'=>$exam_type['max_marks'],'required'=>true]) !!}
                            @else
                            @php $tot_sub_max_marks = $tot_sub_max_marks + 0; @endphp
                            NA
                            @endif
                        </div>
                    </div>
                </td>
                @endforeach
                <td>
                     <div class="" style="min-width: 115px;">
                    M : <span id="subject-marks-text{{$student_id}}{{$subject_id}}">{!! $sub_tot_obtained_marks !!}</span>
                    &nbsp;|&nbsp;
                    G : <span id="subject-grade-text{{$student_id}}{{$subject_id}}"></span>
                    {!! Form::hidden('tot_obtained_subject_marks'.$student_id.'_'.$subject_id,$sub_tot_obtained_marks, ['class' => 'tot_marks subject_lock_unlock'.$subject_id.' st_sub_marks'.$student_id,'id'=>'subject_tot_obt'.$student_id.$subject_id,'rel'=>$subject_id,'min'=>'0','max'=>$tot_sub_max_marks,'readonly'=>true,'sub-subject'=>'no']) !!}
                </div>
                </td>
                </tr>
                </tbody>
            </table>
            @endif
        </td>
        @php $tot_stu_sub_max_marks = $tot_stu_sub_max_marks + $tot_sub_max_marks; @endphp
        @endforeach
        <!-- class subject array loop end -->
        <td style="width:100% !important; border: 1px solid #eee !important;">
              <div class="" style="min-width: 100px;">
            <!--student all subjects total obtained marks-->
            {!! Form::hidden('total_obtained_marks_'.$student_id,$total_obtained_marks, ['class' => 'tot_marks total_marks_sum','id' => 'student_tot_obt'.$student_id,'readonly'=>true,'rel'=>$student_id,'min'=>'0','max'=>$tot_stu_sub_max_marks,'required'=>true])!!}
            M : <span id="all-subject-marks-text{{$student_id}}"> {!! $total_obtained_marks !!} </span>
            &nbsp;|&nbsp;
            G : <span id="all-subject-grade{{$student_id}}"></span>
        </div>
        </td>
        <td style="border: 1px solid #eee !important;"">
            {!! Form::hidden('percentage'.$student_id,'', ['class' =>'st_percentage'.$student_id,'id'=>'student_per'.$student_id,'rel'=>$student_id,'readonly'=>true]) !!}
            <span id="student-percentage{{$student_id}}"></span></td>
        <td style="border: 1px solid #eee !important;""><span id="student-rank{{$student_id}}"></span></td>
        </tr>
        @php $count++; @endphp 
        @endforeach 
        </tbody>
    </table>
</div>
@endif
<script>
    $('.marks').attr('readonly', true);
    $('.marks').attr('required', false);
    $('.marks').css("background-color", "#efefea");
    $('.marks').css("border", "none");
    $('.marks').attr('tabindex', '-1');
</script>

