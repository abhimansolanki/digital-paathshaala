@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_student_marks')}}</span>
                </div>

                <div class="topbar-right">
                    <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right custom-btn">{{trans('language.add_marks')}}</a>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-mark-table" cellspacing="0" width="100%">                    
                    <thead>
                        <tr>
                            <!--<th>{{trans('language.session_year')}}</th>-->
                            <th>{{trans('language.exam_name')}}</th>
                            <th>{{trans('language.class_name')}}</th>
                            <th>{{trans('language.section_name')}}</th>
                            <th>{{trans('language.result_in')}}</th>
                            <th>{{trans('language.publish_status')}}</th>
                            <th class="text-center">{{trans('language.action')}}</th>
                            <th class="text-center">{{trans('language.view_summary')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#student-mark-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('student-mark/data')}}",
            columns: [
//                {data: 'session_year', name: 'session_year'},
                {data: 'exam_name', name: 'exam_name'},
                {data: 'class_name', name: 'class_name'},
                {data: 'section_name', name: 'section_name'},
                {data: 'result_in_name', name: 'result_in_name'},
                {data: 'publish', name: 'publish'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'view', name:'view', orderable: false, searchable: false}

            ]
        });
        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('fee-parameter/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'fee_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();

                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
    });

</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



