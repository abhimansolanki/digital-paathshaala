<style type="text/css">
    .marks {
        height: 25px;
        width: auto;
    }
    .tot_marks {
        height: 25px;
        width: 100%;
    }
    .lock-btn{
        width: 20px;
        height: 25px !important;
        line-height: 0px !important;
        padding: 0px !important;
        background: #fafafa05 !important;
        vertical-align:middle !important;
    }
    .state-error{
        display: block!important;
        margin-top: 5px !important;
        padding: 0;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 9px !important;
        color: #DE888A !important;
    }
    th{
        vertical-align: middle !important;
    }
    input{
        border: 1px solid #ccc !important;
        margin: 5px !important;
    }
    .form-group{
        margin-bottom: 0px;
    }
    table td{
        border: 0px !important;
    }
    table {
        margin-bottom: 0px;
    }
    tr{
        border: 0px !important;
    }
</style>
@if(!empty($arr_student_exam))
@php $total_max_marks = 0; @endphp
<div class="" style="overflow-x: scroll; width: 100%">
    <table class="table table-bordered" style="width:auto;">
        <thead>
            <tr style="font-size: 12px !important; text-align: center;">
                <th >S.No.</th>
                <th >S.R</th>
                <th ><div class="" style="min-width: 50px;">Roll No</div></th>
                <th> <div class="" style="min-width: 100px;"> Student Name</div></th>
                <!--all subject list with its type loop start-->
                @foreach($arr_student_exam['exam_subject_marks'] as $subject_id => $subject)

                <!--explode subject type of each subject like written, practical &  oral-->
	    @if(isset($arr_student_exam['subject_marks_criteria'][$subject_id]))
	    @php
	    $arr_type = $arr_student_exam['subject_marks_criteria'][$subject_id];
	    unset($arr_type['total_max_marks']);
	    $arr_subject_type_id = array_keys($arr_type);
	    @endphp
	    @endif
                @php $col_count = count($subject); 
                
                @endphp
                <th  style="text-align:center;">
                    {!! $subject['subject_name'] !!} <!--subject name -->
                    <button type="button" class="button btn-primary lock-btn all_subject_lock_unlock" id="" rel="{{$subject_id}}">
                        <i class="fa fa-lock" id="lock-unlock-{{$subject_id}}" style="color: #575553;"></i>
                    </button>
                    <table>
                        <tbody>
                            <tr>
                                <!--subject type loop for header-->
                                @foreach($arr_subject_type_id as  $key =>$subject_type_id)
                                <th>
                                    <!--get sum of subject max marks-->
                                    @php 
                                    $subject_type_max_marks = isset($arr_student_exam['subject_marks_criteria'][$subject_id][$subject_type_id]['max_marks']) ? 
                                    $arr_student_exam['subject_marks_criteria'][$subject_id][$subject_type_id]['max_marks']:0;
                                    @endphp
                                    {!! $arr_subject_type[$subject_type_id] !!}
                                    <br>
                                    (max : {!! $subject_type_max_marks !!})

                                </th>
                                @endforeach
                                <th>Tot. Obt.
                                    <!--get maximum marks of all subject-->
                                    @php 
                                    $subject_max_marks = isset($arr_student_exam['subject_marks_criteria'][$subject_id]['total_max_marks']) ? 
                                    $arr_student_exam['subject_marks_criteria'][$subject_id]['total_max_marks']:0;

                                    $total_max_marks = $total_max_marks +  $subject_max_marks;                                    
                                    @endphp
                                    <br>
                                    (sub max : {!! $subject_max_marks !!})
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </th>
                @endforeach
                <th  style="text-align:center;">Tot. Obtained
                    <br>
                    (tot max : {!! $total_max_marks !!})
                </th>
                <th >%</th>
                <th >Rank</th>
            </tr>
        </thead>
        <tbody>
            @php  $count = 1;@endphp 
            <!--start student loop-->
            @foreach($arr_student_exam['exam_student_list'] as $key => $student_exam)

            <!--get subject obtained marks-->
            @php $student_id = $student_exam['student_id']; $tot_stu_sub_max_marks = 0;
            $total_obtained_marks = isset($student_exam['obtained_marks']['tot_obtained']) ? $student_exam['obtained_marks']['tot_obtained'] : null;
            @endphp 
            <tr class="student_row_id" rel='{{$student_id}}'>
                <td style="border: 1px solid #eee !important;">
                    {!! $count !!}
                </td>
                <td style="border: 1px solid #eee !important;">
                    {!! $student_exam['enrollment_number'] !!}
                </td>
                <td style="border: 1px solid #eee !important;">
                    {!! Form::hidden('mark_detail_id_'.$student_id,isset($student_exam['student_mark_detail_id']) ? $student_exam['student_mark_detail_id'] : null, ['class' => 'gui-input','id' => 'student_mark_detail_id']) !!}
                    {!! $student_exam['roll_number'] !!} 
                </td>
                <td style="border: 1px solid #eee !important;">
                    <input type="hidden" name="arr_student_id[]" value="{{$student_id}}" id="student_id" class="student_id"  readonly="">
                    {!! $student_exam['student_name'] !!}
                </td>
                <!-- class subject array loop start -->
                @foreach($arr_student_exam['exam_subject_marks'] as $subject_id => $subject)

	    @if(isset($arr_student_exam['subject_marks_criteria'][$subject_id]))
	    @php
	    $arr_type = $arr_student_exam['subject_marks_criteria'][$subject_id];
	    unset($arr_type['total_max_marks']);
	    $arr_subject_type_id = array_keys($arr_type);
	    @endphp
	    @endif
                @php $tot_sub_max_marks = 0; 
               
                @endphp

                @php $sub_tot_obtained_marks = isset($student_exam['obtained_marks'][$subject_id]['sub_tot_obtained']) ? $student_exam['obtained_marks'][$subject_id]['sub_tot_obtained'] : null; @endphp
        <input type="hidden" name="arr_subject_id[]" value="{{$subject_id}}" id="subject_id" class="subject_id"  readonly="">
        <td  style="text-align:center; border: 1px solid #eee !important;" >
            <table>
                <tbody>
                    <tr>
                        @foreach($arr_subject_type_id as  $key =>$subject_type_id)

                        @php $sub_marks = isset($student_exam['obtained_marks'][$subject_id][$subject_type_id]) ? $student_exam['obtained_marks'][$subject_id][$subject_type_id] : null; 
                        $unassigned_student = json_decode($subject['unassigned_student_id'],true);
                        $unassigned_student = !empty($unassigned_student) ? $unassigned_student :[];
                        $subject_max_marks = isset($arr_student_exam['subject_marks_criteria'][$subject_id][$subject_type_id]['max_marks']) ? 
                        $arr_student_exam['subject_marks_criteria'][$subject_id][$subject_type_id]['max_marks']:0;

                        $subject_min_marks = isset($arr_student_exam['subject_marks_criteria'][$subject_id][$subject_type_id]['min_marks']) ? 
                        $arr_student_exam['subject_marks_criteria'][$subject_id][$subject_type_id]['min_marks']:0;

                        @endphp
                <input type="hidden" name="arr_subject_type_id[]" value="{{$subject_type_id}}" id="subject_type_id" class="subject_type_id"  readonly="">
                <td style="width:40% !important; border-right: 1px solid #eee !important;">
                    <div class=" padding_remove">
                        <div class="form-group"> 
                            <!--print subject type like writtrn, practical and oral -->
                            @if(!in_array($student_id,$unassigned_student))

                            @php $tot_sub_max_marks = ($tot_sub_max_marks + $subject_max_marks); @endphp

                            {!! Form::number('obtained_subject_marks_'.$student_id.'_'.$subject_id.'_'.$subject_type_id,$sub_marks, ['class' => 'marks subject_lock_unlock'.$subject_id.' student_subject_'.$student_id.' subject_'.$student_id.'_'.$subject_id,'id' =>'','rel'=>$subject_id,'min'=>'0','max'=>$subject_max_marks,'required'=>true]) !!}

                            @else
                            @php $tot_sub_max_marks = $tot_sub_max_marks + 0; @endphp
                            NA
                            @endif
                        </div>
                    </div>
                </td>
                @endforeach
                <td>
                    <!--total marks of subject and grade -->
                    <div class="" style="min-width: 115px;">
                        M : <span id="subject-marks-text{{$student_id}}{{$subject_id}}">{!! $sub_tot_obtained_marks !!}</span>
                        &nbsp;|&nbsp;
                        G : <span id="subject-grade-text{{$student_id}}{{$subject_id}}"></span>
                        {!! Form::hidden('tot_obtained_subject_marks'.$student_id.'_'.$subject_id,$sub_tot_obtained_marks, ['class' => 'tot_marks subject_lock_unlock'.$subject_id.' st_sub_marks'.$student_id,'id'=>'subject_tot_obt'.$student_id.$subject_id,'rel'=>$subject_id,'min'=>'0','max'=>$tot_sub_max_marks,'readonly'=>true]) !!}
                    </div>
                </td>
                </tr>
                </tbody>
            </table>
        </td>
        @php $tot_stu_sub_max_marks = $tot_stu_sub_max_marks + $tot_sub_max_marks; @endphp
        @endforeach
        <!-- class subject array loop end -->
        <td style="width:100% !important; border: 1px solid #eee !important;">
            <div class="" style="min-width: 100px;">
                <!--student all subjects total obtained marks-->
                {!! Form::hidden('total_obtained_marks_'.$student_id,$total_obtained_marks, ['class' => 'tot_marks total_marks_sum','id' => 'student_tot_obt'.$student_id,'readonly'=>true,'rel'=>$student_id,'min'=>'0','max'=>$tot_stu_sub_max_marks,'required'=>true])!!}
                M : <span id="all-subject-marks-text{{$student_id}}"> {!! $total_obtained_marks !!} </span>
                &nbsp;|&nbsp;
	    {!! Form::hidden('grade'.$student_id,$total_obtained_marks, ['class' => '','id' => 'all-subject-grade-val'.$student_id,'readonly'=>true])!!}
                G : <span id="all-subject-grade{{$student_id}}"></span>
            </div>
        </td>
        <td style="border: 1px solid #eee !important;">
            {!! Form::hidden('percentage'.$student_id,'', ['class' =>'st_percentage'.$student_id,'id'=>'student_per'.$student_id,'rel'=>$student_id]) !!}
            {!! Form::hidden('rank'.$student_id,'', ['class' =>'st_rank'.$student_id,'id'=>'student-rank-val'.$student_id,'rel'=>$student_id,'readonly'=>true]) !!}
            <span id="student-percentage{{$student_id}}"></span></td>
        <td style="border: 1px solid #eee !important;"><span id="student-rank{{$student_id}}"></span></td>
        </tr>
        @php $count++; @endphp 
        @endforeach 
        <!--end student loop-->
        </tbody>
    </table>
</div>
@endif
<script type="text/javascript">
    $(document).ready(function () {
        $('.marks').attr('readonly', true);
        $('.marks').attr('required', false);
        $('.marks').css("background-color", "#efefea");
        $('.marks').css("border", "none");
        $('.marks').attr('tabindex', '-1');
    });
</script>

