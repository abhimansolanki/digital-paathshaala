<style type="text/css">
    .btn-primary{
        margin: 0px !important;
    }
</style>


<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.marks')}}</li>
                    {!! Form::hidden('student_mark_id',isset($student_mark['student_mark_id']) ? $student_mark['student_mark_id'] : null, ['class' => 'gui-input','id' => 'student_mark_id']) !!}
                    {!! Form::hidden('exam_class_id',isset($student_mark['exam_class_id']) ? $student_mark['exam_class_id'] : null, ['class' => 'gui-input','id' => 'exam_class_id']) !!}
                </ol>
            </div>
            <div class="topbar-right">
                <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right" title="View Student">View Marks</a>
            </div>
        </div>
        @if(Session::has( 'success'))
        @include('backend.partials.messages')
        @endif
        <div class="panel-body bg-light">
            <!-- .section-divider -->
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $student_mark['arr_session'],isset($student_mark['session_id']) ? $student_mark['session_id'] : null, ['class' => 'form-control','id'=>'session_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('session_id')) 
                    <p class="help-block">{{ $errors->first('session_id') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{ trans('language.exam')}}<span class="asterisk">*</span></span></label>
                    <label for="exam_id" class="field select">
                        {!!Form::select('exam_id', $student_mark['arr_exam'],isset($student_mark['exam_id']) ? $student_mark['exam_id'] : '', ['class' => 'form-control','id'=>'exam_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('exam_id')) 
                    <p class="help-block">{{ $errors->first('exam_id') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{ trans('language.class')}}<span class="asterisk">*</span></span></label>
                    <label for="class_id" class="field select">
                        {!!Form::select('class_id', $student_mark['arr_class'],isset($student_mark['class_id']) ? $student_mark['class_id'] : '', ['class' => 'form-control','id'=>'class_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('class_id')) 
                    <p class="help-block">{{ $errors->first('class_id') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{ trans('language.section')}}</span></label>
                    <label for="section_id" class="field select">
                        {!!Form::select('section_id', $student_mark['arr_section'],isset($student_mark['section_id']) ? $student_mark['section_id'] : '', ['class' => 'form-control','id'=>'section_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('section_id')) 
                    <p class="help-block">{{ $errors->first('section_id') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.result_in')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('result_in', $student_mark['arr_result_in'],isset($student_mark['result_in']) ? $student_mark['result_in'] : null, ['class' => 'form-control','id'=>'result_in'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('result_in')) 
                    <p class="help-block">{{ $errors->first('result_in') }}</p>
                    @endif
                </div>
                @if(empty($student_mark['student_mark_id']))
                <div class="col-md-3">
                    <label><span class="radioBtnpan"></span></label>
                    <label for="" class="field prepend-icon">
                        {!! Form::button('Continue', ['class' => 'button btn-primary','style'=>'width:100px;margin-top: 6px;','id'=>'get_exam_subjects']) !!}   
                    </label>
                </div>
                @endif
            </div>
            <!-- end .section row section -->
        </div>
        <div id="sbject-data-detail" style="display:none">
            <div class="section-divider mv40">
                <span><i class="fa fa-book"></i>&nbsp;Add Marks</span>
            </div>
            <div class="section " id="spy1">
                <div class="" id="accordion2">
                    <div id="subject-marks-table" class="classopen">
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <div class="col-md-3" style="">
                    <div class="option-group field" id="checkBooxx">
                        <label class="option block option-primary">
		    <b>Note</b> -    &nbsp;G : Grade & M : Marks
                        </label>
                    </div>
                </div>
                <div class="col-md-2" style="">
                    <div class="option-group field" id="checkBooxx">
                        <label class="option block option-primary">
		    @php $publish_status = false; @endphp 
		    @if(!empty($student_mark['publish_status']) && $student_mark['publish_status'] == 1)
		     @php $publish_status = true; @endphp 
		    @endif
                            {!! Form::checkbox('publish_status','',$publish_status, ['class' => 'gui-input ', 'id' => 'publish_status']) !!}
                            <span class="checkbox radioBtnpan"></span><em>Publish Result</em>
                        </label>
                    </div>
                </div>
                {!! Form::button('Calculate Grade', ['class' => 'button btn-primary', 'id' => 'calculate_grade','style'=>'height:32px !important; padding:none']) !!}
                {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
                {!! Form::Reset('Cancel', ['class' => 'button btn-primary']) !!}   
            </div>
        </div>
    </div>
</div>
</div>
@include('backend.partials.popup')
<script type="text/javascript">
    jQuery(document).ready(function () {
        var student_mark_id = $("#student_mark_id").val();
        if (student_mark_id !== '')
        {
            getExamSubject(student_mark_id);
        }
        $("#student-mark-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             * $("#section_id option").length
             ------------------------------------------ */
//            ignore: [],
            rules: {
                class_id: {
                    required: true
                },
                session_id: {
                    required: true
                },
                'section_id': {
                    required: true
                },
                'exam_id': {
                    required: true
                },
                'result_in': {
                    required: true
                },
            },

            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        $(document).on('change', '#class_id,#session_id', function (e) {
            e.preventDefault();
            var class_id = $('#class_id').val();
            var session_id = $('#session_id').val();
            $('#section_id').empty();
            if (class_id !== '' && session_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                        'session_id': session_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $('#section_id').append('<option value="">-- Select section --</option>');
                            $.each(resopose_data, function (key, value) {
                                $('#section_id').append('<option value="' + key + '">' + value + '</option>');
                            });
                            $("#LoadingImage").hide();
                        }
                    }
                });
            }

        });

        $(document).on('click', '#get_exam_subjects', function (e) {
            if ($("#class_id").valid() && $("#exam_id").valid() && $("#session_id").valid() && $("#result_in").valid() && $("#section_id").valid())
            {
                getExamSubject();
            }
        });

        function getExamSubject(student_mark_id = null)
        {

            $("#subject-marks-table").html('');
            $("#sbject-data-detail").hide();
            var form_data = $("#student-mark-form").serializeArray();
            form_data.push({'student_mark_id': student_mark_id});
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-exam-subject-data')}}",
                datatType: 'json',
                type: 'POST',
                data: form_data,
                beforeSend: function () {
//                    $("#LoadingImage").show();
                },
                success: function (response) {
                    $("#LoadingImage").hide();
                    if (response.status === 'success')
                    {
                        $("#subject-marks-table").html(response.data);
                        $("#sbject-data-detail").show();
                        if (student_mark_id !== '')
                        {
                            getMarksGrade();
                        }
                        countMarks();
                    } else if (response.status === 'already_exist')
                    {
                        $('#server-response-message').text('For selected parameters, marks were already added.');
                        $('#alertmsg-student').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    } else
                    {
                        $('#server-response-message').text('No data available.');
                        $('#alertmsg-student').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }

                }
            });
        }
        $(document).on('change', '#class_id,#exam_id', function (e) {
            $("#subject-marks-table").html('');
            $("#sbject-data-detail").hide();
        });

        $(document).on('change', '.marks', function (e) {
            countMarks();

        });

        function countMarks()
        {
            $('.student_row_id').each(function () {
                var student_id = $(this).attr('rel');
                var student_subjects_marks = 0;
                $('.st_sub_marks' + student_id).each(function () {
                    var subject_id = $(this).attr('rel');
                    var subject_marks = 0;
                    $('.subject_' + student_id + '_' + subject_id).each(function () {
                        var subject_exam_type_marks = $(this).val();
                        subject_exam_type_marks = parseInt(subject_exam_type_marks);
                        if (isNaN(subject_exam_type_marks))
                        {
                            subject_exam_type_marks = 0;
                        }
                        if (subject_exam_type_marks > 0)
                        {
                            subject_marks = parseInt(subject_marks) + subject_exam_type_marks;
                        }
                    });
                    $("#subject-marks-text" + student_id + subject_id).text(subject_marks);
                    $("#subject_tot_obt" + student_id + subject_id).val(subject_marks);

                    student_subjects_marks = parseInt(student_subjects_marks) + parseInt(subject_marks);
//                    console.log(student_subjects_marks);
                });
//                console.log(student_subjects_marks);
                $("#all-subject-marks-text" + student_id).text(student_subjects_marks);
                $("#student_tot_obt" + student_id).val(student_subjects_marks);
//                            var stu_obt_mark = student_subjects_marks;
//                             console.log(student_subjects_marks);

                var stu_max_mark = $("#student_tot_obt" + student_id).attr('max');
//                            console.log(stu_max_mark);
                var stu_per = (student_subjects_marks / stu_max_mark) * 100;
//                             console.log(stu_per);
                stu_per = stu_per.toFixed(2);
                $("#student_per" + student_id).val(stu_per);
                $("#student-percentage" + student_id).text(stu_per);
                getRank();

            });
        }
        function getRank()
        {
            var arr_student_per = [];
            $('.student_row_id').each(function () {
                var student_id = $(this).attr('rel');
                var stu_per = $("#student_per" + student_id).val();
                if (stu_per > 0)
                {
                    arr_student_per.push({'id': student_id, 'per': stu_per});
                }
            });
            var sorted_student = arr_student_per.sort(function (a, b) {
                return  b.per - a.per;
            });

            var count = 1;
            $.each(sorted_student, function (key, value) {
                var s = ["th", "st", "nd", "rd"], v = count % 100;
                var rank = v + (s[(v - 20) % 10] || s[v] || s[0]);
                $("#student-rank" + value.id).text(rank);
                $("#student-rank-val" + value.id).val(rank);
                count++;
            });
        }

        $(document).on('click', '.all_subject_lock_unlock', function (e) {
            var subject_id = $(this).attr('rel');
            if ($("#lock-unlock-" + subject_id).hasClass('fa fa-lock'))
            {
                $("#lock-unlock-" + subject_id).removeClass("fa fa-lock");
                $("#lock-unlock-" + subject_id).addClass("fa fa-unlock");

                $('.subject_lock_unlock' + subject_id).each(function () {
                    $(this).attr('readonly', false);
                    $(this).attr('required', true);
                    $(this).css("background-color", "");
                    $(this).attr('tabindex', '');
                    $(this).css("border", "1px solid #DDD");
                });
            } else
            {
                $("#lock-unlock-" + subject_id).removeClass("fa fa-unlock");
                $("#lock-unlock-" + subject_id).addClass("fa fa-lock");
                $('.subject_lock_unlock' + subject_id).each(function () {
                    $(this).attr('readonly', true);
                    $(this).attr('required', false);
                    $(this).css("background-color", "#efefea");
                    $(this).css("border", "none");
                    $(this).attr('tabindex', '-1');
                });
            }
        });
        $(document).on('click', '#calculate_grade', function (e) {
            getMarksGrade();

        });
        function getMarksGrade()
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-marks-grade')}}",
                datatType: 'json',
                type: 'POST',
                data: {marks: ''},
                beforeSend: function () {
                    $("#LoadingImage").show();
                },
                success: function (response) {
                    if (response.status === 'success')
                    {
                        $("#LoadingImage").hide();
                        $('.student_row_id').each(function () {
                            var student_id = $(this).attr('rel');
                            $('.st_sub_marks' + student_id).each(function () {
                                var subject_id = $(this).attr('rel');
                                var stu_sub_mark = $("#subject_tot_obt" + student_id + subject_id).val();
                                var stu_sub_max_mark = $("#subject_tot_obt" + student_id + subject_id).attr('max');
                                var stu_sub_per = Math.ceil((stu_sub_mark / stu_sub_max_mark) * 100);
                                $.each(response.grade, function (key, value) {
                                    var grade_range = value.range.split(",");
                                    if ($.inArray("" + stu_sub_per + "", grade_range) !== -1)
                                    {
                                        $("#subject-grade-text" + student_id + subject_id).text(value.grade_name);

                                    }
                                });
                            });
                            var stu_obt_mark = $("#student_tot_obt" + student_id).val();
                            var stu_max_mark = $("#student_tot_obt" + student_id).attr('max');
                            var stu_per = Math.ceil((stu_obt_mark / stu_max_mark) * 100);
                            $.each(response.grade, function (key, value) {
                                var grade_range = value.range.split(",");
                                if ($.inArray("" + stu_per + "", grade_range) !== -1)
                                {
                                    $("#all-subject-grade" + student_id).text(value.grade_name);
                                    $("#all-subject-grade-val" + student_id).val(value.grade_name);

                                }
                            });
                        });

                    } else
                    {
                        $('#server-response-message').text('Something went wrong');
                        $('#alertmsg-student').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }

                }
            });
        }
    });
</script>