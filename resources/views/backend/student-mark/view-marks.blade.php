@extends('admin_panel/layout')
<style>
    .bootstrap-tagsinput {
        width: 100%;
    }
    .marks {
        height: 25px;
        width: auto;
    }
    .tot_marks {
        height: 25px;
        width: 100%;
    }
    .lock-btn{
        width: 20px;
        height: 25px !important;
        line-height: 0px !important;
        padding: 0px !important;
        background: #fafafa05 !important;
        vertical-align:middle !important;
    }
    .state-error{
        display: block!important;
        margin-top: 5px !important;
        padding: 0;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 9px !important;
        color: #DE888A !important;
    }
    th{
        vertical-align: middle !important;
    }
    input{
        border: 1px solid #ccc !important;
        margin: 5px !important;
    }
    table td{
        border: 0px !important;
    }
    table {
        margin-bottom: 0px !important;
    }
    table tbody {
        font-size: 12px !important;
    }
    tr{
        border: 0px !important;
    }
</style>
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
                    <div class="panel heading-border">
                        <div id="topbar">
                            <div class="topbar-left">
                                <ol class="breadcrumb">
                                    <li class="crumb-active">
                                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                                    </li>
                                    <li class="crumb-trail">{{trans('language.marks')}}</li>
                                </ol>
                            </div>
                            <div class="topbar-right">
                                <a  href="" role="button" class="button btn-primary text-right pull-right ml10 marks-print" title="View Student"><span class="glyphicons glyphicons-print"></span> Print</a>
                                <a  href="" role="button" class="button btn-primary text-right pull-right ml10 marks-excel" title="View Student"><span class="glyphicons glyphicons-file"></span> Excel</a>
                                <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right" title="View Student">View Marks</a>
                            </div>
                        </div>
                        @if(!empty($arr_student))
		@php $arr_grade = get_marks_grade(); @endphp
		<div class="panel-body bg-light">
		    <!-- .section-divider -->
		    <div class="section row">
		        <div class="col-md-3" id="marks_details_year">
			<label>
			    <span class="radioBtnpan">{{trans('language.academic_year')}}</span>
			</label>
			: {!! $arr_student['arr_marks']['mark_session']['session_year'] !!}
		        </div>
		        <div class="col-md-3" id="marks_details_exam">
			<label>
			    <span class="radioBtnpan">{{ trans('language.exam')}}</span>
			</label>
			: {!! $arr_student['arr_marks']['mark_exam']['exam_name'] !!}

		        </div>
		        <div class="col-md-3" id="marks_details_class">
			<label>

			    <span class="radioBtnpan">{{ trans('language.class')}}</span>
			</label>
			: {!! $arr_student['arr_marks']['mark_class']['class_name'] !!}

		        </div>
		        <div class="col-md-3" id="marks_details_section">
			<label>
			    <span class="radioBtnpan">{{ trans('language.section')}} </span>
			</label> 
			: {!! $arr_student['arr_marks']['mark_section']['section_name'] !!}

		        </div>
		    </div>
		    <!-- end .section row section -->
		</div>
		@php $total_max_marks = 0; @endphp
		<div class="" style="overflow-x: scroll; width: 100%">
		    <div id="student_marks">
		        <table class="table table-bordered" style="width:auto;">
			<thead>
			    <tr style="font-size: 12px !important; text-align: center;">
			        <th >S.No.</th>
			        <th >S.R</th>
			        <th ><div class="" style="min-width: 50px;">Roll No</div></th>
			        <th> <div class="" style="min-width: 100px;"> Student Name</div></th>
			        <!--all subject list with its type loop start-->
			        @foreach($arr_student['exam_subject_marks'] as $subject_id => $subject)

			        <!--explode subject type of each subject like written, practical &  oral-->
			        @if(isset($arr_student['subject_marks_criteria'][$subject_id]))
			        @php
			        $arr_type = $arr_student['subject_marks_criteria'][$subject_id];
			        unset($arr_type['total_max_marks']);
			        $arr_subject_type_id = array_keys($arr_type);
			        @endphp
			        @endif
			        @php $col_count = count($subject);
			        @endphp
			        <th  style="text-align:center;">
				{!! $subject['subject_name'] !!} <!--subject name -->
				<table class="table table-bordered" style="width:auto; font-size: 10px !important">
				    <thead>
				        <tr>
					<!--subject type loop for header-->
					@foreach($arr_subject_type_id as  $key =>$subject_type_id)
					<th>
					    <!--get sum of subject max marks-->
					    @php 
					    $subject_type_max_marks = isset($arr_student['subject_marks_criteria'][$subject_id][$subject_type_id]['max_marks']) ? 
					    $arr_student['subject_marks_criteria'][$subject_id][$subject_type_id]['max_marks']:0;
					    @endphp
					    {!! $arr_subject_type[$subject_type_id] !!}
					    <br>
					    {!! $subject_type_max_marks !!}

					</th>
					@endforeach
					<th style="min-width: 80px !important;">Tot.
					    <!--get maximum marks of all subject-->
					    @php 
					    $subject_max_marks = isset($arr_student['subject_marks_criteria'][$subject_id]['total_max_marks']) ? 
					    $arr_student['subject_marks_criteria'][$subject_id]['total_max_marks']:0;

					    $total_max_marks = $total_max_marks+  $subject_max_marks;                                    
					    @endphp
					    <br>
					    {!! $subject_max_marks !!}
					</th>
				        </tr>
				    </thead>                               
				</table>
			        </th>
			        @endforeach
			        <th  style="text-align:center;">
				Total
				<br>
				{!! $total_max_marks !!}
			        </th>
			        <th>%</th>
			    </tr>
			</thead>
			<tbody>
			    @php  $count = 1;@endphp 

			    <!--start student loop-->
			    @foreach($arr_student['arr_marks']['mark_details'] as $key => $student_exam)
			    <!--get subject obtained marks-->
			    @php $student_exam['obtained_marks'] = json_decode($student_exam['obtained_marks'],true);
			    $student_id = $student_exam['student_id']; $tot_stu_sub_max_marks = 0;
			    $total_obtained_marks = isset($student_exam['obtained_marks']['tot_obtained']) ? $student_exam['obtained_marks']['tot_obtained'] : null;
			    $student = $student_exam['mark_student'];
			    @endphp 
			    <tr class="student_row_id" rel='{{$student_id}}'>
			        <td style="border: 1px solid #eee !important;">
				{!! $count !!}
			        </td>
			        <td style="border: 1px solid #eee !important;">
				{!! $student['enrollment_number'] !!}
			        </td>
			        <td style="border: 1px solid #eee !important;">
				{!! $student['get_roll_no']['prefix'].$student['get_roll_no']['roll_number'] !!} 
			        </td>
			        <td style="border: 1px solid #eee !important;">
				{!! $student['first_name'].' '.$student['middle_name'].' '.$student['last_name'] !!}
			        </td>
			        <!-- class subject array loop start -->
			        @foreach($arr_student['exam_subject_marks'] as $subject_id => $subject)

			        @if(isset($arr_student['subject_marks_criteria'][$subject_id]))
			        @php
			        $arr_type = $arr_student['subject_marks_criteria'][$subject_id];
			        unset($arr_type['total_max_marks']);
			        $arr_subject_type_id = array_keys($arr_type);
			        @endphp
			        @endif
			        @php $tot_sub_max_marks = 0; 
			        @endphp

			        @php $sub_tot_obtained_marks = isset($student_exam['obtained_marks'][$subject_id]['sub_tot_obtained']) ? $student_exam['obtained_marks'][$subject_id]['sub_tot_obtained'] : null; @endphp
			        <td  style="text-align:center; border: 1px solid #eee !important;" >
				<table>
				    <tbody>
				        <tr>
					@foreach($arr_subject_type_id as  $key =>$subject_type_id)

					@php $sub_marks = isset($student_exam['obtained_marks'][$subject_id][$subject_type_id]) ? $student_exam['obtained_marks'][$subject_id][$subject_type_id] : null; 
					$unassigned_student = json_decode($subject['unassigned_student_id'],true);
					$unassigned_student = !empty($unassigned_student) ? $unassigned_student :[];
					$subject_max_marks = isset($arr_student['subject_marks_criteria'][$subject_id][$subject_type_id]['max_marks']) ? 
					$arr_student['subject_marks_criteria'][$subject_id][$subject_type_id]['max_marks']:0;

					$subject_min_marks = isset($arr_student['subject_marks_criteria'][$subject_id][$subject_type_id]['min_marks']) ? 
					$arr_student['subject_marks_criteria'][$subject_id][$subject_type_id]['min_marks']:0;

					@endphp
					<td style="text-align:center; border: 1px solid #eee !important;">
					    <!--print subject type like writtrn, practical and oral -->
					    @if(!in_array($student_id,$unassigned_student))

					    @php $tot_sub_max_marks = ($tot_sub_max_marks + $subject_max_marks); @endphp

					    {!! $sub_marks !!}

					    @else
					    @php $tot_sub_max_marks = $tot_sub_max_marks + 0; @endphp
					    NA
					    @endif
					</td>
					@endforeach
					<td style="width:100% !important; border: 1px solid #eee !important;">
					    <!--total marks of subject and grade -->
					    <div class="" style="min-width: 115px;">
					        M : <span>{!! $sub_tot_obtained_marks !!}</span>
					        &nbsp;|&nbsp;
					        @php $obt_grade = ''; @endphp
					        @if($sub_tot_obtained_marks > 0)
					        @php $per = round($sub_tot_obtained_marks*100/$tot_sub_max_marks);
					        @endphp
					        @foreach($arr_grade as $grade) 
					        @if(in_array($per,explode(',',$grade['range'])))
					        @php $obt_grade = $grade['grade_name']; @endphp
					        @endif
					        @endforeach
					        @endif
					        G : <span>{!! $obt_grade !!}</span>
					    </div>
					</td>
				        </tr>
				    </tbody>
				</table>
			        </td>
			        @php $tot_stu_sub_max_marks = $tot_stu_sub_max_marks + $tot_sub_max_marks; @endphp
			        @endforeach
			        <!-- class subject array loop end -->
			        <td style="width:100% !important; border: 1px solid #eee !important;">
				<div class="" style="min-width: 100px;">
				    <!--student all subjects total obtained marks-->
				    M : <span id="all-subject-marks-text{{$student_id}}"> {!! $total_obtained_marks !!} </span>
				    &nbsp;|&nbsp;
				    <!--calculate grade from per-->
				    @php $obt_final_grade = ''; @endphp
				    @if($student_exam['percentage'] > 0)
				    @php 
				    $per = round($student_exam['percentage']);
				    @endphp
				    @foreach($arr_grade as $grade) 
				    @if(in_array($per,explode(',',$grade['range'])))
				    @php $obt_final_grade = $grade['grade_name']; @endphp
				    @endif
				    @endforeach
				    @endif
				    G : <span> {!! $obt_final_grade !!}</span>
				</div>
			        </td>
			        <td style="border: 1px solid #eee !important;">
				{!! $student_exam['percentage'] !!}
			        </td>
			    </tr>
			    @php $count++; @endphp 
			    @endforeach 
			    <!--end student loop-->
			</tbody>
		        </table>
		    </div>
		</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(document).on('click', '.marks-print', function () {

            var divToPrint = document.getElementById("class-sbject-data-table");
            newWin = window.open("");
            newWin.document.write('<html><head><title></title>');
            newWin.document.write('<style> table {  border-collapse: collapse;  border-spacing: 0; width:100%; margin-top:20px;} .table td, .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{ padding:8px 18px;  } .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {     border: 1px solid #e2e2e2;} </style>');
            var details = "&nbsp; " + $('#marks_details_year').html() +
                    "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; " + $('#marks_details_exam').html() +
                    "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; " + $('#marks_details_class').html() +
                    "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; " + $('#marks_details_section').html();
            newWin.document.title = "Student marks sheet";
            newWin.document.write('</head><body>');
            newWin.document.write(details);
            //newWin.document.write($('#marks_details_year').html());     
            //newWin.document.write($('#marks_details_exam').html());     
            //newWin.document.write($('#marks_details_class').html());     
            //newWin.document.write($('#marks_details_section').html());     
            newWin.document.write($('#student_marks').html());
            newWin.document.write('</body></html>');
            newWin.print();
            newWin.close();

        });

        $(document).on('click', '.marks-excel', function () {
            var htmls_data = "";
            var uri = 'data:application/vnd.ms-excel;base64,';
            var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
            var base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            };
            var format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                });
            };
            htmls_data += $('#student_marks').html();
            var ctx = {
                worksheet: 'Student marks sheet',
                table: htmls_data
            }
            var link = document.createElement("a");
            link.download = "view_marks.xls";
            link.href = uri + base64(format(template, ctx));
            link.click();
        });
    })
</script>
@endsection
