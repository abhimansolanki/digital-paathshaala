<style type="text/css">
    .admin-form .select > select{
        height: 35px !important;
    }
    .admin-form .select{
        margin-top: 0px;
    }
</style>

<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail"> {{trans('language.add_session')}}</li>
                </ol>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages')
        @endif
        <div class="bg-light panelBodyy">
            <div class="section-divider mv40">
                <span><i class="fa fa-calendar"></i> Session Detail</span>
            </div>
            <div class="section row" id="spy1">
                {!! Form::hidden('session_id', old('session_id',isset($session->session_id) ? $session->session_id : ''), ['class' => 'gui-input', 'id' => 'session_id','readonly'=>true]) !!}
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.session_start_date')}}<span class="asterisk">*</span></span></label>
                    <label for="start_date" class="field prepend-icon">
                        {!! Form::text('start_date', old('session_name',isset($session->start_date) ? $session->start_date : ''), ['class' => 'gui-input date_picker','id' => 'start_date', 'readonly' => 'readonly']) !!}
                        <label for="start_date" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('start_date')) <p class="help-block">{{ $errors->first('start_date') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.session_end_date')}}<span class="asterisk">*</span></span></label>
                    <label for="end_date" class="field prepend-icon">
                        {!! Form::text('end_date',old('end_date',isset($session->end_date) ? $session->end_date : ''), ['class' => 'gui-input date_picker','id' => 'end_date', 'readonly' => 'readonly']) !!}
                        <label for="end_date" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label> 
                    @if ($errors->has('end_date')) <p class="help-block">{{ $errors->first('end_date') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.session')}}<span class="asterisk">*</span></span></label>
                    <label for="session_year" class="field prepend-icon">
                        {!! Form::text('session_year', old('session_name',isset($session->session_year) ? $session->session_year : ''), ['class' => 'gui-input', 'id' => 'session_year','readonly'=>true]) !!}
                        <label for="session_year" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('session_year')) <p class="help-block">{{ $errors->first('session_year') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.current_session')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_status', $session['arr_current_session'],isset($session->session_status) ? $session->session_status : '', ['class' => 'form-control','id'=>'session_status'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if($errors->has('session_status')) <p class="help-block">{{ $errors->first('session_status') }}</p> @endif 
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-6">
                    <label><span class="radioBtnpan">{{trans('language.session_descrition')}}</span></label>
                    <label for="description" class="field prepend-icon">
                        {{ Form::textarea('description',old('description',isset($session->description) ? $session->description : '') , ['class' => 'gui-textarea','id' => 'description']) }}
                        @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                        <label for="description" class="field-icon">
                            <i class="fa fa-comments"></i>
                        </label>
                    </label>
                </div>
                @if(empty($session->session_id))
                    <div class="col-md-3">
                        <label><span class="radioBtnpan">Syncing {{trans('language.source_session')}}<span class="asterisk">*</span></span></label>
                        <label class="field select">
                            {!!Form::select('source_session_id', $session['arr_existing_sessions'],'', ['class' => 'form-control','id'=>'source_session_id'])!!}
                            <i class="arrow double"></i>
                        </label>
                        @if($errors->has('source_session_id')) <p class="help-block">{{ $errors->first('source_session_id') }}</p> @endif 
                    </div>
                @endif
            </div>

        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::reset('Reset', ['class' => 'button btn-primary', 'id' => 'form-reload']) !!}
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
        <!-- <div class="panel-footer text-left">
            <a class="button btn-primary" href="{{url('admin/session-data-forward')}}">@lang('language.sync_session_data')
            </a> 
        </div> -->
    </div>
    <!-- end .form-footer section -->
</div>
<!--Session view list-->
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_session')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="session-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Session ID</th>
                            <th>{{trans('language.session')}}</th>
                            <th>{{trans('language.session_start_date')}}</th>
                            <th>{{trans('language.session_end_date')}}</th>
                            <th>{{trans('language.current_session_status')}}</th>
                            <th>{{trans('language.action')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $(document).on('click', '#form-reload', function (e) {
            window.location.href = "{{ url('admin/session') }}";
        });
        var table = $('#session-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('session/data')}}",
            columns: [
                {data: 'session_id', name: 'session_id'},
                {data: 'session_year', name: 'session_year'},
                {data: 'start_date', name: 'start_date'},
                {data: 'end_date', name: 'end_date'},
                {data: 'current_session', name: 'current_session'},
                {data: 'action', name: 'action'}

            ]
        });
        $('#start_date,#end_date').change(function () {
            setSessionYear();
        });
        var start_date = $("#start_date").val();
        if (start_date !== '')
        {
            setEndDate(start_date);
        }
        function setSessionYear()
        {
            var date1 = $("#start_date").val();
            var date2 = $("#end_date").val();
            if (date1 !== '' && date2 !== '')
            {
                var new_date1 = date1.split("/");
                var y1 = new_date1[2];
                var new_date2 = date2.split("/");
                var y2 = new_date2[2];
                var session_year = y1 + '-' + y2;
                $("#session_year").val(session_year);
                $("#session_year").valid();
            }
        }
        $("#session-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                session_year: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                session_status: {
                    required: true
                },
            },

            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        $('#start_date').datepicker().on('change', function (ev) {
            var start_date = $(this).val();
            setEndDate(start_date);
            $(this).valid();
        });
        $('#end_date').datepicker().on('change', function (ev) {
            $(this).valid();
        });

    });
    function setEndDate(start_date)
    {
        var arr_date = start_date.split('/');
        var d = new Date(arr_date[2], arr_date[1], arr_date[0]);
        var d1 = new Date(arr_date[2], arr_date[1], arr_date[0]);
        d.setMonth(d.getMonth() + 11);
        d1.setMonth(d1.getMonth() + 12);
        $("#end_date").datepicker('destroy');
        $("#end_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear(),
            maxDate: d1.getDate() + '/' + d1.getMonth() + '/' + d1.getFullYear(),
        });
    }
// check current session status

    // $(document).on('change', '#session_status', function (e) {
    //     if ($(this).val() == 1)
    //     {
    //         $.ajax({
    //             headers: {
    //                 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    //             },
    //             url: "{{url('check-session')}}",
    //             datatType: 'json',
    //             type: 'POST',
    //             data: {
    //                 'session_id': $("#session_id").val()
    //             },
    //             beforeSend: function () {
    //                 $("#LoadingImage").show();
    //             },
    //             success: function (response) {
    //                 $("#LoadingImage").hide();
    //                 if (response.status == 'error')
    //                 {
    //                     // Alert modal for alert msg
    //                     $('#server-response-message').text(response.message);
    //                     $('#alertmsg-student').modal({
    //                         backdrop: 'static',
    //                         keyboard: false
    //                     });
    //                     $('select[name="session_status"]').val(0);
    //                 }

    //             }
    //         });
    //     }
    // });

</script>

