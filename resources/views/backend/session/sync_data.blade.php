    @extends('admin_panel/layout')
    @push('styles')
    <link href="{{ asset('/bower_components/dropify/dist/css/dropify.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/bower_components/AdminLTE/plugins/bootstrap-tags-input/bootstrap-tagsinput.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .bootstrap-tagsinput {
            width: 100%;
        }
    </style>
    @endpush
    @section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                    </div>
                    {!! Form::open(['files'=>TRUE,'id' => 'session-sync-form' , 'class'=>'form-horizontal','url' => $save_url,'autocomplete'=>'off']) !!}
                    <style type="text/css">
                        .admin-form .select > select{
                            height: 35px !important;
                        }
                        .admin-form .select{
                            margin-top: 0px;
                        }
                    </style>

                    <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
                        @include('backend.partials.loader')
                        <div class="panel heading-border">
                            <div id="topbar">
                                <div class="topbar-left">
                                    <ol class="breadcrumb">
                                        <li class="crumb-active">
                                            <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                                        </li>
                                        <li class="crumb-trail"> {{trans('language.add_session')}}</li>
                                    </ol>
                                </div>
                            </div>
                            @include('backend.partials.messages')
                            <div class="bg-light panelBodyy">
                                <div class="section-divider mv40">
                                    <span><i class="fa fa-calendar"></i> Session Sync Details</span>
                                </div>
                                <div class="section row" id="spy1">
                                    <div class="col-md-3">
                                        <label><span class="radioBtnpan">{{trans('language.source_session')}}<span class="asterisk">*</span></span></label>
                                        <label class="field select">
                                            {!!Form::select('source_session', $source_session,'', ['class' => 'form-control','id'=>'source_session'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                        @if($errors->has('source_session')) <p class="help-block">{{ $errors->first('source_session') }}</p> @endif 
                                    </div>
                                    <div class="col-md-3">
                                        <label><span class="radioBtnpan">{{trans('language.target_session')}}<span class="asterisk">*</span></span></label>
                                        <label class="field select">
                                            {!!Form::select('target_session', $target_session,'', ['class' => 'form-control','id'=>'target_session'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                        @if($errors->has('target_session')) <p class="help-block">{{ $errors->first('target_session') }}</p> @endif 
                                    </div>
                                </div>
                            </div>
                            <!-- end .form-body section -->
                            <div class="panel-footer text-right">
                                {!! Form::reset('Reset', ['class' => 'button btn-primary', 'id' => 'form-reload']) !!}
                                {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
                            </div>
                        </div>
                        <!-- end .form-footer section -->
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @endsection
