<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
</style>
<div class="row">
    @php $arr = range(0, 10, 10); @endphp
    @foreach($arr as $key =>$value)
    <div class="col-md-6" style="padding: 5% 0 5% 3%; float: left;">
        <div class="col-md-12" style="padding:10px;">
	<!--<h5 style="text-align: center"><u>Due Fee Reminder</u></h5>-->
	<table>
	    <thead>
	        <tr>
		<td rowspan="3" colspan="4"><img src="{{ get_school_logo() }}"> </td>
		<td colspan="3">MANDORE</td>
	        </tr>
	        <tr>
		<td colspan="7">9414291586</td>
	        </tr>
	        <tr>
		<td colspan="7">Formative assisment 1</td>
	        </tr>
	    </thead>
	    <tbody>
	        <tr>
		<td>Student Name</td>
		<td>Amba</td>
		<td>Roll No</td>
		<td colspan="4">PK07</td>
	        </tr>
	        <tr>
		<td>DOB</td>
		<td>05 Jul 1995</td>
		<td>Class(Sec)</td>
		<td colspan="4">Play Group(A)</td>
	        </tr>
	        <tr>
		<td>Father's Name</td>
		<td>Chaina Ram Patel</td>
		<td>Mother's Name</td>
		<td colspan="4">Divya Patel</td>
	        </tr>
	        <tr>
		<th rowspan="2">Subject</th>
		<th rowspan="2">Max Marks</th>
		<th rowspan="2">Passing Marks</th>
		<th colspan="3">Obt. Marks </th>
		<th rowspan="2">Tot. Marks</th>
	        </tr>
	        <tr>
		<th>O</th>
		<th>W</th>
		<th>P</th>
	        </tr>
	        <tr>
		<td>Hindi</td>
		<td>80</td>
		<td>30</td>
		<td>20</td>
		<td></td>
		<td></td>
		<td>45</td>
	        </tr>
	        <tr>
		<td>English</td>
		<td>80</td>
		<td>30</td>
		<td>20</td>
		<td>10</td>
		<td>10</td>
		<td>45</td>
	        </tr>
	    </tbody>
	</table>
        </div>
    </div>
    @endforeach
</div>
<style>
    @page { size: auto;  margin: 0mm; }
    .pagebreak { page-break-before: always; }
</style>
<script type="text/javascript">
//window.print();
</script>