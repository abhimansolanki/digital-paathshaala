<!-- Field Options -->
<style>
    .state-error{
        display:block !important;
        margin-top: 6px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    }
    #student_id-error{
        margin-top:25px !important;
    }
    #employee_id-error{
        margin-top:25px !important;
    }
</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_notice')}}</li>
                </ol>
            </div>
        </div>

        @include('backend.partials.messages')

        <div class="bg-light panelBodyy">
            {!! Form::hidden('notice_id',old('notice_id',isset($notice['notice_id']) ? $notice['notice_id'] : ''),['class' => 'gui-input', 'id' => 'notice_id', 'readonly' => 'true']) !!}
            <div class="section row" id="spy1">
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.notice_title')}}<span class="asterisk">*</span></span></label>
                    <label for="notice_title" class="field prepend-icon">
                        {!! Form::text('notice_title', old('notice_title',isset($notice['notice_title']) ? $notice['notice_title'] : ''), ['class' => 'gui-input', 'id' => 'notice_title']) !!}
                        <label for="notice_title" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('notice_title')) 
                    <p class="help-block">{{ $errors->first('notice_title') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">Schedule Start Date<span class="asterisk">*</span></span></label>
                    <label for="start_date" class="field prepend-icon">
                        {!! Form::text('start_date', old('start_date',isset($notice['start_date']) ? $notice['start_date'] : ''), ['class' => 'gui-input','id' => 'start_date', 'readonly' => 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('start_date')) 
                    <p class="help-block">{{ $errors->first('start_date') }}</p>
                    @endif
                </div>

                <div class="col-md-2">
                    <label><span class="radioBtnpan">Schedule End Date<span class="asterisk">*</span></span></label>
                    <label for="end_date" class="field prepend-icon">
                        {!! Form::text('end_date', old('end_date',isset($notice['end_date']) ? $notice['end_date'] : ''), ['class' => 'gui-input', 'id' => 'end_date', 'readonly' => 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('end_date')) 
                    <p class="help-block">{{ $errors->first('end_date') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.notice_for')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('notice_for', $notice['arr_notice_for'],isset($notice['notice_for']) ? $notice['notice_for'] : '', ['class' => 'form-control','id'=>'notice_for','style'=>'height:34px !important;'])!!}
                    </label>
                    @if ($errors->has('notice_for')) <p class="help-block">{{ $errors->first('notice_for') }}</p> @endif
                </div>
	    <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.notice_image')}}</span></label>
                    <label>{!!Form::file('notice_image',['accept'=>'image/*'])!!} 
                        @if(isset($notice['notice_image']) && !empty($notice['notice_image']))
                        <a href="" data-toggle="modal" data-target="#img1" data-backdrop="static" data-keyboard="false">Click to view </a>
                        @endif
                    </label>
                </div>
            </div>
            <div class="section row main-div" id="spy1" style="display:none">
                <div id='student'>
                    <div class="col-md-2">
                        <label><span class="radioBtnpan">{{trans('language.class')}}<span class="asterisk">*</span></span></label>
                        <label class="field select">
                            {!!Form::select('class_id', $notice['arr_class'],isset($notice['class_id']) ? $notice['class_id'] : '', ['class' => 'form-control','id'=>'class_id'])!!} 
                        </label>
                        @if ($errors->has('class_id')) 
                        <p class="help-block">{{ $errors->first('class_id') }}</p>
                        @endif 
                    </div>
	        <div class="col-md-2">
                        <label><span class="radioBtnpan">{{trans('language.section')}}</span></label>
                        <label class="field select">
                            {!!Form::select('section_id', $notice['arr_section'],isset($notice['section_id']) ? $notice['section_id'] : '', ['class' => 'form-control','id'=>'section_id'])!!}
                            <i class="arrow double"></i>
                        </label>
                        @if ($errors->has('section_id')) 
                        <p class="help-block">{{ $errors->first('section_id') }}</p>
                        @endif                           

	        </div>
                    <div class="col-md-3">
                        <label class="field select">
                            <label><span class="radioBtnpan">{{trans('language.student')}}</span></label>
                            {!!Form::select('student_id[]',$notice['arr_student'],isset($notice['student_id']) ? $notice['student_id'] : '', ['class' => 'form-control','id'=>'student_id','multiple'=>'multiple'])!!}
                        </label>
                        @if ($errors->has('student_id')) 
                        <p class="help-block">{{ $errors->first('student_id') }}</p>
                        @endif  
                    </div>
                </div>
                <div id='staff'>
                    <div class="col-md-2">
                        <label><span class="radioBtnpan">{{trans('language.department')}}<span class="asterisk">*</span></span></label>
                        <label class="field select">
                            {!!Form::select('department_id', $notice['arr_department'],isset($notice['department_id']) ? $notice['department_id'] : '', ['class' => 'form-control','id'=>'department_id'])!!} 
                        </label>
                        @if ($errors->has('class_id')) 
                        <p class="help-block">{{ $errors->first('class_id') }}</p>
                        @endif 
                    </div>
                    <div class="col-md-3">
                        <label class="field select">
                            <label><span class="radioBtnpan">{{trans('language.staff')}}</span></label>
                            {!!Form::select('employee_id[]',$notice['arr_employee'],isset($notice['employee_id']) ? $notice['employee_id'] : '', ['class' => 'form-control','id'=>'employee_id','multiple'=>'multiple'])!!}
                        </label>
                        @if ($errors->has('employee_id')) 
                        <p class="help-block">{{ $errors->first('employee_id') }}</p>
                        @endif  
                    </div>
                </div>
                <div id='class-wise'>
                    <div class="col-md-2">
                        <label><span class="radioBtnpan">{{trans('language.class')}}<span class="asterisk">*</span></span></label>
                        <label class="field select">
                            {!!Form::select('class_id', $notice['arr_class'],isset($notice['class_id']) ? $notice['class_id'] : '', ['class' => 'form-control','id'=>'class_id'])!!}
                        </label>
                        @if ($errors->has('class_id'))
                            <p class="help-block">{{ $errors->first('class_id') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-8">
                    <label><span class="radioBtnpan">{{trans('language.notice')}}<span class="asterisk">*</span></span></label>
                    <label class="field">
                        {!!Form::textarea('notice',isset($notice['notice']) ? $notice['notice'] : '', ['class' => 'form-control ckeditor','id'=>'notice','rows'=>4])!!}
                        <i class="arrow double"></i>
                    </label> @if ($errors->has('notice'))
                    <p class="help-block">{{ $errors->first('notice') }}</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
</div>
<div id="img1" class="modal fade-scale" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                @if(isset($notice['notice_image']) && !empty($notice['notice_image']))
                <img src="{{url($notice['notice_image'])}}" style="width: 100%">
                @endif
            </div>
        </div>
    </div>
</div>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" title="">{{trans('language.list_notice')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-striped table-hover" id="notice-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.notice_title') !!}</th>
                            <th>{!!trans('language.notice_for') !!}</th>
                            <th>Schedule Start Date</th>
                            <th>Schedule End Date</th>
                            <!--<th>{!!trans('language.notice_image') !!}</th>-->
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        $("#start_date,#end_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: 0,
            maxDate: "{!! $notice['session_end_date'] !!}"
        });

        var session_id = '{!! !empty($notice["session_id"]) ? $notice["session_id"] : null !!}';
        if (session_id !== '')
        {
            var notice_for = $("#notice_for").val();
            if (notice_for == 4 || notice_for == 5 || notice_for == 6)
            {
                $(".main-div").show();
                $("#student").hide();
                $("#staff").hide();
                if (notice_for == 4)
                {
                    $("#student").show();
                } else if (notice_for == 5)
                {
                    $("#staff").show();
                } else if (notice_for == 6)
                {
                    $("#student").show();
                    $("#staff").show();
                }
            }
        }
        $("#notice-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            ignore: [],
            debug: false,
            rules: {
                notice_title: {
                    required: true,
                    lettersonly: true
                },
                notice_for: {
                    required: true
                },
                class_id: {
                    required: function () {
                        if ($("#notice_for").val() == 4 || $("#notice_for").val() == 6) {
                            return true;
                        } else
                        {
                            return false;
                        }
                    }
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                department_id: {
                    required: function () {
                        if ($("#notice_for").val() == 5 || $("#notice_for").val() == 6) {
                            return true;
                        } else
                        {
                            return false;
                        }
                    }
                },
                notice: {
                    required: function () {
                        var messageLength = CKEDITOR.instances['notice'].getData().replace(/<[^>]*>/gi, '').length;
                        if (!messageLength) {
                            return true;
                        } else
                        {
                            return false;
                        }
                    }
                },
//                'student_id[]': {
//                    required: function () {
//                        if ($("#notice_for").val() == 4 || $("#notice_for").val() == 6) {
//                            return true;
//                        } else
//                        {
//                            return false;
//                        }
//                    }
//                },
//                'employee_id[]': {
//                    required: function () {
//                        if ($("#notice_for").val() == 5 || $("#notice_for").val() == 6) {
//                            return true;
//                        } else
//                        {
//                            return false;
//                        }
//                    }
//                },
                notice_image: {
                    extension: true
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || !/[~`!@#$%\^&*()+=\-\_\.\[\]\\';,/{}|\\":<>\?]/.test(value) || /^[a-zA-Z][a-zA-Z0-9\s\.]+$/i.test(value);
        }, "Please enter only alphabets(more than 1)");

        jQuery.validator.addMethod("extension", function (a, b, c) {
            return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
        }, jQuery.validator.format("Only valid image formats are allowed"));

        $('#start_date').datepicker().on('change', function (ev) {
            var max_date = $(this).datepicker("option", "maxDate");
            $("#end_date").datepicker('destroy');
            $("#end_date").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                minDate: $(this).val(),
                maxDate: max_date
            });
            $(this).valid();
        });
        $('#end_date').datepicker().on('change', function (ev) {
            $(this).valid();
        });

        var table = $('#notice-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('notice/data')}}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'notice_title', name: 'notice_title'},
                {data: 'notice_for_name', name: 'notice_for_name'},
                {data: 'start_date', name: 'start_date'},
                {data: 'end_date', name: 'end_date'},
//                {data: 'notice_image', name: 'notice_image'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            var id = $(this).attr("data-id");
            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        $.ajax(
                                {
                                    url: "{{url('notice/delete/')}}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'notice_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status === "success")
                                        {
                                            table.ajax.reload();
                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
        $("#student_id").select2();
        $("#employee_id").select2();
        $(document).on('change', '#class_id,#section_id', function (e) {
	$("#student_id").trigger("change").empty();
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            if (class_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-student-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                        'section_id': section_id,
                    },
                    beforeSend: function () {
                        $('#LoadingImage').show();
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $.each(resopose_data, function (key, value) {
                                $("#student_id").append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                        $('#LoadingImage').hide();
                    }
                });
            }
        });

        $(document).on('change', '#department_id', function (e) {
            $("#employee_id").empty();
            var department_id = $(this).val();
            if (department_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-employee-list')}}",
                    datatType: 'json',
                    type: 'GET',
                    data: {
                        'department_id': department_id,
                    },
                    beforeSend: function () {
                        $('#LoadingImage').show();
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $.each(resopose_data, function (key, value) {
                                $("#employee_id").append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                        $('#LoadingImage').hide();
                    }
                });
            }
        });
        $(document).on('change', '#notice_for', function (e) {
            $(".main-div").hide();
            $("#student").hide();
            $("#staff").hide();
            $("#class-wise").hide();
            var notice_for = $(this).val();
            if (notice_for == 4)
            {
                $(".main-div").show();
                $("#student").show();
            } else if (notice_for == 5)
            {
                $(".main-div").show();
                $("#staff").show();
            } else if (notice_for == 6)
            {
                $(".main-div").show();
                $("#student").show();
                $("#staff").show();
            } else if (notice_for == 7)
            {
                $(".main-div").show();
                $("#class-wise").show();
            }

        });



        $(document).on('change', '#class_id', function (e) {
            $('select[name="section_id"]').empty();
            $('select[name="section_id"]').append('<option value="">--Select section--</option>');
            var class_id = $("#class_id").val();
            var session_id = $("#session_id").val();
            if (class_id !== '' && session_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status == 'success')
                        {
                            $.each(resopose_data, function (key, value) {
                                $('select[name="section_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        } else
                        {
                            return false;
                        }
                    }
                });
            }

        });

    });
</script>