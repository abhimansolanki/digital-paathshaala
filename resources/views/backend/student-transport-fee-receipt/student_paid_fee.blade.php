<style type="text/css">
    .table td {
        text-align: center !important;
    }

    .slt {
        height: 30px;
        padding: 5px;
    }
</style>
@if(isset($fee) && !empty($fee))
    @foreach($fee as $student_key=> $display_fee)
        <div class="student_tables" id="" style="margin-bottom:8px;">
            <div class="text-left" id="dummy-table-class">
                <label class="fullwitst"><b class="radioBtnpan">
                        <i class="fas fa-rupee-sign"></i>{{trans('language.paid_fees')}}</b>
                </label>
                @if(isset($display_fee['transport_fee']['paid']))
                    <div id="student-paid-fee-detail" class="commbdr">
                        <table class="table table-fixed" style="font-size: 14px !important;">
                            <thead>
                            <tr>
                                <th>Receipt</th>
                                <th>{{trans('language.month')}}</th>
                                <th>{{trans('language.fee-amount')}}</th>
                                <th>{{trans('language.cheque_bounce_charges')}}</th>
                                <th>{{trans('language.discount')}}</th>
                                <th>{{trans('language.net_amount')}}</th>
                                <th>{{trans('language.payment_mode')}}</th>
                                <th>{{trans('language.transaction_id_challan_no_cheque')}}</th>
                                <th>{{trans('language.fee_refund_cancellation_cheque_bounce')}}</th>
                                <th>{{trans('language.print_receipt')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $counter = 1;
                            @endphp
                            @foreach($display_fee['transport_fee']['paid'] as  $arr_transport_paid)
                                @if(isset($arr_transport_paid['paid_fee_detail']))
                                    @php $row_span = count($arr_transport_paid['paid_fee_detail']); @endphp
                                    @foreach($arr_transport_paid['paid_fee_detail'] as $pt_key => $transport_paid)
                                        @php $paid_rel = $student_key.'_'.$counter; @endphp
                                        @php $pt_rel = $pt_key +1 ; @endphp
                                        <tr class="student_information">
                                            @if($pt_key == 0)
                                                <td style="width:100px;" rowspan="{{$row_span}}">
                                                    <a href="">R.No. {!! $arr_transport_paid['receipt_number'] !!}</a> / <br> {!! get_formatted_date($arr_transport_paid['receipt_date'], 'display') !!}
                                                </td>
                                            @endif
                                            <td style="width:115px;">{!! $transport_paid['month'] !!}</td>
                                            @if($pt_key == 0)
                                                <td style="width:99px;"
                                                    rowspan="{{count($arr_transport_paid['paid_fee_detail'])}}">{!! $arr_transport_paid['total_paid_amount'] !!}</td>
                                                <td style="width:99px;"
                                                    rowspan="{{count($arr_transport_paid['paid_fee_detail'])}}">{!! ($arr_transport_paid['cheque_bounce_charges_received'] > 0) ? $arr_transport_paid['cheque_bounce_charges_received'] : "0.00" !!}</td>
                                                <td style="width:106px;"
                                                    rowspan="{{count($arr_transport_paid['paid_fee_detail'])}}">{!! ($arr_transport_paid['discount_amount'] > 0 ) ? $arr_transport_paid['discount_amount'] : "0.00"  !!}</td>
                                                <td style="width:106px;"
                                                    rowspan="{{count($arr_transport_paid['paid_fee_detail'])}}">{!! $arr_transport_paid['net_amount'] + $arr_transport_paid['cheque_bounce_charges_received'] !!}</td>
                                                <td style="width:106px;"
                                                    rowspan="{{count($arr_transport_paid['paid_fee_detail'])}}">{{$arr_payment_mode[$arr_transport_paid['payment_mode_id']]}}</td>
                                                <td style="width:106px;"
                                                    rowspan="{{count($arr_transport_paid['paid_fee_detail'])}}">
                                                    @switch($arr_transport_paid['payment_mode_id'])
                                                        @case(1)
                                                        {{$arr_payment_mode[$arr_transport_paid['payment_mode_id']]}}
                                                        @break;
                                                        @case(2)
                                                        No. {{$arr_transport_paid['cheque_number']}}
                                                        <br> @if($arr_transport_paid['cheque_date'] != "")
                                                            ({{ get_formatted_date($arr_transport_paid['cheque_date'], 'display') }}
                                                            ) @else (--/--/--) @endif
                                                        @break
                                                        @case(3)
                                                        @case(4)
                                                        No. {{$arr_transport_paid['transaction_id']}}
                                                        <br> @if($arr_transport_paid['transaction_date'] != "")
                                                            ({{ get_formatted_date($arr_transport_paid['transaction_date'], 'display') }}
                                                            ) @else (--/--/--) @endif
                                                        @break
                                                        @default
                                                        No. -- <br>
                                                    @endswitch
                                                </td>
                                                <td style="width:106px;" class="removePadding"
                                                    rowspan="{{count($arr_transport_paid['paid_fee_detail'])}}">
                                                    @if($arr_transport_paid['transaction_type'] == 1)
                                                        @if($arr_transport_paid['fee_status'] == 1)
                                                            {!! Form::button('Click Here',['class'=>'button btn-primary delete_installment', 'data-toggle' => "modal", 'data-target'=>"#feeRefundModel", 'id'=>'revert_transport_'.$pt_rel, 'rel'=>$pt_rel,'receipt-id'=>$arr_transport_paid['transport_fee_id']]) !!}
                                                            {{--                                                        {!! Form::button('Click Here',['class' =>'delete_installment button btn-primary ','id' => 'remove_' . $paid_rel, 'rel'=>$paid_rel, 'payment-mode' => $arr_transport_paid['payment_mode_id'], 'data-toggle' => "modal", 'data-target'=>"#feeRefundModel", 'receipt-id'=>$arr_transport_paid['student_fee_receipt_id']]) !!}--}}
                                                        @else
                                                            @if($arr_transport_paid['cheque_bounce_date'] != '')
                                                                Cheque-bounce
                                                                ({!! get_formatted_date($arr_transport_paid['cheque_bounce_date'], 'display') !!}
                                                                )
                                                            @elseif($arr_transport_paid['cancel_fee_date'] != '')
                                                                Fees Cancelled
                                                                ({!! get_formatted_date($arr_transport_paid['cancel_fee_date'],'display') !!}
                                                                ) - ({!! $arr_transport_paid['cancel_fee_reason'] !!})
                                                            @else
                                                                No Reason Found
                                                            @endif
                                                        @endif
                                                    @else
                                                        Fee Refunded
                                                    @endif
                                                </td>
                                                <td style="width:135px;"
                                                    rowspan="{{count($arr_transport_paid['paid_fee_detail'])}}">
{{--                                                    {!! Form::button("Click Here",["class"=> "button btn-primary print_receipt ", "onClick" => "parent.location='".route('student_transport_fee.print.receipt',['student_id' => $display_fee['student_id'], 'id' => $arr_transport_paid['transport_fee_id']])."'", "id" => "print_receipt_" . $paid_rel, "rel"=>$paid_rel,"receipt-id"=>$arr_transport_paid['transport_fee_id'], "target" => "_blank"]) !!}--}}
                                                    <a href="{{route('student_transport_fee.print.receipt',['student_id' => $display_fee['student_id'], 'id' => $arr_transport_paid['transport_fee_id']])}}" target="_blank" class="button btn-primary print_receipt" role="button">Click Here</a>
                                                </td>
                                            @endif
                                        </tr>
                                        @php
                                            $counter ++;
                                            $rowspan_row = false;
                                        @endphp
                                    @endforeach
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        <div class="inner_table wrap" style="height:150px;">
                            <table class="table table-striped" style="font-size: 12px !important;"></table>
                        </div>
                    </div>
                @endif
            </div>
            <div class="clearfix"></div>
        </div>
    @endforeach
@endif
<div class="modal fade" id="feeRefundModel" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Fee Refund / Cancellation / Cheque-bounce</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" name="return_fee_form" id="return_fee_form">
                    <input type="hidden" name="student_fee_receipt_id" id="student_fee_receipt_id" value=""/>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row frl">
                                <div class="col-md-3">
                                    <label>Select Reason</label>
                                </div>
                                <div class="col-md-3">
                                    {!!Form::select('return_fee_reason', add_blank_option(array('1' => 'Fee Refund','2' => 'Fee Cancellation','3' => 'Cheque Bounce'),'--Select Reason --'),'', ['class' => 'form-control frm-hrz', 'id'=>'return_fee_reason'])!!}
                                </div>
                            </div>
                            <div class="fee_refund_div" style="display: none;">
                                <div class="row frl">
                                    <div class="col-md-6">
                                        <b>Fee Refund</b>
                                    </div>
                                </div>
                                <div class="row frl">
                                    <div class="col-md-3">
                                        <label>{{trans('language.receipt_date')}}</label>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::text('return_receipt_date', old('return_receipt_date',date('d/m/Y')), ['class' => 'form-control frm-hrz date_picker', 'id' => 'return_receipt_date',"readonly"=>'readonly']) !!}
                                        @if ($errors->has('return_receipt_date'))
                                            <p class="help-block">{{ $errors->first('return_receipt_date') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="row frl">
                                    <div class="col-md-3">
                                        <label>{{trans('language.payment_mode')}}</label>
                                    </div>
                                    <div class="col-md-3">
                                        {!!Form::select('return_payment_mode_id', add_blank_option(array('1' => 'Cash','2' => 'Cheque','3' => 'Online','4' => 'Paytm'),'--Select mode --'),'', ['class' => 'form-control frm-hrz', 'id'=>'return_payment_mode_id'])!!}
                                    </div>
                                    <div class="col-md-3">
                                        <label>{{trans('language.bank')}}</label>
                                    </div>
                                    <div class="col-md-3">
                                        {!!Form::select('return_bank_id', isset($arr_bank) ? $arr_bank : [],'', ['class' => 'form-control frm-hrz','id'=>'return_bank_id'])!!}
                                    </div>
                                </div>
                                <div class="row frl">
                                    <div class="col-md-3">
                                        <label>{{trans('language.transaction_id')}}
                                            / {{trans('language.cheque_number')}}</label>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::text('return_trans_number', old('return_trans_number'), ['class' => 'form-control frm-hrz ', 'id' => 'return_trans_number']) !!}
                                    </div>
                                    <div class="col-md-3">
                                        <label>{{trans('language.transaction_date')}}
                                            / {{trans('language.cheque_date')}}</label>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::text('return_trans_date', old('return_trans_date', date('d/m/Y')), ['class' => 'form-control frm-hrz',''=>trans('language.cheque_date'), 'id' => 'return_trans_date', 'readonly'=>'readonly']) !!}
                                    </div>
                                </div>
                                <div class="row frl">
                                    <div class="col-md-3">
                                        <label>Refund Amount</label>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::number('return_refund_amount', old('return_refund_amount',0), ['class' => 'form-control frm-hrz ','id' => 'return_refund_amount']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="fee_cancellation_div" style="display: none;">
                                <div class="row frl">
                                    <div class="col-md-6">
                                        <b>Cheque Bounce</b>
                                    </div>
                                </div>
                                <div class="row frl">
                                    <div class="col-md-3">
                                        <label>Cheque Bound Date</label>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::text('bounce_cheque_date', old('bounce_cheque_date',date('d/m/Y')), ['class' => 'form-control frm-hrz','id' => 'bounce_cheque_date','readonly'=>true]) !!}
                                    </div>
                                    <div class="col-md-3">
                                        <label>Cheque Bounce Charge</label>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::number('bounce_cheque_charge', old('bounce_cheque_charge', 0), ['class' => 'form-control frm-hrz ','id' => 'bounce_cheque_charge','min'=>0]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="cheque_bounce_div" style="display: none;">
                                <div class="row frl">
                                    <div class="col-md-6">
                                        <b>Fee Cancellation</b>
                                    </div>
                                </div>
                                <div class="row frl">
                                    <div class="col-md-3">
                                        <label>Fee Cancellation Date</label>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::text('fee_cancel_date', old('fee_cancel_date',date('d/m/Y')), ['class' => 'form-control frm-hrz date_picker', 'id' => 'fee_cancel_date','readonly' => true]) !!}
                                    </div>
                                    <div class="col-md-3">
                                        <label>Fee Cancellation Reason</label>
                                    </div>
                                    <div class="col-md-3">
                                        {!!Form::text('fee_cancellation_reason', '',['class' => 'form-control frm-hrz', 'id'=>'fee_cancellation_reason'])!!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary return_fee_form_submit">Submit</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#return_fee_reason').on('change', function () {
        if ($('#return_fee_reason').val() == 1) {
            $('.fee_refund_div').show();
            $('.fee_cancellation_div').hide();
            $('.cheque_bounce_div').hide();
        } else if ($('#return_fee_reason').val() == 3) {
            $('.fee_refund_div').hide();
            $('.fee_cancellation_div').show();
            $('.cheque_bounce_div').hide();
        } else if ($('#return_fee_reason').val() == 2) {
            $('.fee_refund_div').hide();
            $('.fee_cancellation_div').hide();
            $('.cheque_bounce_div').show();
        } else {
            $('.fee_refund_div').hide();
            $('.fee_cancellation_div').hide();
            $('.cheque_bounce_div').hide();
        }
    });
</script>