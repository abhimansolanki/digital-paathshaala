<html>
<head>
    <head>
        <style type="text/css">h1, h2, h3, h4, h5, h6 {
                padding: 0px;
                margin: 0px;
            }

            p {
                padding: 0px;
                margin: 0px;
            }

            .tabletr tr td {
                line-height: 27px;
            }

            .border-left td {
                border-left: 0px !important;
            }

            .pagebreak {
                page-break-before: always;
            }
        </style>
    </head>
</head>
<body>
@php
    $school_data = get_school_data();
    $school_logo = get_school_logo();
@endphp
@foreach($fee as  $display_fee)
    <div style="margin: 0px auto; width: 700px; padding: 15px ">
        <!--student transport fee receipt code-->
        @if(isset($display_fee['transport_fee']['recently_paid_fee']) && !empty($display_fee['transport_fee']['recently_paid_fee']))
            @php $transport_receipt_number = isset($display_fee['transport_fee']['recently_paid_fee']['receipt_number']) ? $display_fee['transport_fee']['recently_paid_fee']['receipt_number'] : null;
    $transport_receipt_date = isset($display_fee['transport_fee']['recently_paid_fee']['receipt_date']) ? $display_fee['transport_fee']['recently_paid_fee']['receipt_date'] : null;
            @endphp
            <div class="border" style="border:1px solid #ccc; padding-top: 10px; ">
                <div style="float: left; width: 30%; padding-left: 10px;">
                    <img src="{{$school_logo}}">
                </div>
                <div style="float: right; width: 60%;  text-align: right; padding-right: 10px;">
                    <h3 style="color:#000; font-size: 18px; border:2px solid #000; float: right; padding:5px 20px ;">
                        TRANSPORT FEE RECEIPT
                    </h3>
                    <div style="padding-top: 10px;"></div>
                    <h3 style="font-size: 15px; line-height: 30px;">Academic
                        Year {!! $display_fee['session_year'] !!}</h3>
                    @if(!empty($school_data))
                        <p>
                            {!! $school_data['address']!!}
                        </p>
                        <p style=""><b>Contact No</b> : {!! $school_data['contact_number']!!}
                            <br>{!! $school_data['mobile_number']!!}</p>
                    @endif
                </div>
                <div style="clear: both;"></div>
                <div style="border-top: 1px solid #ccc; margin-top: 10px; padding:5px 10px; ">
                    <table width="100%" class="tabletr" style=" border-top: 0px; border-bottom: 0px;">
                        <tr>
                            <td width="65%"><b>{!! trans('language.receipt_number') !!}&nbsp;
                                    :</b>&nbsp;{!! $transport_receipt_number !!}</td>
                            <td width="35%"><b>{!! trans('language.receipt_date') !!} &nbsp; :</b>
                                &nbsp;{!! date('d M Y') !!}</td>
                        </tr>
                        <tr>
                            <td><b>{!! trans('language.student_name') !!}&nbsp;
                                    :</b>&nbsp;{!! $display_fee['student_name']!!}</td>
                            <td style=""><b>{!! trans('language.class') !!}&nbsp;
                                    :</b>&nbsp;{!! $display_fee['class_name']!!}</td>
                        </tr>
                        <tr>
                            <td><b>{!! trans('language.father_name') !!}&nbsp; :</b>
                                &nbsp; {!! $display_fee['father_name']!!}</td>
                            <td style=""><b>{!! trans('language.enrollment_number') !!}&nbsp; :</b>
                                {!! $display_fee['enrollment_number']!!}</td>
                        </tr>
                    </table>
                </div>

                <table width="100%" style="border-collapse: collapse;" cellpadding="10">
                    <thead>
                    <tr>
                        <th style="border: 1px solid #000000;text-align: left;"
                            width="60%">{!! trans('language.description') !!}</th>
                        <th style="border: 1px solid #000000;text-align: right;" colspan="2"
                            width="40%">{!! trans('language.amount') !!}</th>
                    </tr>
                    </thead>
                    <thead>
                    <tr>
                        <td style="border: 1px solid #000000;"
                            width="60%">{!! implode(',',array_column($display_fee['transport_fee']['recently_paid_fee']['paid_fee_detail'],'month')) !!}</td>
                        <td style="border: 1px solid #000000;text-align: right;" colspan="2"
                            width="40%">{!! amount_format($display_fee['transport_fee']['recently_paid_fee']['deposit_amount']) !!}</td>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <td style="border: 1px solid #000000;" width="60%">
                        </td>
                        <td style="border: 1px solid #000000;" width="25%">
                            <b style="line-height: 25px;">{!! trans('language.net') !!}:- </b><br/>
                        </td>
                        <td style="border: 1px solid #000000; text-align: right;" width="15%">
                            <span style="line-height: 25px;">{!! amount_format($display_fee['transport_fee']['recently_paid_fee']['deposit_amount']) !!}</span><br/>
                        </td>
                    </tr>
                    </tfoot>
                </table>

                {{--                <table border="1" width="100%" cellspacing="0" cellpadding="10"--}}
                {{--                       style="border-left:0px; border-right: 0px; border-top: 0px; border-bottom: 0px;">--}}
                {{--                    <th colspan="3" style="text-align: left;">{!! trans('language.description') !!}</th>--}}
                {{--                    <th style="text-align: right;">Amount</th>--}}
                {{--                    <tr>--}}
                {{--                        <td colspan="3">{!! implode(',',array_column($display_fee['transport_fee']['recently_paid_fee']['paid_fee_detail'],'month')) !!}</td>--}}
                {{--                        <td style="text-align: right;">{!! amount_format($display_fee['transport_fee']['recently_paid_fee']['deposit_amount']) !!}</td>--}}
                {{--                    </tr>--}}
                {{--                    <tr>--}}
                {{--                        <td colspan="5" style="padding: 0px !important;border:none;">--}}
                {{--                            <table width="100%" border="1" cellspacing="0" cellpadding="20"--}}
                {{--                                   style="border-top: 0px; border-bottom: 0px; border-left:0px; border-right: 0px;">--}}
                {{--                                <tr>--}}
                {{--                                    <td style=" width: 79.7%;">--}}
                {{--                                    </td>--}}
                {{--                                    <td style="width: 30%">--}}
                {{--                                        <b style="line-height: 25px;">{!! trans('language.net') !!}:- </b><br/>--}}
                {{--                                    </td>--}}
                {{--                                    <td style="width: 10%">--}}
                {{--                                        <span style="line-height: 25px;">{!! amount_format($display_fee['transport_fee']['recently_paid_fee']['deposit_amount']) !!}</span><br/>--}}
                {{--                                    </td>--}}
                {{--                                </tr>--}}
                {{--                            </table>--}}
                {{--                        </td>--}}
                {{--                    </tr>--}}
                {{--                </table>--}}
            </div>
            <div style="margin-top: 5px;">
                <div style="border:1px solid #ccc; float: left; width:50%;">
                    <table border="1" cellspacing="0" cellpadding="20" width="100%"
                           style="border-collapse: collapse; border-top: 0px; border-bottom: 0px; border-left: 0px; border-right: 0px;">
                        <tr>
                            <th>Demand Details</th>
                        </tr>
                        <tr>
                            <td style="padding: 0px; border:0px !important;">
                                <div>
                                    <table width="100%" border="1" cellspacing="0" cellpadding="10"
                                           style="text-align: center; border-top: 0px; border-bottom: 0px; border-left: 0px; border-right: 0px;">
                                        <tr>
                                            <th>{!! trans('language.description') !!}</th>
                                            <th style="width:15%">{!! trans('language.amount') !!}</th>
                                        </tr>
                                        @if(isset($display_fee['transport_fee']['pending']))
                                            @foreach($display_fee['transport_fee']['pending'] as $pending_fee)
                                                <tr>
                                                    <td>{!! $pending_fee['month'] !!}</td>
                                                    <td>{!! $pending_fee['pending_amount'] !!}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 0px; border:0px !important;">
                                <table width="100%" border="1" cellspacing="0" cellpadding="10"
                                       style="border-top: 0px; border-bottom: 0px; border-left: 0px; border-right: 0px; float: left;">
                                    <tr>
                                        <td>{!! trans('language.pending_amount') !!}:-</td>
                                        <td>@if(isset($display_fee['transport_fee']['total_pending_amount']))
                                                {!! amount_format($display_fee['transport_fee']['total_pending_amount']) !!}
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="border:1px solid #ccc; float: right; width:49%;">
                    <table border="1" cellspacing="0" cellpadding="20" width="100%"
                           style="border-collapse: collapse; text-align: center; border-top: 0px; border-bottom: 0px; border-left: 0px; border-right: 0px;">
                        <tr>
                            <th>Collection Detail</th>
                        </tr>
                        <tr>
                            <td style="padding: 0px; border:0px !important;">
                            <table width="100%" border="1" cellspacing="0" cellpadding="10"
                                   style="text-align: center; border-top: 0px; border-bottom: 0px; border-left: 0px; border-right: 0px;">
                                <tr>
                                    <th>{!! trans('language.description') !!}</th>
                                    <th>{!! trans('language.receipt_date') !!}</th>
                                    <th style="width:15%">{!! trans('language.amount') !!}</th>
                                </tr>
                                @php $transport_deposit_fee = 0 ; @endphp
                                @if(isset($display_fee['transport_fee']['paid']))
                                    @foreach($display_fee['transport_fee']['paid'] as  $receipt)
                                        @if(isset($receipt['paid_fee_detail']))
                                            @foreach($receipt['paid_fee_detail'] as  $receipt_detail)
                                                <tr>
                                                    <td>{!! $receipt_detail['month'] !!}</td>
                                                    <td>{!! date('d M Y') !!}</td>
                                                    <td>{!! $receipt_detail['paid_fee_amount'] !!}</td>
                                                    @php $transport_deposit_fee = $transport_deposit_fee +  $receipt_detail['paid_fee_amount']; @endphp
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 0px; border:0px !important;">
                                <table width="100%" border="1" cellspacing="0" cellpadding="10"
                                       style="border-top: 0px; border-bottom: 0px; border-left: 0px; border-right: 0px; float: left;">
                                    <tr>
                                        <th>{!! trans('language.deposit_amount') !!}:-</th>
                                        <th></th>
                                        <th>@if(isset($display_fee['transport_fee']['paid']))
                                                {!! amount_format($transport_deposit_fee)!!}
                                            @endif
                                        </th>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="clear: both; margin: 0pt; padding: 0pt; "></div>
            </div>
            <div style="padding: 10px 0px;">
                <p style="line-height: 25px;"> a) If fee and fine are not paid for three month consecutively,the name of
                    student
                    will be struck off the rolls
                </p>
                <p style="line-height: 25px;">
                    b) Receipt must be Obtained against payment of fee
                </p>
                <p style="line-height: 25px;">
                    c) No admission & annual fee will be refunded if pupil's withdrwan from the school in the mid of
                    session
                </p>
            </div>
            <div style="padding: 10px 0px;">
                <b> Note:</b> Late fee.If fee not paid by the due date late fee @Rs.50 per day will be charge
            </div>
            <div style="text-align: left;">
                All Fees once paid shall not be refunded under any circumstance
            </div>
            <div style="text-align: right;">
                <b>
                    Auth. Signature
                </b>
            </div>
        @endif
    </div>
@endforeach
</body>
</html>

