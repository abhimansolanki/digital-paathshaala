<!--transport fee-->
<!--<a id="button" href="#">Transport</a>-->
<div class="student_tables tab-pane" id="transport_fee{{$student_key}}" style="margin-bottom:0px;display: none ">
    <div class="col-md-5 text-left bordernonetable" id="rightBorderClass">
        <label class="fullwitst colorchange"><b class="radioBtnpan" ><i class="fas fa-rupee-sign"></i>{{trans('language.transport_pending_fees')}}</b></label>
        @if(isset($display_fee['transport_fee']['pending']))
            <div class="commbdr" id="student-pending-fee-detail" id="show">
                <table class="table table-striped table-fixed " style="font-size: 14px !important;">
                    <thead>
                    <tr>
                        <th class="text-center">{{trans('language.month')}}</th>
                        <th class="text-center">{{trans('language.table_fee_amount')}}</th>
                        <th class="text-center">{{trans('language.table_paid_amount')}}</th>
                        <th class="text-center">{{trans('language.add')}}</th>
                        <th class="text-center">{{trans('language.remove')}}</th>
                    </tr>
                    </thead>
                </table>
                <div class="inner_table wrap">
                    <table class="table table-striped"  style="font-size: 12px !important;" >
                        <tbody>
                        @php $counter  = 1; @endphp
                        @foreach($display_fee['transport_fee']['pending'] as $t_key=> $transport_pending)
                            @php $transport_pending_amount = $transport_pending['pending_amount'];
                            $t_rel = $student_key.'_'.$counter;;
                            @endphp
                            <tr>
                                <td style="width:80px">
                                    {!! Form::hidden('month_id_'.$student_key.'[]',$transport_pending['month_id'],['class'=>'','id'=>'']) !!}
                                    {!! $transport_pending['month'] !!}
                                </td>
                                <td style="width:80px;">
                                    {!! $transport_pending_amount !!}
                                </td>
                                <td style="width:112px;" class="inputtd">
                                    <div>
                                        {!! Form::text('transport_fee_amount_'.$student_key.'[]','',['class'=>'valid_empty_value transport_pay_amount','id'=>'transport_pay_amount_'.$t_rel,'rel'=>$t_rel,'trans-data-fee-amount'=>$transport_pending_amount]) !!}
                                        {!! Form::hidden('transport_fee_original_'.$student_key.'[]',$transport_pending_amount,['class'=>'amount','id'=>'transport_pay_amount_'.$t_rel]) !!}
                                    </div>
                                </td>
                                <td class="cbtnn" style="padding-left: 20px !important;">
                                    {!! Form::button('<i class="fas fa-plus"></i>',['class' =>'transport_add_amount','id' => 'transport_add_'.$t_rel, 'transport-paid-amount'=>$transport_pending_amount,'rel'=>$t_rel,'data-clicked-class'=>'add_transport_amount']) !!}
                                </td>
                                <td class="cbtnn">
                                    {!! Form::button('<i class="fas fa-minus"></i>',['class' =>'transport_remove_amount btn-primary','id' => 'transport_remove_'.$t_rel,'transport-paid-amount'=>$transport_pending_amount,'rel'=>$t_rel,'data-clicked-class'=>'remove_transport_amount']) !!}
                                </td>
                            </tr>
                            @php  $counter ++; @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="pendingAmountClass " >
                    <div class="col-md-7 paddingrr">
                        {{trans('language.pending_amount')}} : {!! isset($display_fee['transport_fee']['total_pending_amount']) ? amount_format($display_fee['transport_fee']['total_pending_amount']) : 0 !!} </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        @endif
    </div>
    <!--paid fee table-->
    <div class="col-md-7 text-left bordernonetable" id="rightBorderClass1">
        <label class="fullwitst"><b class="radioBtnpan"><i class="fas fa-rupee-sign"></i>{{trans('language.transport_paid_fees')}}</b></label>
        @if(isset($display_fee['transport_fee']['paid']))
            <div id="student-paid-fee-detail" class="commbdr">
                <table class="table table-striped head"  style="font-size: 14px !important;" >
                    <thead>
                    <tr>
                        <th>{{trans('language.table_receipt_date')}}</th>
                        <th>{{trans('language.table_receipt_number')}}</th>
                        <th>{{trans('language.month')}}</th>
                        <th>{{trans('language.table_fee_amount')}}</th>
                        <th>{{trans('language.remove')}}</th>
                    </tr>
                    </thead>
                </table >
                <div class="inner_table wrap">
                    <table class="table table-striped"  style="font-size: 12px !important;" >
                        @foreach($display_fee['transport_fee']['paid'] as $arr_transport_paid)
                            @if(isset($arr_transport_paid['paid_fee_detail']))
                                @php $row_span = count($arr_transport_paid['paid_fee_detail']); @endphp
                                @foreach($arr_transport_paid['paid_fee_detail'] as $pt_key => $transport_paid)
                                    @php
                                        $pt_rel = $pt_key +1 ;
                                    @endphp
                                    <tr class="student_information">
                                        @if($pt_key == 0)
                                            <td style="width:100px;" rowspan="{{$row_span}}">
                                                {!! $arr_transport_paid['receipt_date'] !!}
                                            </td>
                                            <td style="width:80px;" rowspan="{{$row_span}}">
                                                {!! $arr_transport_paid['receipt_number'] !!}
                                            </td>
                                        @endif
                                        <td style="width:150px;">
                                            {!! $transport_paid['month'] !!}
                                        </td>
                                        <td style="width:162px;">
                                            {!! $transport_paid['paid_fee_amount'] !!}
                                        </td>
                                        @if($pt_key == 0)
                                            <td class="removePadding">
                                                {!! Form::button('-',['class'=>'tableforpendingremove revert_transport_installment btn-primary','id'=>'revert_transport_'.$pt_rel, 'rel'=>$pt_rel,'receipt-id'=>$arr_transport_paid['transport_fee_id']]) !!}
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                    </table>
                </div>
                <div class="pendingAmountClass text-center">
                    {{trans('language.deposit_amount')}} : {!! amount_format(array_sum(array_column($display_fee['transport_fee']['paid'],'deposit_amount')))!!}
                </div>
            </div>
        @endif
    </div>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $(".transport_button").click(function () {
            var rel = $(this).attr('rel');
            var img = $("#trans_img" + rel).attr('current-img');
            if (img === 'details_open')
            {
                $("#trans_img" + rel).attr('current-img', 'details_close');
                $("#trans_img" + rel).attr('src', '<?php echo url('public/details_close.png') ?>');
            } else
            {
                $("#trans_img" + rel).attr('current-img', 'details_open');
                $("#trans_img" + rel).attr('src', '<?php echo url('public/details_open.png') ?>');
            }
            $("#transport_fee" + rel).toggle();
        });
    });
</script>