<style type="text/css">
    .inner_table {
        height: 190px;
        overflow-y: auto;
    }

    .inner_table1 {
        height: 189px;
        overflow-y: auto;
    }

    .inputtd input {
        width: 106px !important;
        height: 29px !important;
    }

    .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 5px 9px;
    }

    .fee_installment_paid_amount {
        padding: 0px;
        margin: 4px 0px !important;
    }

    .inputtd input {
        width: 97px !important;
        height: 22px !important;
    }

    .inner_table {
        height: 157px !important;
    }

    .pendingAmountClass {
        border-top: 1px solid #ccc;
        font-size: 11px;
        padding: 7px 10px;
    }

    .inner_table::-webkit-scrollbar {
        width: 7px !important;
    }

    .inner_table::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3) !important;
    }

    .inner_table::-webkit-scrollbar-thumb {
        background-color: darkgrey !important;
        outline: 1px solid slategrey !important;
    }

    .margincommm {
        margin: 0px !important;
    }

    .margincommm input {
        width: 61% !important;
        height: 25px !important;
    }

    .margincommm span {
        font-size: 13px !important;
    }

    .transport_main_button {
        margin-left: 15px !important;
        margin-top: 12px !important;
    }

    .bordernonetable {
        padding-bottom: 5px;
    }
</style>
@if(isset($fee) && !empty($fee))
    @php $total_pending_amount = 0;
    @endphp
    @foreach($fee as $student_key=> $display_fee)
        @php $student_key = $display_fee['student_id']; $each_student_pending = 0; @endphp
        {!! Form:: hidden('student_class_id_'.$student_key,$display_fee['class_id'],['id'=>'student_class_id','readonly'=>true]) !!}
        <div class="student_tables" id="transport_fee{!!$student_key!!}" style="margin-bottom:8px;">
            <label class="fullwitst colorchange">
                <b class="radioBtnpan"><i class="fas fa-rupee-sign"></i>{{trans('language.pending_fees')}}</b>
            </label>
            @if(isset($display_fee['transport_fee']['pending']))
                <div class="commbdr" id="student-pending-fee-detail" id="show">
                    <table class="table table-striped table-fixed " style="font-size: 14px !important;">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 150px">{{trans('language.month')}}</th>
                            <th class="text-center" style="width: 150px">{{trans('language.amount')}}</th>
                            <th class="text-center" style="width: 150px">Total Payable Amount</th>
                            <th class="text-center">Paid Amount</th>
                            <th class="text-center">
                                {{trans('language.action')}}
                                {{ Form::checkbox('transport_fee_all_'.$student_key.'[]','value',false,['class' =>'form-control frm-hrz add_fee_check_all','id' => 'all_transport_fee', 'style' => 'height: 21px !important;width: 27px !important;']) }}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $counter  = 1; @endphp
                        @foreach($display_fee['transport_fee']['pending'] as $t_key=> $transport_pending)
                            @php $transport_pending_amount = $transport_pending['pending_amount'];
                            $t_rel = $student_key.'_'.$counter;;
                            @endphp
                            <tr>
                                <td style="width:80px">
                                    {!! Form::hidden('month_id_'.$student_key.'[]',$transport_pending['month_id'],['class'=>'','id'=>'']) !!}
                                    {!! $transport_pending['month'] !!}
                                </td>
                                <td style="width:80px;">
                                    {!! $transport_pending_amount !!}
                                </td>
                                <td style="width:80px;">
                                    {{$transport_pending_amount}}
                                </td>
                                <td style="width:112px;" class="inputtd">
                                    <div>
                                        <span class="transport_pay_amount_text valid_empty_value_text transport_pay_amount_text{{$student_key}}"
                                              id='transport_pay_amount_text_{{$t_rel}}'></span>

                                        {!! Form::hidden('transport_fee_amount_'.$student_key.'[]','',['class'=>'valid_empty_value transport_pay_amount student_installments_'.$student_key,'id'=>'transport_pay_amount_'.$t_rel,'rel'=>$t_rel,'trans-data-fee-amount'=>$transport_pending_amount]) !!}
                                        {!! Form::hidden('transport_fee_original_'.$student_key.'[]',$transport_pending_amount,['class'=>'amount','id'=>'transport_pay_original_'.$t_rel]) !!}

                                    </div>
                                </td>
                                <td class="cbtnn">
                                    {!! Form::checkbox('transport_add_remove_amount','','', ['class' => 'transport_add_remove_amount form-control frm-hrz transport_add_remove_amount_'.$t_rel, 'id' => 'transport_add_remove_amount', 'style' => 'height: 21px !important;width: 27px !important;', 'title' => "Add Remove Amount", 'transport-paid-amount'=>$transport_pending_amount ,'rel'=>$t_rel]) !!}
{{--                                    {!! Form::button('<i class="fas fa-plus"></i>',['class' =>'transport_add_amount','id' => 'transport_add_'.$t_rel, 'transport-paid-amount'=>$transport_pending_amount,'rel'=>$t_rel,'data-clicked-class'=>'add_transport_amount']) !!}--}}
{{--                                    {!! Form::button('<i class="fas fa-minus"></i>',['class' =>'transport_remove_amount btn-primary','id' => 'transport_remove_'.$t_rel,'transport-paid-amount'=>$transport_pending_amount,'rel'=>$t_rel,'data-clicked-class'=>'remove_transport_amount']) !!}--}}
                                </td>
                            </tr>
                            @php  $counter ++; @endphp
                            @php
                                $total_pending_amount = $total_pending_amount + $transport_pending_amount;
                                $each_student_pending = $each_student_pending + $transport_pending_amount;
                            @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
        {!! Form::hidden('pending_amount_'.$student_key,$each_student_pending, ['class' => 'pending_amount_'.$student_key,'id' => 'student_pending_amount_'. $student_key,'readonly'=>true]) !!}
        {!! Form::hidden('student_assigned_amount_'.$student_key,'', ['class' => 'student_assigned_amount_'.$student_key,'id' => 'student_assigned_amount_'. $student_key,'readonly'=>true]) !!}
    @endforeach
    {!! Form::hidden('total_pending_amount',$total_pending_amount, ['id' => 'total_pending_amount','readonly'=>true]) !!}
@endif


