<div class="admin-panels ">
    <div class="row">
        <!-- Dashboard Menu Tabing -->
        <div class="mainDashboard">
            <a href="{{url('admin/dashboard')}}" class="colmd1 colmd1active  text-center" title="Home">
                <div class="tableCell">
                    <span class="glyphicon glyphicon-home"></span>
                    <h3>Home</h3>
                </div>
            </a>
            <a href="{{url('admin/student-enquire/add')}}" class="colmd1  text-center" title="Front Desk Enquiry">
                <div class="tableCell">
                    <span class="glyphicons glyphicons-pen"></span>
                    <h3>Front Desk Enquiry</h3>
                </div>
            </a>
            <a href="{{url('admin/student/add')}}" class="colmd1  text-center" title="Student Manager">
                <div class="tableCell"> <span class="glyphicon glyphicon-edit"></span>
                    <h3>Student Manager</h3>
                </div>
            </a>
            <a href="{{url('admin/student-fee-receipt-new/add')}}" class="colmd1  text-center" title="">
                <div class="tableCell"> <span class="fa fa-rupee"></span>
                    <h3>Fee Manager</h3>
                </div>
            </a>
            <a href="{{url('admin/employee/add')}}" class="colmd1  text-center" title="">
                <div class="tableCell"> <span class="glyphicons glyphicons-group "></span>
                    <h3>Staff Manager</h3>
                </div>
            </a>
            <a href="{{url('admin/create-exam')}}" class="colmd1  text-center" title="">
                <div class="tableCell"><span class="glyphicons glyphicons-book "></span>
                    <h3>Exam Manager</h3>
                </div>
            </a>
            <a href="{{url('admin/vehicle/add')}}" class="colmd1  text-center" title="">
                <div class="tableCell"><span class="glyphicons glyphicons-bus"></span>
                    <h3>Transport Manager</h3>
                </div>
            </a>
	<a href="{{url('admin/student-enquire/add')}}" class="colmd1  text-center" title="Front Desk Enquiry">
                <div class="tableCell">
                    <span class="glyphicons glyphicons-file"></span>
                    <h3>Report</h3>
                </div>
            </a>
            <a href="#" class="colmd1  text-center" title="TC Manager">
                <div class="tableCell"><span class="glyphicons glyphicons-user_remove"></span>
                    <h3>TC Manager</h3>
                </div>
            </a>
            <a href="#" class="colmd1  text-center" title="App Manager">
                <div class="tableCell"><span class="glyphicons glyphicons-iphone"></span>
                    <h3>App Manager</h3>
                </div>
            </a>
            <a href="#" class="colmd1  text-center" title="">
                <div class="tableCell"><span class="glyphicons glyphicons-calculator"></span>
                    <h3>Account Manager</h3>
                </div>
            </a>
            <a href="{{url('admin/session')}}" class="colmd1  text-center" title="">
                <div class="tableCell"><span class="glyphicon glyphicon-cog"></span>
                    <h3>Configuration</h3>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>

        <script>
            $(function () {
                $('.colmd1').click(function () {
                    $(this).addClass('colmd1active').siblings().removeClass('colmd1active')
                })
            })
        </script>


        <div class="col-md-6 admin-grid">
            <div class="page-heading">
                <div class="media clearfix">
                    <div class="media-left pr30">
                        <a href="#">
                            {!!Html::image(get_school_logo(),'',array('style'=>'100%;height:100px;')) !!} 
                        </a>
                    </div>
                    <div class="media-body va-m">
                        <h2 class="media-heading">School Diary</h2>

                        <div class="media-links">
                            <ul class="list-inline list-unstyled">
                                <li style="width: 400px;">
                                    <a href="#" title="email link">
                                        <span class="fa fa-envelope-square fs35 text-muted"></span> @if (!empty($school_summary['school']['email']))
                                        <h5>{!! $school_summary['school']['email']; !!}</h5>
                                        @else
                                        <h5> - </h5>
                                        @endif

                                    </a>
                                </li>
                                <li style="width: 400px;">
                                    <a href="#" title="facebook link">
                                        <span class="fa fa-globe fs35 text-primary"></span> @if (!empty($school_summary['school']['website_url']))
                                        <h5>{!! $school_summary['school']['website_url']; !!}</h5>
                                        @else
                                        <h5> - </h5>
                                        @endif

                                    </a>
                                </li>
                                <li class="" style="width: 400px;">
                                    <a href="#" title="phone link">
                                        <span class="fa fa-phone-square fs35 text-system"></span> @if (!empty($school_summary['school']['contact_number']) || !empty($school_summary['school']['mobile_number'])) {!! $school_summary['school']['contact_number']
                                        !!}, {!! $school_summary['school']['mobile_number'] !!} @else
                                        <h5> - </h5>
                                        @endif
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <!--  Fee Management -->
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-icon">
                        <i class="fa fa-rupee"></i>
                    </span>
                    <span class="panel-title">{!! trans('language.fee_management') !!}</span>
                </div>
                <div class="panel-body pn">
                    <table class="table mbn tc-icon-1 tc-med-2 tc-bold-last">
                        <thead>
                            <tr class="hidden">
                                <th class="mw30"></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="fa fa-rupee text-primary"></span>
                                </td>
                                <td>Total Fee Collection</td>
                                <td>
                                    855,913
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="fa fa-rupee text-primary"></span>
                                </td>
                                <td>Total Expense</td>
                                <td>
                                    349,712
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="fa fa-rupee text-primary"></span>
                                </td>
                                <td>Today Net Collection</td>
                                <td>
                                    1,259,742
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="fa fa-rupee text-primary"></span>
                                </td>
                                <td>Total Due</td>
                                <td>
                                    1,259,742
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- otal Student - 1000 -->
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-icon">
                        <i class="fa fa-users"></i>
                    </span>
                    <span class="panel-title">
                        {{trans('language.total_students')}} - 
                        @if(!empty($school_summary['student']['total_students']))
                        {!! $school_summary['student']['total_students'] !!}
                        @else
                        0 
                        @endif
                    </span>
                </div>
                <div class="panel-body pn">
                    <table class="table mbn tc-icon-1 tc-med-2 tc-bold-last">
                        <thead>
                            <tr class="">
                                <th>
                                    {!! trans('language.boys')!!} - @if(!empty($school_summary['student']['total_boys'])) {!! $school_summary['student']['total_boys'] !!} @else 0 @endif
                                </th>
                                <th class="text-right">
                                    {!! trans('language.girls') !!} - @if(!empty($school_summary['student']['total_girls'])) {!! $school_summary['student']['total_girls'] !!} @else 0 @endif
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-left">
                                    {!! trans('language.present') !!} - @if(!empty($school_summary['student']['present_students'])) {!! $school_summary['student']['present_students'] !!} @else 0 @endif
                                </td>
                                <td class="text-right">
                                    {!! trans('language.absent') !!} - @if(!empty($school_summary['student']['absent_students'])) {!! $school_summary['student']['absent_students'] !!} @else 0 @endif
                                    <br>
                                    <span><a href="{{url('admin/student-absence-summary')}}">View attendance</a></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-icon">
                        <i class="fa fa-users"></i>
                    </span>
                    <span class="panel-title">Teaching Staff</span>
                </div>
                <div class="panel-body pn">
                    <table class="table mbn tc-icon-1 tc-med-2 tc-bold-last">
                        <thead>
                            <tr class="">
                                <th>{!! trans('language.total_teachers') !!}</th>
                                <th class="text-right">
                                    @if(!empty($school_summary['employee']['total_teachers'])) {!! $school_summary['employee']['total_teachers'] !!} &nbsp; &nbsp;&nbsp; {!! trans('language.male') !!} - {!! $school_summary['employee']['total_teacher_male'] !!} | {!! trans('language.female')
                                    !!} - {!! $school_summary['employee']['total_teacher_female'] !!} @else 0 @endif
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-left">
                                    {!! trans('language.present') !!} - @if(!empty($school_summary['employee']['total_present_teachers'])) {!! $school_summary['employee']['total_present_teachers'] !!} @else 0 @endif
                                </td>
                                <td class="text-right">
                                    {!! trans('language.absent') !!} - @if(!empty($school_summary['employee']['total_absent_teachers'])) {!! $school_summary['employee']['total_absent_teachers'] !!} @else 0 @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-icon">
                        <i class="fa fa-users"></i>
                    </span>
                    <span class="panel-title">Non-Teaching Staff</span>
                </div>
                <div class="panel-body pn">
                    <table class="table mbn tc-icon-1 tc-med-2 tc-bold-last">
                        <thead>
                            <tr class="">
                                <th>Non-Teaching Staff</th>
                                <th class="text-right">
                                    @if(!empty($school_summary['employee']['total_non_teaching_statff'])) {!! $school_summary['employee']['total_non_teaching_statff'] !!} &nbsp; &nbsp;&nbsp; {!! trans('language.male') !!} - {!! $school_summary['employee']['total_non_teaching_male'] !!}
                                    | {!! trans('language.female') !!} - {!! $school_summary['employee']['total_non_teaching_female'] !!} @else 0 @endif

                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-left">
                                    {!! trans('language.present') !!} - @if(!empty($school_summary['employee']['total_present_non_teaching_staff'])) {!! $school_summary['employee']['total_present_non_teaching_staff'] !!} @else 0 @endif
                                </td>
                                <td class="text-right">
                                    {!! trans('language.absent') !!} - @if(!empty($school_summary['employee']['total_absent_non_teaching_staff'])) {!! $school_summary['employee']['total_absent_non_teaching_staff'] !!} @else 0 @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6 admin-grid">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-icon">
                      <!-- <i class="fa fa-users"></i> -->
                        <i class="fa fa-graduation-cap"></i>
                    </span>
                    <span class="panel-title">{!! trans('language.school_manager') !!}</span>
                </div>
                <div class="panel-body  heightYesr">
                    <h4 class="">{!! trans('language.academic_year') !!}</h4>
                    <div class="section">
                        @include('backend.header.session')
                    </div>
                </div>
            </div>

            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-icon">
                        <i class="fa fa-book"></i>
                    </span>
                    <span class="panel-title">Complaints Box</span>
                </div>
                <div class="panel-body pn">
                    <table class="table mbn tc-icon-1 tc-med-2 tc-bold-last">

                        <tbody>
                            <tr>
                                <td class="text-left">
                                    <p class="ComplaintsClasss">
                                        @if(count($school_summary['arr_complaint_pending']))
                                    <ul>
                                        @foreach($school_summary['arr_complaint_pending'] as $complaint)
                                        <a href="{{url('admin/complaint-detail/'.get_encrypted_value($complaint['complaint_id'],true))}}"><li>{!! $complaint['heading'] !!}</li></a>
                                        @endforeach
                                    </ul>
                                    @endif
                                    @if($school_summary['arr_complaint'] > 0)
                                    <a href="{{url('admin/complaint')}}" style="margin-left: 5%;">View all</a>
                                    @else
                                    <span style="text-align:center"> No complaint(s)</span>
                                    @endif
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tray tray-center" id="calendreID">
                <div id='calendar' class="admin-theme"></div>
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="panel" style="margin: 0px 11px;">
            <div class="panel-heading">
                <span class="panel-icon">
                    <i class="glyphicons glyphicons-clock"></i>
                </span>
                <span class="panel-title">{!! trans('language.time_table') !!}</span>
            </div>
            <div class="">
                <table class="table mbn tc-icon-1 tc-med-2 tc-bold-last panel-heading" id="timetableStudent">

                    <tr class="">

                        <td align="center"></td>

                        <td>8:30-9:00</td>

                        <td>9:00-09:30</td>

                        <td>09:30-10:00</td>

                        <td>10:00- 10:30</td>

                        <td>10:30-11:00</td>

                        <td>11:00-11:30</td>

                        <td>11:30-11:30</td>

                        <td>11:30- 12:30</td>

                        <td>1:00 - 2:00</td>

                        <td>2:00 - 3:00</td>

                        <td>3:00 - 4:00</td>
                    </tr>
                    <tr>

                        <td align="center">Mon</td>

                        <td align="center">
                            <font color="pink">SUB1<br></font>
                        </td>

                        <td align="center">
                            <font color="orange">SUB2<br></font>
                        </td>

                        <td align="center">
                            <font color="brown">SUB3<br></font>
                        </td>

                        <td align="center">
                            <font color="orange">SUB4<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB5<br></font>
                        </td>

                        <td rowspan="6" align="center" class="rowspan">L<br>U<br>N<br>C<br>H</td>

                        <td align="center">
                            <font color="maroon">SUB6<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB7<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB8<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB9<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB10<br></font>
                        </td>
                    </tr>
                    <tr>

                        <td align="center">Tue</td>

                        <td align="center">
                            <font color="pink">SUB1<br></font>
                        </td>

                        <td align="center">
                            <font color="orange">SUB2<br></font>
                        </td>

                        <td align="center">
                            <font color="brown">SUB3<br></font>
                        </td>

                        <td align="center">
                            <font color="orange">SUB4<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB5<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB6<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB7<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB8<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB9<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB10<br></font>
                        </td>
                    </tr>
                    <tr>

                        <td align="center">Wed</td>

                        <td align="center">
                            <font color="pink">SUB1<br></font>
                        </td>

                        <td align="center">
                            <font color="orange">SUB2<br></font>
                        </td>

                        <td align="center">
                            <font color="brown">SUB3<br></font>
                        </td>

                        <td align="center">
                            <font color="orange">SUB4<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB5<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB6<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB7<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB8<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB9<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB10<br></font>
                        </td>
                    </tr>
                    <tr>

                        <td align="center">Thu</td>

                        <td align="center">
                            <font color="pink">SUB1<br></font>
                        </td>

                        <td align="center">
                            <font color="orange">SUB2<br></font>
                        </td>

                        <td align="center">
                            <font color="brown">SUB3<br></font>
                        </td>

                        <td align="center">
                            <font color="orange">SUB4<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB5<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB6<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB7<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB8<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB9<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB10<br></font>
                        </td>
                    </tr>
                    <tr>

                        <td align="center">Fri</td>

                        <td align="center">
                            <font color="pink">SUB1<br></font>
                        </td>

                        <td align="center">
                            <font color="orange">SUB2<br></font>
                        </td>

                        <td align="center">
                            <font color="brown">SUB3<br></font>
                        </td>

                        <td align="center">
                            <font color="orange">SUB4<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB5<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB6<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB7<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB8<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB9<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB10<br></font>
                        </td>
                    </tr>
                    <tr>

                        <td align="center">Sat</td>

                        <td align="center">
                            <font color="pink">SUB1<br></font>
                        </td>

                        <td align="center">
                            <font color="orange">SUB2<br></font>
                        </td>

                        <td align="center">
                            <font color="brown">SUB3<br></font>
                        </td>

                        <td align="center">
                            <font color="orange">SUB4<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB5<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB6<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB7<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB8<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB9<br></font>
                        </td>

                        <td align="center">
                            <font color="maroon">SUB10<br></font>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <!-- end: .col-md-12.admin-grid -->

</div>