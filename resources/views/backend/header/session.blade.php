<?php
    $updated_session    = get_current_session();
    $updated_session_id = !empty($updated_session) ? $updated_session['session_id'] : null;
?>
<label class="field select">
    {!!Form::select('session_id',add_blank_option(get_session('yes'),'-- Select Session --'),isset($updated_session_id) ? $updated_session_id : null, ['class' => 'form-control','id'=>'set_dashboard_session'])!!}
    <i class="arrow"></i>
</label>
<script>
    $(document).ready(function () {
        $("#set_dashboard_session").on('change', function (e)
        {
            var session_id = $(this).val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('set-session')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'session_id': session_id,
                },
                success: function (response) {
                    $('#server-response-success').text(response.message);
                    $('#alertmsg-success').modal({
                        backdrop: 'static',
                        keyboard: false
                    });


                }
            });
        });
        $('#alertmsg-success').on('hide.bs.modal', function (e) {
            location.reload();
        });
    });
</script>
