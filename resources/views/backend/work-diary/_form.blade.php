    <style>
        #accordionpanel1 {
            padding: 0px !important;
            border-radius: 0px;
        }

        .accordionpanel {
            background: #f9f9f9 !important;
            width: 100%;
            display: block;
            padding: 10px 8px;
            font-weight: bold;
            text-transform: capitalize;
            font-size: 12px;
        }

        .accordionpanel label {
            color: #000;
        }

        .classopen {
            display: none;
        }

        table tr td {
            text-transform: capitalize;
        }

    </style>
    <div class="admin-form theme-primary mw1000 center-block">
        @include('backend.partials.loader')
        <div class="panel heading-border">
            <div id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                        </li>
                        <li class="crumb-trail">{{trans('language.add_work_diary')}}</li>
                    </ol>
                </div>
                <div class="topbar-right">
                    <a href="{{$redirect_url}}" class="button btn-primary text-right pull-right">{{trans('language.view_work_diary')}}</a>
                </div>
            </div>

            @include('backend.partials.messages')
            
            <div class="bg-light panelBodyy" style="margin-bottom: 5%;">
                <div class="section-divider mv40">
                    <span><i class="fa fa-graduation-cap"></i>&nbsp;Work Diary</span>
                </div>
                {!! Form::hidden('work_diary_id',old('work_diary_id',isset($work_diary['work_diary_id']) ? $work_diary['work_diary_id'] : ''),['class' => 'gui-input', 'id' => 'work_diary_id', 'readonly' => 'true']) !!}

                {!! Form::hidden('session_id',old('session_id',isset($session_id) ? $session_id : ''),['class' => 'gui-input', 'id' => 'session_id']) !!}
                
                <div class="section row" id="spy1">
                   <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.class')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('class_id', $work_diary['arr_class'],isset($work_diary['class_id']) ? $work_diary['class_id'] : null, ['class' => 'form-control','id'=>'class_id'])!!}
                        <i class="arrow double"></i>
                    </label> @if ($errors->has('class_id'))
                    <p class="help-block">{{ $errors->first('class_id') }}</p>
                    @endif
                </div>
                
                <div class="col-md-3">
                   <label><span class="radioBtnpan">{{trans('language.section')}}<span class="asterisk">*</span></span></label>
                   <label class="field select">
                      {!!Form::select('section_id', $work_diary['arr_section'],isset($work_diary['section_id']) ? $work_diary['section_id'] : '', ['class' => 'form-control','id'=>'section_id'])!!}
                      <i class="arrow double"></i>
                  </label>
                  @if ($errors->has('section_id')) 
                  <p class="help-block">{{ $errors->first('section_id') }}</p>
                  @endif                           

              </div>
              <div class="col-md-3">
                <label class="field select">
                    <label><span class="radioBtnpan">{{trans('language.student')}}</span></label>
                    {!!Form::select('student_id',$work_diary['arr_student'],isset($work_diary['student_id']) ? $work_diary['student_id'] : '', ['class' => 'form-control','id'=>'student_id'])!!}
                </label>
                @if ($errors->has('student_id')) 
                <p class="help-block">{{ $errors->first('student_id') }}</p>
                @endif  
            </div>
            <div class="col-md-3">
                <label><span class="radioBtnpan">{{trans('language.subject')}}<span class="asterisk">*</span></span></label>
                <label class="field select">
                    {!!Form::select('subject_id', $work_diary['arr_class_subject'],isset($work_diary['subject_id']) ? $work_diary['subject_id'] : '', ['class' => 'form-control','id'=>'subject_id'])!!}
                    <i class="arrow double"></i>
                </label> @if ($errors->has('subject_id'))
                <p class="help-block">{{ $errors->first('subject_id') }}</p>
                @endif
            </div>
        </div>
        <div class="section row subject_content" id="spy1">
            <div class="col-md-3">
                <label><span class="radioBtnpan">{{trans('language.work_type')}}<span class="asterisk">*</span></span></label>
                <label class="field select">
                    {!!Form::select('work_type', $work_diary['arr_work_type'],isset($work_diary['work_type']) ? $work_diary['work_type'] : null, ['class' => 'form-control','id'=>'work_type'])!!}
                    <i class="arrow double"></i>
                </label> @if ($errors->has('work_type'))
                <p class="help-block">{{ $errors->first('work_type') }}</p>
                @endif
            </div>
            <div class="col-md-3">
                <label for="work_start_date"><span class="radioBtnpan">{{trans('language.work_start_date')}}<span class="asterisk">*</span></span></label>
                <label for="work_start_date" class="field prepend-icon">
                    {!! Form::text('work_start_date', old('work_start_date',isset($work_diary['work_start_date']) ? $work_diary['work_start_date'] : ''), ['class' => 'gui-input','id' => 'work_start_date']) !!}
                    <label for="" class="field-icon">
                        <i class="fa fa-eye"></i>
                    </label>
                </label>
                @if ($errors->has('work_start_date')) <p class="help-block">{{ $errors->first('work_start_date') }}</p> @endif
            </div>
            <div class="col-md-3">
                <label><span class="radioBtnpan">{{trans('language.work_end_date')}}<span class="asterisk">*</span></span></label>
                <label for="work_end_date" class="field prepend-icon">
                    {!! Form::text('work_end_date', old('work_end_date',isset($work_diary['work_end_date']) ? $work_diary['work_end_date'] : ''), ['class' => 'gui-input', 'id' => 'work_end_date', 'readonly' => 'readonly']) !!}
                    <label for="" class="field-icon">
                        <i class="fa fa-calendar-o"></i>
                    </label>
                </label>
                @if ($errors->has('work_start_date')) <p class="help-block">{{ $errors->first('work_start_date') }}</p> @endif
            </div>
            <!-- <div class="col-md-3">
                <span class="radioBtnpan">{{trans('language.work_doc')}}
                    @if(isset($work_diary['work_doc']) && !empty($work_diary['work_doc']))
                    <a href="" data-toggle="modal" data-target="#img1" data-backdrop="static" data-keyboard="false">Click to view </a>
                    @endif
                </span>
                {!!Form::file('work_doc',['accept'=>'image/*'])!!}
                @if(isset($work['work_doc']))
                @endif
            </div>
            <div id="img1" class="modal fade-scale" role="dialog">
               <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                          @if(isset($work_diary['work_doc']) && !empty($work_diary['work_doc']))
                          <img src="{{url($work_diary['work_doc'])}}" style="width: 100%">
                          @endif
                      </div>
                    </div>
                </div>
            </div> -->
      </div>
      <div class="row mt-5">
        <div class="col-md-12">
            <div class="form-group row">
                <label for="staticEmail" class="col-md-2 col-form-label">
                    {{trans('language.work_doc_image')}} 
                </label>
                <div class="col-md-1">
                    <input type="checkbox" class="form-check-input" id="check_for_doc" name="check_for_doc" {{ !empty($work_diary['work_doc_file']['title']) ? 'checked="checked"' : '' }} value="1">
                </div>
                <div class="col-sm-3">
                    <label>
                        <span class="radioBtnpan">{{trans('language.title')}}
                        </span>
                    </label>
                    <label for="work_doc_video" class="field prepend-icon">
                        {!! Form::text('work_doc_image_title', old('work_doc_image_title',isset($work_diary['work_doc_file']['title']) ? $work_diary['work_doc_file']['title'] : ''), ['class' => 'form-control', 'id' => 'work_doc_image_title','readonly' => (isset($work_diary['work_doc_file']['doc']) && !empty($work_diary['work_doc_file']['doc'])) ? false : true]) !!}
                    </label>
              </div>
              <div class="col-md-3">
                <span class="radioBtnpan">{{trans('language.file_types')}}
                    @if(isset($work_diary['work_doc_file']) && !empty($work_diary['work_doc_file']))
                    <a href="" data-toggle="modal" data-target="#view_doc_1" data-backdrop="static" data-keyboard="false">Click to view </a>
                    @endif
                </span>
                {!!Form::file('work_doc_image',["accept" => "image/*, application/pdf"])!!}
                <div id="view_doc_1" class="modal fade-scale" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                          <div class="modal-body">
                              @if(isset($work_diary['work_doc_file']) && !empty($work_diary['work_doc_file']))
                              <embed src="{{url($work_diary['work_doc_file']['doc'])}}" width="500px" height="500px" />
                              <!-- <img src="{{url($work_diary['work_doc_file']['doc'])}}" style="width: 100%"> -->
                              @endif
                          </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-md-3">
                <input type="checkbox" class="form-check-input" id="download_permission" name="download_permission" {{ (isset($work_diary['download_permission']) && $work_diary['download_permission'] == 1) ? 'checked="checked"' : '' }}>
                <label class="form-check-label" for="download_permission">
                    {{trans('language.download_permission')}}
                </label>
            </div>
          </div>
      </div>    
{{--      <div class="col-md-12">--}}
{{--        <div class="form-group row">--}}
{{--            <label for="staticEmail" class="col-sm-2 col-form-label">--}}
{{--                {{trans('language.work_doc_video')}}--}}
{{--            </label>--}}
{{--            <div class="col-md-1">--}}
{{--                <input type="radio" class="form-check-input" id="check_for_video" name="check_for_video" {{ !empty($work_diary['work_doc_video']) ? 'checked="checked"' : '' }} value="1">--}}
{{--            </div>--}}
{{--            <div class="col-md-3">--}}
{{--                <label>--}}
{{--                    <span class="radioBtnpan">{{trans('language.title')}}--}}
{{--                    </span>--}}
{{--                </label>--}}
{{--                <label for="work_doc_video_title" class="field prepend-icon">--}}
{{--                    {!! Form::text('work_doc_video_title', old('work_doc_video_title',isset($work_diary['work_doc_video']['title']) ? $work_diary['work_doc_video']['title'] : ''), ['class' => 'form-control', 'id' => 'work_doc_video_title','readonly' => isset($work_diary['work_doc_video']) ? false : true]) !!}--}}
{{--                </label>--}}
{{--            </div>--}}
{{--            <div class="col-md-3">--}}
{{--                <span class="radioBtnpan">{{trans('language.file')}}--}}
{{--                    @if(isset($work_diary['work_doc_video']) && !empty($work_diary['work_doc_video']['doc']))--}}
{{--                    <a href="" data-toggle="modal" data-target="#view_doc_2" data-backdrop="static" data-keyboard="false">Click to view </a>--}}
{{--                    @endif--}}
{{--                </span>--}}
{{--                {!!Form::file('work_doc_video',['accept' => "video/*"])!!}--}}
{{--                <div id="view_doc_2" class="modal fade-scale" role="dialog">--}}
{{--                    <div class="modal-dialog">--}}
{{--                      <div class="modal-content">--}}
{{--                          <div class="modal-header">--}}
{{--                              <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                          </div>--}}
{{--                          <div class="modal-body">--}}
{{--                              @if(isset($work_diary['work_doc_video']) && !empty($work_diary['work_doc_video']['doc']))--}}
{{--                                <!-- <iframe width="420" height="315"--}}
{{--                                    src="{{url($work_diary['work_doc_video']['doc'])}}">--}}
{{--                                </iframe> -->--}}
{{--                                <video width="320" height="240" controls="true">--}}
{{--                                  <source src="{{url($work_diary['work_doc_video']['doc'])}}" type="video/mp4">--}}
{{--                                  <source src="{{url($work_diary['work_doc_video']['doc'])}}" type="video/ogg">--}}
{{--                                  Your browser does not support the video tag.--}}
{{--                                </video>--}}
{{--                              <!-- <img src="{{url($work_diary['work_doc_video']['doc'])}}" style="width: 100%"> -->--}}
{{--                              @endif--}}
{{--                          </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="col-md-12">
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">
                {{trans('language.work_doc_link')}}
            </label>
            <div class="col-md-1">
                <input type="radio" class="form-check-input" id="check_for_video" name="check_for_video" {{ !empty($work_diary['work_doc_link']) ? 'checked="checked"' : '' }} value="2">
            </div>
            <div class="col-sm-3">
                <label>
                    <span class="radioBtnpan">{{trans('language.title')}}
                    </span>
                </label>
                <label for="work_doc_link_title" class="field prepend-icon">
                    {!! Form::text('work_doc_link_title', old('work_doc_link_title',isset($work_diary['work_doc_link']['title']) ? $work_diary['work_doc_link']['title'] : ''), ['class' => 'form-control', 'id' => 'work_doc_link_title','readonly' => isset($work_diary['work_doc_link']) ? false : true]) !!}
                </label>
          </div>
          <div class="col-md-3">
                <label>
                    <span class="radioBtnpan">
                    </span>
                </label>
                <label for="work_doc_video" class="field prepend-icon">
                    {!! Form::text('work_doc_link', old('work_doc_link',isset($work_diary['work_doc_link']['doc']) ? $work_diary['work_doc_link']['doc'] : ''), ['class' => 'form-control', 'placeholder' => 'Paste youtube video link here','id' => 'work_doc_link','readonly' => isset($work_diary['work_doc_link']) ? false : true]) !!}
                </label>
          </div>
      </div>
    </div>
    </div>
    <div class="section row" id="spy1">
        <div class="col-md-8">
            <label><span class="radioBtnpan">{{trans('language.work')}}<span class="asterisk">*</span></span></label>
            <label class="field">
                {!!Form::textarea('work',isset($work_diary['work']) ? $work_diary['work'] : '', ['class' => 'form-control ckeditor','id'=>'work','rows'=>4])!!}
                <i class="arrow double"></i>
            </label> @if ($errors->has('work'))
            <p class="help-block">{{ $errors->first('work') }}</p>
            @endif
        </div>
    </div>
    </div>
    <!-- end .form-body section -->
    <div class="panel-footer text-right">
        {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
    </div>
    </div>
    <!-- end .form-footer section -->
    </div>

    <script type="text/javascript">
        jQuery(document).ready(function(){

            $("#work_start_date,#work_end_date").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                minDate: 0,
                maxDate: "{!! $work_diary['session_end_date'] !!}"
            });
            $("#work-diary-form").validate({

                /* @validation states + elements 
                ------------------------------------------- */
                errorClass: "state-error",
                validClass: "state-success",
                errorElement: "em",
                /* @validation rules 
                ------------------------------------------ */
                ignore: [],
                debug: false,
                rules: {
                    work_type: {
                        required: true
                    },
                    class_id: {
                        required: true
                    },
                    subject_id: {
                        required: true
                    },
                    session_id: {
                        required: true
                    },
                    work_doc: {
                        extension: true
                    },
                    work_start_date: {
                        required: true
                    },
                    work_end_date: {
                        required: true
                    },
                    work: {
                        required: function () {
                            var messageLength = CKEDITOR.instances['work'].getData().replace(/<[^>]*>/gi, '').length;
                            if (!messageLength) {
                                return true;
                            } else
                            {
                                return false;
                            }
                        }
                    },
                    section_id: {
                        required: true
                    },
                },
                /* @validation error messages 
                ---------------------------------------------- */

                /* @validation highlighting + error placement  
                ---------------------------------------------------- */
                highlight: function (element, errorClass, validClass) {
                    $(element).closest('.field').addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).closest('.field').removeClass(errorClass).addClass(validClass);
                },

                errorPlacement: function (error, element) {
                    if (element.is(":radio") || element.is(":checkbox")) {
                        element.closest('.option-group').after(error);
                    } else {
                        error.insertAfter(element.parent());
                    }
                }
            });
            $('#work_start_date').datepicker().on('change', function (ev) {
                var max_date = $(this).datepicker("option", "maxDate");
                $("#work_end_date").datepicker('destroy');
                $("#work_end_date").datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>',
                    nextText: '<i class="fa fa-chevron-right"></i>',
                    showButtonPanel: false,
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd/mm/yy",
                    minDate: $(this).val(),
                    maxDate: max_date
                });
                $(this).valid();
            });
            $('#work_start_date').datepicker().on('change', function (ev) {
                $(this).valid();
            });
            $('#work_end_date').datepicker().on('change', function (ev) {
                $(this).valid();
            });

            jQuery.validator.addMethod("extension", function (a, b, c) {
                return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
            }, jQuery.validator.format("Only valid image formats are allowed"));

            $(document).on('change', '#class_id,#section_id', function (e) {
                if ($(this).attr('id') == "class_id")
                {
                    getClassSection();
                }
                getClassSubject();
            });
            function getClassSubject()
            {
                var class_id = $('#class_id').val();
                var section_id = $('#section_id').val();
                $('select[name="subject_id"]').empty();
                $('select[name="subject_id"]').append('<option value=""> -- Select -- </option>');
                if (class_id !== '' && section_id !== '')
                {
                    $.ajax(
                    {
                        url: "{!! url('get-class-subject/" + class_id + "/" +section_id + "') !!}",
                        datatType: 'json',
                        type: 'GET',
                        beforeSend: function () {
                            $("#LoadingImage").show();
                        },
                        success: function (res)
                        {
                            $("#LoadingImage").hide();
                            var arr_subject = res.data.subject;
                            if (res.status === "success")
                            {
                                                // subject dropdown
                                                $.each(arr_subject, function (key, value) {
                                                    $('select[name="subject_id"]').append('<option value="' + key + '">' + value + '</option>');
                                                });
                                            } else
                                            {
                                                $('#server-response-message').text('Something went wrong');
                                                $('#alertmsg-work_diary').modal({
                                                    backdrop: 'static',
                                                    keyboard: false
                                                });
                                            }
                                        }
                                    });
                }
            }

            function getClassSection() {
                $('select[name="section_id"]').empty();
                $('select[name="section_id"]').append('<option value="">--Select section--</option>');
                var class_id = $("#class_id").val();
                var session_id = $("#session_id").val();
                if (class_id !== '' && session_id !== '')
                {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url: "{{url('get-section-list')}}",
                        datatType: 'json',
                        type: 'POST',
                        data: {
                            'class_id': class_id,
                            'session_id': session_id,
                        },
                        success: function (response) {
                            var resopose_data = [];
                            resopose_data = response.data;
                            if (response.status == 'success')
                            {
                                $.each(resopose_data, function (key, value) {
                                    $('select[name="section_id"]').append('<option value="' + key + '">' + value + '</option>');
                                });
                            } else
                            {
                                return false;
                            }
                        }
                    });
                }

            }


            $(document).on('change', '#class_id,#section_id', function (e) {
            	$("#student_id").empty();
            	$("#student_id").append('<option value=""> -- Select Student -- </option>');
                var class_id = $("#class_id").val();
                var section_id = $("#section_id").val();
                if (class_id !== '' && section_id !=='')
                {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url: "{{url('get-student-list')}}",
                        datatType: 'json',
                        type: 'POST',
                        data: {
                            'class_id': class_id,
                            'section_id': section_id,
                        },
                        beforeSend: function () {
                            $('#LoadingImage').show();
                        },
                        success: function (response) {
                            var resopose_data = [];
                            resopose_data = response.data;
                            if (response.status === 'success')
                            {
                                $.each(resopose_data, function (key, value) {
                                    $("#student_id").append('<option value="' + key + '">' + value + '</option>');
                                });
                            }
                            $('#LoadingImage').hide();
                        }
                    });
                }
            });
        });
        $(document).ready(function(){
            $(document).on('change', '#check_for_video', function(e){
                if(this.value == 2){
                    $("#work_doc_link_title").prop("readonly", false);
                    $("#work_doc_link").prop("readonly", false);
                    $("#work_doc_link_title").prop('required',true);
                    $("#work_doc_link").prop('required',true);

                    $("#work_doc_video_title").prop("readonly", true);
                    $("#work_doc_video_title").prop('required',false);
                }else if(this.value == 1){
                    $("#work_doc_video_title").prop("readonly", false);
                    $("#work_doc_video_title").prop('required',true);

                    $("#work_doc_link_title").prop("readonly", true);
                    $("#work_doc_link").prop("readonly", true);

                    $("#work_doc_link_title").prop('required',false);
                    $("#work_doc_link").prop('required',false);
                }
            });

            $(document).on('change', '#check_for_doc', function(e) {
                if(this.checked) {
                    $("#work_doc_image_title").prop("readonly", false);
                    $("#work_doc_image").prop("readonly", false);
                }else{
                    $("#work_doc_image_title").prop("readonly", true);
                    $("#work_doc_image").prop("readonly", true);
                }
            });

            jQuery('#view_doc_2').on('hidden.bs.modal', function (e) {
              // do something...
              jQuery('#view_doc_2 video').attr("src", jQuery("#view_doc_2  video").attr("src"));
            });
        });
    </script>