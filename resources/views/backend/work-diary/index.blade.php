@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_work_diary')}}</span>
                </div>
                 <div class="topbar-right">
                     <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right custom-btn">{{trans('language.add_work_diary')}}</a>
                 </div>
            </div>
            <div class="panel" id="transportId">
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!! Form::text('search_start_date',null,['class' => 'form-control date_picker','id' => 'search_start_date', 'readonly' => 'readonly','placeholder'=>'Start Date']) !!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="work-diary-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.work_type') !!}</th>
                            <th>{!!trans('language.class') !!}</th>
                            <th>{!!trans('language.subject') !!}</th>
                            <th>{!!trans('language.work_start_date') !!}</th>
                            <th>{!!trans('language.work_end_date') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        workDiaryData();
        function workDiaryData(){
            var table = $('#work-diary-table').DataTable({
                destroy: true,
                // dom: 'Blfrtip',
                paging: false,

                processing: true,
                serverSide: true,
                order: false,
                ajax: {
                    url: "{{ url('work-diary/data') }}",
                    data: function (f) {
                        f.search_start_date = $('#search_start_date').val();
                    }
                },
                {{--ajax: "{{ url('work-diary/data') }}",--}}
                {{--data: {"search_start_date" : $('#search_start_date').val() },--}}
                columns: [
                    {data: 'work_type_name', name: 'work_type_name'},
                    {data: 'class_name', name: 'class_name'},
                    {data: 'subject_name', name: 'subject_name'},
                    {data: 'work_start_date', name: 'work_start_date'},
                    {data: 'work_end_date', name: 'work_end_date'},
//                {data: 'work-diary_content', name: 'work-diary_content'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}

                ]
            });
        }

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        $.ajax(
                                {
                                    url: "{{url('work-diary/delete/')}}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'work_diary_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();

                                        }
                                        else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });

        $(document).on('change', '#search_start_date', function (e) {
            workDiaryData();
        });
    });
</script>
</body>
</html>
@endsection
@push('scripts')
@endpush

