<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_department')}}</li>
                </ol>
            </div>
        </div>
        <div class="bg-light panelBodyy">
            @if(Session::has('success'))
             @include('backend.partials.messages')
            @endif
            <div class="section-divider mv40">
                <span><i class="fa fa-building-o"></i>&nbsp;Department Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.department_name')}}<span class="asterisk">*</span></span></label>
                    <label for="department_name" class="field prepend-icon">
                        {!! Form::text('department_name', old('department_name',isset($department->department_name) ? $department->department_name : ''), ['class' => 'gui-input', 'id' => 'department']) !!}
                        <label for="department_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('department_name')) 
                    <p class="help-block">{{ $errors->first('department_name') }}</p>
                    @endif
                </div>
            </div>
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_department')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="department-table" cellspacing="0" width="100%">                    
                    <thead>
                        <tr>
                            <th>{!!trans('language.department_name') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

        $("#department-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                department_name: {
                    required: true,
                    lettersonly: true,
                },

            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        
        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || !/[~`!@#$%\^&*()+=\-\_\.\[\]\\';,/{}|\\":<>\?]/.test(value) || /^[a-zA-Z][a-zA-Z0-9\s\.]+$/i.test(value);
        }, "Please enter only alphabets(more than 1)");

// view index code
 var table = $('#department-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('department/data')}}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'department_name', name: 'department_name'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        $.ajax(
                                {
                                    url: "{{ url('department/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'department_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();

                                        } 
                                         else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
    });
</script>