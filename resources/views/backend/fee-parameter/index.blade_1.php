@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_fee_parameter')}}</span>
                </div>

                <div class="topbar-right">
                    <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right custom-btn">{{trans('language.add_fee_parameter')}}</a>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="fee-parameter-table" cellspacing="0" width="100%">                    
                    <thead>
                        <tr>
                            <th>{{trans('language.class_name')}}</th>
                            <th>{{trans('language.fee_type')}}</th>
                            <th>{{trans('language.apply_to')}}</th>
                            <th>{{trans('language.fee_circular')}}</th>
                            <!--<th>{{trans('language.fee_amount')}}</th>-->
                            <!--<th>{{trans('language.grace_period')}}</th>-->
                            <th class="text-center">{{trans('language.action')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function () {
    var table = $('#fee-parameter-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{url('fee-parameter/data')}}",
        columns: [
            {data: 'class_name', name: 'class_name'},
            {data: 'fee_type', name: 'fee_type'},
            {data: 'apply_to_student', name: 'apply_to_student'},
            {data: 'fee_circular', name: 'fee_circular'},
//            {data: 'fee_amount', name: 'fee_amount'},
//            {data: 'grace_period', name: 'grace_period'},
            {data: 'action', name: 'action', orderable: false, searchable: false}

        ]
    });

    $(document).on("click", ".delete-button", function (e) {
        e.preventDefault();
        var id = $(this).attr("data-id");

        bootbox.confirm({
            message: "Are you sure to delete ?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result)
                {
                    var token = '{!!csrf_token()!!}';
                    $.ajax(
                            {
                                url: "{{ url('fee-parameter/delete/') }}",
                                datatType: 'json',
                                type: 'POST',
                                data: {
                                    'fee_id': id,
                                },
                                success: function (res)
                                {
                                    if (res.status == "success")
                                    {
                                        table.ajax.reload();

                                    } else if (res.status === "used")
                                    {
                                        $('#server-response-message').text(res.message);
                                        $('#alertmsg-student').modal({
                                            backdrop: 'static',
                                            keyboard: false
                                        });
                                    }
                                }
                            });
                }
            }
        });

    });
});

</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



