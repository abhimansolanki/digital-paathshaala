<style type="text/css">
    .table > thead > tr > th{
        font-weight: bold;
        font-size: 14px !important;
    }
    .table {
        background-color: #f3f3f3;
    }
    tr td{
        font-weight: bold;
    }
</style>

@if(!empty($arr_sub_circular))
<table class="table table-striped">
    <thead>
        <tr >
            <th class="text-left">{{trans('language.sub_circular')}}</th>
            @if($fee_circular == 'monthly' || $fee_circular == 'installment')
            @else
            <th class="text-left">{{trans('language.start_month')}}</th>
            <th class="text-left">{{trans('language.end_month')}}</th>
            @endif
            <th class="text-left">{{trans('language.fee_amount')}}</th>
            <th class="text-left">{{trans('language.fine_date')}}</th>
            <th class="text-left">{{trans('language.grace_period')}}</th>
        </tr>
    </thead>
    <tbody>

        @php $custom_key = 0; @endphp 
        @foreach($arr_sub_circular as $key=> $sub_circular)
        @php $fee_detail = isset($arr_fee_detail[$custom_key]) ? $arr_fee_detail[$custom_key] :[]; @endphp
        <tr class="student_information">
            <td class="text-center">
                {!!Form::hidden('fee_detail_id['.$custom_key.']',isset($fee_detail['fee_detail_id']) ? $fee_detail['fee_detail_id'] :'', ['class' => 'form-control','id'=>'fee_detail_id'])!!}
                {!!Form::hidden('fee_sub_circular['.$custom_key.']',$key, ['class' => 'form-control','id'=>'fee_sub_circular'])!!}
                {{$sub_circular}}
            </td>
            @if($fee_circular == 'monthly' || $fee_circular == 'installment')
            {!!Form::hidden('start_month['.$custom_key.']',isset($custom_months[ucfirst($key)]) ? $custom_months[ucfirst($key)] : '', ['class' => 'form-control','id'=>'start_month'])!!}
            {!!Form::hidden('end_month['.$custom_key.']',isset($custom_months[ucfirst($key)]) ? $custom_months[ucfirst($key)] : '', ['class' => 'form-control','id'=>'end_month'])!!}
            @else
            <td class="text-left">
                <div>
                    {!!Form::select('start_month['.$custom_key.']', $arr_months,isset($fee_detail['start_month']) ? $fee_detail['start_month'] : '', ['class' => 'form-control','id'=>'start_month'.$custom_key,'required'=>"true", $circular_edit_readonly])!!}
                </div>
            </td>
            <td class="text-left">
                <div>
                    {!!Form::select('end_month['.$custom_key.']', $arr_months,isset($fee_detail['end_month']) ? $fee_detail['end_month'] : '', ['class' => 'form-control','id'=>'end_month'.$custom_key,'required'=>"true", $circular_edit_readonly])!!}
                </div>
            </td>
            @endif
            <td class="text-left">
                <div>
                    {!! Form::number('fee_amount['.$custom_key.']', old('fee_amount',isset($fee_detail['fee_amount']) ? $fee_detail['fee_amount'] : ''), ['class' => 'gui-input form-control',  'id' => 'fee_amount'.$custom_key,'min'=>0,'required'=>"true", $circular_edit_readonly]) !!}
                </div> 
            </td>
            <td class="text-left ">
                <div>
                    {!! Form::text('fine_date['.$custom_key.']', old('fine_date',isset($fee_detail['fine_date']) ? $fee_detail['fine_date'] : ''), ['class' => 'gui-input date_picker ', 'id' => '', 'readonly' => 'readonly']) !!}
                </div>
            </td>
            <td class="text-left ">
                <div>
                    {!! Form::number('grace_period['.$custom_key.']', old('grace_period',isset($fee_detail['grace_period']) ? $fee_detail['grace_period'] : ''), ['class' => 'gui-input','id' => 'grace_period','min'=>0]) !!}
                </div>
            </td>
        </tr>
        @php $custom_key++; @endphp 
        @endforeach

    <script type="text/javascript">
        $(".date_picker").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            yearRange: "-10:+50"

        });
    </script>

</tbody>
</table>
@endif

