<style type="text/css">
    .admin-form .select>select{
        height: 35px !important;
    }
    /*    .state-error{
            margin-top: 15px !important;
        }*/
    .radioBtnpan{
        margin-top: 10px;
    }
    .admin-form .select{
        margin-top: 0px;
    }
    .help-block{
        margin-top: 13px;
    }

</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_fee_parameter')}}</li>
                </ol>
            </div>
            <div class="topbar-right">
                <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right">{{trans('language.view_fee_parameter')}}</a>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages') 
        @endif
        <div class="bg-light panelBodyy">

            <div class="section-divider mv40">
                <span><i class="fa fa-rupee"></i> Fee Detail</span>
            </div>
            <div class="section row" id="spy1">
                {!! Form::hidden('fee_id', old('fee_id',isset($fee['fee_id']) ? $fee['fee_id'] : ''), ['class' => 'gui-input', 'id' => 'fee_id', 'readonly' => 'readonly']) !!}
                {!! Form::hidden('circular_edit','', ['class' => 'gui-input', 'id' => 'circular_edit', 'readonly' => 'readonly']) !!}
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.session')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $fee['arr_session'],isset($fee['session_id']) ? $fee['session_id'] :$fee['session']['session_id'], ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if($errors->has('session_id')) <p class="help-block">{{ $errors->first('session_id') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.fee_type')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('fee_type_id', $fee['arr_fee_type'],isset($fee['fee_type_id']) ? $fee['fee_type_id'] : '', ['class' => 'form-control','id'=>'fee_type_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if($errors->has('fee_type_id')) <p class="help-block">{{ $errors->first('fee_type_id') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.apply_to')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('apply_to', $fee['arr_fee_apply_to'], isset($fee['apply_to']) ? $fee['apply_to'] : '', ['class' => 'form-control','id'=>'apply_to'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if($errors->has('apply_to')) <p class="help-block">{{ $errors->first('apply_to') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.fee_circular')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('fee_circular_id', $fee['arr_fee_circular'],isset($fee['fee_circular_id']) ? $fee['fee_circular_id'] : '', ['class' => 'form-control','id'=>'fee_circular_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('fee_circular_id')) <p class="help-block">{{ $errors->first('fee_circular_id') }}</p> @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3" style="display:none" id="main_class_div">
                    <label><span class="radioBtnpan">{{trans('language.class')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('class_id', $fee['arr_class'],isset($fee['class_id']) ? $fee['class_id'] : '', ['class' => 'form-control','id'=>'class_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('class_id')) <p class="help-block">{{ $errors->first('class_id') }}</p> @endif
                </div>
                <div class="col-md-3" style="display:block" id="dummy_class_div">
                    <label><span class="radioBtnpan">{{trans('language.class')}}</span></label>
                    <label class="field select">
                        {!!Form::select('dummy_class_id',[''=>'--Select class --'],'', ['class' => 'form-control','id'=>'dummy_class_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('class_id')) <p class="help-block">{{ $errors->first('class_id') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.student_type')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('student_type_id', $fee['arr_student_type'],isset($fee['student_type_id']) ? $fee['student_type_id'] : '', ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if($errors->has('student_type_id')) 
                    <p class="help-block">{{ $errors->first('student_type_id') }}</p>
                    @endif
                </div>

            </div>
            <div class="section-divider mv40">
                <span><i class="fa fa-rupee"></i> Fine Detail</span>
            </div>
            <div class="section row" id="spy1">

                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.fine_amount')}}</span></label>
                    <label for="fine_amount" class="field prepend-icon">
                        {!! Form::number('fine_amount', old('fine_amount',isset($fee['fine_amount']) ? $fee['fine_amount'] : ''), ['class' => 'gui-input',''=>trans('language.fine_amount'), 'id' => 'fine_amount','min'=>0]) !!}
                        <label for="fine_amount" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('fine_amount')) <p class="help-block">{{ $errors->first('fine_amount') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.fine_type')}}</span></label>
                    <label class="field select">
                        {!!Form::select('fine_type', $fee['arr_fine_type'],isset($fee['fine_type']) ? $fee['fine_type'] : '', ['class' => 'form-control','id'=>'fine_type'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('fine_type')) <p class="help-block">{{ $errors->first('fine_type') }}</p> @endif
                </div>
                <!-- end section -->
            </div>
            <div class="section-divider mv40">
                <span><i class="fa fa-rupee"></i> Fee Circular Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="" id="fee-sub-circular-panel" style="font-size: 11px;">
                </div>
            </div>
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

         displayClassDiv();
        $("#fine_amount").removeAttr('max', '');
        $("#fee-parameter-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
//                fee_amount: {
//                    required: true
//                },
                fine_type: {
                    required: function (e) {
                        if ($("#fine_amount").val() != 0) {
                            return true;
                        } else {
                            return false;
                        }
                    },
                },
                fee_circular_id: {
                    required: true
                },
                student_type_id: {
                    required: true
                },
                fee_type_id: {
                    required: true,
                },
                session_id: {
                    required: true,
                },
                apply_to: {
                    required: true,
                },
                'start_month[]': {
                    required: true,
                },
                'end_month[]': {
                    required: true,
                },
                'fee_amount[]': {
                    required: true,
                },
                class_id: {
                    required: function (e) {
                        if (($("#apply_to").val() == 'all_student' || $("#apply_to").val() == 'new_student'))
                        {
                            return false;
                        } else
                        {
                            return true;
                        }
                    }
                },

            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        setAppfee();
//        $(document).on('change', '#fee_type_id', function (ev) {
//            setAppfee();
//        });
        function setAppfee()
        {
            $("#fee_amount0").attr('readonly', false);
            var fee_id = $("#fee_id").val();
            var fee_type_id = $("#fee_type_id").val();
            if (fee_id !== '' && fee_type_id == 1 || fee_type_id == 2)
            {
                $("#fee_amount0").attr('readonly', true);
                $('#fee_type_id option:not(:selected)').attr('disabled', true);
            }
        }

        var fee_circular_id = $("#fee_circular_id").val();
        var fee_id = $("#fee_id").val();
        if (fee_circular_id !== '')
        {
            fee_sub_circular_data(fee_circular_id, fee_id);
        }
    });
    $(document).on('change', '#fine_type', function (e) {
        $("#fine_amount").removeAttr('max', '');
        if ($(this).val() == 1)
        {
            $("#fine_amount").attr('max', 100);
        }

    });
    $(document).on('change', '#fee_circular_id', function (e) {
        var fee_circular_id = $(this).val();
        fee_sub_circular_data(fee_circular_id, null);
    });
    $(document).on('change', '#apply_to', function (e) {
        displayClassDiv();

    });
    function displayClassDiv() {
        $("#dummy_class_div").show();
        $("#main_class_div").hide();
        if (($("#apply_to").val() == 'class_wise' || $("#apply_to").val() == 'new_student_class_wise'))
        {
            $("#dummy_class_div").hide();
            $("#main_class_div").show();
        }
    }
    function fee_sub_circular_data(fee_circular_id, fee_id = null)
    {
        if (fee_circular_id != '')
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-fee-sub-circular')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'fee_circular_id': fee_circular_id,
                    'fee_id': fee_id,
                },
                success: function (response) {
                    var resopose_data = [];
                    var circular_edit = '';
                    resopose_data = response.data;
                    circular_edit = response.circular_edit;
                    if (response.status == 'success')
                    {
                        $("#fee-sub-circular-panel").html(resopose_data);
                        $("#circular_edit").val(circular_edit);
                        var fee_type_id = $("#fee_type_id").val();
                        if (fee_type_id == 1 || fee_type_id == 2)
                        {
                            $("#fee_amount0").attr('readonly', true);
                            $("#fee_amount0").prop('required', false);
                        }
                        if (circular_edit == 'not_allowed')
                        {
                            $('#fee_circular_id option:not(:selected)').attr('disabled', true);
                        }

                    } else
                    {
                        $("#fee-sub-circular-panel").html('');
                    }
                }
            });
        } else
        {
            $("#fee-sub-circular-panel").html('');
        }
    }

</script>
