@extends('admin_panel/layout')
@section('content')
<style>
    #deletebtn{
        margin-top:-14px !important;
    }
    .btn:hover, .btn:focus, .btn:active{
       border:none !important;
    }
</style>
<div class="admin-form theme-primary" style="padding-bottom: 175px;" id="CustomInput">
    @include('backend.partials.loader')
    <div class="tray tray-center tableCenter">
        <div class="">
            <div class="panel panel-visible" id="spy2">
                <div class="panel heading-border">
                    <div id="topbar">
                        <div class="topbar-left">
                            <ol class="breadcrumb">
                                <li class="crumb-active">
                                    <a href="{{url('admin/dashboard')}}"> <span class="glyphicon glyphicon-home"></span> Home</a>
                                </li>
                                <li class="crumb-trail">{{ trans('language.view_fee_parameter')}}</li>
                            </ol>
		    <div class="topbar-right">
		        <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right custom-btn">{{trans('language.add_fee_parameter')}}</a>
		    </div>
                        </div>
                    </div>
                    <div class="panel-body pn">
                        <div class="row">
                            <br>
                            {!! Form::open(['id'=>'class-fee-from']) !!}
                            <div class="col-md-2">
                                <label><span class="radioBtnpan">{{ trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                                <label for="section_id" class="field prepend-icon">
                                    <label class="field select">
                                        {!!Form::select('session_id', $arr_session, $session['session_id'], ['class' => 'form-control','id'=>'session_id'])!!}
                                        <i class="arrow"></i>
                                    </label>
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label><span class="radioBtnpan">{{ trans('language.class')}}<span class="asterisk">*</span></span></label>
                                <label for="section_id" class="field prepend-icon">
                                    <label class="field select">
                                        {!!Form::select('class_id', $arr_class,null, ['class' => 'form-control','id'=>'class_id'])!!}
                                        <i class="arrow"></i>
                                    </label>
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label><span class="radioBtnpan"></span></label>
                                <label for="" class="field prepend-icon">
                                    {!! Form::button('Download Excel', ['class' => 'button btn-primary','style'=>'width:170px; margin-top: 5px;','id'=>'excel-data','disabled'=>false]) !!}   
                                </label>
                            </div>
                            <div class="col-md-2">
                                <label><span class="radioBtnpan"></span></label>
                                <label for="" class="field prepend-icon">
                                    {!! Form::button('Print', ['class' => 'button btn-primary','style'=>'width:120px; margin-top: 5px;','id'=>'print-data','disabled'=>false]) !!}   
                                </label>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div id="view-class-subject">{!! $class_data !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('backend.partials.popup')
<script>
    $(document).ready(function () {
        $("#class-fee-from").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             * $("#section_id option").length
             ------------------------------------------ */
            ignore: [],
            rules: {
                class_id: {
                    required: true,
                },
                session_id: {
                    required: true
                },
                'section_id': {
                    required: true
                },

            },

            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('fee-parameter/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'fee_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            location.reload();

                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
        $(document).on('change', '#class_id,#session_id', function (e) {
            e.preventDefault();
            // get class-subject
            getClassFeeData();

        });
    });

    // get class subject all data 
    function getClassFeeData()
    {
        var class_id = $('#class_id').val();
        var session_id = $('#session_id').val();
        $('#view-class-subject').html('');
        $('#excel-data').attr('disabled', true);
        $('#print-data').attr('disabled', true);
        if (session_id !== '')
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-class-fee-data')}}",
                datatType: 'json',
                type: 'GET',
                data: {
                    'class_id': class_id,
                    'session_id': session_id,
                },
                beforeSend: function () {
                    $("#LoadingImage").show();
                },
                success: function (response) {
                    $("#LoadingImage").hide();
                    var resopose_data = response.data;
                    if (response.status === 'success')
                    {
                        $('#excel-data').attr('disabled', false);
                        $('#print-data').attr('disabled', false);
                        $("#view-class-subject").html(resopose_data);
                    }
                }
            });
        } else
        {
            $("#view-class-subject").html('<div class="text-center">No records found</div>');
        }
    }

// print class -subject mapping data
    function printData()
    {
        var divToPrint = document.getElementById("class-sbject-data-table");
        newWin = window.open("");
        newWin.document.write('<html><head><title></title>');
        newWin.document.write('<style>table {  border-collapse: collapse;  border-spacing: 0; width:100%; margin-top:20px;} .table td, .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{ padding:8px 18px;  } .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {     border: 1px solid #e2e2e2;} </style>');
        newWin.document.title = "Class Fee report";
        newWin.document.write('</head><body>');
        newWin.document.write('<h3>Academic Year : ' + $('#session_id option:selected').html() + '</h3>')
        newWin.document.write(divToPrint.outerHTML);
        newWin.document.write('</body></html>');
        newWin.print();
        newWin.close();
    }

    $(document).on('click', '#print-data', function () {
        printData();
    });

// download excel of data class-subject mapping
    $(document).on('click', '#excel-data', function () {
        var htmls_data = "";
        var uri = 'data:application/vnd.ms-excel;base64,';
        var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
        var base64 = function (s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        };
        var format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            });
        };
        htmls_data = '<h4>Academic Year: ' + $('#session_id option:selected').html() + '</h4>';
        htmls_data += $('#class-sbject-data-table').html();
        var ctx = {
            worksheet: 'Class Fee',
            table: htmls_data
        }
        var link = document.createElement("a");
        link.download = "class_fee.xls";
        link.href = uri + base64(format(template, ctx));
        link.click();
    });

</script>
</body>
</html>
@endsection