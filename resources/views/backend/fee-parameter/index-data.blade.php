<!--get class data-->
@if(!empty($class_fee))
<table class="table table-bordered" id="class-sbject-data-table">
    <thead>
    <th>{!! trans('language.class')!!}</th>
    <th>{!! trans('language.total_fee')!!}</th>
    <th>{!! trans('language.fee_type')!!}</th>
    <th>{!! trans('language.fee_amount')!!}</th>
    <th>{!! trans('language.fee_circular')!!}</th>
    <th>{!! trans('language.apply_to')!!}</th>
    <th>{!! trans('language.action')!!}</th>
    
</thead>
<tbody>
    <!--start out class loop-->
    @foreach($class_fee as $class_id => $fee)
    @php $class = 1; @endphp
    <!--start inner fee type loop-->
    @foreach($fee['arr_fee'] as  $fee_data)
    <tr>
        @if($class == 1)<td rowspan="{!! count($fee['arr_fee']) !!}">{!! $fee['class_name'] !!}</td>@endif
        @if($class == 1)<td rowspan="{!! count($fee['arr_fee']) !!}">{!! array_sum(array_column($fee['arr_fee'],'total_fee')) !!}</td>@endif
        <td>{!! $fee_data['fee_type'] !!}</td>
        <td>{!! $fee_data['total_fee'] !!}</td>
        <td>{!! $fee_data['fee_circular'] !!}</td>
        <td>{!! $fee_data['apply_to'] !!}</td>
        <td>{!! $fee_data['action'] !!}</td>
        
    </tr>
    @php $class++; @endphp
    @endforeach
    <!--end inner fee type loop -->
    @endforeach
    <!--end out class loop -->
</tbody>
</table>
@endif
