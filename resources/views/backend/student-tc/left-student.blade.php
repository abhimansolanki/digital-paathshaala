@extends('admin_panel/layout')
@section('content')
    <style>
        button[disabled] {
            background: #76aaef !important;
        }
    </style>
    <div class="tray tray-center tableCenter">
        <div class="">
            <div class="panel panel-visible" id="spy2">
                <div class="panel-heading">

                    <div class="panel-title hidden-xs col-md-7">
                        <span class="glyphicon glyphicon-tasks"></span> <span>List Left Student's</span>
                    </div>
                </div>
                <div class="panel" id="transportId">
                    <div class="panel-body">
                        <div class="tab-content  br-n">
                            <div id="tab1_1" class="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('session_id',add_blank_option(get_session('yes'),'-- Select Session --'),null, ['class' => 'form-control','id'=>'session_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('class_id', $arr_class,'', ['class' => 'form-control','id'=>'class_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body pn">
                    @include('backend.partials.messages')
                    <table class="table table-bordered table-striped table-hover" id="student-tc-table" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>Name</th>
                            <th>Father Name</th>
                            <th>{{trans('language.class')}}</th>
                            <th>{{trans('language.dob')}}</th>
                            <th>{{trans('language.admission_date')}}</th>
                            <th>{{trans('language.address')}}</th>
                            <th>{{trans('language.father_contact_number')}}</th>
                            <th class="text-center">Revert</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            getStudentTc();

            function getStudentTc() {
                $('#student-tc-table').DataTable({
                    destroy: true,
                    processing: true,
                    serverSide: true,
                    dom: 'Blfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                            "title": 'Student TC',
                            "filename": 'student-tc',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                                modifier: {
                                    selected: true
                                }
                            }
                        },
                        {
                            extend: 'print',
                            "text": '<span class="fa fa-print"></span> &nbsp; Print',
                            "title": 'Student TC',
                            "filename": 'student-tc',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                                modifier: {
                                    selected: true
                                }
                            }
                        }
                    ],
                    select: {
                        style: 'multi',
                        selector: 'td:first-child'
                    },
                    'columnDefs': [
                        {
                            'targets': 0,
                            'className': 'select-checkbox',
                            'checkboxes': {
                                'selectRow': true
                            }
                        }
                    ],
                    ajax: {
                        url: "{{ url('student-left-data')}}",
                        data: function (f) {
                            f.session_id = $("#session_id option:selected").val();
                            f.class_id = $("#class_id option:selected").val();
                            f.section_id = $("#section_id option:selected").val();
                        }
                    },
                    columns: [
                        {data: 'student_id', name: 'student_id'},
                        {data: 'enrollment_number', name: 'enrollment_number'},
                        {data: 'student_name', name: 'student_name'},
                        {data: 'father_name', name: 'father_name'},
                        {data: 'class_name', name: 'class_name'},
                        {data: 'dob', name: 'dob'},
                        {data: 'admission_date', name: 'admission_date'},
                        {data: 'address_line1', name: 'address_line1'},
                        {data: 'father_contact_number', name: 'father_contact_number'},
                        {data: 'revert', name: 'revert', orderable: false, searchable: false},

                    ]
                });

                $(".buttons-excel,.buttons-print").css({
                    'margin-left': '7px',
                    'background-color': '#2e76d6',
                    'color': 'white',
                    'border': '1px solid #eeeeee',
                    'float': 'right',
                    'padding': '5px'
                });

                $(".buttons-excel").prop('disabled', true);
                $(".buttons-print").prop('disabled', true);
            }

            var table = $('#student-tc-table').DataTable();
            table.on('select deselect', function (e) {
                var arr_checked_student = checkedStudent();
                if (arr_checked_student.length > 0) {
                    $(".buttons-excel").prop('disabled', false);
                    $(".buttons-print").prop('disabled', false);
                } else {
                    $(".buttons-excel").prop('disabled', true);
                    $(".buttons-print").prop('disabled', true);
                }
            });

            function checkedStudent() {
                var arr_checked_student = [];
                $.each(table.rows('.selected').data(), function () {
                    arr_checked_student.push(this["student_id"]);
                });
                return arr_checked_student;
            }

            $(document).on('change', '#session_id,#class_id', function (e) {
                getStudentTc();
            });
        });

    </script>
    </body>
    </html>
@endsection


