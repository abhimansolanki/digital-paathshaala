<style type="text/css">
    .admin-form .select {
        margin-top: 0px;
    }

    .select2-container .select2-selection--single {
        height: 35px !important;
    }

    .admin-form .select > select {
        height: 35px !important;
    }

    .state-error {
        display: block !important;
        margin-top: 6px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    }

</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_student_tc')}}</li>
                </ol>
            </div>
            <div class="topbar-right">
{{--                <a href="{{$edit_student_url}}" class="button btn-primary text-right" style="margin-right: 10px;"--}}
{{--                   title="Edit Student">Edit Student</a>--}}
                <a href="{{$redirect_url}}" class="button btn-primary text-right pull-right" title="View Student TC">View
                    Student TC</a>
            </div>
        </div>
            @include('backend.partials.messages')
        <div class="bg-light panelBodyy">
            {!! Form::hidden('student_tc_id',old('student_tc_id',isset($student_tc['student_tc_id']) ? $student_tc['student_tc_id'] : ''),['class' => 'gui-input', 'id' => 'student_tc_id', 'readonly' => 'true']) !!}
            {!! Form::hidden('session_id',old('session_id',isset($student_tc['session_id']) ? $student_tc['session']['session_id'] : ''),['class' => 'gui-input', 'id' => 'session_id', 'readonly' => 'true']) !!}
            <div class="section-divider mv40">
                <span><i class="fa fa-graduation-cap"></i> Student details</span>
            </div>
            @if(empty($student_tc['student_tc_id']))
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Select Class<span class="asterisk">*</span></span></label>
                    <div class="section">
                        <label class="field select">
                            {!!Form::select('class_id', $student_tc['arr_class'],isset($student_tc['class_id']) ? $student_tc['class_id'] : '', ['class' => 'form-control','id'=>'class_id'])!!}
                            <i class="arrow double"></i>
                        </label>
                        @if ($errors->has('class_id'))
                            <p class="help-block">{{ $errors->first('class_id') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="section">
                        <label><span class="radioBtnpan">Select Section<span class="asterisk">*</span></span></label>
                        <label class="field select">
                            {!!Form::select('section_id', $student_tc['arr_section'],isset($student_tc['section_id']) ? $student_tc['section_id'] : '', ['class' => 'form-control','id'=>'section_id'])!!}
                            <i class="arrow double"></i>
                        </label>
                        @if ($errors->has('section_id'))
                            <p class="help-block">{{ $errors->first('section_id') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="section">
                        <label><span class="radioBtnpan">Select Student<span class="asterisk">*</span></span></label>
                        <label class="field select">
                            {!!Form::select('student_id', $student_tc['arr_student'],isset($student_tc['student_id']) ? $student_tc['student_id'] : '', ['class' => 'form-control','id'=>'student_id'])!!}
                            <i class="arrow double"></i>
                        </label>
                        @if ($errors->has('student_id'))
                            <p class="help-block">{{ $errors->first('student_id') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            @endif
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">TC Number by system<span class="asterisk">*</span></span></label>
                    <label for="tc_date" class="field prepend-icon">
                        {!! Form::text('tc_number', old('tc_date',isset($student_tc['tc_number']) ? $student_tc['tc_number'] : ''), ['class' => 'gui-input','id' => 'tc_number', 'readonly' =>true]) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('tc_number'))
                        <p class="help-block">{{ $errors->first('tc_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Print TC No.<span class="asterisk">*</span></span></label>
                    <label for="print_tc_number" class="field prepend-icon">
                        {!! Form::text('print_tc_number', old('print_tc_number',isset($student_tc['print_tc_number']) ? $student_tc['print_tc_number'] : ''), ['class' => 'gui-input','id' => 'print_tc_number']) !!}
                        <label for="print_tc_number" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('print_tc_number'))
                        <p class="help-block">{{ $errors->first('print_tc_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">TC Application Date<span class="asterisk">*</span></span></label>
                    <label for="tc_application_date" class="field prepend-icon">
                        {!! Form::text('tc_application_date', old('tc_application_date',isset($student_tc['tc_application_date']) ? $student_tc['tc_application_date'] : ''), ['class' => 'gui-input', 'id' => 'tc_application_date', 'readonly' => 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('tc_application_date'))
                        <p class="help-block">{{ $errors->first('tc_application_date') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">TC Date<span class="asterisk">*</span></span></label>
                    <label for="tc_date" class="field prepend-icon">
                        {!! Form::text('tc_date', old('tc_date',isset($student_tc['tc_date']) ? $student_tc['tc_date'] : ''), ['class' => 'gui-input','id' => 'tc_date', 'readonly' => 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('tc_date'))
                        <p class="help-block">{{ $errors->first('tc_date') }}</p>
                    @endif
                </div>
            </div>

            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.student_name')}}<span
                                    class="asterisk">*</span></span></label>
                    <label for="student_name" class="field prepend-icon">
                        {!! Form::text('student_name', old('student_name',isset($student_tc['student_name']) ? $student_tc['student_name'] : ''), ['class' => 'gui-input', 'id' => 'student_name','readonly'=>true]) !!}
                        <label for="student_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('student_name'))
                        <p class="help-block">{{ $errors->first('student_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_name')}}</span></label>
                    <label for="father_name" class="field prepend-icon">
                        {!! Form::text('father_name', old('father_name',isset($student_tc['father_name']) ? $student_tc['father_name'] : ''), ['class' => 'gui-input', 'id' => 'father_name','readonly'=>true]) !!}
                        <label for="father_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_name'))
                        <p class="help-block">{{ $errors->first('father_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_name')}}</span></label>
                    <label for="mother_name" class="field prepend-icon">
                        {!! Form::text('mother_name', old('mother_name',isset($student_tc['mother_name']) ? $student_tc['mother_name'] : ''), ['class' => 'gui-input','readonly'=>true, 'id' => 'mother_name','readonly'=>true]) !!}
                        <label for="mother_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_name'))
                        <p class="help-block">{{ $errors->first('mother_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.dob')}}</span></label>
                    <label for="datepicker1" class="field prepend-icon">
                        {!! Form::text('dob', old('dob',isset($student_tc['dob']) ? $student_tc['dob'] : ''), ['class' => 'gui-input dateofbirthseprate','id' => 'dob', 'readonly' => 'readonly']) !!}
                    </label>
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.enrollment_number') !!}<span
                                    class="asterisk">*</span></span></label>
                    <label for="enrollment_number" class="field prepend-icon">
                        {!! Form::text('enrollment_number', old('enrollment_number',isset($student_tc['enrollment_number']) ? $student_tc['enrollment_number'] : ''), ['class' => 'gui-input','id' => 'enrollment_number', 'readonly' =>true]) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('enrollment_number'))
                        <p class="help-block">{{ $errors->first('enrollment_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Admission Date<span class="asterisk">*</span></span></label>
                    <label for="admission_date" class="field prepend-icon">
                        {!! Form::text('admission_date', old('admission_date',isset($student_tc['admission_date']) ? $student_tc['admission_date'] : ''), ['class' => 'gui-input', 'id' => 'admission_date', 'readonly' => 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('admission_date'))
                        <p class="help-block">{{ $errors->first('admission_date') }}</p>
                    @endif
                </div>
{{--                <div class="col-md-3">--}}
{{--                    <label><span class="radioBtnpan">Join Date<span class="asterisk">*</span></span></label>--}}
{{--                    <label for="join_date" class="field prepend-icon">--}}
{{--                        {!! Form::text('join_date', old('join_date',isset($student_tc['join_date']) ? $student_tc['join_date'] : ''), ['class' => 'gui-input', 'id' => 'join_date', 'readonly' => 'readonly']) !!}--}}
{{--                        <label for="datepicker1" class="field-icon">--}}
{{--                            <i class="fa fa-calendar-o"></i>--}}
{{--                        </label>--}}
{{--                    </label>--}}
{{--                    @if ($errors->has('join_date'))--}}
{{--                        <p class="help-block">{{ $errors->first('join_date') }}</p>--}}
{{--                    @endif--}}
{{--                </div>--}}
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Religion</span></label>
                    <label for="religion" class="field prepend-icon">
                        {!! Form::text('religion', old('religion',isset($student_tc['religion']) ? $student_tc['religion'] : ''), ['class' => 'gui-input', 'id' => 'religion', 'readonly' => 'readonly']) !!}
                        <label for="religion" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('religion'))
                        <p class="help-block">{{ $errors->first('religion') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Last Session<span class="asterisk">*</span></span></label>
                    <label for="tc_date" class="field prepend-icon">
                        {!! Form::text('last_session', old('last_session',isset($student_tc['last_session']) ? $student_tc['last_session'] : ''), ['class' => 'gui-input','id' => 'last_session', 'readonly' =>true]) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('last_session'))
                        <p class="help-block">{{ $errors->first('last_session') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
{{--                <div class="col-md-3">--}}
{{--                    <span class="radioBtnpan">Last School</span>--}}
{{--                    <label for="last_school" class="field prepend-icon">--}}
{{--                        {!! Form::text('last_school', old('last_school',isset($student_tc['last_school']) ? $student_tc['last_school'] : ''), ['class' => 'gui-input ','id' => 'last_school','readonly'=>true]) !!}--}}
{{--                        <label for="last_school" class="field-icon">--}}
{{--                            <i class="fa fa-calendar-o"></i>--}}
{{--                        </label>--}}
{{--                    </label>--}}
{{--                    @if ($errors->has('last_school'))--}}
{{--                        <p class="help-block">{{ $errors->first('last_school') }}</p>--}}
{{--                    @endif--}}
{{--                </div>--}}
                <div class="col-md-3">
                    <span class="radioBtnpan">Admission Class<span
                                class="asterisk">*</span></span>
                    <label for="admission_class_name" class="field prepend-icon">
                        {!! Form::text('admission_class_name', old('admission_class_name',isset($student_tc['admission_class_name']) ? $student_tc['admission_class_name'] : ''), ['class' => 'gui-input ','id' => 'admission_class_name','readonly'=>true]) !!}
                        <label for="admission_class_name" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('admission_class_name'))
                        <p class="help-block">{{ $errors->first('admission_class_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <span class="radioBtnpan">Last Class<span
                                class="asterisk">*</span></span>
                    <label for="last_class" class="field prepend-icon">
                        {!! Form::text('last_class', old('last_class',isset($student_tc['last_class']) ? $student_tc['last_class'] : ''), ['class' => 'gui-input ','id' => 'last_class','readonly'=>true]) !!}
                        <label for="last_class" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('last_class'))
                        <p class="help-block">{{ $errors->first('last_class') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Examination Result / School Leaving Date<span class="asterisk">*</span></span></label>
                    <label for="tc_application_date" class="field prepend-icon">
                        {!! Form::text('final_date', old('final_date',isset($student_tc['final_date']) ? $student_tc['final_date'] : ''), ['class' => 'gui-input', 'id' => 'final_date', 'readonly' => 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('final_date'))
                        <p class="help-block">{{ $errors->first('final_date') }}</p>
                    @endif
                </div>
{{--                <div class="col-md-3">--}}
{{--                    <label><span class="radioBtnpan">Promoted Higher Class ?</span></label>--}}
{{--                    <div class="section">--}}
{{--                        <label class="field select">--}}
{{--                            {!!Form::select('status', $student_tc['status'],isset($student_tc['student_promote_id']) ? $student_tc['student_promote_id'] : '', ['class' => 'form-control','id'=>'status'])!!}--}}
{{--                            <i class="arrow double"></i>--}}
{{--                        </label>--}}
{{--                        @if ($errors->has('status'))--}}
{{--                            <p class="help-block">{{ $errors->first('status') }}</p>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Exam Result Remarks<span class="asterisk">*</span></span></label>
                    <label for="exam_result" class="field prepend-icon">
                        {!! Form::text('exam_result', old('exam_result',isset($student_tc['exam_result_remark']) ? $student_tc['exam_result_remark'] : ''), ['class' => 'gui-input', 'id' => 'exam_result']) !!}
                        <label for="exam_result" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('exam_result'))
                        <p class="help-block">{{ $errors->first('exam_result') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
{{--                <div class="col-md-3">--}}
{{--                    <span class="radioBtnpan">Next Class</span>--}}
{{--                    <label for="next_class" class="field prepend-icon">--}}
{{--                        {!! Form::text('next_class', old('next_class',isset($student_tc['next_class']) ? $student_tc['next_class'] : ''), ['class' => 'gui-input ','id' => 'next_class','readonly'=>true]) !!}--}}
{{--                        <label for="next_class" class="field-icon">--}}
{{--                            <i class="fa fa-calendar-o"></i>--}}
{{--                        </label>--}}
{{--                    </label>--}}
{{--                    @if ($errors->has('next_class'))--}}
{{--                        <p class="help-block">{{ $errors->first('next_class') }}</p>--}}
{{--                    @endif--}}
{{--                </div>--}}
                <div class="col-md-3">
                    <label><span class="radioBtnpan">TC Status<span class="asterisk">*</span></span></label>
                    <div class="section">
                        <label class="field select">
                            {!!Form::select('tc_status', $student_tc['arr_tc_status'],isset($student_tc['tc_status']) ? $student_tc['tc_status'] : 1, ['class' => 'form-control','id'=>'tc_status'])!!}
                            <i class="arrow double"></i>
                        </label>
                        @if ($errors->has('tc_status'))
                            <p class="help-block">{{ $errors->first('tc_status') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Student Conduct<span class="asterisk">*</span></span></label>
                    <label for="student_conduct" class="field prepend-icon">
                        {!! Form::text('student_conduct', old('student_conduct',isset($student_tc['student_conduct']) ? $student_tc['student_conduct'] : ''), ['class' => 'gui-input', 'id' => 'student_conduct']) !!}
                        <label for="student_conduct" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('student_conduct'))
                        <p class="help-block">{{ $errors->first('student_conduct') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Total Working Days</span></label>
                    <label for="religion" class="field prepend-icon">
                        {!! Form::text('total_working_day', old('total_working_day',isset($student_tc['total_working_day']) ? $student_tc['total_working_day'] : ''), ['class' => 'gui-input', 'id' => 'total_working_day']) !!}
                        <label for="total_working_day" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('total_working_day'))
                        <p class="help-block">{{ $errors->first('total_working_day') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Total Attendance</span></label>
                    <label for="religion" class="field prepend-icon">
                        {!! Form::text('total_attendance', old('total_attendance',isset($student_tc['total_attendance']) ? $student_tc['total_attendance'] : ''), ['class' => 'gui-input', 'id' => 'total_attendance']) !!}
                        <label for="total_attendance" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('total_attendance'))
                        <p class="help-block">{{ $errors->first('total_attendance') }}</p>
                    @endif
                </div>
{{--                <div class="col-md-7">--}}
{{--                    <span class="radioBtnpan">Remarks</span>--}}
{{--                    <label for="remarks" class="field prepend-icon">--}}
{{--                        {!! Form::text('remarks', old('remarks',isset($student_tc['remarks']) ? $student_tc['remarks'] : ''), ['class' => 'gui-input ','id' => 'remarks','readonly'=>true]) !!}--}}
{{--                        <label for="remarks" class="field-icon">--}}
{{--                            <i class="fa fa-calendar-o"></i>--}}
{{--                        </label>--}}
{{--                    </label>--}}
{{--                    @if ($errors->has('remarks'))--}}
{{--                        <p class="help-block">{{ $errors->first('remarks') }}</p>--}}
{{--                    @endif--}}
{{--                </div>--}}
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-12">
                    <span class="radioBtnpan">Reason of TC<span class="asterisk">*</span></span></span>
                    <label for="tc_reason" class="field prepend-icon">
                        {!! Form::text('tc_reason', old('tc_reason',isset($student_tc['tc_reason']) ? $student_tc['tc_reason'] : ''), ['class' => 'gui-input ','id' => 'tc_reason']) !!}
                        <label for="tc_reason" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('tc_reason'))
                        <p class="help-block">{{ $errors->first('tc_reason') }}</p>
                    @endif
                </div>
            </div>
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit('Save', ['class' => 'button btn-primary','name'=>'save']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
</div>
@include('backend.partials.popup')
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#student-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                enrollment_number: {
                    required: true
                },
                student_name: {
                    required: true,
                },
                class_id: {
                    required: true,
                },
                student_id: {
                    required: true,
                },
                admission_class_name: {
                    required: true,
                },
                last_class: {
                    required: true,
                },
                tc_date: {
                    required: true
                },
                tc_application_date: {
                    required: true
                },
                print_tc_number: {
                    required: true
                },
                admission_date: {
                    required: true
                },
                final_date: {
                    required: true
                },
                exam_result: {
                    required: true
                },
                student_conduct: {
                    required: true
                },
                tc_number: {
                    required: true
                },
                tc_reason: {
                    required: true
                },
                tc_status: {
                    required: true
                },
                section_id: {
                    required: true,
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        $("#tc_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: '{!! $student_tc["session"]["session_start_date"] !!}',
            maxDate: '{!! $student_tc["session"]["session_end_date"] !!}'
        });

        $('#tc_date').datepicker().on('change', function (ev) {
            $(this).valid();
        });

        $("#tc_application_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: '{!! $student_tc["session"]["session_start_date"] !!}',
            maxDate: '{!! $student_tc["session"]["session_end_date"] !!}'
        });

        $("#final_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
        });


        $('#tc_application_date').datepicker().on('change', function (ev) {
            $(this).valid();
        });

        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || !/[~`!@#$%\^&*()+=\-\_\.\[\]\\';,/{}|\\":<>\?]/.test(value) || /^[a-zA-Z][a-zA-Z0-9\s\.\-\_]+$/i.test(value);
        }, "Please enter valid data, except spacial characters");
        // get enquire data 


    });

    $('#class_id').on('change', function (e) {
        e.preventDefault();
        $('select[name="section_id"]').empty();
        $('select[name="section_id"]').append('<option value="">--Select section--</option>');
        var class_id = $(this).val();
        getClassSetcion(class_id);

    });

    function getClassSetcion(class_id) {
        clearFields();
        if (class_id !== '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-section-list')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'class_id': class_id,
                    'session_id': $("#session_id").val(),
                },
                beforeSend: function () {
                    $("#LoadingImage").show();
                },
                success: function (response) {
                    var resopose_data = [];
                    $("#LoadingImage").hide();
                    resopose_data = response.data;
                    if (response.status == 'success') {
                        $.each(resopose_data, function (key, value) {
                            $('select[name="section_id"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    } else {
                        return false;
                    }
                }
            });
        }
    }

    $('#section_id').on('change', function (e) {
        e.preventDefault();
        clearFields();
        getStudentList();
    });

    function getStudentList() {
        var section_id = $("#section_id").val();
        var class_id = $("#class_id").val();
        var session_id = $("#session_id").val();
        $('select[name="student_id"]').empty();
        $('select[name="student_id"]').append('<option value="">--Select Student--</option>');
        if (section_id !== '' && class_id !== '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-student-list')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'section_id': section_id,
                    'class_id': class_id,
                    'session_id': session_id,
                },
                beforeSend: function () {
                    $("#LoadingImage").show();
                },
                success: function (response) {
                    $("#LoadingImage").hide();
                    if (response.status === 'success') {
                        $.each(response.data, function (key, value) {
                            $('select[name="student_id"]').append('<option value="' + key + '">' + value + '</option>');

                        });
                    }
                }
            });
        } else {
        }
    }

    $("#student_id").select2();
    // onchange student event
    $("#student_id").select2().on('change', function (e) {
        var student_id = $(this).val();
        $('#submit_form').prop('disabled', true);
        if (student_id !== '') {
            getStudentDetail();
        }
    });

    function getStudentDetail() {
        var section_id = $("#section_id").val();
        var class_id = $("#class_id").val();
        var session_id = $("#session_id").val();
        var student_id = $("#student_id").val();
        clearFields();
        if (section_id !== '' && class_id !== '' && student_id !== '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-student-details')}}",
                datatType: 'json',
                type: 'GET',
                data: {
                    'section_id': section_id,
                    'class_id': class_id,
                    'session_id': session_id,
                    'student_id': student_id,
                },
                beforeSend: function () {
                    $("#LoadingImage").show();
                },
                success: function (response) {
                    $("#LoadingImage").hide();
                    if (response.status === 'success') {
                        $("#last_session").val(response.data.session_year);
                        $("#student_name").val(response.data.student_name);
                        $("#father_name").val(response.data.father_name);
                        $("#mother_name").val(response.data.mother_name);
                        $("#dob").val(response.data.dob);
                        $("#enrollment_number").val(response.data.enrollment_number);
                        $("#admission_date").val(response.data.admission_date);
                        $("#religion").val(response.data.religion);
                        $("#admission_class_name").val(response.data.admission_class_name);
                        $("#last_class").val(response.data.previous_class_name);
                        $('select[name="status"]').val(response.data.student_promote_id);
                        // $("#remarks").val('Studing in Class ' + response.data.class_name);

                        new PNotify({
                            title: 'Student Data',
                            text: 'Student data found successfully',
                            type: 'success',
                            width: '290px',
                            delay: 1400
                        });
                    } else {
                        new PNotify({
                            title: 'Student Data',
                            text: 'Somthing went wrong, please try again ',
                            type: 'error',
                            width: '290px',
                            delay: 1400
                        });
                    }

                }
            });
        } else {
        }
    }

    function clearFields() {
        $("#student_name").val('');
        $("#father_name").val('');
        $("#mother_name").val('');
        $("#enrollment_number").val('');
        $("#last_class").val('');
        $("#last_school").val('');
        $("#next_class").val('');
        $("#dob").val('');
        $("#jin_date").val('');
        $("#tc_date").val('');
        $("#tc_reason").val('');
        $("#remarks").val('');
    }
</script>
