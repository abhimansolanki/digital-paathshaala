<style>
    table{
        border: 1px solid black;
        border-collapse: collapse;
    }
    .border-bottom-dash{
        border-bottom:dashed 1px black !important;
    }
    .border-bottom-solid{
        border-top:solid 1px black !important;
    }
</style>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Student TC</title>
</head>
<div class="row">
    @php 
    $school = get_school_data();
    $session = get_current_session();
    @endphp
    <div class="col-md-6" style="padding: 0% 0 5% 3%; float: left;">
        <div class="col-md-12" style="padding:10px;">
	<table cellpadding="5">
	    <thead>
	        <tr class="border-bottom-solid">
		<td rowspan="5" colspan="2"><img src="{{ url(get_school_logo())}}" style="height:30%; width: 60%"></td>
		<td style="text-transform: capitalize; color: red; border:solid 1px black;">TRANSFER CERTIFICATE</td>

	        </tr>
	        <tr>
		<td> Year :- {!! $session['session_year'] !!}</td>
	        </tr>
	        <tr>
		<td>{!! $school['address'] !!}</td>
	        </tr>

	        <tr>
		<td>{!! $school['contact_number'] !!}</td>
	        </tr>
	        <tr>
		<td>Registration No:- {!! $school['registration_number'] !!}</td>
	        </tr>
	    </thead>
	    <tbody>
	        <tr>
		<td class="border-bottom-solid">TC No <span>{!! $tc_data['tc_number'] !!}</span></td>
		<td  colspan="2" class="border-bottom-dash border-bottom-solid">{!! $tc_data['tc_number'] !!}</td>

	        </tr>
	        <tr>
		<td>CERTIFIED THAT</td>
		<td  colspan="2" class="border-bottom-dash">{!! $tc_data['student_name'] !!}</td>
	        </tr>

	        <tr>
		<td>FATHER NAME :-</td>
		<td  colspan="2" class="border-bottom-dash">{!! $tc_data['father_name'] !!}</td>
	        </tr>
	        <tr>
		<td>MOTHER NAME :-</td>
		<td  colspan="2" class="border-bottom-dash">{!! $tc_data['mother_name'] !!}</td>
	        </tr>
	        <tr>
		<td>RESIDENT OF :-</td>
		<td colspan="2" class="border-bottom-dash">{!! $tc_data['address_line1'] !!}</td>
	        </tr>
	        <tr>
		<td>DATE OF BIRTH (In Words) :-</td>
		<td colspan="2" class="border-bottom-dash">{!! covert_dob_words($tc_data['dob']) !!}</td>
	        </tr>
	        <tr>
		<td>(In Figures) :-</td>
		<td colspan="2" class="border-bottom-dash">{!! $tc_data['dob'] !!}</td>
	        </tr>
	        <tr>
		<td>JOINED THAT SCHOOL IN CLASS :-</td>
		<td colspan="2" class="border-bottom-dash">{!! $tc_data['previous_class_name'] !!}</td>
	        </tr>
	        <tr>
		<td>ON DATE :-</td>
		<td colspan="2" class="border-bottom-dash">{!! $tc_data['join_date'] !!}</td>
	        </tr>
	        <tr>
		<td>VIDE ADMISSION NO :-</td>
		<td colspan="2" class="border-bottom-dash">{!! $tc_data['enrollment_number'] !!}</td>
	        </tr>
	        <tr>
		<td>AND LEFT FROM CLASS :-</td>
		<td colspan="2" class="border-bottom-dash">{!! $tc_data['class_name'] !!}</td>
	        </tr>
	        <tr>
		<td>ON DATE :-</td>
		<td colspan="2" class="border-bottom-dash">{!! $tc_data['tc_date'] !!}</td>
	        </tr>
	        <tr>
		<td>IN ORDER TO :-</td>
		<td colspan="2" class="border-bottom-dash">{!! $tc_data['tc_reason'] !!}</td>
	        </tr>
	        <tr>
		<td colspan="3">HIS/HER CONDUCT AS FAR AS KNOWN TO THE UNDERSIGNED WAS GOOD</td>
	        </tr>
	        <tr>
		<td colspan="3">TO THE UNDERSIGNED WAS GOOD</td>
	        </tr>
	        <tr>
		<td>Date of Issue of Certificate
		    <br>
		    {!! $tc_data['tc_date'] !!}
		</td>
		<td colspan="2" style="text-align: right;">Headmaster Signature</td>
	        </tr>
	    </tbody>
	</table>
        </div>
    </div>
</div>
<style>
    @page { size: auto;  margin: 20mm; }
    .pagebreak { page-break-before: always; }
    .sig_row{
        border-right: 0px; text-align: center;
    }
</style>
