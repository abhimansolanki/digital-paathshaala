@extends('admin_panel/layout')
<style>
    .bootstrap-tagsinput {
        width: 100%;
    }
</style>
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                </div>
                {!! Form::open(['files'=>TRUE,'id' => 'user-role-form' , 'class'=>'form-horizontal','url' => $save_url,'autocomplete'=>'off']) !!}
                @include('backend.user-role._form',['submit_button' =>$submit_button])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="contactModal" 
     tabindex="-1" role="dialog" 
     aria-labelledby="contactModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush
