@extends('admin_panel/layout')
@section('content')
    <style>
        #deletebtn {
            margin-top: -14px !important;
        }

        .btn:hover, .btn:focus, .btn:active {
            border: none !important;
        }
    </style>
    <div class="admin-form theme-primary" style="padding-bottom: 175px;" id="CustomInput">
        @include('backend.partials.messages')
        @include('backend.partials.loader')
        <div class="tray tray-center tableCenter">
            <div class="">
                <div class="panel panel-visible" id="spy2">
                    <div class="panel heading-border">
                        <div id="topbar">
                            <div class="topbar-left">
                                <ol class="breadcrumb">
                                    <li class="crumb-active">
                                        <a href="{{url('admin/dashboard')}}"> <span
                                                    class="glyphicon glyphicon-home"></span> Home</a>
                                    </li>
                                    <li class="crumb-trail">{{ trans('language.e_class_room')}}</li>
                                </ol>
                            </div>
                        </div>
                        <div class="panel-body pn">
                            <div class="row">
                                <br>
                                {!! Form::open(['id'=>'class-fee-from']) !!}
                                <div class="col-md-2">
                                    <label><span class="radioBtnpan">{{ trans('language.academic_year')}}<span
                                                    class="asterisk">*</span></span></label>
                                    <label for="session_id" class="field prepend-icon">
                                        <label class="field select">
                                            {!!Form::select('session_id', $arr_session, $session['session_id'], ['class' => 'form-control','id'=>'session_id', 'disabled' => true])!!}
                                            <i class="arrow"></i>
                                        </label>
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label><span class="radioBtnpan">{{ trans('language.class')}}<span class="asterisk">*</span></span></label>
                                    <label for="class_id" class="field prepend-icon">
                                        <label class="field select">
                                            {!!Form::select('class_id', $arr_class,old('class_id'), ['class' => 'form-control','id'=>'class_id'])!!}
                                            <i class="arrow"></i>
                                        </label>
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label><span class="radioBtnpan">{{ trans('language.section')}}<span
                                                    class="asterisk">*</span></span></label>
                                    <div class="section_id" class="field prepend-icon">
                                        <label class="field select">
                                            {!!Form::select('section_id', ['' => '--Select Section--'], old('section_id'), ['class' => 'form-control','id'=>'section_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label><span class="radioBtnpan"></span></label>
                                    <label class="field select">
                                        {!! Form::button('Search', ['class' => 'button btn-primary search-button']) !!}
                                    </label>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="row" style="margin-top: 30px;">
                                <div id="class-subject-data"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.partials.popup')
    <script>
        $(document).ready(function () {
            $(document).on('change', '#class_id', function (e) {
                e.preventDefault();
                $('select[name="section_id"]').empty();
                $('select[name="section_id"]').append('<option value="">--Select Section--</option>');
                getClassSection();
            });

            $(document).on('click', '.search-button', function (e) {
                e.preventDefault();
                getClassData();
            });
        });

        function getClassSection() {
            var class_id = $("#class_id").val();
            if (class_id !== '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status == 'success') {
                            $.each(resopose_data, function (key, value) {
                                $('select[name="section_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        } else {
                            return false;
                        }
                    }
                });
            }
        }

        function getClassData() {
            var session_id = $('#session_id').val();
            var class_id = $('#class_id').val();
            var section_id = $('#section_id').val();
            var subject_id = $('#subject_id').val();
            $('#class-subject-data').html('');
            if (session_id !== '' && class_id !== '' && subject_id !== '' && section_id !== '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('e-class-room/get-class-data')}}",
                    datatType: 'json',
                    type: 'GET',
                    data: {
                        'session_id': session_id,
                        'class_id': class_id,
                        'section_id': section_id,
                        'subject_id': subject_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        console.log('start');
                        $("#LoadingImage").hide();
                        $("#class-subject-data").html(response);
                        console.log('end');
                    }
                });
            } else {
                $("#class-subject-data").html('<div class="text-center">No records found</div>');
            }
        }

        function hideModal(modal) {
            $("#"+modal).removeClass("in");
            $(".modal-backdrop").remove();
            $('body').removeClass('modal-open');
            $('body').css('padding-right', '');
            $("#"+modal).hide();
        }
    </script>
    </body>
    </html>
@endsection