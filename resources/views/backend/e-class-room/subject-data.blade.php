<style type="text/css">
    .table > thead > tr > th {
        font-weight: bold;
        font-size: 14px !important;
    }

    .table {
        background-color: #f3f3f3;
    }

    tr td {
        font-weight: bold;
    }
</style>
<form class="form-horizontal" method="GET" action="{{url("e-class-room/save")}}">
    <table class="table table-bordered table-hover" id="chapter_table" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th class="text-center" width="8%">{{trans('language.sr_no')}}</th>
            <th class="text-center">{{trans('language.subject_name')}}</th>
            <th class="text-center">{{trans('language.teacher_name')}}</th>
            <th class="text-center">{{trans('language.teacher_meeting_id')}}</th>
            <th class="text-center">{{trans('language.start_time')}}</th>
            <th class="text-center">{{trans('language.end_time')}}</th>
            <th class="text-center">{{trans('language.meeting_days')}}</th>
        </tr>
        </thead>
        <tbody>

        @php $sr = 1; @endphp
        @php $i = 1; @endphp
        @foreach($arr_subject_data as $subject)
            <tr class="student_information">
                <td class="text-center">
                    {{ $sr++ }}
                </td>
                <td class="text-center">
                    {{ $subject['subject_name'] }}
                    {!!Form::hidden('data['.$i.'][e_class_room_id]',$subject['e_class_room_id'], ['class' => 'form-control','id'=>'e_class_room_id'])!!}
                    {!!Form::hidden('data['.$i.'][subject_id]',$subject['subject_id'], ['class' => 'form-control','id'=>'subject_id'])!!}
                    {!!Form::hidden('data['.$i.'][session_id]',$subject['session_id'], ['class' => 'form-control','id'=>'session_id'])!!}
                    {!!Form::hidden('data['.$i.'][class_id]',$subject['class_id'], ['class' => 'form-control','id'=>'class_id'])!!}
                    {!!Form::hidden('data['.$i.'][section_id]',$subject['section_id'], ['class' => 'form-control','id'=>'section_id'])!!}
                </td>
                <td class="text-center">
                    {!!Form::text('data['.$i.'][teacher_name]',old('teacher_name', $subject['teacher_name']), ['class' => 'form-control','id'=>'teacher_name','placeholder'=>trans('language.teacher_name')])!!}
                </td>
                <td class="text-center">
                    {!!Form::text('data['.$i.'][meeting_link]',old('meeting_link', $subject['meeting_link']), ['class' => 'form-control','id'=>'meeting_link','placeholder'=>trans('language.teacher_meeting_id')])!!}
                </td>
                <td class="text-center">
                    <label for="data[{{$i}}][meeting_start_time]" class="field prepend-icon">
                        {!!Form::time('data['.$i.'][meeting_start_time]',old('meeting_start_time', $subject['meeting_start_time']), ['class' => 'gui-input','id'=>'meeting_start_time','placeholder'=>trans('language.start_time')])!!}
                        <label for="data[{{$i}}][meeting_start_time]" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                </td>
                <td class="text-center">
                    <label for="data[{{$i}}][meeting_end_time]" class="field prepend-icon">
                        {!!Form::time('data['.$i.'][meeting_end_time]',old('meeting_end_time', $subject['meeting_end_time']), ['class' => 'gui-input','id'=>'meeting_end_time','placeholder'=>trans('language.end_time')])!!}
                        <label for="data[{{$i}}][meeting_end_time]" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                </td>
                <td class="text-center">
                    @foreach($days as $key => $day)
                        <div class="col-sm-4">
                            <input type="checkbox" class="form-check-input" id="{{$day}}"
                                   @if(in_array($key, $subject['meeting_day'])) checked @endif
                                   name="data[{{$i}}][meeting_day][{{$key}}]">
                            <label class="form-check-label" for="{{$day}}">
                                {{$day}}
                            </label>
                        </div>
                    @endforeach
                </td>
            </tr>
            @php $i++; @endphp
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-1 pull-right">
            <label><span class="radioBtnpan"></span></label>
            <label class="field select">
                {!! Form::submit('Submit', ['class' => 'button btn-primary submit-form']) !!}
            </label>
        </div>
    </div>
</form>
