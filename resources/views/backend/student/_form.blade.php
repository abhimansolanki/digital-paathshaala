<style type="text/css">
    .admin-form .select{
        margin-top: 0px;
    }
    .select2-container .select2-selection--single{
        height: 35px !important;
    }
    .admin-form .select>select{
        height: 35px !important;
    }

    .state-error{
        display:block !important;
        margin-top: 6px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    }
    .fileupload-preview{
        line-height: 70px !important;
    }
</style>


<!-- Field Options -->
@if(isset($student['student_id']) && !empty($student['student_id']))
<?php
    $readonly = true;
    $disabled = 'disabled';
?>
@else
<?php
    $readonly = false;
    $disabled = '';
?>
@endif
<style type="text/css">
    .admin-form .file .button{
        top: 0px !important;
        right:0px !important;
    }
</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_student')}}</li>
                </ol>
            </div>
            <div class="topbar-right">
                <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right" title="View Student">View Student</a>
            </div>
        </div>
        {{-- @if(Session::has('success')) --}}
        @include('backend.partials.messages')
        {{-- @endif --}}
        <div class="bg-light panelBodyy">
            {!! Form::hidden('student_id',old('student_id',isset($student['student_id']) ? $student['student_id'] : ''),['class' => 'gui-input', 'id' => 'student_id', 'readonly' => 'true']) !!}
            {!! Form::hidden('student_parent_id',old('student_parent_id',isset($student['student_parent_id']) ? $student['student_parent_id'] : ''),['class' => 'gui-input', 'id' => 'student_parent_id', 'readonly' => 'true']) !!}
            <div class="section row" id="spy1">
                <div class="col-md-2">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-preview thumbnail mb20">
                            @if(isset($student['profile']) && !empty($student['profile']))
                            <img src="{{url($student['profile'])}}"  height="100px" width="100px">
                            @else
                            <img data-src="holder.js/93%x160" alt="holder">
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <span class="btn btn-system btn-file btn-block">
                                    <span class="fileupload-new">Select</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input type="file" class="gui-file" name="profile_photo" accept="image/*" id="file1" onChange="document.getElementById('profile_photo').value = this.value;">
                                    @if ($errors->has('profile_photo')) 
                                    <p class="help-block">{{ $errors->first('profile_photo') }}</p>
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.enrollment_number')}}<span class="asterisk">*</span></span></label>
                    <label for="enrollment_number" class="field prepend-icon">
{{--                        ,'readonly' => true--}}
                        {!! Form::text('enrollment_number', old('enrollment_number',isset($student['enrollment_number']) ? $student['enrollment_number'] : ''), ['class' => 'gui-input', 'id' => 'enrollment_number']) !!}
                        <label for="enrollment_number" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('enrollment_number')) 
                    <p class="help-block">{{ $errors->first('enrollment_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        <label><span class="radioBtnpan">{{trans('language.form_number')}}</span></label>
                        <label for="form_number" class="field prepend-icon">
                            {!! Form::text('form_number', old('form_number',isset($student['form_number']) ? $student['form_number'] : ''), ['class' => 'gui-input',''=>trans('language.form_number'), 'id' => 'form_number','readonly' => $readonly]) !!}
                            <label for="form_number" class="field-icon">
                                <i class="fa fa-user"></i>
                            </label>
                        </label>
                        @if ($errors->has('form_number')) 
                        <p class="help-block">{{ $errors->first('form_number') }}</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <br>
                        {!! Form::button('Search', ['class' => 'gui-input button btn-primary','id' => 'search_enquire_data',$disabled]) !!}
                    </div>
                    <div class="col-md-6">
                        <br>
                        {!! Form::reset('Reset', ['class' => 'gui-input button btn-primary','id' => 'reset_data',$disabled]) !!}
                    </div>
                </div>
            </div>
            <div class="section-divider mv40">
                <span><i class="fa fa-graduation-cap"></i> Academic details</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $student['arr_session'],isset($student['current_session_id']) ? $student['current_session_id'] : null, ['class' => 'form-control','id'=>'session_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('session_id')) 
                    <p class="help-block">{{ $errors->first('session_id') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Admission Class name</span></label>
                    <label for="admission_class_name" class="field prepend-icon">
                        {!! Form::text('admission_class_name', old('admission_class_name',isset($student['class_name']) ? $student['class_name'] : ''), ['class' => 'gui-input',''=>trans('language.class_name'), 'id' => 'class_name']) !!}
                        <label for="admission_class_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('class_name')) 
                    <p class="help-block">{{ $errors->first('class_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Select Class<span class="asterisk">*</span></span></label>
                    <div class="section">
                        <label class="field select">
                            {!!Form::select('current_class_id', $student['arr_class'],isset($student['current_class_id']) ? $student['current_class_id'] : '', ['class' => 'form-control','id'=>'current_class_id'])!!}
                            <i class="arrow double"></i>
                        </label>
                        @if ($errors->has('current_class_id')) 
                        <p class="help-block">{{ $errors->first('current_class_id') }}</p>
                        @endif                            
                    </div>
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Avail: &nbsp;<span id="available"></span>&nbsp;</span></label>
                    <label><span class="radioBtnpan">&nbsp;Regi:<span id="admission"></span>&nbsp;</span></label>
                    <label><span class="radioBtnpan">&nbsp;Strength:<span id="strength"></span></span></label>
                    <div class="section">
                        <label class="field select">
                            {!!Form::select('current_section_id', $student['arr_section'],isset($student['current_section_id']) ? $student['current_section_id'] : '', ['class' => 'form-control','id'=>'current_section_id'])!!}
                            <i class="arrow double"></i>
                        </label>
                        @if ($errors->has('current_section_id')) 
                        <p class="help-block">{{ $errors->first('current_section_id') }}</p>
                        @endif                           
                    </div>
                </div>

            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Admission Date<span class="asterisk">*</span></span></label>
                    <label for="admission_date" class="field prepend-icon">
                        {!! Form::text('admission_date', old('admission_date',isset($student['admission_date']) ? $student['admission_date'] : ''), ['class' => 'gui-input','id' => 'admission_date', 'readonly' => 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('admission_date')) 
                    <p class="help-block">{{ $errors->first('admission_date') }}</p>
                    @endif
                </div>

                <div class="col-md-3">
                    <label><span class="radioBtnpan">Join Date<span class="asterisk">*</span></span></label>
                    <label for="join_date" class="field prepend-icon">
                        {!! Form::text('join_date', old('join_date',isset($student['join_date']) ? $student['join_date'] : ''), ['class' => 'gui-input', 'id' => 'join_date', 'readonly' => 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('join_date')) 
                    <p class="help-block">{{ $errors->first('join_date') }}</p>
                    @endif
                </div>
            </div>
	
            <div class="section-divider mv40">
                <span><i class="fa fa-user"></i> Student Details</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.first_name')}}<span class="asterisk">*</span></span></label>
                    <label for="first_name" class="field prepend-icon">
                        {!! Form::text('first_name', old('first_name',isset($student['first_name']) ? $student['first_name'] : ''), ['class' => 'gui-input',''=>trans('language.first_name'), 'id' => 'first_name']) !!}
                        <label for="first_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('first_name')) 
                    <p class="help-block">{{ $errors->first('first_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.middle_name')}}</span></label>
                    <label for="middle_name" class="field prepend-icon">
                        {!! Form::text('middle_name', old('middle_name',isset($student['middle_name']) ? $student['middle_name'] : ''), ['class' => 'gui-input',''=>trans('language.middle_name'), 'id' => 'middle_name']) !!}
                        <label for="middle_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('middle_name')) 
                    <p class="help-block">{{ $errors->first('middle_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.last_name')}}</span></label>
                    <label for="last_name" class="field prepend-icon">
                        {!! Form::text('last_name', old('last_name',isset($student['last_name']) ? $student['last_name'] : ''), ['class' => 'gui-input',''=>trans('language.last_name'), 'id' => 'last_name']) !!}
                        <label for="last_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('last_name')) 
                    <p class="help-block">{{ $errors->first('last_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.gender')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('gender', $student['arr_gender'],isset($student['gender']) ? $student['gender'] : '', ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('gender')) 
                    <p class="help-block">{{ $errors->first('gender') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.dob')}}</span></label>
                    <label for="datepicker1" class="field prepend-icon">
                        {!! Form::text('dob', old('dob',isset($student['dob']) ? $student['dob'] : ''), ['class' => 'gui-input dateofbirthseprate','id' => 'dob', 'readonly' => 'readonly']) !!}
                    </label>
                </div>
                <div class="col-md-3">
                    @include('backend.partials.get-age')
                </div>
                <div class="col-md-3">
                    <span class="radioBtnpan">{{trans('language.caste_category')}}<span class="asterisk">*</span></span>
                    <label for="caste_category_id" class="field select">
                        {!!Form::select('caste_category_id', $student['arr_caste_category'],isset($student['caste_category_id']) ? $student['caste_category_id'] : '', ['class' => 'form-control','id'=>'caste_category_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('caste_category_id')) 
                    <p class="help-block">{{ $errors->first('caste_category_id') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <span class="radioBtnpan">{{trans('language.new_student')}}<span class="asterisk">*</span></span>
                    <label for="new_student" class="field select">
                        {!!Form::select('new_student', $student['arr_new_student'],isset($student['new_student']) ? $student['new_student'] : '', ['class' => 'form-control','id'=>'new_student'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('new_student')) 
                    <p class="help-block">{{ $errors->first('new_student') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <span class="radioBtnpan">{{trans('language.aadhaar_number')}}</span>
                    <label for="aadhaar_number" class="field prepend-icon">
                        {!! Form::number('aadhaar_number', old('aadhaar_number',isset($student['aadhaar_number']) ? $student['aadhaar_number'] : ''), ['class' => 'gui-input ','id' => 'aadhaar_number','pattern'=>'[0-9]*']) !!}
                        <label for="aadhaar_number" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('aadhaar_number')) 
                    <p class="help-block">{{ $errors->first('aadhaar_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <span class="radioBtnpan">{{trans('language.birth_place')}}</span>
                    <label for="birth_place" class="field prepend-icon">
                        {!! Form::text('birth_place', old('birth_place',isset($student['birth_place']) ? $student['birth_place'] : ''), ['class' => 'gui-input ',''=>trans('language.aadhaar_number'), 'id' => 'birth_place']) !!}
                        <label for="birth_place" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('birth_place')) 
                    <p class="help-block">{{ $errors->first('birth_place') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <span class="radioBtnpan">{{trans('language.care_of')}}<span class="asterisk">*</span></span>
                    <label for="care_of" class="field select">
                        {!!Form::select('care_of',$student['arr_care_of'],isset($student['care_of']) ? $student['care_of'] : '', ['class' => 'form-control','id'=>'care_of'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('care_of')) 
                    <p class="help-block">{{ $errors->first('care_of') }}</p>
                    @endif
                </div>
              <div class="col-md-3">
                    <span class="radioBtnpan">{{trans('language.religion')}}</span>
                    <label for="religion" class="field prepend-icon">
                        {!! Form::text('religion', old('religion',isset($student['religion']) ? $student['religion'] : ''), ['class' => 'gui-input ', 'id' => 'religion']) !!}
                        <label for="religion" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('birth_place')) 
                    <p class="help-block">{{ $errors->first('religion') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.address_line1')}}<span class="asterisk">*</span></span></label>
                    <label for="address_line1" class="field prepend-icon">
                        {!! Form::textarea("address_line1", old('address_line1',isset($student['address_line1']) ? $student['address_line1'] : ''), ["class" => "gui-input", "id" => "address_line1"]) !!}
                        <label for="address_line1" class="field-icon">
                            <span class="glyphicons glyphicons-google_maps"></span>
                        </label>
                    </label>
                    @if ($errors->has('address_line1')) 
                    <p class="help-block">{{ $errors->first('address_line1') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <input type="checkbox" class="" id="copyAddress" name="ifaddressCopy" onclick="AddressCopy(this.form)">
                    <span style="font-size:12px;">(Copy address)</span>
                    <label><span class="radioBtnpan">{{trans('language.address_line2')}}</span></label>
                    <label for="address_line2" class="field prepend-icon">
                        {!! Form::textarea("address_line2", old('address_line2',isset($student['address_line2']) ? $student['address_line2'] : ''), ["class" => "gui-input","id" => "address_line2"]) !!}
                        <label for="address_line2" class="field-icon">
                            <span class="glyphicons glyphicons-google_maps"></span>
                        </label>
                    </label>
                    @if ($errors->has('address_line2')) 
                    <p class="help-block">{{ $errors->first('address_line2') }}</p>
                    @endif
                </div>
            </div>

	<div class="section row" id="spy1">
	    @foreach($student['student_document'] as $key=> $doc)
	    @php $checked = false; @endphp
	    @if(!empty($student['student_doc']) && in_array($key,$student['student_doc']))
	    @php $checked = true; @endphp
	    @endif
	    <div class="col-md-3">
	        <div class="option-group field" id="checkBooxx">
		    <label class="option block option-primary">
		        {!! Form::checkbox('student_doc[]',$key,$checked, ['class' => 'gui-input checked_all', 'id' =>$key]) !!}
		        <span class="checkbox radioBtnpan"></span><em>{!! $doc !!}</em>
		    </label>
	        </div>    
                </div> 
	    @endforeach
	</div>
            <div class="section-divider mv40">
                <span><i class="fa fa-graduation-cap"></i> Last School's details</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.previous_school_name')}}</span></label>
                    <label for="previous_school_name" class="field prepend-icon">
                        {!! Form::text('previous_school_name', old('previous_school_name',isset($student['previous_school_name']) ? $student['previous_school_name'] : ''), ['class' => 'gui-input  ',''=>trans('language.previous_school_name'), 'id' => 'previous_school_name']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-book"></i>
                        </label>
                    </label>
                    @if ($errors->has('previous_school_name')) 
                    <p class="help-block">{{$errors->first('previous_school_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.previous_class_name')}}</span></label>
                    <label for="previous_school_name" class="field prepend-icon">
                        {!! Form::text('previous_class_name', old('previous_class_name',isset($student['previous_class_name']) ? $student['previous_class_name'] : ''), ['class' => 'gui-input  ',''=>trans('language.previous_class_name'), 'id' => 'previous_class_name']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-book"></i>
                        </label>
                    </label>
                    @if($errors->has('previous_class_name')) 
                    <p class="help-block">{{ $errors->first('previous_school_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.tc_number')}}</span></label>
                    <label for="tc_number" class="field prepend-icon">
                        {!! Form::text('tc_number', old('tc_number',isset($student['tc_number']) ? $student['tc_number'] : ''), ['class' => 'gui-input ', 'id' => 'tc_number','pattern'=>'[a-zA-Z0-9]*']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-book"></i>
                        </label>
                    </label>
                    @if ($errors->has('tc_number')) 
                    <p class="help-block">{{ $errors->first('tc_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.tc_date')}}</span></label>
                    <label for="tc_date" class="field prepend-icon">
                        {!! Form::text('tc_date', old('tc_date',isset($student['tc_date']) ? $student['tc_date'] : ''), ['class' => 'gui-input date_picker ',''=>trans('language.tc_date'), 'id' => 'tc_date', 'readonly' => 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('tc_date')) 
                    <p class="help-block">{{ $errors->first('tc_date') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Result (in % or grade) </span></label>
                    <label for="previous_result" class="field prepend-icon">
                        {!! Form::text('previous_result', old('previous_result',isset($student['previous_result']) ? $student['previous_result'] : ''), ['class' => 'gui-input ',''=>trans('language.result'), 'id' => 'previous_result']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-book"></i>
                        </label>
                    </label>
                    @if ($errors->has('previous_result')) 
                    <p class="help-block">{{ $errors->first('previous_result') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1" style="margin-top:2%">
                <div class="col-md-12">
                    <div class="student_fee_panel" id="spy1">
                        <div class="col-md-2 paddingclassforsibling">
                            @php $checked = false;
                            $disabled = '';
                            @endphp
                            @if(!empty($student['student_parent_id']) && $student['sibling_exist'] == 1)
                            @php $checked = true; $disabled = 'disabled'; @endphp

                            @elseif(isset($student['student_id']) && !empty($student['student_id']))
                            @php $disabled = 'disabled'; @endphp
                            @endif
                            <div class="option-group field" id="checkBooxx">
                                <label class="option block option-primary">
                                    {!! Form::checkbox('sibling_exist','',$checked, ['class' => 'gui-input ', 'id' => 'sibling_exist',$disabled,'style'=>'opacity: 0']) !!}
                                    <span class="checkbox radioBtnpan"></span><em>{{trans('language.sibling_exist')}} </em>
                                </label>
                            </div>   
                        </div>
                        <div id='multiple_student'>
                            @if(!empty($student['student_parent_id']) && $student['sibling_exist'] == 1)
                            <div class="col-md-2 text-left">
                                <a href="{{url('admin/student/sibling/'.$student['student_id'])}}"  target="_blank" style="padding:5px;">View Sibling(s)</a>
                            </div>
                            @elseif(isset($student['student_id']) && !empty($student['student_id']))
                            <div class="col-md-9 text-left"></div>
                            @else
                            <div class="col-md-1 text-left">
                                <label><span class="student_fee_panel_label"><span id="care_of_contact_text">Father</span> (C/o) </span></label>
                            </div>
                            <div class="col-md-4">
                                <label for="care_of_contact" class="field prepend-icon">
                                    {!! Form::number("care_of_contact",'', ["class" => "gui-input","id" => "care_of_contact","placeholder"=>"Contact or aadhaar number",'pattern'=>'[0-9]*']) !!}
                                    <label for="care_of_contact" class="field-icon">
                                        <i class="fa fa-eye"></i>
                                    </label>
                                </label>
                            </div>
                            <div class="col-md-1">
                                {!! Form::button('Search',['class' =>'button btn-primary','id' => 'search_parent']) !!}
                            </div> 
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="section-divider mv40">
                <span><i class="fa fa-user"></i>Father's Details</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_first_name')}}</span></label>
                    <label for="father_first_name" class="field prepend-icon">
                        {!! Form::text("father_first_name", old('father_first_name',isset($student['father_first_name']) ? $student['father_first_name'] : ''), ["class" => "gui-input",""=>trans('language.father_first_name'), "id" => "father_first_name"]) !!}
                        <label for="father_first_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_first_name')) 
                    <p class="help-block">{{ $errors->first('father_first_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_middle_name')}}</span></label>
                    <label for="father_middle_name" class="field prepend-icon">
                        {!! Form::text("father_middle_name", old('father_middle_name',isset($student['father_middle_name']) ? $student['father_middle_name'] : ''), ["class" => "gui-input",""=>trans('language.father_middle_name'), "id" => "father_middle_name"]) !!}
                        <label for="father_middle_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_middle_name')) 
                    <p class="help-block">{{ $errors->first('father_middle_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_last_name')}}</span></label>
                    <label for="father_last_name" class="field prepend-icon">
                        {!! Form::text("father_last_name", old('father_last_name',isset($student['father_last_name']) ? $student['father_last_name'] : ''), ["class" => "gui-input",""=>trans('language.father_last_name'), "id" => "father_last_name"]) !!}
                        <label for="father_last_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_last_name')) 
                    <p class="help-block">{{ $errors->first('father_last_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_contact_number')}}</span></label>
                    <label for="father_contact_number" class="field prepend-icon">
                        {!! Form::number("father_contact_number", old('father_contact_number',isset($student['father_contact_number']) ? $student['father_contact_number'] : ''), ["class" => "gui-input","id" => "father_contact_number",'pattern'=>'[0-9]*']) !!}
                        <label for="father_contact_number" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_contact_number')) 
                    <p class="help-block">{{ $errors->first('father_contact_number') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_email')}}</span></label>
                    <label for="father_email" class="field prepend-icon">
                        {!! Form::email("father_email", old('father_email',isset($student['father_email']) ? $student['father_email'] : ''), ["class" => "gui-input","id" => "father_email",'pattern'=>'^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$']) !!}
                        <label for="father_email" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_email')) 
                    <p class="help-block">{{ $errors->first('father_email') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_aadhaar_number')}}</span></label>
                    <label for="father_aadhaar_number" class="field prepend-icon">
                        {!! Form::number("father_aadhaar_number", old('father_aadhaar_number',isset($student['father_aadhaar_number']) ? $student['father_aadhaar_number'] : ''), ["class" => "gui-input","id" => "father_aadhaar_number",'pattern'=>'[0-9]*']) !!}
                        <label for="father_aadhaar_number" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_aadhaar_number')) 
                    <p class="help-block">{{ $errors->first('father_aadhaar_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_occupation')}}</span></label>
                    <label for="father_occupation" class="field prepend-icon">
                        {!! Form::text("father_occupation", old('father_occupation',isset($student['father_occupation']) ? $student['father_occupation'] : ''), ["class" => "gui-input","id" => "father_occupation"]) !!}
                        <label for="father_occupation" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_occupation')) 
                    <p class="help-block">{{ $errors->first('father_occupation') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_income')}}</span></label>
                    <label for="father_income" class="field prepend-icon">
                        {!! Form::text("father_income", old('father_income',isset($student['father_income']) ? $student['father_income'] : ''), ["class" => "gui-input",""=>"Occupation", "id" => "father_income"]) !!}
                        <label for="father_income" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_income')) 
                    <p class="help-block">{{ $errors->first('father_income') }}</p>
                    @endif
                </div>
            </div>
            <div class="section-divider mv40">
                <span><i class="fa fa-user"></i> Mother's Details</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_first_name')}}</span></label>
                    <label for="mother_first_name" class="field prepend-icon">
                        {!! Form::text("mother_first_name", old('mother_first_name',isset($student['mother_first_name']) ? $student['mother_first_name'] : ''), ["class" => "gui-input",""=>trans('language.mother_first_name'), "id" => "mother_first_name"]) !!}
                        <label for="mother_first_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_first_name')) 
                    <p class="help-block">{{ $errors->first('mother_first_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_middle_name')}}</span></label>
                    <label for="mother_middle_name" class="field prepend-icon">
                        {!! Form::text("mother_middle_name", old('mother_middle_name',isset($student['mother_middle_name']) ? $student['mother_middle_name'] : ''), ["class" => "gui-input",""=>trans('language.mother_middle_name'), "id" => "mother_middle_name"]) !!}
                        <label for="mother_middle_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_middle_name')) 
                    <p class="help-block">{{ $errors->first('mother_middle_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_last_name')}}</span></label>
                    <label for="mother_last_name" class="field prepend-icon">
                        {!! Form::text("mother_last_name", old('mother_last_name',isset($student['mother_last_name']) ? $student['mother_last_name'] : ''), ["class" => "gui-input",""=>trans('language.mother_last_name'), "id" => "mother_last_name"]) !!}
                        <label for="mother_last_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_last_name')) 
                    <p class="help-block">{{ $errors->first('mother_last_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_contact_number')}}</span></label>
                    <label for="mother_contact_number" class="field prepend-icon">
                        {!! Form::number("mother_contact_number", old('mother_contact_number',isset($student['mother_contact_number']) ? $student['mother_contact_number'] : ''), ["class" => "gui-input","id" => "mother_contact_number",'pattern'=>'[0-9]*']) !!}
                        <label for="mother_contact_number" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_contact_number')) 
                    <p class="help-block">{{ $errors->first('mother_contact_number') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_email')}}</span></label>
                    <label for="mother_email" class="field prepend-icon">
                        {!! Form::email("mother_email", old('mother_email',isset($student['mother_email']) ? $student['mother_email'] : ''), ["class" => "gui-input","id" => "mother_email",'pattern'=>'^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$']) !!}
                        <label for="mother_email" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_email')) 
                    <p class="help-block">{{ $errors->first('mother_email') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_aadhaar_number')}}</span></label>
                    <label for="mother_aadhaar_number" class="field prepend-icon">
                        {!! Form::number("mother_aadhaar_number", old('mother_aadhaar_number',isset($student['mother_aadhaar_number']) ? $student['mother_aadhaar_number'] : ''), ["class" => "gui-input", "id" => "mother_aadhaar_number",'pattern'=>'[0-9]*']) !!}
                        <label for="mother_aadhaar_number" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_aadhaar_number')) 
                    <p class="help-block">{{ $errors->first('mother_aadhaar_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_occupation')}}</span></label>
                    <label for="mother_occupation" class="field prepend-icon">
                        {!! Form::text("mother_occupation", old('mother_occupation',isset($student['mother_occupation']) ? $student['mother_occupation'] : ''), ["class" => "gui-input",""=>"Occupation", "id" => "mother_occupation"]) !!}
                        <label for="mother_occupation" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_occupation')) 
                    <p class="help-block">{{ $errors->first('mother_occupation') }}</p>
                    @endif
                </div>

                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_income')}}</span></label>
                    <label for="mother_income" class="field prepend-icon">
                        {!! Form::text("mother_income", old('mother_income',isset($student['mother_income']) ? $student['mother_income'] : ''), ["class" => "gui-input",""=>"Occupation", "id" => "mother_income"]) !!}
                        <label for="mother_income" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_income')) 
                    <p class="help-block">{{ $errors->first('mother_income') }}</p>
                    @endif
                </div>
            </div>
            <div class="section-divider mv40">
                <span><i class="fa fa-user"></i> Guardian's Details</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_first_name')}}</span></label>
                    <label for="guardian_first_name" class="field prepend-icon">
                        {!! Form::text("guardian_first_name", old('guardian_first_name',isset($student['guardian_first_name']) ? $student['guardian_first_name'] : ''), ["class" => "gui-input",""=>trans('language.guardian_first_name'), "id" => "guardian_first_name"]) !!}
                        <label for="guardian_first_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_first_name')) 
                    <p class="help-block">{{ $errors->first('guardian_first_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_middle_name')}}</span></label>
                    <label for="guardian_middle_name" class="field prepend-icon">
                        {!! Form::text("guardian_middle_name", old('guardian_middle_name',isset($student['guardian_middle_name']) ? $student['guardian_middle_name'] : ''), ["class" => "gui-input",""=>trans('language.guardian_middle_name'), "id" => "guardian_middle_name"]) !!}
                        <label for="guardian_middle_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_middle_name')) 
                    <p class="help-block">{{ $errors->first('guardian_middle_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_last_name')}}</span></label>
                    <label for="guardian_last_name" class="field prepend-icon">
                        {!! Form::text("guardian_last_name", old('guardian_last_name',isset($student['guardian_last_name']) ? $student['guardian_last_name'] : ''), ["class" => "gui-input",""=>trans('language.guardian_last_name'), "id" => "guardian_last_name"]) !!}
                        <label for="guardian_last_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_last_name')) 
                    <p class="help-block">{{ $errors->first('guardian_last_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_contact_number')}}</span></label>
                    <label for="guardian_contact_number" class="field prepend-icon">
                        {!! Form::number("guardian_contact_number", old('guardian_contact_number',isset($student['guardian_contact_number']) ? $student['guardian_contact_number'] : ''), ["class" => "gui-input","id" => "guardian_contact_number",'pattern'=>'[0-9]*']) !!}
                        <label for="guardian_contact_number" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_contact_number')) 
                    <p class="help-block">{{ $errors->first('guardian_contact_number') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_email')}}</span></label>
                    <label for="guardian_email" class="field prepend-icon">
                        {!! Form::email("guardian_email", old('guardian_email',isset($student['guardian_email']) ? $student['guardian_email'] : ''), ["class" => "gui-input","id" =>"guardian_email",'pattern'=>'^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$']) !!}
                        <label for="guardian_email" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_email')) 
                    <p class="help-block">{{ $errors->first('guardian_email') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_relation')}}</span></label>
                    <label for="guardian_relation" class="field prepend-icon">
                        {!! Form::text("guardian_relation", old('guardian_relation',isset($student['guardian_relation']) ? $student['guardian_relation'] : ''), ["class" => "gui-input","id" => "guardian_relation"]) !!}
                        <label for="guardian_relation" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_relation')) 
                    <p class="help-block">{{ $errors->first('guardian_relation') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_aadhaar_number')}}</span></label>
                    <label for="guardian_aadhaar_number" class="field prepend-icon">
                        {!! Form::number("guardian_aadhaar_number", old('guardian_aadhaar_number',isset($student['guardian_aadhaar_number']) ? $student['guardian_aadhaar_number'] : ''), ["class" => "gui-input","id" => "guardian_aadhaar_number",'pattern'=>'[0-9]*']) !!}
                        <label for="guardian_aadhaar_number" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_aadhaar_number')) 
                    <p class="help-block">{{ $errors->first('guardian_aadhaar_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_occupation')}}</span></label>
                    <label for="guardian_occupation" class="field prepend-icon">
                        {!! Form::text("guardian_occupation", old('guardian_occupation',isset($student['guardian_occupation']) ? $student['guardian_occupation'] : ''), ["class" => "gui-input",""=>trans('language.guardian_occupation'), "id" => "guardian_occupation"]) !!}
                        <label for="guardian_occupation" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_occupation')) 
                    <p class="help-block">{{ $errors->first('guardian_occupation') }}</p>
                    @endif
                </div>
            </div>
            <!--            <div class="section row" id="spy1">
                            <div class="col-md-3">
                                <label><span class="radioBtnpan">{{trans('language.guardian_income')}}</span></label>
                                <label for="guardian_income" class="field prepend-icon">
                                    {!! Form::text("guardian_income", old('guardian_income',isset($student['guardian_income']) ? $student['guardian_income'] : ''), ["class" => "gui-input","id" => "guardian_income"]) !!}
                                    <label for="guardian_income" class="field-icon">
                                        <i class="fa fa-eye"></i>
                                    </label>
                                </label>
                                @if ($errors->has('guardian_occupation')) 
                                <p class="help-block">{{ $errors->first('guardian_occupation') }}</p>
                                @endif
                            </div>
                        </div>-->
            <div class="section-divider mv40">
                <span><i class="fa fa-rupee"></i> Fee Details</span>
            </div>
            <div class="section row " id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.student_type')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('student_type_id', $student['arr_student_type'],isset($student['student_type_id']) ? $student['student_type_id'] : '', ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if($errors->has('student_type_id')) 
                    <p class="help-block">{{ $errors->first('student_type_id') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.fee_calculate_month')}}<span class="asterisk">*</span></span></label>
                    <label for="fee_calculate_month" class="field select">
                        {!!Form::select('fee_calculate_month', $student['arr_month'],isset($student['fee_calculate_month']) ? $student['fee_calculate_month'] :'' , ['class' => 'form-control','id'=>'fee_calculate_month'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('fee_calculate_month')) 
                    <p class="help-block">{{ $errors->first('fee_calculate_month') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.previous_due_fee')}}</span></label>
                    <label for="previous_due_fee" class="field prepend-icon">
                        {!! Form::number("previous_due_fee", old('previous_due_fee',isset($student['previous_due_fee']) ? $student['previous_due_fee'] : ''), ["class" => "gui-input","id" => "previous_due_fee",'readonly'=>$readonly]) !!}
                        <label for="previous_due_fee" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('previous_due_fee')) 
                    <p class="help-block">{{ $errors->first('previous_due_fee') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.remark')}}</span></label>
                    <label for="remark" class="prepend-icon">
                        {!! Form::textarea("remark", old('remarks',isset($student['remark']) ? $student['remark'] : ''), ["class" => "gui-input heightcls", "id" => "remark","rows"=>3]) !!}
                        <label for="remark" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('remark')) 
                    <p class="help-block">{{ $errors->first('remark') }}</p>
                    @endif
                </div>
                <!--                <div class="col-md-3">
                                    <label><span class="radioBtnpan">{{trans('language.fee_discount')}}</span></label>
                                    <label for="fee_discount" class="field prepend-icon">
                                        {!! Form::number("fee_discount", old('fee_discount',isset($student['fee_discount']) ? $student['fee_discount'] : ''), ["class" => "gui-input", "id" => "fee_discount",'pattern'=>'[0-9]*']) !!}
                                        <label for="fee_discount" class="field-icon">
                                            <i class="fa fa-eye"></i>
                                        </label>
                                    </label>
                                    @if ($errors->has('fee_discount')) 
                                    <p class="help-block">{{ $errors->first('fee_discount') }}</p>
                                    @endif
                                </div>-->
            </div>
            <div class="section row " id="spy1">
                <!--                <div class="col-md-3">
                                    <label><span class="radioBtnpan">{{trans('language.discount_date')}}</span></label>
                                    <label for="discount_date" class="field prepend-icon">
                                        {!! Form::text('discount_date', old('discount_date',isset($student['discount_date']) ? $student['discount_date'] : ''), ['class' => 'gui-input date_picker ','id' => 'discount_date', 'readonly' => 'readonly']) !!}
                                        <label for="datepicker1" class="field-icon">
                                            <i class="fa fa-calendar-o"></i>
                                        </label>
                                    </label>
                                    @if ($errors->has('discount_date')) 
                                    <p class="help-block">{{ $errors->first('discount_date') }}</p>
                                    @endif
                                </div>-->
            </div>
            <div class="section-divider mv40">
                <span><i class="fa fa-hand-o-right" aria-hidden="true"></i> Fill left Info</span>
            </div>
            <div class="section row " id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.student_left')}}</span></label>
                    <div class="section">
                        <label class="field select">
                            {!!Form::select('student_left', $student['arr_student_left'],isset($student['student_left']) ? $student['student_left'] : 'No', ['class' => 'form-control'])!!}
                        </label>
                        @if ($errors->has('student_left')) 
                        <p class="help-block">{{ $errors->first('student_left') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.reason')}}</span></label>
                    <label for="reason" class="prepend-icon">
                        {!! Form::textarea("reason", old('reason',isset($student['reason']) ? $student['reason'] : ''), ["class" => "gui-input heightcls", "id" => "reason","rows"=>3]) !!}
                        <label for="reason" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('reason')) 
                    <p class="help-block">{{ $errors->first('reason') }}</p>
                    @endif
                </div>
            </div>
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::button('Cancel', ['class' => 'button btn-primary','id'=>'form-reload','style'=>'height:auto;width:15%']) !!}
            {!! Form::submit('Save', ['class' => 'button btn-primary','name'=>'save']) !!}
<!--            <a href="" target="_blank">
                {!! Form::submit('Save&Print', ['class' => 'button btn-primary','name'=>'savePrint']) !!}
            </a>-->
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
</div>
@include('backend.partials.popup')
<script type="text/javascript">
    function AddressCopy(f) {
//        $("#dob").datepicker('destroy');
        $("#address_line2").prop('readonly', false);
        f.address_line2.value = '';
        if (f.ifaddressCopy.checked == true) {
            f.address_line2.value = f.address_line1.value;
            $("#address_line2").prop('readonly', true);
        }
    }
    jQuery(document).ready(function () {


        $("#dob").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            maxDate: "-2Y",
            minDate: "-50Y",
            yearRange: "-50:-2"
        });
        var session_id = '{!! !empty($student["current_session_id"]) ?  $student["current_session_id"]: null !!}';

        if (session_id !== '')
        {
            getDateData(session_id);
        }

        var dob = $("#dob").val();
        if (dob !== '')
        {
            getAge(dob);
        }

        $("#search_parent").prop('disabled', true);
        $("#student-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                care_of: {
                    required: true
                },
                enrollment_number: {
                    required: true
                },
                first_name: {
                    required: true,
                    lettersonly: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                    // nowhitespace: true
                },
                last_name: {
//                    required: true,
                    lettersonly: true,
                    // nowhitespace: true
                },
                gender: {
                    required: true
                },
                student_type_id: {
                    required: true
                },
                caste_category_id: {
                    required: true
                },
                admission_date: {
                    required: true
                },
                join_date: {
                    required: true
                },
                fee_calculate_month: {
                    required: true
                },
                current_class_id: {
                    required: true
                },
                father_first_name: {
                    required: function () {
                        return $("#care_of option:selected").val() == 1
                    },
                    lettersonly: true,
                    // nowhitespace: true
                },
                father_last_name: {
//                    required: function () {
//                        return $("#care_of option:selected").val() == 1
//                    },
                    lettersonly: true,
                    // nowhitespace: true
                },
                father_contact_number: {
                    minlength: 7,
                    required: function (e) {
                        if ($("#care_of option:selected").val() == 1) {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    remote: {
                        url: "{{url('verify_mobile_number')}}",
                        type: "post",
                        data: {
                            student_parent_id: function () {
                                return $("#student_parent_id").val();

                            }
                        }
                    }
                },
                mother_first_name: {
                    required: function () {
                        return $("#care_of option:selected").val() == 2
                    },
                    lettersonly: true,
                    // nowhitespace: true

                },
                mother_last_name: {
//                    required: function () {
//                        return $("#care_of option:selected").val() == 2
//                    },
                    lettersonly: true,
                    // nowhitespace: true
                },
                mother_contact_number: {
                    minlength: 7,
                    required: function (e) {
                        if ($("#care_of option:selected").val() == 2) {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    remote: {
                        url: "{{url('verify_mobile_number')}}",
                        type: "post",
                        data: {
                            student_parent_id: function () {
                                return $("#student_parent_id").val();

                            }
                        }
                    },
                    // nowhitespace: true
                },
                guardian_first_name: {
                    required: function () {
                        return $("#care_of option:selected").val() == 3
                    },
                    lettersonly: true,
                    // nowhitespace: true
                },
                guardian_last_name: {
//                    required: function () {
//                        return $("#care_of option:selected").val() == 3
//                    },
                    lettersonly: true,
                    // nowhitespace: true
                },
                guardian_contact_number: {
                    minlength: 7,
                    required: function (e) {
                        if ($("#care_of option:selected").val() == 3) {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    remote: {
                        url: "{{url('verify_mobile_number')}}",
                        type: "post",
                        data: {
                            student_parent_id: function () {
                                return $("#student_parent_id").val();

                            }
                        }
                    }
                },
                current_session_id: {
                    required: true
                },
                address_line1: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                    // nowhitespace: true
                },
                address_line2: {
                    // nowhitespace: true
                },
                student_left: {
                    required: true
                },
                new_student: {
                    required: true
                },
                discount_date: {
                    required: function (e) {
                        if ($("#fee_discount").val() !== '' && $("#fee_discount").val() > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }

                },
                mother_occupation: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                father_occupation: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                father_middle_name: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                mother_middle_name: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                middle_name: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                guardian_middle_name: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                guardian_occupation: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                birth_place: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                profile_photo: {
                    extension: true,
                },
                previous_school_name: {
                    // nowhitespace: true,

                },
                previous_class_name: {
                    // nowhitespace: true
                },
                tc_number: {
                    // nowhitespace: true
                },
                reason: {
                    // nowhitespace: true
                },
                remark: {
                    // nowhitespace: true
                },
                admission_class_name: {
                    // nowhitespace: true
                },
                previous_result: {
                    // nowhitespace: true
                },
                current_section_id: {
                    required: true,
                },
            },
            messages: {
                father_contact_number: {
                    remote: "Father's number already exist, please try with other",
                    minlength: "Please enter at least 7 digits."
                },
                mother_contact_number: {
                    remote: "Mother's number already exist, please try with other",
                    minlength: "Please enter at least 7 digits."
                },
                guardian_contact_number: {
                    remote: "Guardian's number already exist, please try with other",
                    minlength: "Please enter at least 7 digits."
                },

            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        $('#admission_date').datepicker().on('change', function (ev) {
            var max_date = $(this).datepicker("option", "maxDate");
            $("#join_date").datepicker('destroy');
            $("#join_date").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                minDate: $(this).val(),
                maxDate: max_date
            });
            $(this).valid();
        });
        $('#join_date').datepicker().on('change', function (ev) {
            $(this).valid();
        });
        $('#dob').datepicker().on('change', function (ev) {
            getAge($(this).val());
        });

//        $.validator.addMethod("lettersonly", function (value, element) {
//            return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9\s\.]+$/i.test(value);
//        }, "Please enter only alphabets(more than 1");
//        
        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || !/[~`!@#$%\^&*()+=\-\_\.\[\]\\';,/{}|\\":<>\?]/.test(value) || /^[a-zA-Z][a-zA-Z0-9\s\.\-\_]+$/i.test(value);
        }, "Please enter valid data, except spacial characters");


        jQuery.validator.addMethod("extension", function (a, b, c) {
            return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
        }, jQuery.validator.format("Only valid image formats are allowed"));

        // get enquire data 
        $('#search_enquire_data').on('click', function (e) {
            e.preventDefault();
            var form_number = '';
            $("#care_of_contact").val('');
            $("#care_of_aadhaar").val('');
            $("#sibling_exist").prop('checked', false);
            form_number = $("#form_number").val();
            if (form_number != '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{ url('get-enquire') }}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'form_number': form_number,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        var resopose_data = [];
                        $("#LoadingImage").hide();
                        resopose_data = response.data;
                        if (response.status == 'success')
                        {
                            jQuery.each(resopose_data, function (key, val) {
                                if (key == 'caste_category_id')
                                {
                                    $('select[name="caste_category_id"]').val(val);
                                } else if (key == 'gender')
                                {
                                    $('select[name="gender"]').val(val);
                                } else if (key == 'student_enquire_id')
                                {
                                    $("#enquire_id").val(val);
                                } else if (key == 'session_id')
                                {
                                    $('select[name="session_id"]').val(val);
                                    $("#join_date").val('');
                                    $("#join_date").datepicker('destroy');
                                    $("#admission_date").val('');
                                    $("#admission_date").datepicker('destroy');
                                    var minDate = resopose_data.start_date;
                                    var maxDate = resopose_data.end_date;
                                    setDate(minDate, maxDate);
                                } else if (key == 'class_id')
                                {
                                    $('select[name="current_class_id"]').val(val);
                                     getClassSetcion(val);
                                } else
                                {
                                    $("#" + key).val(val);
                                }
                            });
                            $("#student-form").valid();
                            $('#server-response-success').text(response.message);
                            $('#alertmsg-success').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        } else
                        {
                            $('#server-response-message').text(response.message);
                            $('#alertmsg-student').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        }

                    }
                });
            } else
            {

                $('#server-response-message').text('Please enter enquire number.');
                $('#alertmsg-student').modal({
                    backdrop: 'static',
                    keyboard: false
                })
            }

        });



        // reset form data
//        $('#reset_data').on('click', function (e) {
//            $("#student-form")[0].reset();
//        });

    });
    
    function getScholarNo(class_id)
    {
        var student_id = $("#student_id").val();
        if(student_id == '')
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-scholar-number')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'class_id': class_id,
                },
                success: function (response) {
                    if (response.status == 'success')
                    {
                        $("#enrollment_number").val(response.scholar_no);
                    }
                }
            });
        }
    }

    $('#current_class_id').on('change', function (e) {
        e.preventDefault();
        getStudentStrength();
//        $("#enrollment_number").val('');
        $('select[name="current_section_id"]').empty();
        $('select[name="current_section_id"]').append('<option value="">--Select section--</option>');
        var class_id = $(this).val();
         getClassSetcion(class_id);
      
    });
    function getClassSetcion(class_id)
    {
          if (class_id !== '')
        {
            getScholarNo(class_id);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-section-list')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'class_id': class_id,
                },
                success: function (response) {
                    var resopose_data = [];
                    resopose_data = response.data;
                    if (response.status == 'success')
                    {
                        $.each(resopose_data, function (key, value) {
                            $('select[name="current_section_id"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    } else
                    {
                        return false;
                    }
                }
            });
        }
    }
    $('#current_section_id').on('change', function (e) {
        e.preventDefault();
        getStudentStrength();
    });
    function getStudentStrength()
    {
        $("#available").text('');
        $("#strength").text('');
        $("#admission").text('');
        var section_id = $("#current_section_id").val();
        var class_id = $("#current_class_id").val();
        var session_id = $("#session_id").val();
        if (section_id !== '' && class_id !== '')
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-section-strength')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'section_id': section_id,
                    'class_id': class_id,
                    'session_id': session_id,
                },
                success: function (response) {
                    if (response.status == 'success')
                    {
                        $("#available").text(response.data.available);
                        $("#strength").text(response.data.strength);
                        $("#admission").text(response.data.admission);
                    }
                }
            });
        } else
        {
        }
    }
    $(document).on('click', '#sibling_exist', function (e) {
        if ($("#care_of option:selected").val() != '')
        {
            if (this.checked)
            {
                $("#search_parent").prop('disabled', false);
            } else
            {
                $("#search_parent").prop('disabled', true);
            }
        } else
        {
            $("#sibling_exist").prop('checked', false);
            $('.modal-body').text('Please select student care of.');
            $('#alertmsg-student').modal({
                backdrop: 'static',
                keyboard: false
            })
        }
    });
    $(document).on('change', '#care_of', function (e) {
        $("#care_of_contact_text").text('');
        if ($("#care_of option:selected").val() != '')
        {
            var care_of = $("#care_of option:selected").text();
            $("#care_of_contact_text").text(care_of);
        }
    });

    $(document).on('click', '#search_parent', function (e) {
        clearFields();
        if ($("#care_of").valid())
        {
            var care_of_contact = $("#care_of_contact").val();
            if (care_of_contact != '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-parent')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'care_of_contact': care_of_contact,
                        'care_of': $("#care_of").val(),
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status == 'success')
                        {
                            $.each(resopose_data, function (key, value) {
                                $("#" + key).val(value);
                            });
                        } else
                        {
                            $("#sibling_exist").prop('checked', false);
                            $('#server-response-message').text('Parent not found.');
                            $('#alertmsg-student').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        }
                        $("#LoadingImage").hide();
                    }
                });
            } else
            {
                $('#server-response-message').text('Please enter contac or aadhaar number.');
                $('#alertmsg-student').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
        }
    });
    function clearFields()
    {
        $("#father_first_name").val('');
        $("#father_middel_name").val('');
        $("#father_last_name").val('');
        $("#father_contact_number").val('');
        $("#father_email").val('');
        $("#father_occupation").val('');
        $("#mother_first_name").val('');
        $("#mother_middle_name").val('');
        $("#mother_last_name").val('');
        $("#mother_email").val('');
        $("#mother_occupation").val('');
        $("#mother_contact_number").val('');
        $("#guardian_first_name").val('');
        $("#guardian_middle_name").val('');
        $("#guardian_last_name").val('');
        $("#guardian_email").val('');
        $("#guardian_occupation").val('');
        $("#guardian_contact_number").val('');
    }
    $(document).on('change', '#session_id', function () {
        var session_id = $("#session_id").val();
        $("#join_date").val('');
        $("#admission_date").val('');
        getDateData(session_id);
        getStudentStrength();

    });

    function getDateData(session_id)
    {
        if (session_id !== '')
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-session')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'session_id': session_id,
                },
                beforeSend: function () {
                    $("#LoadingImage").show();
                },
                success: function (response) {
                    if (response.status === 'success')
                    {
                        var minDate = response.start_date;
                        var maxDate = response.end_date;
                        // setDate(minDate, maxDate);
                        setDate("01/01/1955", maxDate);
                        $("#LoadingImage").hide();
                    }
                }
            });
        }
    }
    function setDate(minDate, maxDate)
    {
        $("#join_date").datepicker('destroy');
        $("#admission_date").datepicker('destroy');
        $("#admission_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: minDate,
            maxDate: maxDate
        });
        if ($("#admission_date").val() !== '')
        {
            $("#join_date").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                minDate: $("#admission_date").val(),
                maxDate: maxDate
            });
        }

    }
    $(document).on('click', '#admission_date', function () {
        var session = $("#session_id").val();
        if (session === '')
        {
            $('#server-response-message').text('First select academic year');
            $('#alertmsg-student').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    });
    $(document).on('click', '#join_date', function () {
        var admission_date = $("#admission_date").val();
        if (admission_date === '')
        {
            $('#server-response-message').text('First select admission date');
            $('#alertmsg-student').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    });
    $(document).on('click', '#form-reload', function (e) {
        window.location.href="{!! url('admin/student/add') !!}";
    });


</script>
