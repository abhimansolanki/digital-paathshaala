@extends('admin_panel/layout')
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
    #session_id {
        padding:0px !important;
        height: 30px !important;
    }
    .btn-bs-file input[type="file"]{
        position: absolute;
        top: -9999999;
        filter: alpha(opacity=0);
        opacity: 0;
        width:0;
        height:0;
        outline: none;
        cursor: inherit;
    }
    .btn{
        margin-top: 0px !important;
    }
    .btncustom{
        border: 1px solid #ccc;
        padding: 0px 0px
    }
    .save-button{
        min-width: 100px;
        margin-top: 30px;
        height: 33px;
        border: none;
    }
</style>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-7">
                    <span class="glyphicon glyphicon-tasks"></span> <span>{{ trans('language.list_student')}}</span>
                </div>
                <div class="topbar-right">
                    <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right custom-btn">{{trans('language.add_student')}}</a>
                </div>
            </div>
	<div class="panel">
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        {!! Form::open(['url'=>$import_url,'id'=>'import-student','files'=>true]) !!}
                        <div class="row">
                            <div class="col-md-3">
                                <label>
                                    <span class="radioBtnpan">Import Student<span class="asterisk">*</span>
                                    </span>
                                </label>
                                <label class="btn-bs-file btn btn-xs col-md-12 btncustom">
                                    <div class="col-md-6 classfontsize">
                                        <span class="glyphicons glyphicons-upload"></span>
                                        {!! trans('language.upload_excel') !!}
                                    </div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-4 fontsizeand">
                                        <span class="glyphicons glyphicons-search"></span>Browse file </div>
                                    {!! Form::file('import_student'); !!}
                                </label>
                            </div>
		    <div class="col-md-2">
		        <label>
                                    <span class="radioBtnpan">Admin Password<span class="asterisk">*</span>
                                    </span>
                                </label>
		        {!! Form::password('admin_password',['class'=>'gui-input form-control','placeholder'=>'Admin Password','required'=>true]) !!}
		    </div>
                            <div class="col-md-3">
                                <label><span class="radioBtnpan"></span></label>
                                <label class="field select">
                                    {!! Form::submit('Save', ['class' => 'button btn-primary save-button']) !!}
                                </label>
                            </div>
                            <div class="pull-right mr10">
                                <label><span class="radioBtnpan"></span></label>
                                <label class="field select">
                                    {!! Form::button('Download Sample File', ['class' => 'button btn-primary save-button download-sample']) !!}
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>{{trans('language.class_name')}}</th>
                            <th>Stu. Name</th>
                            <th>{{trans('language.gender')}}</th>
                            <th>{{trans('language.dob')}}</th>
                            <th>Admission</th>
                            <th>{{trans('language.care_of_rel')}}</th>
                            <th>{{trans('language.care_of_name')}}</th>
                            <th>{{trans('language.care_of_contact')}}</th>
                            <th>{{trans('language.caste_category')}}</th>
                            <th>{{trans('language.address')}}</th>
                            <th class="text-center">{{trans('language.action')}}</th>
                            <th class="text-center">{{trans('language.view_print')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        //validate file
        $("#import-student").validate({

            /* @validation states + elements
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules
             ------------------------------------------ */
            rules: {
                import_student: {
                    required: true,
                    extension: true,
                },
                admin_password: {
                    required: true,
                },

            },
            /* @validation error messages
             ---------------------------------------------- */

            /* @validation highlighting + error placement
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        getStudents();
        function getStudents()
        {
            $('#student-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
//                bFilter:false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'Student Admission',
                        "filename": 'student-admission',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Student Admission',
                        "filename": 'student-admission',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                            modifier: {
                                selected: true
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: "{{ url('student/data')}}",
                    data: function (f) {
//                        f.session_id = $("input[name='session_id']").val();
                    }
                },
                columns: [
                    {data: 'student_id', name: 'student_id'},
                    {data: 'enrollment_number', name: 'enrollment_number'},
                    {data: 'class_name', name: 'class_name'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'gender_name', name: 'gender_name'},
                    {data: 'dob', name: 'dob'},
                    {data: 'admission_date', name: 'admission_date'},
                    {data: 'care_of_rel', name: 'care_of_rel'},
                    {data: 'care_of_name', name: 'care_of_name'},
                    {data: 'care_of_contact', name: 'care_of_contact'},
                    {data: 'caste_name', name: 'caste_name'},
                    {data: 'address_line1', name: 'address_line1'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'view_print', name: 'view_print', orderable: false, searchable: false}

                ]
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
        }
        var table = $('#student-table').DataTable();
        table.on('select deselect', function (e) {
            var arr_checked_student = checkedStudent();
            if (arr_checked_student.length > 0)
            {
                $(".buttons-excel").prop('disabled', false);
                $(".buttons-print").prop('disabled', false);
            } else
            {
                $(".buttons-excel").prop('disabled', true);
                $(".buttons-print").prop('disabled', true);
            }
        });

        function checkedStudent()
        {
            var arr_checked_student = [];
            $.each(table.rows('.selected').data(), function () {
                arr_checked_student.push(this["student_id"]);
            });
            return arr_checked_student;
        }

        $(document).on('change', '#session_id', function (e) {
            getStudents();
        });

        jQuery.validator.addMethod("extension", function (a, b, c) {
            return c = "string" == typeof c ? c.replace(/,/g, "|") : "xls|xlsx", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
        }, jQuery.validator.format("Only valid formats(xls|xlsx) are allowed"));

        $('.download-sample').click(function () {
            window.open('{{ url("public/sample/student-import-sample.xlsx") }}');
        });
    });

</script>
</body>
</html>
@endsection



