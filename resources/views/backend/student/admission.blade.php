<!doctype html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{{ trans('language.admission_form')}} </title>
    </head>
    <style type="text/css">

        .addtext{
            position: relative;
            width: 300px;
            padding: 14px 55px;
            margin-right: -50px;
            float: right;
            position: relative;
            background: #000;
            color: #fff;
            font-size: 16px;
            text-transform: uppercase;
            margin-bottom: 70px;

        }
        .addtext:after {
            content: " ";
            position: absolute;
            display: block;
            width: 30px;
            height: 50.2px;
            top: 0;
            left: 0;
            z-index: -1;
            background: #000;
            transform-origin: top left;
            transform: skew(-30deg, 0deg);
        }

        body{
            font-size: 12px;

            margin:20px 15px;
        }
        /*  .addtext{
              background: #000;
              width: 50%;
              text-align: left;
              padding: 8px 0px;
              color: #fff;
              text-transform: uppercase;
              position: relative;
              float: right;
              margin-top: 0px;
              padding-left: 30px;
          }*/
        .studentClass{
            float: right;
            padding: 5px;
            border: 1px solid;
            height: 120px;
            width: 100px;
            z-index: 999999;
            position: absolute;
            background: #ffff;
            right: 0px;
            top: -2%;
        }
        .logo{
            float: left;
            width: 20%;
            margin-top: -30px;
        }
        .addmiNo{
            width:250px;
            float: right;
            margin-right: 15.5%;
            margin-top: -82px;
        }
        .foemno{
            float: left;
            width: 70px;
	margin-top: -15px;
        }
        .inderform{
            float: right;
            width: 140px;
            margin-right: 20px;
            border-bottom: 1px dotted #000;
            text-align: left;
	margin-top: -15px;
        }
        .admisdate{
            float: right;
            width: 132px;
            border-bottom: 1px dotted #000;
            text-align: left;
            margin-right: 20px;
        }
        .admisdate1{
            float: left;
            width: 93px;
        }
        .stduenamelabel{
            float: left;
            width: 14%;
            padding-top: 2px;
            font-weight: bold;
        }

        .stduenamelabel345{
            float: left;
            width: 16%;
            padding-top: 2px;
            font-weight: bold; 
        }

        .stduenamelabel3{
            float: left;
            width: 11%;
            padding-top: 2px;
            font-weight: bold;
        }
        .stduenamelabel32{
            float: left;
            width: 20%;
            padding-top: 2px;
            font-weight: bold; 
        }
        .stduenamelabel323{
            float: left;
            width: 15%;
            padding-top: 2px;
            font-weight: bold;
        }
        .stduenName1{
            float: left;
            border-bottom: 1px dotted #000;
            width: 78%;
            margin-left: 2%;
            padding-bottom: 5px;
        }
        .stduenName{
            float: left;
            border-bottom: 1px dotted #000;
            width: 84%;
            margin-left: 2%;
            padding-bottom: 5px;
        }
        .genderarea{
            float: left;
            border:1px solid;
            width: 50%;
        }
        .genderlabel356{
            float: left;
            width:6%;
            padding-top: 2px;
            font-weight: bold;
        }
        .genderlabel3568{
            float: left;
            margin-left: 10px;
            width:12%;
            padding-top: 2px;
            font-weight: bold;
        }
        .attacment{
            float: left;
            width:18%;
            padding-top: 2px;
            font-weight: bold;
        }
        .lastone{
            float: left;
            width:10%;
            padding-top: 2px;
            font-weight: bold;
        }
        .genderlabel{
            float: left;
            width: 12%;
            padding-top: 2px;
            font-weight: bold;
        }
        .genderlabel1{
            float: left;
            width: 9%;
            padding-top: 2px;
            font-weight: bold;
        }
        .gengerClass2{
            float: left;
            border-bottom: 1px dotted #000;
            width: 36%;
            margin-left: 2%;
            padding-bottom: 5px; 
            margin-right: 10px; 
        }
        .Signature{
            float: left;
            width: 30%;
            text-align: center;
            margin: 0px 20px;
        }
        .Signature2{
            width:70%;
            text-align: center;
            margin: 0px 20px;
        }
        .checkedby{
            float: left;
            margin-top: 15px;
            border-bottom: 1px solid #000;
            width: 20%;
            padding-bottom: 5px; 
        }
        .studneclasss{
            float: left;
            border-bottom: 1px dotted #000;
            width: 19%;
            margin-left: 2%;
            margin-right: 10px;
            padding-bottom: 5px; 
        }
        .gengerClass{
            float: left;
            border-bottom: 1px dotted #000;
            width: 37%;
            margin-right: 10px;
            margin-left: 2%;
            padding-bottom: 5px;  
        }
        .gengerClass235{
            float: left;
            margin-right: 10px;
            border-bottom: 1px dotted #000;
            width:34.6%;
            margin-left: 2%;
            padding-bottom: 5px; 
        }
        .genderlabel2{
            float: left;
            width: 13%;
            padding-top: 2px;
            font-weight: bold;
        }
        .gengerClass1{
            float: left;
            border-bottom: 1px dotted #000;
            width: 33.8%;
            margin-left: 0px;
            padding-bottom: 5px;  
        }
        .gengerClass1235{
            float: left;
            border-bottom: 1px dotted #000;
            width: 25%;
            margin-left: 2%;
            padding-bottom: 5px;  
        }
        .gengerClass12352{
            float: left;
            margin-right: 10px;
            border-bottom: 1px dotted #000;
            width: 21%;
            margin-left: 2%;
            padding-bottom: 5px;    
        }
        .declare{
            font-size: 12px;
            float: left;
            width: 90%;
            line-height: 22px;
            padding-bottom: 15px;
        }
        .centerdive{
            position: relative;
            top: -20px;
            width: 200px;
            background: #000;
            color: #fff;
            padding: 6px 0px;
            left: 35%;
            right: 35%;
            text-align: center;
        }
        .dateoflabel{
            float: right;
        }
        .dateofdate{
            float: right;
            border-bottom: 1px dotted #000;
            padding-bottom: 5px;
            width: 20%;
            margin-left: 10px;
        }
        .footerstudent{
            float: left;
        }
        .footerright{
            float: left;
            border-bottom: 1px dotted #000;
            width: 46%;
            margin-left: 10px;
            margin-top: 12px;
            margin-right: 10px;
        }
        .psign{
            float: right;
            border-top: 1px solid #000;
            width: 25%;
            text-align: center;
            padding-top: 10px;
            margin-top: 10px;
            font-weight: bold;
        }
    </style>
    @if(!empty($arr_student_data))
    @foreach($arr_student_data as $key => $student_data)
    <body>
        <div class="logo">
            <img src="{{url(get_school_logo())}}" width="200">
        </div>
        <div class="addtext">
            <h3 style="padding: 0px; margin: 0px;">{{ trans('language.admission_form')}}</h3>

        </div>
        <div style="clear: both;"></div>
        <br>
        <div class="addmiNo">
            <br>
            <div class="foemno" > <label style="font-weight: bold;">{!! trans('language.form_number') !!} :</label></div>
            <div class="inderform" style="height: 10px !important;">{!! $student_data['form_number'] !!}</div>
            <div style="clear: both;"></div>
            <br>
            <div  class="admisdate1"> <label style="font-weight: bold;">{!! trans('language.admission_date') !!} :</label></div>
            <div  class="admisdate">{!! $student_data['admission_date'] !!}</div>
            <div style="clear: both;"></div>
            <br>
            <div  class="admisdate1"> <label style="font-weight: bold;">{!! trans('language.enrollment_number') !!} :</label></div>
            <div  class="admisdate">{!! $student_data['enrollment_number'] !!}</div>
            <div style="clear: both;"></div>
        </div>
        <div style="clear: both;"></div>
        <div class="studentClass">
            @if(isset($student_data['profile']) && !empty($student_data['profile']))
            <img src="{{url($student_data['profile'])}}" width="100" height="120">
            @else
            <!--<img src="../public/css/profile.png" style="width: 100%" height="100%;">-->
            <img data-src="holder.js/93%x160" alt="holder" height="120">
            @endif

        </div>
        <div style="clear: both;"></div>
        <br>

        <hr style="border:1px solid">
        <br>

        <div class="stduenamelabel"> {!! trans('language.student_name') !!} : </div>
        <div class="stduenName" >{!! $student_data['student_name'] !!}</div>
        <div style="clear: both;"></div>
        <br>

        <div class="genderlabel" style="height: 10px !important;">{!! trans('language.gender') !!} : </div>
        <div class="gengerClass" style="height: 10px !important;"> {!! $student_data['gender_name'] !!}</div>
        <div class="genderlabel1" style="height: 10px !important;">{!! trans('language.caste_category') !!} :</div>
        <div class="gengerClass" style="height: 10px !important;">{!! $student_data['caste_name'] !!}</div>
        <div style="clear: both;"></div>
        <br>

        <div class="genderlabel" style="height: 10px !important;">{!! trans('language.date_of_b') !!} : <br><small> (in figures)</small></div>
        <div class="gengerClass" style="height: 10px !important;"> {!! $student_data['dob'] !!} </div>
        <div style="clear: both;"></div>
        <br>
        <br>

        <div class="genderlabel" style="height: 10px !important;">{!! trans('language.date_of_b') !!} : <br><small> (in words)</small></div>
        <div class="gengerClass" style="height: 10px !important; width: 86%;"> {!! covert_dob_words($student_data['dob']) !!} </div>
        <div style="clear: both;"></div>
        <br>
        <br>
        <div class="genderlabel" style="height: 10px !important;" style="width: 10.5%;">{!! trans('language.birth_place') !!} :</div>

        <div class="gengerClass"  style="height: 10px !important; width: 33.8% !important"> {!! $student_data['birth_place'] !!} </div>
        <div style="clear: both;"></div>
        <br>

        <div class="genderlabel" style="height: 10px !important;">{!! trans('language.father_name') !!} :</div>
        <div class="gengerClass" style="height: 10px !important;">{!! $student_data['father_name'] !!}</div>
        <div class="genderlabel "  style="height: 10px !important;">{!! trans('language.father_email') !!} :</div>
        <div class="gengerClass1"  style="height: 10px !important; width: 35.5%;">{!! $student_data['father_email'] !!}</div>
        <div style="clear: both;"></div>
        <br>

        <div class="genderlabel" style="height: 10px !important; width: 18%;" >{!! trans('language.father_occupation') !!} :</div>
        <div class="gengerClass" style="height: 10px !important; width: 31%" >{!! $student_data['father_occupation'] !!}</div>
        <div class="genderlabel" style="height: 10px !important;">{!! trans('language.father_contact_number') !!} :</div>
        <div class="gengerClass1" style="height: 10px !important;  width:35.5%;">{!! $student_data['father_contact_number'] !!}</div>
        <div style="clear: both;"></div>
        <br>

        <div class="genderlabel" style="height: 10px !important; width: 12.5%;"> {!! trans('language.mother_name') !!} :</div>
        <div class="gengerClass" style="height: 10px !important;  "  >{!! $student_data['mother_name'] !!}</div>
        <div class="genderlabel" style="height: 10px !important; "> {!! trans('language.mother_email') !!} :</div>
        <div class="gengerClass1" style="height: 10px !important;  width: 34.7%;">{!! $student_data['mother_email'] !!}</div>
        <div style="clear: both;"></div>
        <br>

        <div class="genderlabel" style="height: 10px !important;  width: 18%;">{!! trans('language.mother_occupation') !!} : </div>
        <div class="gengerClass" style="height: 10px !important; width: 31%" ">{!! $student_data['mother_occupation'] !!}</div>
        <div class="genderlabel" style="height: 10px !important;"> {!! trans('language.mother_contact_number') !!} :</div>
        <div class="gengerClass1" style="height: 10px !important; width: 34.9%;">{!! $student_data['mother_contact_number'] !!}</div>
        <div style="clear: both;"></div>
        <br>

        <div class="genderlabel2" style="height: 10px !important; width: 14%;">Guardian :</div>
        <div class="gengerClass2" style="height: 10px !important;  width: 35%;">{!! $student_data['guardian_first_name'] !!} {!! $student_data['guardian_last_name'] !!}</div>
        <div class="genderlabel" style="height: 10px !important;">Relationship :</div>
        <div class="gengerClass1" style="height: 10px !important;  width: 35%;">{!! $student_data['guardian_relation'] !!}</div>
        <div style="clear: both;"></div>
        <br>


        <div class="stduenamelabel32" style="height: 10px !important;">{!! trans('language.address_line1') !!} :</div>
        <div class="stduenName1" style="height: 10px !important;">{!! $student_data['address_line1'] !!}</div>
        <div style="clear: both;"></div>
        <br>
        <div class="stduenamelabel32" style="height: 10px !important;">{!! trans('language.address_line2') !!} :</div>
        <div class="stduenName1" style="height: 10px !important;">{!! $student_data['address_line2'] !!}</div>
        <div style="clear: both;"></div>
        <br>
        <div class="stduenamelabel32" style="height: 10px !important;">{!! trans('language.previous_school_name') !!} :</div>
        <div class="stduenName1" style="height: 10px !important;">{!! $student_data['previous_school_name'] !!}</div>
        <div style="clear: both;"></div>
        <br>
        <div class="stduenamelabel32" style="height: 10px !important;">{!! trans('language.previous_class_name') !!} :</div>
        <div class="gengerClass1" style="height: 10px !important;">{!! $student_data['previous_class_name'] !!}</div>
        <div class="genderlabel" style="height: 10px !important; padding-left: 10px;  width: 7%;">Result :</div>
        <div class="gengerClass1" style="height: 10px !important; width: 37%;">{!! $student_data['previous_result'] !!}</div>
        <div style="clear: both;"></div>
        <br>

        <div class="genderlabel" style="height: 10px !important;"> {!! trans('language.tc_number') !!} :</div>
        <div class="gengerClass1" style="height: 10px !important;">{!! $student_data['tc_number'] !!}</div>
        <div class="genderlabel" style="height: 10px !important; width: 10%; padding-left: 10px;"> {!! trans('language.tc_date') !!} :</div>
        <div class="gengerClass1" style="height: 10px !important; width: 42%;" >{!! $student_data['tc_date'] !!}</div>
        <div style="clear: both;"></div>
        <br>
        <!--        <div class="stduenamelabel32"> Transport Required :</div>
                <div class="studneclasss">Yes</div>
                <div style="clear: both;"></div>
                <br>-->
        <hr>
        <div class="stduenamelabel">
            Declaration:
        </div>
        <div class="declare">
            I have gone through the rules and regulation unmentioned in the school hand book and i agree to abide by
            the same with instruction issued from time to time by the school authorities the date of birth mentioned
            above is correct and i shall not request for it's change.

        </div>
        <div style="clear: both;"></div>

        <hr>
        <div class="centerdive">For Office Use Only</div>
        <div class="dateofdate">{!! date('d M Y') !!}</div>
        <div class="dateoflabel">Date : </div>
        <div style="clear: both;"></div>

        <!--      <div class="attacment"  style="width:18.5%;">Attachment Required :</div>
             <div class="Signature2">T.C (Original) , Mark Sheet , (Photocopy) Birth crerificate (Photocopy)</div>
             <br><br> -->
        <div class="footerstudent" >
            On Scrutiny, the student has been found eligible for class
        </div>
        <div class="footerright"  style="width: 39%;"></div>
        <div class="footerstudent">  is accordingly admit. </div>
        <div style="clear: both;"></div>
        <br>
        <div class="lastone" style="width: 12%;">Receipt No. :</div>
        <div class="studneclasss">ADM{!! $student_data['student_id'] !!}</div>
        <div class="lastone" style="width:6%;">Class  :</div>
        <div class="gengerClass12352">{!! $student_data['class_name'] !!}</div>
        <div class="lastone" style="width: 11%;">Scholar No. :</div>
        <div class="gengerClass12352">{!! $student_data['enrollment_number'] !!}</div>
        <div style="clear: both;"></div>
        <br>
        <div class="lastone" style="width: 12%;">Checked By :</div>
        <div class="checkedby"></div>
        <div class="Signature">Signature of Clerk</div>
        <div class="psign">Principle</div>
    </body>
    @endforeach
    @endif
</html>

