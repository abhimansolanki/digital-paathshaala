<!--get class data-->
@php $arr_class_data = get_class_subject_mapping($session_id, $class_id);@endphp
@if(!empty($arr_class_data))
<table class="table table-bordered" id="class-sbject-data-table">
    <thead>
    <th>{!! trans('language.class')!!}</th>
    <th>{!! trans('language.section')!!}</th>
    <th>{!! trans('language.group_name')!!}</th>
    <th>{!! trans('language.subject')!!}</th>
    <th>{!! trans('language.sub_subject')!!}</th>
</thead>
<tbody>
    <!-- class loop start  -->
    @foreach($arr_class_data as $class_data)

        @php $class = 1; @endphp

        <!-- get class row span -->
        @php  $class_row_span = get_row_span_view_subject($session_id, $class_data['class_id'],null,null);@endphp
        
        <!--get section of class-->
        @php $arr_section_data = get_class_subject_section($session_id, $class_data['class_id'], $section_id, null); @endphp

        <!--section loop start-->
        @foreach($arr_section_data as $section_data)
            
            <!--get group of class and section-->
            @php $section = 1;  @endphp

            <!--section row span-->
            @php $section_row_span = get_row_span_view_subject($session_id, $class_data['class_id'],$section_data['section_id'],null); @endphp

            <!--get all groups of session, class and section-->
            @php $arr_group_data = get_class_subject_section_group($session_id,$class_data['class_id'], $section_data['section_id']); @endphp

            <!--group loop  start -->
            @foreach($arr_group_data as $group_data)

                <!--get subject of class, section group-->
                @php $group = 1; @endphp

                <!--group row span from helper file -->
                @php  $group_row_span = get_row_span_view_subject($session_id, $class_data['class_id'],$section_data['section_id'],$group_data['subject_group_id']);@endphp

                <!--get group subjects from helper file -->
                @php $arr_subject_data = get_class_subject_section_group_subject($session_id,$class_data['class_id'], $section_data['section_id'],$group_data['subject_group_id']);@endphp

                <!--start subject loop of group-->
                @foreach($arr_subject_data as $subject_data)

                    <!--get sub-subject of subject-->
                    @php $subject = 1; $arr_sub_subject = get_class_subject_section_group_sub_subject($session_id,$class_data['class_id'], $section_data['section_id'],$group_data['subject_group_id'],$subject_data['subject_id']); @endphp

                    <!--if condition to check sub-subject exist or not-->
                    @if(!empty($arr_sub_subject))
                           
                        <!-- sub-subject loop start -->
                        @foreach($arr_sub_subject as $sub_subject) 
                            <tr>
                                @if($class == 1)<td rowspan="{!! $class_row_span !!}">{!! $class_data['class_name'] !!}</td>@endif
                                @if($section == 1)<td rowspan="{!! $section_row_span !!}">{!! $section_data['section_name'] !!}</td>@endif
                                @if($group == 1)<td rowspan="{!! $group_row_span !!}">{!! $group_data['group_name'] !!}</td>@endif
                                @if($subject == 1)<td rowspan="{!! count($arr_sub_subject) !!}">{!! $subject_data['subject_name'] !!}</td>@endif
                                <td>{!! $sub_subject['subject_name'] !!}</td>
                            </tr>

                        <!--end sub-subject loop-->
                            @php $subject++; $class++; $group++; $section++; @endphp
                        @endforeach
                    @else
                        <!--subject data-->
                        <tr>
                            @if($class == 1) <td rowspan="{!! $class_row_span !!}">{!! $class_data['class_name'] !!}</td>@endif
                            @if($section == 1)<td rowspan="{!! $section_row_span !!}">{!! $section_data['section_name'] !!}</td>@endif
                            @if($group == 1)<td rowspan="{!! $group_row_span !!}">{!! $group_data['group_name'] !!}</td>@endif
                            <td>{!! $subject_data['subject_name'] !!}</td>
                            <td>-</td>
                        </tr>
                        <!--end else condition-->
                        @php $class++; $group++; $section++;  @endphp
                    @endif

                <!--end subject loop-->
                @endforeach

            <!--end group loop-->
            @endforeach

        <!--end section loop-->
        @endforeach  

    <!--end class loop-->
    @endforeach
</tbody>

</table>
@else
No records found
@endif