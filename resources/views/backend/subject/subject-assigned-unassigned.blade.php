<div class="admin-form">
    @if(!empty($student))
    {!! Form::open(['id' => 'subject-unassigned-form' , 'class'=>'form-horizontal','url' => '','autocomplete'=>'off']) !!}
    <div class="col-md-12">
        @php $disabled = ''; @endphp
        @if($action_enable == true)
        @php $disabled = 'disabled'; @endphp
        <span style="padding-left:10px;">Note<span class="asterisk">*</span> : Marks submitted.</span>
        @endif
        <div class="clearfix"></div>
        <div class="col-md-6">
            <h5>Assigned Student(s)</h5>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>UnAssign</th>
                        <th>Stu. Name</th>
                        <th>Enroll. No.</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($student['assigned']))
                    @foreach($student['assigned'] as $key=> $assigned_student)
                    <tr>
                        <td>
                            {!! Form::checkbox('unassigned_student_id[]',$assigned_student['student_id'],'', ['class' => '', 'id' => 'unassigned_student_id','rel'=>'',$disabled]) !!}
                        </td>
                        <td>{!! $assigned_student['student_name']!!}</td>
                        <td>{!! $assigned_student['enrollment_number']!!}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <h5>UnAssigned Student(s)</h5>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Assign </th>
                        <th>Stu. Name</th>
                        <th>Enroll. No.</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($student['unassigned']))
                    @foreach($student['unassigned'] as $key=> $unassigned_student)
                    <tr>
                        <td>
                            {!! Form::checkbox('assigned_student_id[]',$unassigned_student['student_id'],'', ['class' => '', 'id' => 'assigned_student_id','rel'=>'',$disabled]) !!}
                        </td>
                        <td>{!! $unassigned_student['student_name']!!}</td>
                        <td>{!! $unassigned_student['enrollment_number']!!}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>
    @else
    <div style="padding: 5px; text-align: center">No Student(s) available</div>
    {!! Form::close() !!}
    @endif
</div>
