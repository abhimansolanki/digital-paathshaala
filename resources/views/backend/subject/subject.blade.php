@extends('admin_panel/layout')
@section('content')
<style>
    .error{font-size: 0.85em; color: #DE888A;}
    td.details-control {
        background: url("<?php echo url('/public/details_open.png'); ?>") no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url("<?php echo url('/public/details_close.png'); ?>") no-repeat center center;
    }
</style>
<div class="admin-form theme-primary" style="padding-bottom: 175px;" id="CustomInput">
    @include('backend.partials.loader')
    <div class="tray tray-center tableCenter">
        <div class="">
            <div class="panel panel-visible" id="spy2">
                <div class="panel heading-border">
                    <div id="topbar">
                        <div class="topbar-left">
                            <ol class="breadcrumb">
                                <li class="crumb-active">
                                    <a href="{{url('admin/dashboard')}}"> <span class="glyphicon glyphicon-home"></span> Home</a>
                                </li>
                                <li class="crumb-trail">{{ trans('language.add_subject')}}</li>
                            </ol>
                        </div>
                    </div>
                    <div class="panel-body pn">
                        <div class="row">
                            <br>
                            {!! Form::open(['id'=>'subject-group-from']) !!}
                            <div class="col-md-3">
                                <label><span class="radioBtnpan">{{ trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                                <label for="section_id" class="field prepend-icon">
                                    <label class="field select">
                                        {!!Form::select('session_id', $arr_session,$session_id, ['class' => 'form-control','id'=>'session_id'])!!}
                                        <i class="arrow"></i>
                                    </label>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label><span class="radioBtnpan">{{ trans('language.class')}}<span class="asterisk">*</span></span></label>
                                <label for="section_id" class="field prepend-icon">
                                    <label class="field select">
                                        {!!Form::select('class_id', $arr_class,null, ['class' => 'form-control','id'=>'class_id'])!!}
                                        <i class="arrow"></i>
                                    </label>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label><span class="radioBtnpan">{{ trans('language.section')}}<span class="asterisk">*</span></span></label>
                                <label for="section_id" class="field prepend-icon">
                                    <label class="field select">
                                        {!!Form::select('section_id', $arr_section,null, ['class' => 'form-control','id'=>'section_id'])!!}
                                        <i class="arrow"></i>
                                    </label>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label><span class="radioBtnpan"></span></label>
                                <label for="" class="field prepend-icon">
                                    {!! Form::button('Continue', ['class' => 'button btn-primary','style'=>'width:120px;margin-top: 5px;','id'=>'get_class_subject_group']) !!}   
                                </label>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label><span class="radioBtnpan"></span></label>
                                <div class="section1">
                                    <button type="button"  id="add-subject-group" data-toggle="modal" data-backdrop="static" data-keyboard="false" class="button btn-primary" group_type="add" disabled><i class="fas fa-plus-circle"></i> Add Subject Group </button>
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                                <label><span class="radioBtnpan"></span></label>
                                <div class="section1">
                                    <button  type="button" data-toggle="modal" id="import-group-subject" data-target="" data-backdrop="static" data-keyboard="false" class="button btn-primary" disabled><i class="fas fa-plus-circle"></i> Import Subject Groups</button>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label><span class="radioBtnpan"></span></label>
                                <div class="section1">
                                    <button type="button"  id="sort-class-subject" data-toggle="modal"  data-backdrop="static" data-keyboard="false" class="button btn-primary" disabled><i class="fas fa-list-ul"></i> Reorder Class Subjects</button>
                                </div>
                            </div>
                            <br>
                        </div>
                        <div id="subject-html-tables"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  Add optional group -->
    <div id="add_subject_group" class="modal commonboxinput" role="dialog" data-dismiss="modal">
        <div class="modal-dialog">
            <!-- Modal content-->
            {!! Form::open(['id' => 'group-from' , 'class'=>'form-horizontal','url' => '','autocomplete'=>'off']) !!}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Save Subject Group</h4>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('e_subject_group_id','', ['class' => '', 'id' => 'e_subject_group_id']) !!}
                    {!! Form::hidden('group_type','', ['class' => '', 'id' => 'group_type']) !!}
                    <div id="group-edit-case">
                        <div class="col-md-5 section">
                            <label><span class="radioBtnpan">Create New Group?<span class="asterisk">*</span></span></label>
                            <label for="new_group" class="field select" style="margin-top: 0px;">
                                {!! Form::select('new_group',$arr_new_group,'',['class' => 'form-control', 'id' => 'new_group','required'=>true]) !!}
                                <i class="arrow double"></i>
                            </label>
                        </div>
                        <div class="col-md-5 section" id="arr-subject-group-div" style="display: none">
                            <label><span class="radioBtnpan">Select Group<span class="asterisk">*</span></span></label>
                            <label for="arr_subject_group" class="field select" style="margin-top: 0px;">
                                {!! Form::select('arr_subject_group_id',[], '',['class' => 'form-control', 'id' => 'arr_subject_group_id','required'=>true]) !!}
                                <i class="arrow double"></i>
                            </label>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-5" style="margin-top:20px">
                        <label><span class="radioBtnpan">Group Name<span class="asterisk">*</span></span></label>
                        <label for="group_name" class="field prepend-icon">
                            {!! Form::text('group_name','', ['class' => '','placeholder'=>'', 'id' => 'group_name','required'=>true,'subject_group_id'=>null,'call_type'=>'check','pattern'=>'[a-zA-Z][a-zA-Z0-9\s\.]*']) !!}
                        </label>
                        <span class="error" id="group_unique" style="display:none;"></span>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer closebtnforstudent" id="">
                    <button type="button" class="button btn-primary" id="save_group" call_type="save">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="close-group-model">Close</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!--  import Subject -->
    <div id="group-subject" class="modal commonboxinput" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Import Group with Subject(s)</h4>
                </div>
                <div class="modal-body" style=" height:300px; overflow-x: scroll">
                    {!! Form::open(['id' => 'import-group-subject-form' , 'class'=>'form-horizontal','url' => '','autocomplete'=>'off']) !!}
                    <div class="row">
                        <div class="col-md-4 section" id="">
                            <label class="field select">
                                {!!Form::select('import_from_session_id', add_blank_option($arr_session, '-- Select session --'),'', ['class' => 'form-control','id'=>'import_from_session_id','required'=>true])!!}
                                <i class="arrow"></i>
                            </label>
                        </div>  
                        <div class="col-md-4 section" id="">
                            <label class="field select">
                                {!!Form::select('import_from_class_id', add_blank_option($arr_class, '-- Select class --'),'', ['class' => 'form-control','id'=>'import_from_class_id','required'=>true])!!}
                                <i class="arrow"></i>
                            </label>
                        </div>  
                        <div class="section col-md-4" id="">
                            <label class="field select">
                                {!!Form::select('import_from_section_id', add_blank_option([], '-- Select section --'),'', ['class' => 'form-control','id'=>'import_from_section_id','required'=>true])!!}
                                <i class="arrow"></i>
                            </label>
                        </div>  
                    </div>
                    <div id="group-subject-body"></div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer closebtnforstudent" id="">
                    <button type="button" class="button btn-primary" id="save_imported_subject">Import Subject</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="close-import-model">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="student-assign-unassign-modal" class="modal commonboxinput" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Student's mapping with <span id="student-map-subject"></span></h4>
                </div>
                {!! Form::hidden('unassign_class_subject_id','', ['class' => '','id' =>'unassign_class_subject_id','readonly'=>true]) !!}
                <div class="modal-body" id="assign-unassign-body" style=" height:300px; overflow-x: scroll">
                </div>
                <div class="modal-footer closebtnforstudent" id="">
                    <button type="button" class="button btn-primary"  request_type="update_student" id="save_unassigned_student">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="close-student-unassign">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Reorder Subject -->
    <div id="sort-class-subject-modal" class="modal commonboxinput" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reorder Class's Subjects</h4>
                </div>
                <div class="modal-body">
                    <!--<div class="section" id="tableinsideclass">-->
<!--                        <div class="row">
                            <div class="col-md-5">
                                <label><span class="radioBtnpan">Class<span class="asterisk">*</span></span></label>
                                <label class="field select section">
                                    {!!Form::select('sort_class_id', add_blank_option($arr_class, '-- Select --'),'', ['class' => 'form-control','id'=>'sort_class_id'])!!}  
                                </label>
                            </div>
                            <div class="col-md-5">
                                <label><span class="radioBtnpan">Section<span class="asterisk">*</span></span></label>
                                <label class="field select section">
                                    {!!Form::select('sort_section_id', add_blank_option([], '-- Select --'),'', ['class' => 'form-control','id'=>'sort_section_id'])!!}  
                                </label>
                            </div>
                        </div>
                        <div class="clearfix"></div>-->
                    <!--</div>-->
                    <ul id="sortable" class="subject_reorder_data" style="margin-top: 2%;">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer closebtnforstudent" id="">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
@include('backend.partials.popup')
<script>
    $(document).ready(function () {
        $("#subject-group-from").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             * $("#section_id option").length
             ------------------------------------------ */
            ignore: [],
            rules: {
                class_id: {
                    required: true,
                },
                session_id: {
                    required: true
                },
                'section_id': {
                    required: true
                },

            },

            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        function subjectGroup(arr_group_id)
        {
//        if (arr_group_id.length > 0)
            if (arr_group_id !== '')
            {
                var count = 1;
                $.each(JSON.parse(arr_group_id), function (key, value)
                {
                    $('#subject-group' + count).DataTable({
                        destroy: true,
                        processing: true,
                        serverSide: true,
                        ajax: {
                            url: "{{ url('subject_group')}}",
                            data: function (f) {
                                f.subject_group_id = value.subject_group_id;
                                f.class_id = $("#class_id").val();
                                f.session_id = $("#session_id").val();
                                f.section_id = $("#section_id").val();
                            }
                        },
                        columns: [
                            {
                                "className": 'details-control id_' + count,
                                "orderable": false,
                                "data": null,
                                "defaultContent": '',

                            },
                            {data: 'subject_name', name: 'subject_name'},
                            {data: 'subject_code', name: 'subject_code'},
                            {data: 'sub_subject', name: 'sub_subject'},
                            {data: 'action', name: 'action'},
                        ]
                    });
                    count++;
                });
            }
        }

        $(document).on('click', '.details-control', function () {
            var temp_id = $(this).attr('class');
            var temp_id2 = temp_id.split(' ');
            var table_id = temp_id2[1].split('_');
            var id = table_id[1];
            var tr = $(this).closest('#subject-group' + id + ' tbody tr');
            var row = $('#subject-group' + id).DataTable().row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                var child = format(row.data());
                if (child !== '')
                {
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            }
        });
        function format(d) {
            var sub_subject = d.arr_sub_subect;
            var tab_body = '';
            var tab_head = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
            $.each(sub_subject, function (key, value)
            {
                tab_body = tab_body + '<tr><td>' + value.subject_name + '</td>' +
                        '<td>' + value.subject_code + '</td>' +
                        '<td><div class="text-center"><button class="btn btn-primary open_edit_subject_model"  subject_id="' + value.subject_id + '"  type="button" title="Edit" request_type="open_edit" subject_name="' + d.subject_name + '" root_id="' + value.root_id + '"><i class="fas fa-edit"></i></button>\n\
                         <button type="button" class="btn btn-primary subject-data-delete" title="Remove" delete_type="subject" subject_group_id="' + d.subject_group_id + '" root_id="' + d.subject_id + '" subject_id="' + value.subject_id + '"><i class="fa fa-trash"></i></button>\n\
                         </div></td>' +
                        '</tr>';
            });
            var tab_foot = '</table>';
            var table_data = '';
            if (tab_body !== '')
            {
                table_data = tab_head + tab_body + tab_foot;
            }
            return table_data;
        }
        $(document).on('click', '#get_class_subject_group', function () {
            makeButtonDisable();
            if ($("#subject-group-from").valid())
            {
                initializeclassSession();
            } else
            {
                $("#subject-html-tables").html('');
            }
        });
        $(document).on('change', '#session_id,#class_id,#section_id', function () {
            makeButtonDisable();
            $("#subject-html-tables").html('');

        });
        function makeButtonDisable()
        {
            $("#sort-class-subject").attr('disabled', true);
            $("#import-group-subject").attr('disabled', true);
            $("#add-subject-group").attr('disabled', true);
        }
        function initializeclassSession()
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('initialize-class-session')}}",
                datatType: 'json',
                type: 'POST',
                data: {},
                success: function (response) {
                    getAllGroup();
                }
            });
        }
        function getAllGroup()
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-subject-group')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'class_id': $("#class_id").val(),
                    'session_id': $("#session_id").val(),
                    'section_id': $("#section_id").val(),
                },
                beforeSend: function () {
                    $("#LoadingImage").show();
                },
                success: function (response) {
                    if (response.status === 'success')
                    {
                        $("#sort-class-subject").attr('disabled', false);
                        $("#import-group-subject").attr('disabled', false);
                        $("#add-subject-group").attr('disabled', false);
                        var arr_group_id = response.arr_group_id;
                        $("#subject-html-tables").html(response.html_tables);
                        subjectGroup(arr_group_id);
                        $("#LoadingImage").hide();

                    }
                }
            });
        }
        $(document).on('click', '#add-subject-group,.edit-subject-group', function () {
            var group_type = $(this).attr('group_type');
            $("#new_group").val(null);
            $("#arr_subject_group_id").val(null);
            var subject_group_id = null;
            if (group_type === 'edit')
            {
                $("#group-edit-case").hide();
                $("#group_name").attr('readonly', false);
                var group_name = $(this).attr('subject_group_name');
                subject_group_id = $(this).attr('subject_group_id');
                $("#group_name").val(group_name);
                $("#group_type").val(group_type);
                $("#group_name").attr('subject_group_id', subject_group_id);
                $("#e_subject_group_id").val(subject_group_id);
            } else {
                $("#group-edit-case").show();
                $("#group_name").val('');
                $("#group_type").val(group_type);
                $("#e_subject_group_id").val('');
            }
            $("#add_subject_group").modal('show');
        });


        $(document).on('click', '#import-group-subject', function () {
            $("#import_from_class_id").val(null);
            $("#import_from_section_id").val(null);
            $('#group-subject-body').html('');
            $('#group-subject').modal('show');
        });
        $(document).on('change', '#import_from_class_id', '#import_from_session_id', function (e) {
            e.preventDefault();
            var section_id = $('#section_id').val(); // selected section where we want to import subject
            var class_id = $('#import_from_class_id').val();
            var session_id = $('#import_from_session_id').val();
            $('#import_from_section_id').empty();
            $('#import_from_section_id').append('<option value=""> -- Select -- </option>');
            if (class_id !== '' && session_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                        'session_id': session_id,
//                        'import_into_class_id': $("#class_id").val(),
//                        'import_into_session_id': $("#session_id").val(),
//                        'import_into_section_id': $("#session_id").val(),
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $.each(resopose_data, function (key, value) {
//                                if (key !== section_id) // skip selected section where subject will be imported
//                                {
                                $('#import_from_section_id').append('<option value="' + key + '">' + value + '</option>');
//                                }
                            });
                        }
                    }
                });
            }
        });
        $(document).on('change', '#import_from_section_id,#import_from_class_id,#import_from_session_id', function () {

            $('#group-subject-body').html(''); // clear exiting data from div 
            var import_from_class_id = $("#import_from_class_id").val();
            var import_from_section_id = $("#import_from_section_id").val();
            var import_from_session_id = $("#import_from_session_id").val();

            if (import_from_class_id !== '' && import_from_section_id !== '' && import_from_session_id !== '')
            {
                importGroupSubject();
            }
        });
        function importGroupSubject()
        {
            var class_id = $("#class_id").val();
            var session_id = $("#session_id").val();
            var section_id = $("#section_id").val();
            $('#group-subject-body').html('');
            if (class_id !== '' && session_id !== '' && section_id !== '')
            {
                var import_from_class_id = $("#import_from_class_id").val();
                var import_from_section_id = $("#import_from_section_id").val();
                var import_from_session_id = $("#import_from_session_id").val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-group-subject-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                        'section_id': section_id,
                        'session_id': session_id,
                        'import_from_session_id': import_from_session_id,
                        'import_from_class_id': import_from_class_id,
                        'import_from_section_id': import_from_section_id
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        if (response.status === 'success')
                        {
                            $('#group-subject-body').html(response.import_group_subject);

                        } else
                        {
                            $('#group-subject-body').html('<div style="text-align:center;padding: 5%;">No data available to import.</div>');
                        }
                    }
                });
            } else
            {
                $('#server-response-message').text('Please select class');
                $('#alertmsg-student').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
        }
        $(document).on('click', '#save_imported_subject', function () {
            if ($("#import-group-subject-form").valid())
            {
                var import_data = $('#import-group-subject-form').serializeArray();
                import_data.push({'name': 'session_id', 'value': $("#session_id").val()});
                import_data.push({'name': 'class_id', 'value': $("#class_id").val()});
                import_data.push({'name': 'section_id', 'value': $("#section_id").val()});
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('save-imported-subject')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: import_data,
                    success: function (response) {
                        $('#close-import-model').trigger('click');
                        if (response == 'success')
                        {
                            getAllGroup();
                            // Create new Notification
                            new PNotify({
                                title: 'Import Subject Group',
                                text: 'Subject group imported successfully',
                                type: 'success',
                                width: '290px',
                                delay: 1400
                            });
                        } else
                        {
                            //$('#server-response-message').text('Somthing went wrong, please try again');
                            /**$('#alertmsg-student').modal({
                                backdrop: 'static',
                                keyboard: false
                            });**/
                            new PNotify({
                                title: 'Import Subject Group',
                                text: 'Somthing went wrong, please try again ',
                                type: 'error',
                                width: '290px',
                                delay: 1400
                            });
                        }
                    }
                });
            }
        });

        $(document).on('click', '.open_subject_model', function () {

            $("#subject_group_id").val(null);
            $("#subject_name").val('');
            $("#subject_code").val('');
            $("#exclude_cgpa").prop('checked', false);
            $("#subject_group_id").val($(this).attr('subject_group_id'));
            var root_id = parseInt($(this).attr('root_id'));
            $("#root_id").val(root_id);
            $("#subjet_type_div").show(); // show for main subject
            var sub_prefix = '';
            if (root_id !== 0)
            {
                sub_prefix = ' Sub ';
                $("#subjet_type_div").hide(); // hide for sub subject
            }
            $("#subject_model_heading").text('Add ' + sub_prefix + 'Subject');
            $("#new_subject_label").text('Create New ' + sub_prefix + 'Subject?');
            $("#existing_subject_label").text('Select ' + sub_prefix + 'Subject');
            $("#subject_group_name").val($(this).attr('subject_group_name'));
            $("#group_temp_id").val($(this).attr('group_temp_id'));
            $('#add-subject-modal').modal('show');

        });
        $(document).on('change', '#new_subject', function () {

            $('select[name="subject_id"]').empty();
            $('#arr-subject-div').hide();
            $("#s_name_unique").hide();
            $('select[name="subject_id"]').append('<option value="">--Select subject--</option>');
            var s_type = $(this).val();
            $("#subject_name").attr('readonly', true);
            $("#subject_code").attr('readonly', true);
            $("#subject_fee").attr('readonly', true);
            $("#exclude_cgpa").attr('disabled', true);
            if (s_type == 1)
            {
                $("#subject_name").attr('readonly', false);
                $("#subject_code").attr('readonly', false);
                $("#subject_fee").attr('readonly', false);
                $("#exclude_cgpa").attr('disabled', false);
            } else if (s_type == 2)
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-subject-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        class_id: $("#class_id").val(),
                        session_id: $("#session_id").val(),
                        section_id: $("#section_id").val(),
                        subject_group_id: $("#subject_group_id").val(),
                        root_id: $("#root_id").val(),
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        if (response.status == 'success')
                        {
//                         if (response.arr_subject.length > 0)
                            if (response.arr_subject !== '')
                            {
                                $.each(response.arr_subject, function (key, value) {
                                    $('select[name="subject_id"]').append('<option value="' + value.subject_id + '" s_code="' + value.subject_code + '" s_name="' + value.name + '" s_fee="' + value.subject_fee + '">' + value.subject_name + '</option>');
                                });
                                $('#arr-subject-div').show();
                            } else
                            {
//                            $('#server-response-message').text('There is no subject in master');
//                            $('#alertmsg-student').modal({
//                                backdrop: 'static',
//                                keyboard: false
//                            });
                                $("#new_subject").val(1);
                                $("#subject_name").attr('readonly', false);
                                $("#subject_code").attr('readonly', false);
                                $("#subject_fee").attr('readonly', false);
                                $("#exclude_cgpa").attr('disabled', false);
                            }

                        }
                    }
                });
            }
        });

        $(document).on('change', '#subject_id', function () {

            $("#subject_name").val('');
            $("#subject_code").val('');
            $("#subject_fee").val(null);
            $("#exclude_cgpa").attr('checked', false);
            if ($(this).val() !== '')
            {
                $("#subject_name").val($('option:selected', this).attr('s_name'));
                $("#subject_code").val($('option:selected', this).attr('s_code'));
                $("#subject_fee").val(0);
            }
        });

        $(document).on('click', '.open_edit_subject_model,#edit_subject', function () {

            $("#e_subjet_type_div").hide(); // show for main subject
            $("#e_s_name_unique").hide();
            var  root_id = '';
            var request_type = $(this).attr('request_type');
            $("#s_request_type").val('');
            if (request_type === 'open_edit')
            {
                $("#e_subject_id").val($(this).attr('subject_id'));
                $("#e_class_subject_id").val($(this).attr("class_subject_id"));
                $("#s_request_type").val(request_type);
                root_id = $(this).attr('root_id');
//                $("#e_subject_type_dropdown").html('');
            } else
            {
                $("#s_request_type").val(request_type);
            }
            var edit_subject_data = $('#edit-subject-form').serializeArray();

            if (($("#edit-subject-form").valid() && request_type === 'save_edit') || request_type === 'open_edit')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('edit-subject')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: edit_subject_data,
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        if (response.status == 'success')
                        {
                            if (request_type === 'open_edit')
                            {
                                $("#e_subject_name").val(response.data.subject_name);
                                $("#e_subject_code").val(response.data.subject_code);
                                if(root_id == 0)
                                {
                                    $("#e_subjet_type_div").show();
                                }
                                
//                                });
                                $('#edit-subject-modal').modal('show');
                            } else
                            {
                                getAllGroup();
                                $('#close-edit-subject-model').trigger('click');
                            }
                        } else if (response.status === 'name_used')
                        {
                            $("#e_s_name_unique").text('Subject name already exist, please enter something new');
                            $("#e_s_name_unique").show();
                        }
                    }
                });
            }

        });

        $(document).on('click', '.subject-data-delete', function () {
            var subject_id = $(this).attr('subject_id');
            var subject_group_id = $(this).attr('subject_group_id');
            var delete_type = $(this).attr('delete_type');
            var group_temp_id = $(this).attr('group_temp_id');
            if (delete_type !== '' && (subject_id !== '' || subject_group_id !== '' || group_temp_id !== ''))
            {
                bootbox.confirm({
                    message: "Are you sure to delete ?",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result)
                        {
                            $.ajax(
                                    {
                                        url: "{{ url('subject-data-delete') }}",
                                        datatType: 'json',
                                        type: 'POST',
                                        data: {
                                            'class_id': $("#class_id").val(),
                                            'session_id': $("#session_id").val(),
                                            'subject_id': subject_id,
                                            'subject_group_id': subject_group_id,
                                            'section_id': $("#section_id").val(),
                                            'delete_type': delete_type,
                                            'group_temp_id': group_temp_id,
                                        },
                                        success: function (res)
                                        {
                                            if (res.status == "success")
                                            {
                                                getAllGroup();
                                            } else if (res.status === "used")
                                            {
                                                $('#server-response-message').text(res.message);
                                                $('#alertmsg-student').modal({
                                                    backdrop: 'static',
                                                    keyboard: false
                                                });
                                            } else
                                            {
                                                $('#server-response-message').text('Something went wrong, Please try again');
                                                $('#alertmsg-student').modal({
                                                    backdrop: 'static',
                                                    keyboard: false
                                                });
                                            }
                                        }
                                    });
                        }
                    }
                });
            }
        });


        $(document).on('click', '.include_in_card,.include_in_rank', function () {

            var include_in_card = null;
            var include_in_rank = null;
            var class_id = $(this).attr('g_class_id');
            var type = $(this).attr('include_type');
            if (type == 'card')
            {
                if ($(this).is(':checked'))
                {
                    include_in_card = 1;
                } else
                {
                    include_in_card = 0;
                }
            }
            if (type == 'rank')
            {
                if ($(this).is(':checked'))
                {
                    include_in_rank = 1;
                } else
                {
                    include_in_rank = 0;
                }
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('include-group-in-result')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    subject_group_id: $(this).val(),
                    class_id: class_id,
                    session_id: $("#session_id").val(),
                    section_id: $("#section_id").val(),
                    include_in_card: include_in_card,
                    include_in_rank: include_in_rank,
                    type: type,
                },
                success: function (response) {
                    if (response == 'success')
                    {
                        getAllGroup();
                        $('#close-sub-subject-model').trigger('click');
                    } else
                    {
                        $('#server-response-message').text('Somthing went wrong, please try again');
                        $('#alertmsg-student').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                }
            });
        });

        $(document).on('click', '.student-assign-unassign,#save_unassigned_student', function () {

            var request_type = $(this).attr('request_type');
            $("#student-map-subject").text($(this).attr('subject_name'));
            var unassigned_student_id = [];
            var assigned_student_id = [];
            if (request_type === 'get_student')
            {
                var class_subject_id = $(this).attr('class_subject_id');
                $("#unassign_class_subject_id").val(class_subject_id);
            } else
            {
                var class_subject_id = $("#unassign_class_subject_id").val();
                $("input[name='unassigned_student_id[]']").each(function () {
                    if ($(this).is(':checked'))
                    {
                        unassigned_student_id.push($(this).val());
                    }
                });
                $("input[name='assigned_student_id[]']").each(function () {
                    if ($(this).is(':checked'))
                    {
                        assigned_student_id.push($(this).val());
                    }
                });
            }
            $("#save_unassigned_student").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('student-assign-unassign')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'class_id': $("#class_id").val(),
                    'session_id': $("#session_id").val(),
                    'section_id': $("#section_id").val(),
                    'request_type': request_type,
                    'class_subject_id': class_subject_id,
                    'unassigned_student_id': unassigned_student_id,
                    'assigned_student_id': assigned_student_id,
                },
                beforeSend: function () {
                    $("#LoadingImage").show();
                },
                success: function (response) {
                    console.log(response);
                    $("#LoadingImage").hide();
                    if (response.status == 'success')
                    {
                        $("#assign-unassign-body").html(response.student_list);
                        if (response.action_enable === true)
                        {
                            $("#save_unassigned_student").hide();
                        }
                        $("#student-assign-unassign-modal").modal('show');
                    } else
                    {
                        $('#server-response-message').text('Somthing went wrong, please try again');
                        $('#alertmsg-student').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                }
            });
        });

//        $(document).on('change', '#sort_class_id', function (e) {
//            e.preventDefault();
//            var sort_class_id = $(this).val();
//            var session_id = $('#session_id').val();
//            $('#sort_section_id').empty();
//            $('#sort_section_id').append('<option> -- Select -- </option>');
//            $('#sortable').html('');
//            if (sort_class_id !== '' && session_id !== '')
//            {
//                $.ajax({
//                    headers: {
//                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
//                    },
//                    url: "{{url('get-section-list')}}",
//                    datatType: 'json',
//                    type: 'POST',
//                    data: {
//                        'class_id': sort_class_id,
//                        'session_id': session_id,
//                    },
//                    beforeSend: function () {
//                        $("#LoadingImage").show();
//                    },
//                    success: function (response) {
//                        $("#LoadingImage").hide();
//                        var resopose_data = [];
//                        resopose_data = response.data;
//                        if (response.status === 'success')
//                        {
//                            $.each(resopose_data, function (key, value) {
//                                $('#sort_section_id').append('<option value="' + key + '">' + value + '</option>');
//                            });
//                        }
//                    }
//                });
//            }
//        });

        $(document).on('click', '#sort-class-subject', function () {
            var class_id = $('#class_id').val();
            var section_id = $("#section_id").val();
            $('#sortable').html('');
            if (class_id !== '' && section_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-class-subject-order')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        class_id: class_id,
                        section_id: section_id,
                        session_id: $("#session_id").val(),
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        if (response.status === 'success')
                        {
                            $.each(response.data, function (key, value) {
                                $('#sortable').append('<li class="ui-state-default" style="" value="' + value.class_subject_id + '">' + value.subject_name + '</li>');
                            });

                        } else
                        {
                            $('#sortable').append('<li>No data available to sort</li>');
                        }
                    }
                });
                
                $("#sort-class-subject-modal").modal('show');
            }
        });

        $("#sortable").sortable({
            update: function () {
                var order = $('#sortable').sortable('toArray', {attribute: 'value'});
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    datatType: 'json',
                    type: 'POST',
                    async: true,
                    url: "{{url('set-class-subject-order')}}",
                    data: {
                        "item": order,
                    }
                }).done(function (data) {
                });
            }
        });
        $(document).on('change', '#new_group', function () {
            $('select[name="arr_subject_group_id"]').empty();
            $('select[name="arr_subject_group_id"]').append('<option value="">-- Select Group --</option>');
            $('#arr-subject-group-div').hide();
            var g_type = $(this).val();
            $("#group_name").attr('readonly', true);
            $("#group_name").val('');
            if (g_type == 1)
            {
                $("#group_name").attr('readonly', false);
            } else if (g_type == 2)
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-group')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {},
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        if (response.status == 'success')
                        {
//                        if (response.arr_group.length > 0)
//                        {
                            $.each(response.arr_group, function (key, value) {
                                $('select[name="arr_subject_group_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                            $('#arr-subject-group-div').show();

//                        } else
//                        {
//                            $("#new_group").val(1);
//                            $("#group_name").attr('readonly', false);
//                        }

                        } else
                        {
                            $('#server-response-message').text('Something went wrong, please try again ');
                            $('#alertmsg-student').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        }
                    }
                });

            }
        });
        $(document).on('change', '#group_name', function () {
            var group_name = $(this).val();
            var type = $(this).attr('call_type');
            var subject_group_id = $(this).attr('subject_group_id');
            checkGroupExist(group_name, subject_group_id, type);
        });
        $(document).on('click', '#save_group', function () {
            var group_name = $("#group_name").val();
            var type = $(this).attr('call_type');
            var subject_group_id = $("#e_subject_group_id").val();
            checkGroupExist(group_name, subject_group_id, type);
        });

        function saveGroup()
        {
            if ($("#group-from").valid())
            {
                var add_group_data = $('#group-from').serializeArray();
                add_group_data.push({name: 'class_id', value: $("#class_id").val()});
                add_group_data.push({name: 'session_id', value: $("#session_id").val()});
                add_group_data.push({name: 'section_id', value: $("#section_id").val()});
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('add-subject-group')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: add_group_data,
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        $('#close-group-model').trigger('click');
                        if (response == 'success')
                        {
                            if ($("#group_type").val() === 'edit')
                            {
                                //$('#server-response-success').text('Group updated successfully');
                                new PNotify({
                                    title: 'Subject Group',
                                    text: 'Subject Group updated successfully',
                                    type: 'success',
                                    width: '290px',
                                    delay: 1400
                                });
                            } else
                            {
                                $('#server-response-success').text('Group saved successfully  in temporary session, So please add subject(s) into this before proceeding further.');
                                $('#alertmsg-success').modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                            }
                            getAllGroup();
                        } else if (response == 'used')
                        {
                            new PNotify({
                                title: 'Subject Group',
                                text: 'Group already exist in selected class',
                                type: 'warning',
                                width: '290px',
                                delay: 1400
                            });
                            /** $('#server-response-message').text('Group already exist in selected class');
                            $('#alertmsg-student').modal({
                                backdrop: 'static',
                                keyboard: false
                            });**/
                        } else
                        {
                            new PNotify({
                                title: 'Subject Group',
                                text: 'Somthing went wrong, please try again',
                                type: 'error',
                                width: '290px',
                                delay: 1400
                            });
                            /**
                            $('#server-response-message').text('Somthing went wrong, please try again');
                            $('#alertmsg-student').modal({
                                backdrop: 'static',
                                keyboard: false
                            });**/
                        }
                    }
                });
            }
        }
        function checkGroupExist(group_name, subject_group_id, type)
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('check-group-name')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    group_name: group_name,
                    subject_group_id: subject_group_id
                },
                success: function (response) {
                    if (response === false)
                    {
                        $("#group_unique").text('Group name already exist, please reuse existing group');
                        $("#group_unique").show();
                    } else if (response === true)
                    {
                        $("#group_unique").hide();
                        if (type === 'save')
                        {
                            saveGroup();
                        }
                    } else
                    {
                        $('#server-response-message').text('Something went wrong, please try again ');
                        $('#alertmsg-student').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                }
            });
        }

        $(document).on('click', '#save_subject', function () {
            checkSubjectExist();
        });

        function saveSubject()
        {
            if ($("#add-subject-form").valid())
            {
                var add_subject_data = $('#add-subject-form').serializeArray();
                add_subject_data.push({name: 'class_id', value: $("#class_id").val()});
                add_subject_data.push({name: 'session_id', value: $("#session_id").val()});
                add_subject_data.push({name: 'section_id', value: $("#section_id").val()});
                add_subject_data.push({name: 'root_id', value: $("#root_id").val()});
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('add-subject')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: add_subject_data,
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        if (response == 'success')
                        {
                            getAllGroup();
                            $('#close-subject-model').trigger('click');
                        }
                    }
                });
            }
        }
        function checkSubjectExist()
        {
            if ($("#add-subject-form").valid())
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('check-subject-name')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: $('#add-subject-form').serializeArray(),
                    success: function (response) {

                        if (response.status === false)
                        {
                            $("#s_name_unique").text(response.message);
                            $("#s_name_unique").show();
                        } else if (response.status === true)
                        {
                            $("#s_name_unique").hide();
                            saveSubject();
                        } else
                        {
                            $('#server-response-message').text('Something went wrong, please try again ');
                            $('#alertmsg-student').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        }
                    }
                });
            }
        }

        $(document).on('change', '#arr_subject_group_id', function () {

            $("#group_name").val('');
            if ($(this).val() !== '')
            {
                $("#group_name").val($('option:selected', this).text());
                $("#e_subject_group_id").val($('option:selected', this).val());
            }
        });

        $(document).on('hidden.bs.modal', '#sort-class-subject-modal', function () {
            getAllGroup();
        });
//        $(document).on('click', '#sort-class-subject', function () {
//            $("#sort_class_id").val(null);
//            $("#sort_section_id").val(null);
//            $("#sortable").html('');
//            $("#sort-class-subject-modal").modal('show');
//        });

        $(document).on('change', '#class_id,#session_id', function (e) {
            e.preventDefault();
            var class_id = $('#class_id').val();
            var session_id = $('#session_id').val();
            $('#section_id').empty();
            $('#section_id').append('<option value=""> -- Select -- </option>');
            if (class_id !== '' && session_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                        'session_id': session_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $.each(resopose_data, function (key, value) {
                                $('#section_id').append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                    }
                });
            }

        });
    });
</script>
</body>
</html>
@endsection