<div class="admin-form">
    @if(!empty($arr_group_subject))
    @foreach($arr_group_subject as $key=> $group_subject)
    @if(!empty($group_subject['group_subject']))
    <div class="col-md-12">
        <div class="col-md-3 option-group field" id="" style="margin-top: 22px;">
            <label class="option block option-primary" style="margin-right: 0px !important; ">
                {!! Form::checkbox('subject_group_id[]',$group_subject['subject_group_id'],'', ['class' => 'gui-input select_subject form-control', 'id' => 'subject_group_id'.$group_subject['subject_group_id'],'rel'=>$group_subject['subject_group_id'],'required'=>true]) !!}
                <span class="checkbox radioBtnpan"></span><em>{!! $group_subject['group_name'] !!}</em>
            </label>
        </div>
        <div class="col-md-8" id="">
            <div class="" style="margin-top: 7px;"><label>Select subject</label></div>
            <label class="option block option-primary" style="margin-right: 0px !important; ">
                {!!Form::select('subject_id_'.$group_subject['subject_group_id'].'[]', $group_subject['group_subject'],'', ['class' => 'form-control  subject_list','id'=>'subject_id_'.$group_subject['subject_group_id'],'multiple'=>'multiple','required'=>true])!!}
            </label>
        </div>
        <div class="clearfix"></div>
        @endif
    </div>
    @endforeach
    @else
    <!--<span class="import_subject_data">No data found.</span>-->
    @endif
</div>
<script>
    $(document).ready(function (e) {
        $(".subject_list").select2();
        $(".select_subject").click(function () {
            var rel = $(this).attr('rel');
            if ($("#subject_group_id" + rel).is(':checked')) {
                $("#subject_id_" + rel + " > option").prop("selected", "selected").parent().trigger('change');
            } else {
                $("#subject_id_" + rel + " > option").removeAttr("selected").parent().trigger('change');
            }
        });
    });
</script>
