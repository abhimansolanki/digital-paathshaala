@if(!empty($arr_group))
@foreach($arr_group as $key => $subject_group)
@php $count = $key +1; 
$card_checked = false; 
$rank_checked = false;
$disabled = 'disabled';
@endphp
@if(!empty($subject_group['subject_group_id']))
@php $disabled = ''; @endphp
@endif
<div class="row mainsubject">
    <div class="col-md-3">
        <h2>{{$subject_group['group_name']}}</h2>
    </div>
    <div class="col-md-3">
        <label class="field option block">
            @if($subject_group['include_in_card'] == 1)
            @php $card_checked = true; @endphp
            @endif
            {!! Form::checkbox('include_in_card',$subject_group['subject_group_id'],$card_checked, ['class' => 'include_in_card', 'id' => '','include_type'=>'card','g_class_id'=>$class_id,$disabled]) !!}             
            <span class="checkbox mr10"></span>
            Add to Report Card
        </label>
    </div>
    <div class="col-md-3">
        <label class="field option block">
            @if($subject_group['include_in_rank'] == 1)
            @php $rank_checked = true; @endphp
            @endif
            {!! Form::checkbox('include_in_card',$subject_group['subject_group_id'],$rank_checked, ['class' => 'include_in_rank', 'id' => '','include_type'=>'rank','g_class_id'=>$class_id,$disabled]) !!}
            <span class="checkbox mr10"></span>
            Add to Result's Rank
        </label>
    </div>
    <div class="col-md-2 buttons">
        <button type="button" class="open_subject_model" title="Add Subject" data-toggle="modal" data-backdrop="static" data-keyboard="false" root_id="0" subject_group_id="{{$subject_group['subject_group_id']}}" subject_group_name="{{$subject_group['group_name']}}" group_temp_id="{{$subject_group['group_temp_id']}}">
            <i class="fas fa-plus-square"></i>
        </button>
        <button type="button" class="edit-subject-group" title="Edit Group" data-toggle="modal" data-backdrop="static" data-keyboard="false" subject_group_id="{{$subject_group['subject_group_id']}}" g_class_id="{{$class_id}}" group_type="edit" subject_group_name="{{$subject_group['group_name']}}" <?php echo $disabled; ?> >
            <i class="fas fa-edit" style="color: green;"></i>
        </button>
        <button type="button" title="Remove Group" class="subject-data-delete" delete_type='subject_group' subject_group_id='{{$subject_group['subject_group_id']}}' subject_id='' sub_subject_id='' g_class_id='{{$class_id}}' group_temp_id="{{$subject_group['group_temp_id']}}">
            <i class="fa fa-trash"></i>
        </button>
    </div>
    <div class="clearfix"></div>
</div>
<table class="table table-bordered table-striped table-hover tablecustom12" id="subject-group{{$count}}" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Subject Name</th>
            <th>Code</th>
            <!--<th>Type</th>-->
            <th class="text-center">Break Subject</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
</table>
@endforeach

<!--  modal for add for add subject -->
<!--    Add subject-->
<div id="add-subject-modal" class="modal commonboxinput admin-form" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="subject_model_heading"></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['id' => 'add-subject-form' , 'class'=>'form-horizontal','url' => '','autocomplete'=>'off']) !!}
                    {!! Form::hidden('group_temp_id','', ['class' => '','id' => 'group_temp_id','readonly'=>true]) !!}
                    {!! Form::hidden('subject_group_id','', ['class' => '','id' => 'subject_group_id','readonly'=>true]) !!}
                    {!! Form::hidden('subject_id','', ['class' => '','id' => 'subject_id','readonly'=>true]) !!}
                    {!! Form::hidden('root_id','', ['class' => '','id' => 'root_id','readonly'=>true]) !!}
                    {!! Form::hidden('subject_group_name','', ['class' => '','id' =>'subject_group_name','readonly'=>true]) !!}

                    <div class="col-md-5 section">
                        <label><span class="radioBtnpan" id="new_subject_label" style="float:left;"></span>
                        <span class="asterisk" style="float:left;">*</span></label>
                        <label for="new_subject" class="field select" style="margin-top: 0px;">
                            {!! Form::select('new_subject',$arr_new_subject,'',['class' => 'form-control', 'id' => 'new_subject','required'=>true]) !!}
                            <i class="arrow double"></i>
                        </label>
                    </div>
                    <div class="col-md-5 section" id="arr-subject-div" style="display: none">
                        <label><span class="radioBtnpan" id="existing_subject_label"><span class="asterisk">*</span></span></label>
                        <label for="arr_subject" class="field select" style="margin-top: 0px;">
                            {!! Form::select('subject_id',[], '',['class' => 'form-control', 'id' => 'subject_id','required'=>true]) !!}
                            <i class="arrow double"></i>
                        </label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-5" style="margin-top: 20px;">
                        <label><span class="radioBtnpan">Name<span class="asterisk">*</span></span></label>
                        <label for="subject_name" class="field prepend-icon" style="margin-top: -10px;">
                            {!! Form::text('subject_name','', ['class' => '', 'id' => 'subject_name','required'=>true,'check_type'=>'name','pattern'=>'[a-zA-Z][a-zA-Z0-9\s\.]*']) !!}
                        </label>
                    </div>
                    <div class="col-md-5" style="margin-top: 20px;">
                        <label><span class="radioBtnpan">Code<span class="asterisk">*</span></span></label>
                        <label for="subject_code" class="field prepend-icon" style="margin-top: -10px;">
                            {!! Form::text('subject_code','', ['class' => '', 'id' => 'subject_code','required'=>true,'check_type'=>'code','pattern'=>'[a-zA-Z][a-zA-Z0-9\s\.]*']) !!}
                        </label>
                    </div>
                    <div class="clearfix"></div>
                    <span class="error" id="s_name_unique" style="display:none;padding-left: 12px;"></span>
                    <div class="clearfix"></div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer closebtnforstudent" id="">
                <button type="button" class="button btn-primary" id="save_subject" check_type='save'>Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="close-subject-model">Close</button>
            </div>
        </div>
    </div>
</div>
<!--    Edit subject-->
<div id="edit-subject-modal" class="modal commonboxinput" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="edit_subject_heading">Edit Subject</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['id' => 'edit-subject-form' , 'class'=>'form-horizontal','url' => '','autocomplete'=>'off']) !!}
                {!! Form::hidden('s_request_type','', ['class' => '','id' => 's_request_type','readonly'=>true]) !!}
                {!! Form::hidden('e_subject_id','', ['class' => '','id' => 'e_subject_id','readonly'=>true]) !!}
                {!! Form::hidden('e_class_subject_id','', ['class' => '','id' => 'e_class_subject_id','readonly'=>true]) !!}

                <div class="col-md-5">
                    <label><span class="radioBtnpan">Name<span class="asterisk">*</span></span></label>
                    <label for="e_subject_name" class="field prepend-icon" style="margin-top: -10px;">
                        {!! Form::text('e_subject_name','', ['class' => '', 'id' => 'e_subject_name','required'=>true,'pattern'=>'[a-zA-Z][a-zA-Z0-9\s\.]*']) !!}
                    </label>
                </div>
                <div class="col-md-5">
                    <label><span class="radioBtnpan">Code<span class="asterisk">*</span></span></label>
                    <label for="e_subject_code" class="field prepend-icon" style="margin-top: -10px;">
                        {!! Form::text('e_subject_code','', ['class' => '', 'id' => 'e_subject_code','required'=>true,'pattern'=>'[a-zA-Z][a-zA-Z0-9\s\.]*']) !!}
                    </label>
                </div>
<!--                <div class="clearfix"></div>
                <div class="col-md-5 section" id="e_subjet_type_div">
                    <label><span class="radioBtnpan" id="subject_type_label">Type<span class="asterisk">*</span></span></label>
                    <label for="arr_subject" class="field prepend-icon" style="margin-top: 0px;" id="e_subject_type_dropdown">
                        Selected subject type dropdown displaying here when open edit model.
                        <i class="arrow double"></i>
                    </label>
                </div>-->
                <span class="error" id="e_s_name_unique" style="display:none;padding-left: 12px;"></span>
                <div class="clearfix"></div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer closebtnforstudent" id="">
                <button type="button" class="button btn-primary" id="edit_subject" request_type="save_edit">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="close-edit-subject-model">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Add subject break -->
<div id="add-sub-subject" class="modal commonboxinput" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Subject Break<span id="b_subject_name"></span></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['id' => 'add-sub-subject-form' , 'class'=>'form-horizontal','url' => '','autocomplete'=>'off']) !!}
                {!! Form::hidden('class_subject_id','', ['class' => '','id' => 'class_subject_id','readonly'=>true]) !!}
                {!! Form::hidden('s_subject_id','', ['class' => '','id' => 's_subject_id','readonly'=>true]) !!}
                <div class="col-md-5 section">
                    <label><span class="radioBtnpan">New sub Subject<span class="asterisk">*</span></span></label>
                    <label for="new_subject" class="field select" style="margin-top: 0px;">
                        {!! Form::select('new_sub_subject',$arr_new_subject,'',['class' => 'form-control', 'id' => 'new_sub_subject','required'=>true]) !!}
                        <i class="arrow double"></i>
                    </label>
                </div>
                <div class="col-md-5 section" id="arr-sub-subject-div" style="display: none">
                    <label><span class="radioBtnpan">Existing Sub-Subject<span class="asterisk">*</span></span></label>
                    <label for="arr_subject" class="field select" style="margin-top: 0px;">
                        {!! Form::select('sub_subject_id',[], '',['class' => 'form-control', 'id' => 'sub_subject_id','required'=>true]) !!}
                        <i class="arrow double"></i>
                    </label>
                </div>
                <div class="clearfix"></div>
                <span class="error" id="server-response-alert" style="display:none;padding-left: 12px;"></span>
                <div class="clearfix"></div>
                <div class="col-md-5" style="margin-top: 20px;">
                    <label><span class="radioBtnpan">Sub-Subject Name<span class="asterisk">*</span></span></label>
                    <label for="sub_subject_name" class="field prepend-icon" style="margin-top: -10px;">
                        {!! Form::text('sub_subject_name','', ['class' => '', 'id' => 'sub_subject_name','required'=>true,'pattern'=>'[a-zA-Z][a-zA-Z0-9\s\.]*']) !!}
                    </label>
                </div>
                <div class="col-md-5" style="margin-top: 20px;">
                    <label><span class="radioBtnpan">Sub-Subject Code<span class="asterisk">*</span></span></label>
                    <label for="sub_subject_code" class="field prepend-icon" style="margin-top: -10px;">
                        {!! Form::text('sub_subject_code','', ['class' => '', 'id' => 'sub_subject_code','required'=>true,'pattern'=>'[a-zA-Z][a-zA-Z0-9\s\.]*']) !!}
                    </label>
                </div>
                <div class="clearfix"></div>
                <span class="error" id="sub_subject_name_unique" style="display:none;padding-left: 12px;"></span>
                <div class="clearfix"></div>
                <div class="col-md-5" style="margin-top: 10px;">
                    <label><span class="radioBtnpan">Fee (If any)</span></label>
                    <label for="sub_subject_fee" class="field prepend-icon" style="margin-top: -10px;">
                        {!! Form::number('sub_subject_fee','', ['class' => '', 'id' => 'sub_subject_fee','min'=>0]) !!}
                    </label>
                </div>
                <div class="clearfix"></div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer closebtnforstudent" id="">
                <button type="button" class="button btn-primary" id="save_sub_subject">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="close-sub-subject-model">Close</button>
            </div>
        </div>
    </div>
</div>
<!--Edit Sub-Subject-->
<div id="edit-sub-subject" class="modal commonboxinput" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Sub-Subject of <span id="master_subject_name"></span></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['id' => 'edit-sub-subject-form' , 'class'=>'form-horizontal','url' => '','autocomplete'=>'off']) !!}
                {!! Form::hidden('e_sub_subject_id','', ['class' => '','id' => 'e_sub_subject_id','readonly'=>true]) !!}
                {!! Form::hidden('ss_request_type','', ['class' => '','id' => 'ss_request_type','readonly'=>true]) !!}

                <div class="col-md-5">
                    <label><span class="radioBtnpan">Sub-Subject Name<span class="asterisk">*</span></span></label>
                    <label for="e_sub_subject_name" class="field prepend-icon" style="margin-top: -10px;">
                        {!! Form::text('e_sub_subject_name','', ['class' => '', 'id' => 'e_sub_subject_name','required'=>true,'pattern'=>'[a-zA-Z][a-zA-Z0-9\s\.]*']) !!}
                    </label>
                </div>
                <div class="col-md-5">
                    <label><span class="radioBtnpan">Sub-Subject Code<span class="asterisk">*</span></span></label>
                    <label for="e_sub_subject_code" class="field prepend-icon" style="margin-top: -10px;">
                        {!! Form::text('e_sub_subject_code','', ['class' => '', 'id' => 'e_sub_subject_code','required'=>true,'pattern'=>'[a-zA-Z][a-zA-Z0-9\s\.]*']) !!}
                    </label>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-5">
                    <label><span class="radioBtnpan">Fee (If any)</span></label>
                    <label for="e_sub_subject_fee" class="field prepend-icon" style="margin-top: -10px;">
                        {!! Form::number('e_sub_subject_fee','', ['class' => '', 'id' => 'e_sub_subject_fee','min'=>0]) !!}
                    </label>
                </div>
                <div class="clearfix"></div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer closebtnforstudent" id="">
                <button type="button" class="button btn-primary" id="edit_sub_subject" request_type="save_edit">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="close-edit-sub-subject-model">Close</button>
            </div>
        </div>
    </div>
</div>
@endif

