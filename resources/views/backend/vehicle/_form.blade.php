<style type="text/css">
    .btn-bs-file{
        position:relative;
    }
    .radioBtnpan a{
        color: #4a89dc;
        font-size: 15px;
        padding-left: 10px !important;
    }
    .fade-scale {
        transform: scale(0);
        opacity: 0;
        -webkit-transition: all .25s linear;
        -o-transition: all .25s linear;
        transition: all .25s linear;
    }
    .fade-scale.in {
        opacity: 1;
        transform: scale(1);
    }
    .btn-bs-file input[type="file"]{
        position: absolute;
        top: -9999999;
        filter: alpha(opacity=0);
        opacity: 0;
        width:0;
        height:0;
        outline: none;
        cursor: inherit;
    }
    .btn{
        margin-top: 0px !important;
    }
    .btncustom{
        border: 1px solid #ccc;
        padding: 0px 0px
    }
    .admin-form .select > select{
        height: 35px !important
    }
    .admin-form .select{
        margin-top: 0px;
    }
    .state-error{
        display: block!important;
        margin-top: 6px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    }
    .modal-header .close {
        margin-top: -10px;
        font-size: 21px;
        color: red;
        opacity: initial !important;
        background: #fff;
    }
    .modal-backdrop{
        background: transparent;
    }
</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_vehicle')}}</li>
                </ol>
            </div>
            <div class="topbar-right">
                <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right">{{trans('language.view_vehicle')}}</a>
            </div>
        </div>

        {!! Form::hidden('vehicle_provider_id',isset($vehicle['vehicle_provider_id']) ? $vehicle['vehicle_provider_id'] : '', ['class' => '','id' => 'vehicle_provider_id','readonly'=>true]) !!}
        {!! Form::hidden('vehicle_driver_id',isset($vehicle['vehicle_driver_id']) ? $vehicle['vehicle_driver_id'] : '', ['class' => '','id' => 'vehicle_driver_id','readonly'=>true]) !!}
        {!! Form::hidden('vehicle_helper_id',isset($vehicle['vehicle_helper_id']) ? $vehicle['vehicle_helper_id'] : '', ['class' => '','id' => 'vehicle_helper_id','readonly'=>true]) !!}
        <!--@include('backend.partials.messages')-->
        <div class="bg-light panelBodyy">
            <div class="section-divider mv40">
                <span><i class="fa fa-user"></i> Vehicle Provider Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.contact_number') !!}<span class="asterisk">*</span></span></label>
                    <label for="provider_contact_number" class="field prepend-icon">
                        {!! Form::number('provider_contact_number', old('provider_contact_number',isset($vehicle['provider_contact_number']) ? $vehicle['provider_contact_number'] : ''), ['class' => 'gui-input', 'id' => 'provider_contact_number','request-type'=>'provider','min'=>0]) !!}
                        <label for="provider_contact_number" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('provider_contact_number')) <p class="help-block">{{ $errors->first('provider_contact_number') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.full_name')}}<span class="asterisk">*</span></span></label>
                    <label for="provider_full_name" class="field prepend-icon">
                        {!! Form::text('provider_full_name', old('provider_full_name',isset($vehicle['provider_full_name']) ? $vehicle['provider_full_name'] : ''), ['class' => 'gui-input','id' => 'provider_full_name','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="provider_full_name" class="field-icon">
                            <i class="fa fa-bus"></i>
                        </label>
                    </label>
                    @if ($errors->has('provider_full_name')) 
                    <p class="help-block">{{ $errors->first('provider_full_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.email') !!}</span></label>
                    <label for="provider_email_address" class="field prepend-icon">
                        {!! Form::email('provider_email_address', old('provider_email_address',isset($vehicle['provider_email_address']) ? $vehicle['provider_email_address'] : ''), ['class' => 'gui-input', 'id' => 'provider_email_address']) !!}
                        <label for="provider_email_address" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('provider_email_address')) <p class="help-block">{{ $errors->first('provider_email_address') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.address') !!}<span class="asterisk">*</span></span></label>
                    <label for="provider_address" class="field prepend-icon">
                        {!! Form::text('provider_address', old('address',isset($vehicle['provider_address']) ? $vehicle['provider_address'] : ''), ['class' => 'gui-input', 'id' => 'provider_address']) !!}
                        <label for="provider_address" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                        @if ($errors->has('provider_address')) <p class="help-block">{{ $errors->first('provider_address') }}</p> @endif
                    </label>
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.aadhaar_card') !!}<span class="asterisk">*</span></span></label>
                    <label for="provider_aadhaar_number" class="field prepend-icon">
                        {!! Form::number('provider_aadhaar_number', old('provider_aadhaar_number',isset($vehicle['provider_aadhaar_number']) ? $vehicle['provider_aadhaar_number'] : ''), ['class' => 'gui-input', 'id' => 'provider_aadhaar_number','min'=>0]) !!}
                    </label>
                    @if ($errors->has('provider_aadhaar_number')) <p class="help-block">{{ $errors->first('provider_aadhaar_number') }}</p> @endif
                </div>  
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.upload_aadhaar') !!}<span class="asterisk">*</span>
                            @if(isset($vehicle['provider_aadhaar']) && !empty($vehicle['provider_aadhaar']))
                            <a href="" data-toggle="modal" data-target="#img1" data-backdrop="static" data-keyboard="false">Click to view </a>
                            @endif
                        </span>
                    </label>
                    <label class="btn-bs-file btn btn-xs col-md-12 btncustom">
                        <div class="col-md-6 classfontsize">
                            <span class="glyphicons glyphicons-upload"></span>
                            {!! trans('language.upload_aadhaar') !!}
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4 fontsizeand">
                            <span class="glyphicons glyphicons-search"></span>  Browse file </div>
                        {!! Form::file('provider_aadhaar'); !!}
                    </label>
                    @if ($errors->has('provider_aadhaar')) 
                    <p class="help-block">{{ $errors->first('provider_aadhaar') }}</p>
                    @endif
                    <div id="img1" class="modal fade-scale" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    @if(isset($vehicle['provider_aadhaar']) && !empty($vehicle['provider_aadhaar']))
                                    <img src="{{url($vehicle['provider_aadhaar'])}}" style="width: 100%">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-light panelBodyy">
            <div class="section-divider mv40">
                <span><i class="fa fa-bus"></i>&nbsp;Vehicle Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.vehicle_number') !!}<span class="asterisk">*</span></span></label>
                    <label for="vehicle_number" class="field prepend-icon">
                        {!! Form::text('vehicle_number', old('vehicle_number',isset($vehicle['vehicle_number']) ? $vehicle['vehicle_number'] : ''), ['class' => 'gui-input', 'id' => 'vehicle_number','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="vehicle_number" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('vehicle_number')) <p class="help-block">{{ $errors->first('vehicle_number') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.vehicle_name')}}<span class="asterisk">*</span></span></label>
                    <label for="vehicle_name" class="field prepend-icon">
                        {!! Form::text('vehicle_name', old('vehicle_name',isset($vehicle['vehicle_name']) ? $vehicle['vehicle_name'] : ''), ['class' => 'gui-input', 'id' => 'vehicle_name','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="vehicle_name" class="field-icon">
                            <i class="fa fa-bus"></i> 
                        </label>
                    </label>
                    @if ($errors->has('vehicle_name')) 
                    <p class="help-block">{{ $errors->first('vehicle_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.vehicle_color') !!}<span class="asterisk">*</span></span></label>
                    <label for="vehicle_color" class="field prepend-icon">
                        {!! Form::text('vehicle_color', old('vehicle_color',isset($vehicle['vehicle_color']) ? $vehicle['vehicle_color'] : ''), ['class' => 'gui-input', 'id' => 'vehicle_color','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="vehicle_color" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('vehicle_color')) <p class="help-block">{{ $errors->first('vehicle_color') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.gps_device_id') !!}</span></label>
                    <label for="gps_device_id" class="field prepend-icon">
                        <!--,'pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*'-->
                        {!! Form::text('gps_device_id', old('gps_device_id',isset($vehicle['gps_device_id']) ? $vehicle['gps_device_id'] : ''), ['class' => 'gui-input', 'id' => 'gps_device_id']) !!}
                        <label for="gps_device_id" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('gps_device_id')) <p class="help-block">{{ $errors->first('gps_device_id') }}</p> @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.route')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('route_id', $vehicle['arr_route'],isset($vehicle['route_id']) ? $vehicle['route_id'] : '', ['class' => 'form-control','id'=>'route_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if($errors->has('route_id')) <p class="help-block">{{ $errors->first('route_id') }}</p> @endif  
                </div> 
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.vehicle_status')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('vehicle_status', $vehicle['arr_vehicle_status'],isset($vehicle['vehicle_status']) ? $vehicle['vehicle_status'] : '', ['class' => 'form-control','id'=>'vehicle_status'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if($errors->has('vehicle_status')) <p class="help-block">{{ $errors->first('vehicle_status') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.total_seats') !!}<span class="asterisk">*</span></span></label>
                    <label for="total_seats" class="field prepend-icon">
                        {!! Form::number('total_seats', old('total_seats',isset($vehicle['total_seats']) ? $vehicle['total_seats'] : ''), ['class' => 'gui-input', 'id' => 'total_seats','min'=>0]) !!}
                        <label for="total_seats" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('total_seats')) <p class="help-block">{{ $errors->first('total_seats') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.strength') !!}<span class="asterisk">*</span></span></label>
                    <label for="strength" class="field prepend-icon">
                        {!! Form::number('strength', old('strength',isset($vehicle['strength']) ? $vehicle['strength'] : ''), ['class' => 'gui-input','id' => 'strength','min'=>0]) !!}
                        @if ($errors->has('strength')) <p class="help-block">{{ $errors->first('strength') }}</p> @endif
                        <label for="strength" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $vehicle['arr_session'],isset($vehicle['session_id']) ? $vehicle['session_id'] : null, ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('session_id')) 
                    <p class="help-block">{{ $errors->first('session_id') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.purchase_date')}}<span class="asterisk">*</span></span></label>
                    <label for="purchase_date" class="field prepend-icon">
                        {!! Form::text('purchase_date', old('purchase_date',isset($vehicle['purchase_date']) ? $vehicle['purchase_date'] : ''), ['class' => 'gui-input date_picker', 'id' => 'purchase_date', 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('purchase_date')) <p class="help-block">{{ $errors->first('purchase_date') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.insurance_date')}}<span class="asterisk">*</span></span></label>
                    <label for="insurance_date" class="field prepend-icon">
                        {!! Form::text('insurance_date', old('insurance_date',isset($vehicle['insurance_date']) ? $vehicle['insurance_date'] : ''), ['class' => 'gui-input date_picker', 'id' => 'insurance_date', 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('insurance_date')) <p class="help-block">{{ $errors->first('insurance_date') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.ins_expired_date')}}<span class="asterisk">*</span></span></label>
                    <label for="ins_expired_date" class="field prepend-icon">
                        {!! Form::text('ins_expired_date', old('ins_expired_date',isset($vehicle['ins_expired_date']) ? $vehicle['ins_expired_date'] : ''), ['class' => 'gui-input date_picker','id' => 'ins_expired_date', 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('ins_expired_date')) <p class="help-block">{{ $errors->first('ins_expired_date') }}</p> @endif
                </div>
            </div>

        </div>
        <div class="bg-light panelBodyy">
            <div class="section-divider mv40">
                <span><i class="fa fa-user"></i>&nbsp;Vehicle Driver Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.contact_number') !!}<span class="asterisk">*</span></span></label>
                    <label for="driver_contact_number" class="field prepend-icon">
                        {!! Form::number('driver_contact_number', old('driver_contact_number',isset($vehicle['driver_contact_number']) ? $vehicle['driver_contact_number'] : ''), ['class' => 'gui-input', 'id' => 'driver_contact_number','request-type'=>'driver','min'=>0]) !!}
                        <label for="driver_contact_number" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('driver_contact_number')) <p class="help-block">{{ $errors->first('driver_contact_number') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.full_name')}}<span class="asterisk">*</span></span></label>
                    <label for="driver_full_name" class="field prepend-icon">
                        {!! Form::text('driver_full_name', old('driver_full_name',isset($vehicle['driver_full_name']) ? $vehicle['driver_full_name'] : ''), ['class' => 'gui-input','id' => 'driver_full_name','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="driver_full_name" class="field-icon">
                            <i class="fa fa-bus"></i>
                        </label>
                    </label>
                    @if ($errors->has('driver_full_name')) 
                    <p class="help-block">{{ $errors->first('driver_full_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.email') !!}</span></label>
                    <label for="driver_email_address" class="field prepend-icon">
                        {!! Form::email('driver_email_address', old('driver_email_address',isset($vehicle['driver_email_address']) ? $vehicle['driver_email_address'] : ''), ['class' => 'gui-input', 'id' => 'driver_email_address']) !!}
                        <label for="driver_email_address" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('driver_email_address')) <p class="help-block">{{ $errors->first('driver_email_address') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.address') !!}<span class="asterisk">*</span></span></label>
                    <label for="driver_address" class="field prepend-icon">
                        {!! Form::text('driver_address', old('driver_address',isset($vehicle['driver_address']) ? $vehicle['driver_address'] : ''), ['class' => 'gui-input', 'id' => 'driver_address']) !!}
                        <label for="driver_address" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                        @if ($errors->has('driver_address')) <p class="help-block">{{ $errors->first('driver_address') }}</p> @endif
                    </label>
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.aadhaar_card') !!}<span class="asterisk">*</span></span></label>
                    <label for="driver_aadhaar_number" class="field prepend-icon">
                        {!! Form::number('driver_aadhaar_number', old('driver_aadhaar_number',isset($vehicle['driver_aadhaar_number']) ? $vehicle['driver_aadhaar_number'] : ''), ['class' => 'gui-input', 'id' => 'driver_aadhaar_number','min'=>0]) !!}
                    </label>
                    @if ($errors->has('driver_aadhaar_number')) <p class="help-block">{{ $errors->first('driver_aadhaar_number') }}</p> @endif
                </div>  
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.upload_aadhaar') !!}<span class="asterisk">*</span>
                            @if(isset($vehicle['driver_aadhaar']) && !empty($vehicle['provider_aadhaar']))
                            <a href="" data-toggle="modal" data-target="#img1" data-backdrop="static" data-keyboard="false">Click to view</a>
                            @endif
                        </span>
                    </label>
                    <label class="btn-bs-file btn btn-xs col-md-12 btncustom">
                        <div class="col-md-6 classfontsize">
                            <span class="glyphicons glyphicons-upload"></span>
                            {!! trans('language.upload_aadhaar') !!}
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4 fontsizeand">
                            <span class="glyphicons glyphicons-search"></span>  Browse file </div>
                        {!! Form::file('driver_aadhaar'); !!}
                    </label>
                    <div id="img2" class="modal fade-scale" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    @if ($errors->has('driver_aadhaar')) 
                                    <p class="help-block">{{ $errors->first('driver_aadhaar') }}</p>
                                    @endif
                                    @if(isset($vehicle['driver_aadhaar']) && !empty($vehicle['driver_aadhaar']))
                                    <img src="{{url($vehicle['driver_aadhaar'])}}" style="width: 100%">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.license_number') !!}<span class="asterisk">*</span></span></label>
                    <label for="driver_license_number" class="field prepend-icon">
                        {!! Form::text('driver_license_number', old('driver_license_number',isset($vehicle['driver_license_number']) ? $vehicle['driver_license_number'] : ''), ['class' => 'gui-input', 'id' => 'driver_license_number']) !!}
                    </label>
                    @if ($errors->has('driver_license_number')) <p class="help-block">{{ $errors->first('driver_license_number') }}</p> @endif
                </div>  
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.license') !!}<span class="asterisk">*</span>
                            @if(isset($vehicle['driver_aadhaar']) && !empty($vehicle['provider_aadhaar']))
                            <a href="" data-toggle="modal" data-target="#img4" data-backdrop="static" data-keyboard="false">Click to view</a>
                            @endif
                        </span>
                    </label>
                    <label class="btn-bs-file btn btn-xs col-md-12 btncustom">
                        <div class="col-md-6 classfontsize">
                            <span class="glyphicons glyphicons-upload"></span>
                            Upload {!! trans('language.license') !!}
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4 fontsizeand">
                            <span class="glyphicons glyphicons-search"></span>  Browse file 
                        </div>
                        {!! Form::file('driver_license'); !!}
                    </label>
                    <div id="img4" class="modal fade-scale" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    @if ($errors->has('driver_license')) 
                                    <p class="help-block">{{ $errors->first('driver_license') }}</p>
                                    @endif
                                    @if(isset($vehicle['driver_license']) && !empty($vehicle['driver_license']))
                                    <img src="{{url($vehicle['driver_license'])}}" style="width: 100%">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="bg-light panelBodyy">
            <div class="section-divider mv40">
                <span><i class="fa fa-user"></i> Vehicle Helper Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.contact_number') !!}<span class="asterisk">*</span></span></label>
                    <label for="helper_contact_number" class="field prepend-icon">
                        {!! Form::number('helper_contact_number', old('helper_contact_number',isset($vehicle['helper_contact_number']) ? $vehicle['helper_contact_number'] : ''), ['class' => 'gui-input', 'id' => 'helper_contact_number','request-type'=>'helper','min'=>0]) !!}
                        <label for="helper_contact_number" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('helper_contact_number')) <p class="help-block">{{ $errors->first('helper_contact_number') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.full_name')}}<span class="asterisk">*</span></span></label>
                    <label for="helper_full_name" class="field prepend-icon">
                        {!! Form::text('helper_full_name', old('helper_full_name',isset($vehicle['helper_full_name']) ? $vehicle['helper_full_name'] : ''), ['class' => 'gui-input','id' => 'helper_full_name','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="helper_full_name" class="field-icon">
                            <i class="fa fa-bus"></i>
                        </label>
                    </label>
                    @if ($errors->has('helper_full_name')) 
                    <p class="help-block">{{ $errors->first('helper_full_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.email') !!}</span></label>
                    <label for="helper_email_address" class="field prepend-icon">
                        {!! Form::email('helper_email_address', old('helper_email_address',isset($vehicle['helper_email_address']) ? $vehicle['helper_email_address'] : ''), ['class' => 'gui-input', 'id' => 'helper_email_address']) !!}
                        <label for="helper_email_address" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('helper_email_address')) <p class="help-block">{{ $errors->first('helper_email_address') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.address') !!}<span class="asterisk">*</span></span></label>
                    <label for="helper_address" class="field prepend-icon">
                        {!! Form::text('helper_address', old('helper_address',isset($vehicle['helper_address']) ? $vehicle['helper_address'] : ''), ['class' => 'gui-input', 'id' => 'helper_address']) !!}
                        <label for="helper_address" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                        @if ($errors->has('helper_address')) <p class="help-block">{{ $errors->first('helper_address') }}</p> @endif
                    </label>
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.aadhaar_card') !!}<span class="asterisk">*</span></span></label>
                    <label for="helper_aadhaar_number" class="field prepend-icon">
                        {!! Form::number('helper_aadhaar_number', old('helper_aadhaar_number',isset($vehicle['helper_aadhaar_number']) ? $vehicle['helper_aadhaar_number'] : ''), ['class' => 'gui-input', 'id' => 'helper_aadhaar_number','min'=>0]) !!}
                    </label>
                    @if ($errors->has('helper_aadhaar_number')) <p class="help-block">{{ $errors->first('helper_aadhaar_number') }}</p> @endif
                </div>  
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.upload_aadhaar') !!}<span class="asterisk">*</span>
                            @if(isset($vehicle['driver_aadhaar']) && !empty($vehicle['provider_aadhaar']))
                            <a href="" data-toggle="modal" data-target="#img5" data-backdrop="static" data-keyboard="false">Click to view</a>
                            @endif
                        </span>
                    </label>
                    <label class="btn-bs-file btn btn-xs col-md-12 btncustom">
                        <div class="col-md-6 classfontsize">
                            <span class="glyphicons glyphicons-upload"></span>
                            {!! trans('language.upload_aadhaar') !!}
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4 fontsizeand">
                            <span class="glyphicons glyphicons-search"></span>  Browse file 
                        </div>
                        {!! Form::file('helper_aadhaar'); !!}
                    </label>
                    <div id="img5" class="modal fade-scale" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    @if ($errors->has('helper_aadhaar')) 
                                    <p class="help-block">{{ $errors->first('helper_aadhaar') }}</p>
                                    @endif
                                    @if(isset($vehicle['helper_aadhaar']) && !empty($vehicle['helper_aadhaar']))
                                    <img src="{{url($vehicle['helper_aadhaar'])}}" style="width: 100%">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.license_number') !!}<span class="asterisk">*</span></span></label>
                    <label for="helper_license_number" class="field prepend-icon">
                        {!! Form::text('helper_license_number', old('helper_license_number',isset($vehicle['helper_license_number']) ? $vehicle['helper_license_number'] : ''), ['class' => 'gui-input', 'id' => 'helper_license_number']) !!}
                    </label>
                    @if ($errors->has('helper_license_number')) <p class="help-block">{{ $errors->first('helper_license_number') }}</p> @endif
                </div>  
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.license') !!}
                            @if(isset($vehicle['driver_aadhaar']) && !empty($vehicle['provider_aadhaar']))
                            <a href="" data-toggle="modal" data-target="#img6" data-backdrop="static" data-keyboard="false">Click to view </a>
                            @endif
                        </span></label>
                    <label class="btn-bs-file btn btn-xs col-md-12 btncustom">
                        <div class="col-md-6 classfontsize">
                            <span class="glyphicons glyphicons-upload"></span>
                            Upload {!! trans('language.license') !!}
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4 fontsizeand">
                            <span class="glyphicons glyphicons-search"></span>  Browse file 
                        </div>
                        {!! Form::file('helper_license'); !!}
                    </label>
                    <div id="img6" class="modal fade-scale" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    @if ($errors->has('helper_license')) 
                                    <p class="help-block">{{ $errors->first('helper_license') }}</p>
                                    @endif
                                    @if(isset($vehicle['helper_license']) && !empty($vehicle['helper_license']))
                                    <img src="{{url($vehicle['helper_license'])}}" style="width: 100%">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>

        </div>
        <div class="panel-footer text-right">
        {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
    </div>
    </div>
    <!-- end .form-body section -->
    
</div>
<!-- end .form-footer section -->
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        $("#vehicle-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                vehicle_number: {
                    required: true
                },
                route_id: {
                    required: true
                },
                vehicle_status: {
                    required: true
                },
                vehicle_color: {
                    required: true
                },
                vehicle_name: {
                    required: true
                },
                driver_full_name: {
                    required: true
                },
                driver_aadhaar_number: {
                    required: true
                },
                driver_aadhaar: {
                    required: function (e) {
                        if ($("#vehicle_driver_id").val() == '')
                        {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                driver_license_number: {
                    required: true
                },
                driver_license: {
                    required: function (e) {
                        if ($("#vehicle_driver_id").val() == '')
                        {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                driver_contact_number: {
                    required: true
                },
                driver_address: {
                    required: true
                },
                helper_full_name: {
                    required: true
                },
                helper_aadhaar_number: {
                    required: true
                },
                helper_aadhaar: {
                    required: function (e) {
                        if ($("#vehicle_helper_id").val() == '')
                        {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                helper_license_number: {
                    required: true
                },
                helper_license: {
                    required: function (e) {
                        if ($("#vehicle_helper_id").val() == '')
                        {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                helper_contact_number: {
                    required: true
                },
                helper_address: {
                    required: true
                },
                provider_full_name: {
                    required: true
                },
                provider_aadhaar_number: {
                    required: true
                },
                provider_aadhaar: {
                    required: function (e) {
                        if ($("#vehicle_provider_id").val() == '')
                        {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                provider_contact_number: {
                    required: true
                },
                provider_address: {
                    required: true
                },
                total_seats: {
                    required: true
                },
                strength: {
                    required: true
                },
                purchase_date: {
                    required: true
                },
                insurance_date: {
                    required: true
                },
                ins_expired_date: {
                    required: true
                },
                session_id: {
                    required: true
                }
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });


        // search students by class id
        $(document).on('change', '#provider_contact_number,#driver_contact_number,#helper_contact_number', function (e) {
            var provider_contact_number = $("#provider_contact_number").val();
            var driver_contact_number = $("#driver_contact_number").val();
            var helper_contact_number = $("#helper_contact_number").val();
            var request_type = $(this).attr("request-type");

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-vehicle-provider-driver-helper')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'provider_contact_number': provider_contact_number,
                    'driver_contact_number': driver_contact_number,
                    'helper_contact_number': helper_contact_number,
                    'request_type': request_type,
                },
                beforeSend: function () {
                    $("#LoadingImage").show();
                },
                success: function (response) {
                    var result = [];
                    result = response.data;
                    if (response.status == 'success')
                    {
                        if (request_type == 'provider')
                        {
                            $("#vehicle_provider_id").val(result.vehicle_provider_id);
                            $("#provider_full_name").val(result.provider_full_name);
                            $("#provider_email_address").val(result.provider_email_address);
                            $("#provider_address").val(result.provider_address);
                            $("#provider_aadhaar_number").val(result.provider_aadhaar_number);
                        }
                        if (request_type == 'driver')
                        {
                            $("#vehicle_driver_id").val(result.vehicle_driver_id);
                            $("#driver_full_name").val(result.driver_full_name);
                            $("#driver_email_address").val(result.driver_email_address);
                            $("#driver_address").val(result.driver_address);
                            $("#driver_aadhaar_number").val(result.driver_aadhaar_number);
                            $("#driver_license_number").val(result.driver_license_number);
                        }
                        if (request_type == 'helper')
                        {
                            $("#helper_full_name").val(result.helper_full_name);
                            $("#vehicle_helper_id").val(result.vehicle_helper_id);
                            $("#helper_email_address").val(result.helper_email_address);
                            $("#helper_address").val(result.helper_address);
                            $("#helper_aadhaar_number").val(result.helper_aadhaar_number);
                            $("#helper_license_number").val(result.helper_license_number);
                        }
                        $("#LoadingImage").hide();
                    }
                }
            });
        });
    });
</script>