@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_vehicle')}}</span>
                </div>
                <div class="topbar-right">
                    <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right custom-btn">{{trans('language.add_vehicle')}}</a>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="vehicle-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.vehicle_number') !!}</th>
                            <th>{!!trans('language.vehicle_name') !!}</th>
                            <th>{!!trans('language.route') !!}</th>
                            <th>{!!trans('language.driver_name') !!}</th>
                            <th>{!!trans('language.driver_contact_number') !!}</th>
                            <th>{!!trans('language.gps_device_id') !!}</th>
                            <th>{!!trans('language.total_seats') !!}</th>
                            <th>{!!trans('language.strength') !!}</th>
                            <th>{!!trans('language.purchase_date') !!}</th>
                            <th>{!!trans('language.insurance_date') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#vehicle-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('vehicle/data')}}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'vehicle_number', name: 'vehicle_number'},
                {data: 'vehicle_name', name: 'vehicle_name'},
                {data: 'route', name: 'route'},
                {data: 'driver_full_name', name: 'driver_full_name'},
                {data: 'driver_contact_number', name: 'driver_contact_number'},
                {data: 'gps_device_id', name: 'gps_device_id'},
                {data: 'total_seats', name: 'total_seats'},
                {data: 'strength', name: 'strength'},
                {data: 'purchase_date', name: 'purchase_date'},
                {data: 'insurance_date', name: 'insurance_date'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{url('vehicle/delete/')}}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'vehicle_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();
                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
    });
</script>
@endsection
@push('scripts')
@endpush

