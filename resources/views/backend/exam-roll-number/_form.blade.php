<style>
    button[disabled] {
        background: #76aaef !important;
    }

    table tr td {
        text-transform: capitalize;
    }

    .state-error {
        display: block !important;
        margin-top: 6px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    }
</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.exam_roll_no')}}</li>
                </ol>
            </div>
        </div>
        @include('backend.partials.messages')
        <div class="bg-light panelBodyy">
            {!! Form::hidden('check_edit','no', ['class' => 'gui-input','id' =>'check_edit']) !!}
            <div class="section row" id="spy1">
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $exam_roll_number['arr_session'],isset($exam_roll_number['session_id']) ? $exam_roll_number['session_id'] : null, ['class' => 'form-control','id'=>'session_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('session_id'))
                        <p class="help-block">{{ $errors->first('session_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.class')}}<span
                                    class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('class_id', $exam_roll_number['arr_class'],isset($exam_roll_number['class_id']) ? $exam_roll_number['class_id'] : null, ['class' => 'form-control','id'=>'class_id'])!!}
                        <i class="arrow double"></i>
                    </label> @if ($errors->has('class_id'))
                        <p class="help-block">{{ $errors->first('class_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.prefix')}}<span class="asterisk">*</span></span></label>
                    <label for="prefix" class="field prepend-icon">
                        {!! Form::text('prefix', old('prefix',isset($exam_roll_number['prefix']) ? $exam_roll_number['prefix'] : ''), ['class' => 'gui-input',''=>trans('language.prefix'), 'id' => 'prefix','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="prefix" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('prefix'))
                        <p class="help-block">{{ $errors->first('prefix') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">Start No<span class="asterisk">*</span></span></label>
                    <label for="start_no" class="field prepend-icon">
                        {!! Form::text('start_no',$last_roll_no, ['class' => 'gui-input', 'id' => 'start_no','pattern'=>'[0-9]*','placeholder'=>'must be start from '.$last_roll_no]) !!}
                        <label for="start_no" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('start_no'))
                        <p class="help-block">{{ $errors->first('start_no') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <div class="option-group field" id="checkBooxx" style="margin-top:27px !important;">
                        <label class="option block option-primary">
                            {!! Form::checkbox('allocate_roll_no','','', ['class' => 'gui-input ', 'id' => 'allocate_roll_no']) !!}
                            <span class="checkbox radioBtnpan"></span><em>Assign Roll No</em>
                        </label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="" id="checkBooxx" style="margin-top:20px !important;">
                        {!! Form::button($submit_button, ['class' => 'button btn-primary','id'=>'save_roll_no','style'=>'width:100px; height:31px;']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-visible" id="spy2">
            <div class="panel-body pn">
                <table class="table table-striped" id="exam-roll-number">
                    <thead>
                    <tr>
                        <th></th>
                        <th class="text-left">{{trans('language.enrollment_number')}}</th>
                        <th class="text-left">{{trans('language.student_name')}}</th>
                        <th class="text-left">{{trans('language.father_name')}}</th>
                        <th class="text-left">{{trans('language.father_contact_number')}}</th>
                        <th class="text-left">{{trans('language.exam_roll_no')}}</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#exam-roll-number-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            ignore: [],
            debug: false,
            rules: {
                'start_no': {
                    required: true,
//                    remote: {
//                        url: "{{url('check-roll-no')}}",
//                        type: "GET",
//                        data: {
//                            session_id: function () {
//                                return $("#session_id").val();
//
//                            }
//                        }
//                    },
                },
                class_id: {
                    required: true
                },
                session_id: {
                    required: true
                },
                allocate_roll_no: {
                    required: true
                },
                prefix: {
                    required: true,
                    {{--remote: {--}}
                    {{--    url: "{{url('check-roll-no')}}",--}}
                    {{--    type: "GET",--}}
                    {{--    data: {--}}
                    {{--        session_id: function () {--}}
                    {{--            return $("#session_id").val();--}}

                    {{--        },--}}
                    {{--        class_id: function () {--}}
                    {{--            return $("#class_id").val();--}}

                    {{--        },--}}
                    {{--        check_edit: function () {--}}
                    {{--            return $("#check_edit").val();--}}

                    {{--        },--}}
                    {{--    }--}}
                    {{--},--}}
                },

            },
            messages: {
                prefix: {
                    remote: 'Prefix already used, please try another'
                },
                start_no: {
                    remote: 'Roll no already used, please try another'
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });


        $(document).on('change', '#class_id,#session_id', function (e) {
            getStudentRollNo();
        });

        function getStudentRollNo() {
            var class_id = $('#class_id').val();
            var session_id = $('#session_id').val();
            $('#allocate_roll_no').attr("checked", false);
            $('#allocate_roll_no').attr("disabled", false);
            $('#save_roll_no').attr("disabled", false);
            $('#prefix').val("");
            $('#check_edit').val("no");
            $('#prefix').attr("readonly", false);
            getStudentsTable();
            if (class_id !== '' && session_id !== '') {
                $.ajax(
                    {
                        url: "{!! url('get-student-roll-no') !!}",
                        datatType: 'json',
                        type: 'POST',
                        data: {
                            'class_id': class_id,
                            'session_id': session_id,
                        },
                        beforeSend: function () {
                            $("#LoadingImage").show();
                        },
                        success: function (res) {
                            $("#LoadingImage").hide();
                            if (res.status === "success") {
                                $('#start_no').val(res.start_no);
                                // if (res.prefix !== '')
                                // {
                                // $('#allocate_roll_no').prop("checked", true);
                                // $('#allocate_roll_no').attr("disabled", true);
                                // $('#save_roll_no').attr("disabled", true);
                                $('#prefix').val(res.prefix);
                                // $('#prefix').attr("readonly", true);
                                // $('#check_edit').val("yes");
                                // }
                                // if (res.student_status === 'no')
                                // {
                                $('#save_roll_no').attr("disabled", false);
                                $('#allocate_roll_no').attr("disabled", false);
                                // }
                            } else {
                                $('#server-response-message').text('Something went wrong');
                                $('#alertmsg-student').modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                            }
                        }
                    });
            }
        }

        $(document).on('click', '#allocate_roll_no', function (e) {
            assignRollNo();
        });
        $(document).on('click', '#save_roll_no', function (e) {
            var form_data = $('#exam-roll-number-form').serialize();
            if ($('#exam-roll-number-form').valid()) {
                $.ajax({
                    url: "{!! url('admin/exam-roll-no/save/') !!}",
                    datatType: 'json',
                    type: 'POST',
                    data: form_data,
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (res) {
                        $("#LoadingImage").hide();
                        if (res.status === "success") {
                            $('#server-response-success').text(res.message);
                            $('#alertmsg-success').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                            $('#save_roll_no').attr("disabled", true);
                            $('#prefix').attr("readonly", true);
                            $('#check_edit').val("yes");
                            getStudentsTable();

                        } else {
                            new PNotify({
                                title: 'Student Roll no',
                                text: res.message,
                                type: 'error',
                                width: '290px',
                                delay: 1400
                            });
                        }
                    }
                });

            }
        });
        $(document).on('change', '#start_no,#prefix', function (e) {
            assignRollNo();
        });

        function assignRollNo() {
//            if ($('#allocate_roll_no').is(":checked"))
//            {
            if ($('#exam-roll-number-form').valid()) {
                getStudentsTable();
//                    var prefix = $("#prefix").val();
//                    var start_no = $("#start_no").val();
//                    $(".student_data").each(function (e) {
//                        var student_id = $(this).attr('rel');
//                        $("#student_roll_no" + student_id).text(prefix + start_no);
//                        start_no++;
//                    });
            } else {
                $('#allocate_roll_no').attr("checked", false);
            }
//            }
        }

        function getStudentsTable() {
            $('#exam-roll-number').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                pageLength: 100,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": $("#class_id option:selected").text() + ' ' + 'Exam Roll No',
                        "filename": 'student-roll-no',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": $("#class_id option:selected").text() + ' ' + 'Exam Roll No',
                        "filename": 'student-roll-no',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5],
                            modifier: {
                                selected: true
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: "{{ url('student-roll-no-data')}}",
                    data: function (f) {
                        f.session_id = $('#session_id').val();
                        f.class_id = $('#class_id').val();
                    }
                },
                columns: [
                    {data: 'student_id', name: 'student_id'},
                    {data: 'enrollment_number', name: 'enrollment_number'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'father_name', name: 'father_name'},
                    {data: 'father_contact_number', name: 'father_contact_number'},
                    {data: 'roll_number', name: 'roll_number'},
                ],
                rowCallback: function (row, data, index) {
                    var prefix = $("#prefix").val();
                    var start_no = $("#start_no").val();
                    $.each(data, function (key, val) {
                        if (val === '') {
                            if (key === 'roll_number' && $('#allocate_roll_no').is(":checked")) {
                                var roll_num = parseInt(start_no) + parseInt(index);
                                $('td:eq(5)', row).text(prefix + roll_num);
                            }
                        }
                    });

                },
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'
            });

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
        }

        var table = $('#exam-roll-number').DataTable();
        table.on('select deselect', function (e) {
            var arr_checked_student = checkedStudent();
            if ($('#check_edit').val() === "yes") {
                if (arr_checked_student.length > 0) {
                    $(".buttons-excel").prop('disabled', false);
                    $(".buttons-print").prop('disabled', false);
                } else {
                    $(".buttons-excel").prop('disabled', true);
                    $(".buttons-print").prop('disabled', true);
                }
            } else {
                $('#server-response-message').text('Please save data before proceeding further.');
                $('#alertmsg-student').modal({
                    backdrop: 'static',
                    keyboard: false
                });

                $(".dt-checkboxes").prop("checked", false);
            }
        });

        function checkedStudent() {
            var arr_checked_student = [];
            $.each($('#exam-roll-number').DataTable().rows('.selected').data(), function () {
                arr_checked_student.push(this["student_id"]);
            });
            return arr_checked_student;
        }
    });
</script>