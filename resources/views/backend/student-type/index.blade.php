@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<html lang="en">
    <!-- Field Options -->
    <div class="admin-form theme-primary center-block">
        <div class="panel-footer text-right">
            <a href="" class="button btn-primary" title="Add School">Add Student Type</a>
        </div>
        <div class="panel heading-border">
            <div class="panel-body bg-light">
                <div class="section-divider mt20 mb40">
                    <span>{{ trans('language.list_student_type')}}</span>
                </div> 
                <div class="panel-visible" id="spy2">
                    <div class="panel-body pn">

                        <table class="table table-bordered" id="datatable2">
                                <thead>
                                    <tr>
                                        <th>{!!trans('language.student_type') !!}</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
      <style type="text/css">
      .alert-success br{
        display: none !important;
      }
      #datatable2_filter{
        margin-right: 0px;
      }
    </style>
<script>
$(document).ready(function () {
 var table = $('#datatable2').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url('student_type/data') }}',
        columns: [
            {data: 'student_type', name: 'student_type'},
            {data: 'action', name: 'action'}

        ]
    });


$(".delete-button").click(function (e) {
    alert('hello');
    // e.preventDefault();
    var id = $(this).attr("id");
    alert(id);
    bootbox.confirm({
        message: "Are you sure to delete ?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result)
            {
                var token = '{!!csrf_token()!!}';
                $.ajax(
                        {
                            url: 'bank/' + id,
                            type: 'DELETE',
                            dataType: "JSON",
                            data: {
                                "id": id,
                                "_type": 'DELETE',
                                "_token": token
                            },
                            success: function (res)
                            {
                                if (res.status == "success")
                                {
                                    table.ajax.reload();

                                }
                            }
                        });
            }
        }
    });
});
});
        </script>
    </body>
</html>
@endsection
@push('scripts')
@endpush

