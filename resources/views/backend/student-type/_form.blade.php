<!-- Field Options -->
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div class="panel-body bg-light">
            <div class="section-divider mt20 mb40">
                <span>{{trans('language.add_student_type')}}</span>
            </div>
            <!-- .section-divider -->
            <div class="section row" id="spy1">
                <div class="col-md-6">
                    <label for="student_type"><span class="radioBtnpan">{{trans('language.student_type')}}</span></label>
                    <label for="student_type" class="field prepend-icon">
                        {!! Form::text('student_type', old('student_type',isset($student_type->student_type) ? $student_type->student_type : ''), ['class' => 'gui-input','placeholder'=>trans('language.student_type'), 'id' => 'student_type']) !!}
                        @if ($errors->has('student_type')) <p class="help-block">{{ $errors->first('student_type') }}</p> @endif
                        <label for="student_type" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                </div>
            </div>
            <!-- end section -->
            <!-- end .section row section -->
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
        <!-- end .form-footer section -->
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

        $("#student-type-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                student_type: {
                    required: true
                }
            },

            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });
</script>