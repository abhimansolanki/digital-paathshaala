<!-- Field Options -->
<style>
    .clr_button{
        font-size: 12px !important;
        padding: 0px !important;
    }
    #sortable 
    { list-style-type: none; margin: 0; padding: 0; width: 100% !important; }
    #sortable li 
    { margin: 0 3px 3px 3px !important; width: 100% !important;  padding: 0.4em !important; padding-left: 1.5em !important; font-size: 1em !important; height: 30px !important; }
    #sortable li span
    { position: absolute !important; margin-left: -1.3em !important; }
    .error{font-size: 0.85em; color: #DE888A;}
    .state-error{
        display:block !important;
        margin-top: 6px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    }
    td.details-control {
        background: url(<?php echo url('/public/details_open.png'); ?>) no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url(<?php echo url('/public/details_close.png'); ?>) no-repeat center center;
    }
    table, th, td {
        border: 1px solid #eeeeee;
    }
    table {
        width: 100%;
        margin-bottom: 20px;
    }
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_class')}}</li>
                </ol>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages')
        @endif
        <div class="bg-light panelBodyy">
            <button type="button"  style="float:right; padding: 5px !important; margin-top: 5px;" class="btn btn-primary" id="show-section-modal" style="margin-top: 25px !important; padding: 5px !important;">
                Add New Section
            </button>
            <div class="section-divider mv40">
                <span><i class="fa fa-graduation-cap"></i>&nbsp;Class Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-2" style="width: 19.666667% !important;">
                    <label><span class="radioBtnpan">{{trans('language.class_name')}}<span class="asterisk">*</span></span></label>
                    <label for="class_name" class="field prepend-icon">
                        {!! Form::text('class_name', old('class_name',isset($classes['class_name']) ? $classes['class_name'] : ''), ['class' => 'gui-input','id' => 'class_name','pattern'=>'']) !!}
                        <label for="class_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('class_name')) 
                    <p class="help-block">{{ $errors->first('class_name') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                    <label class="field select" style="margin-top:0px !important;">
                        {!!Form::select('session_id', $classes['arr_session'],isset($classes['selected_section_id'][0]['session_id']) ? $classes['selected_section_id'][0]['session_id'] : array_keys($classes['arr_session']), ['class' => 'form-control','id'=>'session_id','style'=>'height: 35px !important;'])!!}

                    </label>
                    @if ($errors->has('session_id')) 
                    <p class="help-block">{{ $errors->first('session_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-primary add-row" id="" style="margin-top: 25px !important; padding: 5px !important;">
                        <i class="fa fa-plus-circle"></i> &nbsp; Map Section
                    </button>
                </div>
            </div>
            <div class="row tabledata1">
                <div class="col-md-6 ">
                    <table class="table table-striped" id="tbUser">
                        <tr class="text-center tabledata">
                            <th class="text-center tabledata">Select Section</th>
                            <th class="text-center tabledata">Total seats</th>
                            <th class="text-center tabledata">Strength</th>
                            <th class="text-center tabledata">Action</th>
                        </tr>

                        @php $count = 0; @endphp
                        @if(count($classes['selected_section_id']))
		@php $count = count($classes['selected_section_id']); @endphp
		@foreach($classes['selected_section_id'] as $key => $section_data)
		@php $rel = $key; @endphp
		<tr class='seats_row' rel='{{$rel}}' id='seats_row{{$rel}}'>
		    <td>{!! Form::hidden('class_section_id[]', old('class_section_id',isset($section_data['class_section_id']) ? $section_data['class_section_id'] : ''), ['class' => 'gui-input ','id' => 'class_section_id']) !!}
		        {!!  Form::select('section_id['.$rel.']', $classes['arr_section'], isset($section_data['section_id']) ? $section_data['section_id'] :null, ['class' => 'form-control section_id','required'=>true, 'id'=>'section_id'.$rel]) !!}
		    </td>
		    <td>
		        <div>
			{!! Form::number('seats['.$rel.']', old('seats',isset($section_data['seats']) ? $section_data['seats'] : ''), ['class' => 'gui-input ','id' => 'seats'.$rel,'required'=>true,'pattern'=>'[0-9]*']) !!}
		        </div>
		    </td>
		    <td>
		        <div>
			{!! Form::number('strength['.$rel.']', old('strength',isset($section_data['strength']) ? $section_data['strength'] : ''), ['class' => 'gui-input ','id' => 'strength'.$rel,'required'=>true,'pattern'=>'[0-9]*']) !!}
		        </div>
		    </td> 
		    <td>@if($key != 0)<button type='button' class='btnDelete' rel='{{$rel}}' class-section-id="{{$section_data['class_section_id']}}"><i class='fa fa-times-circle' title='Remove'></i></button>@endif</td>
		</tr>
		@endforeach
                        @else
		@php $rel = 0; @endphp
		<tr class='seats_row' rel='{{$rel}}' id='seats_row{{$rel}}'>
		    <td>{!! Form::hidden('class_section_id[]', '', ['class' => 'gui-input ','id' => 'class_section_id']) !!}
		        <div>
			{!!  Form::select('section_id['.$rel.']', $classes['arr_section'], isset($section_data['section_id']) ? $section_data['section_id'] :null, ['class' => 'form-control section_id','required'=>true, 'id'=>'section_id'.$rel]) !!}
		        </div>
		    </td>
		    <td>
		        <div>
			{!! Form::number('seats['.$rel.']', old('seats',isset($section_data['seats']) ? $section_data['seats'] : ''), ['class' => 'gui-input ','id' => 'seats'.$rel,'required'=>true,'pattern'=>'[0-9]*']) !!}
		        </div>
		    </td>
		    <td>
		        <div>
			{!! Form::number('strength['.$rel.']', old('strength',isset($section_data['strength']) ? $section_data['strength'] : ''), ['class' => 'gui-input ','id' => 'strength'.$rel,'required'=>true,'pattern'=>'[0-9]*']) !!}
		        </div>
		    </td> 
		    <td></td>
		</tr>
                        @endif

                    </table>
                </div>
            </div>
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::reset('Reset', ['class' => 'button btn-primary', 'id' => 'form-reload']) !!}
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
<!-- Modal -->
<div class="modal fade" id="section-modal" class="modal commonboxinput" role="dialog" data-dismiss="modal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Section</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span id='section-sucess' style="color:green; font-size: 13px;"></span>
                <div class="section row" id="spy1">
                    <div class="col-md-3">
                        <label><span class="radioBtnpan">{!! trans('language.section_name') !!}</span></label>
                    </div>
                    <div class="col-md-3">
                        <label for="section_name" class="field prepend-icon">
                            {!! Form::text('section_name','', ['class' => 'gui-input', 'id' => 'section_name']) !!}
                        </label>
                        <div id="section_error" class="error-class"><span id="section_error_text"></span></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close-section-modal">Close</button>
                <button type="button" class="btn btn-primary" id='add-section'>Save changes</button>
            </div>
        </div>
    </div>
</div>
<!--Class view list code-->
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span  title="">{{ trans('language.list_class')}}</span>
                    <button type="button"  id="sort-class" data-toggle="modal"  data-backdrop="static" data-keyboard="false" class="button btn-primary text-right pull-right custom-btn" >{{trans('language.set_class_order')}}</button>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="class-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Class ID</th>
                            <th>{!!trans('language.class_order') !!}</th>
                            <th>{!!trans('language.class_name') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="sort-class-subject-modal" class="modal commonboxinput" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reorder Classes</h4>
            </div>
            <div class="modal-body">
                <ul id="sortable" class="" style="margin-top: 2%;">
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer closebtnforstudent" id="">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function () {
    $(document).on('click', '#form-reload', function (e) {
        window.location.href = "{{ url('admin/classes') }}";
    });

    var count = 0;
    var count = ' {!! $count >= 0 ? $count + 1 : $count; !!}';
    count = parseInt(count) + 1 - 1;
    $(".add-row").click(function () {
<?php
    $option = '';
    foreach ($classes['arr_section'] as $key => $value)
    {
        $option .= '<option value="' . $key . '">' . $value . '</option>';
    }
?>
        var section_list = '{!! $option !!}';
        var markup = "<tr class='' rel='" + count + "' id='seats_row" + count + "'>\n\
                <td><div><select name='section_id[" + count + "]' class='form-control section_id' id='section_id" + count + "' required>" + section_list + "</select></div></td> \n\
                <td><div><input type='number' name='seats[" + count + "]'  class='gui-input counttext' min='1' id='seats" + count + "' value='' required></div></td>\n\
                <td><div><input type='number' name='strength[" + count + "]'  class='gui-input counttext' min='1' id='strength" + count + "' value='' required></div></td>\n\
                <td><button type='button' class='btnDelete' class-section-id='' rel='" + count + "'><i class='fa fa-times-circle' title='Remove'></i></button></td>\n\
         </tr>";
        $("#tbUser tbody").append(markup);
        count++;

    });
    $("#tbUser").on('click', '.btnDelete', function () {

        var class_section_id = $(this).attr("class-section-id");
        var rel = $(this).attr("rel");
        if (class_section_id !== '')
        {
            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('class-section/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'class_section_id': class_section_id,
                                    },
                                    beforeSend: function () {
                                        $("#LoadingImage").show();
                                    },
                                    success: function (res)
                                    {
                                        $("#LoadingImage").hide();
                                        if (res.status === "success")
                                        {
                                            $("#seats_row" + rel).closest('tr').remove();
                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }

                                    }
                                });
                    }
                }
            });
        } else
        {
            $("#seats_row" + rel).closest('tr').remove();
        }

    });
    $("#class-form").validate({

        /* @validation states + elements 
         ------------------------------------------- */
        errorClass: "state-error",
        validClass: "state-success",
        errorElement: "em",
        /* @validation rules 
         ------------------------------------------ */
        ignore: [],
        rules: {
            class_name: {
                required: true,
                normalizer: function (value) {
                    return $.trim(value);
                },
                lettersonly: true,
            },
            'section_id[]': {
                required: true
            },
            session_id: {
                required: true
            },
            'seats[]': {
                required: true,
                // nowhitespace: true
            },
            'strength[]': {
                required: true,
                // nowhitespace: true
            },

        },
        /* @validation error messages 
         ---------------------------------------------- */

        /* @validation highlighting + error placement  
         ---------------------------------------------------- */
        highlight: function (element, errorClass, validClass) {
            $(element).closest('.field').addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).closest('.field').removeClass(errorClass).addClass(validClass);
        },

        errorPlacement: function (error, element) {
            if (element.is(":radio") || element.is(":checkbox")) {
                element.closest('.option-group').after(error);
            } else {
                error.insertAfter(element.parent());
            }
        }
    });

    $.validator.addMethod("lettersonly", function (value, element) {

        return this.optional(element) || !/[~`!@#$%\^&*()+=\-\_\.\[\]\\';,/{}|\\":<>\?]/.test(value) || /^[a-zA-Z][a-zA-Z0-9\s\.]+$/i.test(value);
    }, "Please enter only number or alphabets");

    $(document).on('click', '.clear_btn', function (e) {
        var rel = $(this).attr('rel');
        $(".clear_subject" + rel).prop('checked', false);
    });
    $(document).on('click', '#add-section', function (e) {
        e.preventDefault();
        $("#section-sucess").text('');
        var section_name = $('#section_name').val();
        if (section_name !== '')
        {
            $('#section_error_text').text('');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('add-section')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'section_name': section_name,
                },
                success: function (response) {
                    var resopose_data = [];

                    if (response.status === 'success')
                    {
                        resopose_data = response.arr_section;
                        $("#section-sucess").text(response.message);
                        $('.section_id').empty();
                        $('.section_id').append('<option value=""> -- Section -- </option>');
                        $.each(resopose_data, function (key, value) {
                            $('.section_id').append('<option value="' + key + '">' + value + '</option>');

                        });
                    } else
                    {
                        $("#section-sucess").text('');
                        $('#section_error_text').text('Section name already exist');

                    }
//                    $('#close-section-modal').trigger('click');
                }
            });
        } else
        {
            $('#section_error_text').text('Please enter section name');
        }
    });

    $('#section-modal').on('hide.bs.modal', function (e) {
        $('#section_error_text').text('');
        $("#section-sucess").text('');
        $("#section_name").val('');
    });
    $('#section_name').on('change', function (e) {
        $('#section_error_text').text('');
    });


//    Class view list code
    $('#sort-class-subject-modal').on('hidden.bs.modal', function () {
        location.reload();
    });
    $(document).on('click', '#show-section-modal', function () {
        $("#section-modal").modal('show');
    });
    var table = $('#class-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{url('classes/data')}}",
        "aaSorting": [[0, "ASC"]],
        columns: [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": '',

            },
            {data: 'class_id', name: 'class_id'},
            {data: 'class_order', name: 'class_order'},
            {data: 'class_name', name: 'class_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
    $('#class-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
//            console.log(row.data());
            // Open this row
            var child = format(row.data());
//            console.log(child);
            if (child !== '')
            {
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        }
    });
    function format(d) {
        // `d` is the original data object for the row
        var class_section = d.class_section;
        var tab_body = '';
        var tab_head = '<table cellpadding="5" cellspacing="0" border="0" style="margin-left: -4px; font-size:11px !important;">';
        tab_head = tab_head + '<tr style="font-weight: 600;"><td>Section ID</td><td>Section Name</td>' +
                '<td>Total Seats</td>' +
                '<td>Strength</td>' +
                '</tr>';
        ;
        $.each(class_section, function (key, value)
        {
            tab_body = tab_body + '<tr><td style="width: 12%;">' + value.section_id + '</td><td style="width: 36%;">' + value.section_name + '</td>' +
                    '<td style="width: 45%;">' + value.seats + '</td>' +
                    '<td style="width: 37%;">' + value.strength + '</td>' +
                    '</tr>';
        });
        var tab_foot = '</table>';
        var table_data = '';
        if (tab_body !== '')
        {
            table_data = tab_head + tab_body + tab_foot;
        }
        return table_data;
    }

    $(document).on("click", ".delete-button", function (e) {
        e.preventDefault();
        var id = $(this).attr("data-id");

        bootbox.confirm({
            message: "Are you sure to delete ?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result)
                {
                    var token = '{!!csrf_token()!!}';
                    $.ajax(
                            {
                                url: "{{ url('classes/delete/') }}",
                                datatType: 'json',
                                type: 'POST',
                                data: {
                                    'class_id': id,
                                },
                                success: function (res)
                                {
                                    if (res.status === "success")
                                    {
                                        table.ajax.reload();

                                    } else if (res.status === "used")
                                    {
                                        $('#server-response-message').text(res.message);
                                        $('#alertmsg-student').modal({
                                            backdrop: 'static',
                                            keyboard: false
                                        });
                                    }
                                }
                            });
                }
            }
        });

    });
    $(document).on('click', '#sort-class', function () {

        $('#sortable').html('');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            url: "{{url('get-class-order')}}",
            datatType: 'json',
            type: 'POST',
            data: {},
            beforeSend: function () {
                $("#LoadingImage").show();
            },
            success: function (response) {
                if (response.status === 'success')
                {
                    $.each(response.arr_classes, function (key, value) {
                        $('#sortable').append('<li class="ui-state-default" style="" value="' + value.class_id + '">' + value.class_name + '</li>');
                    });
                } else
                {
                    $('#sortable').append('<li>No data available to sort</li>');
                }
                $("#LoadingImage").hide();
                $("#sort-class-subject-modal").modal('show');
            }
        });
    });
    $("#sortable").sortable({
        update: function () {
            var order = $('#sortable').sortable('toArray', {attribute: 'value'});
            console.log(order);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                datatType: 'json',
                type: 'POST',
                async: true,
                url: "{{ url('set-class-order') }}",
                data: {
                    "item": order,
                }
            }).done(function (data) {
            });
        }
    });
//    $(document).on("change", "select", function () {
//        $("select").not(this).children("option:contains(" + $(this).val() + ")").hide();
//    });
});
</script>

