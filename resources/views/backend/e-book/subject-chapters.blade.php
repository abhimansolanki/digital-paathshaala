<style type="text/css">
    .table > thead > tr > th {
        font-weight: bold;
        font-size: 14px !important;
    }
    .table {
        background-color: #f3f3f3;
    }
    tr td {
        font-weight: bold;
    }
</style>
<button data-toggle="modal" id="add_chapter" data-target="#add-chepter" class="button btn-primary ml15 custom-btn">Add Chapter</button>
<table class="table table-bordered table-hover" id="chapter_table" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="text-center" width="8%">{{trans('language.sr_no')}}</th>
        <th class="text-center">{{trans('language.chapter')}}</th>
        <th class="text-center">{{trans('language.post_date')}}</th>
        <th class="text-center" width="15%">{{trans('language.action')}}</th>
    </tr>
    </thead>
    <tbody>
    @php $sr = 1; @endphp
    @foreach($subject_chapters as $subject_chapter)
        <tr class="student_information">
            <td class="text-center">
                {{ $sr++ }}
            </td>
            <td class="text-center">
                {{ $subject_chapter->name }}
            </td>
            <td class="text-center">
                {{ $subject_chapter->created_at->format('d-M-Y') }}
            </td>
            <td class="text-center">
{{--                <a title="Add Topic" data-toggle="modal" data-target="#add-topic" data-backdrop="static" data-keyboard="false" data-id="{{$subject_chapter->subject_chapter_id}}" class="btn btn-default open-AddTopicDialog"><i class="fa fa-plus"></i></a>--}}
                <a title="Manage Topic" href="{{ route("view.chapter.topics",['chapter_id' => $subject_chapter->subject_chapter_id])}}" data-d="{{$subject_chapter->subject_chapter_id}}" class="btn btn-default" target="_blank"><i class="fa fa-file"></i></a>
                <button title="Edit Chapter" id="edit_chapter" href=#" class="btn btn-default" data-toggle="modal" id="add_chapter" data-target="#add-chepter"
                   data-chapter_id="{{ $subject_chapter->subject_chapter_id }}" data-chapter_name="{{ $subject_chapter->name }}"><i class="fa fa-edit"></i></button>
                <button title="Delete Chapter" id="delete_chapter" href=#" class="btn btn-default"
                   data-chapter_id="{{ $subject_chapter->subject_chapter_id }}"><i class="fa fa-trash"></i></button>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="modal fade" id="add-chepter">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Chapter</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3"
                                           class="col-sm-4 control-label">{{ trans('language.chapter')}}</label>
                                    <div class="col-sm-8">
                                        {!!Form::text('chapter_name',old('chapter_name'), ['class' => 'form-control','id'=>'chapter_name','placeholder'=>"Chapter Name", "required" => 'true'])!!}
                                        {!!Form::hidden('chapter_id','', ['class' => 'form-control','id'=>'chapter_id'])!!}
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary add-chapter">Save</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var chapter_table = $('#chapter_table').DataTable({
            'responsive': true
        });
    });
</script>
