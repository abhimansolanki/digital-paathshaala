    <style type="text/css">
        .table > thead > tr > th {
            font-weight: bold;
            font-size: 14px !important;
        }
        .table {
            background-color: #f3f3f3;
        }
        tr td {
            font-weight: bold;
        }
    </style>
    <table class="table table-bordered table-hover" id="topic_table" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th class="text-center" width="8%">{{trans('language.sr_no')}}</th>
            <th class="text-center" width="20%">{{trans('language.topic')}}</th>
            <th class="text-center" width="20%">{{ trans('language.youtube_video_link')}}</th>
            <th class="text-center">{{ trans('language.file')}}</th>
            <th class="text-center">{{trans('language.post_date')}}</th>
            <th class="text-center" width="15%">{{trans('language.action')}}</th>
        </tr>
        </thead>
        <tbody>
        @php $sr = 1; @endphp
        @foreach($chapter_topics as $chapter_topic)
            <tr class="student_information">
                <td class="text-center">
                    {{ $sr++ }}
                </td>
                <td class="text-center">
                    {{ $chapter_topic->name }}
                </td>
                <td class="text-center">
                    {{ $chapter_topic->video_link }}
                </td>
                <td class="text-center">
                    @if(!empty($chapter_topic->image_file))
                    <a href="{{ check_file_exist($chapter_topic->image_file, 'eclass_room', 'yes', false) }}" title="Image File" target="_blank">Click Here</a>
                    @else
                        ---
                    @endif
                </td>
                <td class="text-center">
                    {{ $chapter_topic->created_at->format('d-M-Y') }}
                </td>
                <td class="text-center">
                    <button title="Edit Topic" id="edit_topic" class="btn btn-default"
                            data-topic_id="{{ $chapter_topic->chapter_topic_id }}"><i class="fa fa-edit"></i></button>
                    <button title="Delete Topic" id="delete_topic" class="btn btn-default"
                            data-topic_id="{{ $chapter_topic->chapter_topic_id }}"><i class="fa fa-trash"></i></button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <script>
        $(document).ready(function () {
            var topic_table = $('#topic_table').DataTable({
                'responsive': true
            });
        });
    </script>
