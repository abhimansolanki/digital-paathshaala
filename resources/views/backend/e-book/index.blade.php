@extends('admin_panel/layout')
@section('content')
    <style>
        #deletebtn {
            margin-top: -14px !important;
        }

        .btn:hover, .btn:focus, .btn:active {
            border: none !important;
        }
    </style>
    <div class="admin-form theme-primary" style="padding-bottom: 175px;" id="CustomInput">
        @include('backend.partials.messages')
        @include('backend.partials.loader')
        <div class="tray tray-center tableCenter">
            <div class="">
                <div class="panel panel-visible" id="spy2">
                    <div class="panel heading-border">
                        <div id="topbar">
                            <div class="topbar-left">
                                <ol class="breadcrumb">
                                    <li class="crumb-active">
                                        <a href="{{url('admin/dashboard')}}"> <span
                                                    class="glyphicon glyphicon-home"></span> Home</a>
                                    </li>
                                    <li class="crumb-trail">{{ trans('language.ebook')}}</li>
                                </ol>
                            </div>
                        </div>
                        <div class="panel-body pn">
                            <div class="row">
                                <br>
                                {!! Form::open(['id'=>'class-fee-from']) !!}
                                <div class="col-md-2">
                                    <label><span class="radioBtnpan">{{ trans('language.academic_year')}}<span
                                                    class="asterisk">*</span></span></label>
                                    <label for="session_id" class="field prepend-icon">
                                        <label class="field select">
                                            {!!Form::select('session_id', $arr_session, $session['session_id'], ['class' => 'form-control','id'=>'session_id', 'disabled' => true])!!}
                                            <i class="arrow"></i>
                                        </label>
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label><span class="radioBtnpan">{{ trans('language.class')}}<span class="asterisk">*</span></span></label>
                                    <label for="class_id" class="field prepend-icon">
                                        <label class="field select">
                                            {!!Form::select('class_id', $arr_class,old('class_id'), ['class' => 'form-control','id'=>'class_id'])!!}
                                            <i class="arrow"></i>
                                        </label>
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label><span class="radioBtnpan">{{ trans('language.section')}}<span
                                                    class="asterisk">*</span></span></label>
                                    <div class="section_id" class="field prepend-icon">
                                        <label class="field select">
                                            {!!Form::select('section_id', ['' => '--Select Section--'], old('section_id'), ['class' => 'form-control','id'=>'section_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label><span class="radioBtnpan">{{ trans('language.subject')}}<span
                                                    class="asterisk">*</span></span></label>
                                    <label for="subject_id" class="field prepend-icon">
                                        <label class="field select">
                                            {!!Form::select('subject_id', ['' => '--Select Subject--'], old('subject_id'), ['class' => 'form-control','id'=>'subject_id'])!!}
                                            <i class="arrow"></i>
                                        </label>
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label><span class="radioBtnpan"></span></label>
                                    <label class="field select">
                                        {!! Form::button('Search', ['class' => 'button btn-primary search-button']) !!}
                                    </label>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="row" style="margin-top: 30px;">
                                <div id="class-subject-chapters"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add-topic">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">@lang('language.add_topic')</h4>
                </div>
                <form class="form-horizontal" id="add_topic_form" method="POST" enctype="multipart/form-data" action="{{url('e-book/add-topic')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" id="server_response" style="display: none">
                                <div class="alert alert-success" style="font-size: 12px !important; border:0 !important; background-color:#7f92ab0d; color: #666666;padding: 5px; text-align: center;">
                                    <strong>{{trans('language.success')}}!</strong><br><br>
                                    <span id="server_response_message"></span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-sm-3 control-label">{{ trans('language.topic_title')}}</label>
                                        <div class="col-sm-8">
                                            {!!Form::text('topic_title',old('topic_title'), ['class' => 'form-control','id'=>'topic_title','placeholder'=>"Topic Title", "required" => 'true'])!!}
                                            {!!Form::hidden('topic_id','', ['class' => 'form-control','id'=>'topic_id'])!!}
                                            {!!Form::hidden('is_ajax',1, ['class' => 'form-control','id'=>'is_ajax'])!!}
                                            {!!Form::hidden('chapter_id','', ['class' => 'form-control','id'=>'chapter_id'])!!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-sm-3 control-label">{{ trans('language.youtube_video_link')}}</label>
                                        <div class="col-sm-8">
                                            {!!Form::text('video_link',old('video_link'), ['class' => 'form-control','id'=>'video_link','placeholder'=>"Video Link", "required" => 'true'])!!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-sm-3 control-label">{{ trans('language.reading_material')}}</label>
                                        <div class="col-sm-4">
                                            {!!Form::file('image_file',old('image_file'), ['class' => 'form-control','id'=>'image_file', "required" => 'true'])!!}
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="checkbox" class="form-check-input" id="download_permission"
                                                   name="download_permission">
                                            <label class="form-check-label" for="download_permission">
                                                {{trans('language.download_permission')}}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-sm-3 control-label"></label>
                                        <div class="col-sm-8">
                                            {!!Form::textarea('reading_text',old('reading_text'), ['class' => 'form-control','id'=>'reading_text','placeholder'=> trans('language.reading_material')])!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary add-update-topic">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('backend.partials.popup')
    <script>
        $(document).ready(function () {
            $(document).on('change', '#class_id', function (e) {
                e.preventDefault();
                $('select[name="section_id"]').empty();
                $('select[name="section_id"]').append('<option value="">--Select Section--</option>');
                getClassSection();
            });

            $(document).on('change', '#section_id', function (e) {
                e.preventDefault();
                $('select[name="subject_id"]').empty();
                $('select[name="subject_id"]').append('<option value="">--Select Subject--</option>');
                getClassSectionSubjects();
            });

            $(document).on('click', '.search-button', function (e) {
                e.preventDefault();
                getSubjectChaptersData();
            });

            $(document).on('click', '.add-chapter', function (e) {
                e.preventDefault();
                addUpdateChatper();
            });

            $(document).on('click','#add_chapter', function(e){
                $("#chapter_name").val(null);
                $("#chapter_id").val(null);
            });

            $(document).on('click','#edit_chapter', function(e){
                console.log('EDIT Chapter');
                $("#chapter_name").val($(this).data("chapter_name"));
                $("#chapter_id").val($(this).data("chapter_id"));
            });

            $(document).on('click','#delete_chapter', function(e){
                console.log('Delete Chapter');
                e.preventDefault();
                deleteChatper($(this).data("chapter_id"));
            });

            $(document).on("click", ".open-AddTopicDialog", function () {
                var chapter_id = $(this).data('id');
                $('#add_topic_form')[0].reset();
                $("#add_topic_form #chapter_id").val(chapter_id);
                $("#add_topic_form #is_ajax").val(1);
                $('#server_response').hide();
                $('#server_response_message').html(null);
            });

            $(document).on('click', '.add-update-topic', function (e) {
                e.preventDefault();
                addTopic();
            });
        });

        function getClassSection() {
            var class_id = $("#class_id").val();
            if (class_id !== '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status == 'success') {
                            $.each(resopose_data, function (key, value) {
                                $('select[name="section_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        } else {
                            return false;
                        }
                    }
                });
            }
        }

        function getClassSectionSubjects() {
            var session_id = $("#session_id").val();
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            if (session_id !== '' && class_id !== '' && section_id !== '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('e-book/get-class-subjects')}}",
                    datatType: 'json',
                    type: 'GET',
                    data: {
                        'session_id': session_id,
                        'class_id': class_id,
                        'section_id': section_id,
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.arr_subject;
                        if (response.status == 'success') {
                            $.each(resopose_data, function (key, value) {
                                $('select[name="subject_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        } else {
                            return false;
                        }
                    }
                });
            }
        }

        // get class subject chapters all data
        function getSubjectChaptersData() {
            var session_id = $('#session_id').val();
            var class_id = $('#class_id').val();
            var section_id = $('#section_id').val();
            var subject_id = $('#subject_id').val();
            $('#class-subject-chapters').html('');
            if (session_id !== '' && class_id !== '' && subject_id !== '' && section_id !== '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('e-book/data')}}",
                    datatType: 'json',
                    type: 'GET',
                    data: {
                        'session_id': session_id,
                        'class_id': class_id,
                        'section_id': section_id,
                        'subject_id': subject_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        console.log('start');
                        $("#LoadingImage").hide();
                        $("#class-subject-chapters").html(response);
                        console.log('end');
                    }
                });
            } else {
                $("#class-subject-chapters").html('<div class="text-center">No records found</div>');
            }
        }

        function addUpdateChatper() {
            var chapter_id = $("#chapter_id").val();
            var chapter_name = $("#chapter_name").val();
            var session_id = $('#session_id').val();
            var class_id = $('#class_id').val();
            var section_id = $('#section_id').val();
            var subject_id = $('#subject_id').val();
            if (chapter_name !== '') {
                $.ajax({
                    url: "{{url('e-book/add-chapter')}}",
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    dataType: 'json',
                    data: {
                        'chapter_name': chapter_name,
                        'chapter_id': chapter_id,
                        'session_id': session_id,
                        'class_id': class_id,
                        'section_id': section_id,
                        'subject_id': subject_id,
                    },
                    success: function (response) {
                        $('#add-chepter').modal('hide');
                        $('body').removeClass('modal-open');
                        $('body').css('padding-right', '');
                        $('.modal-backdrop').remove();
                        if (response.status == 'success') {
                            getSubjectChaptersData();
                        } else {
                            alert(resopose_data);
                            return false;
                        }
                    }
                });
            }
        }

        function deleteChatper(id) {
            var chapter_id = id;
            if (chapter_id !== '') {
                $.ajax({
                    url: "{{url('e-book/delete-chapter')}}",
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    dataType: 'json',
                    data: {
                        'chapter_id': chapter_id,
                    },
                    success: function (response) {
                        if (response.status == 'success') {
                            getSubjectChaptersData();
                        } else {
                            alert(resopose_data);
                            return false;
                        }
                    }
                });
            }
        }

        function addTopic() {
            $('#server_response').hide();
            $('#server_response_message').html(null);
            var data = $('#add_topic_form').serialize();
            $.ajax({
                url: "{{url('e-book/add-topic')}}",
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                dataType: 'json',
                data: data,
                success: function (response) {
                    // $('#add-chepter').modal('hide');
                    // $('body').removeClass('modal-open');
                    // $('body').css('padding-right', '');
                    // $('.modal-backdrop').remove();
                    if (response.result == 'success') {
                        $('#add_topic_form').trigger("reset");
                        $('#server_response_message').html(response.message);
                        $('#server_response').show();
                    } else {
                        alert(response.message);
                        return false;
                    }
                }
            });
        }

        function hideModal(modal) {
            $("#"+modal).removeClass("in");
            $(".modal-backdrop").remove();
            $('body').removeClass('modal-open');
            $('body').css('padding-right', '');
            $("#"+modal).hide();
        }
    </script>
    </body>
    </html>
@endsection