@extends('admin_panel/layout')
@section('content')
    <style>
        #deletebtn {
            margin-top: -14px !important;
        }

        .btn:hover, .btn:focus, .btn:active {
            border: none !important;
        }
    </style>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="admin-form theme-primary" style="padding-bottom: 175px;" id="CustomInput">
        @include('backend.partials.messages')
        @include('backend.partials.loader')
        <div class="tray tray-center tableCenter">
            <div class="">
                <div class="panel panel-visible" id="spy2">
                    <div class="panel heading-border">
                        <div id="topbar">
                            <div class="topbar-left">
                                <ol class="breadcrumb">
                                    <li class="crumb-active">
                                        <a href="{{url('admin/dashboard')}}"> <span
                                                    class="glyphicon glyphicon-home"></span> Home</a>
                                    </li>
                                    <li class="crumb-trail">{{ trans('language.ebook')}}</li>
                                </ol>
                            </div>
                            <div class="topbar-right">
                                <button type="button" class="button btn-primary text-right pull-right custom-btn"
                                        data-toggle="modal" data-target="#add-topic">
                                    @lang('language.add_topic')
                                </button>
                            </div>
                        </div>
                        <div class="panel-body pn">
                            <div class="row">
                                <br>
                                <div class="col-md-4">
                                    <label><span
                                                class="radioBtnpan">{{ trans('language.class')}} : <b>{{$chapter->classSub->class_name}}</b></span></label>
                                </div>
                                <div class="col-md-4">
                                    <label><span
                                                class="radioBtnpan">{{ trans('language.section')}} : <b>{{$chapter->Section->section_name}}</b></span></label>
                                </div>
                                <div class="col-md-4">
                                    <label><span
                                                class="radioBtnpan">{{ trans('language.chapter_name')}} : <b>{{$chapter->name}}</b></span></label>
                                </div>
                            </div>
                            <div class="row">
                                <div id="chapter_topics"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add-topic">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">@lang('language.add_topic')</h4>
                </div>
                <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{url('e-book/add-topic')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-sm-3 control-label">{{ trans('language.topic_title')}}</label>
                                        <div class="col-sm-8">
                                            {!!Form::text('topic_title',old('topic_title'), ['class' => 'form-control','id'=>'topic_title','placeholder'=>"Topic Title", "required" => 'true'])!!}
                                            {!!Form::hidden('topic_id','', ['class' => 'form-control','id'=>'topic_id'])!!}
                                            {!!Form::hidden('chapter_id',$chapter->subject_chapter_id, ['class' => 'form-control','id'=>'chapter_id'])!!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-sm-3 control-label">{{ trans('language.youtube_video_link')}}</label>
                                        <div class="col-sm-8">
                                            {!!Form::text('video_link',old('video_link'), ['class' => 'form-control','id'=>'video_link','placeholder'=>"Video Link"])!!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-sm-3 control-label">{{ trans('language.reading_material')}}</label>
                                        <div class="col-sm-4">
                                            {!!Form::file('image_file',old('image_file'), ['class' => 'form-control','id'=>'image_file'])!!}
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="checkbox" class="form-check-input" id="download_permission"
                                                   name="download_permission">
                                            <label class="form-check-label" for="download_permission">
                                                {{trans('language.download_permission')}}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-sm-3 control-label"></label>
                                        <div class="col-sm-8">
                                            {!!Form::textarea('reading_text',old('reading_text'), ['class' => 'form-control','id'=>'reading_text','placeholder'=> trans('language.reading_material')])!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary add-update-topic">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('backend.partials.popup')
    <script>
        $(document).ready(function () {
            getChapterTopics();
            // $(document).on('click', '.add-update-topic', function (e) {
            //     e.preventDefault();
            //     addUpdateTopic();
            // });

            // $(document).on('click','#add_chapter', function(e){
            //     $("#chapter_name").val(null);
            //     $("#chapter_id").val(null);
            // });
            //
            // $(document).on('click','#edit_chapter', function(e){
            //     console.log('EDIT Topic');
            //     $("#chapter_name").val($(this).data("chapter_name"));
            //     $("#chapter_id").val($(this).data("chapter_id"));
            // });

            $(document).on('click', '#delete_topic', function (e) {
                console.log('Delete Topic');
                e.preventDefault();
                deleteTopic($(this).data("topic_id"));
            });

            $(document).on('click', '#edit_topic', function (e) {
                console.log('Edit Topic');
                e.preventDefault();
                getTopicDetails($(this).data("topic_id"));
            });
        });

        function addUpdateTopic() {
            var topic_id = $("#topic_id").val();
            var topic_title = $("#topic_title").val();
            var topic_chapter_id = $('#chapter_id').val();
            if (topic_title !== '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('e-book/add-topic')}}",
                    datatType: 'json',
                    type: 'GET',
                    data: {
                        'topic_title': topic_title,
                        'chapter_id': topic_chapter_id,
                        'topic_id': topic_id,
                    },
                    success: function (response) {
                        $('#add-topic').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                        if (response.status == 'success') {
                            console.log('success');
                            location.reload();
                        } else {
                            alert(resopose_data);
                            return false;
                        }
                    }
                });
            }
        }

        function getChapterTopics() {
            console.log('inside get chapter topic');
            var chapter_id = $('#chapter_id').val();
            $('#chapter_topics').html('');
            if (chapter_id !== '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('e-book/topics/data')}}",
                    datatType: 'json',
                    type: 'GET',
                    async: true,
                    data: {
                        'chapter_id': chapter_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        console.log('start');
                        $("#LoadingImage").hide();
                        $("#chapter_topics").html(response);
                        console.log('end');
                    }
                });
            } else {
                $("#chapter_topics").html('<div class="text-center">No records found</div>');
            }
        }

        function deleteTopic(id) {
            var topic_id = id;
            if (topic_id !== '') {
                $.ajax({
                    url: "{{url('e-book/delete-topic')}}",
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    dataType: 'json',
                    data: {
                        'topic_id': topic_id,
                    },
                    success: function (response) {
                        if (response.status == 'success') {
                            getChapterTopics();
                        } else {
                            alert(resopose_data);
                            return false;
                        }
                    }
                });
            }
        }

        function getTopicDetails(id) {
            var topic_id = id;
            if (topic_id !== '') {
                $.ajax({
                    url: "{{url('e-book/get-topic')}}",
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    dataType: 'json',
                    data: {
                        'topic_id': topic_id,
                    },
                    success: function (response) {
                        if (response.status == 'success') {
                            var data = response.data;
                            $('#topic_id').val(data.chapter_topic_id);
                            $('#topic_title').val(data.name);
                            $('#video_link').val(data.video_link);
                            $('#image_file').val(data.image_file);
                            if(data.download_permission == 1){
                                $('#download_permission').prop('checked', true);
                            }else{
                                $('#download_permission').prop('checked', false);
                            }
                            $('#reading_text').val(data.reading_text);
                            $('#add-topic').modal('toggle');
                            console.log(response.data);
                        } else {
                            alert(response.data);
                            return false;
                        }
                    }
                });
            }
        }
    </script>
    </body>
    </html>
@endsection