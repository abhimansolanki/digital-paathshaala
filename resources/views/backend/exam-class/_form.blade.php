<style>
    .select2{
        margin-top: 3%;
    }

</style>
@if(!empty($exam_class))
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">Exam -> {!! $exam_class['exam_name'] !!} -> Assign Class </li>
                    {!! Form::hidden('exam_id',isset($exam_class['exam_id']) ? $exam_class['exam_id'] : null, ['class' => 'gui-input','id' => 'exam_id']) !!}
                    {!! Form::hidden('exam_class_id',isset($exam_class['exam_class_id']) ? $exam_class['exam_class_id'] : null, ['class' => 'gui-input','id' =>'exam_class_id']) !!}
                </ol>
            </div>
            <div class="topbar-right">
                <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right" title="Create Exam">Create Exam</a>
            </div>
        </div>
        @include('backend.partials.messages')
        <div class="panel-body bg-light">
            <!-- .section-divider -->
            <div class="section row" id="spy1">
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}</span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $exam_class['arr_session'],isset($exam_class['session_id']) ? $exam_class['session_id'] : null, ['class' => 'form-control','id'=>'session_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('session_id')) 
                    <p class="help-block">{{ $errors->first('session_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{ trans('language.class')}}<span class="asterisk">*</span></span></label>
                    <label for="class_id" class="field select">
                        {!!Form::select('class_id', $exam_class['arr_class'],isset($exam_class['class_id']) ? $exam_class['class_id'] : '', ['class' => 'form-control','id'=>'class_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('class_id')) 
                    <p class="help-block">{{ $errors->first('class_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{ trans('language.section')}}<span class="asterisk">*</span></span></label>
                    <label for="section_id" class="field select">
                        {!!Form::select('section_id', $exam_class['arr_section'],isset($exam_class['section_id']) ? $exam_class['section_id'] : '', ['class' => 'form-control','id'=>'section_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('section_id')) 
                    <p class="help-block">{{ $errors->first('section_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan"></span></label>
                    <label for="" class="field prepend-icon">
                        {!! Form::button('Continue', ['class' => 'button btn-primary','style'=>'width:100px;margin-top: 6px;','id'=>'get_subject_marks']) !!}   
                    </label>
                </div>
            </div>
            <!-- end .section row section -->
        </div>
        <div id="sbject-data-detail" style="display:none">
            <div class="section-divider mv40">
                <span><i class="fa fa-book"></i>&nbsp;Exam Class Subject</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{ trans('language.global_min_mark')}}</span></label>
                    <label for="global_min_mark" class="field prepend-icon">
                        {!! Form::number('global_min_mark','', ['class' => 'gui-input','id' => 'global_min_mark','marks-type'=>'min','pattern'=>'[0-9]*']) !!}
                        <label for="global_min_mark" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{ trans('language.global_max_mark')}}</span></label>
                    <label for="global_max_mark" class="field prepend-icon">
                        {!! Form::number('global_max_mark', '', ['class' => 'gui-input','id' => 'global_max_mark','marks-type'=>'max','pattern'=>'[0-9]*']) !!}
                        <label for="global_max_mark" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                </div>
            </div>
            <div class="section " id="spy1">
                <div class="" id="accordion2">
                    <div id="subject-marks-table" class="classopen">
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
            </div>
        </div>
    </div>
</div>
@endif