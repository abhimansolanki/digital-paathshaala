<style type="text/css">
    .marks {
        /*height: 25px;*/
        width: 90%;
    }
    .state-error{
        display: block!important;
        margin-top: 5px !important;
        padding: 0;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 9px !important;
        color: #DE888A !important;
    }
    .padding_remove{
        padding: 0px 0px !important;
    }
    .tabletable input{
        text-align: center;
    }
    .form-horizontal .form-group{
        margin-left: 0px !important;
        margin-right: 0px !important;
    }
    .subject_type_checkbox{
        margin-top:10px !important;
    }
</style>
@if(!empty($arr_subject))
<div class="tabletable" style="overflow-x: scroll; width: 100%">
    <table class="table table-bordered" style="font-size: 12px;width:auto;">
        <thead>
            <tr style="font-size: 12px !important; background-color: #eeeeee;">
                <th style="text-align: center;">
	        <div class="option-group field" id="">
		<label class="option block option-primary" style="">
		    {!! Form::checkbox('check_all','','', ['class' => 'check_all', 'id' => 'check_all']) !!}
		    <span class="checkbox radioBtnpan"></span><em></em>
		</label>
	        </div>
                </th>
                <th style="text-align: center;">{!!trans('language.subject') !!}</th>
                @foreach($arr_exam_type as $exam_type)
                <th style="text-align: center;">{!!$exam_type['exam_type'] !!} - Min | Max</th>
                @endforeach
                <th style="text-align: center; width: 100px !important;">Total - Max</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($arr_subject)) 
            @foreach($arr_subject as $key => $subject_detail)
            @php  $subject_id = $subject_detail['subject_id']; $total_marks=''; 
            $checked = false; 
            $required = false; 
            @endphp 

            @if(isset($subject_detail['subject_marks']['total_max_marks'])) 
            @php $total_marks = $subject_detail['subject_marks']['total_max_marks']; 
            $checked = true; 
            $required = true;
            @endphp 
            @endif 
            <tr>
                <td>
                    <div class="option-group field" id="">
		<label class="option block option-primary" style="margin-top:auto;">
		    {!! Form::checkbox('arr_subject_id[]',$subject_id,$checked,['id'=>'checked_subject_id'.$subject_id,'class'=>'arr_checked_subject_id','required'=>true]) !!}
		    <span class="checkbox radioBtnpan"></span><em></em>
		</label>
	        </div>
                </td>
                <td>{!! $subject_detail['subject_name'] !!}
                </td>

                @foreach($arr_exam_type as $exam_type_id => $exam_type)
                @php $exam_type_id = $exam_type['exam_type_id'] ; 
                $min_marks = null; $max_marks = null;$type_checked = false; @endphp 
                <td style="">
	        {!! Form::hidden('exam_type_id[]',$exam_type_id, ['readonly'=>true]) !!} 
                    <!--check exam type match in subject type-->
                    @if(isset($subject_detail['subject_marks'][$exam_type_id]['min_marks'])) 
                    @php $min_marks = $subject_detail['subject_marks'][$exam_type_id]['min_marks'];@endphp 
                    @endif 
                    @if(in_array($exam_type_id,$subject_detail['subject_type_id'])) 
                    @php $type_checked = true;@endphp 
                    @endif 

                    @if(isset($subject_detail['subject_marks'][$exam_type_id]['max_marks'])) 
                    @php $max_marks = $subject_detail['subject_marks'][$exam_type_id]['max_marks'];@endphp 
                    @endif 
                    <div style="height:50px !important; margin-top:5px;">
                        <div class="col-md-5 padding_remove">
                            <div class="form-group"> 
                                {!! Form::number('min_marks_'.$subject_id.'_'.$exam_type_id,old('min_marks',isset($min_marks) ? $min_marks : ''), ['class' => 'marks minimum_val subject_min_mark'.$subject_id,'id' => 'min_marks'.$subject_id.'_'.$exam_type_id,'min'=>'0','readonly'=>true]) !!} 
                            </div>
                        </div>
                        <div class="col-md-5 padding_remove">
                            <div class="form-group"> 
                                {!! Form::number('max_marks_'.$subject_id.'_'.$exam_type_id,old('max_marks',isset($max_marks) ? $max_marks : ''), ['class' => 'marks maximum_val count_marks subject_max_mark'.$subject_id,'id' =>'max_marks'.$subject_id.'_'.$exam_type_id,'rel'=>$subject_id,'min'=>'0','readonly'=>true]) !!}
                            </div>
                        </div>
		<div class="col-md-2 padding_remove">
                            <div class="form-group"> 
                                {!! Form::checkbox('subject_type_'.$subject_id.'_'.$exam_type_id,'',$type_checked, ['class' =>'subject_type_checkbox subject_checkbox'.$subject_id,'id' => 'min_marks'.$subject_id.'_'.$exam_type_id,'rel'=>$subject_id.'_'.$exam_type_id]) !!} 
                            </div>
                        </div>

                    </div>

                </td>
        <div class="clearfix"></div>
        @endforeach
        <td>
            <div>
                {!! Form::number('total_marks_'.$subject_id,old('total_marks',isset($total_marks) ? $total_marks : ''), ['class' => 'total_mar total_marks_sum','id' => 'total_marks'.$subject_id,'readonly'=>true,'rel'=>$subject_id,'min'=>'0'])!!}
            </div>
        </td>
        </tr>
        @endforeach 
        @endif
        </tbody>
    </table>
</div>
@endif

