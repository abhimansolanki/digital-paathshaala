@extends('admin_panel/layout')
@push('styles')
<style>
    .bootstrap-tagsinput {
        width: 100%;
    }
</style>
@endpush
@section('content')
<div class="row">

    <div class="col-md-12">

        <div class="col-md-12">

            <div class="box box-info">
                {!! Form::open(['files'=>TRUE,'id' => 'exam-class-form' , 'class'=>'form-horizontal','url' =>$save_url,'autocomplete'=>'off']) !!}
                @include('backend.exam-class._form',[$submit_button])
                {!! Form::close() !!}
            </div>
<!--            <div class="tray tray-center tableCenter">
                <div class="">
                    <div class="panel panel-visible" id="spy2">
                        <div class="panel-heading">
                            <div class="panel-title hidden-xs">
                                <span class="glyphicon glyphicon-tasks"></span> <span class="" class="" title="">{{ trans('language.exam_class_marks')}}</span>
                            </div>
                        </div>
                        <div class="panel-body pn">
                            <table class="table table-bordered table-striped table-hover" id="exam-class-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>{!!trans('language.exam_name') !!}</th>
                                        <th>{!!trans('language.class_name') !!}</th>
                                        <th>{!!trans('language.section_name') !!}</th>
                                        <th>{!!trans('language.exam_type') !!}</th>
                                        <th>{!!trans('language.action') !!}</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var exam_class_id = $("#exam_class_id").val();
        if (exam_class_id !== '')
        {
            getSubjectMarks();
        }
//        var table = $('#exam-class-table').DataTable({
//            processing: true,
//            serverSide: true,
//            ajax: {
//                url: "{{url('exam-classes/data')}}",
//                data: function (f) {
//                    f.exam_id = $("#exam_id").val();
//                }
//            },
//            "aaSorting": [[0, "ASC"]],
//            columns: [
//                {data: 'exam_name', name: 'exam_name'},
//                {data: 'class_name', name: 'class_name'},
//                {data: 'section_name', name: 'section_name'},
//                {data: 'exam_type_name', name: 'exam_type_name'},
//                {data: 'action', name: 'action', orderable: false, searchable: false}
//
//            ]
//        });
//
//        $(document).on("click", ".delete-button", function (e) {
//            e.preventDefault();
//            var id = $(this).attr("data-id");
//
//            bootbox.confirm({
//                message: "Are you sure to delete ?",
//                buttons: {
//                    confirm: {
//                        label: 'Yes',
//                        className: 'btn-success'
//                    },
//                    cancel: {
//                        label: 'No',
//                        className: 'btn-danger'
//                    }
//                },
//                callback: function (result) {
//                    if (result)
//                    {
//                        var token = '{!!csrf_token()!!}';
//                        $.ajax(
//                                {
//                                    url: "{{ url('exam-class/delete/') }}",
//                                    datatType: 'json',
//                                    type: 'POST',
//                                    data: {
//                                        'exam_class_id': id,
//                                    },
//                                    success: function (res)
//                                    {
//                                        if (res.status == "success")
//                                        {
//                                            table.ajax.reload();
//                                        } else if (res.status === "used")
//                                        {
//                                            $('#server-response-message').text(res.message);
//                                            $('#alertmsg-student').modal({
//                                                backdrop: 'static',
//                                                keyboard: false
//                                            });
//                                        } else
//                                        {
//                                            $('#server-response-message').text(res.message);
//                                            $('#alertmsg-student').modal({
//                                                backdrop: 'static',
//                                                keyboard: false
//                                            });
//                                        }
//                                    }
//                                });
//                    }
//                }
//            });
//        });

        $("#exam-class-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             * $("#section_id option").length
             ------------------------------------------ */
            ignore: [],
            rules: {
                class_id: {
                    required: true,
                },
                session_id: {
                    required: true
                },
                'section_id': {
                    required: true
                },
//                'exam_type_id[]': {
//                    required: true
//                },
                min_marks: {
                    required: true
                },
                max_marks: {
                    required: true
                },
                "arr_subject_id[]": {
                    required: true
                },
            },
            messages: {
                'arr_subject_id[]': {
                    required: "Select"
                }

            },

            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });


//        $('#exam_type_id').select2().on('change', function (e, data) {
//            $(this).valid();
//        });

        $(document).on('change', '#class_id,#session_id', function (e) {
            e.preventDefault();
            var class_id = $('#class_id').val();
            var session_id = $('#session_id').val();
            $('#section_id').empty();
            $('#section_id').append('<option value=""> --Select Section --</option>');
            if (class_id !== '' && session_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                        'session_id': session_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $.each(resopose_data, function (key, value) {
                                $('#section_id').append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                    }
                });
            }

        });

        $(document).on('click', '#get_subject_marks', function (e) {
            $(".global_min_mark").val(null);
            $(".global_max_mark").val(null);
            $("#subject-marks-table").html('');
            $("#sbject-data-detail").hide();
            if ($("#class_id").valid() && $("#session_id").valid() && $("#section_id").valid())
            {
                getSubjectMarks();
            }
        });

        function getSubjectMarks()
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-subject-marks-data')}}",
                datatType: 'json',
                type: 'POST',
                data: $("#exam-class-form").serialize(),
                beforeSend: function () {
//                    $("#LoadingImage").show();
                },
                success: function (response) {
                    $("#LoadingImage").hide();
                    if (response.status === 'success')
                    {
                        $("#subject-marks-table").html(response.data);
                        $("#sbject-data-detail").show();
                        countMaxMarks();
                    }
                    else
                    {
                        $('#server-response-message').html(response.message);
                        $('#alertmsg-student').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }

                }
            });
        }
        $(document).on('change', '#class_id', function (e) {
            $("#subject-marks-table").html('');
            $("#sbject-data-detail").hide();
        });

        $(document).on('change', '#global_min_mark,#global_max_mark', function (e) {
            var marks_type = $(this).attr('marks-type');
            var marks = $(this).val();
            marks = parseInt(marks);
            if (isNaN(marks))
            {
                var marks = 0;
            }
            if (marks_type === 'min')
            {
                if (marks > 0)
                {
                    $('.arr_checked_subject_id').each(function () {
                        if ($(this).prop("checked"))
                        {
                            var rel = $(this).val();
                            $(".subject_min_mark" + rel).val(marks);
                        }
                    });

                } else
                {
                    $(".minimum_val").val(null);
                }
            } else if (marks_type === 'max')
            {
                if (marks > 0)
                {
                    $('.arr_checked_subject_id').each(function () {
                        if ($(this).prop("checked"))
                        {
                            var rel = $(this).val();
                            $(".subject_max_mark" + rel).val(marks);
                        }
                    });

                } else
                {
                    $(".maximum_val").val(null);
                }
                countMaxMarks();
            }
        });

        $(document).on('change', '.maximum_val', function (e) {
            countMaxMarks();
        });
        function countMaxMarks()
        {
            $('.total_marks_sum').each(function () {
                var subject_id = $(this).attr('rel');
                var total_subject_max_marks = 0;
                $('.subject_max_mark' + subject_id).each(function () {
                    var subject_max_marks = $(this).val();
                    subject_max_marks = parseInt(subject_max_marks);
                    if (isNaN(subject_max_marks))
                    {
                        subject_max_marks = 0;
                    }
                    total_subject_max_marks = parseInt(total_subject_max_marks) + subject_max_marks;
                });
                $(this).val(total_subject_max_marks);
            });
        }

        $(document).on("click", "#check_all", function () {

            if ($(this).prop("checked"))
            {
                $(".arr_checked_subject_id").prop("checked", true);
                $(".subject_type_checkbox").prop("checked", true);

                // put minimum & maximum value
                $(".minimum_val").val($("#global_min_mark").val());
                $(".maximum_val").val($("#global_max_mark").val());

                // add required attr to subject marks
                $('.minimum_val').prop('required', true);
                $('.maximum_val').prop('required', true);
	    
                $('.minimum_val').prop('readonly', false);
                $('.maximum_val').prop('readonly', false);

            } else
            {
                $(".arr_checked_subject_id").prop("checked", false);
                $(".subject_type_checkbox").prop("checked", false);
	    
                // remove required attr to subject marks
                $('.minimum_val').prop('required', false);
                $('.maximum_val').prop('required', false);

                // put minimum & maximum value
                $(".minimum_val").val(null);
                $(".maximum_val").val(null);
	    
	    $('.minimum_val').prop('readonly', true);
                $('.maximum_val').prop('readonly', true);
            }
            countMaxMarks();

        });
        $(document).on("click", ".subject_type_checkbox", function () {

           var rel = $(this).attr('rel');
            if ($(this).prop("checked"))
            {
                $("#min_marks"+rel).attr('readonly',false);
                $("#max_marks"+rel).attr('readonly',false);
	    
	    $("#min_marks").val($("#global_min_mark").val());
                $("#max_marks").val($("#global_max_mark").val());
	    
	    $("#min_marks" + rel).prop('required', true);
                $("#max_marks" + rel).prop('required', true);

            } else
            {
              $("#min_marks"+rel).attr('readonly',true);
              $("#max_marks"+rel).attr('readonly',true); 
	  
              $("#min_marks"+rel).val(null);
              $("#max_marks"+rel).val(null);  
	  
	   $("#min_marks" + rel).prop('required', false);
               $("#max_marks" + rel).prop('required', false);
            }
	
	countMaxMarks();

        });

        $(document).on("click", ".arr_checked_subject_id", function () {
            var subject_id = $(this).val();
            var global_min = null;
            var global_max = null;
            if ($(this).prop("checked"))
            {
	    
	    $(".subject_checkbox"+subject_id).prop("checked", true);
                global_min = $("#global_min_mark").val();
                global_max = $("#global_max_mark").val();

                $(".subject_max_mark" + subject_id).val(global_max);
                $(".subject_min_mark" + subject_id).val(global_min);

                // add required attr to subject marks
                $(".subject_max_mark" + subject_id).prop('required', true);
                $(".subject_min_mark" + subject_id).prop('required', true);
                
	     // add readonly attr to subject marks
                $(".subject_max_mark" + subject_id).prop('readonly', false);
                $(".subject_min_mark" + subject_id).prop('readonly', false);

            } else
            {
	    $(".subject_checkbox"+subject_id).prop("checked", false);
                $(".subject_max_mark" + subject_id).val(global_max);
                $(".subject_min_mark" + subject_id).val(global_min);

                // remove required attr to subject marks
                $(".subject_max_mark" + subject_id).prop('required', false);
                $(".subject_min_mark" + subject_id).prop('required', false);
           
            // add readonly attr to subject marks
                $(".subject_max_mark" + subject_id).prop('readonly', true);
                $(".subject_min_mark" + subject_id).prop('readonly', true);

        }
            countMaxMarks();
        });

    });
</script>
@endsection

