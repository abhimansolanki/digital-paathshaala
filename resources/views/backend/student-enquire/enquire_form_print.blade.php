<!doctype html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{{ trans('language.enquire_form')}} </title>
    </head>
    <style type="text/css">
        .addtext{
            position: relative;
            width: 300px;
            padding: 14px 55px;
            margin-right: -50px;
            float: right;
            position: relative;
            background: #000;
            color: #fff;
            font-size: 16px;
            text-transform: uppercase;
            margin-bottom: 70px;

        }
        .addtext:after {
            content: " ";
            position: absolute;
            display: block;
            width: 30px;
            height: 50.2px;
            top: 0;
            left: 0;
            z-index: -1;
            background: #000;
            transform-origin: top left;
            transform: skew(-30deg, 0deg);
        }

        body{
            font-size: 12px;
            margin:20px 15px;
        }
        .studentClass{
            float: right;
            padding: 5px;
            border: 1px solid;
            height: 120px;
            width: 100px;
            z-index: 999999;
            position: absolute;
            background: #ffff;
            right: 0px;
            top: -2%;
        }
        .logo{
            float: left;
            width: 20%;
            margin-top: -30px;
        }
        .addmiNo{
            width:250px;
            float: right;
            margin-right: 15.5%;
            margin-top: -82px;
        }
        .foemno{
            float: left;
            width: 70px;
	margin-top: -5px;
        }
        .inderform{
            float: right;
            width: 140px;
            margin-right: 20px;
            border-bottom: 1px dotted #000;
            text-align: left;
	margin-top: -5px;
        }
        .admisdate{
            float: right;
            width: 132px;
            border-bottom: 1px dotted #000;
            text-align: left;
            margin-right: 20px;
        }
        .admisdate1{
            float: left;
            width: 93px;
            margin-top: -6px;
        }
        .stduenamelabel{
            float: left;
            width: 14%;
            padding-top: 2px;
            font-weight: bold;
        }
        .lastone{
            float: left;
            width:10%;
            margin-top:-5px;
            /*padding-top: 2px;*/
            font-weight: bold;
        }
        .Signature{
            float: left;
            width: 30%;
            text-align: center;
            margin: 0px 20px;
        }
        .checkedby{
            float: left;
            margin-top: 15px;
            border-bottom: 1px solid #000;
            width: 20%;
            padding-bottom: 5px; 
        }
        .studneclasss{
            float: left;
            border-bottom: 1px dotted #000;
            width: 19%;
            margin-left: 2%;
            margin-right: 10px;
            padding-bottom: 5px; 
        }
        .gengerClass{
            float: left;
            border-bottom: 1px dotted #000;
            width: 37%;
            margin-right: 10px;
            margin-left: 2%;
            padding-bottom: 5px;  
        }
        .gengerClass235{
            float: left;
            margin-right: 10px;
            border-bottom: 1px dotted #000;
            width:34.6%;
            margin-left: 2%;
            padding-bottom: 5px; 
        }
        .gengerClass12352{
            float: left;
            margin-right: 10px;
            border-bottom: 1px dotted #000;
            width: 21%;
            margin-left: 2%;
            /*padding-bottom: 5px;*/    
        }
        .declare{
            font-size: 12px;
            float: left;
            width: 90%;
            line-height: 22px;
            padding-bottom: 15px;
        }
        .centerdive{
            position: relative;
            top: -20px;
            width: 200px;
            background: #000;
            color: #fff;
            padding: 6px 0px;
            left: 35%;
            right: 35%;
            text-align: center;
        }
        .dateoflabel{
            float: right;
        }
        .dateofdate{
            float: right;
            border-bottom: 1px dotted #000;
            padding-bottom: 5px;
            width: 20%;
            margin-left: 10px;
        }
        .footerstudent{
            float: left;
        }
        .footerright{
            float: left;
            border-bottom: 1px dotted #000;
            width: 46%;
            margin-left: 10px;
            margin-top: 12px;
            margin-right: 10px;
        }
        .psign{
            float: right;
            border-top: 1px solid #000;
            width: 25%;
            text-align: center;
/*            padding-top: 10px;
            margin-top: 10px;*/
            font-weight: bold;
        }
        .studentBox{
            height:20px;width:20px;border: solid 1px;margin: -2px; display: inline-block;;
        }
        /*        table, th, td {
                    border: 1px solid #cdcdcd;
                }*/
        table {
            border-collapse: collapse;
        }

        td {
            padding-top: .5em;
            padding-bottom: .3em;
        }
        table tr td:first-child{
            font-weight: bold

        }
    </style>
    @if(!empty($arr_enquire_data))
@foreach($arr_enquire_data as $key => $student_data)
    <body>
        <div class="logo">
            <img src="{{url(get_school_logo())}}" width="200">
        </div>
        <div class="addtext">
            <h3 style="padding: 0px; margin: 0px;">{{ trans('language.admission_form')}}</h3>

        </div>
        <div style="clear: both;"></div>
        <br>
        <div class="addmiNo">
            <br>
            <div class="foemno" > <label style="font-weight: bold;">{!! trans('language.form_number') !!} :</label></div>
            <div class="inderform" style="height: 10px !important;">{!! $student_data['form_number'] !!}</div>
            <div style="clear: both;"></div>
            <br>
            <div  class="admisdate1"> <label style="font-weight: bold;">{!! trans('language.admission_date') !!} :</label></div>
            <div  class="admisdate"></div>
            <div style="clear: both;"></div>
            <br>
            <div  class="admisdate1"> <label style="font-weight: bold;">{!! trans('language.enrollment_number') !!} :</label></div>
            <div  class="admisdate"></div>
            <div style="clear: both;"></div>
            <br>
            <div style="clear: both;"></div>
        </div>
        <div style="clear: both;"></div>
        <div class="studentClass">
            <img data-src="holder.js/93%x160" alt="holder" height="120">
        </div>
        <div style="clear: both;"></div>
        <hr style="border:1px solid"><br>
        <table class="table table-striped" style="width:100% !important">
            <tbody>
                <tr colspan="2">
                    <td style="width:10% !important"><div class=""> {!! trans('language.student_name') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="width:60% !important" colspan="2"> 
                        @for($i = 1; $i<=28; $i++)
                        <div class="studentBox"></div>
                        @endfor
                    </td>
                </tr>
                <tr colspan="2">
                    <td style="width:10% !important"><div class=""> {!! trans('language.name_hindi') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="width:60% !important;" colspan="2">
                        <div style="border-bottom:solid 1px;"></div>
                    </td>

                </tr>
                <tr>
                    <td style="width:10% !important"><div class=""> {!! trans('language.gender') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="width:10% !important;">
                        <div class="studentBox"></div> &nbsp;&nbsp;Boy &nbsp;&nbsp;&nbsp;&nbsp;<div class="studentBox"></div> &nbsp;&nbsp;Girl
                    </td>
                    <td style="width:30% !important;">
                        &nbsp;<b>{!! trans('language.caste_category') !!}</b> :&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;GEN &nbsp;&nbsp;&nbsp;&nbsp; <div class="studentBox"></div>
                        &nbsp;&nbsp;OBC &nbsp;&nbsp;&nbsp;&nbsp;<div class="studentBox"></div>
                        &nbsp;&nbsp;SC/ST &nbsp;&nbsp;&nbsp;&nbsp; <div class="studentBox"></div>
                        <!--&nbsp;&nbsp;SC    <div class="studentBox"></div>--> 
                    </td>
                </tr>
                <tr>
                    <td style="width:10% !important"><div class=""> {!! trans('language.date_of_b') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="">
                        @for($i = 1; $i<=8; $i++)
                        <div class="studentBox"></div>
                        @endfor
                    </td>
                    <td style="width:30% !important;">
                        <div style="border-bottom:solid 1px;margin-left: -10% !important;"></div>
                    </td>
                </tr>
                <tr colspan="2">
                    <td style="width:10% !important"><div class=""> {!! trans('language.birth_place') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="width:60% !important;" colspan="2">
                        <div style="border-bottom:solid 1px;"></div>
                    </td>
                </tr>
                <tr colspan="2">
                    <td style="width:10% !important"><div class=""> {!! trans('language.father_name') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="width:60% !important" colspan="2"> 
                        @for($i = 1; $i<=28; $i++)
                        <div class="studentBox"></div>
                        @endfor
                    </td>
                </tr>
                <tr>
                    <td style="width:10% !important"><div class=""> {!! trans('language.occupation') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="">
                        <div style="border-bottom:solid 1px; margin-top: 6px;"></div>
                    </td>
                    <td style="width:30% !important;">
                        &nbsp;<b>{!! trans('language.qualification') !!} : </b>&nbsp;&nbsp;&nbsp;&nbsp;
                        <div style="border-bottom:solid 1px; margin-left:25%;"></div>
                    </td>
                </tr>
                <tr>
                    <td style="width:10% !important"><div class=""> {!! trans('language.father_contact_number') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="">
                        <div style="border-bottom:solid 1px;"></div>
                    </td>
                    <td style="width:30% !important;">
                        &nbsp;<b>{!! trans('language.father_email') !!} :</b>&nbsp;&nbsp;&nbsp;&nbsp;
                        <div style="border-bottom:solid 1px; margin-left:15%;margin-top: -5px;"></div>
                    </td>
                </tr>
                <tr colspan="2">
                    <td style="width:10% !important"><div class=""> {!! trans('language.mother_name') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="width:60% !important" colspan="2"> 
                        @for($i = 1; $i<=28; $i++)
                        <div class="studentBox"></div>
                        @endfor
                    </td>
                </tr>
                <tr>
                    <td style="width:10% !important"><div class=""> {!! trans('language.occupation') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td >
                      <div style="border-bottom:solid 1px; margin-top: 6px;"></div>  
                    </td>
                    <td style="width:30% !important;">
                        &nbsp;<b>{!! trans('language.qualification') !!} :</b>&nbsp;&nbsp;&nbsp;&nbsp;
                        <div style="border-bottom:solid 1px; margin-left:25%;"></div>
                    </td>
                </tr>
                <tr>
                    <td style="width:10% !important"><div class=""> {!! trans('language.mother_contact_number') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="">
                        <div style="border-bottom:solid 1px;"></div>
                    </td>
                    <td style="width:30% !important;">
                        &nbsp;<b>{!! trans('language.mother_email') !!} :</b>&nbsp;&nbsp;&nbsp;&nbsp;
                        <div style="border-bottom:solid 1px; margin-left:20%;margin-top: -5px;"></div>
                    </td>
                </tr>
                <tr colspan="2">
                    <td style="width:10% !important"><div class="">Guardian Name : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="width:60% !important" colspan="2"> 
                        @for($i = 1; $i<=28; $i++)
                        <div class="studentBox"></div>
                        @endfor
                    </td>
                </tr>
                <tr>
                    <td style="width:10% !important"><div class="">Relation : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="">
                        <div style="border-bottom:solid 1px; margin-top: 12px;"></div>
                    </td>
                    <td style="width:30% !important;">
                        &nbsp;<b>Contact No :</b>&nbsp;&nbsp;&nbsp;&nbsp;
                        <div style="border-bottom:solid 1px; margin-left:25%;"></div>
                    </td>
                </tr>
<!--                <tr>
                    <td style="width:10% !important"><div class=""> {!! trans('language.qualification') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="">
                        <div style="border-bottom:solid 1px; margin-top: 12px;"></div>
                    </td>
                    <td style="width:30% !important;">
                        &nbsp;<b>{!! trans('language.guardian_email') !!} :</b>&nbsp;&nbsp;&nbsp;&nbsp;
                        <div style="border-bottom:solid 1px; margin-left:11%;"></div>
                    </td>
                </tr>-->
                <tr colspan="2">
                    <td style="width:10% !important"><div class="">{!! trans('language.previous_school_name') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="" colspan="2">
                        <div style="border-bottom:dashed 1px; margin-top: 12px;"></div>
                    </td>
                </tr>
                <tr>
                    <td style="width:10% !important"><div class=""> Class : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="">
                        <div style="border-bottom:dashed 1px; margin-top: 12px;"></div>
                    </td>
                    <td>
<!--                        &nbsp;<b>Result :</b>&nbsp;&nbsp;&nbsp;&nbsp;
                        <div style="border-bottom:dashed 1px; margin-left:11%; display: inline-table;width: 30%"></div>-->
                        &nbsp;<b>Year : </b>&nbsp;&nbsp;&nbsp;&nbsp;
                        <div style="border-bottom:dashed 1px; margin-left:11%;"></div>
                    </td>
                </tr>
                <tr colspan="2">
                    <td style="width:10% !important"><div class=""> {!! trans('language.tc_number') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="" colspan="2">
                        <div style="border-bottom:dashed 1px; margin-top: 12px;"></div>
                    </td>
                </tr>
                <tr colspan="2">
                    <td style="width:10% !important"><div class=""> {!! trans('language.address_line1') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="" colspan="2">
                        <div style="border-bottom:dashed 1px; margin-top: 12px;"></div>
                    </td>
                </tr>
                <tr colspan="2">
                    <td style="width:10% !important"><div class=""> {!! trans('language.address_line2') !!} : </div></td>
                    <td style="width:2% !important"></td>
                    <td style="" colspan="2">
                        <div style="border-bottom:dashed 1px; margin-top: 12px;"></div>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="stduenamelabel">Declaration:</div>
        <div class="declare">
            I have gone through the rules and regulation unmentioned in the school hand book and i agree to abide by
            the same with instruction issued from time to time by the school authorities the date of birth mentioned
            above is correct and i shall not request for it's change.
        </div>
        <div style="clear: both;"></div>
        <hr>
        <div class="stduenamelabel" style="margin-top:2%">Attachment Req:</div>
        <div class="declare" style="margin-left:10%;">
            T.C.(Original) &nbsp; &nbsp; &nbsp;<div class="studentBox"></div> &nbsp; 
            Mark Sheet (Photocopy) &nbsp; &nbsp; &nbsp;<div class="studentBox"></div> &nbsp; 
            Birth Certificate (Photocopy) &nbsp; &nbsp; &nbsp;<div class="studentBox"></div> &nbsp; 
        </div>
        <br>
        <!--style="margin-bottom:1%"-->
        <div>
            <div style="border-top:solid 1px; margin-top: 3%; text-align: center;width: 20%;padding-top:2px; float: left;">
                Sign. of Student
            </div>
            <div style="border-top:solid 1px; margin-top: 3%; text-align: center;width: 20%;padding-top:2px;float: right;">
                Sign. of Parent/Guardian
            </div>
            <div style="clear: both;"></div>
        </div>
        <div style="clear: both;"></div>
        <hr>
        <div class="centerdive">For Office Use Only</div>
        <div class="dateofdate"></div>
        <div class="dateoflabel" style="margin-top:-8px;">Date : </div>
        <div style="clear: both;"></div>
        <div class="footerstudent" >On Scrutiny, the student has been found eligible for class</div>
        <div class="footerright"  style="width: 39%;"></div>
        <div class="footerstudent">  is accordingly admit. </div>
        <div style="clear: both;"></div> <br>
        <div class="lastone" style="width: 12%;">Receipt No. :</div>
        <div class="studneclasss"></div>
        <div class="lastone" style="width:6%;">Class  :</div>
        <div class="gengerClass12352"></div>
        <div class="lastone" style="width: 13%;">{!! trans('language.enrollment_number') !!} :</div>
        <div class="gengerClass12352" style="margin-top:-10px;">{!! $student_data['form_number'] !!}</div>
        <div style="clear: both;"></div> <br>
        <div class="lastone" style="width: 12%;">Checked By :</div>
        <div class="checkedby"></div>
        <div class="Signature">Signature of Clerk</div>
        <div class="psign">Principle</div>
    </body>
      @endforeach
@endif
</html>