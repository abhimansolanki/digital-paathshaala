<style type="text/css">
    .admin-form .select{
        margin-top: 0px;
    }
    .select2-container .select2-selection--single{
        height: 35px !important;
    }
    .admin-form .select>select{
        height: 35px !important;
    }
    .state-error{
        display:block !important;
        margin-top: 6px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    }
</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>  Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_student_enquire')}}</li>
                </ol>
            </div>
            <div class="topbar-right">
                <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right" title="{{trans('language.view_student_enquire')}}">{{trans('language.view_student_enquire')}}</a>
            </div>
        </div>
{{--        @if( Session::has( 'success' ))--}}
        @include('backend.partials.messages')
{{--        @endif--}}

        <div class="bg-light panelBodyy" >
            <div class="section-divider mv40">
                <span><i class="fa fa-graduation-cap"></i> Academic Details</span>
            </div>
            <div class="section row marginRemove" id="spy1">
                {!! Form::hidden('student_enquire_id',old('student_enquire_id',isset($student_enquire['student_enquire_id']) ? $student_enquire['student_enquire_id'] : ''),['class' => 'gui-input', 'id' => 'student_enquire_id', 'readonly' => 'true']) !!}
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.form_number')}}<span class="asterisk">*</span></span></label>
                    <label for="form_number" class="field prepend-icon">
                        {!! Form::text('form_number', old('form_number',isset($student_enquire['form_number']) ? $student_enquire['form_number'] : ''), ['class' => 'gui-input', 'id' => 'form_number','readonly'=>true]) !!}
                        <label for="form_number" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('form_number')) 
                    <p class="help-block">{{ $errors->first('form_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $student_enquire['arr_session'],isset($student_enquire['session_id']) ? $student_enquire['session_id'] : null, ['class' => 'form-control','id'=>'session_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('session_id')) 
                    <p class="help-block">{{ $errors->first('session_id') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <span class="radioBtnpan">{{trans('language.enquire_date')}}<span class="asterisk">*</span></span>
                    <label for="enquire_date" class="field prepend-icon">
                        {!! Form::text('enquire_date', old('enquire_date',isset($student_enquire['enquire_date']) ? $student_enquire['enquire_date'] : date('d/m/Y')), ['class' => 'gui-input','id' => 'enquire_date', 'readonly']) !!}
                        <label for="enquire_date" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('enquire_date')) 
                    <p class="help-block">{{ $errors->first('enquire_date') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.class')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('class_id', $student_enquire['arr_class'],isset($student_enquire['class_id']) ? $student_enquire['class_id'] : '', ['class' => 'form-control','id'=>'class_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('class_id')) 
                    <p class="help-block">{{ $errors->first('class_id') }}</p>
                    @endif
                </div>

            </div>
            <div class="section-divider mv40">
                <span> <i class="fa fa-user"></i> Student Details</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.first_name')}}<span class="asterisk">*</span></span></label>
                    <label for="first_name" class="field prepend-icon">
                        {!! Form::text('first_name', old('first_name',isset($student_enquire['first_name']) ? $student_enquire['first_name'] : ''), ['class' => 'gui-input','id' => 'first_name']) !!}
                        <label for="first_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('first_name')) 
                    <p class="help-block">{{ $errors->first('first_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.middle_name')}}</span></label>
                    <label for="middle_name" class="field prepend-icon">
                        {!! Form::text('middle_name', old('middle_name',isset($student_enquire['middle_name']) ? $student_enquire['middle_name'] : ''), ['class' => 'gui-input','id' => 'middle_name']) !!}
                        <label for="middle_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('middle_name')) 
                    <p class="help-block">{{ $errors->first('middle_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.last_name')}}</span></label>
                    <label for="last_name" class="field prepend-icon">
                        {!! Form::text('last_name', old('last_name',isset($student_enquire['last_name']) ? $student_enquire['last_name'] : ''), ['class' => 'gui-input', 'id' => 'last_name']) !!}
                        <label for="last_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('last_name')) 
                    <p class="help-block">{{ $errors->first('last_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.gender')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('gender', $student_enquire['arr_gender'],isset($student_enquire['gender']) ? $student_enquire['gender'] : null, ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('gender')) 
                    <p class="help-block">{{ $errors->first('gender') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.dob')}}</span></label>
                    <label for="datepicker1" class="field prepend-icon">
                        {!! Form::text('dob', old('dob',isset($student_enquire['dob']) ? $student_enquire['dob'] : ''), ['class' => 'gui-input','id' => 'dob', 'readonly']) !!}
                        <label for="" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                </div>
                <div class="col-md-3">
                    @include('backend.partials.get-age')
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.caste_category')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('caste_category_id', $student_enquire['arr_caste_category'],isset($student_enquire['caste_category_id']) ? $student_enquire['caste_category_id'] : '', ['class' => 'form-control','id'=>'caste_category_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('caste_category_id')) 
                    <p class="help-block">{{ $errors->first('caste_category_id') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <span class="radioBtnpan">{{trans('language.birth_place')}}</span>
                    <label for="birth_place" class="field prepend-icon">
                        {!! Form::text('birth_place', old('birth_place',isset($student_enquire['birth_place']) ? $student_enquire['birth_place'] : ''), ['class' => 'gui-input ', 'id' => 'birth_place']) !!}
                        <label for="birth_place" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('birth_place')) 
                    <p class="help-block">{{ $errors->first('birth_place') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.address_line1')}}<span class="asterisk">*</span></span></label>
                    <label for="address_line1" class="field prepend-icon">
                        {!! Form::textarea("address_line1", old('address_line1',isset($student_enquire['address_line1']) ? $student_enquire['address_line1'] : ''), ["class" => "gui-input","id" => "address_line1"]) !!}
                        <label for="address_line1" class="field-icon">
                            <span class="glyphicons glyphicons-google_maps"></span>
                        </label>
                    </label>
                    @if ($errors->has('address_line1')) 
                    <p class="help-block">{{ $errors->first('address_line1') }}</p>
                    @endif
                </div>
                <div class="col-md-3 ">
                    <input type="checkbox" class="" id="copyAddress" name="ifaddressCopy" onclick="AddressCopy(this.form)">
                    <span style="font-size:12px;">(Copy address)</span>
                    <label for="copyAddress"><span class="radioBtnpan">{{trans('language.address_line2')}}</span></label>
                    <label for="address_line2" class="field prepend-icon">
                        {!! Form::textarea("address_line2", old('address_line2',isset($student_enquire['address_line2']) ? $student_enquire['address_line2'] : ''), ["class" => "gui-input", "id" => "address_line2"]) !!}
                        <label for="address_line2" class="field-icon">
                            <span class="glyphicons glyphicons-google_maps"></span>
                        </label>
                    </label>
                    @if ($errors->has('address_line2')) 
                    <p class="help-block">{{ $errors->first('address_line2') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <h2 class="lastschool">Last school details :</h2>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.previous_school_name')}}</span></label>
                    <label for="previous_school_name" class="field prepend-icon">
                        {!! Form::text('previous_school_name', old('previous_school_name',isset($student_enquire['previous_school_name']) ? $student_enquire['previous_school_name'] : ''), ['class' => 'gui-input  ', 'id' => 'previous_school_name']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-book"></i>
                        </label>
                    </label>
                    @if ($errors->has('previous_school_name')) 
                    <p class="help-block">{{$errors->first('previous_school_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.previous_class_name')}}</span></label>
                    <label class="field prepend-icon">
                        {!!Form::text('previous_class_name', old('previous_class_name',isset($student_enquire['previous_class_name']) ? $student_enquire['previous_class_name'] : ''),  ['class' => 'gui-input'])!!}
                        <label for="" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if($errors->has('previous_class_name')) 
                    <p class="help-block">{{ $errors->first('previous_class_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Result (in % or grade)</span></label>
                    <label class="field prepend-icon">
                        {!! Form::text("percentage", old('percentage',isset($student_enquire['percentage']) ? $student_enquire['percentage'] : ''), ["class" => "gui-input", "id" => "percentage"]) !!}
                        <label for="" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if($errors->has('percentage')) 
                    <p class="help-block">{{ $errors->first('percentage') }}</p>
                    @endif
                </div>
            </div>
            <div class="section-divider mv40">
                <span><i class="fa fa-user"></i> Father’s Details </span>
            </div>
            <div class="section row " id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_first_name')}}<span class="asterisk">*</span></span></label>
                    <label for="father_first_name" class="field prepend-icon">
                        {!! Form::text("father_first_name", old('father_first_name',isset($student_enquire['father_first_name']) ? $student_enquire['father_first_name'] : ''), ["class" => "gui-input", "id" => "father_first_name"]) !!}
                        <label for="father_first_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_first_name')) 
                    <p class="help-block">{{ $errors->first('father_first_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_middle_name')}}</span></label>
                    <label for="father_middle_name" class="field prepend-icon">
                        {!! Form::text("father_middle_name", old('father_middle_name',isset($student_enquire['father_middle_name']) ? $student_enquire['father_middle_name'] : ''), ["class" => "gui-input", "id" => "father_middle_name"]) !!}
                        <label for="father_middle_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_middle_name')) 
                    <p class="help-block">{{ $errors->first('father_middle_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_last_name')}}</span></label>
                    <label for="father_last_name" class="field prepend-icon">
                        {!! Form::text("father_last_name", old('father_last_name',isset($student_enquire['father_last_name']) ? $student_enquire['father_last_name'] : ''), ["class" => "gui-input","id" => "father_last_name"]) !!}
                        <label for="father_last_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_last_name')) 
                    <p class="help-block">{{ $errors->first('father_last_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_contact_number')}}<span class="asterisk">*</span></span></label>
                    <label for="father_contact_number" class="field prepend-icon">
                        {!! Form::number("father_contact_number", old('father_contact_number',isset($student_enquire['father_contact_number']) ? $student_enquire['father_contact_number'] : ''), ["class" => "gui-input","id" => "father_contact_number",'pattern'=>'[0-9]*']) !!}
                        <label for="father_contact_number" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_contact_number')) 
                    <p class="help-block">{{ $errors->first('father_contact_number') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row " id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_email')}}</span></label>
                    <label for="father_email" class="field prepend-icon">
                        {!! Form::email("father_email", old('father_email',isset($student_enquire['father_email']) ? $student_enquire['father_email'] : ''), ["class" => "gui-input", "id" => "father_email",'pattern'=>'^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$']) !!}
                        <label for="father_email" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_email')) 
                    <p class="help-block">{{ $errors->first('father_email') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_aadhaar_number')}}</span></label>
                    <label for="father_aadhaar_number" class="field prepend-icon">
                        {!! Form::number("father_aadhaar_number", old('father_aadhaar_number',isset($student_enquire['father_aadhaar_number']) ? $student_enquire['father_aadhaar_number'] : ''), ["class" => "gui-input","id" => "father_aadhaar_number",'pattern'=>'[0-9]*']) !!}
                        <label for="father_aadhaar_number" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_aadhaar_number')) 
                    <p class="help-block">{{ $errors->first('father_aadhaar_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_occupation')}}</span></label>
                    <label for="father_occupation" class="field prepend-icon">
                        {!! Form::text("father_occupation", old('father_occupation',isset($student_enquire['father_occupation']) ? $student_enquire['father_occupation'] : ''), ["class" => "gui-input","id" => "father_occupation"]) !!}
                        <label for="father_occupation" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_occupation')) 
                    <p class="help-block">{{ $errors->first('father_occupation') }}</p>
                    @endif
                </div>
            </div>
            <div class="section-divider mv40">
                <span><i class="fa fa-user"></i> Mother's Details </span>
            </div>
            <div class="section row " id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_first_name')}}<span class="asterisk">*</span></span></label>
                    <label for="mother_first_name" class="field prepend-icon">
                        {!! Form::text("mother_first_name", old('mother_first_name',isset($student_enquire['mother_first_name']) ? $student_enquire['mother_first_name'] : ''), ["class" => "gui-input",""=>trans('language.mother_first_name'), "id" => "mother_first_name"]) !!}
                        <label for="mother_first_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_first_name')) 
                    <p class="help-block">{{ $errors->first('mother_first_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_middle_name')}}</span></label>
                    <label for="mother_middle_name" class="field prepend-icon">
                        {!! Form::text("mother_middle_name", old('mother_middle_name',isset($student_enquire['mother_middle_name']) ? $student_enquire['mother_middle_name'] : ''), ["class" => "gui-input",""=>trans('language.mother_middle_name'), "id" => "mother_middle_name"]) !!}
                        <label for="mother_middle_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_middle_name')) 
                    <p class="help-block">{{ $errors->first('mother_middle_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_last_name')}}</span></label>
                    <label for="mother_last_name" class="field prepend-icon">
                        {!! Form::text("mother_last_name", old('mother_last_name',isset($student_enquire['mother_last_name']) ? $student_enquire['mother_last_name'] : ''), ["class" => "gui-input",""=>trans('language.mother_last_name'), "id" => "mother_last_name"]) !!}
                        <label for="mother_last_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_last_name')) 
                    <p class="help-block">{{ $errors->first('mother_last_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_contact_number')}}<span class="asterisk">*</span></span></label>
                    <label for="mother_contact_number" class="field prepend-icon">
                        {!! Form::number("mother_contact_number", old('mother_contact_number',isset($student_enquire['mother_contact_number']) ? $student_enquire['mother_contact_number'] : ''), ["class" => "gui-input","id" => "mother_contact_number",'pattern'=>'[0-9]*']) !!}
                        <label for="mother_contact_number" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_contact_number')) 
                    <p class="help-block">{{ $errors->first('mother_contact_number') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row " id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_email')}}</span></label>
                    <label for="mother_email" class="field prepend-icon">
                        {!! Form::email("mother_email", old('mother_email',isset($student_enquire['mother_email']) ? $student_enquire['mother_email'] : ''), ["class" => "gui-input", "id" => "mother_email",'pattern'=>'^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$']) !!}
                        <label for="mother_email" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_email')) 
                    <p class="help-block">{{ $errors->first('mother_email') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_aadhaar_number')}}</span></label>
                    <label for="mother_aadhaar_number" class="field prepend-icon">
                        {!! Form::number("mother_aadhaar_number", old('mother_aadhaar_number',isset($student_enquire['mother_aadhaar_number']) ? $student_enquire['mother_aadhaar_number'] : ''), ["class" => "gui-input", "id" => "mother_aadhaar_number",'pattern'=>'[0-9]*']) !!}
                        <label for="mother_aadhaar_number" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_aadhaar_number')) 
                    <p class="help-block">{{ $errors->first('mother_aadhaar_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.mother_occupation')}}</span></label>
                    <label for="mother_occupation" class="field prepend-icon">
                        {!! Form::text("mother_occupation", old('mother_occupation',isset($student_enquire['mother_occupation']) ? $student_enquire['mother_occupation'] : ''), ["class" => "gui-input", "id" => "mother_occupation"]) !!}
                        <label for="mother_occupation" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('mother_occupation')) 
                    <p class="help-block">{{ $errors->first('mother_occupation') }}</p>
                    @endif
                </div>
            </div>
            <div class="section-divider mv40">
                <span> <i class="fa fa-user"></i> Guardian’s Details </span>
            </div>
            <div class="section row " id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_first_name')}}</span></label>
                    <label for="guardian_first_name" class="field prepend-icon">
                        {!! Form::text("guardian_first_name", old('guardian_first_name',isset($student_enquire['guardian_first_name']) ? $student_enquire['guardian_first_name'] : ''), ["class" => "gui-input",""=>trans('language.guardian_first_name'), "id" => "guardian_first_name"]) !!}
                        <label for="guardian_first_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_first_name')) 
                    <p class="help-block">{{ $errors->first('guardian_first_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_middle_name')}}</span></label>
                    <label for="guardian_middle_name" class="field prepend-icon">
                        {!! Form::text("guardian_middle_name", old('guardian_middle_name',isset($student_enquire['guardian_middle_name']) ? $student_enquire['guardian_middle_name'] : ''), ["class" => "gui-input",""=>trans('language.guardian_middle_name'), "id" => "guardian_middle_name"]) !!}
                        <label for="guardian_middle_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_middle_name')) 
                    <p class="help-block">{{ $errors->first('guardian_middle_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_last_name')}}</span></label>
                    <label for="guardian_last_name" class="field prepend-icon">
                        {!! Form::text("guardian_last_name", old('guardian_last_name',isset($student_enquire['guardian_last_name']) ? $student_enquire['guardian_last_name'] : ''), ["class" => "gui-input",""=>trans('language.guardian_last_name'), "id" => "guardian_last_name"]) !!}
                        <label for="guardian_last_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_last_name')) 
                    <p class="help-block">{{ $errors->first('guardian_last_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_contact_number')}}</span></label>
                    <label for="guardian_contact_number" class="field prepend-icon">
                        {!! Form::number("guardian_contact_number", old('guardian_contact_number',isset($student_enquire['guardian_contact_number']) ? $student_enquire['guardian_contact_number'] : ''), ["class" => "gui-input", "id" => "guardian_contact_number",'pattern'=>'[0-9]*']) !!}
                        <label for="guardian_contact_number" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_contact_number')) 
                    <p class="help-block">{{ $errors->first('guardian_contact_number') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_email')}}</span></label>
                    <label for="guardian_email" class="field prepend-icon">
                        {!! Form::email("guardian_email", old('guardian_email',isset($student_enquire['guardian_email']) ? $student_enquire['guardian_email'] : ''), ["class" => "gui-input","id" => "guardian_email",'pattern'=>'^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$']) !!}
                        <label for="guardian_email" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_email')) 
                    <p class="help-block">{{ $errors->first('guardian_email') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_relation')}}</span></label>
                    <label for="guardian_relation" class="field prepend-icon">
                        {!! Form::text("guardian_relation", old('guardian_relation',isset($student_enquire['guardian_relation']) ? $student_enquire['guardian_relation'] : ''), ["class" => "gui-input","id" => "guardian_relation"]) !!}
                        <label for="guardian_relation" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_relation')) 
                    <p class="help-block">{{ $errors->first('guardian_relation') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_aadhaar_number')}}</span></label>
                    <label for="guardian_aadhaar_number" class="field prepend-icon">
                        {!! Form::number("guardian_aadhaar_number", old('guardian_aadhaar_number',isset($student_enquire['guardian_aadhaar_number']) ? $student_enquire['guardian_aadhaar_number'] : ''), ["class" => "gui-input","id" => "guardian_aadhaar_number",'pattern'=>'[0-9]*']) !!}
                        <label for="guardian_aadhaar_number" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_aadhaar_number')) 
                    <p class="help-block">{{ $errors->first('guardian_aadhaar_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.guardian_occupation')}}</span></label>
                    <label for="guardian_occupation" class="field prepend-icon">
                        {!! Form::text("guardian_occupation", old('school_name',isset($student_enquire['guardian_occupation']) ? $student_enquire['guardian_occupation'] : ''), ["class" => "gui-input",""=>trans('language.guardian_occupation'), "id" => "guardian_occupation"]) !!}
                        <label for="guardian_occupation" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('guardian_occupation')) 
                    <p class="help-block">{{ $errors->first('guardian_occupation') }}</p>
                    @endif
                </div>
            </div>
            <div class="section-divider mv40">
                <span><i class="fa fa-rupee"></i>  Fee Details </span>
            </div>
            <div class="section row " id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.form_fee')}}</span></label>
                    <label for="form_fee" class="field prepend-icon">
                        {!! Form::number('form_fee', old('form_fee',isset($student_enquire['form_fee']) ? $student_enquire['form_fee'] : ''), ['class' => 'gui-input','id' => 'form_fee','readonly'=>true,'pattern'=>'[0-9]*']) !!}
                        <label for="form_fee" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('form_fee')) 
                    <p class="help-block">{{ $errors->first('form_fee') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <br><br>
                    <div class="radio-custom radio-primary mb5">
                        @php $yes_paid = ''; @endphp
                        @if($student_enquire['form_fee_paid'] == 1)
                        @php $yes_paid = 'checked'; @endphp
                        @endif
                        @php $not_paid = ''; @endphp
                        @if($student_enquire['form_fee_paid'] == 0)
                        @php $not_paid = 'checked'; @endphp
                        @endif
                        Pay : &nbsp;
                        <input type="radio" id="yes" value="1" name="form_fee_paid" {{$yes_paid}}>
                        <label for="yes">Yes</label>
                        <input type="radio" id="no" value="0" name="form_fee_paid" {{$not_paid}}>
                        <label for="no">No</label>
                    </div>
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Remarks</span></label>
                    <label for="remark" class="field prepend-icon">
                        {!! Form::text('remark', old('remark',isset($student_enquire['remark']) ? $student_enquire['remark'] : ''), ['class' => 'gui-input',''=>trans('language.remark'), 'id' => 'remark']) !!}
                        <label for="remark" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('remark')) 
                    <p class="help-block">{{ $errors->first('remark') }}</p>
                    @endif
                </div>
            </div>
            <!-- end .form-body section -->
            <div class="panel-footer text-right">
                {!! Form::button('Cancel', ['class' => 'button btn-primary','id'=>'form-reload','style'=>'height:auto;width:15%']) !!}
                {!! Form::submit('Submit Form', ['class' => 'button btn-primary']) !!}
            </div>
        </div>
        <!-- end .form-footer section -->
    </div>
</div>
@include('backend.partials.popup')
<style type="text/css">
    .admin-form .radio:before{
        margin: 5px;
    }
</style>
<script type="text/javascript">
    $("#class_id").select2();
    function AddressCopy(f) {
        $("#address_line2").prop('readonly', false);
        f.address_line2.value = '';
        if (f.ifaddressCopy.checked === true) {
            f.address_line2.value = f.address_line1.value;
            $("#address_line2").prop('readonly', true);
        }
    }
    jQuery(document).ready(function () {

         $("#enquire_date").datepicker('destroy');
            $("#enquire_date").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                minDate: "{!! $student_enquire['start_date']!!}",
                maxDate: "{!! $student_enquire['end_date'] !!}",
            });

        $("#dob").datepicker('destroy');
        $("#dob").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            maxDate: "-2Y",
            minDate: "-50Y",
            yearRange: "-50:-2"
        });
       
        var dob = $("#dob").val();
        if (dob !== '')
        {
            getAge(dob);
        }

        $("#student-enquire-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                form_fee: {
                    required: true
                },
                class_id: {
                    required: true
                },
                form_number: {
                    required: true
                },
                first_name: {
                    required: true,
                    lettersonly: true,
                    // nowhitespace: true
                },
                last_name: {
//                    required: true,
                    lettersonly: true,
                    // nowhitespace: true
                },
                gender: {
                    required: true
                },
                caste_category_id: {
                    required: true,
                },
                session_id: {
                    required: true,
                },
                father_first_name: {
                    required: true,
                    lettersonly: true,
                    // nowhitespace: true
                },
                father_last_name: {
//                    required: true,
                    lettersonly: true
                },
                father_contact_number: {
                    required: true,
                },
                father_occupation: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                father_email: {
                    // nowhitespace: true
                },
                mother_first_name: {
                    required: true,
                    lettersonly: true,
                    // nowhitespace: true
                },
                mother_last_name: {
//                    required: true,
                    lettersonly: true,
                    // nowhitespace: true
                },
                mother_contact_number: {
                    required: true,
                },
                address_line1: {
                    required: true,
                    // nowhitespace: true
                },
                address_line2: {
                    // nowhitespace: true
                },
                enquire_date: {
                    required: true,
                },
                mother_occupation: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                father_middle_name: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                mother_middle_name: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                middle_name: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                mother_email: {
                    // nowhitespace: true
                },
                guardian_first_name: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                guardian_middle_name: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                guardian_last_name: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                guardian_occupation: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                birth_place: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                previous_school_name: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                previous_class_name: {
                    lettersonly: true,
                    // nowhitespace: true
                },
                percentage: {
                    // nowhitespace: true
                },
            },
            messages: {
                father_contact_number: {
                    remote: "Father's number already exist, please try with other"
                },
                mother_contact_number: {
                    remote: "Mother's number already exist, please try with other"
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        $('#enquire_date').datepicker().on('change', function (ev) {
            $(this).valid();
        });

        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || !/[~`!@#$%\^&*()+=\-\_\.\[\]\\';,/{}|\\":<>\?]/.test(value) || /^[a-zA-Z][a-zA-Z0-9\s\.]+$/i.test(value);
        }, "Please enter valid data, except spacial characters");

        $(document).on('change', '#session_id', function () {
            var session_id = $("#session_id").val();
            $("#enquire_date").val('');
            getDateData(session_id);
        });

        $('#dob').datepicker().on('change', function (ev) {
            getAge($(this).val());
        });
        $('#class_id').on('change', function (ev) {
            $(this).valid();
        });
       
        $(document).on('click', '#form-reload', function (e) {
            location.reload();
        });
    });

</script>
