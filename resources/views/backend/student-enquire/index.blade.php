@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
    #session_id {
        padding:0px !important;
        height: 30px !important;
    }
</style>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-7">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{trans('language.view_student_enquire')}}</span>
                </div>
                <div class="topbar-right">
                    <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right custom-btn">{{trans('language.add_student_enquire')}}</a>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="student-enquire-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>{{trans('language.form_number')}}</th>
                            <th>{{trans('language.enquire_date')}}</th>
                            <th>{{trans('language.class_name')}}</th>
                            <th>{{trans('language.student_name')}}</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>{{trans('language.father_contact_number')}}</th>
                            <th>{{trans('language.address')}}</th>
                            <th class="text-center">{{trans('language.action')}}</th>
                            <th class="text-center">{{trans('language.view_print')}}</th>
                            <!--<th class="text-center">{{trans('language.convert_admission')}}</th>-->
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        getStudentEnquire();
        function getStudentEnquire() {
            $('#student-enquire-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'Student Enquiry',
                        "filename": 'student-enquire',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Student Enquiry',
                        "filename": 'student-enquire',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7],
                            modifier: {
                                selected: true
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: "{{ url('student-enquire/data') }}",
                    data: function (f) {
                        
                    }
                },
                aaSorting: [[0, "desc"]],
                columns: [
                    {data: 'student_enquire_id', name: 'student_enquire_id'},
                    {data: 'form_number', name: 'form_number'},
                    {data: 'enquire_date', name: 'enquire_date'},
                    {data: 'class_name', name: 'class_name'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'father_name', name: 'father_name'},
                    {data: 'father_contact_number', name: 'father_contact_number'},
                    {data: 'address', name: 'address'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'view_print', name: 'view_print', orderable: false, searchable: false},
//                    {data: 'admission', name: 'admission', orderable: false, searchable: false},
                ]

            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
        }
        var table = $('#student-enquire-table').DataTable();
        table.on('select deselect', function (e) {
            var arr_checked_student = checkedStudent();
            if (arr_checked_student.length > 0)
            {
                $(".buttons-excel").prop('disabled', false);
                $(".buttons-print").prop('disabled', false);

            } else
            {
                $(".buttons-excel").prop('disabled', true);
                $(".buttons-print").prop('disabled', true);
            }
        });

        function checkedStudent()
        {
            var arr_checked_student = [];
            $.each(table.rows('.selected').data(), function () {
                arr_checked_student.push(this["student_enquire_id"]);
            });
            return arr_checked_student;
        }
        $(document).on('change', '#session_id', function (e) {
            getStudentEnquire();
        });
    });

</script>
</body>
</html>
@endsection
@push('scripts')
@endpush
