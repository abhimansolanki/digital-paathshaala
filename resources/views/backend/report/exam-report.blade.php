@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
    <style>
        button[disabled] {
            background: #76aaef !important;
        }

        .button {
            background: #2e76d6 !important;
            border-radius: 0px !important;
            border: 0px;
            outline: none;
            box-shadow: 0px;
            color: #fff;
            padding: 7px 10px;
            font-weight: bold;
            font-size: 11px;
            margin-top: 1%;
            z-index: 9999999;
            font-weight: normal;
        }
    </style>
    <div class="tray tray-center tableCenter">
        @include('backend.partials.loader')
        <div class="">
            <div class="panel panel-visible" id="spy2">
                <div class="panel-heading">
                    <div class="panel-title hidden-xs col-md-6">
                        <span class="glyphicon glyphicon-tasks"></span> <span>Exam Report</span>
                    </div>
                </div>
                <div class="panel" id="">
                    <div class="panel-body">
                        <div class="tab-content  br-n">
                            <div id="tab1_1" class="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('session_id',add_blank_option(get_session('yes'),'-- Select Session --'),$session['session_id'], ['class' => 'form-control','id'=>'session_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('class_id', $arr_class,'', ['class' => 'form-control','id'=>'class_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('section_id', $arr_section,'', ['class' => 'form-control','id'=>'section_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('exam_id', $arr_exam,'', ['class' => 'form-control','id'=>'exam_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">

                                        {!! Form::button('Download Excel', ['class' => 'btn-primary button','style'=>'','id'=>'excel-data']) !!}
                                        {!! Form::button('Print Summary', ['class' => 'btn-primary button','style'=>'','id'=>'print-data']) !!}
                                        {!! Form::button('Print Report Card', ['class' => 'btn-primary button','style'=>'','id'=>'print-report-card']) !!}
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body pn" style="overflow:auto;">
                    <div id="exam_marks_report"></div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            function examMarks() {
                $("#exam_marks_report").html('');
                var class_id = $('#class_id option:selected').val();
                var section_id = $('#section_id option:selected').val();
                var exam_id = $('#exam_id option:selected').val();
                var session_id = $('#session_id option:selected').val();
                if (class_id !== '' && session_id !== '' && section_id !== '' && exam_id !== '') {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url: "{{ url('exam-report-data')}}",
                        datatType: 'json',
                        type: 'GET',
                        data: {
                            class_id: class_id,
                            session_id: session_id,
                            section_id: section_id,
                            exam_id: exam_id,
                            type: 'report',
                        },
//                    beforeSend: function () {
//                        $("#LoadingImage").show();
//                    },
                        success: function (response) {
                            $("#exam_marks_report").html(response.data);
                        }
                    });
                }
            }

            $(document).on('change', '#class_id,#session_id', function (e) {
                getClassSection();
            });

            $(document).on('change', '#section_id', function (e) {
                getClassSectionExam();
            });
            $(document).on('change', '#exam_id,#class_id,#session_id,#section_id', function (e) {
                examMarks();
            });

            function getClassSection() {
                var class_id = $('#class_id').val();
                var session_id = $('#session_id').val();
                $('#section_id').empty();
                $('#section_id').append('<option value="">-- Select section --</option>');
                if (class_id !== '' && session_id !== '') {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url: "{{url('get-section-list')}}",
                        datatType: 'json',
                        type: 'POST',
                        data: {
                            'class_id': class_id,
                            'session_id': session_id,
                        },
                        beforeSend: function () {
                            $("#LoadingImage").show();
                        },
                        success: function (response) {
                            var resopose_data = [];
                            resopose_data = response.data;
                            if (response.status === 'success') {

                                $.each(resopose_data, function (key, value) {
                                    $('#section_id').append('<option value="' + key + '">' + value + '</option>');
                                });
                                $("#LoadingImage").hide();
                            }
                        }
                    });
                }
            }

            function getClassSectionExam() {
                var class_id = $('#class_id').val();
                var session_id = $('#session_id').val();
                var section_id = $('#section_id').val();
                $('#exam_id').empty();
                $('#exam_id').append('<option value="">-- Select exam --</option>');
                if (class_id !== '' && session_id !== '' && section_id !== '') {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url: "{{url('class-section-exam')}}",
                        datatType: 'json',
                        type: 'GET',
                        data: {
                            'class_id': class_id,
                            'session_id': session_id,
                            'section_id': section_id,
                        },
                        beforeSend: function () {
                            $("#LoadingImage").show();
                        },
                        success: function (response) {

                            $("#LoadingImage").hide();
                            $.each(response, function (key, value) {
                                $('#exam_id').append('<option value="' + key + '">' + value + '</option>');
                            });

                        }
                    });
                }
            }

            $(document).on("click", "#check_all", function () {

                if ($(this).prop("checked")) {
                    $(".arr_checked_student_id").prop("checked", true);


                } else {
                    $(".arr_checked_student_id").prop("checked", false);

                }

            });

            function checkedStudent() {
                var arr_checked_student = [];
                $('.arr_checked_student_id').each(function () {
                    if ($(this).prop("checked")) {
                        arr_checked_student.push($(this).val());
                    }

                });
                return arr_checked_student;
            }

            $(document).on('click', '#print-report-card', function (e) {
                var arr_checked_student = checkedStudent();
                if (arr_checked_student.length > 0) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url: "{{url('exam-report-data')}}",
                        datatType: 'json',
                        type: 'GET',
                        data: {
                            'class_id': $('#class_id').val(),
                            'session_id': $("#session_id").val(),
                            'section_id': $("#section_id").val(),
                            'exam_id': $("#exam_id").val(),
                            'type': 'print',
                            'arr_checked_student': arr_checked_student
                        },
                        beforeSend: function () {
                            $("#LoadingImage").show();
                        },
                        success: function (response) {
                            $("#LoadingImage").hide();
                            newWin = window.open("");
                            newWin.document.write('<html><head><title></title>');
                            newWin.document.title = "Exam Final Marksheet";
                            newWin.document.write('</head><body>');
                            newWin.document.write(response.data);
                            newWin.document.write('</body></html>');
                            setTimeout(function () {
                                newWin.print();
                                newWin.close();
                            }, 100);
//                    newWin.print();
//                    newWin.close();
                        }
                    });
                } else {
                    alert('Please select student');
                }
            });

            $(document).on('click', '#print-data', function () {
                printData();
            });

            // print class -subject mapping data
            function printData() {
                // var divToPrint = document.getElementById("single-exam-report");
                var divToPrint = $("#single-exam-report").clone()
                    .find('th:first-child, td:first-child').remove().end()
                    .prop('outerHTML');
                newWin = window.open("");
                newWin.document.write('<html><head><title></title>');
                newWin.document.write('<style>table {  border-collapse: collapse;  border-spacing: 0; width:100%; margin-top:20px;} .table td, .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{ padding:8px 18px; } .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {border: 1px solid #e2e2e2;} table, th, td{border:solid 1px;}</style>');
                newWin.document.title = "Class Subject Mapping report";
                newWin.document.write('</head><body>');
                newWin.document.write('<h3>Academic Year : ' + $('#session_id option:selected').html() + '</h3>')
                // newWin.document.write(divToPrint.outerHTML);
                newWin.document.write(divToPrint);
                newWin.document.write('</body></html>');
                newWin.print();
                newWin.close();
            }


            // download excel of data class-subject mapping
            $(document).on('click', '#excel-data', function () {
                var htmls_data_marks = "";
                var uri = 'data:application/vnd.ms-excel;base64,';
                var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
                var base64 = function (s) {
                    return window.btoa(unescape(encodeURIComponent(s)))
                };
                var format = function (s, c) {
                    return s.replace(/{(\w+)}/g, function (m, p) {
                        return c[p];
                    });
                };
                htmls_data_marks = '<h4>Academic Year: ' + $('#session_id option:selected').html() + '</h4>';
                htmls_data_marks += $('#single-exam-report-print').html();
                var ctx_marks = {
                    worksheet: 'Class Subject',
                    table: htmls_data_marks
                }
                var link = document.createElement("a");
                link.download = "exam-marks.xls";
                link.href = uri + base64(format(template, ctx_marks));
                link.click();
            });

        });

    </script>
    </body>
    </html>
@endsection




