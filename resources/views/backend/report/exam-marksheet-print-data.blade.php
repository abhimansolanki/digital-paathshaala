<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    .ach_title {
        padding: 15px;
        border-left: 1px solid black;
        border-bottom: 1px solid black;
        border-top: 1px solid black;
        margin-left: 20px;
        font-size: 15px;
        font-weight: 900;
        color: red;
        margin-top: 10px;
    }

    .address {
        padding: 10px;
    }

    .stu_detail_title {
        font-weight: bold;
        text-align: left;
        width: 23%;
        border: none;
    }

    .stu_detail_title_detail {
        text-align: left;
        width: 30%;
        border: none;
    }

    .stu_detail_title_empty {
        width: 15%;
        border: none;
    }
    .stu_detail_title_detail_s{
        border: none;
    }
    .sig_table {
        border: 0 !important;
    }

    @page {
        size: auto;
        margin: 20mm;
    }

    .pagebreak {
        page-break-after: always;
    }
</style>
@php $arr_subject = get_class_section_subject($session_id,$class_id,$section_id);
    $arr_exam_type = get_all_exam_type();
    $school = get_school_data();
    $session = get_current_session();
    $arr_grade = get_marks_grade($session_id);
    $tot_student = count($data['data']);
    $i = 1;
@endphp
@foreach($data['data'] as $key =>$value)
    @php $tot_exam = 0;$colspan1 =0; $colspan2 =0; $colspan3=0;$colspan4=0;
    $tot_exam = count($value['arr_exam']);
    $colspan1 =  $tot_exam * 4;
    $colspan2 = $colspan1;
    $colspan3 = $colspan1/2;
    $colspan4 = $colspan1+2/2;
    @endphp
    <div class="row">
        <div class="col-md-12" style="margin:5% 10%;">
            <table style="width:99%;border-bottom: none;" cellpadding="5">
                <tr>
                    <td style="width: 60%; border: none;">
                        <img src="{{ url(get_school_logo(true))}}" style="width: 100%">
                    </td>
                    <td style="width: 40%; padding: 0px; border: none;">
                        <table style="width: 100%; text-align: center; border: none;">
                            <tr style="border: none;">
                                <td style="border: none;">
                                    <div class="ach_title">
                                        ACHIEVEMENT RECORD
                                    </div>
                                </td>
                            </tr>
                            <tr style="border: none; font-size: 13px;">
                                <td class="address" style="border: none;">Year {!! $session['session_year'] !!}</td>
                            </tr>
                            <tr style="border: none; font-size: 13px;">
                                <td class="address" style="border: none;">{!! $school['address'] !!}, Mob. {!! $school['contact_number'] !!}</td>
                            </tr>
                            <tr style="border: none; font-size: 13px;">
                                <td class="address" style="border: none;">Effilated No. - {!! $school['effilated_number'] !!}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table style="width:99%; border-bottom:none;" cellpadding="5">
                <tbody>
                <tr>
                    <td class="stu_detail_title">Student Name :-</td>
                    <td class="stu_detail_title_detail">{!! $value['student_name'] !!}</td>
                    <td class="stu_detail_title_empty"></td>
                    <td class="stu_detail_title">Class(Sec) :-</td>
                    <td class="stu_detail_title_detail_s">{!! $value['class_name'].'('.$value['section_name'].')' !!}</td>
                </tr>
                <tr>
                    <td class="stu_detail_title">Father's Name :-</td>
                    <td class="stu_detail_title_detail">{!! $value['father_name'] !!}</td>
                    <td class="stu_detail_title_empty"></td>
                    <td class="stu_detail_title">{!! trans('language.enrollment_number') !!} :-</td>
                    <td class="stu_detail_title_detail_s">{!! $value['enrollment_number'] !!}</td>
                </tr>
                <tr>
                    <td class="stu_detail_title">Mother's Name :-</td>
                    <td class="stu_detail_title_detail">{!! $value['mother_name'] !!}</td>
                    <td class="stu_detail_title_empty"></td>
                    <td class="stu_detail_title">Roll No :-</td>
                    <td class="stu_detail_title_detail_s">{!! $value['roll_number'] !!}</td>
                </tr>
                <tr>
                    <td class="stu_detail_title">DOB :-</td>
                    <td class="stu_detail_title_detail">{!! date_format(date_create($value['dob']),'jS F, Y') !!}</td>
                    <td class="stu_detail_title_empty"></td>
{{--                    <td class="stu_detail_title">Attendance :-</td>--}}
{{--                    <td class="stu_detail_title_detail_s">100/250</td>--}}
                    <td class="stu_detail_title"></td>
                    <td class="stu_detail_title_detail_s"></td>
                </tr>
                </tbody>
            </table>
            <table style="width:99%" cellpadding="8">
                <tbody>
                <tr>
                    @php
                        $exam_per = 0;
                        $each_exam_width = 70 / count($value['arr_exam']);
                    @endphp
                    <th width="20%">SUBJECTS</th>
                    @foreach($value['arr_exam'] as $key =>$exam_data)
                        @php $exam_per = $exam_per + $exam_data['percentage']; @endphp
                        <th width="{!! $each_exam_width."%" !!}">{!! $exam_data['exam_alias'] !!}</th>
                    @endforeach
                    <th width="10%">GRADE</th>
                </tr>
                @foreach($arr_subject as $subject_id =>$subject)
                    @php $all_sub_tot = $all_sub_max_tot = 0; @endphp
                    <tr>
                        <td>{!! strtoupper($subject) !!}</td>
                        @foreach($value['arr_exam'] as $key =>$exam_data)
                            @php
                                $subject_marks = $exam_data['obtained_marks'];
                                $subject_marks_criteria = $exam_data['subject_marks_criteria'];
                                $sub_tot = isset($subject_marks[$subject_id]['sub_tot_obtained']) ? $subject_marks[$subject_id]['sub_tot_obtained'] : 0;
                                $all_sub_tot += + $sub_tot;
                                $all_sub_max_tot += $subject_marks_criteria[$subject_id]['total_max_marks'];
                            @endphp
                            @if(isset($subject_marks[$subject_id]['sub_tot_obtained']))
                                @php unset($subject_marks[$subject_id]['sub_tot_obtained']); @endphp
                            @endif
                            <td>{!! isset($subject_marks[$subject_id]['subject_grade']) ? $subject_marks[$subject_id]['subject_grade'] : "-" !!}</td>
                        @endforeach
                        @php
                            $all_sub_tot_percentage = 0;
                            if($all_sub_tot > 0) {
                                $all_sub_tot_percentage = round(($all_sub_tot * 100 / $all_sub_max_tot));
                            }
                        @endphp
                        @php $all_sub_grade = "-"; @endphp
                        @foreach($arr_grade as $grade)
                            @if(in_array($all_sub_tot_percentage,explode(',',$grade['range'])))
                                @php $all_sub_grade = $grade['grade_name']; @endphp
                            @endif
                        @endforeach
                        <td>{!! $all_sub_grade !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <br>
            <table style="border: none; margin-left: 2px; width:99%">
                <tr style="border: none;">
                    <td style="border: none;" width="50%">
                        <table style="width:99%" cellpadding="5">
                            <tr>
                                <td>Promoted to Class : &nbsp;&nbsp; {!! $value['promoted_to'] !!}</td>
                            </tr>
                            <tr>
                                <td>Remarks : &nbsp;&nbsp;
                                    @php
                                        $grade_comment = '';
                                        $final_per = round($exam_per/$tot_exam);
                                    @endphp
                                    @foreach($arr_grade as $grade)
                                        @if(in_array($final_per,explode(',',$grade['range'])))
                                            @php $grade_comment = $grade['grade_comments']; @endphp
                                        @endif
                                    @endforeach
                                    {!! $grade_comment !!}
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="border: none;">
                        <table style="width:99%; font-size: 13px;" cellpadding="5">
                            @php $middle = round(count($arr_grade) / 2); @endphp
                            <tr>
                                @foreach($arr_grade as $key => $grade)
                                    @if($middle > $key)
                                        <td>{!! $grade['grade_name'] !!}</td>
                                        <td>{!! $grade['maximum'] .'-'.$grade['minimum'] !!}</td>
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                @foreach($arr_grade as $key => $grade)
                                    @if($middle <= $key)
                                        <td>{!! $grade['grade_name'] !!}</td>
                                        <td>{!! $grade['maximum'] .'-'.$grade['minimum'] !!}</td>
                                    @endif
                                @endforeach
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
            <table class="sig_table" style="width:99%">
                <tr class="sig_table">
                    <td class="sig_table" colspan="{{$colspan4}}" style="float:left; margin:2%"> RESULT DATE : {!! $result_date !!} </td>
                    <td class="sig_table" colspan="{{$colspan4}}" style="float:right; margin:2%">SCHOOL REOPEN ON : {!! $reopen_date !!}</td>
                </tr>
            </table>
            <br>
            <table class="sig_table" style="width:99%">
                <tr class="sig_table">
                    <td class="sig_table" colspan="{{$colspan4}}" style="float:left; margin:2%"></td>
                    <td class="sig_table" colspan="{{$colspan4}}" style="float:right; margin:2%"></td>
                </tr>
                <tr>
                    <td colspan="{{$colspan4}}" class="sig_table" style="float:left; margin:2%">Principal Signature</td>
                    <td colspan="{{$colspan4}}" class="sig_table" style="float:right; margin:2%">Class Teacher Signature</td>
                </tr>
            </table>
        </div>
    </div>
    @if($i < $tot_student)
        <div class="pagebreak"></div>
    @endif
    @php  $i++; @endphp
@endforeach