@extends('admin_panel/layout')
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
</style>
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span>Student Transport Fee Details</span>
                </div>
            </div>
            <div class="panel" id="transportId">
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('class_id', $arr_class,'', ['class' => 'form-control','id'=>'class_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('student_id',$arr_student,'', ['class' => 'form-control','id'=>'student_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('vehicle_id',$arr_vehicle,'', ['class' => 'form-control','id'=>'vehicle_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::text('receipt_number','', ['class' => 'form-control','id'=>'receipt_number','placeholder'=>'Receipt Number'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!! Form::text('from_date','',['class' => 'form-control date_picker','id' => 'from_date', 'readonly' => 'readonly','placeholder'=>'From']) !!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!! Form::text('to_date','',['class' => 'form-control date_picker','id' => 'to_date', 'readonly' => 'readonly','placeholder'=>'To']) !!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-transport-fee" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>S.No</th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>{{trans('language.receipt_number')}}</th>
                            <th>{{trans('language.receipt_date')}}</th>
                            <th>{{trans('language.class_name')}}</th>
                            <th>Stu. Name</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>Mobile No</th>
                            <th>{{trans('language.route')}}</th>
                            <th>{{trans('language.stop_point')}}</th>
                            <th>{{trans('language.vehicle')}}</th>
                            <th>{{trans('language.total_amount')}}</th>
                        </tr>
                    </thead>
	        <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        studentTransportFeeReport();
        function studentTransportFeeReport()
        {
            var table = $('#student-transport-fee').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
//                bFilter:false,
                dom: 'Blfrtip',
                paging: false,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'Student Transport Fee Report',
                        "filename": 'student-transport-fee-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
                            modifier: {
                                selected: true
                            }
                        },
                        footer: true,
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Student Transport Fee Report',
                        "filename": 'student-transport-fee-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
                            modifier: {
                                selected: true
                            }
                        },
                        footer: true,
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: '{{ url('student_transport_fee_report_data')}}',
                    data: function (f) {
                        f.class_id = $('#class_id').val();
                        f.student_id = $('#student_id').val();
                        f.receipt_number = $('#receipt_number').val();
                        f.from_date = $('#from_date').val();
                        f.to_date = $('#to_date').val();
                        f.vehicle_id = $('#vehicle_id').val();
                    }
                },
                columns: [
                    {data: 'student_id', name: 'student_id'},
                    {data: 's_no', name: 's_no'},
                    {data: 'enrollment_number', name: 'enrollment_number'},
                    {data: 'receipt_number', name: 'receipt_number'},
                    {data: 'receipt_date', name: 'receipt_date'},
                    {data: 'class_name', name: 'class_name'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'father_name', name: 'care_of_name'},
                    {data: 'father_contact_number', name: 'father_contact_number'},
                    {data: 'route', name: 'route'},
                    {data: 'stop_point', name: 'stop_point'},
                    {data: 'vehicle', name: 'vehicle'},
                    {data: 'total_amount', name: 'total_amount'},
                ],
                drawCallback: function (row, data, start, end, display) {
                    var api = this.api();

                    $(api.column(0).footer()).html('');
                    $(api.column(1).footer()).html('');
                    $(api.column(2).footer()).html('');
                    $(api.column(3).footer()).html('');
                    $(api.column(4).footer()).html('');
                    $(api.column(5).footer()).html('');
                    $(api.column(6).footer()).html('');
                    $(api.column(7).footer()).html('');
                    $(api.column(8).footer()).html('');
                    $(api.column(9).footer()).html('');
                    $(api.column(10).footer()).html('');
                    $(api.column(11).footer()).html('');

                    // Total over this page
                    net_amount = api
                            .column(12, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);

                    // Update footer
                    $(api.column(12).footer()).html(net_amount);

                },
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
            table.on('select deselect', function (e, dt, type, indexes) {
                var arr_checked_student = checkedStudent();
                if (arr_checked_student.length > 0)
                {
                    $(".buttons-excel").prop('disabled', false);
                    $(".buttons-print").prop('disabled', false);
                } else
                {
                    $(".buttons-excel").prop('disabled', true);
                    $(".buttons-print").prop('disabled', true);
                }
            });
        }

        function checkedStudent()
        {
            var arr_checked_student = [];
            $.each($('#student-transport-fee').DataTable().rows('.selected').data(), function () {
                arr_checked_student.push(this["student_id"]);
            });
            return arr_checked_student;
        }
        $(document).on('change', '#class_id,#student_id,#receipt_number,#vehicle_id,#from_date,#to_date', function (e) {
            studentTransportFeeReport();
        });

    });

    $(document).on('change', '#class_id', function ()
    {
        $('select[name="student_id"]').empty();
        $('select[name="student_id"]').append('<option value="">--Select Student--</option>');
        var class_id = $("#class_id").val();
        if (class_id != '')
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-fee-student')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'class_id': class_id,
                },
                beforeSend: function () {
                    $('#LoadingImage').show();
                },
                success: function (response) {
                    var resopose_data = [];
                    resopose_data = response.data;
                    if (response.status == 'success')
                    {
                        $.each(resopose_data, function (key, value) {
                            $('select[name="student_id"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                    $('#LoadingImage').hide();
                }
            });
        }
    });

</script>
</body>
</html>
@endsection
