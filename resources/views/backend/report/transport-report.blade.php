@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
<div class="tray tray-center tableCenter">
    <style type="text/css">
        .buttons-html5{
            margin-right: 81px !important; 
        }
    </style>
    <div class="">
        @include('backend.partials.loader')
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="" class="" title="">Transport Report</span>
                </div>
            </div>
            <div class="panel" id="transportId">
                {!! Form::text('current_opened_tab','vehicle',['class'=>'gui-input','id'=>'current_opened_tab','readonly'=>true,'title'=>'Vehicle']) !!}
                <div class="panel-heading panelAnothertab">
                    <ul class="nav panel-tabs-border panel-tabs customTabingTrans">
                        <li class="active">
                            <a href="#tab1_1" class="current_tab" data-toggle="tab" aria-expanded="true" type='vehicle'  title ='Vehicle'>
                                <i class="fa fa-bus"></i> Vehicle</a>
                        </li>
                        <li class="">
                            <a href="#tab1_2" class="current_tab" data-toggle="tab" aria-expanded="false" type='route' title ='Destination'> 
                                <i class="fas fa-map-marked-alt"></i> Destination</a>
                        </li>
                        <li class="">
                            <a href="#tab1_3" class="current_tab" data-toggle="tab" aria-expanded="false" type='student' title ='Student Wise'>
                                <i class="fas fa-user-graduate"></i> Student Wise</a>
                        </li>
                        <li class="">
                            <a href="#tab1_4" class="current_tab" data-toggle="tab" aria-expanded="false" type='class' title ='Class Wise Student Route'>
                                <i class="fas fa-map-marker-alt"></i> Class Wise Student Route</a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('vehicle_id', $arr_vehicle,'', ['class' => 'form-control','id'=>'vehicle_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <!--<hr>-->
                                <div class="col-md-3"><span style="font-size:15px; font-weight: 600">Route : </span><span id="route"></span></div>
                                <div class="col-md-3"><span style="font-size:15px; font-weight: 600">Driver : </span><span id="driver_name"></span></div>
                                <div class="col-md-3"><span style="font-size:15px; font-weight: 600">Driver Mob : </span><span id="driver_contact_number"></span></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div id="tab1_2" class="tab-pane">
                            <div class="row">
                                <div class="col-md-2 ">
                                    <div class="section">
                                        {!!Form::select('route_id', $arr_route,'', ['class' => 'form-control','id'=>'route_id'])!!} 
                                    </div>
                                </div>
                                <div class="col-md-2 ">
                                    <div class="section">
                                        {!!Form::select('stop_point_id', $arr_stop_point,'', ['class' => 'form-control','id'=>'stop_point_id'])!!} 
                                    </div>
                                </div>
                                <div class="col-md-8">
                                </div>
                            </div>
                        </div>
                        <div id="tab1_3" class="tab-pane">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="section">
                                        {!! Form::text('first_name','',['class'=>'gui-input','id'=>'first_name','placeholder'=>'first name']) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        {!! Form::text('middle_name','',['class'=>'gui-input','id'=>'middle_name','placeholder'=>'middle name']) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        {!! Form::text('last_name','',['class'=>'gui-input','id'=>'last_name','placeholder'=>'last name']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">

                                </div>
                            </div>
                        </div>
                        <div id="tab1_4" class="tab-pane ">
                            <div class="row">
                                <div class="col-md-2 ">
                                    <div class="section">
                                        {!!Form::select('class_id', $arr_class,'', ['class' => 'form-control','id'=>'class_id'])!!} 
                                    </div>
                                </div>
                                <div class="col-md-2 ">
                                    <div class="section">
                                        {!!Form::select('section_id', $arr_section,'', ['class' => 'form-control','id'=>'section_id'])!!} 
                                    </div>
                                </div>
                                <div class="col-md-8">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-right">
                    <!--//data-target="#SendSMS"-->
                    <button type="button" class="btn btn-info" id='sms-button' style="margin-top: 14px;">Send SMS</button>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="transport-report-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>{{trans('language.type')}}</th>
                            <th>Scholar/Staff No</th>
                            <th>{{trans('language.name')}}</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>{{trans('language.father_contact_number')}}</th>
                            <th>Class/Department</th>
                            <th>{{trans('language.vehicle_number')}}</th>
                            <th>{{trans('language.stop_point')}}</th>
                            <th>{{trans('language.fair')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@include('backend.partials.send-sms')
<script>
    $(document).ready(function () {

        getDatatable();
        var table = $('#transport-report-table').DataTable();
        function getDatatable()
        {
            $('#transport-report-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
//                bFilter:false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'Transport Report by ' + $("#current_opened_tab").attr('title') + '',
                        "filename": 'transport-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Transport Report by ' + $("#current_opened_tab").attr('title') + '',
                        "filename": 'transport-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9],
                            modifier: {
                                selected: true
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: "{{ url('transport-report-data')}}",
                    data: function (f) {
                        f.vehicle_id = $('#vehicle_id').val();
                        f.route_id = $('#route_id').val();
                        f.stop_point_id = $('#stop_point_id').val();
                        f.class_id = $('#class_id').val();
                        f.section_id = $('#section_id').val();
                        f.first_name = $('#first_name').val();
                        f.middle_name = $('#middle_name').val();
                        f.last_name = $('#last_name').val();
                        f.type = $("#current_opened_tab").val();

                    }
                },
                columns: [

                    {data: 'id', name: 'id'},
                    {data: 'type', name: 'type'},
                    {data: 'code', name: 'code'},
                    {data: 'name', name: 'name'},
                    {data: 'father_name', name: 'father_name'},
                    {data: 'contact_number', name: 'contact_number'},
                    {data: 'from', name: 'from'},
                    {data: 'vehicle_number', name: 'vehicle_number'},
                    {data: 'stop_point', name: 'stop_point'},
                    {data: 'stop_fair', name: 'stop_fair'},
                ]
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
            $("#sms-button").prop('disabled', true);
        }


        $('#transport-report-table').DataTable().on('select deselect', function (e, dt, type, indexes) {
            var arr_checked_student = checkedUser();
            if (arr_checked_student.length > 0)
            {
                $(".buttons-excel").prop('disabled', false);
                $(".buttons-print").prop('disabled', false);
                $("#sms-button").prop('disabled', false);
            } else
            {
                $(".buttons-excel").prop('disabled', true);
                $(".buttons-print").prop('disabled', true);
                $("#sms-button").prop('disabled', true);
            }
        });


//        $('#transport-report-table tbody').on('click', 'input[type="checkbox"]', function (e) {
//            var row = $(this).closest('tr');
//            if (this.checked) {
//                row.addClass('selected');
//            } else {
//                row.removeClass('selected');
//            }
//        });

//        $(document).on('click', '#sms-button', function () {
//
//            $("#send_sms").prop('disabled', false);
//            $("#transport_sms_message").val('');
//            $("#success-message").text('');
//            $("#SendSMS").modal('show');
//
//        });
        function checkedUser()
        {
            var table = $('#transport-report-table').DataTable();
            var arr_checked_student = [];
            $.each(table.rows('.selected').data(), function () {
                arr_checked_student.push({id: this["user_id"], assigned_to: this["assigned_to"]});
            });
            return arr_checked_student;
        }

        $('#send_sms').on('click', function (e) {
            var arr_checked_student = checkedUser();
            if (arr_checked_student.length > 0 && $("#newModalFormId").valid())
            {
                var message = $.trim($("#sms_message").val());
                if (message != '')
                {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url: "{{url('transport-send-sms')}}",
                        datatType: 'json',
                        type: 'POST',
                        data: {
                            arr_student: arr_checked_student,
                            transport_sms_message: message,
                        },
                        beforeSend: function () {
                            $("#LoadingImage").show();
                        },
                        success: function (response) {
                            $("#LoadingImage").hide();
                            if (response.status == 'success')
                            {
                                $("#send_sms").prop('disabled', true);
                                $("#sms_message").val('');
                                $("#success-message").text('Messages  were added in queue and will be sending soon...');
                            } else
                            {
                                alert('something went wrong');
                            }
                        }
                    });
                } else
                {
                    alert('Please enter sms-text');
                }


            }
        });

        $(document).on('change', '#vehicle_id', function () {

            var vehicle_id = $(this).val();
            $("#route").text('');
            $("#driver_name").text('');
            $("#driver_contact_number").text('');
            if (vehicle_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('admin/get-vehicle-driver')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        vehicle_id: vehicle_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {

                        if (response.status == 'success')
                        {
                            $("#route").text(response.route);
                            $("#driver_name").text(response.driver_name);
                            $("#driver_contact_number").text(response.driver_contact_number);
                        } else
                        {
                            alert('something went wrong');
                        }
                        $("#LoadingImage").hide();
                    }
                });
            }
            getDatatable();
        });

        $(document).on('change', '#route_id, #class_id, #first_name,#middle_name,#last_name, #section_id,#stop_point_id', function () {
            getDatatable();

        });

        $(document).on('click', '.current_tab', function () {
            var type = $(this).attr('type');
            var title = $(this).attr('title');
            $("#current_opened_tab").val(type);
            $("#current_opened_tab").attr('title', title);
            getDatatable();
        });

        $(document).on('change','#class_id,#session_id', function (e) {
            e.preventDefault();
           var class_id = $('#class_id').val();
           var session_id = $('#session_id').val();
            if (class_id !== null && class_id !== null)
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                        'session_id': session_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },

                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $("#LoadingImage").hide();
                            $('select[name="section_id"]').empty();
                            $('select[name="section_id"]').append('<option value="">--Select section--</option>');
                            $.each(resopose_data, function (key, value) {
                                $('select[name="section_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        } else
                        {
                            $("#LoadingImage").hide();
                        }
                    }
                });
            }
        });

        $('#route_id').on('change', function (e) {
            e.preventDefault();
            var route_id = $(this).val();
            if (route_id !== null)
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-stop-point-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'route_id': route_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        if (response.status === 'success')
                        {
                            var resopose_data = [];
                            resopose_data = response.data;
                            $('select[name="stop_point_id"]').empty();
                            $('select[name="stop_point_id"]').append('<option value="">--Select stop point--</option>');
                            $.each(resopose_data, function (key, value) {
                                $('select[name="stop_point_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });

                        }
                        $("#LoadingImage").hide();
                    }
                });
            }
        });
    });
</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



