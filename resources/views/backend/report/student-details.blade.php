@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
</style>
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span>Student Details Report</span>
                </div>
            </div>
            <div class="panel" id="transportId">
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('session_id',add_blank_option(get_session('yes'),'-- Select Session --'),null, ['class' => 'form-control','id'=>'session_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('class_id', $arr_class,'', ['class' => 'form-control','id'=>'class_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('student_type_id', $arr_student_presence,1, ['class' => 'form-control','id'=>'student_type_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body pn" style="overflow:auto;">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-detail-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>SrNo</th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>Roll No</th>
                            <th>Stu. Name</th>
                            <th>Photo</th>
		    <th>{{trans('language.class_sec')}}</th>
		    <th>{{trans('language.father_name')}}</th>
		    <th>{{trans('language.father_m_number')}}</th>
		    <th>{{trans('language.mother_name')}}</th>
		    <th>{{trans('language.mother_m_number')}}</th>
		    <th>{{trans('language.father_income')}}</th>
                            <th>{{trans('language.gender')}}</th>
                            <th>{{trans('language.dob')}}</th>
                            <th>{{trans('language.dob_word')}}</th>
                            <th>Admission Date</th>
                            <th>{{trans('language.care_of_rel')}}</th>
                            <th>{{trans('language.caste_category')}}</th>
                            <th>{{trans('language.address_line1')}}</th>
		    <th>{{trans('language.address_line2')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        studentDetail();
        function studentDetail()
        {
            var table = $('#student-detail-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
//                bFilter:false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": $("#student_type_id option:selected").text() +' Student Details Report',
                        "filename": 'student-details-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,12,13,14,15,16,17,18],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": $("#student_type_id option:selected").text() +' Student Details Report',
                        "filename": 'student-details-report',
                        exportOptions: {
                            columns: [1,2,4, 7, 9, 6, 13,12, 15, 8, 17, 18],
                            modifier: {
                                selected: true
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: "{{ url('student_detail')}}",
                    data: function (f) {
                        f.class_id = $('#class_id option:selected').val();
                        f.sessionid = $("#session_id option:selected").val();
                        f.student_type_id = $("#student_type_id option:selected").val();
                    }
                },
	    
                columns: [
                    {data: 'student_id', name: 'student_id'},
                    {data: 'sr_no', name: 'sr_no'},
                    {data: 'enrollment_number', name: 'enrollment_number'},
                    {data: 'roll_number', name: 'roll_number'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'profile', name: 'profile'},
	        {data: 'class_name', name: 'class_name'},
	        {data: 'father_name', name: 'father_name'},
	        {data: 'father_contact_number', name: 'father_contact_number'},
	        {data: 'mother_name', name: 'mother_name'},
	        {data: 'mother_contact_number', name: 'mother_contact_number'},
	        {data: 'father_income', name: 'father_income'},
                    {data: 'gender_name', name: 'gender_name'},
                    {data: 'dob', name: 'dob'},
                    {data: 'dob_word', name: 'dob_word'},
                    {data: 'admission_date', name: 'admission_date'},
                    {data: 'care_of_rel', name: 'care_of_rel'},
                    {data: 'caste_name', name: 'caste_name'},
                    {data: 'address_line1', name: 'address_line1'},
                    {data: 'address_line2', name: 'address_line2'},
                ]
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
            table.on('select deselect', function (e, dt, type, indexes) {
                var arr_checked_student = checkedStudent();
                if (arr_checked_student.length > 0)
                {
                    $(".buttons-excel").prop('disabled', false);
                    $(".buttons-print").prop('disabled', false);
                } else
                {
                    $(".buttons-excel").prop('disabled', true);
                    $(".buttons-print").prop('disabled', true);
                }
            });
        }

        function checkedStudent()
        {
            var arr_checked_student = [];
            $.each($('#student-detail-table').DataTable().rows('.selected').data(), function () {
                arr_checked_student.push(this["student_id"]);
            });
            return arr_checked_student;
        }
        $(document).on('change', '#class_id,#session_id,#student_type_id', function (e) {
//            if($("#session_id").val() !=='')
//            {
             studentDetail();
//            }
        });

    });

</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



