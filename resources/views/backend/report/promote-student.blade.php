@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
    .state-error{
        display:block !important;
        margin-top: 6px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    }
    .div-width{
        width: 14.666667%;
    }
    .btn-div-width{
        padding-top: 5px;
        padding-left: 4px;
        width: 5.666667%;
    }
    #transportId button{
        top: 2px !important;
        right: 8px !important;
        z-index: 0 !important;
    }
</style>
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span>Promote Student</span>
                </div>
            </div>
            <div class="panel" id="transportId">
                {!! Form::open(['id'=>'promote-student-form','class'=>'']) !!}
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                <div class="col-md-2 div-width">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!! Form::select('class_id',add_blank_option($arr_class, 'Current class'),'', ['class' => 'form-control','id'=>'class_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 div-width">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('section_id',add_blank_option([],'Current section '),'', ['class' => 'form-control','id'=>'section_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 div-width">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('session_id',add_blank_option($arr_session, 'Current session') ,'', ['class' => 'form-control','id'=>'session_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 btn-div-width">
                                    <button type="button" class="btn btn-info" id='search_student'>Search</button>
                                </div>
                                <div class="col-md-2 div-width">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('promote_class_id',add_blank_option($arr_class, 'Promote class'),'', ['class' => 'form-control','id'=>'promote_class_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 div-width">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('promote_section_id',add_blank_option([],'Promote section '),'', ['class' => 'form-control','id'=>'promote_section_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 div-width">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('promote_session_id',add_blank_option($promote_sessions, 'Promote session') ,'', ['class' => 'form-control','id'=>'promote_session_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 btn-div-width">
                                    <button type="button" class="btn btn-info" id='promote_student'>Promote</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-promote-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>{{trans('language.class_name')}}</th>
                            <th>Stu. Name</th>
                            <th>{{trans('language.gender')}}</th>
                            <th>{{trans('language.dob')}}</th>
                            <th>Admission</th>
                            <th>{{trans('language.care_of_rel')}}</th>
                            <th>{{trans('language.care_of_name')}}</th>
                            <th>{{trans('language.care_of_contact')}}</th>
                            <th>{{trans('language.caste_category')}}</th>
                            <th>{{trans('language.address')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        studentDetail();
        function studentDetail()
        {
            var table = $('#student-promote-table').DataTable({
                destroy: true,
                bPaginate:false,
                processing: true,
                serverSide: true,
//                bFilter:false,
//                dom: 'Blfrtip',
//                buttons: [
//                    {
//                        extend: 'excelHtml5',
//                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
//                        "title": 'Student Promote Report',
//                        "filename": 'student-promote-report',
//                        exportOptions: {
//                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
//                            modifier: {
//                                selected: true
//                            }
//                        }
//                    },
//                    {
//                        extend: 'print',
//                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
//                        "title": 'Student Promote Report',
//                        "filename": 'student-promote-report',
//                        exportOptions: {
//                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
//                            modifier: {
//                                selected: true
//                            }
//                        }
//                    }
//                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: '{{ url('student_promote')}}',
                    data: function (f) {
                        f.class_id = $('#class_id').val();
                        f.session_id = $('#session_id').val();
                        f.section_id = $('#section_id').val();
                    }
                },
                columns: [
                    {data: 'student_id', name: 'student_id'},
                    {data: 'enrollment_number', name: 'enrollment_number'},
                    {data: 'class_name', name: 'class_name'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'gender_name', name: 'gender_name'},
                    {data: 'dob', name: 'dob'},
                    {data: 'admission_date', name: 'admission_date'},
                    {data: 'care_of_rel', name: 'care_of_rel'},
                    {data: 'care_of_name', name: 'care_of_name'},
                    {data: 'care_of_contact', name: 'care_of_contact'},
                    {data: 'caste_name', name: 'caste_name'},
                    {data: 'address_line1', name: 'address_line1'},
                ]
            });

            $("#promote_student").prop('disabled', true);
            table.on('select deselect', function (e, dt, type, indexes) {
                var arr_checked_student = checkedStudent();
                if (arr_checked_student.length > 0)
                {
                    $("#promote_student").prop('disabled', false);
                } else
                {
                    $("#promote_student").prop('disabled', true);
                }
            });
        }
        $("#promote-student-form").validate({

            /* @validation states + elements 
             -*/
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             */
            rules: {
                class_id: {
                    required: true,
                },
                section_id: {
                    required: function (e) {
                        if ($("#section_id option").length > 1)
                        {
                            return true;
                        } else
                        {
                            return false;
                        }
                    }
                },
                session_id: {
                    required: true
                },
                'promote_class_id': {
                    required: true,
                },
                'promote_section_id': {
                    required: function (e) {
                        if ($("#promote_section_id option").length > 1)
                        {
                            return true;
                        } else
                        {
                            return false;
                        }
                    }
                },
                'promote_session_id': {
                    required: true,
                    notEqualTo: "#session_id"
                }
            },
            messages: {
                promote_session_id: "Promote and current session can't be same. "
            }
            ,
            /* @validation error messages 
             */

            /* @validation highlighting + error placement  
             */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        function checkedStudent()
        {
            var arr_checked_student = [];
            $.each($('#student-promote-table').DataTable().rows('.selected').data(), function () {
                arr_checked_student.push(this["student_id"]);
            });
            return arr_checked_student;
        }
        $(document).on('change', '#class_id,#promote_class_id', function (e) {
            var type = '';
            if ($(this).attr('id') === 'class_id')
            {
                type = 'class';
            } else
            {
                type = 'promote_class';
            }
            getClassSection(type);

        });
        $(document).on('click', '#search_student', function (e) {
            if ($("#class_id").valid() && $("#session_id").valid())
            {
                studentDetail();
            }
        });
        $(document).on('click', '#promote_student', function (e) {
            promoteStudent();
        });
        function promoteStudent()
        {
            if ($("#promote-student-form").valid())
            {
                var student_data = $('#promote-student-form').serializeArray();
                student_data.push({name: 'student_id', value: checkedStudent()});
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('promote-student-save')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: student_data,
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        if (response.status == 'success')
                        {
                            $('#server-response-success').text(response.message);
                        } else
                        {
                            $('#server-response-message').text(response.message);
                        }
                        $('#alertmsg-success').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        studentDetail();
                    }
                });
            }

        }
        function getClassSection(type)
        {
            var class_id = '';
            if (type === 'class')
            {
                $('select[name="section_id"]').empty();
                $('select[name="section_id"]').append('<option value="">Current section </option>');
                class_id = $("#class_id").val();
            } else
            {
                $('select[name="promote_section_id"]').empty();
                $('select[name="promote_section_id"]').append('<option value="">Current section </option>');
                class_id = $("#promote_class_id").val();
            }
            var session_id = $("#session_id").val();
            if (class_id !== null && session_id !== null)
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                    },
                     beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            if (type === 'class')
                            {
                                $.each(resopose_data, function (key, value) {
                                    $('select[name="section_id"]').append('<option value="' + key + '">' + value + '</option>');
                                });
                            } else
                            {
                                $.each(resopose_data, function (key, value) {
                                    $('select[name="promote_section_id"]').append('<option value="' + key + '">' + value + '</option>');
                                });
                            }
                        } else
                        {
                            return false;
                        }
                    }
                });
            }

        }
    });

</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



