@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
</style>
@php 
$current_segment2 = Request::segment(3); 
$current_segment3 = Request::segment(4); 
$class_id = !empty($current_segment2) ? $current_segment2 : 1;
$section_id = !empty($current_segment3) ? $current_segment3 : 1;
@endphp
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span>Student Progress Report</span>
                </div>
            </div>
            <div class="panel" id="transportId">
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('class_id', $arr_class,$class_id, ['class' => 'form-control','id'=>'class_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('section_id',$arr_section,$section_id, ['class' => 'form-control','id'=>'section_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('month',$arr_month,date('m'), ['class' => 'form-control','id'=>'month'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
	    <div  id="progress-criteria" class="panel-title hidden-xs col-md-6 pull-right"  style="padding-top:1%;">
	        &nbsp; &nbsp; 
	        <strong>Progress Criteria </strong>=> &nbsp;&nbsp;&nbsp;
	        @foreach($arr_progress_setting as $key =>$psetting)
                    <strong><span>{!! $psetting !!}</span>&nbsp;: &nbsp;<span>{!! ucfirst($key) !!}</span></strong> &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;
	        @endforeach
                </div>
            </div>
	
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-progress-table" cellspacing="0" width="100%">
                    <thead>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        tableStructure();
        function tableStructure() {
            var progress_dynamic_col = [];
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{!! url('progress-table-header/" + class_id + "/" + section_id + "') !!}",
                datatType: 'json',
                type: 'GET',
                success: function (arr_col) {
                    jQuery.each(arr_col, function (key, val) {
                        progress_dynamic_col.push({data: '' + val['key'] + '', name: '' + val['key'] + '', title: val['val'], orderable: false, searchable: false});

                    });
                    studentProgress(progress_dynamic_col);
                }
            });
        }

        function studentProgress(progress_dynamic_col)
        {
            var class_name = $("#class_id option:selected").text();
            var month = $("#month option:selected").text();
            var year = '<?php echo date('Y') ?>';
            var message_top = 'Class : ' + class_name;
            if ($("#section_id").val() !== '')
            {
                var section_name = $("#section_id option:selected").text();
                message_top = message_top + ' /Section : ' + section_name;
            }
            message_top = message_top + ', ' + month + ', ' + year;
            message_top += $("#progress-criteria").text();
            var table = $('#student-progress-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
//                bFilter:false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        messageTop: message_top,
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'Student Progress Report',
                        "filename": 'student-progress-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'print',
                        messageTop: message_top,
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Student Progress Report',
                        "filename": 'student-progress-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9],
                            modifier: {
                                selected: true
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: "{{ url('student_progress_report')}}",
                    data: function (f) {
                        f.class_id = $('#class_id').val();
                        f.section_id = $("#section_id").val();
                        f.month = $("#month").val();
                    },
                    method: 'POST'

                },
                columns: progress_dynamic_col,
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
            table.on('select deselect', function (e, dt, type, indexes) {
                var arr_checked_student = checkedStudent();
                if (arr_checked_student.length > 0)
                {
                    $(".buttons-excel").prop('disabled', false);
                    $(".buttons-print").prop('disabled', false);
                } else
                {
                    $(".buttons-excel").prop('disabled', true);
                    $(".buttons-print").prop('disabled', true);
                }
            });
        }

        function checkedStudent()
        {
            var arr_checked_student = [];
            $.each($('#student-progress-table').DataTable().rows('.selected').data(), function () {
                arr_checked_student.push(this["student_id"]);
            });
            return arr_checked_student;
        }
        $(document).on('change', '#class_id,#section_id', function (e) {
            var class_id = $("#class_id").val();
            var section_id = $("#section_id").val();
            var url = '{!! url("admin/student-progress/' + class_id + '/' + section_id + '") !!}';
            window.location.replace(url);
        });
        $(document).on('change', '#month', function (e) {
            tableStructure();
        });
    });

</script>
</body>
</html>
@endsection



