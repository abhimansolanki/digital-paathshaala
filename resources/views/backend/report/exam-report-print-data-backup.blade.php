<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
</style>
<div class="row">
    @php $arr = range(0, 10, 10); @endphp
    @php 
    $school = get_school_data();
    $i = 1;
     $tot_student = count($arr_marks);
    @endphp
    @foreach($arr_marks as $key =>$marks)
    @php $marks = (array)$marks;
    $arr_subject_cre = json_decode($marks['subject_marks_criteria'],true);
    $arr_obt_marks = json_decode($marks['obtained_marks'],true);
    @endphp
    <div class="col-md-6" style="padding: 0% 0 5% 3%; float: left;">
        <div class="col-md-12" style="padding:10px;">
	<table cellpadding="5">
	    <thead>
	        <tr>
		<td rowspan="3" colspan="4"><img src="{{ url(get_school_logo())}}" style="height:30%; width: 60%"></td>
		<td colspan="3">{!! $school['address'] !!}</td>
	        </tr>
	        <tr>
		<td colspan="3">{!! $school['contact_number'] !!}</td>
	        </tr>
	        <tr>
		<td colspan="3">{!! $marks['exam_name'] !!}</td>
	        </tr>
	    </thead>
	    <tbody>
	        <tr>
		<td>Student Name</td>
		<td>{!! $marks['student_name'] !!}</td>
		<td>Roll No</td>
		<td colspan="4">{!! $marks['roll_number'] !!}</td>
	        </tr>
	        <tr>
		<td>DOB</td>
		<td>{!! $marks['dob'] !!}</td>
		<td>Class(Sec)</td>
		<td colspan="4">{!! $marks['class_name'] !!}</td>
	        </tr>
	        <tr>
		<td>Father's Name</td>
		<td>{!! $marks['father_name'] !!}</td>
		<td>Mother's Name</td>
		<td colspan="4">{!! $marks['mother_name'] !!}</td>
	        </tr>
	        <tr>
		<th rowspan="2">Subject</th>
		<th colspan="3">Obt. Marks </th>
		<th rowspan="2">Max Marks</th>
		<th rowspan="2">Passing Marks</th>
		<th rowspan="2">Tot. Marks</th>
	        </tr>
	        <tr>
		<!--subject and exam type are same thing-->
		@foreach($arr_subject_type as $exam_type)
		<th>{!! substr($exam_type, 0, 1) !!}</th>
		@endforeach
	        </tr>
	        @foreach($arr_subject_cre as $subject_id =>$subject_data)
	        @php $all_sub_tot = 0;
	        $sub_obt = 0;
	        $max_marks = 0;
	        $sub_min = 0;
	        @endphp
	        @if(isset($subject_data['total_max_marks']))

	        @php 
	        $max_marks = $subject_data['total_max_marks'];
	        $sub_obt = $arr_obt_marks[$subject_id]['sub_tot_obtained'];
	        unset($arr_obt_marks[$subject_id]['sub_tot_obtained']);
	        unset($subject_data[$subject_id]['total_max_marks']);
	        @endphp
	        @endif
	        <tr>
		<td>{!! $arr_subject[$subject_id] !!}</td>
		@foreach($arr_subject_type as $exam_type_id =>$exam_type_marks)
		<td>
		    @if(isset($arr_obt_marks[$subject_id][$exam_type_id]))
		    {!! $arr_obt_marks[$subject_id][$exam_type_id] !!}
		    @php 
		    $sub_type_min = isset($subject_data[$exam_type_id]['min_marks']) ? $subject_data[$exam_type_id]['min_marks'] :0;
		    $sub_min = $sub_min + $sub_type_min; 
		    @endphp
		    @endif
		</td>
		@endforeach
		<td>
		    {!! $max_marks !!}
		</td>

		<td>{!! $sub_min !!}</td>
		<td>{!! $sub_obt !!}</td>
	        </tr>
	        @endforeach
	        <tr>
		<td colspan="7">
		    <br>
		</td>
	        </tr>
	        <tr>
		<td>PERCENTAGE</td>
		<td>{!! $marks['percentage'] !!}</td>
		<td>RESULT / GRADE</td>
		<td>{!! $marks['grade'] !!}</td>
		<td>Division</td>
		<td>First</td>
		<td>RANK : {!! $marks['rank'] !!}</td>
	        </tr>
	        <tr>
		<td colspan="2" class='sig_row'>
		    <br>
		    <br>
		    Signature Class
		    <br>
		    Teacher
		</td>
		<td colspan="2" class='sig_row'>
		    <br>
		    <br>
		    Sign.Examination
		    <br>
		    In Charge
		</td>
		<td colspan="2" class='sig_row'>
		    <br>
		    <br>
		    Principle
		    <br>
		    Signature
		</td>
		<td class='sig_row'>
		    <br>
		    <br>
		    Parents
		    <br>
		    Signature
		</td>
	        </tr>
	    </tbody>
	</table>
        </div>
    </div>
     @if($i < $tot_student)
    <div class="pagebreak"></div>
    @endif
    @php  $i++; @endphp
    @endforeach
</div>
<style>
    @page { size: auto;  margin: 20mm; }
    .pagebreak { page-break-before: always; }
    .sig_row{
        border-right: 0px; text-align: center;
    }
</style>
<script type="text/javascript">
//window.print();
</script>