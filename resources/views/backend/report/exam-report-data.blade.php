@if(!empty($arr_subjet_type_data))
<table class="text-center" id="single-exam-report">
    <thead>
        <tr>
	<th rowspan="2">
	    {!! Form::checkbox('check_all','','', ['class' => 'check_all', 'id' => 'check_all']) !!}
	    <span class="checkbox radioBtnpan"></span><em></em>
	</th>
	<th rowspan="2">S.No</th>
	<th rowspan="2">S.Rs.</th>
	<th rowspan="2">Roll No.</th>
	<th rowspan="2">Stu. Name</th>
	@php  unset($arr_subjet_type_data['tot_obtained']);
	$tot_max = 0; 
	$sub_marks_criteria = $marks_criteria;
	@endphp
	@foreach($arr_subjet_type_data as $subject_id => $subject)
	@php $start = 0; unset($subject['sub_tot_obtained']);
	@endphp
	@php $tot_max = $tot_max + $marks_criteria[$subject_id]['total_max_marks']; unset($marks_criteria[$subject_id]['total_max_marks']); @endphp
	<th colspan="{{ count($subject)+1 }}" class="text-center">
	    {!! isset($arr_subject[$subject_id]) ? $arr_subject[$subject_id] :'' !!}
	</th>
	@endforeach
	<th rowspan="2" style="min-width:78px;" class="text-center">Tot. obt.<br>{!! ($tot_max) !!}</th>
	<th rowspan="2">%</th>
	<th rowspan="2">Grade</th>
	<th rowspan="2">Rank</th>
	<th rowspan="2">Remark</th>
        </tr>
        <tr>
	@foreach($arr_subjet_type_data as $subject_id => $subject)
	@php $start = 0; $sub_tot_max = 0; unset($subject['sub_tot_obtained']);@endphp
	@php  $sub_tot_max = $tot_max + isset($sub_marks_criteria[$subject_id]['total_max_marks']) ? $sub_marks_criteria[$subject_id]['total_max_marks'] : 0;
	unset($sub_marks_criteria[$subject_id]['total_max_marks']);
	@endphp
	@foreach($subject as $type_id => $sub)
	<th class="text-center">
	    ({!! isset($arr_subject_type[$type_id]) ? substr($arr_subject_type[$type_id], 0, 1):'' !!})
	    <br>
	    {!! ($sub_marks_criteria[$subject_id][$type_id]['max_marks']) !!}
	</th>
	@endforeach
	<th class="text-center" style="min-width:78px;">Tot. Obt.<br>{!! ($sub_tot_max) !!}</th>
	@endforeach	
        </tr>
    </thead> 
    <tbody>
        
	@foreach($arr_marks as $key =>$marks_data)
	@php $marks_data = (array) $marks_data; $marks_data['obtained_marks'] = json_decode($marks_data['obtained_marks'],true);
	$total_obtained = $marks_data['obtained_marks']['tot_obtained']; unset($marks_data['obtained_marks']['tot_obtained']);
	@endphp
	<tr>
	<td>
	    {!! Form::checkbox('arr_student_id[]',$marks_data['student_id'],null,['id'=>'checked_subject_id'.$subject_id,'class'=>'arr_checked_student_id','required'=>true]) !!}
	    <span class="checkbox radioBtnpan"></span><em></em>
	</td>
	<td>{!! $key+1 !!}</td>
	<td>{!! $marks_data['enrollment_number'] !!}</td>
	<td>{!! $marks_data['prefix'].$marks_data['roll_number'] !!}</td>
	<td>{!! $marks_data['first_name'].' '. $marks_data['middle_name'].' '.$marks_data['last_name'] !!}</td>
	@foreach($marks_data['obtained_marks'] as $subject_id => $subject)
	@php $sub_total_obtained = $subject['sub_tot_obtained']; unset($subject['sub_tot_obtained']);@endphp
	@foreach($subject as $type_id => $sub_marks)
	<td class="text-center">
	    {!! isset($sub_marks) ? $sub_marks :0 !!}
	</td>
	@endforeach
	<td class="text-center">{!! $sub_total_obtained !!}</td>
	@endforeach
	<td class="text-center">{!! $total_obtained !!}</td>
	<td class="text-center">{!! $marks_data['percentage'] !!}</td>
	<td class="text-center">{!! $marks_data['grade'] !!}</td>
	<td>{!! $marks_data['rank'] !!}</td>
	<td>
	    @php $grade_comment = ''; $per = round($marks_data['percentage']);
	    @endphp
	    @foreach($arr_grade as $grade) 
	    @if(in_array($per,explode(',',$grade['range'])))
	    @php $grade_comment = $grade['grade_comments'];
	    @endphp
	    @endif
	    @endforeach
	    {!! $grade_comment !!}
	</td>
	</tr>   
	@endforeach
        
    </tbody>
</table>
<table class="text-center" id="single-exam-report-print" style="display: none">
	<thead>
	<tr>
		<th rowspan="2">S.No</th>
		<th rowspan="2">S.Rs.</th>
		<th rowspan="2">Roll No.</th>
		<th rowspan="2">Stu. Name</th>
		@php  unset($arr_subjet_type_data['tot_obtained']);
	$tot_max = 0;
	$sub_marks_criteria = $marks_criteria_p;
		@endphp
		@foreach($arr_subjet_type_data as $subject_id => $subject)
			@php $start = 0; unset($subject['sub_tot_obtained']);
			@endphp
			@php $tot_max = $tot_max + $marks_criteria_p[$subject_id]['total_max_marks']; unset($marks_criteria_p[$subject_id]['total_max_marks']); @endphp
			<th colspan="{{ count($subject)+1 }}" class="text-center">
				{!! isset($arr_subject[$subject_id]) ? $arr_subject[$subject_id] :'' !!}
			</th>
		@endforeach
		<th rowspan="2" style="min-width:78px;" class="text-center">Tot. obt.<br>{!! ($tot_max) !!}</th>
		<th rowspan="2">%</th>
		<th rowspan="2">Grade</th>
		<th rowspan="2">Rank</th>
		<th rowspan="2">Remark</th>
	</tr>
	<tr>
		@foreach($arr_subjet_type_data as $subject_id => $subject)
			@php $start = 0; $sub_tot_max = 0; unset($subject['sub_tot_obtained']);@endphp
			@php  $sub_tot_max = $tot_max + isset($sub_marks_criteria[$subject_id]['total_max_marks']) ? $sub_marks_criteria[$subject_id]['total_max_marks'] : 0;
	unset($sub_marks_criteria[$subject_id]['total_max_marks']);
			@endphp
			@foreach($subject as $type_id => $sub)
				<th class="text-center">
					({!! isset($arr_subject_type[$type_id]) ? substr($arr_subject_type[$type_id], 0, 1):'' !!})
					<br>
					{!! ($sub_marks_criteria[$subject_id][$type_id]['max_marks']) !!}
				</th>
			@endforeach
			<th class="text-center" style="min-width:78px;">Tot. Obt.<br>{!! ($sub_tot_max) !!}</th>
		@endforeach
	</tr>
	</thead>
	<tbody>

	@foreach($arr_marks as $key =>$marks_data)
		@php $marks_data = (array) $marks_data; $marks_data['obtained_marks'] = json_decode($marks_data['obtained_marks'],true);
	$total_obtained = $marks_data['obtained_marks']['tot_obtained']; unset($marks_data['obtained_marks']['tot_obtained']);
		@endphp
		<tr>
			<td>{!! $key+1 !!}</td>
			<td>{!! $marks_data['enrollment_number'] !!}</td>
			<td>{!! $marks_data['prefix'].$marks_data['roll_number'] !!}</td>
			<td>{!! $marks_data['first_name'].' '. $marks_data['middle_name'].' '.$marks_data['last_name'] !!}</td>
			@foreach($marks_data['obtained_marks'] as $subject_id => $subject)
				@php $sub_total_obtained = $subject['sub_tot_obtained']; unset($subject['sub_tot_obtained']);@endphp
				@foreach($subject as $type_id => $sub_marks)
					<td class="text-center">
						{!! isset($sub_marks) ? $sub_marks :0 !!}
					</td>
				@endforeach
				<td class="text-center">{!! $sub_total_obtained !!}</td>
			@endforeach
			<td class="text-center">{!! $total_obtained !!}</td>
			<td class="text-center">{!! $marks_data['percentage'] !!}</td>
			<td class="text-center">{!! $marks_data['grade'] !!}</td>
			<td>{!! $marks_data['rank'] !!}</td>
			<td>
				@php $grade_comment = ''; $per = round($marks_data['percentage']);
				@endphp
				@foreach($arr_grade as $grade)
					@if(in_array($per,explode(',',$grade['range'])))
						@php $grade_comment = $grade['grade_comments'];
						@endphp
					@endif
				@endforeach
				{!! $grade_comment !!}
			</td>
		</tr>
	@endforeach

	</tbody>
</table>
@endif