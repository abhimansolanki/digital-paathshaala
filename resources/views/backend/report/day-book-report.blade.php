@extends('admin_panel/layout')
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
</style>
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span>Day Book Report</span>
                </div>
            </div>
            <div class="panel" id="transportId">
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!! Form::text('from_date',date('d/m/Y'),['class' => 'form-control date_picker','id' => 'from_date', 'readonly' => 'readonly','placeholder'=>'From']) !!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!! Form::text('to_date',date('d/m/Y'),['class' => 'form-control date_picker','id' => 'to_date', 'readonly' => 'readonly','placeholder'=>'To']) !!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body pn" style="overflow:auto;">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-detail-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Sr. No</th>
                            <th>{{trans('language.receipt_number')}}</th>
                            <th>{{trans('language.receipt_date')}}</th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>Fee Type</th>
                            <th>Student Name</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>{{trans('language.father_contact_number')}}</th>
                            <th>{{trans('language.class_sec')}}</th>
                            <th>{{trans('language.payment_mode')}}</th>
                            <th>{{trans('language.amount')}}</th>
                        </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        dayBookReport();
        function dayBookReport()
        {
            var table = $('#student-detail-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                dom: 'Blfrtip',
                paging: false,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'Day Book Report',
                        "filename": 'daybook-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                            modifier: {
                                selected: true
                            }
                        },
                        footer: true,
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Day Book Report',
                        "filename": 'daybook-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                            modifier: {
                                selected: true
                            }
                        },
                        footer: true,
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: "{{ url('daybook-report-data')}}",
                    data: function (f) {
                        f.from_date = $('#from_date').val();
                        f.to_date = $('#to_date').val();
                    }
                },
                columns: [
                    {data: 'student_id', name: 'student_id'},
                    {data: 'sn', name: 'sn'},
                    {data: 'receipt_number', name: 'receipt_number'},
                    {data: 'receipt_date', name: 'receipt_date'},
                    {data: 'enrollment_number', name: 'enrollment_number'},
                    {data: 'type', name: 'type'},
                    {data: 'student_name', name: 'type'},
                    {data: 'father_name', name: 'father_name'},
                    {data: 'father_contact_number', name: 'father_contact_number'},
                    {data: 'class_name', name: 'class_name'},
                    {data: 'payment_mode', name: 'payment_mode'},
                    {data: 'amount', name: 'amount'},
                ],
                drawCallback: function (row, data, start, end, display) {
                    var api = this.api();
                    $(api.column(0).footer()).html('');
                    $(api.column(1).footer()).html('');
                    $(api.column(2).footer()).html('');
                    $(api.column(3).footer()).html('');
                    $(api.column(4).footer()).html('');
                    $(api.column(5).footer()).html('');
                    $(api.column(6).footer()).html('');
                    $(api.column(7).footer()).html('');
                    $(api.column(8).footer()).html('');
                    $(api.column(9).footer()).html('');
                    $(api.column(10).footer()).html('');

                    // Total over this page
                    total_amount = api
                        .column(11, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return parseInt(a) + parseInt(b);
                        }, 0);
                    // Update foote
                    $(api.column(11).footer()).html(total_amount);
                },
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
            table.on('select deselect', function (e, dt, type, indexes) {
                var arr_checked_student = checkedStudent();
                if (arr_checked_student.length > 0)
                {
                    $(".buttons-excel").prop('disabled', false);
                    $(".buttons-print").prop('disabled', false);
                } else
                {
                    $(".buttons-excel").prop('disabled', true);
                    $(".buttons-print").prop('disabled', true);
                }
            });
        }

        function checkedStudent()
        {
            var arr_checked_student = [];
            $.each($('#student-detail-table').DataTable().rows('.selected').data(), function () {
                arr_checked_student.push(this["student_id"]);
            });
            return arr_checked_student;
        }
        $(document).on('change', '#from_date,#to_date', function (e) {
            dayBookReport();
        });

    });
</script>
</body>
</html>
@endsection
