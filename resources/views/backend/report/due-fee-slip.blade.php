<div class="row">
    @php $arr = range(0, 100, 10); @endphp
    @foreach($arr as $key =>$value)
        <div class="col-md-6" style="padding: 5% 0 5% 3%; float: left;">
         <div class="col-md-12" style="border: dashed 2px; padding:10px;">
	<h5 style="text-align: center"><u>Due Fee Reminder</u></h5>
	<table>
	    <tr>
	        <td>{!! trans('language.students_name')!!}</td>
	        <td>:</td>
	        <td>Amba</td>
	    </tr>
	    <tr>
	        <td>{!! trans('language.fathers_name')!!}</td>
	        <td>:</td>
	        <td>Amba</td>
	    </tr>
	    <tr>
	        <td>{!! trans('language.class_sec')!!}</td>
	        <td>:</td>
	        <td>Amba</td>
	    </tr>
	    <tr>
	        <td>{!! trans('language.due_fee_upto')!!}</td>
	        <td>:</td>
	        <td>Amba</td>
	    </tr>
	    <tr>
	        <td colspan="3">{!! trans('language.fee_slip_message')!!}</td>
	    </tr>
	</table>
        </div>
        </div>
    @endforeach
</div>
<style>
    @page { size: auto;  margin: 0mm; }
    .pagebreak { page-break-before: always; }
</style>
<!--<script type="text/javascript">
window.print();
</script>-->