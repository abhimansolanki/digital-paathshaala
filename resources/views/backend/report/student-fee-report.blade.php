@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
    <style>
        button[disabled] {
            background: #76aaef !important;
        }
    </style>
    <div class="tray tray-center tableCenter">
        @include('backend.partials.loader')
        <div class="">
            <div class="panel panel-visible" id="spy2">
                <div class="panel-heading">
                    <div class="panel-title hidden-xs col-md-6">
                        <span class="glyphicon glyphicon-tasks"></span> <span>Student Fee Report Detail</span>
                    </div>
                </div>
                <div class="panel" id="transportId">
                    <div class="panel-body">
                        <div class="tab-content  br-n">
                            <div id="tab1_1" class="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('class_id', $arr_class,'', ['class' => 'form-control','id'=>'class_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('student_id', $arr_student,'', ['class' => 'form-control','id'=>'student_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('payment_mode_id',$arr_payment_mode,'', ['class' => 'form-control','id'=>'payment_mode_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::text('receipt_number','', ['class' => 'form-control','id'=>'receipt_number','placeholder'=>'Receipt Number'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!! Form::text('from_date','',['class' => 'form-control date_picker','id' => 'from_date', 'readonly' => 'readonly','placeholder'=>'From']) !!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!! Form::text('to_date','',['class' => 'form-control date_picker','id' => 'to_date', 'readonly' => 'readonly','placeholder'=>'To']) !!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body pn">
                    @include('backend.partials.messages')
                    <table class="table table-bordered table-striped table-hover" id="student-detail-table"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th></th>
                            <th>S.No</th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>{{trans('language.receipt_number')}}</th>
                            <th>{{trans('language.receipt_date')}}</th>
                            <th>{{trans('language.class_sec')}}</th>
                            <th>Stu. Name</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>Mobile No</th>
                            <th>{{trans('language.payment_mode')}}</th>
                            <th>{{trans('language.total_amount')}}</th>
                            <th>{{trans('language.fine_amount')}}</th>
                            <th>{{trans('language.cheque_bounce_amount')}}</th>
                            <th>{{trans('language.discount_amount')}}</th>
                            <th>{{trans('language.net_amount')}}</th>
                            <th>{{trans('language.print_receipt')}}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            // $('#student-detail-table tbody').on( 'click', '#print_receipt', function () {
            //     var data = table.row( $(this).parents('tr') ).data();
            //     alert( data[0] +"'s salary is: "+ data[ 5 ] );
            // } );



            studentFeeReport();
            function studentFeeReport() {
                var table = $('#student-detail-table').DataTable({
                    destroy: true,
                    processing: true,
                    serverSide: true,
//                bFilter:false,
                    scrollX: true,
                    scroller: true,
                    dom: 'Blfrtip',
                    paging: false,
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                            "title": 'Student Fee Report',
                            "filename": 'student-fee-report',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
                                modifier: {
                                    selected: true
                                }
                            },
                            footer: true,
                        },
                        {
                            extend: 'print',
                            "text": '<span class="fa fa-print"></span> &nbsp; Print',
                            "title": 'Student Fee Report',
                            "filename": 'student-fee-report',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
                                modifier: {
                                    selected: true
                                }
                            },
                            footer: true,
                        }
                    ],
                    select: {
                        style: 'multi',
                        selector: 'td:first-child'
                    },
                    'columnDefs': [
                        {
                            'targets': 0,
                            'className': 'select-checkbox',
                            'checkboxes': {
                                'selectRow': true
                            }
                        },
                        {
                            'targets': 15,
                            "data": null,
                            "render": function(data,type,row,meta)
                            {
                                return "<button id='print_receipt' data-id='" + data.student_id + "' data-receiptId='" + data.receipt_id + "'>Print Receipt</button>";
                            }
                         }
                    ],
                    ajax: {
                        url: "{{ url('student_fee_report_data')}}",
                        data: function (f) {
                            f.class_id = $('#class_id').val();
                            f.student_id = $('#student_id').val();
                            f.receipt_number = $('#receipt_number').val();
                            f.from_date = $('#from_date').val();
                            f.to_date = $('#to_date').val();
                            f.payment_mode_id = $('#payment_mode_id').val();
                        }
                    },
                    columns: [
                        {data: 'student_id', name: 'student_id'},
                        {data: 's_no', name: 's_no'},
                        {data: 'enrollment_number', name: 'enrollment_number'},
                        {data: 'receipt_number', name: 'receipt_number'},
                        {data: 'receipt_date', name: 'receipt_date'},
                        {data: 'class_name', name: 'class_name'},
                        {data: 'student_name', name: 'student_name'},
                        {data: 'father_name', name: 'father_name'},
                        {data: 'father_contact_number', name: 'father_contact_number'},
                        {data: 'payment_mode', name: 'payment_mode'},
                        {data: 'total_amount', name: 'total_amount'},
                        {data: 'fine_amount', name: 'fine_amount'},
                        {data: 'cheque_bounce_charges_received', name: 'cheque_bounce_charges_received'},
                        {data: 'discount_amount', name: 'discount_amount'},
                        {data: 'net_amount', name: 'net_amount'},
                    ],
                    drawCallback: function (row, data, start, end, display) {
                        var api = this.api();

                        $(api.column(0).footer()).html('');
                        $(api.column(1).footer()).html('');
                        $(api.column(2).footer()).html('');
                        $(api.column(3).footer()).html('');
                        $(api.column(4).footer()).html('');
                        $(api.column(5).footer()).html('');
                        $(api.column(6).footer()).html('');
                        $(api.column(7).footer()).html('');
                        $(api.column(8).footer()).html('');
                        $(api.column(9).footer()).html('');

                        total = api
                            .column(10)
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);

                        total_fine = api
                            .column(11)
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);

                        cheque_bounce_charges_received = api
                            .column(12)
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);

                        total_discount = api
                            .column(13)
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);

                        // Total over this page
                        net_amount = api
                            .column(14, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);
                        // Update footer
                        $(api.column(10).footer()).html(total);
                        $(api.column(11).footer()).html(total_fine);
                        $(api.column(12).footer()).html(cheque_bounce_charges_received);
                        $(api.column(13).footer()).html(total_discount);
                        $(api.column(14).footer()).html(net_amount);
                        $(api.column(15).footer()).html('');

                    },
                });

                $("#student-detail-table tbody tr").on('click',function(event) {
                    $("#student-detail-table tbody tr").removeClass('row_selected');
                    $(this).addClass('row_selected');
                });

                $(".buttons-excel,.buttons-print").css({
                    'margin-left': '7px',
                    'background-color': '#2e76d6',
                    'color': 'white',
                    'border': '1px solid #eeeeee',
                    'float': 'right',
                    'padding': '5px'
                });

                $(".buttons-excel").prop('disabled', true);
                $(".buttons-print").prop('disabled', true);
                table.on('select deselect', function (e, dt, type, indexes) {
                    var arr_checked_student = checkedStudent();
                    if (arr_checked_student.length > 0) {
                        $(".buttons-excel").prop('disabled', false);
                        $(".buttons-print").prop('disabled', false);
                    } else {
                        $(".buttons-excel").prop('disabled', true);
                        $(".buttons-print").prop('disabled', true);
                    }
                });
            }

            function checkedStudent() {
                var arr_checked_student = [];
                $.each($('#student-detail-table').DataTable().rows('.selected').data(), function () {
                    arr_checked_student.push(this["student_id"]);
                });
                return arr_checked_student;
            }

            $(document).on('change', '#class_id,#student_id,#receipt_number,#payment_mode_id,#from_date,#to_date', function (e) {
                studentFeeReport();
            });

        });

        $(document).on('click', '#print_receipt', function () {
            var student_id = $(this).data('id');
            var receipt_id = $(this).data('receiptid');
            if (student_id != '' && receipt_id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('student-fee-receipt-print')}}",
                    datatType: 'json',
                    type: 'GET',
                    data: {
                        'student_id': student_id,
                        'receipt_id': receipt_id,
                    },
                    beforeSend: function () {
                        $('#LoadingImage').show();
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status == 'success') {
                            var time = new Date().getTime();
                            newWin = window.open("");
                            newWin.document.write('<html><head>');
                            newWin.document.title = "Student-fees-" + time;
                            newWin.document.write('<head><style type="text/css">h1,h2,h3,h4,h5,h6{padding: 0px;margin: 0px;}p{padding: 0px;margin: 0px;}.tabletr tr td{line-height: 27px;}.border-left td{border-left: 0px !important; }.pagebreak { page-break-before: always; }</style></head></head><body>');
                            newWin.document.write(resopose_data);
                            newWin.document.write('</body></html>');
                            setTimeout(function () {
                                newWin.print();
                                newWin.close();
                            }, 100);
                        }
                        $('#LoadingImage').hide();
                    }
                });
            }
        });

        $(document).on('change', '#class_id', function () {
            $('select[name="student_id"]').empty();
            $('select[name="student_id"]').append('<option value="">--Select Student--</option>');
            var class_id = $("#class_id").val();
            if (class_id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-fee-student')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                    },
                    beforeSend: function () {
                        $('#LoadingImage').show();
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status == 'success') {
                            $.each(resopose_data, function (key, value) {
                                $('select[name="student_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                        $('#LoadingImage').hide();
                    }
                });
            }
        });

    </script>
    </body>
    </html>
@endsection
@push('scripts')
@endpush



