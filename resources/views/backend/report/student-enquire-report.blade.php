@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
    #transportId button{
        background: #2e76d6 !important;
        border-radius: 0px !important; 
        border: 0px;
        outline: none;
        box-shadow: 0px;
        color: #fff;
        padding: 7px 10px;
        font-weight: bold;
        font-size: 11px;
        margin-top: 1%;
        position: absolute;
        right: 19px;
        top: 37px;
        z-index: 9999999;
        font-weight: normal;
    }
    .buttons-html5{
        margin-right: 81px !important; 
    }
</style>
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span>Student's Enquire Report</span>
                </div>
            </div>
            <div class="panel" id="transportId">
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('session_id',add_blank_option(get_session('yes'),'-- Select Session --'),null, ['class' => 'form-control','id'=>'session_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('class_id', $arr_class,'', ['class' => 'form-control','id'=>'class_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('enquire_type',$arr_enquire_type,'', ['class' => 'form-control','id'=>'enquire_type'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-info" id='sms-button' style="margin-top: 14px;">Send SMS</button>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-enquire-report" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>S.No.</th>
                            <th>Enq. No</th>
                            <th>{{trans('language.enquire_date')}}</th>
                            <th>Stu. Name</th>
                            <th>{{trans('language.gender')}}</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>{{trans('language.mother_name')}}</th>
                            <th>{{trans('language.class_name')}}</th>
                            <th>{{trans('language.dob')}}</th>
                            <th>{{trans('language.father_contact_number')}}</th>
                            <th>{{trans('language.address')}}</th>

                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@include('backend.partials.send-sms')
<script>
    $(document).ready(function () {
        studentEnquireReport();
        function studentEnquireReport()
        {
            var table = $('#student-enquire-report').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
//                bFilter:false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'Student Enquiry Report',
                        "filename": 'student-enquire-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Student Enquiry Report',
                        "filename": 'student-enquire-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                            modifier: {
                                selected: true
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: "{{ url('student_enquire_report_data')}}",
                    data: function (f) {
                        f.class_id = $('#class_id').val();
                        f.enquire_type = $('#enquire_type').val();
                        f.session_id = $("#session_id option:selected").val();
                    }
                },
                columns: [
                    {data: 'student_enquire_id', name: 'student_enquire_id'},
                    {data: 's_no', name: 's_no'},
                    {data: 'form_number', name: 'form_number'},
                    {data: 'enquire_date', name: 'enquire_date'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'gender_name', name: 'gender_name'},
                    {data: 'father_name', name: 'father_name'},
                    {data: 'mother_name', name: 'mother_name'},
                    {data: 'class_name', name: 'class_name'},
                    {data: 'dob', name: 'dob'},
                    {data: 'father_contact_number', name: 'father_contact_number'},
                    {data: 'address', name: 'address'},
                ]
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
            $("#sms-button").prop('disabled', true);
            table.on('select deselect', function (e, dt, type, indexes) {
                var arr_checked_student = checkedStudent();
                if (arr_checked_student.length > 0)
                {
                    $(".buttons-excel").prop('disabled', false);
                    $(".buttons-print").prop('disabled', false);

                    if ($("#enquire_type").val() == 2)
                    {
                        $("#sms-button").prop('disabled', false);
                    }
                } else
                {
                    $(".buttons-excel").prop('disabled', true);
                    $(".buttons-print").prop('disabled', true);

                    $("#sms-button").prop('disabled', true);

                }
            });
        }

        function checkedStudent()
        {
            var arr_checked_student = [];
            $.each($('#student-enquire-report').DataTable().rows('.selected').data(), function () {
                arr_checked_student.push(this["student_enquire_id"]);
            });
            return arr_checked_student;
        }
        $(document).on('change', '#class_id,#session_id,#enquire_type', function (e) {
            studentEnquireReport();
        });

        $('#send_sms').on('click', function (e) {
            var arr_checked_student = checkedStudent();
            if (arr_checked_student.length > 0 && $("#newModalFormId").valid())
            {
                var message = $.trim($("#sms_message").val());
                if (message != '')
                {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url: "{{url('send-unregistered-enquire-sms')}}",
                        datatType: 'json',
                        type: 'POST',
                        data: {
                            arr_student: arr_checked_student,
                            enquire_sms_message: message,
                        },
                        beforeSend: function () {
                            $("#LoadingImage").show();
                        },
                        success: function (response) {
                            $("#LoadingImage").hide();
                            if (response.status == 'success')
                            {
                                $("#send_sms").prop('disabled', true);
                                $("#sms_message").val('');
                                $("#success-message").text('Messages  were added in queue and will be sending soon...');
                            } else
                            {
                                alert('something went wrong');
                            }
                        }
                    });
                } else
                {
                    alert('Please enter sms-text');
                }

            }
        });
    });

</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



