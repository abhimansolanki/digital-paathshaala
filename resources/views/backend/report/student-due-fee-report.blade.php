@extends('admin_panel/layout')
@section('content')
    <style>
        button[disabled] {
            background: #76aaef !important;
        }

        .buttons-html5 {
            margin-right: 150px !important;
        }
    </style>
    <div class="tray tray-center tableCenter">
        @include('backend.partials.loader')
        <div class="">
            <div class="panel panel-visible" id="spy2">
                <div class="panel-heading">
                    <div class="panel-title hidden-xs col-md-6">
                        <span class="glyphicon glyphicon-tasks"></span> <span>View Fee</span>
                    </div>
                </div>
                <div class="panel" id="transportId">
                    @include('backend.partials.messages')
                    <div class="panel-body">
                        <div class="tab-content  br-n">
                            <div id="tab1_1" class="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('class_id', $arr_class,'', ['class' => 'form-control','id'=>'class_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('fee_circular_id',$arr_fee_circular,'', ['class' => 'form-control','id'=>'fee_circular_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('sub_fee_circular_id',$arr_sub_fee_circular,'', ['class' => 'form-control','id'=>'sub_fee_circular_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('student_id', $arr_student,'', ['class' => 'form-control','id'=>'student_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('fee_type_id',$arr_fee_type,'', ['class' => 'form-control','id'=>'fee_type_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body pn">
                    <div class="row">
                        <div class="col-md-12" style="margin-left: 17px;">
                            <button type="button" class="btn btn-info btn-sm app_accessbility" status-val="1"
                                    style="margin-top: 14px;"
                            >Active
                            </button>
                            <button type="button" class="btn btn-info btn-sm app_accessbility" status-val="2"
                                    style="margin-top: 14px;">Inactive
                            </button>
                            <button type="button" class="btn btn-info btn-sm" id='notification-button'
                                    style="margin-top: 14px;">Send Notification
                            </button>
                            <button type="button" class="btn btn-info btn-sm" id='reminder-notification-button'
                                    style="margin-top: 14px;">Send Reminder Notification
                            </button>
                            <button type="button" class="btn btn-info btn-sm" id='sms-button'
                                    style="margin-top: 14px;">Send SMS
                            </button>
                            <button type="button" class="btn btn-info btn-sm" id='print-due-slip'
                                    style="margin-top: 14px;">Print Slip
                            </button>
                        </div>
                    </div>
                    {{--                    </div>--}}
                    {{--                    <div class="col-md-8 text-right">--}}
                    {{--                        <!--//data-target="#SendSMS"-->--}}
                    {{--                    </div>--}}
                    @include('backend.partials.messages')
                    <table class="table table-bordered table-striped table-hover" id="student-detail-table"
                           cellspacing="0" width="100%" style="overflow-y:auto;">
                        <thead>
                        <tr>
                            <th></th>
                            <th>S.No</th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>{{trans('language.class_sec')}}</th>
                            <th>Stu. Name</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>{{trans('language.father_contact')}}</th>
                            <th>{{trans('language.mother_contact')}}</th>
                            <th>{{trans('language.fee-amount')}}</th>
                            <th>{{trans('language.fine_amount')}}</th>
                            <th>{{trans('language.cheque_bounce_amount')}}</th>
                            <th>{{trans('language.previous_due_fee')}}</th>
                            <th>{{trans('language.total_fee')}}</th>
                            <th>{{trans('language.deposit_fee')}}</th>
                            <th>{{trans('language.due_fee')}}</th>
                            <th>{{trans('language.app_accessbility')}}</th>
                            <th>{{trans('language.fee_due_date')}}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            studentFeeReport();

            function studentFeeReport() {
                var table = $('#student-detail-table').DataTable({
                    destroy: true,
                    processing: true,
                    serverSide: true,
                    dom: 'Blfrtip',
                    // scrollY: 300,
                    scrollX: true,
                    scroller: true,
                    paging: false,
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                            "title": 'Student Fee Report',
                            "filename": 'student-fee-report',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                                modifier: {
                                    selected: true
                                }
                            },
                            footer: true,
                        },
                        {
                            extend: 'print',
                            "text": '<span class="fa fa-print"></span> &nbsp; Print',
                            "title": 'Student Fee Report',
                            "filename": 'student-fee-report',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                                modifier: {
                                    selected: true
                                }
                            },
                            footer: true,
                        }
                    ],
                    select: {
                        style: 'multi',
                        selector: 'td:first-child'
                    },
                    'columnDefs': [
                        {
                            'targets': 0,
                            'className': 'select-checkbox',
                            'checkboxes': {
                                'selectRow': true
                            }
                        },
                        {
                            'targets': 15,
                            'className': '',
                            'render': function (data, type, full, meta) {
                                if (data == 1) {
                                    return '<span class="label label-success">Active</span>';
                                } else if (data == 2) {
                                    return '<span class="label label-danger">Inactive</span>';
                                }
                            }
                        }
                    ],
                    // 'columnDefs': [],
                    ajax: {
                        url: "{{ url('student_due_fee_data')}}",
                        data: function (f) {
                            f.class_id = $('#class_id').val();
                            f.student_id = $('#student_id').val();
                            f.fee_type_id = $('#fee_type_id option:selected').val();
                            f.fee_circular_id = $('#fee_circular_id').val();
                            f.sub_fee_circular_id = $('#sub_fee_circular_id').val();
                        }
                    },
                    columns: [
                        {data: 'student_id', name: 'student_id'},
                        {data: 'DT_Row_Index', name: 'DT_Row_Index'},
                        {data: 'enrollment_number', name: 'enrollment_number'},
                        {data: 'class_name', name: 'class_name'},
                        {data: 'student_name', name: 'student_name'},
                        {data: 'father_name', name: 'father_name'},
                        {data: 'father_contact_number', name: 'father_contact_number'},
                        {data: 'mother_contact_number', name: 'mother_contact_number'},
                        {data: 'academy_fee', name: 'academy_fee'},
                        {data: 'fine_amount', name: 'fine_amount'},
                        {data: 'chq_bounce_amount', name: 'chq_bounce_amount'},
                        {data: 'previous_due_fee', name: 'previous_due_fee'},
                        {data: 'total_fee', name: 'total_fee'},
                        {data: 'deposite_fee', name: 'deposite_fee'},
                        {data: 'total_due_fee', name: 'total_due_fee'},
                        {data: 'app_accessbility_status', name: 'app_accessbility_status'},
                        {data: 'fee_due_date', name: 'fee_due_date'}
                    ],
                    drawCallback: function (row, data, start, end, display) {
                        var api = this.api();

                        $(api.column(0).footer()).html('');
                        $(api.column(1).footer()).html('');
                        $(api.column(2).footer()).html('');
                        $(api.column(3).footer()).html('');
                        $(api.column(4).footer()).html('');
                        $(api.column(5).footer()).html('');
                        $(api.column(6).footer()).html('');
                        $(api.column(7).footer()).html('');

                        // Total over this page
                        total_academy = api
                            .column(8, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);
                        total_fine = api
                            .column(9, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);
                        total_chq = api
                            .column(10, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);
                        total_previous = api
                            .column(11, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);
                        total_amount = api
                            .column(12, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);
                        deposite_amount = api
                            .column(13, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);
                        total_due_amount = api
                            .column(14, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);

                        // Update footer
                        $(api.column(8).footer()).html(total_academy);
                        $(api.column(9).footer()).html(total_fine);
                        $(api.column(10).footer()).html(total_chq);
                        $(api.column(11).footer()).html(total_previous);
                        $(api.column(12).footer()).html(total_amount);
                        $(api.column(13).footer()).html(deposite_amount);
                        $(api.column(14).footer()).html(total_due_amount);
                        $(api.column(15).footer()).html('');
                    },
                });

                $(".buttons-excel,.buttons-print").css({
                    'margin-left': '7px',
                    'background-color': '#2e76d6',
                    'color': 'white',
                    'border': '1px solid #eeeeee',
                    'float': 'right',
                    'padding': '5px'
                });

                $(".buttons-excel").prop('disabled', true);
                $(".buttons-print").prop('disabled', true);
                $("#sms-button").prop('disabled', true);
                $("#notification-button").prop('disabled', true);
                $("#reminder-notification-button").prop('disabled', true);
                $(".app_accessbility").prop('disabled', true);
                $("#print-due-slip").prop('disabled', true);
                table.on('select deselect', function (e, dt, type, indexes) {
                    var arr_checked_student = checkedStudent();
                    if (arr_checked_student.length > 0) {
                        $(".buttons-excel").prop('disabled', false);
                        $(".buttons-print").prop('disabled', false);
                        $("#sms-button").prop('disabled', false);
                        $("#notification-button").prop('disabled', false);
                        $("#reminder-notification-button").prop('disabled', false);
                        $(".app_accessbility").prop('disabled', false);
                        $("#print-due-slip").prop('disabled', false);
                    } else {
                        $(".buttons-excel").prop('disabled', true);
                        $(".buttons-print").prop('disabled', true);
                        $("#sms-button").prop('disabled', true);
                        $("#notification-button").prop('disabled', true);
                        $("#reminder-notification-button").prop('disabled', true);
                        $(".app_accessbility").prop('disabled', true);
                        $("#print-due-slip").prop('disabled', true);
                    }
                });
            }

            function checkedStudent() {
                var arr_checked_student = [];
                console.log($('#student-detail-table').DataTable().rows('.selected').data());
                $.each($('#student-detail-table').DataTable().rows('.selected').data(), function () {
                    arr_checked_student.push(this["student_id"]);
                });
                return arr_checked_student;
            }

            $(document).on('change', '#class_id,#student_id,#fee_type_id,#fee_circular_id,#sub_fee_circular_id', function (e) {
                studentFeeReport();
            });

//send sms
            function checkedUser() {
                var table = $('#student-detail-table').DataTable();
                var arr_checked_student = [];
                $.each(table.rows('.selected').data(), function () {
                    arr_checked_student.push(this);
                });
                return arr_checked_student;
            }

            $('#sms-button').on('click', function (e) {
                var arr_checked_student = checkedUser();
                if (arr_checked_student.length > 0) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url: "{{url('send-due-fee-sms')}}",
                        datatType: 'json',
                        type: 'POST',
                        data: {
                            arr_fee_data: arr_checked_student,

                        },
                        beforeSend: function () {
                            $("#LoadingImage").show();
                        },
                        success: function (response) {
                            $("#LoadingImage").hide();
                            if (response.status == 'success') {
                                $("#send_sms").prop('disabled', true);
                                $("#sms_message").val('');
                                $("#success-message").text('Messages  were added in queue and will be sending soon...');
                            } else {
                                alert('something went wrong');
                            }
                        }
                    });
                }
            });

            $('#notification-button').on('click', function (e) {
                var arr_checked_student = checkedUser();
                if (arr_checked_student.length > 0) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url: "{{url('send-due-fee-notification')}}",
                        datatType: 'json',
                        type: 'POST',
                        data: {
                            arr_fee_data: arr_checked_student,

                        },
                        beforeSend: function () {
                            $("#LoadingImage").show();
                        },
                        success: function (response) {
                            $("#LoadingImage").hide();
                            if (response.status == 'success') {
                                $("#send_sms").prop('disabled', true);
                                $("#sms_message").val('');
                                alert('Notification were added in queue and will be sending soon...');
                                $("#success-message").text('Notification were added in queue and will be sending soon...');
                            } else {
                                alert('something went wrong');
                            }
                        }
                    });
                }
            });

            $('#reminder-notification-button').on('click', function (e) {
                var arr_checked_student = checkedUser();
                if (arr_checked_student.length > 0) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url: "{{url('send-due-fee-notification')}}",
                        datatType: 'json',
                        type: 'POST',
                        data: {
                            arr_fee_data: arr_checked_student,
                            reminder_button: true
                        },
                        beforeSend: function () {
                            $("#LoadingImage").show();
                        },
                        success: function (response) {
                            $("#LoadingImage").hide();
                            if (response.status == 'success') {
                                $("#send_sms").prop('disabled', true);
                                $("#sms_message").val('');
                                alert('Notification were added in queue and will be sending soon...');
                                $("#success-message").text('Notification were added in queue and will be sending soon...');
                            } else {
                                alert('something went wrong');
                            }
                        }
                    });
                }
            });

            $('.app_accessbility').on('click', function (e) {
                var arr_checked_student = checkedUser();
                var status = $(this).attr("status-val");
                if (arr_checked_student.length > 0) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url: "{{url('student-set-accessbility-status')}}",
                        datatType: 'json',
                        type: 'POST',
                        data: {
                            arr_student_data: arr_checked_student,
                            status: status,
                        },
                        beforeSend: function () {
                            $("#LoadingImage").show();
                        },
                        success: function (response) {
                            $("#LoadingImage").hide();
                            if (response.result == 'success') {
                                $(".app_accessbility").prop('disabled', true);
                                $("#success-message").text('Successfully Changed');
                            } else {
                                alert('something went wrong');
                            }
                            studentFeeReport();
                        }
                    });
                }
            });

            // Print Due fee slip
            $('#print-due-slip').on('click', function (e) {
                var arr_checked_student = checkedUser();
                if (arr_checked_student.length > 0) {
                    var no_records = 0;
                    var arr_slip = '<div class="row">';
                    var upto_sub_circular = "";
                    var due_date = "{{date("d M y")}}";
                    if($("#sub_fee_circular_id").val()){
                        upto_sub_circular = "(up to " + $("#sub_fee_circular_id option:selected").text() + ")";
                    }
                    $.each(arr_checked_student, function (key, value) {
                        if(value.total_due_fee > 0){
                            var message = '{!! trans("language.fee_slip_message")!!}';
                            if(value.fee_due_date == ""){
                                message = "";
                            }
                            arr_slip = arr_slip + '<div class="col-md-5" style="width:45%; padding: 3% 0 3% 3%; float: left;">' +
                                '<div class="col-md-12" style="border: dashed 2px; padding:10px;">' +
                                '<h5 style="text-align: center; margin:15px 0px; font-size: 15px;"><u>Due Fee Reminder</u></h5>' +
                                '<table style="font-size: 13px;">' +
                                '<tr>' +
                                "<td style=\"width:40%\">{!! trans('language.students_name')!!}</td>" +
                                '<td style="width:60%">: ' + value.student_name + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                "<td>{!! trans('language.fathers_name')!!}</td>" +
                                '<td>: ' + value.father_name + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td>{!! trans("language.class_sec")!!}</td>' +
                                '<td>: ' + value.class_name + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td>{!! trans("language.due_tuition_fee") !!} '+ upto_sub_circular + '</td>' +
                                '<td>: ' + (value.total_due_fee - value.fine_amount) + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td>{!! trans("language.fine_date")!!}</td>' +
                                '<td>: ' + value.fee_due_date + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td>{!! trans("language.late_payment_fee")!!}</td>' +
                                '<td>: ' + value.fine_amount + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td>{!! trans("language.after_due_date_payable_amount")!!}</td>' +
                                '<td>: ' + value.total_due_fee + '</td>' +
                                '</tr>' +
                                {{--'<tr>' +--}}
                                {{--'<td>{!! trans("language.total_due")!!}</td>' +--}}
                                {{--'<td>:</td>' +--}}
                                {{--'<td>' + value.total_due_fee + '</td>' +--}}
                                {{--'</tr>' +--}}
                                '<tr>' +
                                '<td colspan="3"><br>'+message+'</td>' +
                                '</tr>' +
                                '</table>' +
                                '</div>' +
                                '</div>';
                            no_records = no_records + 1;
                            if(no_records == 6){
                                arr_slip = arr_slip + "<div style='page-break-after: always;'></div>";
                                no_records = 0;
                            }
                        }
                    });
                    arr_slip = arr_slip + '</div>';
                    newWin = window.open("");
                    newWin.document.write('<html><head><title></title>');
                    newWin.document.title = "Student due fees";
                    newWin.document.write('</head><body>');
                    newWin.document.write(arr_slip);
                    newWin.document.write('</body></html>');
                    newWin.print();
                    newWin.close();
                }
            });
        });

        $(document).on('change', '#class_id', function () {
            $('select[name="student_id"]').empty();
            $('select[name="student_id"]').append('<option value="">--Select Student--</option>');
            var class_id = $("#class_id").val();
            if (class_id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-fee-student')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                    },
                    beforeSend: function () {
                        $('#LoadingImage').show();
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status == 'success') {
                            $.each(resopose_data, function (key, value) {
                                $('select[name="student_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                        $('#LoadingImage').hide();
                    }
                });
            }
        });
        $(document).on('change', '#fee_circular_id', function (e) {
            var fee_circular_id = $(this).val();
            fee_sub_circular_data(fee_circular_id, null);
        });
        function fee_sub_circular_data(fee_circular_id, fee_id = null)
        {
            if (fee_circular_id != '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-fee-sub-circular')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'fee_circular_id': fee_circular_id,
                        'fee_id': fee_id,
                        'required_drop_down': true
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status == 'success')
                        {
                            $('#sub_fee_circular_id').html('');
                            $.each(resopose_data, function(key, value) {
                                $('#sub_fee_circular_id')
                                    .append($("<option></option>")
                                        .attr("value", key)
                                        .text(value));
                            });
                        } else
                        {
                            $("#sub_fee_circular_id").html("<option value=''>-- select sub fee circular --</option>");
                        }
                    }
                });
            } else
            {
                $("#sub_fee_circular_id").html('');
            }
        }
    </script>
    </body>
    </html>
@endsection