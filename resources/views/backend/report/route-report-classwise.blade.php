@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<div class="tray tray-center tableCenter">
    <style type="text/css">
        /*        .buttons-html5{
                    margin-right: 81px !important; 
                }*/
        .pn{
            border-top: 1px solid #e5e5e5 !important;
        }
        button[disabled]{
            background: #76aaef !important;
        }
        .btn-danger[disabled]{
            border:0px !important;
        }
    </style>
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="" class="" title="">All Class Transport Count</span>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="route-report-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>S.No</th>
                            <th>Route Name</th>
                            @foreach ($arr_col as $class)
                            <th>{!!$class['val'] !!}</th>
                            @endforeach
                            <th>Total</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@include('backend.partials.send-sms')
<script>
    $(document).ready(function () {
        getDatatable();
        function getDatatable()
        {
            var dynamic_col = [];
            dynamic_col.push({data: 'route_id', name: 'route_id'});
            dynamic_col.push({data: 's_no', name: 's_no'});
            dynamic_col.push({data: 'route', name: 'route'});
            var arr_col = '{!! json_encode($arr_col) !!}';
            arr_col = $.parseJSON(arr_col);
            jQuery.each(arr_col, function () {
                dynamic_col.push({data: 'class' + this['key'] + '', name: 'class' + this['key'] + '', orderable: false, searchable: false});
            });
            dynamic_col.push({data: 'total', name: 'total'});
            console.log(dynamic_col);
            $('#route-report-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'Route Report Class wise',
                        "filename": 'route-report',
                        exportOptions: {
//                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Route Report Class wise',
                        "filename": 'route-report',
                        exportOptions: {
//                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9],
                            modifier: {
                                selected: true
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: "{{ url('route-report-classwise-data')}}",
                    data: function (f) {
                    }
                },
                columns: dynamic_col,
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
            $("#sms-button").prop('disabled', true);
        }


        $('#route-report-table').DataTable().on('select deselect', function (e, dt, type, indexes) {
            var arr_checked_student = checkedUser();
            if (arr_checked_student.length > 0)
            {
                $(".buttons-excel").prop('disabled', false);
                $(".buttons-print").prop('disabled', false);
                $("#sms-button").prop('disabled', false);
            } else
            {
                $(".buttons-excel").prop('disabled', true);
                $(".buttons-print").prop('disabled', true);
                $("#sms-button").prop('disabled', true);
            }
        });

        function checkedUser()
        {
            var arr_checked_student = [];
            $.each($('#route-report-table').DataTable().rows('.selected').data(), function () {
                arr_checked_student.push(this["route_id"]);
            });
            return arr_checked_student;
        }
    });
</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



