<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
</style>
<div class="row">
    @php $arr = range(0, 10, 10); @endphp
    @php
        $school = get_school_data();
        $i = 1;
         $tot_student = count($arr_marks);
    @endphp
    @foreach($arr_marks as $key =>$marks)
        @php $marks = (array)$marks;
    $arr_subject_cre = json_decode($marks['subject_marks_criteria'],true);
    $arr_obt_marks = json_decode($marks['obtained_marks'],true);
        @endphp
        <div class="col-md-6" style="padding: 0% 0 5% 3%; float: left;">
            <div class="col-md-12" style="padding:10px;">
                <table cellpadding="5">
                    <thead>
                    <tr>
                        <td colspan="2"><img src="{{ url(get_school_logo())}}" style="height:30%; width: 60%"></td>
                        <td colspan="2" style="text-align: center">
                            {!! $school['address'] !!} <br>
                            {!! $school['contact_number'] !!} <br><br>
                            <b>{!! $marks['exam_name'] !!}</b>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><b>Student Name :</b></td>
                        <td colspan="3">{!! ($marks['middle_name'] != "") ? $marks['first_name']." ".$marks['middle_name']." ".$marks['last_name'] : $marks['first_name']." ".$marks['last_name'] !!}</td>
                    </tr>
                    <tr>
                        <td><b>Father Name :</b></td>
                        <td colspan="3">{!! ($marks['father_middle_name'] != "") ? $marks['father_first_name']." ".$marks['father_middle_name']." ".$marks['father_last_name'] : $marks['father_first_name']." ".$marks['father_last_name'] !!}</td>
                    </tr>
                    <tr>
                        <td><b>Mother Name :</b></td>
                        <td colspan="3">{!! ($marks['mother_middle_name'] != "") ? $marks['mother_first_name']." ".$marks['mother_middle_name']." ".$marks['mother_last_name'] : $marks['mother_first_name']." ".$marks['mother_last_name'] !!}</td>
                    </tr>

                    <tr>
                        <td><b>Class(Sec) :</b></td>
                        <td>{!! $marks['class_name']." (".$marks['section_name'].")" !!}</td>
                        <td><b>Roll No</b></td>
                        <td>{!! $marks['roll_number'] !!}</td>
                    </tr>
                    <tr>
                        <td><b>Sr. No. :</b></td>
                        <td>{!! $marks['student_id'] !!}</td>
                        <td><b>DOB</b></td>
                        <td>{!! date("d-m-Y", strtotime($marks['dob'])) !!}</td>
                    </tr>
                    <tr>
                        {{--                        <th rowspan="2">Subject</th>--}}
                        {{--                        <th colspan="3">Obt. Marks</th>--}}
                        {{--                        <th rowspan="2">Max Marks</th>--}}
                        {{--                        <th rowspan="2">Passing Marks</th>--}}
                        {{--                        <th rowspan="2">Tot. Marks</th>--}}
                        <th><b>Subject</b></th>
                        <th><b>Total Mark</b></th>
                        <th><b>Minimum</b></th>
                        <th><b>Obtain</b></th>
                    </tr>
                    @php
                        $max_total = 0;
                        $min_total = 0;
                        $obt_total = 0;
                    @endphp
                    @foreach($arr_subject_cre as $subject_id =>$subject_data)
                        @php $all_sub_tot = 0;
                            $sub_obt = 0;
                            $max_marks = 0;
                            $sub_min = 0;
                        @endphp
                        @if(isset($subject_data['total_max_marks']))
                            @php
                                $max_marks = $subject_data['total_max_marks'];
                                $sub_obt = $arr_obt_marks[$subject_id]['sub_tot_obtained'];
                                unset($arr_obt_marks[$subject_id]['sub_tot_obtained']);
                                unset($subject_data[$subject_id]['total_max_marks']);
                            @endphp
                        @endif
                        <tr>
                            <td>{!! strtoupper($arr_subject[$subject_id]) !!}</td>
                            <td>{!! $max_marks !!}</td>
                            @foreach($arr_subject_type as $exam_type_id =>$exam_type_marks)
                                @if(isset($arr_obt_marks[$subject_id][$exam_type_id]))
                                    @php
                                        $sub_type_min = isset($subject_data[$exam_type_id]['min_marks']) ? $subject_data[$exam_type_id]['min_marks'] :0;
                                        $sub_min = $sub_min + $sub_type_min;
                                    @endphp
                                @endif
                            @endforeach
                            <td>{!! $sub_min !!}</td>
                            <td>{!! $sub_obt !!}</td>
                        </tr>
                        @php
                            $max_total += $max_marks;
                            $min_total += $sub_min;
                            $obt_total += $sub_obt;
                        @endphp
                    @endforeach
                    <tr>
                        <td colspan="4">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td>{{$max_total}}</td>
                        <td>{{$min_total}}</td>
                        <td>{{$obt_total}}</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td><b>PERCENTAGE</b></td>
                        <td>{!! $marks['percentage'] !!} %</td>
                        <td><b>DIVISION</b></td>
                        <td>{{get_marks_division($marks['percentage'])}}</td>
                    </tr>
                    <tr>
                        <td><b>RESULT / GRADE</b></td>
                        <td>{!! $marks['grade'] !!}</td>
                        <td><b>RANK</b></td>
                        <td>{!! $marks['rank'] !!}</td>
                    </tr>
                    <tr>
                        <td class='sig_row'>
                            <br>
                            <br>
                            Signature Class
                            <br>
                            Teacher
                        </td>
                        <td class='sig_row'>
                            <br>
                            <br>
                            Sign.Examination
                            <br>
                            In Charge
                        </td>
                        <td class='sig_row'>
                            <br>
                            <br>
                            Principle
                            <br>
                            Signature
                        </td>
                        <td class='sig_row'>
                            <br>
                            <br>
                            Parents
                            <br>
                            Signature
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @if($i < $tot_student)
            <div class="pagebreak"></div>
        @endif
        @php  $i++; @endphp
    @endforeach
</div>
<style>
    @page {
        size: auto;
        margin: 20mm;
    }

    .pagebreak {
        page-break-before: always;
    }

    .sig_row {
        border-right: 0px;
        text-align: center;
    }
</style>
<script type="text/javascript">
    //window.print();
</script>