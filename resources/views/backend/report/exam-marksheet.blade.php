@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
    <style>
        button[disabled] {
            background: #76aaef !important;
        }

        .button {
            background: #2e76d6 !important;
            border-radius: 0px !important;
            border: 0px;
            outline: none;
            box-shadow: 0px;
            color: #fff;
            padding: 7px 10px;
            font-weight: bold;
            font-size: 11px;
            margin-top: 1%;
            z-index: 9999999;
            font-weight: normal;
        }

        /*.buttons-html5 {*/
        /*    margin-right: 136px !important;*/
        /*}*/

        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
    <div class="tray tray-center tableCenter">
        @include('backend.partials.loader')
        <div class="">
            <div class="panel panel-visible" id="spy2">
                <div class="panel-heading">
                    <div class="panel-title hidden-xs col-md-6">
                        <span class="glyphicon glyphicon-tasks"></span> <span>Exam MarkSheet</span>
                    </div>
                </div>
                <div class="panel" id="transportId">
                    <div class="panel-body">
                        <div class="tab-content  br-n">
                            <div id="tab1_1" class="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('session_id',add_blank_option(get_session('yes'),'-- Select Session --'),$session['session_id'], ['class' => 'form-control','id'=>'session_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('class_id', $arr_class,'', ['class' => 'form-control','id'=>'class_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::select('section_id', $arr_section,'', ['class' => 'form-control','id'=>'section_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label for="result_date" class="field select" style="width: 100%;">
                                                Result Date
                                                {!! Form::text('result_date', date("d/m/Y"), ['class' => 'form-control','id' => 'result_date', 'readonly' => 'readonly']) !!}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label for="reopen_date" class="field select" style="width: 100%;">
                                                Reopen Date
                                                {!! Form::text('reopen_date', date("d/m/Y"), ['class' => 'form-control','id' => 'reopen_date', 'readonly' => 'readonly']) !!}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-body pn" style="overflow:auto;">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-info printMarksheet button" attr="GradeOnly"
                                    id='print-grade-marksheet' style="margin-top: 14px; margin-left: 10px;">Print
                                Grade Only
                            </button>
                            <button type="button" class="btn btn-info printMarksheet button" attr="MarksOnly"
                                    id='print-marks-marksheet' style="margin-top: 14px;">Print
                                Marks Only
                            </button>
                            <button type="button" class="btn btn-info printMarksheet button" attr="Both"
                                    id='print-both-marksheet' style="margin-top: 14px;">Print
                                Full
                            </button>
                        </div>
                    </div>
                    <table class="table table-bordered table-striped table-hover" id="exam-marksheet" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>Roll No</th>
                            <th>Student Name</th>
                            <th>Father Name</th>
                            <th>Mother Name</th>
                            <th>{{trans('language.dob')}}</th>
                            <th>Father Contact No</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            marksReportCard();

            var date = $('#result_date').datepicker({ dateFormat: 'dd/mm/yy' }).val();
            var date1 = $('#reopen_date').datepicker({ dateFormat: 'dd/mm/yy' }).val();


            // $('#result_date').datepicker().on('change', function (ev) {
            //     $(this).valid();
            // });
            //
            // $('#reopen_date').datepicker().on('change', function (ev) {
            //     $(this).valid();
            // });

            function marksReportCard() {
                var table = $('#exam-marksheet').DataTable({
                    destroy: true,
                    processing: true,
                    serverSide: true,
                    "bPaginate": false,
                    dom: 'Blfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                            "title": 'Exam Marksheet',
                            "filename": 'exam-final-marksheet',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5, 6, 7, 8],
                                modifier: {
                                    selected: true
                                }
                            }
                        },
                        {
                            extend: 'print',
                            "text": '<span class="fa fa-print"></span> &nbsp; Print',
                            "title": 'Exam Marksheet',
                            "filename": 'exam-final-marksheet',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5, 6, 7, 8],
                                modifier: {
                                    selected: true
                                }
                            }
                        }
                    ],
                    select: {
                        style: 'multi',
                        selector: 'td:first-child'
                    },
                    'columnDefs': [
                        {
                            'targets': 0,
                            'className': 'select-checkbox',
                            'checkboxes': {
                                'selectRow': true
                            }
                        }
                    ],
                    ajax: {
                        url: "{{ url('exam-mark-sheet-data')}}",
                        data: function (f) {
                            f.class_id = $('#class_id').val();
                            f.session_id = $("#session_id").val();
                            f.section_id = $("#section_id").val();
                        }
                    },
                    columns: [
                        {data: 'student_id', name: 'student_id'},
                        {data: 'student_id', name: 'student_id'},
                        {data: 'enrollment_number', name: 'enrollment_number'},
                        {data: 'roll_number', name: 'roll_number'},
                        {data: 'student_name', name: 'student_name'},
                        {data: 'father_name', name: 'father_name'},
                        {data: 'mother_name', name: 'mother_name'},
                        {data: 'dob', name: 'dob'},
                        {data: 'father_contact_number', name: 'father_contact_number'},
                    ]
                });
                $(".buttons-excel,.buttons-print").css({
                    'margin-left': '7px',
                    'background-color': '#2e76d6',
                    'color': 'white',
                    'border': '1px solid #eeeeee',
                    'float': 'right',
                    'padding': '5px'
                });
                $(".buttons-excel").prop('disabled', true);
                $(".buttons-print").prop('disabled', true);
                $("#print-due-slip").prop('disabled', true);
                table.on('select deselect', function (e, dt, type, indexes) {
                    var arr_checked_student = checkedStudent();
                    if (arr_checked_student.length > 0) {
                        $(".buttons-excel").prop('disabled', false);
                        $(".buttons-print").prop('disabled', false);
                        $("#print-due-slip").prop('disabled', false);
                    } else {
                        $(".buttons-excel").prop('disabled', true);
                        $(".buttons-print").prop('disabled', true);
                        $("#print-due-slip").prop('disabled', true);
                    }
                });
            }

            function checkedStudent() {
                var arr_checked_student = [];
                $.each($('#exam-marksheet').DataTable().rows('.selected').data(), function () {
                    arr_checked_student.push(this["student_id"]);
                });
                return arr_checked_student;
            }

            $(document).on('change', '#class_id,#session_id', function (e) {
                getClassSection();
            });
            $(document).on('change', '#class_id,#session_id,#section_id', function (e) {
                marksReportCard();
//            if ($('#session_id').val() !== '' && $('#class_id').val() !== '' && $('#section_id').val() !== '')
//            {
//                marksReportCard();
//            }
            });

            function getClassSection() {
                var class_id = $('#class_id').val();
                var session_id = $('#session_id').val();
                $('#section_id').empty();
                $('#section_id').append('<option value="">-- Select section --</option>');
                if (class_id !== '' && session_id !== '') {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url: "{{url('get-section-list')}}",
                        datatType: 'json',
                        type: 'POST',
                        data: {
                            'class_id': class_id,
                            'session_id': session_id,
                        },
                        beforeSend: function () {
                            $("#LoadingImage").show();
                        },
                        success: function (response) {
                            var resopose_data = [];
                            resopose_data = response.data;
                            if (response.status === 'success') {

                                $.each(resopose_data, function (key, value) {
                                    $('#section_id').append('<option value="' + key + '">' + value + '</option>');
                                });
                                $("#LoadingImage").hide();
                            }
                        }
                    });
                }
            }

            // console.log(checkedUser());

            function checkedUser() {
                var table = $('#exam-marksheet').DataTable();
                var arr_checked_student = [];
                $.each(table.rows('.selected').data(), function () {
                    arr_checked_student.push(this.student_id);
                    // arr_checked_student.push(this);
                });
                return arr_checked_student;
            }

            $('.printMarksheet').on('click', function (e) {
                var arr_checked_student = checkedUser();
                console.log(arr_checked_student);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('print-final-marksheet')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'data': arr_checked_student,
                        'class_id': $('#class_id').val(),
                        'session_id': $("#session_id").val(),
                        'section_id': $("#section_id").val(),
                        'result_date': $("#result_date").val(),
                        'reopen_date': $("#reopen_date").val(),
                        'print_type': $(this).attr('attr')
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        // newWin = window.open("");
                        // newWin.document.write('<html><head><title></title>');
                        // // newWin.document.title = "Exam Final Marksheet";
                        // newWin.document.write('</head><body>');
                        // newWin.document.write(response.data);
                        // newWin.document.write('</body></html>');
                        // setTimeout(function () {
                        //     newWin.print();
                        //     newWin.close();
                        // }, 250);
                        // newWin.print();
                        // newWin.close();
                        var newWindow = window.open('', '', 'width=100, height=100'),
                            document = newWindow.document.open(),
                            pageContent =
                                '<!DOCTYPE html>' +
                                '<html>' +
                                '<head>' +
                                '<meta charset="utf-8" />' +
                                '<title>Exam Final Marksheet</title>' +
                                '</head><body>' +
                                 response.data + '</body></html>';
                        document.write(pageContent);
                        document.close();
                        newWindow.moveTo(0, 0);
                        newWindow.resizeTo(screen.width, screen.height);
                        setTimeout(function() {
                            newWindow.print();
                            newWindow.close();
                        }, 250);
                    }
                });
            });
        });

    </script>
    </body>
    </html>
@endsection




