@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
    .select2-container, .select2-selection--single, .select2-container--default, .select2-selection--single, .select2-selection__rendered
    {
        /*line-height: 35px !important;*/
        height: 37px !important;
        /*border: 1px solid #dddddd !impportant;*/
    }
    .select2-container .select2-selection--single .select2-selection__rendered {
        display: block;
        padding-left: 16px !important;
        padding-top: 4px !important;
    }

</style>
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span>Student's Sibling(s)</span>
                </div>
            </div>
            <div class="panel" id="transportId">
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('class_id', $arr_class,$class_id, ['class' => 'form-control','id'=>'class_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('student_id', $arr_student,$student_id, ['class' => 'form-control select2','id'=>'student_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-sibling-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>{{trans('language.class_sec')}}</th>
                            <th>Stu. Name</th>
		    <th>{{trans('language.dob')}}</th>
		    <th>{{trans('language.gender')}}</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>F Mobile No</th>
                            <th>{{trans('language.mother_name')}}</th>
                            <th>M Mobile No</th>
                            <th>Admission</th>
                            <th>{{trans('language.address')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        getStudent();
    
    studentSibling();
    function studentSibling()
    {
        var parent_student = $('#student_id').val();
        var parent_id = null;
        var student_id = null;
        if (parent_student != null)
        {
            parent_student = parent_student.split('_');
            parent_id = parent_student[0];
            student_id = parent_student[1];
        }
        var table = $('#student-sibling-table').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
//                bFilter:false,
            dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                    "title": 'Student Sibling',
                    "filename": 'student-sibling-report',
                    exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                        modifier: {
                            selected: true
                        }
                    },
                    footer: true,
                },
                {
                    extend: 'print',
                    "text": '<span class="fa fa-print"></span> &nbsp; Print',
                    "title": 'Student Sibling',
                    "filename": 'student-sibling-report',
                    exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                        modifier: {
                            selected: true
                        }
                    },
                    footer: true,
                }
            ],
            select: {
                style: 'multi',
                selector: 'td:first-child'
            },
            'columnDefs': [
                {
                    'targets': 0,
                    'className': 'select-checkbox',
                    'checkboxes': {
                        'selectRow': true
                    }
                }
            ],
            ajax: {
                url: "{{ url('student_sibling')}}",
                data: function (f) {
                    f.class_id = $("#class_id option:selected").val();
                    f.parent_id = parent_id;
                    f.sibling_student_id = student_id;
                }
            },
            columns: [
	
                {data: 'student_id', name: 'student_id'},
                {data: 'enrollment_number', name: 'enrollment_number'},
                {data: 'class_name', name: 'class_name'},
                {data: 'student_name', name: 'student_name'},
	    {data: 'dob', name: 'dob'},
                {data: 'gender_name', name: 'gender_name'},
	    {data: 'father_name', name: 'father_name'},
                {data: 'father_contact_number', name: 'father_contact_number'},
                {data: 'mother_name', name: 'mother_name'},
                {data: 'mother_contact_number', name: 'mother_contact_number'},
                {data: 'admission_date', name: 'admission_date'},
                {data: 'address_line1', name: 'address_line1'},
            ]
        });

        $(".buttons-excel,.buttons-print").css({
            'margin-left': '7px',
            'background-color': '#2e76d6',
            'color': 'white',
            'border': '1px solid #eeeeee',
            'float': 'right',
            'padding': '5px'});

        $(".buttons-excel").prop('disabled', true);
        $(".buttons-print").prop('disabled', true);

        table.on('select deselect', function (e, dt, type, indexes) {
            var arr_checked_student = checkedStudent();
            if (arr_checked_student.length > 0)
            {
                $(".buttons-excel").prop('disabled', false);
                $(".buttons-print").prop('disabled', false);
            } else
            {
                $(".buttons-excel").prop('disabled', true);
                $(".buttons-print").prop('disabled', true);
            }
        });
        $('#LoadingImage').hide();
    }

    function checkedStudent()
    {
        var arr_checked_student = [];
        $.each($('#student-sibling-table').DataTable().rows('.selected').data(), function () {
            arr_checked_student.push(this["student_id"]);
        });
        return arr_checked_student;
    }

    $(document).on('change', '#class_id', function (e) {
        getStudent();
    });
    $("#student_id").select2();

    function getStudent()
    {
        $('select[name="student_id"]').empty();
        $('select[name="student_id"]').append('<option value="">--Select Student--</option>');
        var class_id = $("#class_id").val();
        var student = '<?php echo $student_id ?>';
        studentSibling();
        if (class_id != '')
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-student-class')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'class_id': class_id,
                },
                beforeSend: function () {
                    $('#LoadingImage').show();
                },
                success: function (response) {
                    var resopose_data = [];
                    resopose_data = response.data;
                    if (response.status == 'success')
                    {
                        $.each(resopose_data, function (key, value) {
                            if (student == key)
                            {
                                $('select[name="student_id"]').append('<option value="' + key + '" selected>' + value + '</option>');
                            } else
                            {
                                $('select[name="student_id"]').append('<option value="' + key + '">' + value + '</option>');
                            }
                        });
                    }
                    studentSibling();
                    $('#LoadingImage').hide();
                }
            });
        } else
        {
            studentSibling();
        }


    }
    $(document).on('change', '#student_id', function (e) {
        studentSibling();
    });
});

</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



