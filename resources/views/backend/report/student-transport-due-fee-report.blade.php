@extends('admin_panel/layout')
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
    .buttons-html5{
        margin-right: 150px !important; 
    }
</style>
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span>View Transport Fee</span>
                </div>
            </div>
            <div class="panel" id="transportId">
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('class_id', $arr_class,'', ['class' => 'form-control','id'=>'class_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('vehicle_id', $arr_vehicle,'', ['class' => 'form-control','id'=>'vehicle_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('month_id',$arr_month,'', ['class' => 'form-control','id'=>'month_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
	    <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-info" id='sms-button' style="margin-top: 14px; margin-right:72px;">Send SMS</button>
                    <button type="button" class="btn btn-info" id='print-due-slip' style="margin-top: 14px;">Print Slip</button>
	    </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-transport-detail-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>S.No</th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>Stu. Name</th>
                            <th>{{trans('language.class_sec')}}</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>{{trans('language.father_contact')}}</th>
                            <th>{{trans('language.mother_contact')}}</th>
                            <th>{{trans('language.total_fee')}}</th>
                            <th>{{trans('language.deposit_fee')}}</th>
                            <th>{{trans('language.due_fee')}}</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        studentTransportFeeReport();
        function studentTransportFeeReport()
        {
            var table = $('#student-transport-detail-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
//                bFilter:false,
                dom: 'Blfrtip',
                paging: false,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'Student Transport Fee Report',
                        "filename": 'student-fee-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                            modifier: {
                                selected: true
                            }
                        },
                        footer: true,
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Student Transport Fee Report',
                        "filename": 'student-fee-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                            modifier: {
                                selected: true
                            }
                        },
                        footer: true,
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: "{{ url('student_due_transport_fee_data')}}",
                    data: function (f) {
                        f.class_id = $('#class_id').val();
                        f.vehicle_id = $('#vehicle_id').val();
                        f.month_id = $('#month_id').val();
                    }
                },
                columns: [
                    {data: 'student_id', name: 'student_id'},
                    {data: 'DT_Row_Index', name: 'DT_Row_Index'},
                    {data: 'enrollment_number', name: 'enrollment_number'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'class_name', name: 'class_name'},
                    {data: 'father_name', name: 'father_name'},
                    {data: 'father_contact_number', name: 'father_contact_number'},
                    {data: 'mother_contact_number', name: 'mother_contact_number'},
                    {data: 'total_fee', name: 'total_fee'},
                    {data: 'deposite_fee', name: 'deposite_fee'},
                    {data: 'due_fee', name: 'due_fee'},
                ],
                drawCallback: function (row, data, start, end, display) {
                    var api = this.api();

                    $(api.column(0).footer()).html('');
                    $(api.column(1).footer()).html('');
                    $(api.column(2).footer()).html('');
                    $(api.column(3).footer()).html('');
                    $(api.column(4).footer()).html('');
                    $(api.column(5).footer()).html('');
                    $(api.column(6).footer()).html('');
                    $(api.column(7).footer()).html('');

                    // Total over this page
                    total_amount = api
                            .column(8, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);
                    deposite_amount = api
                            .column(9, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);
                    due_amount = api
                            .column(10, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return parseInt(a) + parseInt(b);
                            }, 0);

                    // Update footer
                    $(api.column(8).footer()).html(total_amount);
                    $(api.column(9).footer()).html(deposite_amount);
                    $(api.column(10).footer()).html(due_amount);
                },
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
            $("#sms-button").prop('disabled', true);
            $("#print-due-slip").prop('disabled', true);
            table.on('select deselect', function (e, dt, type, indexes) {
                var arr_checked_student = checkedStudent();
                if (arr_checked_student.length > 0)
                {
                    $(".buttons-excel").prop('disabled', false);
                    $(".buttons-print").prop('disabled', false);
                    $("#sms-button").prop('disabled', false);
                    $("#print-due-slip").prop('disabled', false);
                } else
                {
                    $(".buttons-excel").prop('disabled', true);
                    $(".buttons-print").prop('disabled', true);
                    $("#sms-button").prop('disabled', true);
                    $("#print-due-slip").prop('disabled', true);
                }
            });
        }

        function checkedStudent()
        {
            var arr_checked_student = [];
            $.each($('#student-transport-detail-table').DataTable().rows('.selected').data(), function () {
                arr_checked_student.push(this["student_id"]);
            });
            return arr_checked_student;
        }
        $(document).on('change', '#class_id,#vehicle_id,#month_id', function (e) {
            studentTransportFeeReport();
        });

        //send sms
        function checkedUser()
        {
            var table = $('#student-transport-detail-table').DataTable();
            var arr_checked_student = [];
            $.each(table.rows('.selected').data(), function () {
                arr_checked_student.push(this);
            });
            return arr_checked_student;
        }
        $('#sms-button').on('click', function (e) {
            var arr_checked_student = checkedUser();
            if (arr_checked_student.length > 0)
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('send-due-fee-sms')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        arr_fee_data: arr_checked_student,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        if (response.status == 'success')
                        {
                            $("#send_sms").prop('disabled', true);
                            $("#sms_message").val('');
                            $("#success-message").text('Messages were added in queue and will be sending soon...');
                        } else
                        {
                            alert('something went wrong');
                        }
                    }
                });
            }
        });

        // Print Due fee slip
        $('#print-due-slip').on('click', function (e) {
            var arr_checked_student = checkedUser();
            if (arr_checked_student.length > 0)
            {
                var arr_slip = '<div class="row">';
                $.each(arr_checked_student, function (key, value) {
                    arr_slip = arr_slip + '<div class="col-md-5" style="width:45%; padding: 5% 0 5% 5%; float: left;">' +
                            '<div class="col-md-12" style="border: dashed 2px; padding:10px;">' +
                            '<h5 style="text-align: center"><u>Due Transport Fee Reminder</u></h5>' +
                            '<table>' +
                            '<tr>' +
                            "<td>{!! trans('language.students_name')!!}</td>" +
                            '<td>:</td>' +
                            '<td>' + value.student_name + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            "<td>{!! trans('language.fathers_name')!!}</td>" +
                            '<td>:</td>' +
                            '<td>' + value.father_name + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td>{!! trans("language.class_sec")!!}</td>' +
                            '<td>:</td>' +
                            '<td>' + value.class_name + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td>{!! trans("language.due_fee_upto")!!}</td>' +
                            '<td>:</td>' +
                            '<td>' + value.due_fee + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td colspan="3"><br>{!! trans("language.fee_slip_message")!!}</td>' +
                            '</tr>' +
                            '</table>' +
                            '</div>' +
                            '</div>';
                });
                arr_slip = arr_slip + '</div>';
                newWin = window.open("");
                newWin.document.write('<html><head><title></title>');
                newWin.document.title = "Student due fees";
                newWin.document.write('</head><body>');
                newWin.document.write(arr_slip);
                newWin.document.write('</body></html>');
                newWin.print();
                newWin.close();
            }
        });

    });

</script>
</body>
</html>
@endsection

