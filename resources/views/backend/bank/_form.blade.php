<!-- Field Options -->
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_bank')}}</li>
                </ol>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages')
        @endif
        <div class="bg-light panelBodyy">
            <div class="section-divider mv40">
                <span><i class="fa fa-bank"></i>&nbsp;Bank Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan"></span>{{trans('language.bank_name')}}<span class="asterisk">*</span></label>
                    <label for="bank_name" class="field prepend-icon">
                        {!! Form::text('bank_name', old('bank_name',isset($bank->bank_name) ? $bank->bank_name : ''), ['class' => 'gui-input','id' => 'bank_name','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="bank_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('bank_name')) 
                    <p class="help-block">{{ $errors->first('bank_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.bank_alias') !!}</span></label>
                    <label for="bank_alias" class="field prepend-icon">
                        {!! Form::text('bank_alias', old('bank_alias',isset($bank->bank_alias) ? $bank->bank_alias : ''), ['class' => 'gui-input', 'id' => 'bank_alias','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="bank_alias" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('bank_alias')) <p class="help-block">{{ $errors->first('bank_alias') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label for="ifsc_code"><span class="radioBtnpan">{{trans('language.ifsc_code')}}<span class="asterisk">*</span></span></label>
                    <label for="ifsc_code" class="field prepend-icon">
                        {!! Form::text('ifsc_code', old('ifsc_code',isset($bank->ifsc_code) ? $bank->ifsc_code : ''), ['class' => 'gui-input', 'id' => 'ifsc_code','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="ifsc_code" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('ifsc_code')) <p class="help-block">{{ $errors->first('ifsc_code') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label for="bank_branch"><span class="radioBtnpan">{{trans('language.bank_branch')}}<span class="asterisk">*</span></span></label>
                    <label for="bank_branch" class="field prepend-icon">
                        {!! Form::text('bank_branch', old('bank_branch',isset($bank->bank_branch) ? $bank->bank_branch : ''), ['class' => 'gui-input','id' => 'bank_branch','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="bank_branch" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('bank_branch')) <p class="help-block">{{ $errors->first('bank_branch') }}</p> @endif
                </div>

            </div>

        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_bank')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
               
                <table class="table table-bordered table-striped table-hover" id="bank-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.bank_name') !!}</th>
                            <th>{!!trans('language.bank_alias') !!}</th>
                            <th>{!!trans('language.bank_branch') !!}</th>
                            <th>{!!trans('language.ifsc_code') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var table = $('#bank-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('bank/data') }}",
            columns: [
                {data: 'bank_name', name: 'bank_name'},
                {data: 'bank_alias', name: 'bank_alias'},
                {data: 'bank_branch', name: 'bank_branch'},
                {data: 'ifsc_code', name: 'ifsc_code'},
                {data: 'action', name: 'action'}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('bank/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'bank_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();

                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
        $("#bank-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                bank_name: {
                    required: true,
                    // nowhitespace: true
                },
                ifsc_code: {
                    required: true,
                    // nowhitespace: true
                },
                bank_branch: {
                    required: true,
                    // nowhitespace: true
                },

            },

            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });
</script>

