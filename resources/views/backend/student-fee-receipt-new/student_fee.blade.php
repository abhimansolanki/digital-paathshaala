<style type="text/css">
    .inner_table {
        height: 190px;
        overflow-y: auto;
    }

    .inner_table1 {
        height: 189px;
        overflow-y: auto;
    }

    .inputtd input {
        width: 106px !important;
        height: 29px !important;
    }

    .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 5px 9px;
    }

    .fee_installment_paid_amount {
        padding: 0px;
        margin: 4px 0px !important;
    }

    .inputtd input {
        width: 97px !important;
        height: 22px !important;
    }

    .inner_table {
        height: 157px !important;
    }

    .pendingAmountClass {
        border-top: 1px solid #ccc;
        font-size: 11px;
        padding: 7px 10px;
    }

    .inner_table::-webkit-scrollbar {
        width: 7px !important;
    }

    .inner_table::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3) !important;
    }

    .inner_table::-webkit-scrollbar-thumb {
        background-color: darkgrey !important;
        outline: 1px solid slategrey !important;
    }

    .margincommm {
        margin: 0px !important;
    }

    .margincommm input {
        width: 61% !important;
        height: 25px !important;
    }

    .margincommm span {
        font-size: 13px !important;
    }

    .transport_main_button {
        margin-left: 15px !important;
        margin-top: 12px !important;
    }

    .bordernonetable {
        padding-bottom: 5px;
    }
</style>
@if(isset($fee) && !empty($fee))
    @php $total_pending_amount = 0; $without_fine_total_pending_amount = 0;
    @endphp
    @foreach($fee as $student_key=> $display_fee)
        @php $student_key = $display_fee['student_id']; @endphp
        {!! Form:: hidden('student_class_id_'.$student_key,$display_fee['class_id'],['id'=>'student_class_id','readonly'=>true]) !!}
        <div class="student_tables" id="student_{!!$student_key!!}" style="margin-bottom:8px;">
            <label class="fullwitst colorchange">
                <b class="radioBtnpan"><i class="fas fa-rupee-sign"></i>{{trans('language.pending_fees')}}</b>
            </label>
            @if(isset($display_fee['student_fee']['pending']))
                <div class="commbdr" id="student-pending-fee-detail" id="show">
                    <table class="table table-striped table-fixed " style="font-size: 14px !important;">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 8px;">{{trans('language.fee_circular')}}</th>
                            <th class="text-center">{{trans('language.fine_date')}}</th>
                            <th class="text-center">{{trans('language.fee_type')}}</th>
                            <th class="text-center">Fee<br>Amount &nbsp;&nbsp; <span>+</span></th>
                            <th class="text-center">
                                <div class="col-md-8">
                                    Late<br>Fees&nbsp;&nbsp;=
                                </div>
                                <div class="col-md-4">
                                &nbsp;&nbsp; {{ Form::checkbox('installment_fine_all_'.$student_key.'[]','value',false,['class' =>'form-control frm-hrz add_fine_check_all','id' => 'all_fine', 'style' => 'height: 21px !important;width: 27px !important;']) }}
                                </div>
                            </th>
                            <th class="text-center">Total<br>Payable<br>Amount</th>
                            <th class="text-center">Paid <br>Amount</th>
                            <th class="text-center">
                                {{trans('language.action')}}<br>
                                {{ Form::checkbox('installment_fee_all_'.$student_key.'[]','value',false,['class' =>'form-control frm-hrz add_fee_check_all','id' => 'all_fee', 'style' => 'height: 21px !important;width: 27px !important;']) }}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $counter = 1;
                        @endphp
                        @foreach($display_fee['student_fee']['pending'] as $pending_fee)
                            @if(isset($pending_fee['arr_fee_detail']) && !empty($pending_fee['arr_fee_detail']))
                                @foreach($pending_fee['arr_fee_detail'] as $key2=> $fee_detail)
                                    @php
                                        $rel = $student_key.'_'.$counter;
                                        $fine_amount = 0;
                                        $fee_amount = 0;
                                        $fine_amount = (int) $pending_fee['fine_amount'];
                                        $fee_amount = $fee_detail['fee_amount'];
                                        $pending_amount = $fee_detail['final_pending_amount'];
                                    @endphp
                                    <tr>
                                        {!! Form::hidden('fee_id_'.$student_key.'[]',$pending_fee['fee_id'].'_'.$fee_detail['fee_detail_id'], ['class' => '','id' => 'fee_id']) !!}
                                        <td style="width: 8px;">{!! Form::hidden('fee_circular_id_'.$student_key.'[]',$pending_fee['fee_circular_id'], ['class' => '','id' => 'fee_circular_id']) !!}
                                            {!! Form::hidden('fee_circular_name_'.$student_key.'[]',$fee_detail['fee_sub_circular_name'], ['class' => '','id' => 'fee_sub_circular_name']) !!}
                                            {!! $fee_detail['fee_sub_circular'] !!}
                                        </td>
                                        <td>
                                            @php
                                                $due_date=date_create($fee_detail['fine_date']);
                                                if(!empty($fee_detail['grace_period'])){
                                                    date_add($due_date,date_interval_create_from_date_string($fee_detail['grace_period']." days"));
                                                }
                                            @endphp
                                            {{ date_format($due_date,"d-M-Y") }}
                                        </td>
                                        <td style="width: 10px;">{!! Form::hidden('fee_type_id_'.$student_key.'[]',$pending_fee['fee_type_id'], ['class' => '','id' => 'fee_type']) !!}
                                            {!! $pending_fee['fee_type'] !!}
                                        </td>
                                        <td>
                                            {!! Form::hidden('fee_amount_'.$student_key.'[]',$pending_amount, ['class'=>'fee_installment_amount','id' => 'fee_amount_'.$rel,'rel'=>$rel]) !!}
                                            {!! $pending_amount !!}
                                        </td>
                                        <td style="width: 112px;">
                                            <div class="col-md-8">
                                                {{$fee_detail['installment_fine_amount']}}
                                            </div>
                                            <div class="col-md-4 flot-left">
                                                @php $checked_value = $fee_detail['installment_fine_amount'] >= 0 ? true : false; @endphp
                                                {{ Form::checkbox('installment_fine_'.$student_key.'[]',$fee_detail['installment_fine_amount'],false,['class' =>'form-control frm-hrz add_fine_check','id' => 'fine_' . $rel, 'rel'=>$rel, 'style' => 'height: 21px !important;width: 27px !important;', 'checked' => $checked_value]) }}
                                            </div>
                                         </td>
                                        <td>
                                            <span class="total_payable_fee_{{$rel}}">{{ $pending_amount + $fee_detail['installment_fine_amount'] }}</span>
                                        </td>
                                        <td style="width:112px;" class="inputtd">
                                            <div>
                                                <span class="fee_installment_paid_amount_text valid_empty_value_text student_installments_text_{{$student_key}}"
                                                      id='paid_amount_text_{{$rel}}'
{{--                                                      data-fee-amount="{{$pending_amount}}"--}}
{{--                                                      data-fine-amount="{{$fee_detail['installment_fine_amount']}}"--}}
{{--                                                      data-final-payable-amount="{{($pending_amount + $fee_detail['installment_fine_amount'])}}"--}}
{{--                                                      rel="{{$rel}}"--}}
{{--                                                      fine-type="{{$pending_fee['fine_type']}}"--}}
{{--                                                      fine-days="{{$fee_detail['fine_days']}}"--}}
{{--                                                      fine="{{$fine_amount}}"--}}
{{--                                                      final-fine-amount=""--}}
{{--                                                      student-id="{{$student_key}}"--}}
{{--                                                      fine-auto=""--}}
                                                ></span>

                                                {!! Form::hidden('paid_amount_'.$student_key.'[]','', array(
                                                        'class' => 'fee_installment_paid_amount valid_empty_value student_installments_'.$student_key,
                                                        'min'=>0,
                                                        'id' => 'paid_amount_'. $rel,
                                                        'data-fee-amount'=>$pending_amount,
                                                        'data-fine-amount'=>$fee_detail['installment_fine_amount'],
                                                        'data-total-payable-fee'=>($pending_amount + $fee_detail['installment_fine_amount'] ),
                                                        'rel'=>$rel,
                                                        'fine-type'=>$pending_fee['fine_type'],
                                                        'fine-days'=>$fee_detail['fine_days'],
                                                        'fine'=>$fine_amount,
                                                        'final-fine-amount'=>'',
                                                        'student-id'=>$student_key,
                                                        'fine-auto'=>'',
                                                        'fine-apply'=>($checked_value == true) ? 'yes' : 'no'))
                                                        !!}
                                            </div>
                                        </td>
                                        <td class="cbtnn">
                                            {!! Form::checkbox('add_remove_amount','','', ['class' => 'add_remove_amount form-control frm-hrz add_remove_amount_'.$rel, 'id' => 'add_remove_amount', 'style' => 'height: 21px !important;width: 27px !important;', 'title' => "Add Remove Amount", 'data-paid-amount'=>$pending_amount ,'rel'=>$rel]) !!}
{{--                                            {!! Form::button('<i class="fas fa-plus"></i>',['class' =>'add_amount','id' => 'add_' . $rel, 'data-paid-amount'=>$pending_amount,'rel'=>$rel,'data-clicked-class'=>'add_amount']) !!}--}}
{{--                                            {!! Form::button('<i class="fas fa-minus"></i>',['class' =>'btn-primary remove_amount','id' => 'remove_' . $rel,'data-paid-amount'=>$fee_amount,'rel'=>$rel,'data-clicked-class'=>'remove_amount']) !!}--}}
                                        </td>
                                    </tr>
                                    @php  $counter ++; @endphp
                                @endforeach
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="pendingAmountClass ">
                    <div class="col-md-7 paddingrr">
                        @php $each_student_pending = 0;
                        $each_student_pending = $display_fee['student_fee']['pending_amount'] + $display_fee['student_fee']['installment_fine_amount']; 
                        $each_student_pending_without_fine = $display_fee['student_fee']['pending_amount'];
                        @endphp
                        {!! Form::hidden('pending_amount_'.$student_key,$each_student_pending, ['class' => 'pending_amount_'.$student_key,'id' => 'student_pending_amount_'. $student_key,'readonly'=>true]) !!}
                        {!! Form::hidden('without_fine_pending_amount_'.$student_key,$each_student_pending_without_fine, ['class' => 'pending_amount_'.$student_key,'id' => 'without_fine_pending_amount_'. $student_key,'readonly'=>true]) !!}
                        {!! Form::hidden('student_assigned_amount_'.$student_key,'', ['class' => 'student_assigned_amount_'.$student_key,'id' => 'student_assigned_amount_'. $student_key,'readonly'=>true]) !!}
{{--                        {{trans('language.pending_amount')}} : {{amount_format($display_fee['student_fee']['pending_amount'])}}--}}
                    </div>
{{--                    <span class="text-right pull-right">Fine : {{amount_format($display_fee['student_fee']['installment_fine_amount'])}} </span>--}}
                    <div class="clearfix"></div>
                    @php
                        $total_pending_amount = $total_pending_amount + $display_fee['student_fee']['pending_amount'] + $display_fee['student_fee']['installment_fine_amount'];
                        $without_fine_total_pending_amount = $without_fine_total_pending_amount + $display_fee['student_fee']['pending_amount'];
                    @endphp
                </div>
            @endif
        </div>
        <!-- <div class="clearfix"></div>
        <div class=" row marginRemove" id="spy1">
            <div class="col-md-2">
                <button class="button btn-primary transport_main_button transport_button" type="button" style="font-size:13px;" rel="{{$student_key}}"><img src="{{url('public/details_open.png')}}" style="" rel="{{$student_key}}" class="transport_button" current-img="details_open" id="trans_img{{$student_key}}"><b>Transport Fee</b></img></button>
            </div>
            <div class="col-md-2 margincommm  " style="margin-left: 17px !important;">
                <label><span class="radioBtnpan">Discount</span></label>
                <span id="discount_status" style="font-size:9px; color:red"></span>
                <label for="discount_amount" class="field prepend-icon">
                    <input type="number" name="discount_amount_{{$student_key}}" class='gui-input student_discount'  id='discount_amount_{{$student_key}}'  rel="{{$student_key}}" min=0 />
                    <label for="datepicker1" class="field-icon">
                        <i class="fa fa-eye"></i>
                    </label>
                </label>
            </div>
            <div class="col-md-2 margincommm" style="margin-left: -51PX !important;">
                <label><span class="radioBtnpan">Fine</span></label>
                <label for="fine_amount" class="field prepend-icon">
                    <input type="number" name="fine_amount_{{$student_key}}" class='gui-input student_fine' id='fine_amount_{{$student_key}}' readonly=true min=0 />
                    <label for="datepicker1" class="field-icon">
                        <i class="fa fa-eye"></i>
                    </label>
                </label>
            </div>
        </div>
    </div> -->
    @endforeach
    {!! Form::hidden('total_pending_amount',$total_pending_amount, ['id' => 'total_pending_amount','readonly'=>true]) !!}
    {!! Form::hidden('without_fine_total_pending_amount',$without_fine_total_pending_amount, ['id' => 'without_fine_total_pending_amount','readonly'=>true]) !!}
@endif


