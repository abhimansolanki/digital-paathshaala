<!--  Left Panel start-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
      integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<style type="text/css">
    .form-horizontal .checkbox, .form-horizontal .radio {
        min-height: 21px !important;
    }

    .table > thead > tr > th {
        font-weight: bold;
        font-size: 12px !important;
    }

    /* .table {
    background-color: #f3f3f3;
    }*/
    .mv40 {
        margin: 20px 0px !important;
    }

    /*    .textareaClass{
    height: 35px !important;
    }*/
    .admin-form .select > select {
        padding: 7px 10px !important;
    }

    .admin-form .select {
        margin-top: 0px;
    }

    .select2-container .select2-selection--single {
        height: 35px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 35px;
    }

    .table > tbody + tbody {
        border-top: 1px solid #eeeeee !important;
    }

    .option-primary em {
        font-weight: 600 !important;
        color: #333333;
    }

    .fa-user-circle {
        font-size: 65px;
        color: #ccc;
    }

    /*.section, #multiple_student{
        padding-top: 15px;
        }*/
    #submit_form {
        margin-bottom: 20px !important;
    }

    .continebutton {
        width: 142px !important;
        height: 40px !important;
        background: #4a89dc !important;
        color: #fff !important;
        text-align: center !important;
        margin-top: 15px !important;
        font-weight: bold !important;
        font-size: 16px;
    }

    .asterisk {
        padding-top: 8px;
    }

    #topbar {
        padding: 5px 21px !important;
    }

    .admin-form .panel {
        padding-top: 0px;
    }

    #studentDetails_fee {
        padding-top: 0px !important;
        padding-left: 0px !important;
    }

    #highlighid {
        border: 0px !important;
        padding: 5px 0px !important;
    }

    .rightbarclass {
        border: 0px !important;
        padding: 10px 0px;
        padding-bottom: 0px !important;
    }

    #studentDetails_fee {
        border: 0px !important;
        padding-bottom: 0px !important;
    }

    legend {
        margin-bottom: 5px !important;
    }

    .fullwitst {
        padding: 0px 15px !important;
    }

    .fullwitst i {
        padding: 3px 4px;
        border-radius: 100%;
        width: 23px;
        font-size: 13px;
    }

    .fullwitst {
        padding-top: 5px !important;
    }

    /*.commbdr table th {*/
    /*    padding: 3px 5px !important;*/
    /*}*/

    .commbdr {
        padding: 0px 0px !important;
    }

    input {
        height: 27px !important;
    }

    #fine_amount, #bank_id, #payment_mode_id, #search_class_id, #selected_fee_type_id, #student_id {
        height: 27px !important;
        line-height: 14px !important;
        padding: 0px 32px !important;
    }

    form {
        margin-top: 0px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 23px;
        height: 27px !important;
    }


    .student_fee_panel {
        margin-bottom: 0px !important;
    }

    #lightpanelBodyy {
        padding-top: 3px !important;
    }

    .select2-container .select2-selection--single {
        height: 27px !important;
    }

    #student-list table th {
        padding: 2px 9px !important;
        font-size: 13px !important;
    }

    #student-list table tr tr {
        padding: 3px 9px !important;
        font-size: 13px !important;
    }

    body.sb-l-m #content-footer.affix {
        display: none !important;
    }

    @media only screen and (max-width: 1366px) and (min-width: 1024px) {
        #dummy-table-class {
            padding: 0px;
        }

        #dummy-table-class1 {
            padding: 0px 0px 0px 10px;
        }
    }

    /*    .marginRemove{
    margin-bottom: 10px;
    }*/
    @media (min-width: 992px) {
        .col-md-2 {
            width: 13.666667% !important;
        }
    }

    .inner_table_parent::-webkit-scrollbar {
        width: 7px !important;
    }

    .inner_table_parent::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3) !important;
    }

    .inner_table_parent::-webkit-scrollbar-thumb {
        background-color: darkgrey !important;
        outline: 1px solid slategrey !important;
    }

    .det-box {
        height: 60px;
        width: 124px;
        color: #ffff;
        float: left;
        padding: 12px;
        margin: 15px 15px 15px 0px;
        font-weight: bold;
        text-align: center;
        font-size: 12px;
    }

    .frm-line {
        margin-bottom: 10px;
    }

    .student-fee-detail-box {
        padding: 0px 17px;
    }

    .colmd1 {
        height: 77.3px !important;
    }

    .colmd1 span {
        color: #0b0b0b;
        font-size: 18px;
    }

    .frl {
        margin-bottom: 5px;
    }

    #exTab2 ul li {
        width: 50%;
    }

    .frm-hrz {
        width: 190px !important;
    }
</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;" id="CustomInput">
    @include('backend.partials.loader')
    <div class="col-md-12">
        <div class="panel heading-border">
            <div id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                        </li>
                        <li class="crumb-trail">{{trans('language.add_fee_receipt')}}</li>
                    </ol>
                </div>
                <div class="topbar-right">
                    <a href="#" class="button btn-primary text-right pull-right" title="View Fee Receipt">View Fee
                        Receipt</a>
                </div>
            </div>
            <div>
                {!! Form:: hidden('fee_request','student',['id'=>'fee_request','readonly'=>true]) !!}
                {!! Form:: hidden('total_amount_hidden','',['id'=>'total_amount_hidden','readonly'=>true]) !!}
                <div class="row" style="margin-left: 0px;">
                    <div class="col-md-12" style="margin-bottom: 10px;padding: 10px;border-bottom: 1px solid #e0e0e0;">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email">Choose Class <small class="asterisk"
                                                                           style="width: 10px;">*</small> </label>
                                    {!!Form::select('search_class_id', $fee_receipt['arr_class'],isset($fee_receipt['class_id']) ? $fee_receipt['class_id'] : '', ['class' => 'form-control','id'=>'search_class_id'])!!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="email">Choose Student <small class="asterisk"
                                                                             style="width: 10px;">*</small> </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!!Form::select('student_id', $fee_receipt['arr_student'],isset($fee_receipt['student_id']) ? $fee_receipt['student_id'] : '', ['class' => 'form-control','id'=>'student_id'])!!}
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--                    <div class="col-md-12">--}}
                    {{--                        <div class="col-md-2">Choose Class</div>--}}
                    {{--                        <div class="col-md-3">--}}
                    {{--                            <small class="asterisk" style="float: left;display: block;width: 10px;">*</small>--}}
                    {{--                            <div class="section" style="padding-top: 0px;float: left;width: 94%;">--}}
                    {{--                                <label class="field select">--}}
                    {{--                                    {!!Form::select('search_class_id', $fee_receipt['arr_class'],isset($fee_receipt['class_id']) ? $fee_receipt['class_id'] : '', ['class' => 'form-control','id'=>'search_class_id'])!!}--}}
                    {{--                                    <i class="arrow double"></i>--}}
                    {{--                                </label>--}}
                    {{--                                @if ($errors->has('class_id'))--}}
                    {{--                                    <p class="help-block">{{ $errors->first('class_id') }}</p>--}}
                    {{--                                @endif--}}
                    {{--                            </div>--}}
                    {{--                            <div class="clearfix"></div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-2">Choose Student</div>--}}
                    {{--                        <div class="col-md-3">--}}
                    {{--                            <small class="asterisk" style="float: left;display: block;width: 10px;">*</small>--}}
                    {{--                            <div class="section" style="padding-top: 0px;float: left;width: 94%;">--}}

                    {{--                                <label class="field select">--}}
                    {{--                                    {!!Form::select('student_id', $fee_receipt['arr_student'],isset($fee_receipt['student_id']) ? $fee_receipt['student_id'] : '', ['class' => 'form-control','id'=>'student_id'])!!}--}}
                    {{--                                    <i class="arrow double"></i>--}}
                    {{--                                </label>--}}
                    {{--                                @if ($errors->has('student_id'))--}}
                    {{--                                    <p class="help-block">{{ $errors->first('student_id') }}</p>--}}
                    {{--                                @endif--}}
                    {{--                            </div>--}}
                    {{--                            <div class="clearfix"></div>--}}
                    {!! Form:: hidden('selected_student_id','',['id'=>'selected_student_id','readonly'=>true]) !!}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{URL::to('/')}}/public/logo/frame.jpeg" id="student_profile" height="100px"
                                     width="100px">
                            </div>
                            <div class="col-md-5">
                                <div class="row">
                                    <div id="st-details">
                                        <div class="col-md-6">Student Name :</div>
                                        <div class="col-md-6">---</div>
                                        <div class="col-md-6">Class :</div>
                                        <div class="col-md-6">---</div>
                                        <div class="col-md-6">Father Name :</div>
                                        <div class="col-md-6">---</div>
                                        <div class="col-md-6">Enroll No. :</div>
                                        <div class="col-md-6">---</div>
                                        <div class="col-md-6">Session :</div>
                                        <div class="col-md-6">---</div>
                                        <div class="col-md-6">Receipt No. :</div>
                                        <div class="col-md-6">--</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12" style="margin-bottom: 10px;">
                                        <div class="form-group">
                                            <label for="remark">{{trans('language.remark')}} <small class="asterisk"
                                                                                                    style="width: 10px;">*</small>
                                            </label>
                                            {!! Form::text('remark', old('remark',isset($fee_receipt['remark']) ? $fee_receipt['remark'] : ''), ['class' => 'form-control', 'id' => 'remark',"readonly"=>'readonly']) !!}
                                            @if ($errors->has('remark'))
                                                <p class="help-block">{{ $errors->first('remark') }}</p>
                                            @endif
                                        </div>
                                        {{--                                        <label> <span class="radioBtnpan">{{trans('language.remark')}}</span></label>--}}
                                        {{--                                        <label for="remark" class="field prepend-icon" style="float: left; width: 94%">--}}
                                        {{--                                            {!! Form::text('remark', old('remark',isset($fee_receipt['remark']) ? $fee_receipt['remark'] : ''), ['class' => 'gui-input', 'id' => 'remark',"readonly"=>'readonly']) !!}--}}
                                        {{--                                        </label>--}}
                                        {{--                                        @if ($errors->has('remark'))--}}
                                        {{--                                            <p class="help-block">{{ $errors->first('remark') }}</p>--}}
                                        {{--                                        @endif--}}
                                    </div>
                                    {{--                                    <div class="clearfix"></div>--}}
                                    <div class="col-md-12" style="margin-bottom: 10px;">
                                        <div class="form-group">
                                            <label for="fee_type"
                                                   style="margin-right: 5px;">{{trans('language.fee_type')}} </label>
                                            {!!Form::select('fee_type_id', $fee_receipt['arr_fee_type'],isset($fee_receipt['fee_type_id']) ? $fee_receipt['fee_type_id'] : '', ['class' => 'form-control','id'=>'selected_fee_type_id'])!!}
                                            @if($errors->has('fee_type_id'))
                                                <p class="help-block">{{ $errors->first('fee_type_id') }}</p>
                                            @endif
                                        </div>
                                        {{--                                        <label><span class="radioBtnpan">{{trans('language.fee_type')}}</span></label>--}}
                                        {{--                                        <label class="field select">--}}
                                        {{--                                            {!!Form::select('fee_type_id', $fee_receipt['arr_fee_type'],isset($fee_receipt['fee_type_id']) ? $fee_receipt['fee_type_id'] : '', ['class' => 'form-control','id'=>'selected_fee_type_id'])!!}--}}
                                        {{--                                            <i class="arrow double"></i>--}}
                                        {{--                                        </label>--}}
                                        {{--                                        @if($errors->has('fee_type_id'))--}}
                                        {{--                                            <p class="help-block">{{ $errors->first('fee_type_id') }}</p>--}}
                                        {{--                                        @endif--}}
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="option-group field">
                                                    <label class="option block option-primary">
                                                        {!! Form::checkbox('is_sms_send','',true, ['class' => 'gui-input ', 'id' => 'is_sms_send']) !!}
                                                        <span class="checkbox radioBtnpan"></span><em>{{trans('language.is_sms_send')}} </em>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="option-group field">
                                                    <label class="option block option-primary">
                                                        {!! Form::checkbox('is_receipt_print','',true, ['class' => 'gui-input ', 'id' => 'is_receipt_print']) !!}
                                                        <span class="checkbox radioBtnpan"></span><em>{{trans('language.is_receipt_print')}} </em>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel body" style="padding-top: 10px;">
            <div class="row">
                <div class="student-fee-detail-box">
                    <a class="colmd1 colmd1active  text-center" title="">
                        <h3>Total Fees</h3>
                        <span id="total_fee">--</span>
                    </a>
                    <a class="colmd1  text-center" title="">
                        <h3>Paid Fees</h3>
                        <span id="paid_fee">--</span>
                    </a>
                    <a class="colmd1  text-center" title="">
                        <h3>Unpaid Fees</h3>
                        <span id="unpaid_fee">--</span>
                    </a>
                    <a class="colmd1  text-center" title="">
                        <h3>Cheque Bounce Charges</h3>
                        <span id="cheque_bounce_charges">--</span>
                    </a>
                    <a class="colmd1  text-center" title="">
                        <h3>Total Late Fees</h3>
                        <span id="late_fee">--</span>
                    </a>
                    <a class="colmd1  text-center" title="">
                        <h3>Total Discount</h3>
                        <span id="total_discount">--</span>
                    </a>
                    <a class="colmd1  text-center" title="">
                        <h3>Total Payable Fees</h3>
                        <span id="payable_fee">--</span>
                    </a>
                </div>
            </div>
            <div class="row" style="margin-left: 0px;">
                <div id="exTab2" class="col-md-12">
                    <ul class="nav nav-tabs" style="margin-bottom: 10px;">
                        <li class="active">
                            <a href="#due_details" data-toggle="tab"><b>Due Details</b></a>
                        </li>
                        <li>
                            <a href="#payment_history" data-toggle="tab"><b>Payment History</b></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-content ">
                <div class="tab-pane active" id="due_details">
                    <div class="row" style="margin-left:0px;">
                        <div class="col-md-8">
                            <span style="color:#DE888A; display: none;font-size: 0.85em;padding-left: 30px;"
                                  id="paid_amount_error">Please enter amount in installment(s)</span>
                            <div id="fee-tables">
                                @include("backend.student-fee-receipt-new.dummy-table")
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row frl">
                                <div class="col-md-4">
                                    <label>{{trans('language.receipt_date')}}</label>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::text('receipt_date', old('receipt_date',isset($fee_receipt['receipt_date']) ? $fee_receipt['receipt_date'] : date('d/m/Y')), ['class' => 'form-control frm-hrz', 'id' => 'receipt_date',"readonly"=>'readonly']) !!}
                                    @if ($errors->has('receipt_date'))
                                        <p class="help-block">{{ $errors->first('receipt_date') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="row frl">
                                <div class="col-md-4">
                                    <label>{{trans('language.payment_mode')}}</label>
                                </div>
                                <div class="col-md-6">
                                    {!!Form::select('payment_mode_id', $fee_receipt['arr_payment_mode'],isset($fee_receipt['payment_mode_id']) ? $fee_receipt['payment_mode_id'] : '', ['class' => 'form-control frm-hrz', 'id'=>'payment_mode_id'])!!}
                                </div>
                            </div>
                            <div class="row frl">
                                <div class="col-md-4">
                                    <label>{{trans('language.bank')}}</label>
                                </div>
                                <div class="col-md-6">
                                    {!!Form::select('bank_id', $fee_receipt['arr_bank'],isset($fee_receipt['bank_id']) ? $fee_receipt['bank_id'] : '', ['class' => 'form-control frm-hrz','id'=>'bank_id'])!!}
                                </div>
                            </div>
                            <div class="cheque_cash" style="display:none;">
                                <div class="row frl">
                                    <div class="col-md-4">
                                        <label>{{trans('language.cheque_number')}}</label>
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::text('cheque_number', old('cheque_number',isset($fee_receipt['cheque_number']) ? $fee_receipt['cheque_number'] : ''), ['class' => 'form-control frm-hrz ', 'id' => 'cheque_number']) !!}
                                    </div>
                                </div>
                                <div class="row frl">
                                    <div class="col-md-4">
                                        <label>{{trans('language.cheque_date')}}</label>
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::text('cheque_date', old('cheque_date',isset($fee_receipt['cheque_date']) ? $fee_receipt['cheque_date'] : ''), ['class' => 'form-control frm-hrz date_picker',''=>trans('language.cheque_date'), 'id' => 'cheque_date', 'readonly'=>'readonly']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row frl online_paytm" style="display:none;">
                                <div class="col-md-4">
                                    <label>{{trans('language.transaction_id')}}</label>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::text('transaction_id', old('transaction_id',isset($fee_receipt['transaction_id']) ? $fee_receipt['transaction_id'] : ''), ['class' => 'form-control frm-hrz ',''=>trans('language.transaction_id'), 'id' => 'transaction_id']) !!}
                                </div>
                            </div>
                            <div class="row frl online_paytm" style="display:none;">
                                <div class="col-md-4">
                                    <label>{{trans('language.transaction_date')}}</label>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::text('transaction_date', old('transaction_date',isset($fee_receipt['transaction_date']) ? $fee_receipt['transaction_date'] : ''), ['class' => 'form-control frm-hrz date_picker',''=>trans('language.transaction_date'), 'id' => 'transaction_date', 'readonly'=>'readonly']) !!}
                                </div>
                            </div>
                            <div class="row frl">
                                <div class="col-md-4">
                                    <label>{{trans('language.cheque_bounce_charges')}}</label>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::number('cheque_bounce_charges_paid', old('cheque_bounce_charges_paid'), ['class' => 'form-control frm-hrz ','id' => 'cheque_bounce_charges_paid','readonly'=>true]) !!}
                                </div>
                            </div>
                            <div class="row frl">
                                <div class="col-md-4">
                                    <label>{{trans('language.total_fine')}}</label>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::text('fine_amount', old('fine_amount',isset($fee_receipt['fine_amount']) ? $fee_receipt['fine_amount'] :0), ['class' => 'form-control frm-hrz ','id' => 'fine_amount','readonly'=>true]) !!}
                                </div>
                            </div>
                            <div class="row frl">
                                <div class="col-md-4">
                                    <label>{{trans('language.total_discount')}}</label>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::number('discount_amount', old('discount_amount',isset($fee_receipt['discount_amount']) ? $fee_receipt['discount_amount'] : 0), ['class' => 'form-control frm-hrz','id' => 'discount_amount','min'=>0]) !!}
                                </div>
                            </div>
                            <div class="row frl">
                                <div class="col-md-4">
                                    <label>{{trans('language.total_amount')}}</label>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::number('total_amount', old('total_amount',isset($fee_receipt['total_amount']) ? $fee_receipt['total_amount'] : 0), ['class' => 'form-control frm-hrz ','id' => 'total_amount','readonly' => 'true','min'=>0]) !!}
                                </div>
                                <div class="col-md-2">
                                    {!! Form::checkbox('auto_adjusted','','', ['class' => 'form-control frm-hrz ', 'id' => 'auto_adjusted', 'style' => 'height: 21px !important;width: 27px !important;', 'title' => trans('language.auto_adjust')]) !!}
                                </div>
                            </div>
                            <div class="row frl">
                                <div class="col-md-4">
                                    <label>{{trans('language.net_amount')}}</label>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::number('net_amount', old('net_amount',isset($fee_receipt['net_amount']) ? $fee_receipt['net_amount'] : 0), ['class' => 'form-control frm-hrz ',''=>trans('language.net_amount'), 'id' => 'net_amount','readonly' => 'true','min'=>0]) !!}
                                </div>
                            </div>
                            <div class="row frl">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-4">
                                    {!! Form::submit($submit_button, ['class' => 'button btn-primary','id'=>'submit_form','disabled' =>'disabled','style'=>'width:125px !important;height: 27px !important;line-height: 27px !important;margin-top:12%;']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="payment_history">
                    <div class="row" style="margin-left:0px;">
                        <div class="col-md-12">
                            <div id="paid-fee-tables">
                                @include("backend.student-fee-receipt-new.dummy-paid-table")
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        // receipt date calendar will based on session date
        $("#student_id").select2();
        var minDate = '<?php
            if (!empty($fee_receipt['start_date'])) {
                echo $fee_receipt['start_date'];
            }
            ?>';
        var maxDate = '<?php
            if (!empty($fee_receipt['end_date'])) {
                echo $fee_receipt['end_date'];
            }
            ?>';
        $("#receipt_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: minDate,
            maxDate: maxDate
        });
        $("#return_receipt_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: minDate,
            maxDate: maxDate
        });
        $("#fee_cancel_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: minDate,
            maxDate: maxDate
        });
        $("#bounce_cheque_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: minDate,
            maxDate: maxDate
        });
        $("#return_trans_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: minDate,
            maxDate: maxDate
        });
        $("#fee-parameter-form").validate({
            /* @validation states + elements 
            ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
            ------------------------------------------ */
            rules: {
                receipt_date: {
                    required: true,
                },
                payment_mode_id: {
                    required: true,
                },
                cheque_number: {
                    required: function (e) {
                        if ($("#payment_mode_id option:selected").text().toLowerCase() == 'cheque') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                cheque_date: {
                    required: function (e) {
                        if ($("#payment_mode_id option:selected").text().toLowerCase() == 'cheque') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                transaction_id: {
                    required: function (e) {
                        if (($("#payment_mode_id option:selected").text().toLowerCase() == 'online') || ($("#payment_mode_id option:selected").text().toLowerCase() == 'paytm')) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                transaction_date: {
                    required: function (e) {
                        if (($("#payment_mode_id option:selected").text().toLowerCase() == 'online') || ($("#payment_mode_id option:selected").text().toLowerCase() == 'paytm')) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                total_amount: {
                    required: function (e) {
                        if ($("#total_amount").val() > 0) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                },
                net_amount: {
                    required: true,
                },
                is_sms_send: {
                    required: true,
                },
                'bank_id': {
                    required: function (e) {
                        if ($("#payment_mode_id option:selected").text().toLowerCase() == 'cheque') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            /* @validation highlighting + error placement
            ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        $('#receipt_date').datepicker().on('change', function (ev) {
            $(this).valid();
            $("#student_tables").html('');
            $("#fee-tables").html('@include("backend.student-fee-receipt-new.dummy-table")');
            $("#paid-fee-tables").html('@include("backend.student-fee-receipt-new.dummy-paid-table")');
            clearFields();
            var arr_student = [];
            var student_id = $('#student_id').val();
            $('#submit_form').prop('disabled', true);
            if (student_id != '') {
                $("#selected_student_id").val(student_id);
                arr_student.push(student_id);
                studentFeeTables(arr_student);
            }
        });
        // submit form
        $('#submit_form').on('click', function (e) {
            e.preventDefault();
            var status = '';
            status = true;
            $(".valid_empty_value").each(function (e) {
                var entered_paid_amount = $(this).val();
                // var entered_paid_amount = $(this).html();
                entered_paid_amount = parseInt(entered_paid_amount);
                if (isNaN(entered_paid_amount)) {
                    var entered_paid_amount = 0;
                }

                if (entered_paid_amount > 0) {
                    status = false;
                }
            });
            if (status == true) {
                $("#paid_amount_error").show();
                return false;
            } else {
                $("#paid_amount_error").hide();
            }
            if ($("#fee-parameter-form").valid()) {
                var total_fine_amount = $("#fine_amount").val();
                var total_discount_amount = $("#discount_amount").val();
                var extra_amount_message = '';
                total_fine_amount = parseInt(total_fine_amount);
                if (isNaN(total_fine_amount)) {
                    var total_fine_amount = 0;
                }
                total_discount_amount = parseInt(total_discount_amount);
                if (isNaN(total_discount_amount)) {
                    var total_discount_amount = 0;
                }
                if ((total_fine_amount != '' && total_fine_amount > 0) && (total_discount_amount != '' && total_discount_amount > 0)) {
                    extra_amount_message = 'Rs. ' + total_fine_amount + ' is fine amount and Rs. ' + total_discount_amount + ' is discount amount, Do you want to update net amount and continue?';
                } else if ((total_fine_amount != '' && total_fine_amount > 0)) {
                    extra_amount_message = 'Rs. ' + total_fine_amount + ' is fine amount, Do you want to apply fine and continue?';
                } else if ((total_discount_amount != '' && total_discount_amount > 0)) {
                    extra_amount_message = 'Rs. ' + total_discount_amount + ' is discount amount, Do you want to apply discount and continue?';
                }

                if (total_fine_amount > 0 || total_discount_amount > 0) {
                    bootbox.confirm({
                        message: extra_amount_message,
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            //$('#receipt_date').val(null);
                            if (result) {
                                $('#fine_apply').prop('checked', true);
                                $('#discount_apply').prop('checked', true);
                                updateTotalNetAmount();
                                submitForm();
                            } else {
                                var arr_student = checkedStudent();
                                var total_pending_without_fine = 0;
                                total_pending_without_fine = parseInt(total_pending_without_fine);
                                var total_checked_student = arr_student.length;
                                for (var i = 0; i < total_checked_student; i++) {
                                    var student_id = arr_student[i];

                                    var student_pending_amount = $("#without_fine_pending_amount_" + student_id).val();
                                    student_pending_amount = parseInt(student_pending_amount);
                                    if (isNaN(student_pending_amount)) {
                                        var student_pending_amount = 0;
                                    }
                                    total_pending_without_fine = total_pending_without_fine + student_pending_amount;
                                }
                                var total_entered_amount = $("#total_amount_hidden").val();
                                total_entered_amount = parseInt(total_entered_amount);
                                if (isNaN(total_entered_amount)) {
                                    var total_entered_amount = 0;
                                }
                                if (total_entered_amount > total_pending_without_fine) {
                                    $(".transport_pay_amount").val(null);
                                    $(".fee_installment_paid_amount").val(null);
                                    $('.fee_installment_paid_amount').attr('fine-auto', '');
                                    clearFields();
                                } else {
//                                $('.student_discount').val(0);
//                                $('#discount_amount').val(0);
                                    $('#fine_apply').prop('checked', false);
                                    $('#discount_apply').prop('checked', false);
                                    $(".student_fine").val(null);
                                    $('.fee_installment_paid_amount').attr('fine-auto', '');
                                    if ($("#auto_adjusted").is(":checked")) {
                                        $(".student_fine").val(null);
                                        var fine_calculate = checkFineApplyStatus();
                                        distributeTotalAmount(fine_calculate);
                                    }
                                    updateTotalNetAmount();
                                    submitForm();
                                }
                            }
                        }
                    });
                } else {
                    submitForm();
                }

            } else {
                return false;
            }
        });

        function submitForm() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('admin/student-fee-receipt-new/save/')}}",
                datatType: 'json',
                type: 'GET',
                data: $('#fee-parameter-form').serialize(),
                success: function (response) {
                    if (response.status == 'success') {
                        $('#server-response-success').text(response.message);
                        $('#alertmsg-success').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        var arr_student = response.data.arr_student;
                        studentFeeTables(arr_student);
//                        $('#alertmsg-success').on('hidden.bs.modal', function (e) {

                        if ($("#is_receipt_print").is(":checked")) {
                            var time = new Date().getTime();
                            var paid_receipt = response.data.paid_receipt;
                            newWin = window.open("");
                            newWin.document.write('<html><head>');
                            newWin.document.title = "Student-fees-" + time;
                            newWin.document.write('<head><style type="text/css">h1,h2,h3,h4,h5,h6{padding: 0px;margin: 0px;}p{padding: 0px;margin: 0px;}.tabletr tr td{line-height: 27px;}.border-left td{border-left: 0px !important; }.pagebreak { page-break-before: always; }</style></head></head><body>');
                            newWin.document.write(paid_receipt);
                            newWin.document.write('</body></html>');
                            setTimeout(function () {
                                newWin.print();
                                newWin.close();
                            }, 100);
                        }
                    } else {
                        $('#server-response-message').text('Something went wrong');
                        $('#alertmsg-student').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                }
            });
        }

        // onchange student event
        $("#student_id").on('change', function (e) {
            $("#student_tables").html('');
            $("#fee-tables").html('@include("backend.student-fee-receipt-new.dummy-table")');
            $("#paid-fee-tables").html('@include("backend.student-fee-receipt-new.dummy-paid-table")');
            clearFields();
            var arr_student = [];
            var student_id = $(this).val();
            $('#submit_form').prop('disabled', true);
            if (student_id != '') {
                $("#selected_student_id").val(student_id);
                arr_student.push(student_id);
                studentFeeTables(arr_student);
            }
        });

        // auto adjust amount
        $('#auto_adjusted').on('click', function (e) {
            $("#total_amount").val(null);
            $("#net_amount").val(null);
            $("#discount_amount").val(null);
            $("#fine_amount").val(null);
            $(".student_fine").val(null);
            $(".student_discount").val(null);
            $('.add_remove_amount').prop('checked', false);
            $('.add_fee_check_all').prop('checked', false);
            $(".fee_installment_paid_amount").val(null);
            $(".fee_installment_paid_amount_text").html(null);

            if ($(this).prop("checked") == true) {
                $('#total_amount').prop('readonly', false);
                // $('#fine_apply').prop('checked', true);
            } else {
                // $('#fine_apply').prop('checked', false);
                $('#total_amount').prop('readonly', true);
                $('.fee_installment_paid_amount').attr('fine-auto', '');
                //remove amount
                clearFields();
                $("#total_amount").val(null);
                $(".fee_installment_paid_amount_text").html(null);
                $(".fee_installment_paid_amount").val(null);
            }
        });
    });

    //distribute auto adjust amount to student installments equally
    $('#total_amount').on('change', function (e) {
        $('.fee_installment_paid_amount').attr('fine-auto', '');
        $(".student_fine").val(null);
        $("#fine_amount").val(null);
        checkTotalEnteredAutoAmount();
    });

    function checkTotalEnteredAutoAmount() {
        var total_amount = $('#total_amount').val();
        var cheque_bounce_charges_paid = $('#cheque_bounce_charges_paid').val();
        total_amount = parseInt(total_amount);
        var fine_calculate = 'yes';
        if (isNaN(total_amount)) {
            var total_amount = 0;
        }
        var total_pending_amount = $("#total_pending_amount").val();
        var without_fine_total_pending_amount = $("#without_fine_total_pending_amount").val();
        total_pending_amount = parseInt(total_pending_amount);
        if (isNaN(total_pending_amount)) {
            var total_pending_amount = 0;
        }
        if (isNaN(without_fine_total_pending_amount)) {
            var without_fine_total_pending_amount = 0;
        }
        if ((total_amount > 0) && (
            (total_amount <= total_pending_amount && fine_calculate === 'yes') ||
            (total_amount <= without_fine_total_pending_amount && fine_calculate !== 'yes')) && (cheque_bounce_charges_paid <= total_amount)) {
            $("#total_amount_hidden").val((total_amount - cheque_bounce_charges_paid));
            distributeTotalAmount(fine_calculate);
        } else {
            console.log('sdf');
            $('.fee_installment_paid_amount').val(null);
            $('.fee_installment_paid_amount_text').html(null);
            clearFields();
            $('#auto_adjusted').prop('checked', true);
            $('#fine_apply').prop('checked', true);

            if (total_amount > total_pending_amount && fine_calculate === 'yes') {
                $(this).val(0);
                $('#server-response-message').text("You can't enter amount more than total pending amount");
                $('#alertmsg-student').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
            if (total_amount > without_fine_total_pending_amount && fine_calculate !== 'yes') {
                $(this).val(0);
                $('#server-response-message').text("You can't enter amount more than total pending amount without fine");
                $('#alertmsg-student').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
            if (cheque_bounce_charges_paid > 0 && cheque_bounce_charges_paid > total_amount) {
                $(this).val(0);
                $('#server-response-message').text("You can't enter amount less than cheque bounce due amount");
                $('#alertmsg-student').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
        }
    }

    function distributeAmountInStudent(fine_calculate) {
        var total_entered_amount = $("#total_amount_hidden").val();
        total_entered_amount = parseInt(total_entered_amount);
        var arr_student = checkedStudent();
        var total_checked_student = arr_student.length;
        var each_student_amount = parseInt(total_entered_amount / total_checked_student);
        var arr_student_list = [];
        var new_student_list = [];
        for (var i = 0; i < total_checked_student; i++) {
            var student_id = arr_student[i];
            if (fine_calculate == 'yes') {
                var student_pending_amount = $("#student_pending_amount_" + student_id).val();
            } else {
//                alert(fine_calculate);
                var student_pending_amount = $("#without_fine_pending_amount_" + student_id).val();
//                 alert(student_pending_amount);
            }

            student_pending_amount = parseInt(student_pending_amount);
//            alert(student_pending_amount);
            if (isNaN(student_pending_amount)) {
                var student_pending_amount = 0;
            }
            arr_student_list.push({student_id: student_id, amount: student_pending_amount});
        }
        var sorted_student = arr_student_list.sort(function (a, b) {
            return a.amount - b.amount;
        });
        // assign amount to students whose pending is less than divided amount
        $.each(sorted_student, function (key, value) {
            var student_id = value.student_id;
            var student_pending_amount = value.amount;
//            alert(student_pending_amount);
            if (each_student_amount > student_pending_amount) {
                if (student_pending_amount > 0) {
                    $("#student_assigned_amount_" + student_id).val(student_pending_amount);
                    total_entered_amount = (total_entered_amount - student_pending_amount);
                }
            } else {
                new_student_list.push(student_id);
            }
        });

        var final_student_list = [];
        if (new_student_list.length > 0) {
            final_student_list = new_student_list;
        } else {
            final_student_list = arr_student;
        }
        // assign amount to students whose pending is greater than divided amount
//        alert(total_entered_amount);
        var total_pending_student = final_student_list.length;
//        alert(total_pending_student);
        var student_amount = parseInt(total_entered_amount / total_pending_student);
//        alert(student_amount);
        var will_assign_amount = parseInt(student_amount * total_pending_student);
        var amount_diff = parseInt(total_entered_amount - will_assign_amount);
        for (var i = 0; i < total_pending_student; i++) {
            var studentid = final_student_list[i];
            if (i === (total_pending_student - 1)) {
                student_amount = parseInt(student_amount) + parseInt(amount_diff);
            }
            $("#student_assigned_amount_" + studentid).val(student_amount);

        }
    }

    function distributeTotalAmount(fine_calculate) {
        distributeAmountInStudent(fine_calculate);
        var total_entered_amount = $("#total_amount_hidden").val();
//        console.log('total amount '+total_entered_amount);
        var arr_student = checkedStudent();
        var total_checked_student = arr_student.length;
        $(".fee_installment_paid_amount").val(null);
        $(".fee_installment_paid_amount_text").html(null);
        if (total_entered_amount >= 0) {
            for (var i = 0; i < total_checked_student; i++) {
                var student_id = arr_student[i];
//                console.log('student_id'+student_id);
                var student_amount_assign = $("#student_assigned_amount_" + student_id).val();
//                console.log('assign : ' + student_amount_assign);
                student_amount_assign = parseInt(student_amount_assign);

                $(".student_installments_" + student_id).each(function () {
                    if (student_amount_assign > 0) {
                        var rel = $(this).attr('rel');
                        var fine_apply = $(this).attr('fine-apply');
                        // var fee_amount = $(this).attr('data-fee-amount');
                        var payable_amount = $(this).attr('data-total-payable-fee');
//                        console.log('payable-amount :' + payable_amount);
                        payable_amount = parseInt(payable_amount);
                        if (isNaN(payable_amount)) {
                            var payable_amount = 0;
                        }
                        if (student_amount_assign > payable_amount) {
                            if (fine_apply == 'yes') {
                                $('#fine_' + rel).prop('checked', true);
                            }
                            $(".add_remove_amount_" + rel).prop('checked', true);
                            $(this).val(payable_amount);
                            $("#paid_amount_text_" + rel).html(payable_amount);
                            // $(this).html(payable_amount);
                            var paid_amount = $(this).val();
                            console.log('send :' + student_amount_assign);
                            console.log('send rel :' + rel);
//                             var fine_amount = calculateFine(rel, paid_amount, fine_calculate, student_amount_assign);
// //                             console.log('iffine_amount :' +fine_amount);
//                             student_amount_assign = student_amount_assign - fine_amount;
//                            console.log('ifstudent_amount_assign :' +student_amount_assign);
                            if (student_amount_assign > payable_amount) {
                                student_amount_assign = student_amount_assign - payable_amount;
//                                 console.log(rel+'student_amount_assign :' +student_amount_assign);
                            } else {
                                $(this).val(student_amount_assign);
                                $("#paid_amount_text_" + rel).html(student_amount_assign);
                                // $(this).html(student_amount_assign);
                                student_amount_assign = 0;
                            }
                        } else {
                            var stu_amt_assign = parseInt(student_amount_assign);
                            var stu_fine_amt = parseInt($(this).attr("data-fine-amount"));

                            $(this).val(student_amount_assign);
                            if (fine_apply == 'yes' && stu_amt_assign > stu_fine_amt) {
                                $('#fine_' + rel).prop('checked', true);
                            }else{
                                $('#fine_' + rel).prop('checked', false);
                            }
                            $(".add_remove_amount_" + rel).prop('checked', true);
                            $("#paid_amount_text_" + rel).html(student_amount_assign);
                            // $(this).html(student_amount_assign);
                            var paid_amount = $(this).val();
                            // var paid_amount = $(this).html();
                            // console.log('how to pay :' +paid_amount);
//                             var fine_amount = calculateFine(rel, paid_amount, fine_calculate);
// //                            console.log('fine_amount :' +fine_amount);
//                             student_amount_assign = student_amount_assign - fine_amount;
//                            console.log('student_amount_assign :' +student_amount_assign);
                            $(this).val(student_amount_assign);
                            $("#paid_amount_text_" + rel).html(student_amount_assign);

                            // $(this).html(student_amount_assign);
                            student_amount_assign = 0;
                        }
                        calculateFeeInstallment();
                    }
                });
            }
        }
    }

    // filter fee by fee type
    $('#selected_fee_type_id').on('change', function (e) {
        e.preventDefault();
        var arr_student = checkedStudent();
        studentFeeTables(arr_student);
    });

    // search students by class id
    $(document).on('change', '#search_class_id', function (e) {
        $('select[name="student_id"]').empty();
        // var dummy_student = $("#dummy-student-data-table").html();
        var dummy_student = '@include("backend.student-fee-receipt-new.dummy-table")';
        $("#parent_student_list").html(dummy_student);
        $("#student_tables").html('');
        // $("#fee-tables").html('');
        $("#fee-tables").html('@include("backend.student-fee-receipt-new.dummy-table")');
        $("#paid-fee-tables").html('@include("backend.student-fee-receipt-new.dummy-paid-table")');
        clearFields();
        e.preventDefault();
        var class_id = $(this).val();
        if (class_id != '') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-student-list')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'class_id': class_id,
                },
                beforeSend: function () {
                    $('#LoadingImage').show();
                },
                success: function (response) {
                    var resopose_data = [];
                    resopose_data = response.data;
                    if (response.status == 'success') {
                        $('select[name="student_id"]').append('<option value="">--Select Student--</option>');
                        $.each(resopose_data, function (key, value) {
                            $('select[name="student_id"]').append('<option value="' + key + '">' + value + '</option>');

                        });

                    }
                    $('#LoadingImage').hide();
                }
            });
        }
    });

    // show payment mode according to its master
    $('#payment_mode_id').on('change', function (e) {
        var payment_mode = $("#payment_mode_id option:selected").text().toLowerCase();
        var payment_mode_id = $(this).val();
        if (payment_mode_id != null) {
            if (payment_mode == 'cheque') {
                $(".cheque_cash").show();
                $(".online_paytm").hide();
            } else if (payment_mode == 'online' || payment_mode == 'paytm') {
                $(".cheque_cash").hide();
                $(".online_paytm").show();
            } else {
                $(".cheque_cash").hide();
                $(".online_paytm").hide();
            }
        } else {
            $(".cheque_cash").hide();
            $(".online_paytm").hide();
        }

    });

    function checkedStudent() {
        var arr_student = [];
        var fee_request = $("#fee_request").val();
        if (fee_request === 'parent') {
            $(".parent_child:checked").each(function () {
                var student_id = $(this).attr('student-id');
                arr_student.push(student_id);
            });
        } else if (fee_request === 'student') {
            var student_id = $("#selected_student_id").val();
            if (student_id !== '') {
                arr_student.push(student_id);
            }
        }
        return arr_student;
    }

    function studentFeeTables(arr_student) {
        var valueDate = document.getElementById('receipt_date').value;
        if (arr_student.length > 0) {
            $.ajax(
                {
                    url: "{{ url('get-student-fee-details-new') }}",
                    dataType: 'json',
                    type: 'GET',
                    data: {
                        'arr_student': arr_student,
                        'fee_type_id': $("#selected_fee_type_id").val(),
                        'fee_receipt_date': valueDate
                    },
                    beforeSend: function () {
                        $('#LoadingImage').show();
                    },
                    success: function (res) {
                        if (res.status == "success") {
                            clearFields();
                            $("#fee-tables").html(res.data);
                            $("#paid-fee-tables").html(res.paid_data);
                            var fee_request = $("#fee_request").val();
                            if (fee_request == 'student') {
                                // $("#student-tr").html('<td>1</td><td>' + res.student_name + '</td><td>' + res.care_of_name + '</td><td>' + res.enrollment_number + '</td><td>' + res.class_name + '</td><td><a title="view remark" class="btn btn-info view_remark centerAlign" style="text-align:center;" student-remark="' + res.remark + '"><i class="fa fa-eye" ></i></a></td>');
                                $("#student_profile").attr("src", res.profile);
                                $("#st-details").html('<div class="col-md-6">Student Name :</div>' +
                                    '<div class="col-md-6">' + res.student_name + '</div>' +
                                    '<div class="col-md-6">Class :</div>' +
                                    '<div class="col-md-6">' + res.class_name + '</div>' +
                                    '<div class="col-md-6">Father Name :</div>' +
                                    '<div class="col-md-6">' + res.care_of_name + '</div>' +
                                    '<div class="col-md-6">Enroll No. :</div>' +
                                    '<div class="col-md-6">' + res.enrollment_number + '</div>' +
                                    '<div class="col-md-6">Session :</div>' +
                                    '<div class="col-md-6">' + res.session_year + '</div>' +
                                    '<div class="col-md-6">Receipt No. :</div>' +
                                    '<div class="col-md-6">' + res.new_receipt_id + '</div>');

                                $("#total_fee").html(res.fee_detail.total_fee);
                                $("#paid_fee").html(res.fee_detail.paid_fee);
                                $("#unpaid_fee").html(res.fee_detail.unpaid_fee);
                                $("#cheque_bounce_charges").html(res.fee_detail.cheque_bounce_charges);
                                $("#cheque_bounce_charges_paid").val(res.fee_detail.cheque_bounce_charges);
                                $("#net_amount").val(res.fee_detail.cheque_bounce_charges);
                                $("#late_fee").html(res.fee_detail.late_fee);
                                $("#total_discount").html(res.fee_detail.total_discount);
                                $("#payable_fee").html(res.fee_detail.payable_fee);
                            }
                            if ($(".valid_empty_value").length > 0) {
                                $('#submit_form').prop('disabled', false);
                            } else {
                                $('#submit_form').prop('disabled', true);
                            }
                        }
                        $('#LoadingImage').hide();
                    }
                });
        } else {
            $('.modal-body').text('Please select student(s)');
            $('#alertmsg-student').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    }

    // calculate fine with installment
    $(document).on('click', '#all_fine', function () {
        $('.add_fine_check').prop('checked', this.checked);
        addFineInPayableAmount();
        addRemoveButtonTask();
    });

    $(document).on('click', '.add_fine_check', function () {
        addFineInPayableAmount();
        clearFields();
        addRemoveButtonTask();
    });

    function addFineInPayableAmount() {
        $(".add_fine_check").each(function () {
            var unique_id = $(this).attr('rel');
            console.log(unique_id);
            var fee_amount = $('#paid_amount_' + unique_id).attr('data-fee-amount');
            console.log(fee_amount);
            var fine_amount = $('#paid_amount_' + unique_id).attr('data-fine-amount');
            console.log(fine_amount);
            fine_amount = parseInt(fine_amount);
            fee_amount = parseInt(fee_amount);
            if (isNaN(fine_amount)) {
                fine_amount = 0;
            }
            if (isNaN(fee_amount)) {
                fee_amount = 0;
            }
            var total_payable_fee = fee_amount;
            if ($(this).prop("checked") == true) {
                $('#paid_amount_' + unique_id).attr('fine-apply', 'yes');
                total_payable_fee = fine_amount + fee_amount;
            } else {
                $('#paid_amount_' + unique_id).attr('fine-apply', 'no');
            }
            $('.total_payable_fee_' + unique_id).html(total_payable_fee);
            $('#paid_amount_' + unique_id).attr('data-total-payable-fee', total_payable_fee);
        });
    }

    // add installment amount when click on checkbox
    $(document).on('click', '#all_fee', function () {
        $('.add_remove_amount').prop('checked', this.checked);
        if ($("#auto_adjusted").is(":checked")) {
            bootbox.confirm({
                message: "If you click on this, it will cancel auto adjust process",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        clearFields();
                        addRemoveButtonTask();
                    }
                }
            });
        } else {
            addRemoveButtonTask();
        }
    });

    $(document).on('click', '.add_remove_amount', function (e) {
        clearFields();
        addRemoveButtonTask();
        // if ($("#auto_adjusted").is(":checked")) {
        //     bootbox.confirm({
        //         message: "If you click on this, it will cancel auto adjust process",
        //         buttons: {
        //             confirm: {
        //                 label: 'Yes',
        //                 className: 'btn-success'
        //             },
        //             cancel: {
        //                 label: 'No',
        //                 className: 'btn-danger'
        //             }
        //         },
        //         callback: function (result) {
        //             if (result) {
        //                 clearFields();
        //                 addRemoveButtonTask();
        //             }
        //         }
        //     });
        // } else {
        //     addRemoveButtonTask();
        // }
    });

    function addRemoveButtonTask() {
        $(".add_remove_amount").each(function () {
            var rel = $(this).attr('rel');
            $('#paid_amount_' + rel).attr('fine-auto', '');
            var paid_amount = $("#paid_amount_" + rel).attr('data-total-payable-fee');
            // console.log(paid_amount);
            if (isNaN(paid_amount)) {
                var paid_amount = 0;
            }
            if ($(this).prop("checked") == true) {
                $("#paid_amount_" + rel).val(paid_amount);
                $("#paid_amount_text_" + rel).html(paid_amount);
            } else if ($(this).prop("checked") == false) {
                $("#paid_amount_" + rel).val(null);
                $("#paid_amount_text_" + rel).html(null);
            }
        });
        calculateFeeInstallment();
    }

    function calculateFeeInstallment() {
        var total_paid_amount = 0;
        var total_fine_amount = 0;
        $("#paid_amount_error").hide();
        $(".fee_installment_paid_amount").each(function () {
            var rel = $(this).attr('rel');
            if ($('.add_remove_amount_' + rel).prop("checked") == true) {
                // var paid_amount = $(this).attr('data-fee-amount');
                var paid_amount = $(this).val();
                paid_amount = parseInt(paid_amount);
                if (isNaN(paid_amount)) {
                    paid_amount = 0;
                }
                total_paid_amount = paid_amount + parseInt(total_paid_amount);
                if ($('#fine_' + rel).prop("checked") == true) {
                    var fine_amount = $(this).attr('data-fine-amount');
                    fine_amount = parseInt(fine_amount);
                    if (isNaN(fine_amount)) {
                        fine_amount = 0;
                    }
                    total_fine_amount = fine_amount + parseInt(total_fine_amount);
                }
            }
        });
        $("#total_amount").val(total_paid_amount - total_fine_amount);
        $("#fine_amount").val(total_fine_amount);
        // update net amount with fine and discount
        updateTotalNetAmount();
    }

    $(document).on("click", ".delete_installment", function () {
        $('#student_fee_receipt_id').val($(this).attr("receipt-id"));
    });

    // reverted paid installment
    $(document).on("click", ".return_fee_form_submit", function (e) {
        e.preventDefault();
        var form = $("#return_fee_form");
        bootbox.confirm({
            message: "Are you sure to take action on this installment ?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var token = '{!!csrf_token()!!}';
                    $.ajax(
                        {
                            url: "{{ url('fee-installment/edit/') }}",
                            datatType: 'json',
                            type: 'GET',
                            data: form.serialize(),
                            success: function (res) {
                                $('#feeRefundModel').modal('toggle');
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                if (res.status === "success") {
                                    $('#server-response-success').text(res.message);
                                    $('#alertmsg-success').modal({
                                        backdrop: 'static',
                                        keyboard: false
                                    });
                                    var arr_student = checkedStudent();
                                    studentFeeTables(arr_student);
                                } else {
                                    $('#server-response-message').text('Something went wrong');
                                    $('#alertmsg-student').modal({
                                        backdrop: 'static',
                                        keyboard: false
                                    });
                                }
                            }
                        });
                }
            }
        });
    });

    function checkFineApplyStatus() {
        var fine_calculate = '';
        if ($("#fine_apply").is(':checked')) {
            fine_calculate = 'yes';
        } else {
            fine_calculate = 'no';
        }
        return fine_calculate;
    }

    $(document).on('click', '#fine_apply', function (e) {
        $('.fee_installment_paid_amount').attr('fine-auto', '');
        if ($("#auto_adjusted").is(":checked")) {
            $(".student_fine").val(null);
        }
        checkTotalEnteredAutoAmount();
        var fine_calculate = checkFineApplyStatus();
        distributeTotalAmount(fine_calculate);
        updateTotalNetAmount();
    });

    $('#discount_apply').on('click', function (e) {
        if ($('input[name=discount_apply]').is(":checked")) {
            $("#discount_amount").prop("readonly", false);
            $("#discount_amount").val(null);
            $(".student_discount").val(null);
        } else {
            $("#discount_amount").prop("readonly", true);
            $("#discount_amount").val(null);
            $(".student_discount").val(null);
        }
        updateTotalNetAmount();
    });

    $(document).on('change', '#discount_amount', function () {
        calculateFeeInstallment();
    });

    $(document).on('change', '.student_discount', function (e) {
        $("#discount_amount").val(null);
        if ($("#auto_adjusted").is(":checked")) {
            $("#discount_apply").prop("checked", true);
        }
        updateDiscountAmount();
    });

    function updateTotalNetAmount() {
        var net_amount = 0;
        var total_amount = $("#total_amount").val();
        var cheque_bounce_charges_paid = $("#cheque_bounce_charges_paid").val();
        total_amount = parseInt(total_amount);
        if (isNaN(total_amount)) {
            total_amount = 0;
        }

        cheque_bounce_charges_paid = parseInt(cheque_bounce_charges_paid);

        var discount_amount = $("#discount_amount").val();
        discount_amount = parseInt(discount_amount);
        if (isNaN(discount_amount)) {
            discount_amount = 0;
        }
        if (total_amount > discount_amount) {
            total_amount = total_amount - discount_amount;
        }

        $("#total_amount").val(total_amount);
        var fine_amount = $("#fine_amount").val();
        fine_amount = parseInt(fine_amount);
        if (isNaN(fine_amount)) {
            fine_amount = 0;
        }
        net_amount = total_amount + fine_amount + cheque_bounce_charges_paid;

        var transport_total_amount = $("#transport_total_amount").val();
        transport_total_amount = parseInt(transport_total_amount);
        if (isNaN(transport_total_amount)) {
            transport_total_amount = 0;
        }
        if (transport_total_amount > 0) {
            net_amount = net_amount + transport_total_amount;
        }
        // put sum of total itno total 
        $("#net_amount").val(net_amount);
    }

    function updateDiscountAmount() {
        // update discount amount
        var arr_student = checkedStudent();
        var total_checked_student = arr_student.length;
        var total_discount_amount = 0;
        var total_fine_amount = 0;
        for (var i = 0; i < total_checked_student; i++) {
            var student_id = arr_student[i];
            var student_discount_amount = $("#discount_amount_" + student_id).val();
            student_discount_amount = parseInt(student_discount_amount);
            var student_fine_amount = $("#fine_amount_" + student_id).val();
            student_fine_amount = parseInt(student_fine_amount);

            if (isNaN(student_discount_amount)) {
                student_discount_amount = 0;
            }
            if (isNaN(student_fine_amount)) {
                student_fine_amount = 0;
            }
            total_discount_amount = total_discount_amount + student_discount_amount;
            total_fine_amount = total_fine_amount + student_fine_amount;
        }
        $("#discount_amount").val(total_discount_amount);
        $("#fine_amount").val(total_fine_amount);
        updateTotalNetAmount();
    }

    function clearFields() {
        // $('#receipt_date').val(null);
        $('#transport_total_amount').val(null);
        $('#total_amount').val(null);
        $('#discount_amount').val(null);
        $('#net_amount').val(null);
        $('#fine_amount').val(null);
        $('#transaction_id').val(null);
        $('#transaction_date').val(null);
        $('#cheque_number').val(null);
        $('#cheque_date').val(null);
        $('#payment_mode_id').val(null);
        $('#bank_id').val(null);
        $('.student_fine').val(null);
        $('.student_discount').val(null);
        $('#auto_adjusted').prop('checked', false);
        $('#fine_apply').prop('checked', false);
        $('#discount_apply').prop('checked', false);
    }

    function calculateFine(rel, paid_amount, fine_calculate, student_amount_assign = 0) {
//        console.log('get rel'+rel);
        var fine_amount = 0;
        var fine_type = $('#paid_amount_' + rel).attr('fine-type');
        var fine = $('#paid_amount_' + rel).attr('fine');
        var fine_days = $('#paid_amount_' + rel).attr('fine-days');
        var fine_auto = $('#paid_amount_' + rel).attr('fine-auto');
//        console.log('fine_auto : '+fine_auto);
        fine = parseInt(fine);
        // fine calculation of every installment according to fine type
        if (fine_calculate === 'yes') {

            // fine auto means (fine calculated automatic its used to prevent multiple fine calculate)
            if (fine_auto != 'yes') {

//                console.log('yes in : '+fine_auto);
                if (fine > 0) {

//                    console.log('yes in  fine h: '+fine);
                    if (fine_type == 1) {
                        if (fine_days > 0) {
//                         console.log('yes in  % h: '+fine_type);
                            var fine_val = fine / 100;
                            fine_amount = parseInt(paid_amount * fine_val);
                            var pay_amount = parseInt(paid_amount) + fine_amount;
//                        console.log('get : '+student_amount_assign);
//                        console.log('pay : '+pay_amount);
                            if (pay_amount > student_amount_assign) {

                                // total amount = x + x(fine/100);
                                // get x value and x will be installment amount
                                // the get fine of that x value
                                // this calculation will be used for auto adjust fine
//                            var fine_x = 1 + fine_val;
//                            var installment_x = parseInt(student_amount_assign / fine_x);
//                            fine_amount = parseInt((installment_x) * fine_val);
//                            console.log('last fine'+fine_amount);
                            }
                        }

                    } else {
                        fine_amount = parseInt(fine_days * fine);
                    }
                }
                if (isNaN(fine_amount)) {
                    var fine_amount = 0;
                }
                $('#paid_amount_' + rel).attr('final-fine-amount', fine_amount);

                if (paid_amount > 0) {
                    $('#paid_amount_' + rel).attr('fine-auto', 'yes');
                }
            }
        } else {
            fine_amount = 0;
            $('#paid_amount_' + rel).attr('final-fine-amount', fine_amount);
        }
        return fine_amount;
    }


    //   Transport Fee Calculation
    {{--$(document).on('click', '.transport_add_amount, .transport_remove_amount', function (e) {--}}

    {{--    var rel = $(this).attr('rel');--}}
    {{--    var class_name = $(this).attr('data-clicked-class');--}}

    {{--    //remove auto fine set flag --}}
    {{--    var paid_amount = $("#transport_add_" + rel).attr('transport-paid-amount');--}}
    {{--    if (isNaN(paid_amount)) {--}}
    {{--        var paid_amount = 0;--}}
    {{--    }--}}
    {{--    if (class_name == 'add_transport_amount') {--}}
    {{--        $('#transport_add_' + rel).prop('disabled', true);--}}
    {{--        $('#transport_remove_' + rel).prop('disabled', false);--}}
    {{--        $("#transport_pay_amount_" + rel).val(paid_amount);--}}
    {{--    } else if (class_name == 'remove_transport_amount') {--}}
    {{--        $("#transport_pay_amount_" + rel).val(null);--}}
    {{--        $('#transport_add_' + rel).prop('disabled', false);--}}
    {{--        $('#transport_remove_' + rel).prop('disabled', true);--}}
    {{--    }--}}
    {{--    calculateTransportFeeInstallment();--}}

    {{--});--}}
    {{--$(document).on('change', '.transport_pay_amount', function (e) {--}}
    {{--    calculateTransportFeeInstallment();--}}
    {{--});--}}

    {{--function calculateTransportFeeInstallment() {--}}
    {{--    var transport_total_paid_amount = 0;--}}
    {{--    $(".transport_pay_amount").each(function () {--}}

    {{--        var rel = $(this).attr('rel');--}}
    {{--        var paid_amount = $(this).val();--}}

    {{--        $('#transport_add_' + rel).prop('disabled', false);--}}
    {{--        $('#transport_remove_' + rel).prop('disabled', false);--}}
    {{--        paid_amount = parseInt(paid_amount);--}}
    {{--        if (isNaN(paid_amount)) {--}}
    {{--            var paid_amount = 0;--}}
    {{--        }--}}
    {{--        var installment_fee_amount = $("#transport_pay_amount_" + rel).attr('trans-data-fee-amount');--}}
    {{--        installment_fee_amount = parseInt(installment_fee_amount);--}}
    {{--        if (paid_amount > installment_fee_amount) {--}}
    {{--            $(this).val(0);--}}
    {{--            $('#server-response-message').text("Entered amount can't  be greater than fee amount");--}}
    {{--            $('#alertmsg-student').modal({--}}
    {{--                backdrop: 'static',--}}
    {{--                keyboard: false--}}
    {{--            });--}}
    {{--        } else {--}}
    {{--            transport_total_paid_amount = paid_amount + parseInt(transport_total_paid_amount);--}}
    {{--        }--}}
    {{--    });--}}
    {{--    $("#transport_total_amount").val(transport_total_paid_amount);--}}
    {{--    updateTotalNetAmount();--}}
    {{--}--}}

    {{--$(document).on('click', '.revert_transport_installment', function (e) {--}}
    {{--    e.preventDefault();--}}
    {{--    var receipt_id = $(this).attr("receipt-id");--}}
    {{--    if (receipt_id !== '') {--}}
    {{--        bootbox.confirm({--}}
    {{--            message: "Are you sure to revert this installment ?",--}}
    {{--            buttons: {--}}
    {{--                confirm: {--}}
    {{--                    label: 'Yes',--}}
    {{--                    className: 'btn-success'--}}
    {{--                },--}}
    {{--                cancel: {--}}
    {{--                    label: 'No',--}}
    {{--                    className: 'btn-danger'--}}
    {{--                }--}}
    {{--            },--}}
    {{--            callback: function (result) {--}}
    {{--                if (result) {--}}
    {{--                    var token = '{!!csrf_token()!!}';--}}
    {{--                    $.ajax(--}}
    {{--                        {--}}
    {{--                            url: "{{ url('transport-fee-installment/delete/') }}",--}}
    {{--                            datatType: 'json',--}}
    {{--                            type: 'POST',--}}
    {{--                            data: {--}}
    {{--                                'transport_fee_id': receipt_id,--}}
    {{--                            },--}}
    {{--                            success: function (res) {--}}
    {{--                                if (res.status === "success") {--}}
    {{--                                    var arr_student = checkedStudent();--}}
    {{--                                    studentFeeTables(arr_student);--}}
    {{--                                }--}}
    {{--                            }--}}
    {{--                        });--}}
    {{--                }--}}
    {{--            }--}}
    {{--        });--}}
    {{--    }--}}

    {{--});--}}

    $(document).on('click', '.view_remark', function () {
        var remark = $(this).attr('student-remark');
        $('#student-data-modal').text(remark);
        $('#remark-student-name').text();
        $('#show-remark').modal({
            backdrop: 'static',
            keyboard: false
        });
    });
</script>
