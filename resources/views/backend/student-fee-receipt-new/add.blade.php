@extends('admin_panel/layout')
@push('styles')
<link href="{{ asset('/bower_components/dropify/dist/css/dropify.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/bower_components/AdminLTE/plugins/bootstrap-tags-input/bootstrap-tagsinput.css')}}" rel="stylesheet" type="text/css" />
<style>
  .bootstrap-tagsinput {
  width: 100%;
  }
</style>
@endpush
@section('content')
<div class="row">
  <div class="col-md-12 padding_remove">
    <div class="box box-info">
      <div class="box-header with-border"></div>
      {!! Form::open(['files'=>TRUE,'id' => 'fee-parameter-form' , 'class'=>'form-inline','url' => $save_url,'autocomplete'=>'off']) !!}
      @include('backend.student-fee-receipt-new._form',['submit_button' => $submit_button])
      {!! Form::close() !!}
    </div>
  </div>
</div>
<div class="modal fade" id="contactModal" 
  tabindex="-1" role="dialog" 
  aria-labelledby="contactModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
@endpush