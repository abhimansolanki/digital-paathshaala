@php
    $school_data = get_school_data();
    $school_logo = get_school_logo();
@endphp
@foreach($fee as $display_fee)
    <div style="margin: 0px auto; width: 700px; padding: 15px ">
    @if(isset($display_fee['student_fee']['recently_paid_fee']) && !empty($display_fee['student_fee']['recently_paid_fee']))
        @php $receipt_number = isset($display_fee['student_fee']['recently_paid_fee']['receipt_number']) ? $display_fee['student_fee']['recently_paid_fee']['receipt_number'] : null;
        $receipt_date = isset($display_fee['student_fee']['recently_paid_fee']['receipt_date']) ? $display_fee['student_fee']['recently_paid_fee']['receipt_date'] : date('d M Y');
        $receipt_payment_mode = isset($display_fee['student_fee']['recently_paid_fee']['payment_mode_id']) ? $payment_modes[$display_fee['student_fee']['recently_paid_fee']['payment_mode_id']] : "---";
        @endphp
        <!--student education fee receipt code-->
            <div class="border" style="border:1px solid #ccc; padding-top: 10px; padding-bottom: 15px;">
                <div style="float: left; width: 25%; padding-left: 10px;">
                    <img src="{{url($school_logo)}}" style="width: 120px">
                    <br>
                    <br>
                    <h5><b>(Office Copy)</b></h5>
                </div>
                <div style="float: left; width: 70%; text-align: right; padding-right: 10px;">
                    <h4 style="color:#000; font-size: 18px; border:2px solid #000; text-align: center;">
                        FEE RECEIPT
                    </h4>
                    <div style="clear: both;"></div>
                    <h4 style="color:#000; font-size: 12px;  text-align: center;">
                        {{ $school_data['effilated_number'] }}
                    </h4>
                    <div style="clear: both;"></div>
                    <h4 style="color:#000; font-size: 20px; text-align: center;">
                        {{ strtoupper($school_data['school_name']) }}
                    </h4>
                    <div style="clear: both;"></div>
                    <h4 style="color:#000; font-size: 12px; text-align: center;">
                        {{$school_data['address']}}
                    </h4>
                    <div style="clear: both;"></div>
                    <h4 style="color:#000; font-size: 12px; text-align: center;">
                        Ph. {{$school_data['contact_number']}}, {{$school_data['mobile_number']}}
                    </h4>
                </div>
                <div style="clear: both;"></div>
                <div style=" border-top: 1px solid #ccc; margin-top: 10px; padding:5px 10px; ">
                    <table width="100%" class="tabletr" style=" border-top: 0px; border-bottom: 0px;">
                        <tr>
                            <td><b>Receipt No. &nbsp; :</b>&nbsp;{{$display_fee['session_year']}}/{{$receipt_number}}</td>
                            <td style="width: 25%"><b>Date &nbsp; :</b>
                                &nbsp;{{ date("d-m-Y",strtotime($receipt_date)) }}</td>
                        </tr>
                        <tr>
                            <td><b>Student Name&nbsp;:</b>&nbsp;{{$display_fee['student_name']}}</td>
                            <td style="width: 25%"><b>Pay Mode&nbsp;:</b>&nbsp;{{strtoupper($receipt_payment_mode)}}</td>
                        </tr>
                        <tr>
                            <td><b>Father Name&nbsp; :</b> &nbsp; {{$display_fee['father_name']}}</td>
                            <td><b>Class No.&nbsp; :</b> &nbsp; {{$display_fee['class_name']}}</td>
                        </tr>
                    </table>
                </div>
                <table border="1" width="100%" cellspacing="0" cellpadding="10"
                       style="border-left:0px; border-right: 0px; border-top: 0px; border-bottom: 0px;">
                    <tr>
                        <th>S. No.</th>
                        <th style="text-align: left;">{!! trans('language.description') !!}</th>
                        <th style="text-align: right;">Amount</th>
                    </tr>
                    @php $s = 1; @endphp
                    @foreach($display_fee['student_fee']['recently_paid_fee']['fee_receipt_detail'] as $fee_details_obj)
                        <tr>
                            <td>{{$s++}}</td>
                            <td style="text-align: left;">{!! $fee_details_obj['fee_type']." (".$fee_details_obj['fee_circular_name'].")" !!}</td>
                            <td style="text-align: right;">{!! amount_format($fee_details_obj['installment_paid_amount']) !!}</td>
                        </tr>
                    @endforeach
                    <tr>
                    <tr>
                        <th></th>
                        <th style="text-align: left;">
                            @if($display_fee['student_fee']['recently_paid_fee']['fine_amount'] > 0 || $display_fee['student_fee']['recently_paid_fee']['discount_amount'] > 0)
                                <b style="line-height: 25px;">{!! trans('language.total') !!}:-</b><br/>
                            @endif
                            @if($display_fee['student_fee']['recently_paid_fee']['fine_amount'] > 0)
                                <b style="line-height: 25px;"> {!! trans('language.fine') !!} :-</b><br/>
                            @endif
                            @if($display_fee['student_fee']['recently_paid_fee']['discount_amount'] > 0)
                                <b style="line-height: 25px;"> {!! trans('language.discount') !!}:- </b><br/>
                            @endif
                            <b style="line-height: 25px;">{!! trans('language.net') !!}:- </b><br/>
                        </th>
                        <th style="text-align: right;">
                            @if($display_fee['student_fee']['recently_paid_fee']['fine_amount'] > 0 || $display_fee['student_fee']['recently_paid_fee']['discount_amount'] > 0)
                                <span style="line-height: 25px;"> {!! amount_format($display_fee['student_fee']['recently_paid_fee']['total_amount'])!!}</span><br>
                            @endif
                            @if($display_fee['student_fee']['recently_paid_fee']['fine_amount'] > 0)
                                <span style="line-height: 25px;">{!! amount_format($display_fee['student_fee']['recently_paid_fee']['fine_amount'])!!}</span><br/>
                            @endif
                            @if($display_fee['student_fee']['recently_paid_fee']['discount_amount'] > 0)
                                <span style="line-height: 25px;">{!! amount_format($display_fee['student_fee']['recently_paid_fee']['discount_amount'])!!}</span><br/>
                            @endif
                            <span style="line-height: 25px;">{!! amount_format($display_fee['student_fee']['recently_paid_fee']['net_amount'])!!}</span><br/>
                        </th>
                    </tr>
                </table>
            </div>
            <div style="text-align: right;">
                <b>
                    Auth. Signature
                </b>
            </div>
        @endif
    </div>
@endforeach
<hr>
<div class="pagebreak"> </div>
@foreach($fee as $display_fee)
    <div style="margin: 0px auto; width: 700px; padding: 15px ">
    @if(isset($display_fee['student_fee']['recently_paid_fee']) && !empty($display_fee['student_fee']['recently_paid_fee']))
        @php $receipt_number = isset($display_fee['student_fee']['recently_paid_fee']['receipt_number']) ? $display_fee['student_fee']['recently_paid_fee']['receipt_number'] : null;
        $receipt_date = isset($display_fee['student_fee']['recently_paid_fee']['receipt_date']) ? $display_fee['student_fee']['recently_paid_fee']['receipt_date'] : date('d M Y');
        $receipt_payment_mode = isset($display_fee['student_fee']['recently_paid_fee']['payment_mode_id']) ? $payment_modes[$display_fee['student_fee']['recently_paid_fee']['payment_mode_id']] : "---";
        @endphp
        <!--student education fee receipt code-->
            <div class="border" style="border:1px solid #ccc; padding-top: 10px; padding-bottom: 15px;">
                <div style="float: left; width: 25%; padding-left: 10px;">
                    <img src="{{url($school_logo)}}" style="width: 120px">
                    <br>
                    <br>
                    <h5><b>(Student Copy)</b></h5>
                </div>
                <div style="float: left; width: 70%; text-align: right; padding-right: 10px;">
                    <h4 style="color:#000; font-size: 18px; border:2px solid #000; text-align: center;">
                        FEE RECEIPT
                    </h4>
                    <div style="clear: both;"></div>
                    <h4 style="color:#000; font-size: 12px;  text-align: center;">
                        {{ $school_data['effilated_number'] }}
                    </h4>
                    <div style="clear: both;"></div>
                    <h4 style="color:#000; font-size: 20px; text-align: center;">
                        {{ strtoupper($school_data['school_name']) }}
                    </h4>
                    <div style="clear: both;"></div>
                    <h4 style="color:#000; font-size: 12px; text-align: center;">
                        {{$school_data['address']}}
                    </h4>
                    <div style="clear: both;"></div>
                    <h4 style="color:#000; font-size: 12px; text-align: center;">
                        Ph. {{$school_data['contact_number']}}, {{$school_data['mobile_number']}}
                    </h4>
                </div>
                <div style="clear: both;"></div>
                <div style=" border-top: 1px solid #ccc; margin-top: 10px; padding:5px 10px; ">
                    <table width="100%" class="tabletr" style=" border-top: 0px; border-bottom: 0px;">
                        <tr>
                            <td><b>Receipt No. &nbsp; :</b>&nbsp;{{$display_fee['session_year']}}/{{$receipt_number}}</td>
                            <td style="width: 25%"><b>Date &nbsp; :</b>
                                &nbsp;{{ date("d-m-Y",strtotime($receipt_date)) }}</td>
                        </tr>
                        <tr>
                            <td><b>Student Name&nbsp;:</b>&nbsp;{{$display_fee['student_name']}}</td>
                            <td style="width: 25%"><b>Pay Mode&nbsp;:</b>&nbsp;{{strtoupper($receipt_payment_mode)}}</td>
                        </tr>
                        <tr>
                            <td><b>Father Name&nbsp; :</b> &nbsp; {{$display_fee['father_name']}}</td>
                            <td><b>Class No.&nbsp; :</b> &nbsp; {{$display_fee['class_name']}}</td>
                        </tr>
                    </table>
                </div>
                <table border="1" width="100%" cellspacing="0" cellpadding="10"
                       style="border-left:0px; border-right: 0px; border-top: 0px; border-bottom: 0px;">
                    <tr>
                        <th>S. No.</th>
                        <th style="text-align: left;">{!! trans('language.description') !!}</th>
                        <th style="text-align: right;">Amount</th>
                    </tr>
                    @php $s = 1; @endphp
                    @foreach($display_fee['student_fee']['recently_paid_fee']['fee_receipt_detail'] as $fee_details_obj)
                        <tr>
                            <td>{{$s++}}</td>
                            <td style="text-align: left;">{!! $fee_details_obj['fee_type']." (".$fee_details_obj['fee_circular_name'].")" !!}</td>
                            <td style="text-align: right;">{!! amount_format($fee_details_obj['installment_paid_amount']) !!}</td>
                        </tr>
                    @endforeach
                    <tr>
                    <tr>
                        <th></th>
                        <th style="text-align: left;">
                            @if($display_fee['student_fee']['recently_paid_fee']['fine_amount'] > 0 || $display_fee['student_fee']['recently_paid_fee']['discount_amount'] > 0)
                                <b style="line-height: 25px;">{!! trans('language.total') !!}:-</b><br/>
                            @endif
                            @if($display_fee['student_fee']['recently_paid_fee']['fine_amount'] > 0)
                                <b style="line-height: 25px;"> {!! trans('language.fine') !!} :-</b><br/>
                            @endif
                            @if($display_fee['student_fee']['recently_paid_fee']['discount_amount'] > 0)
                                <b style="line-height: 25px;"> {!! trans('language.discount') !!}:- </b><br/>
                            @endif
                            <b style="line-height: 25px;">{!! trans('language.net') !!}:- </b><br/>
                        </th>
                        <th style="text-align: right;">
                            @if($display_fee['student_fee']['recently_paid_fee']['fine_amount'] > 0 || $display_fee['student_fee']['recently_paid_fee']['discount_amount'] > 0)
                                <span style="line-height: 25px;"> {!! amount_format($display_fee['student_fee']['recently_paid_fee']['total_amount'])!!}</span><br/>
                            @endif
                            @if($display_fee['student_fee']['recently_paid_fee']['fine_amount'] > 0)
                                <span style="line-height: 25px;">{!! amount_format($display_fee['student_fee']['recently_paid_fee']['fine_amount'])!!}</span><br/>
                            @endif
                            @if($display_fee['student_fee']['recently_paid_fee']['discount_amount'] > 0)
                                <span style="line-height: 25px;">{!! amount_format($display_fee['student_fee']['recently_paid_fee']['discount_amount'])!!}</span><br/>
                            @endif
                            <span style="line-height: 25px;">{!! amount_format($display_fee['student_fee']['recently_paid_fee']['net_amount'])!!}</span><br/>
                        </th>
                    </tr>
                </table>
            </div>
            <div style="text-align: right;">
                <b>
                    Auth. Signature
                </b>
            </div>
        @endif
    </div>
@endforeach