@if(!empty($parent))
<div class="col-md-12">
    <div class=" row" id="highlighid">
        <div class="col-md-4">
            <label><b class="hightli">{{trans('language.care_of_name')}} : </b></label>  
            <span id="father_name">{{$parent['care_of_name']}}</span>
        </div>
    </div>
</div>
<!-- Student details with check boxes -->
@if(isset($parent['student_detail']))
<div class="col-md-12" id="studentDetails_fee">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>S.No.</th>
                <th>Name</th>
                <th>Scholar No.</th>
                <th>Class</th>
                <th class="text-center"> Action</th>
            </tr>
        </thead>
    </table>
    <div class=" inner_table_parent wrap" style="height: 97px; overflow-x: auto">
        <table class="table table-striped">
    <!--        <thead>
                <tr>
                    <th>S.No.</th>
                    <th>Name</th>
                    <th>Scholar No.</th>
                    <th>Class</th>
                    <th class="text-center"> Action</th>
                </tr>
            </thead>-->
            <tbody>
                @foreach($parent['student_detail'] as $key=> $student_detail)
                <tr>
                    <td style="width: 100px;">{{$key +1}}</td>
                    <td style="width: 170px;"> @if(!empty($student_detail['profile']))
                        <img src="{{url($student_detail['profile'])}}">
                        @else
                        <img src="../../public/st.jpg">
                        @endif

                        &nbsp; {{$student_detail['student_name']}}</td>
                    <td style="width: 200px;">{{$student_detail['enrollment_number']}}</td>
                    <td style="width: 140px;">{{$student_detail['class_name']}}</td>
                    <td>
                        <div class="option-group field" id="checkBooxx" style="float:left; margin-left: 33px;">
                            <label class="option block option-primary">
                                {!! Form::checkbox('checked_student[]',$student_detail['student_id'],'', ['class' => 'gui-input parent_child', 'id' => 'checked_student','student-id'=>$student_detail['student_id'],'class_id'=>$student_detail['class_id']]) !!}
                                <span class="checkbox radioBtnpan" style="top:-5px;"></span>
                            </label>
                        </div>
                        <a title="view remark" class="btn btn-info view_remark centerAlign" style="margin: 5px  auto !important;" student-remark="{{$student_detail['remark']}}"><i class="fa fa-eye" ></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {!! Form::button('Continue',['class' =>'button btn-primary continebutton','id' => 'parent_student_fee','style'=>'width: 100px !important;height: 30px !important;']) !!}
    <div class="clearfix"></div>
</div>
@else
<!--     parent found but student not may be in not in current session-->
<div id='dummy-student-data-table' style="">
    <div class="col-md-12" id="studentDetails_fee">
        <table class="table table-striped" id="student-list">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Scholar No.</th>
                    <th>Class</th>
                    <th class="text-center"> Action</th>
                </tr>
            </thead>
            <tbody>
                <tr id="student-tr">
                    <td colspan="5" style="text-align:center; color:#DE888A;font-size: 12px;">No student(s) available in table.</td>
                </tr>
            </tbody>
        </table>
        <div class="clearfix"></div>
    </div>
</div>
@endif
@else
<div id='dummy-student-data-table' style="">
    <div class="col-md-12">
        <div class=" row" id="highlighid">
            <div class="col-md-4">
                <label><b class="hightli">{{trans('language.care_of_name')}} : </b></label>  
                <span id="father_name"></span>
            </div>
        </div>
    </div>
    <div class="col-md-12" id="studentDetails_fee">
        <table class="table table-striped" id="student-list">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Scholar No.</th>
                    <th>Class</th>
                    <th class="text-center"> Action</th>
                </tr>
            </thead>
            <tbody>
                <tr id="student-tr">
                    <td colspan="5" style="text-align:center; color:#DE888A;font-size: 12px;">No student(s) available in table.</td>
                </tr>
            </tbody>
        </table>
        <div class="clearfix"></div>
    </div>
</div>
@endif