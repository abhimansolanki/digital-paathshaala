@extends('admin_panel/layout')
<!--<link href="{{ asset('/bower_components/dropify/dist/css/dropify.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/bower_components/AdminLTE/plugins/bootstrap-tags-input/bootstrap-tagsinput.css')}}" rel="stylesheet" type="text/css" />-->
<style>
    td.details-control {
        background: url(<?php echo url('/public/details_open.png'); ?>) no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url(<?php echo url('/public/details_close.png'); ?>) no-repeat center center;
    }
    table td{
        font-size:11px !important; 
    }
</style>
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="tray tray-center tableCenter"> 
	        <div class="panel panel-visible">
		<div class="panel-heading">
		    <div class="panel-title hidden-xs col-md-6">
		        <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_create_exam')}}</span>
		    </div>
		</div>
		@if(Session::has( 'success'))
                        @include('backend.partials.messages')
		@endif
		<div class="panel-body pn">
		    <table class="table table-bordered table-striped table-hover" id="create-exam-table" cellspacing="0" width="100%">
		        <thead>
			<tr>
			    <th></th>
			    <th>{!!trans('language.exam_name') !!}</th>
			    <th>{!!trans('language.exam_category') !!}</th>
                <th>{!!trans('language.exam_nature') !!}</th>
                <th>{!!trans('language.sequence') !!}</th>
			    <th>{!!trans('language.action') !!}</th>
			</tr>
		        </thead>
		    </table>
		</div>
	        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var table = $('#create-exam-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('create-exam/data') }}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',

                },
                {data: 'exam_name', name: 'exam_name'},
                {data: 'exam_category', name: 'exam_category'},
                {data: 'exam_nature_name', name: 'exam_nature_name'},
                {data: 'sequence', name: 'sequence'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });


        $('#create-exam-table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                var child = format(row.data());
                if (child !== '')
                {
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            }
        });
        function format(d) {
            // `d` is the original data object for the row
            var exam_class = d.exam_class_data;
            var tab_body = '';
            var tab_head = '<table cellpadding="5" cellspacing="0" border="0" style="margin-left: -4px; font-size:11px !important;">';
            tab_head = tab_head + '<tr style="font-weight: 600;">' +
                    '<th style="width: 29%;">Class</th>' +
                    '<th style="width: 24%;">Section</th>' +
//                    '<th style="width: 29%;">Exam Type</th>' +
                    '<th style="width: 20%;">Action</th>' +
                    '</tr>';
            ;
            $.each(exam_class, function (key, value)
            {
                tab_body = tab_body + '<tr><td style="">' + value.class_name + '</td>' +
                        '<td>' + value.section_name + '</td>' +
//                        '<td style="">' + value.exam_type + '</td>' +
                        '<td style=""><a title="Edit" id="deletebtn1" href="' + value.url + '" class="btn btn-success"><i class="fa fa-edit" ></i></a>' +
                        '<button title="Delete" id="deletebtn" class="btn btn-danger delete-button-class" data-id="' + value.exam_class_id + '"><i class="fa fa-trash"></i></button></td>' +
                        '</tr>';
            });
            var tab_foot = '</table>';
            var table_data = '';
            if (tab_body !== '')
            {
                table_data = tab_head + tab_body + tab_foot;
            }
            return table_data;
        }

// delete exam
        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");
            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax({
                            url: "{{ url('create-exam/delete/') }}",
                            datatType: 'json',
                            type: 'POST',
                            data: {
                                'exam_id': id,
                            },
                            success: function (res)
                            {
                                if (res.status == "success")
                                {
                                    table.ajax.reload();

                                } else if (res.status === "used")
                                {
                                    $('#server-response-message').text(res.message);
                                    $('#alertmsg-student').modal({
                                        backdrop: 'static',
                                        keyboard: false
                                    });
                                }
                            }
                        });
                    }
                }
            });

        });

        // delete exam class-section mapping

        $(document).on("click", ".delete-button-class", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('exam-class/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'exam_class_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();
                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        } else
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });
        });


        $("#create-exam-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                exam_name: {
                    required: true,
                    // nowhitespace: true
                },
                session_id: {
                    required: true
                },
                exam_category_id: {
                    required: true
                },
                exam_nature: {
                    required: true
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
    });
</script>
@endsection
