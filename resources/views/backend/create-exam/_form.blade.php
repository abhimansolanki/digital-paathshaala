<style>
    .marks {
        width: 49%;
    }

    .total_mar {
        width: 100%;
    }

    #accordionpanel1 {
        padding: 0px !important;
        border-radius: 0px;
    }

    .accordionpanel {
        background: #f9f9f9 !important;
        width: 100%;
        display: block;
        padding: 10px 8px;
        font-weight: bold;
        text-transform: capitalize;
        font-size: 12px;
    }

    .accordionpanel label {
        color: #000;
    }

    .classopen {
        display: none;
    }

    table tr td {
        text-transform: capitalize;
    }
</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.create_exam')}}</li>
                </ol>
            </div>
        </div>
        @if(Session::has( 'success'))
        @include('backend.partials.messages')
        @endif
        <div class="bg-light panelBodyy">
            <div class="section-divider mv40">
                <span><i class="fa fa-graduation-cap"></i> Exam's Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.exam_name')}}<span class="asterisk">*</span></span></label>
                    <label for="exam_name" class="field prepend-icon">
                        {!! Form::text('exam_name', old('exam_name',isset($exam['exam_name']) ? $exam['exam_name'] : ''), ['class' => 'gui-input',''=>trans('language.exam_name'), 'id' => 'exam_name','pattern'=>'[a-zA-Z][a-zA-Z0-9\s\.]*']) !!}
                        <label for="exam_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('exam_name'))
                    <p class="help-block">{{ $errors->first('exam_name') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.exam_alias')}}</span></label>
                    <label for="exam_alias" class="field prepend-icon">
                        {!! Form::text('exam_alias', old('exam_alias',isset($exam['exam_alias']) ? $exam['exam_alias'] : ''), ['class' => 'gui-input',''=>trans('language.exam_alias'), 'id' => 'exam_alias','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="exam_alias" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('exam_alias'))
                    <p class="help-block">{{ $errors->first('exam_alias') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $exam['arr_session'],isset($exam['session_id']) ? $exam['session_id'] : null, ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label> @if ($errors->has('session_id'))
                    <p class="help-block">{{ $errors->first('session_id') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.exam_category')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('exam_category_id', $exam['arr_exam_category'],isset($exam['exam_category_id']) ? $exam['exam_category_id'] : null, ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label> @if ($errors->has('exam_category_id'))
                    <p class="help-block">{{ $errors->first('exam_category_id') }}</p>
                    @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.exam_nature')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('exam_nature', $exam['arr_exam_nature'],isset($exam['exam_nature']) ? $exam['exam_nature'] : null, ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label> @if ($errors->has('exam_nature'))
                    <p class="help-block">{{ $errors->first('exam_nature') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.sequence')}}<span class="asterisk">*</span></span></label>
                    <label for="sequence" class="field prepend-icon">
                        {!! Form::number('sequence', old('sequence',isset($exam['sequence']) ? $exam['sequence'] : ''), ['class' => 'gui-input',''=>trans('language.sequence'), 'id' => 'sequence']) !!}
                        <label for="sequence" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('sequence'))
                        <p class="help-block">{{ $errors->first('sequence') }}</p>
                    @endif
                </div>
                <div class="col-md-3" style="margin-top: 21px;">
                    <div class="option-group field" id="checkBooxx">
                        <label class="option block option-primary">
                            @php $checked = !empty($exam['in_final_marksheet']) && ($exam['in_final_marksheet'] == 1) ? true : false; @endphp
                            {!! Form::checkbox('in_final_marksheet','',$checked, ['class' => 'gui-input ', 'id' => 'in_final_marksheet']) !!}
                            <span class="checkbox radioBtnpan"></span><em>{{trans('language.in_final_marksheet')}} </em>
                        </label>
                    </div>
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan"></span></label>
                    <label class="field select">
                        {!! Form::submit($submit_button, ['class' => 'button btn-primary','style'=>'width:100px;margin-top: 6px;']) !!}
                    </label> 
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#create-exam-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                exam_name: {
                    required: true,
                    // nowhitespace: true
                },
                session_id: {
                    required: true
                },
                exam_category_id: {
                    required: true
                },
                exam_nature: {
                    required: true
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
    });
</script>