<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.exam_category')}}</li>
                </ol>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages')
        @endif
        <div class="panel-body bg-light">
            <!-- .section-divider -->
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{ trans('language.exam_category')}}<span class="asterisk">*</span></span></label>
                    <label for="exam_category" class="field prepend-icon">
                        {!! Form::text('exam_category', old('exam_category',isset($exam_category['exam_category']) ? $exam_category['exam_category'] : ''), ['class' => 'gui-input','id' => 'exam_category','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="exam_category" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('exam_category')) 
                    <p class="help-block">{{ $errors->first('exam_category') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{ trans('language.exam_category_alias')}}</span></label>
                    <label for="exam_category_alias" class="field prepend-icon">
                        {!! Form::text('exam_category_alias', old('exam_category_alias',isset($exam_category['exam_category_alias']) ? $exam_category['exam_category_alias'] : ''), ['class' => 'gui-input','id' => 'exam_category_alias','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="exam_category_alias" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                </div>
                <div class="col-md-4">
                    <label><span class="radioBtnpan"></span></label>
                    <label for="" class="field prepend-icon">
                        {!! Form::submit($submit_button, ['class' => 'button btn-primary','style'=>'width:100px;margin-top: 6px;']) !!}
                        <label for="" class="field-icon">

                        </label>
                    </label>
                </div>
            </div>
            <!-- end .section row section -->
        </div>
    </div>
</div>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" class="" title="">{{ trans('language.exam_category')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="exam-category-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.exam_category') !!}</th>
                            <th>{!!trans('language.exam_category_alias') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .admin-form .radio:before{
        margin: 5px;
    }
</style>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var table = $('#exam-category-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('exam-category/data') }}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'exam_category', name: 'exam_category'},
                {data: 'exam_category_alias', name: 'exam_category_alias'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('exam-category/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'exam_category_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();

                                        } 
                                        else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });

        $("#exam-category-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                exam_category: {
                    required: true
                },
            },

            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });
</script>