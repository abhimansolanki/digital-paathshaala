<!-- Field Options -->
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_section')}}</li>
                </ol>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages')
        @endif
        <div class="bg-light panelBodyy">

            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.section_name') !!}<span class="asterisk">*</span></span></label>
                    <label for="section_name" class="field prepend-icon">
                        {!! Form::text('section_name', old('section_name',isset($section['section_name']) ? $section['section_name'] : ''), ['class' => 'gui-input', 'id' => 'section_name','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="section_name" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('section_name')) <p class="help-block">{{ $errors->first('section_name') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan"></span></label>
                    <label for="section_name" class="field prepend-icon">
                        {!! Form::submit($submit_button, ['class' => 'button btn-primary','style'=>'width:100px;margin-top: 6px;']) !!}   
                    </label>

                </div>
            </div>
        </div>
        <!-- end .form-body section -->
    </div>
    <!-- end .form-footer section -->
</div>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_section')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="section-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Section ID</th>
                            <th>{!!trans('language.section_name') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#section-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('section/data')}}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'section_id', name: 'section_id'},
                {data: 'section_name', name: 'section_name'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('section/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'section_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();

                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function () {

        $("#section-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                class_id: {
                    required: true
                },
                section_name: {
                    required: true,
                    // nowhitespace: true
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });
</script>