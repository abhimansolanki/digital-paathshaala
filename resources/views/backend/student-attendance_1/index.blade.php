@extends('admin_panel/layout')
@section('content')
<style>
    .btn-bs-file input[type="file"]{
        position: absolute;
        top: -9999999;
        filter: alpha(opacity=0);
        opacity: 0;
        width:0;
        height:0;
        outline: none;
        cursor: inherit;
    }
    .btn{
        margin-top: 0px !important;
    }
    .btncustom{
        border: 1px solid #ccc;
        padding: 0px 0px
    }
    .save-button{
        width: 100px;
        margin-top: 30px;
        height: 33px;
        border: none;
    }
</style>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.view_attendance')}}</span>
                </div>
                <div class="topbar-right">
                    <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right custom-btn">{{trans('language.add_attendance')}}</a>
                </div>
            </div>
             @include('backend.partials.messages')
            {{-- <div class="panel" id="transportId">
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        {!! Form::open(['url'=>'','id'=>'import-employee','files'=>true]) !!}
                        <div class="row">
                            <div class="col-md-3">
                                <label>
                                    <span class="radioBtnpan">{!! trans('language.import_employee') !!}<span class="asterisk">*</span>
                                    </span>
                                </label>
                                <label class="btn-bs-file btn btn-xs col-md-12 btncustom">
                                    <div class="col-md-6 classfontsize">
                                        <span class="glyphicons glyphicons-upload"></span>
                                        {!! trans('language.upload_excel') !!}
                                    </div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-4 fontsizeand">
                                        <span class="glyphicons glyphicons-search"></span>  Browse file </div>
                                    {!! Form::file('import_employee'); !!}
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label><span class="radioBtnpan"></span></label>
                                <label class="field select">
                                    {!! Form::submit('Save', ['class' => 'button btn-primary save-button']) !!}
                                </label> 
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div> --}}
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="attendance-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
		    <th>{{trans('language.attendance_date')}}</th>
                            <th>{{trans('language.class_name')}}</th>
                            <th>{{trans('language.section_name')}}</th>
                            <th>{{trans('language.total_present')}}</th>
                            <th>{{trans('language.total_absent')}}</th>
                            <!--<th>{{trans('language.on_leave')}}</th>-->
                            <th class="text-center">{{trans('language.action')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#attendance-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('student-attendance-data')}}",
            columns: [
                {data: 'attendance_date', name: 'attendance_date'},
                {data: 'class_name', name: 'class_name'},
                {data: 'section_name', name: 'section_name'},
                {data: 'total_present', name: 'total_present'},
                {data: 'total_absent', name: 'total_absent'},
//                {data: 'on_leave', name: 'on_leave'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });

</script>
</body>
</html>
@endsection

