@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
    .display-icon{
        width: 15px; 
        height: 15px;
        border-radius: 100%;
        margin: 10px;
        float:left;
    }
    .display-text{
        margin-top: 7px;
        float:left;
    }
    .custom-table {
        font-size: 11px !important;   
    }
    .table > thead > tr > th{
        padding:4px !important;
        text-align: center !important;
    }
    .overlay {
        background-color:#EFEFEF;
        /*position: fixed;*/
        width: 100%;
        height: 100%;
        z-index: 1000;
        top: 0px;
        left: 0px;
        opacity: .5; /* in FireFox */ 
        filter: alpha(opacity=50); /* in IE */
    }
</style>
@php $current_segment2 = Request::segment(3); @endphp
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel" id="transportId">
                <!--<span style="font-size:18px;">Student Attendance Register</span>-->
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        <div id="tab1_1" class="">
                            <div class="row">
                                {!! Form::open(['id' => 'attendance-register-form' , 'class'=>'form-horizontal','url' => '','autocomplete'=>'off']) !!}
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('class_id', $arr_class,'', ['class' => 'form-control','id'=>'class_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="section">
                                        <label class="field select" style="width: 100%;">
                                            {!!Form::select('section_id', $arr_section,'', ['class' => 'form-control','id'=>'section_id'])!!}
                                            <i class="arrow double"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="field select" style="width: 100%;">
                                        {!! Form::select('table_view',$arr_table_view,$current_segment2, ['class' =>'form-control','id'=>'table-view', 'readonly' =>true]) !!}
                                    </label>
                                </div>
                                {!! Form::hidden('file_title',date('F, Y'), ['class' =>'','id'=>'file-title', 'readonly' =>true]) !!}
                                @if($current_segment2 !='student-wise')
                                <div class="col-md-2">
                                    <label class="field select" style="width: 100%;">
                                        {!! Form::text('attendance_calendar',date('D F d, Y'), ['class' => 'form-control date_piker','id' => 'attendance_calendar', 'readonly' => 'readonly','placeholder'=>'Select Calendar']) !!}
                                    </label>
                                </div>
                                @endif
                                <div class="clearfix"></div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2" style="width: 8.666667% !important">
                    <span class="display-icon" style=" background-color:greenyellow; "></span>
                    <span class="display-text">Holiday</span>
                </div>
                <div class="col-md-2" style="width: 8.666667% !important">
                    <span class="display-icon" style=" background-color:blue; "></span>
                    <span class="display-text">Present</span>
                </div>
                <div class="col-md-2" style="width: 8.666667% !important">
                    <span class="display-icon" style=" background-color:red; "></span>
                    <span class="display-text">Absent</span>
                </div>
                <div class="col-md-2" style="width: 8.666667% !important">
                    <span class="display-icon" style=" background-color:yellow; "></span>
                    <span class="display-text">Leave</span>
                </div>
            </div>
            <div class="panel-heading" style="font-size: 14px; font-weight: bold">
                <div class="col-md-4">
                    <span style="" id="class-name"></span>
                    <span style="" id="section-name"></span>&nbsp;
                </div>
                <div class="col-md-4">
                    <span style="text-align:center">Student Attendance Register</span>
                </div>
                <div class="col-md-4" style="text-align:right">
                    <span style="" id="month-year">{!! date('F') !!}</span> &nbsp;<span>{!! $session['session_year'] !!}</span>
                </div>
            </div>
            <div class="panel-body pn" style="border-top:1px solid #e5e5e5 !important;">
                <div style="overflow:auto;">
                    <table class="table table-bordered table-striped table-hover custom-table" id="attendance-register-table" cellspacing="0" width="100%">
                        <thead>
                            <tr id="thead-tr">
                            </tr>
                        </thead>
                    </table>
                </div>
                @if($current_segment2 == 'month' || $current_segment2 =='week')
                <div style="overflow:auto;margin-bottom: 50px;">
                    <table class="table table-bordered table-striped table-hover" id="register-summary" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                            </tr>
                        </thead>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var arr_col = '{!! json_encode($month_calendar) !!}';
        arr_col = $.parseJSON(arr_col);
        tableHeader(arr_col);
        function tableHeader(arr_col, c_date = null)
        {
            var table_th = '';
            var dynamic_col = [];
            jQuery.each(arr_col, function () {
                if (this['key'] !== 'student_id')
                {
                    dynamic_col.push({data: '' + this['key'] + '', name: '' + this['key'] + '', orderable: false, searchable: false});
                    if ($.isNumeric(this['key']))
                    {
                        table_th = table_th + '<th>' + this['key'] + '<br>' + this['val'] + '</th>';
                    } else
                    {
                        table_th = table_th + '<th>' + this['val'] + '</th>';
                    }
                }
            });

            $("#thead-tr").html(table_th);
            studentAttendance(dynamic_col, c_date);
        }

        function studentAttendance(dynamic_col, c_date = null)
        {
            var table = $('#attendance-register-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                pageLength: 100,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": $("#class_id option:selected").text() + ',' + $("#file-title").val(),
                        "filename": 'student-attendance-register',
//                        exportOptions: {
//                            modifier: {
//                                selected: true
//                            }
//                        }
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": $("#class_id option:selected").text() + ',' + $("#file-title").val(),
                        "filename": 'student-attendance-register',
//                        exportOptions: {
//                            modifier: {
//                                selected: true
//                            }
//                        }
                    }
                ],
//                select: {
//                    style: 'multi',
//                    selector: 'td:first-child'
//                },
//                'columnDefs': [
//                    {
//                        'targets': 0,
//                        'className': 'select-checkbox',
//                        'checkboxes': {
//                            'selectRow': true
//                        }
//                    }
//                ],
                ajax: {
                    url: '{{ url('attendance_register')}}',
                    data: function (f) {
                        f.class_id = $('#class_id').val();
                        f.section_id = $('#section_id').val();
                        f.c_date = c_date;
                        f.view_type = $("#table-view").val()
                    },
                    method: 'POST'
                },
                columns: dynamic_col,
                rowCallback: function (row, data, index) {
                    $.each(data, function (key, val) {
                        console.log(index);
                        if (val === "Tot. Scholars" || val === "Tot. Attendance" || val === "Tot. Absence" || val === "Tot. Leave") {
                            var td_index = table.column('' + key + ':name').index();
                            //fontWeight:'bold', borderBottom: "1px solid pink"
                            $(row).find('td:eq("' + td_index + '")').css({color: "black"});
                        }
                        if (val === 'H') {
                            var td_index = table.column('' + key + ':name').index();
                            $(row).find('td:eq("' + td_index + '")').css({backgroundColor: "greenyellow", borderBottom: "1px solid pink"});
                        }
                        if (val === 'W') {
                            var td_index = table.column('' + key + ':name').index();
                            $(row).find('td:eq("' + td_index + '")').css({backgroundColor: "pink", borderBottom: "1px solid pink"});
                        }
                        if (val === 'A') {
                            var td_index = table.column('' + key + ':name').index();
                            $(row).find('td:eq("' + td_index + '")').css({color: "red"});
                        }
                        if (val === 'P') {
                            var td_index = table.column('' + key + ':name').index();
                            $(row).find('td:eq("' + td_index + '")').css({color: "blue"});
                        }
                        if (val === 'L') {
                            var td_index = table.column('' + key + ':name').index();
                            $(row).find('td:eq("' + td_index + '")').css({backgroundColor: "rgb(255, 255, 0)"});
                        }

                    });
                },
//                columnDefs: [
//                {
//                    "targets": 2, // your case first column
//                    "width": "90%"
//                }
//            ]
            });
            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

//            $(".buttons-excel").prop('disabled', true);
//            $(".buttons-print").prop('disabled', true);
//            table.on('select deselect', function (e, dt, type, indexes) {
//                var arr_checked_student = checkedStudent();
//                if (arr_checked_student.length > 0)
//                {
//                    $(".buttons-excel").prop('disabled', false);
//                    $(".buttons-print").prop('disabled', false);
//                } else
//                {
//                    $(".buttons-excel").prop('disabled', true);
//                    $(".buttons-print").prop('disabled', true);
//                }
//            });
        }

//        function checkedStudent()
//        {
//            var arr_checked_student = [];
//            $.each($('#attendance-register-table').DataTable().rows().data(), function () {
//                console.log(this);
//                arr_checked_student.push(this["student_id"]);
//            });
//            return arr_checked_student;
//        }

        function getClassSection()
        {
            $('select[name="section_id"]').empty();
            $('select[name="section_id"]').append('<option value="">--Select section--</option>');
            var class_id = $("#class_id").val();
            if (class_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $.each(resopose_data, function (key, value) {
                                $('select[name="section_id"]').append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                    }
                });
            }
        }
        $("#attendance_calendar").datepicker('destroy');
        $(function () {
            var i = $("#attendance_calendar").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                dateFormat: "D MM dd, yy",
                minDate: '{!! $session["start_date"] !!}',
                maxDate: '{!! $session["end_date"] !!}',
                onChangeMonthYear: function (year, month, inst) {
//                    if ($("#attendance-register-form").valid())
//                    {
                    var n = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    var month_num = parseInt(month) - 1;
                    var file_name = n[month_num] + ', ' + year;
                    $("#file-title").val(file_name);
                    $("#month-year").text(n[month_num]);
                    var view = $("#table-view").val();
                    if (view === 'month')
                    {
                        var c_date = year + '-' + month + '-' + '01';
                        var a = new Date(c_date);
                        var weekday = new Array(7);
                        weekday[0] = "Sun";
                        weekday[1] = "Mon";
                        weekday[2] = "Tue";
                        weekday[3] = "Wed";
                        weekday[4] = "Thu";
                        weekday[5] = "Fri";
                        weekday[6] = "Sat";
                        tableStructure(c_date);
                        var day = weekday[a.getDay()];
                        $("#attendance_calendar").val(day + ' ' + n[month_num] + ' ' + '01, ' + year);
                        RegisterSummary(summary_dynamic_col, c_date);
//                        }
                    }
                },
                onSelect: function (c_date, inst) {
                    var view = $("#table-view").val();
                    if (view === 'week')
                    {
                        tableStructure(c_date);
                        RegisterSummary(summary_dynamic_col, c_date);
                    }
                }
            });
        });

        $(document).on('change', '#class_id,#section_id', function (e) {
            $("#section-name").text('');
            if ($(this).attr("id") === "class_id")
            {
                getClassSection();
                $("#class-name").text('');
                if ($(this).val() !== "")
                {
                    $("#class-name").text('Class - ' + $("#class_id option:selected").text());
                }
            } else
            {
                if ($(this).val() !== "")
                {
                    $("#section-name").text(' / Section - ' + $("#section_id option:selected").text());
                }
            }
            var c_date = $("#attendance_calendar").val();
            tableStructure(c_date);
            RegisterSummary(summary_dynamic_col, c_date)
        });
        $(document).on('change', '#table-view', function (e) {
            var view_type = $(this).val();
            var url = '{!! url("admin/attendance-register/' + view_type + '") !!}';
            window.location.replace(url);
        });

        function tableStructure(c_date = '') {
            if (c_date !== '')
            {
                var d = new Date(c_date),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();
                if (month.length < 2)
                    month = '0' + month;
                if (day.length < 2)
                    day = '0' + day;

                c_date = [year, month, day].join('-');
            }
            var view_type = $("#table-view").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{!! url('attendance-table-header/" + view_type + "/" + c_date + "') !!}",
                datatType: 'json',
                type: 'GET',
                success: function (response) {
                    tableHeader(response, c_date);

                }
            });
        }


        // second table
        var arr_col = '{!! json_encode($arr_th) !!}';
        var arr_col = $.parseJSON(arr_col);
        var summary_dynamic_col = [];
        summaryTableHeader(arr_col);
        function summaryTableHeader(arr_col, c_date = null)
        {
            var table_th = '';
            summary_dynamic_col = [];
            jQuery.each(arr_col, function (key, val) {
                summary_dynamic_col.push({data: '' + key + '', name: '' + key + '', orderable: false, searchable: false});
                table_th = table_th + '<th>' + val + '</th>';

            });
            $("#register-summary thead tr").html(table_th);
            RegisterSummary(summary_dynamic_col, c_date);
        }
        function RegisterSummary(summary_dynamic_col, c_date)
        {
           $('#register-summary').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                bFilter:false,
                bPaginate:false,
                lengthChange: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": $("#class_id option:selected").text() + ',' + $("#file-title").val(),
                        "filename": 'student-attendance-register',
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": $("#class_id option:selected").text() + ',' + $("#file-title").val(),
                        "filename": 'student-attendance-register',
                    }
                ],

                ajax: {
                    url: '{{ url('attendance_register_summary')}}',
                    data: function (f) {
                        f.class_id = $('#class_id').val();
                        f.section_id = $('#section_id').val();
                        f.c_date = c_date;
                        f.view_type = $("#table-view").val()
                    },
                },
                columns:summary_dynamic_col,
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'margin-bottom': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});
        }

    });
</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



