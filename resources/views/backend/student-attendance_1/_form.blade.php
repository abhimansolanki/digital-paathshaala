<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_attendance')}}</li>
                </ol>
            </div>
            <div class="topbar-right">
                <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right">{{trans('language.view_attendance')}}</a>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages')
        @endif
        <div class="bg-light panelBodyy">
            <!-- .section-divider -->
            {!! Form::hidden('student_attendance_id',old('student_attendance_id',isset($attendance['student_attendance_id']) ? $attendance['student_attendance_id'] : null),['class' => 'gui-input', 'id' => 'student_attendance_id', 'readonly' => 'true']) !!}
            {!! Form::hidden('session_id',old('session_id',isset($attendance['session']['session_id']) ? $attendance['session']['session_id'] : null),['class' => 'gui-input', 'id' => 'session_id', 'readonly' => 'true']) !!}
            <div class="section-divider mv40">
                <span><i class="fa fa-graduation-cap"></i>&nbsp;Student's Attendance</span>
            </div>
            <div class="section row" id="spy1">
	    <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.attendance_date')}}<span class="asterisk">*</span></span></label>
                    <label for="attendance_date" class="field prepend-icon">
                        {!! Form::text('attendance_date', old('attendance_date',isset($attendance['attendance_date']) ? $attendance['attendance_date'] : date('d/m/Y')), ['class' => 'gui-input','id' => 'attendance_date', 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('attendance_date')) <p class="help-block">{{ $errors->first('attendance_date') }}</p> @endif
                </div>
	    <div class="col-md-2">
                    <label><span class="radioBtnpan">{{ trans('language.class')}}<span class="asterisk">*</span></span></label>
                    <label for="class_id" class="field select">
                        {!!Form::select('class_id', $attendance['arr_class'],isset($attendance['class_id']) ? $attendance['class_id'] : '', ['class' => 'form-control','id'=>'class_id'])!!}
                    </label>
                    @if ($errors->has('class_id')) 
                    <p class="help-block">{{ $errors->first('class_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{ trans('language.section')}}<span class="asterisk">*</span></span></label>
                    <label for="section_id" class="field select">
                        {!!Form::select('section_id', $attendance['arr_section'],isset($attendance['section_id']) ? $attendance['section_id'] : '', ['class' => 'form-control','id'=>'section_id'])!!}
                    </label>
                    @if ($errors->has('section_id')) 
                    <p class="help-block">{{ $errors->first('section_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.total_present')}}<span class="asterisk">*</span></span></label>
                    <label for="total_present" class="field prepend-icon">
                        {!! Form::text('total_present', old('total_present',isset($attendance['total_present']) ? $attendance['total_present'] : null), ['class' => 'gui-input', 'id' => 'total_present', 'readonly']) !!}
                        <label for="total_present" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('total_present')) <p class="help-block">{{ $errors->first('total_present') }}</p> @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.total_absent')}}<span class="asterisk">*</span></span></label>
                    <label for="total_absent" class="field prepend-icon">
                        {!! Form::text('total_absent', old('total_absent',isset($attendance['total_absent']) ? $attendance['total_absent'] : null), ['class' => 'gui-input', 'id' => 'total_absent', 'readonly']) !!}
                        <label for="total_absent" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('total_absent')) <p class="help-block">{{ $errors->first('total_absent') }}</p> @endif
                </div>
            </div>
        </div>
        <div class="section-divider mb40" id="spy1">
            <span>Students</span>
        </div>
        <div class="panel-body pn" id="student_list">
	{!! isset($attendance['student_list']) ? $attendance['student_list'] :'' !!}
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit('Save', ['class' => 'button btn-primary','name'=>'submit']) !!}
        </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        if ($("#student_attendance_id").val() !== '')
        {
            $('#class_id option:not(:selected)').attr('disabled', true);
            $('#section_id option:not(:selected)').attr('disabled', true);
        }
        $("#attendance_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            minDate: '{!! $attendance["session"]["session_start_date"] !!}',
            maxDate: '{!! $attendance["session"]["session_end_date"] !!}',
            dateFormat: "dd/mm/yy",
        });
        $("#student-attendance-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                class_id: {
                    required: true,
                },
                section_id: {
                    required: true,
                },
                total_present: {
                    required: true,
                    min: 0
                },
                attendance_date: {
                    required: true,
                },
                total_absent: {
                    required: true,
                    min: 0
                },
                'arr_student_id[]': {
                    required: true,
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || !/[~`!@#$%\^&*()+=\-\_\.\[\]\\';,/{}|\\":<>\?]/.test(value) || /^[a-zA-Z][a-zA-Z0-9\s\.]+$/i.test(value);
        }, "Please enter only alphabets(more than 1)");

        $(document).on('change', '.attenance-status', function () {
            var total_present = 0;
            var total_absent = 0;
            $('.attenance-status').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
                if (sThisVal == 1) {
                    total_present++;
                } else if (sThisVal == 2) {
                    total_absent++;
                }

            });
            $('#total_present').val(total_present);
            $('#total_absent').val(total_absent);
        });

        $(document).on('change', '#class_id', function (e) {
            e.preventDefault();
            var class_id = $('#class_id').val();
            var session_id = $('#session_id').val();
            $('#section_id').empty();
            if (class_id !== '' && session_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                        'session_id': session_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        var resopose_data = [];
                        resopose_data = response.data;
		$("#LoadingImage").hide();
                        if (response.status === 'success')
                        {
                            $('#section_id').append('<option value="">-- Select section --</option>');
                            $.each(resopose_data, function (key, value) {
                                $('#section_id').append('<option value="' + key + '">' + value + '</option>');
                            });
                            
                        }
                    }
                });
            }

        });

        // get stuedent list
        $(document).on('change', '#class_id, #section_id', function (e) {
            e.preventDefault();
            var class_id = $('#class_id').val();
            var session_id = $('#session_id').val();
            var section_id = $('#section_id').val();
            if (class_id !== '' && session_id !== '' && section_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-student-table')}}",
                    datatType: 'json',
                    type: 'GET',
                    data: {
                        'session_id': session_id,
                        'class_id': class_id,
                        'section_id': section_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        $("#student_list").html(response.data);
                    }
                });
            }

        });

    });
</script>
