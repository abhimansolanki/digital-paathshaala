@extends('admin_panel/layout')
@section('content')
<style>
    .btn-bs-file input[type="file"]{
        position: absolute;
        top: -9999999;
        filter: alpha(opacity=0);
        opacity: 0;
        width:0;
        height:0;
        outline: none;
        cursor: inherit;
    }
    .btn{
        margin-top: 0px !important;
    }
    .btncustom{
        border: 1px solid #ccc;
        padding: 0px 0px
    }
    .save-button{
        min-width: 100px;
        margin-top: 30px;
        height: 33px;
        border: none;
    }
</style>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_employee')}}</span>
                </div>
                <div class="topbar-right">
                    <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right custom-btn">{{trans('language.add_employee')}}</a>
                </div>
            </div>
            @include('backend.partials.messages')
<!--            <div class="panel">
                <div class="panel-body">
                    <div class="tab-content  br-n">
                        {!! Form::open(['url'=>$import_url,'id'=>'import-employee','files'=>true]) !!}
                        <div class="row">
                            <div class="col-md-3">
                                <label>
                                    <span class="radioBtnpan">{!! trans('language.import_employee') !!}<span class="asterisk">*</span>
                                    </span>
                                </label>
                                <label class="btn-bs-file btn btn-xs col-md-12 btncustom">
                                    <div class="col-md-6 classfontsize">
                                        <span class="glyphicons glyphicons-upload"></span>
                                        {!! trans('language.upload_excel') !!}
                                    </div>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-4 fontsizeand">
                                        <span class="glyphicons glyphicons-search"></span>  Browse file </div>
                                    {!! Form::file('import_employee'); !!}
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label><span class="radioBtnpan"></span></label>
                                <label class="field select">
                                    {!! Form::submit('Save', ['class' => 'button btn-primary save-button']) !!}
                                </label> 
                            </div>
                            <div class="pull-right mr10">
                                <label><span class="radioBtnpan"></span></label>
                                <label class="field select">
                                    {!! Form::button('Download Sample File', ['class' => 'button btn-primary save-button download-sample']) !!}
                                </label> 
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>-->
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="employee-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{trans('language.employee_code')}}</th>
                            <th>{{trans('language.department')}}</th>
                            <th>{{trans('language.user_role')}}</th>
                            <th>{{trans('language.full_name')}}</th>
                            <th>{{trans('language.dob')}}</th>
                            <th>{{trans('language.gender')}}</th>
                            <th>{{trans('language.email')}}</th>
                            <th>{{trans('language.contact_number')}}</th>
                            <th>{{trans('language.address')}}</th>
                            <th class="text-center">{{trans('language.action')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#employee-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('employee/data')}}",
            columns: [
                {data: 'employee_code', name: 'employee_code'},
                {data: 'department_name', name: 'department'},
                {data: 'user_role', name: 'user_role'},
                {data: 'full_name', name: 'full_name'},
                {data: 'dob', name: 'dob'},
                {data: 'gender_name', name: 'gender_name'},
                {data: 'email', name: 'email'},
                {data: 'contact_number', name: 'contact_number'},
                {data: 'address', name: 'address'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");
            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        $.ajax({
                            url: "{{ url('employee/delete/') }}",
                            datatType: 'json',
                            type: 'POST',
                            data: {
                                'employee_id': id,
                            },
                            success: function (res)
                            {
                                console.log(res);
                                if (res.status == "success")
                                {
                                    table.ajax.reload();
                                } else if (res.status === "used")
                                {
                                    $('#server-response-message').text(res.message);
                                    $('#alertmsg-student').modal({
                                        backdrop: 'static',
                                        keyboard: false
                                    });
                                }
                            }
                        });
                    }
                }
            });

        });


        //validate file
        $("#import-employee").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                import_employee: {
                    required: true,
                    extension: true,
                },

            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        jQuery.validator.addMethod("extension", function (a, b, c) {
            return c = "string" == typeof c ? c.replace(/,/g, "|") : "xls|xlsx", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
        }, jQuery.validator.format("Only valid formats(xls|xlsx) are allowed"));

        $('.download-sample').click(function(){
            window.open('{{ url("public/sample/sample-file.xlsx") }}');
        });
    });

</script>
@endsection

