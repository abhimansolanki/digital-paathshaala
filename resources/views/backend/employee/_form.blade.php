<style>
    .btn-bs-file input[type="file"]{
        position: absolute;
        top: -9999999;
        filter: alpha(opacity=0);
        opacity: 0;
        width:0;
        height:0;
        outline: none;
        cursor: inherit;
    }
    .btn{
        margin-top: 0px !important;
    }
    .btncustom{
        border: 1px solid #ccc;
        padding: 0px 0px
    }
</style>
@if(isset($employee['employee_id']) && !empty($employee['employee_id']))
@php $readonly = true;  $disabled = 'disabled'; @endphp
@else
@php $readonly = false;$disabled = ''; @endphp
@endif
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_employee')}}</li>
                </ol>
            </div>
            <div class="topbar-right">
                <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right">{{trans('language.view_employee')}}</a>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages')
        @endif
        <div class="bg-light panelBodyy">
            <!-- .section-divider -->
            {!! Form::hidden('employee_id',old('employee_id',isset($employee['employee_id']) ? $employee['employee_id'] : ''),['class' => 'gui-input', 'id' => 'employee_id', 'readonly' => 'true']) !!}
            <div class="section-divider mv40">
                <span><i class="fa fa-graduation-cap"></i>&nbsp;Personal Information</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.employee_code')}}<span class="asterisk">*</span></span></label>
                    <label for="employee_code" class="field prepend-icon">
                        {!! Form::text('employee_code', old('employee_code',isset($employee['employee_code']) ? $employee['employee_code'] : ''), ['class' => 'gui-input','id' => 'employee_code', 'readonly'=>true]) !!}
                        <label for="employee_code" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('employee_code')) <p class="help-block">{{ $errors->first('employee_code') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.full_name')}}<span class="asterisk">*</span></span></label>
                    <label for="full_name" class="field prepend-icon">
                        {!! Form::text('full_name', old('full_name',isset($employee['full_name']) ? $employee['full_name'] : ''), ['class' => 'gui-input', 'id' => 'full_name']) !!}
                        <label for="full_name" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('full_name')) <p class="help-block">{{ $errors->first('full_name') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.father_husband_name')}}<span class="asterisk">*</span></span></label>
                    <label for="father_name" class="field prepend-icon">
                        {!! Form::text('father_name', old('father_name',isset($employee['father_name']) ? $employee['father_name'] : ''), ['class' => 'gui-input','id' => 'father_name']) !!}
                        <label for="father_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('father_name')) <p class="help-block">{{ $errors->first('father_name') }}</p> @endif                         
                </div>

                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.dob')}}<span class="asterisk">*</span></span></label>
                    <label for="dob" class="field prepend-icon">
                        {!! Form::text('dob', old('dob',isset($employee['dob']) ? $employee['dob'] : ''), ['class' => 'gui-input','id' => 'dob', 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('dob')) <p class="help-block">{{ $errors->first('dob') }}</p> @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.email')}}<span class="asterisk">*</span></span></label>
                    <label for="email" class="field prepend-icon">
                        {!! Form::email("email", old('email',isset($employee['email']) ? $employee['email'] : ''), ["class" => "gui-input","id" => "email",'pattern'=>'^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$']) !!}
                        <label for="email" class="field-icon">
                            <span class="glyphicons glyphicons-message_empty"></span>
                        </label>
                    </label>
                    @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.contact_number')}}<span class="asterisk">*</span></span></label>
                    <label for="contact_number" class="field prepend-icon">
                        {!! Form::number("contact_number", old('contact_number',isset($employee['contact_number']) ? $employee['contact_number'] : ''), ["class" => "gui-input","id" => "contact_number",'pattern'=>'[0-9]*']) !!}
                        <label for="contact_number" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('esi_number')) <p class="help-block">{{ $errors->first('esi_number') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.aadhaar_number')}}<span class="asterisk">*</span></span></label>
                    <label for="aadhaar_number" class="field prepend-icon">
                        {!! Form::number("aadhaar_number", old('aadhaar_number',isset($employee['aadhaar_number']) ? $employee['aadhaar_number'] : ''), ["class" => "gui-input","id" => "aadhaar_number",'pattern'=>'[0-9]*']) !!}
                        <label for="aadhaar_number" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('aadhaar_number')) <p class="help-block">{{ $errors->first('aadhaar_number') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.pan_number')}}</span></label>
                    <label for="pan_number" class="field prepend-icon">
                        {!! Form::text('pan_number', old('pan_number',isset($employee['pan_number']) ? $employee['pan_number'] : ''), ['class' => 'gui-input','id' => 'pan_number']) !!}
                        <label for="pan_number" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('pan_number')) <p class="help-block">{{ $errors->first('pan_number') }}</p> @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.caste_category')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('caste_category_id', $employee['arr_category'],isset($employee['caste_category_id']) ? $employee['caste_category_id'] : '', ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('caste_category_id')) <p class="help-block">{{ $errors->first('caste_category_id') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.gender')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('gender', $employee['arr_gender'],isset($employee['gender']) ? $employee['gender'] : '', ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('gender')) <p class="help-block">{{ $errors->first('gender') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.marital_status')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('marital_status', $employee['arr_marital'],isset($employee['marital_status']) ? $employee['marital_status'] : '', ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('marital_status')) <p class="help-block">{{ $errors->first('marital_status') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.profile_photo') !!}<span class="asterisk">*</span>
                            @if(isset($employee['profile_photo']) && !empty($employee['profile_photo']))
                            <a href="" data-toggle="modal" data-target="#img1" data-backdrop="static" data-keyboard="false">Click to view </a>
                            @endif
                        </span>
                    </label>
                    <label class="btn-bs-file btn btn-xs col-md-12 btncustom">
                        <div class="col-md-6 classfontsize">
                            <span class="glyphicons glyphicons-upload"></span>
                            {!! trans('language.profile_photo') !!}
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4 fontsizeand">
                            <span class="glyphicons glyphicons-search"></span>  Browse file </div>
                        {!! Form::file('profile_photo'); !!}
                    </label>
                    @if ($errors->has('profile_photo')) 
                    <p class="help-block">{{ $errors->first('profile_photo') }}</p>
                    @endif
                    <div id="img1" class="modal fade-scale" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    @if(isset($employee['profile_photo']) && !empty($employee['profile_photo']))
                                    <img src="{{url($employee['profile_photo'])}}" style="width: 100%">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-6">
                    <label><span class="radioBtnpan">{{trans('language.address')}}<span class="asterisk">*</span></span></label>
                    <label for="address" class="field prepend-icon">
                        {!! Form::textarea("address", old('address',isset($employee['address']) ? $employee['address'] : ''), ["class" => "gui-input","id" => "address","style"=>"height:80px"]) !!}
                        <label for="address" class="field-icon">
                            <span class="glyphicons glyphicons-google_maps"></span>
                        </label>
                    </label>
                    @if ($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif
                </div>
            </div>
            <div class="section-divider mv40">
                <span><i class="fa fa-user"></i>Professional Information</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.department')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('department_id', $employee['arr_department'],isset($employee['department_id']) ? $employee['department_id'] : null, ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('department_id')) <p class="help-block">{{ $errors->first('department_id') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.user_role')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('user_role_id',$employee['arr_user_role'],isset($employee['user_role_id']) ? $employee['user_role_id'] : null, ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('user_role_id')) <p class="help-block">{{ $errors->first('user_role_id') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.staff_type')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('staff_type_id',$employee['arr_staff_type'],isset($employee['staff_type_id']) ? $employee['staff_type_id'] : null, ['class' => 'form-control'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('staff_type_id')) <p class="help-block">{{ $errors->first('staff_type_id') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <span class="radioBtnpan">{{trans('language.join_date')}}<span class="asterisk">*</span></span>
                    <label for="join_date" class="field prepend-icon">
                        {!! Form::text('join_date', old('join_date',isset($employee['join_date']) ? $employee['join_date'] : ''), ['class' => 'gui-input date_picker ','id' => 'join_date', 'readonly']) !!}
                        <label for="join_date" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('join_date')) <p class="help-block">{{ $errors->first('join_date') }}</p> @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.bank')}}<span class="asterisk">*</span></span></label>
                    <label class="field prepend-icon">
                        {!!Form::text('bank_name', old('bank_name',isset($employee['bank_name']) ? $employee['bank_name'] : ''),  ['class' => 'gui-input'])!!}
                        <label for="" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if($errors->has('bank_name')) <p class="help-block">{{ $errors->first('bank_name') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.ifsc_code')}}<span class="asterisk">*</span></span></label>
                    <label class="field prepend-icon">
                        {!!Form::text('ifsc_code', old('ifsc_code',isset($employee['ifsc_code']) ? $employee['ifsc_code'] : ''),  ['class' => 'gui-input','pattern'=>'[a-zA-Z0-9]*'])!!}
                        <label for="" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if($errors->has('ifsc_code')) <p class="help-block">{{ $errors->first('ifsc_code') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.account_number')}}<span class="asterisk">*</span></span></label>
                    <label class="field prepend-icon">
                        {!!Form::number('account_number', old('account_number',isset($employee['account_number']) ? $employee['account_number'] : ''),  ['class' => 'gui-input','pattern'=>'[0-9]*'])!!}
                        <label for="" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if($errors->has('account_number')) <p class="help-block">{{ $errors->first('account_number') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.medical_leave')}}</span></label>
                    <label for="medical_leave" class="field prepend-icon">
                        {!! Form::number("medical_leave", old('medical_leave',isset($employee['medical_leave']) ? $employee['medical_leave'] : ''), ["class" => "gui-input","id" => "medical_leave"]) !!}
                        <label for="medical_leave" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('medical_leave')) <p class="help-block">{{ $errors->first('medical_leave') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.paid_leave_pm')}}</span></label>
                    <label for="paid_leave_pm" class="field prepend-icon">
                        {!! Form::number("paid_leave_pm", old('paid_leave_pm',isset($employee['paid_leave_pm']) ? $employee['paid_leave_pm'] : ''), ["class" => "gui-input","id" => "paid_leave_pm"]) !!}
                        <label for="paid_leave_pm" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('paid_leave_pm')) <p class="help-block">{{ $errors->first('paid_leave_pm') }}</p> @endif
                </div> 
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.casual_leave_pm')}}</span></label>
                    <label for="casual_leave_pm" class="field prepend-icon">
                        {!! Form::number("casual_leave_pm", old('casual_leave_pm',isset($employee['casual_leave_pm']) ? $employee['casual_leave_pm'] : ''), ["class" => "gui-input","id" => "casual_leave_pm"]) !!}
                        <label for="casual_leave_pm" class="field-icon">
                            <i class="fa fa-mobile"></i>
                        </label>
                    </label>
                    @if ($errors->has('casual_leave_pm')) <p class="help-block">{{ $errors->first('casual_leave_pm') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.tds_deducation')}}</span></label>
                    <label for="tds_deducation" class="field prepend-icon">
                        {!! Form::number("tds_deducation", old('school_name',isset($employee['tds_deducation']) ? $employee['tds_deducation'] : ''), ["class" => "gui-input", "id" => "tds_deducation"]) !!}
                        <label for="tds_deducation" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('tds_deducation')) <p class="help-block">{{ $errors->first('tds_deducation') }}</p> @endif
                </div>
            </div>
            <div class="section-divider mv40">
                <span><i class="fa fa-graduation-cap"></i>Qualification/Experience</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-6">
                    <label><span class="radioBtnpan">{{trans('language.qualification')}}<span class="asterisk">*</span></span></label>
                    <label for="qualification" class="field prepend-icon">
                        {!! Form::textarea("qualification", old('qualification',isset($employee['qualification']) ? $employee['qualification'] : ''), ["class" => "gui-input","id" => "qualification","style"=>"height:80px"]) !!}
                    </label>
                    @if ($errors->has('qualification')) <p class="help-block">{{ $errors->first('qualification') }}</p> @endif
                </div>
                <div class="col-md-6">
                    <label><span class="radioBtnpan">{{trans('language.specialization')}}</span></label>
                    <label for="specialization" class="field prepend-icon">
                        {!! Form::textarea("specialization", old('specialization',isset($employee['specialization']) ? $employee['specialization'] : ''), ["class" => "gui-input","id" => "specialization","style"=>"height:80px"]) !!}
                    </label>
                    @if ($errors->has('specialization')) <p class="help-block">{{ $errors->first('specialization') }}</p> @endif
                </div>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-6">
                    <label><span class="radioBtnpan">{{trans('language.experience')}}</span></label>
                    <label for="experience" class="field prepend-icon">
                        {!! Form::textarea("experience", old('experience',isset($employee['experience']) ? $employee['experience'] : ''), ["class" => "gui-input","id" => "experience","style"=>"height:80px"]) !!}
                    </label>
                    @if ($errors->has('experience')) <p class="help-block">{{ $errors->first('experience') }}</p> @endif
                </div>
                <div class="col-md-6">
                    <label><span class="radioBtnpan">{{trans('language.reference')}}</span></label>
                    <label for="reference" class="field prepend-icon">
                        {!! Form::textarea("reference", old('reference',isset($employee['reference']) ? $employee['reference'] : ''), ["class" => "gui-input","id" => "reference","style"=>"height:80px"]) !!}
                    </label>
                    @if ($errors->has('reference')) <p class="help-block">{{ $errors->first('reference') }}</p> @endif
                </div>
            </div>

<!--            <div class="section-divider mv40">
                <span><i class="fa fa-book"></i>Upload Documents</span>
            </div>
            <div id="employee-document">
                @php $count = 0; @endphp
                @if(!empty($employee['document_type_data']))
                @php $count = count($employee['document_type_data']); @endphp
                @foreach($employee['document_type_data'] as $key => $document_data)
                @php $rel = $key; @endphp
                <div class="section row employee_row_{{$rel}}"  id="spy1">
                    {!! Form::hidden('employee_document_id[]', old('employee_document_id',isset($document_data['employee_document_id']) ? $document_data['employee_document_id'] : ''), ['class' => 'gui-input ','id' => 'employee_document_id']) !!}
                    <div class="col-md-3 seats_row" id="seats_row{{$rel}}">
                        <label><span class="radioBtnpan">{{trans('language.document_type')}}</span></label>
                        <label class="field select">
                            {!!Form::select('document_type_id['.$rel.']', $employee['arr_document_type'],isset($document_data['document_type_id']) ? $document_data['document_type_id'] : null, ['class' => 'form-control'])!!}
                        </label>
                    </div>
                    <div class="col-md-3">
                        <label>
                            <span class="radioBtnpan">
                                {!! trans('language.document') !!}
                                <span class="asterisk">*</span>
                                @if(isset($document_data['document_url']) && !empty($document_data['document_url']))
                                <a href="{{url($document_data['document_url'])}}" target="_blank">Click to view </a>
                                @endif
                            </span>
                        </label>
                        <label class="btn-bs-file btn btn-xs col-md-12 btncustom">
                            <div class="col-md-6 classfontsize">
                                <span class="glyphicons glyphicons-upload"></span>
                                {!! trans('language.upload_document') !!}
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-4 fontsizeand">
                                <span class="glyphicons glyphicons-search"></span>  Browse file </div>
                            {!! Form::file('document_url'.$rel,["accept"=>"image/jpeg,image/gif,image/png,application/pdf"]); !!}
                        </label>
                    </div>
                    <div class="col-md-3 seats_row" id="row-delete{{$rel}}">
                        @if($key == 0)
                        <button type="button" class="btn btn-primary add-row" id="" style="margin-top: 25px !important; padding: 5px !important;">
                            <i class="fa fa-plus-circle"></i> &nbsp; Document
                        </button>
                        @else
                        <button type='button' class='btnDelete' rel='{{$rel}}' employee-document-id="{{$document_data['employee_document_id']}}" style="margin-top: 8% !important;"><i class='fa fa-times-circle' title='Remove' ></i></button>
                        @endif
                    </div>
                </div>
                @endforeach
                @else
                @php $rel = 0; @endphp
                {!! Form::hidden('employee_document_id[]', null, ['class' => 'gui-input ','id' => 'employee_document_id']) !!}
                <div class="section row employee_row_{{$rel}}"  id="spy1">
                    <div class="col-md-3 seats_row" id="seats_row{{$rel}}">
                        <label><span class="radioBtnpan">{{trans('language.document_type')}}</span></label>
                        <label class="field select">
                            {!!Form::select('document_type_id['.$rel.']', $employee['arr_document_type'],isset($employee['document_type_id']) ? $employee['document_type_id'] : null, ['class' => 'form-control'])!!}
                            <i class="arrow double"></i>
                        </label>
                        @if ($errors->has('document_type_id')) <p class="help-block">{{ $errors->first('document_type_id') }}</p> @endif
                    </div>
                    <div class="col-md-3">
                        <label>
                            <span class="radioBtnpan">{!! trans('language.document') !!}
                            </span>
                        </label>
                        <label class="btn-bs-file btn btn-xs col-md-12 btncustom">
                            <div class="col-md-6 classfontsize">
                                <span class="glyphicons glyphicons-upload"></span>
                                {!! trans('language.document') !!}
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-4 fontsizeand">
                                <span class="glyphicons glyphicons-search"></span>  Browse file 
                            </div>
                            {!! Form::file('document_url'.$rel,["accept"=>"image/jpeg,image/gif,image/png,application/pdf"]); !!}
                        </label>
                    </div>
                    <div class="col-md-3 seats_row" id="row-delete{{$rel}}">
                        <button type="button" class="btn btn-primary add-row" id="" style="margin-top: 24px !important; padding: 5px !important;">
                            <i class="fa fa-plus-circle"></i> &nbsp; Document
                        </button>
                    </div>
                </div>
                @endif
            </div>-->
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit('Save', ['class' => 'button btn-primary','name'=>'submit']) !!}
        </div>
        <!-- end .form-footer section -->
    </div>

</div>
</div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#dob").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            maxDate: "-10Y",
            minDate: "-100Y",
            yearRange: "-100:-10"
        });
        $("#employee-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                employee_code: {
                    required: true,
                },
                full_name: {
                    required: true,
                    lettersonly: true
                },
                aadhaar_number: {
                    required: true,
                    min: 0
                },
                account_number: {
                    required: true,
                    min: 0
                },
                father_name: {
                    required: true,
                    lettersonly: true
                },
                department_id: {
                    required: true
                },
                session_id: {
                    required: true,
                },
                email: {
                    required: true,
                },
                contact_number: {
                    required: true,
                },
                bank_name: {
                    required: true,
                    lettersonly: true
                },
                ifsc_code: {
                    required: true,
                },
                gender: {
                    required: true,
                },
                marital_status: {
                    required: true,
                },
                address: {
                    required: true,
                },
                caste_category_id: {
                    required: true,
                },
                qualification: {
                    required: true,
                },
                join_date: {
                    required: true,
                },
                user_role_id: {
                    required: true,
                },
                staff_type_id: {
                    required: true,
                },
                dob: {
                    required: true,
                },
                profile_photo: {
                    extension: true,
//                    required: true,
                },

            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || !/[~`!@#$%\^&*()+=\-\_\.\[\]\\';,/{}|\\":<>\?]/.test(value) || /^[a-zA-Z][a-zA-Z0-9\s\.]+$/i.test(value);
        }, "Please enter only alphabets(more than 1)");

        $('#join_date').datepicker().on('change', function (ev) {
            $(this).valid();
        });

        jQuery.validator.addMethod("extension", function (a, b, c) {
            return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
        }, jQuery.validator.format("Only valid image formats are allowed"));

        // add more dociument
        var count = 0;
        var count = '{!! $count >= 0 ? $count + 1 : $count !!}';
        count = parseInt(count) + 1 - 1;
        $(".add-row").click(function () {
<?php
    $option = '';
    foreach ($employee['arr_document_type'] as $key => $value)
    {
        $option .= '<option value="' . $key . '">' . $value . '</option>';
    }
?>
            var document_list = '{!! $option !!}';

            var markup = '<div class="section row employee_row_' + count + '" id="spy1">' +
                    '<div class="col-md-3 seats_row" id="seats_row">' +
                    '<label><span class="radioBtnpan">Document Type</span></label>' +
                    '<label class="field select">' +
                    '<select name="document_type_id[' + count + ']" class="form-control section_id" id="section_id' + count + '" required>' + document_list + '</select>' +
                    '</label>' +
                    '</div>' +
                    '<div class="col-md-3 seats_row" id="seats_row">' +
                    '<label><span class="radioBtnpan">Document</span></label>' +
                    '<label class="btn-bs-file btn btn-xs col-md-12 btncustom">' +
                    '<div class="col-md-6 classfontsize">' +
                    '<span class="glyphicons glyphicons-upload"></span>' +
                    'Upload Document' +
                    '</div>' +
                    '<div class="col-md-2"></div>' +
                    '<div class="col-md-4 fontsizeand">' +
                    '<span class="glyphicons glyphicons-search"></span>  Browse file </div>' +
                    '<input name="document_url' + count + '" type="file" accept="image/jpeg,image/gif,image/png,application/pdf">' +
                    '</label>' +
                    '</label>' +
                    '</div>' +
                    '<div class="col-md-3 seats_row" id="" style="margin-top:2%;">' +
                    "<button type='button' class='btnDelete' rel=" + count + " employee-document-id=''><i class='fa fa-times-circle' title='Remove'></i></button>" +
                    '</div>';
            $("#employee-document").append(markup);
            count++;

        });
        $(document).on('click', '.btnDelete', function () {

            var employee_document_id = $(this).attr("employee-document-id");
            var rel = $(this).attr("rel");
            if (employee_document_id !== '')
            {
                bootbox.confirm({
                    message: "Are you sure to delete ?",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result)
                        {
                            var token = '{!!csrf_token()!!}';
                            $.ajax({
                                url: "{{ url('employee-document/delete/') }}",
                                datatType: 'json',
                                type: 'POST',
                                data: {
                                    'employee_document_id': employee_document_id,
                                },
                                beforeSend: function () {
                                    $("#LoadingImage").show();
                                },
                                success: function (res)
                                {
                                    $("#LoadingImage").hide();
                                    if (res.status === "success")
                                    {
                                        $(".employee_row_" + rel).remove();
                                    } else if (res.status === "used")
                                    {
                                        $('#server-response-message').text(res.message);
                                        $('#alertmsg-student').modal({
                                            backdrop: 'static',
                                            keyboard: false
                                        });
                                    }

                                }
                            });
                        }
                    }
                });
            } else
            {
                $(".employee_row_" + rel).remove();
            }

        });
    });
</script>
