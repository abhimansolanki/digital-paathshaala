@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<style>
    button[disabled]{
        background: #76aaef !important;
    }
    #session_id {
        padding:0px !important;
        height: 30px !important;
    }
</style>
<div class="tray tray-center tableCenter">
    @include('backend.partials.loader')
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">

                <div class="panel-title hidden-xs col-md-7">
                    <span class="glyphicon glyphicon-tasks"></span> <span>View Complaints</span>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="student-complaint-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>{{trans('language.complaint_id')}}</th>
                            <th>{{trans('language.heading')}}</th>
                            <th>{{trans('language.description')}}</th>
                            <th>{{trans('language.enrollment_number')}}</th>
                            <th>Stu. Name</th>
                            <th>{{trans('language.father_name')}}</th>
                            <th>{{trans('language.father_contact_number')}}</th>
                            <th>{{trans('language.status')}}</th>
                            <th>Change Status</th>
                            <th>{{trans('language.action')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        getComplaints();
        function getComplaints()
        {
            $('#student-complaint-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
//                bFilter:false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'Student Complaint',
                        "filename": 'student-complaint',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Student Complaint',
                        "filename": 'student-complaint',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8],
                            modifier: {
                                selected: true
                            }
                        }
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'className': 'select-checkbox',
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                ajax: {
                    url: "{{ url('complaint/data')}}",
                    data: function (f) {
                    }
                },
                columns: [
                    {data: 'complaint_id', name: 'complaint_id'},
                    {data: 'complaint_id', name: 'complaint_id'},
                    {data: 'heading', name: 'heading'},
                    {data: 'description', name: 'description'},
                    {data: 'enrollment_number', name: 'enrollment_number'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'father_name', name: 'father_name'},
                    {data: 'father_contact_number', name: 'father_contact_number'},
                    {data: 'status_name', name: 'status'},
                    {data: 'update_status', name: 'update_status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $(".buttons-excel,.buttons-print").css({
                'margin-left': '7px',
                'background-color': '#2e76d6',
                'color': 'white',
                'border': '1px solid #eeeeee',
                'float': 'right',
                'padding': '5px'});

            $(".buttons-excel").prop('disabled', true);
            $(".buttons-print").prop('disabled', true);
        }
        var table = $('#student-complaint-table').DataTable();
        table.on('select deselect', function (e) {
            var arr_checked_complaint = checkedComplaint();
            if (arr_checked_complaint.length > 0)
            {
                $(".buttons-excel").prop('disabled', false);
                $(".buttons-print").prop('disabled', false);
            } else
            {
                $(".buttons-excel").prop('disabled', true);
                $(".buttons-print").prop('disabled', true);
            }
        });

        function checkedComplaint()
        {
            var arr_checked_complaint = [];
            $.each(table.rows('.selected').data(), function () {
                arr_checked_complaint.push(this["complaint_id"]);
            });
            return arr_checked_complaint;
        }

        $(document).on('change', '.complaint_status', function (e)
        {
            var status = $(this).val();
            var id = $(this).attr('id');
            if (id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{!! url('save-complaint-status/" + id + "/" + status + "') !!}",
                    datatType: 'json',
                    type: 'GET',
                     beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        if (response.status === 'success')
                        {
                          $('#server-response-success').text('Status changed successfully');
                            $('#alertmsg-success').modal({
                                backdrop: 'static',
                                keyboard: false
                            }); 
                            table.ajax.reload();
                        }
                    }
                });
            }
        });
    });

</script>
</body>
</html>
@endsection
@push('scripts')
@endpush



