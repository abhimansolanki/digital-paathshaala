@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_syllabus')}}</span>
                </div>
                 <div class="topbar-right">
                     <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right custom-btn">{{trans('language.add_syllabus')}}</a>
                 </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="create-exam-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.syllabus_type') !!}</th>
                            <th>{!!trans('language.name') !!}</th>
                            <th>{!!trans('language.class') !!}</th>
                            <th>{!!trans('language.subject') !!}</th>
                            <!--<th>{!!trans('language.syllabus_content') !!}</th>-->
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#create-exam-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('syllabus/data') }}",
//        "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'syllabus_type_name', name: 'syllabus_type_name'},
                {data: 'name', name: 'name'},
                {data: 'class_name', name: 'class_name'},
                {data: 'subject_name', name: 'subject_name'},
//                {data: 'syllabus_content', name: 'syllabus_content'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('syllabus/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'syllabus_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();

                                        }
                                        else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
    });
</script>
</body>
</html>
@endsection
@push('scripts')
@endpush

