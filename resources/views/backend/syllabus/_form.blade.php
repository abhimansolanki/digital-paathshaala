<style>
    #accordionpanel1 {
        padding: 0px !important;
        border-radius: 0px;
    }

    .accordionpanel {
        background: #f9f9f9 !important;
        width: 100%;
        display: block;
        padding: 10px 8px;
        font-weight: bold;
        text-transform: capitalize;
        font-size: 12px;
    }

    .accordionpanel label {
        color: #000;
    }

    .classopen {
        display: none;
    }

    table tr td {
        text-transform: capitalize;
    }

</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_syllabus')}}</li>
                </ol>
            </div>
            <div class="topbar-right">
                <a href="{{$redirect_url}}" class="button btn-primary text-right pull-right">{{trans('language.view_syllabus')}}</a>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages')
        @endif
        <div class="bg-light panelBodyy" style="margin-bottom: 25%;">
            <div class="section-divider mv40">
                <span><i class="fa fa-graduation-cap"></i>&nbsp;Syllabus</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $syllabus['arr_session'],isset($syllabus['session_id']) ? $syllabus['session_id'] : null, ['class' => 'form-control','id'=>'session_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('session_id')) 
                    <p class="help-block">{{ $errors->first('session_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.class')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('class_id', $syllabus['arr_class'],isset($syllabus['class_id']) ? $syllabus['class_id'] : null, ['class' => 'form-control','id'=>'class_id'])!!}
                        <i class="arrow double"></i>
                    </label> @if ($errors->has('class_id'))
                    <p class="help-block">{{ $errors->first('class_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.section')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('section_id', $syllabus['arr_section'],isset($syllabus['section_id']) ? $syllabus['section_id'] : null, ['class' => 'form-control','id'=>'section_id'])!!}
                        <i class="arrow double"></i>
                    </label> @if ($errors->has('section_id'))
                    <p class="help-block">{{ $errors->first('section_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.syllabus_type')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('syllabus_type', $syllabus['arr_syllabus_type'],isset($syllabus['syllabus_type']) ? $syllabus['syllabus_type'] : null, ['class' => 'form-control','id'=>'syllabus_type'])!!}
                        <i class="arrow double"></i>
                    </label> @if ($errors->has('syllabus_type'))
                    <p class="help-block">The syllabus type has already been taken with other selected parameters.</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <div id='syllabus_monthly'>
                        <label><span class="radioBtnpan">{{trans('language.monthly')}}<span class="asterisk">*</span></span></label>
                        <label class="field select">
                            {!!Form::select('month', $syllabus['arr_months'],isset($syllabus['month']) ? $syllabus['month'] : '', ['class' => 'form-control','id'=>'month'])!!}
                            <i class="arrow double"></i>
                        </label> @if ($errors->has('month'))
                        <p class="help-block">{{ $errors->first('month') }}</p>
                        @endif
                    </div>
                    <div id='syllabus_exam'>
                        <label><span class="radioBtnpan">{{trans('language.exam')}}<span class="asterisk">*</span></span></label>
                        <label class="field select">
                            {!!Form::select('exam_id', $syllabus['arr_exam'],isset($syllabus['exam_id']) ? $syllabus['exam_id'] : '', ['class' => 'form-control','id'=>'exam_id'])!!}
                            <i class="arrow double"></i>
                        </label> @if ($errors->has('exam_id'))
                        <p class="help-block">{{ $errors->first('exam_id') }}</p>
                        @endif
                    </div>
                </div>
                <div id='syllabus_subject'>
                    <div class="col-md-2">
                        <label><span class="radioBtnpan">{{trans('language.subject')}}<span class="asterisk">*</span></span></label>
                        <label class="field select">
                            {!!Form::select('subject_id', $syllabus['arr_class_subject'],isset($syllabus['subject_id']) ? $syllabus['subject_id'] : '', ['class' => 'form-control','id'=>'subject_id'])!!}
                            <i class="arrow double"></i>
                        </label> @if ($errors->has('subject_id'))
                        <p class="help-block">{{ $errors->first('subject_id') }}</p>
                        @endif
                    </div>  
                </div>
            </div>
            <div class="section row subject_content" id="spy1">
                <!--style='display: none;'-->
                <div id='subject_content'>
                    <div class="col-md-10">
                        <label><span class="radioBtnpan">{{trans('language.syllabus_content')}}<span class="asterisk">*</span></span></label>
                        <label class="field">
                            {!!Form::textarea('syllabus_content',isset($syllabus['syllabus_content']) ? $syllabus['syllabus_content'] : '', ['class' => 'form-control ckeditor','id'=>'syllabus_content','rows'=>4])!!}
                            <i class="arrow double"></i>
                        </label> @if ($errors->has('syllabus_content'))
                        <p class="help-block">{{ $errors->first('syllabus_content') }}</p>
                        @endif
                    </div>  
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#syllabus-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            ignore: [],
            debug: false,
            rules: {
//                syllabus_title: {
//                    required: true
//                },
                syllabus_type: {
                    required: true
                },
                class_id: {
                    required: true
                },
                subject_id: {
                    required: true
                },
                session_id: {
                    required: true
                },
                syllabus_content: {
                    required: function () {
                        var messageLength = CKEDITOR.instances['syllabus_content'].getData().replace(/<[^>]*>/gi, '').length;
                        if (!messageLength) {
                            return true;
                        } else
                        {
                            return false;
                        }
                    }
                },
                exam_id: {
                    required: function (e) {
                        if ($("#syllabus_type option:selected").val() == 1)
                        {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                month: {
                    required: function (e) {
                        if ($("#syllabus_type option:selected").val() == 2)
                        {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

//        $('#syllabus_content').on('change', function (ev) {
//            $(this).valid();
//        });
//           CKEDITOR.replace('syllabus_content');


        $(document).on('change', '#class_id,#session_id', function (e) {
            e.preventDefault();
            var class_id = $('#class_id').val();
            var session_id = $('#session_id').val();
            $('#section_id').empty();
            $('#section_id').append('<option value=""> --Select Section --</option>');
            if (class_id !== '' && session_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                        'session_id': session_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $.each(resopose_data, function (key, value) {
                                $('#section_id').append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                    }
                });
            }

        });
    });

    $(document).on('change', '#class_id,#session_id,#syllabus_type', function (e) {
        getSubjectExam();
    });
    function getSubjectExam()
    {
        var class_id = $('#class_id').val();
        var section_id = $('#section_id').val();
        var session_id = $('#session_id').val();
        var syllabus_type = $('#syllabus_type').val();
        $('select[name="subject_id"]').empty();
        $('select[name="subject_id"]').append('<option value=""> -- Select subject -- </option>');

        $('select[name="exam_id"]').empty();
        $('select[name="exam_id"]').append('<option value=""> -- Select subject -- </option>');
        if (section_id !== '' && class_id !== '' && session_id !== '' && syllabus_type !== '')
        {
            $.ajax(
                    {
                        url: "{!! url('get-class-exam-subject/" + session_id + "/" + class_id + "/" + section_id + "') !!}",
                        datatType: 'json',
                        type: 'GET',
                        beforeSend: function () {
                            $("#LoadingImage").show();
                        },
                        success: function (res)
                        {
                            $("#LoadingImage").hide();
                            var arr_subject = res.data.subject;
                            var arr_exam = res.data.exam;
                            if (res.status === "success")
                            {
                                // subject dropdown
                                $.each(arr_subject, function (key, value) {
                                    $('select[name="subject_id"]').append('<option value="' + key + '">' + value + '</option>');
                                });
                                // exam dropdown
                                $.each(arr_exam, function (key, value) {
                                    $('select[name="exam_id"]').append('<option value="' + key + '">' + value + '</option>');
                                });
                                // dispaly subjec content div
//                                $(".subject_content").show();
                            } else
                            {
                                $('#server-response-message').text('Something went wrong');
                                $('#alertmsg-student').modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                            }
                        }
                    });
        }
    }
    getSyllabusTypeData();
    $(document).on('change', '#syllabus_type', function (e) {
        getSyllabusTypeData();
    });

    function getSyllabusTypeData()
    {
        var syllabus_type = $('#syllabus_type').val();
        if (syllabus_type !== '')
        {
            if (syllabus_type == 1)
            {
                $("#syllabus_exam").show();
                $("#syllabus_monthly").hide();
            } else if (syllabus_type == 2)
            {
                $("#syllabus_exam").hide();
                $("#syllabus_monthly").show();
            }
        } else
        {
            $("#syllabus_monthly").show();
            $("#syllabus_exam").hide();
        }
    }
</script>