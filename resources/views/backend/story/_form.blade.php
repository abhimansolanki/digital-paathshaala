<!-- Field Options -->
<style>
    .state-error{
        display:block !important;
        margin-top: 6px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    }
    .custom-cla
    {
        height: 36px !important;margin-top: -7px;
    }
</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_story')}}</li>
                </ol>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages')
        @endif
        <div class="bg-light panelBodyy">
            {!! Form::hidden('story_id',old('story_id',isset($story['story_id']) ? $story['story_id'] : ''),['class' => 'gui-input', 'id' => 'story_id', 'readonly' => 'true']) !!}
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.story_title')}}<span class="asterisk">*</span></span></label>
                    <label for="story_title" class="field prepend-icon">
                        {!! Form::text('story_title', old('story_title',isset($story['story_title']) ? $story['story_title'] : ''), ['class' => 'gui-input', 'id' => 'story_title']) !!}
                        <label for="story_title" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('story_title')) 
                    <p class="help-block">{{ $errors->first('story_title') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">Start Date<span class="asterisk">*</span></span></label>
                    <label for="start_date" class="field prepend-icon">
                        {!! Form::text('start_date', old('start_date',isset($story['start_date']) ? $story['start_date'] : ''), ['class' => 'gui-input','id' => 'start_date', 'readonly' => 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('start_date')) 
                    <p class="help-block">{{ $errors->first('start_date') }}</p>
                    @endif
                </div>

                <div class="col-md-2">
                    <label><span class="radioBtnpan">Expiry Date<span class="asterisk">*</span></span></label>
                    <label for="end_date" class="field prepend-icon">
                        {!! Form::text('end_date', old('end_date',isset($story['end_date']) ? $story['end_date'] : ''), ['class' => 'gui-input', 'id' => 'end_date', 'readonly' => 'readonly']) !!}
                        <label for="datepicker1" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('end_date')) 
                    <p class="help-block">{{ $errors->first('end_date') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.story_url')}}<span class="asterisk">*</span></span></label>
                    <label>{!!Form::file('story_url',['accept'=>'image/*'])!!} 
                        @if(isset($story['story_url']) && !empty($story['story_url']))
                        <a href="" data-toggle="modal" data-target="#img1" data-backdrop="static" data-keyboard="false">Click to view </a>
                        @endif
                    </label>
                </div>
            </div>
        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
	{!! Form::reset('Cancel', ['class' => 'button btn-primary', 'id' => 'form-reload']) !!}
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
<div id="img1" class="modal fade-scale" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                @if(isset($story['story_url']) && !empty($story['story_url']))
                <img src="{{url($story['story_url'])}}" style="width: 100%">
                @endif
            </div>
        </div>
    </div>
</div>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" title="">{{trans('language.list_story')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-striped table-hover" id="story-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.story_title') !!}</th>
                            <th>{!!trans('language.story_url') !!}</th>
                            <th>Start Date</th>
                            <th>Expiry Date</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
// set date picker
        $("#start_date,#end_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: 0,
            maxDate: "{!! $story['session_end_date'] !!}"
        });

        $(document).on('click', '#form-reload', function (e) {
            window.location.href = "{{ url('admin/story') }}";
        });
        $("#story-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                story_title: {
                    required: true,
                    lettersonly: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                story_url: {
                    required: function () {
                        if ($("#story_id").val() === '')
                        {
                            return true;
                        } else
                        {
                            return false;
                        }
                    },
                    extension: true
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        $('#start_date').datepicker().on('change', function (ev) {
            var max_date = $(this).datepicker("option", "maxDate");
            $("#end_date").datepicker('destroy');
            $("#end_date").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                minDate: $(this).val(),
                maxDate: max_date
            });
            $(this).valid();
        });
        $('#end_date').datepicker().on('change', function (ev) {
            $(this).valid();
        });

//     alert("{!! $story['session_end_date'] !!}");


        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || !/[~`!@#$%\^&*()+=\-\_\.\[\]\\';,/{}|\\":<>\?]/.test(value) || /^[a-zA-Z][a-zA-Z0-9\s\.]+$/i.test(value);
        }, "Please enter only alphabets(more than 1)");

        jQuery.validator.addMethod("extension", function (a, b, c) {
            return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
        }, jQuery.validator.format("Only valid image formats are allowed"));

        var table = $('#story-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('story/data')}}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'story_title', name: 'story_title'},
                {data: 'story_url', name: 'story_url'},
                {data: 'start_date', name: 'start_date'},
                {data: 'end_date', name: 'end_date'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            var id = $(this).attr("data-id");
            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        $.ajax(
                                {
                                    url: "{{url('story/delete/')}}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'story_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status === "success")
                                        {
                                            table.ajax.reload();
                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });

    });
</script>