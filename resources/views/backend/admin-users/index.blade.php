@extends('admin_panel/layout')
@section('content')
    <style>
        button[disabled] {
            background: #76aaef !important;
        }
    </style>
    <div class="tray tray-center tableCenter">
        <div class="">
            <div class="panel panel-visible" id="spy2">
                <div class="panel-heading">

                    <div class="panel-title hidden-xs col-md-7">
                        <span class="glyphicon glyphicon-tasks"></span> <span>App Users</span>
                    </div>
                </div>
                <div class="panel" id="transportId">
                    <div class="panel-body">
                        <div class="tab-content  br-n">
                            <div id="tab1_1" class="">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="section">
                                            <label class="field select" style="width: 100%;">
                                                {!!Form::text('mobile_number',old('mobile_number'), ['class' => 'form-control','id'=>'mobile_number','placeholder'=>"Mobile No."])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body pn">
                    @include('backend.partials.messages')
                    <table class="table table-bordered table-striped table-hover" id="app-user-tc-table" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Mobile No.</th>
                            <th>Father Name</th>
                            <th>{{trans('language.student')}}</th>
                            <th class="text-center">{{trans('language.action')}}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="subscription-details" role="dialog" style="display: none;">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Subscription Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" id="subscription-content">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="update-password" role="dialog" style="display: none;">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                {!! Form::open(['id'=>'student-password-form', 'method' => 'POST', 'class'=>'form-horizontal','url' => url("admin/app-users/update-password"),'autocomplete'=>'off']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Update Password</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label><span class="radioBtnpan">{{trans('language.password')}}*</span></label>
                            <label for="form_number" class="field prepend-icon">
                                {!! Form::text('password', old('password'), ['class' => 'gui-input',''=>trans('language.password'), 'id' => 'password']) !!}
                                <label for="password" class="field-icon">
                                </label>
                            </label>
                        </div>
                        {!! Form::hidden('update_admin_user_id', "", ['id' => 'update-admin-user-id']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary','name'=>'save']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            getStudentTc();

            function getStudentTc() {
                $('#app-user-tc-table').DataTable({
                    destroy: true,
                    processing: true,
                    serverSide: true,
                    dom: 'Blfrtip',
                    pageLength: 50,
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                            "title": 'App Users',
                            "filename": 'app-users',
                            exportOptions: {
                                columns: [1, 2, 3, 4],
                                modifier: {
                                    selected: true
                                }
                            }
                        },
                        {
                            extend: 'print',
                            "text": '<span class="fa fa-print"></span> &nbsp; Print',
                            "title": 'App Users',
                            "filename": 'app-users',
                            exportOptions: {
                                columns: [1, 2, 3, 4],
                                modifier: {
                                    selected: true
                                }
                            }
                        }
                    ],
                    select: {
                        style: 'multi',
                        selector: 'td:first-child'
                    },
                    'columnDefs': [
                        {
                            'targets': 0,
                            'className': 'select-checkbox',
                            'checkboxes': {
                                'selectRow': true
                            }
                        }
                    ],
                    ajax: {
                        url: "{{ url('app-users/data')}}",
                        data: function (f){
                            f.mobile_number = $("#mobile_number").val();
                        }
                    },
                    columns: [
                        {data: 'admin_user_id', name: 'admin_user_id'},
                        {data: 'mobile_number', name: 'mobile_number'},
                        {data: 'student_parent_name', name: 'student_parent_name'},
                        {data: 'students', name: 'students'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });

                $(".buttons-excel,.buttons-print").css({
                    'margin-left': '7px',
                    'background-color': '#2e76d6',
                    'color': 'white',
                    'border': '1px solid #eeeeee',
                    'float': 'right',
                    'padding': '5px'
                });

                $(".buttons-excel").prop('disabled', true);
                $(".buttons-print").prop('disabled', true);
            }

            var table = $('#app-user-tc-table').DataTable();
            table.on('select deselect', function (e) {
                var arr_checked_student = checkedStudent();
                if (arr_checked_student.length > 0) {
                    $(".buttons-excel").prop('disabled', false);
                    $(".buttons-print").prop('disabled', false);
                } else {
                    $(".buttons-excel").prop('disabled', true);
                    $(".buttons-print").prop('disabled', true);
                }
            });

            function checkedStudent() {
                var arr_checked_student = [];
                $.each(table.rows('.selected').data(), function () {
                    arr_checked_student.push(this["student_id"]);
                });
                return arr_checked_student;
            }

            $(document).on('change', '#mobile_number', function (e) {
                getStudentTc();
            });
        });
        function setSubscriptionData(e) {
            $("#subscription-content").html($("#set-subscription-"+e).attr('tb-data'));
            $("#subscription-details").modal('show');
        }
        function viewUpdatePassword(e) {
            $("#update-admin-user-id").val($("#update-password-"+e).attr('tb-data'));
            $("#update-password").modal('show');
        }
    </script>
    </body>
    </html>
@endsection


