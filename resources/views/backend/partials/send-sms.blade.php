<style>
     textarea{
        min-height: 200px;
        resize: none !important;
    }
    .error{
        color: red;
    }
</style>
<div class="modal" id="SendSMS" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Message</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="newModalFormId">
                    <div class="form-group">
                        <div class="">
                            
                            {!!Form::textarea('sms_message','', ['class' => 'form-control form-group','id'=>'sms_message','rows'=>5,'placeholder'=>'Enter message'])!!}
                        </div>
                        <span id='success-message' style="font-size:13px; color: green"></span>
                    </div>
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" id="btnCloseIt" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" title="Send Sms" id="send_sms"><i class="fa fa-comment"></i>&nbsp;Send</button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(function () {
            $("#newModalFormId").validate({
                rules: {
                    sms_message: {
                        required: true,
//                        // nowhitespace: true,
                    },
                    action: "required"
                },
                messages: {
                    sms_message: {
                        required: "Please enter message",
                    }
                }
            });
        });
        
        $(document).on('click', '#sms-button', function () {
            $("#send_sms").prop('disabled', false);
            $("#sms_message").val('');
            $("#success-message").text('');
            $("#SendSMS").modal('show');

        });
    });
</script>