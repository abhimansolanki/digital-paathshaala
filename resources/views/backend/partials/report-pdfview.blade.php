<style type="text/css">
	table td, table th{
		border:0px;
	}
</style>
<div class="container">

    <table width="100%" style="border:solid 1px">
		<tr>
                    <th colspan="5" style="text-align:center">{{trans('language.report')}}</th>
			
		</tr>
		
                <tr>
			<td>{{trans('language.contract_serial_no')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{$contract_serial_no  }}</td>
			
		</tr>
                <tr>
			<td>{{trans('language.report_type')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{$report_type  }}</td>
			
		</tr>
                <tr>
			<td>{{trans('language.report_place')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $report_place }}</td>
			
		</tr>
                <tr>
			<td>{{trans('language.report_date')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $report_date  }}</td>
			
		</tr>
                <tr>
			<td>{{trans('language.partner_name')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $partner_name }}</td>
			
		</tr>
                <tr>
			<td>{{trans('language.partner_license_number')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $partner_license_number }}</td>
			
		</tr>
                <tr>
			<td>{{trans('language.report_note')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $report_note }}</td>
			
		</tr>
                <tr>
			<td>{{trans('language.user')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $user_name }}</td>
			
		</tr>
                <tr>
			<td>{{trans('language.manager')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $manager_name }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.created_at')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $created_at }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.description')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $description }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.fees')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $fees }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.cr_number')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $cr_number }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.client_name')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $client_name }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.client_number')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $client_number }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.client_type')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $client_type }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.audittype')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $audittype }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.category')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $category_name }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.payment_method')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $payment_method }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.payment_type')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $payment_type }}</td>
			
		</tr>
		
		
	</table>
</div>