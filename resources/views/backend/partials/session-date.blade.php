<script>
    jQuery(document).ready(function () {
    $('#start_date').datepicker().on('change', function (ev) {
        var max_date = $(this).datepicker("option", "maxDate");
        $("#end_date").datepicker('destroy');
        $("#end_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: $(this).val(),
            maxDate: max_date
        });
        $(this).valid();
    });
    $('#end_date').datepicker().on('change', function (ev) {
        $(this).valid();
    });

    $(document).on('change', '#session_id', function () {
        var session_id = $("#session_id").val();
        $("#end_date").val('');
        $("#start_date").val('');
         $.getDateData(session_id);

    });

    $.getDateData = function (session_id)
    {
        if (session_id !== '')
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-session')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'session_id': session_id,
                },
                beforeSend: function () {
                    $("#LoadingImage").show();
                },
                success: function (response) {
                    if (response.status === 'success')
                    {
                        var minDate = response.start_date;
                        var maxDate = response.end_date;
                        setDate(minDate, maxDate);
                        $("#LoadingImage").hide();
                    }
                }
            });
        }
    }
    function setDate(minDate, maxDate)
    {
        $("#end_date").datepicker('destroy');
        $("#start_date").datepicker('destroy');
        $("#start_date").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            minDate: 0,
            maxDate: maxDate
        });
        if ($("#start_date").val() !== '')
        {
            $("#end_date").datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: false,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                minDate: $("#start_date").val(),
                maxDate: maxDate
            });
        }

    }
    $(document).on('click', '#start_date', function () {
        var session = $("#session_id").val();
        if (session === '')
        {
            $('#server-response-message').text('First select academic year');
            $('#alertmsg-student').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    });
    $(document).on('click', '#end_date', function () {
        var start_date = $("#start_date").val();
        if (start_date === '')
        {
            $('#server-response-message').text('First select admission date');
            $('#alertmsg-student').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    });
    });
</script>
