<label><span class="radioBtnpan">{{trans('language.age')}}</span></label>
<label for="age" class="field prepend-icon">
    {!! Form::text('age', old('dob',isset($student['age']) ? $student['age'] : ''), ['class' => 'gui-input',''=>trans('language.age'), 'id' => 'age', 'readonly' => 'readonly']) !!}
</label>
<script>
     $(document).on('change', '#dob', function () {
        var dob = $("#dob").val();
        $("#age").val('');
        if (dob !== '')
        {
         getAge(dob);  
        }
     });
     function getAge(dob)
     {
          $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('get-age')}}",
                datatType: 'json',
                type: 'POST',
                data: {
                    'dob': dob,
                },
                beforeSend: function () {
                    $("#LoadingImage").show();
                },
                success: function (response) {
                    if (response.status === 'success')
                    {
                        $("#age").val(response.age);
                        $("#LoadingImage").hide();
                    }
                }
            });
     }
</script>
