<style type="text/css">
	table td, table th{
		border:0px;
	}
</style>
<div class="container">

    <table width="100%" style="border:solid 1px">
		<tr>
                    <th colspan="5" style="text-align:center">{{trans('language.quotation')}}</th>
			
		</tr>
		
		<tr>
			<td>{{trans('language.created_at')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $created_at }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.description')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $description }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.fees')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $fees }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.cr_number')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $cr_number }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.client_name')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $client_name }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.client_number')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $client_number }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.client_type')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $client_type }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.audittype')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $audittype }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.category')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $category_name }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.payment_method')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $payment_method }}</td>
			
		</tr>
		<tr>
			<td>{{trans('language.payment_type')}}</td>
                        <td></td>
                        <td>:</td>
                        <td></td>
			<td>{{ $payment_type }}</td>
			
		</tr>
		
		
	</table>
</div>