@extends('admin_panel/layout')
@push('styles')
@endpush
@section('content')
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_subject')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                @include('backend.partials.messages')
                <table class="table table-bordered table-striped table-hover" id="room-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.room_number') !!}</th>
                            <th>{!!trans('language.total_seats') !!}</th>
                            <!--<th>{!!trans('language.room_type') !!}</th>-->
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
               </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#room-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ url('room/data') }}',
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'room_number', name: 'room_number'},
                {data: 'total_seats', name: 'total_seats'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('room/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'room_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            toastr.success(res.message, {timeOut: 3000});
                                            table.ajax.reload();

                                        } else
                                        {
                                            toastr.error(res.message, {timeOut: 3000});
                                        }
                                    }
                                });
                    }
                }
            });

        });
    });
</script>
@endsection
@push('scripts')
@endpush

