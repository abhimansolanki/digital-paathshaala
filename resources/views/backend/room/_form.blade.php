<!-- Field Options -->
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_room')}}</li>
                </ol>
            </div>
            <div class="topbar-right">
                <a  href="{{$redirect_url}}" class="button btn-primary text-right pull-right">{{trans('language.view_room')}}</a>
            </div>
        </div>
        <!--@include('backend.partials.messages')-->
        <div class="bg-light panelBodyy">

            <div class="section-divider mv40">
                <span><i class="material-icons">room_service</i>Room Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan"></span>{{trans('language.room_number')}}</label>
                    <label for="room_number" class="field prepend-icon">
                        {!! Form::number('room_number', old('room_number',isset($room->room_number) ? $room->room_number : ''), ['class' => 'gui-input',''=>trans('language.room_number'), 'id' => 'room_number']) !!}
                        <label for="room_number" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('room_number')) 
                    <p class="help-block">{{ $errors->first('room_number') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.total_seats') !!}</span></label>
                    <label for="total_seats" class="field prepend-icon">
                        {!! Form::number('total_seats', old('total_seats',isset($room->total_seats) ? $room->total_seats : ''), ['class' => 'gui-input', 'id' => 'total_seats']) !!}
                        <label for="total_seats" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('total_seats')) <p class="help-block">{{ $errors->first('total_seats') }}</p> @endif
                </div>

            </div>

        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

        $("#room-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                room_number: {
                    required: true
                },
                total_seats: {
                    required: true
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });
</script>