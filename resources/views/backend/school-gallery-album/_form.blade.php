<style type="text/css">
    table th, td{
        border:none !important;
    }
    .buttons button {
        float: left; 
        background: transparent; 
        outline: none; 
        border: none; 
        display: block; 
        width: 40px; 
    }
    .buttons i {
        font-size: 24px;
        margin: 5px 5px;
    }
    .remove_img{
        height: 20px;
        margin-top: -99px;
        width: 13px;
        margin-left: 90px;
        border: solid 1px;
        border-radius: 100%;
        color: white;
        padding: 1px;
        background-color: red;
    }
</style>

@php $id = Request::segment(3);
@endphp
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">School Gallery</li>
                </ol>
            </div>
        </div>
        @include('backend.partials.messages')
        <div class="bg-light panelBodyy">
            <div class="section row" id="spy1">
	    {!! Form::hidden('album_id',$id,['readonly'=>true]) !!}
	    <div class="col-md-3">
                    <label><span class="radioBtnpan">Album Title<span class="asterisk">*</span></span></label>
                    <label for="album_title" class="field prepend-icon">
                        {!! Form::text('album_title', old('album_title',isset($album['arr_album_images'][$id]['album_title'])? $album['arr_album_images'][$id]['album_title']: ''), ['class' => 'gui-input', 'id' => 'album_title']) !!}
                        <label for="album_title" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('album_title')) 
                    <p class="help-block">{{ $errors->first('album_title') }}</p>
                    @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">Description</span></label>
                    <label for="description" class="field prepend-icon">
                        {!! Form::text('description', old('album_title',isset($album['arr_album_images'][$id]['description'])? $album['arr_album_images'][$id]['description'] :''), ['class' => 'gui-input', 'id' => 'description']) !!}
                        <label for="description" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('description')) 
                    <p class="help-block">{{ $errors->first('description') }}</p>
                    @endif
                </div>
	    <div class="col-md-3">
                    <label><span class="radioBtnpan">Upload Photo(s)<span class="asterisk">*</span></span></label>
                    <label for="driver_aadhaar_number" class="field prepend-icon">
		<input type="file" name="image_url[]" multiple="multiple">
	        </label>
                </div>  
	    <div class="col-md-3">
                    <label><span class="radioBtnpan"></span></label>
                    <label for="" class="field prepend-icon">
		{!! Form::submit('Save', ['class' => 'button btn-primary','name'=>'save','style'=>'width:100px']) !!}
	        </label>
                </div>  
            </div>
        </div>
        <div class="section-divider">
	<span><i class="fa fa-home"></i>&nbsp;View School Gallery</span>
        </div>
        <div class="section row" id="spy1">
	@if(!empty($album))
	@foreach($album['arr_album_images'] as $album)
	<div id="Album{{$album['album_id']}}">
	    <table class="table mainsubject" border='0' style="background:#f5f5f5; border-color:#f5f5f5">
	        <tr>
		<td colspan="3">Title : <b>{!! $album['album_title'] !!}</b></td>
		<td colspan="3">Description : <b>{!! $album['description'] !!}</b></td>
		<td colspan="9" class="buttons">
		    <a href="{{url('admin/school-gallery-album/'.$album['album_id'])}}">
		        <button type="button" class="open_subject_model" title="Add Subject" data-toggle="modal" data-backdrop="static" data-keyboard="false" root_id="0" >
			<i class="fas fa-plus-square"></i>
		        </button>
		    </a>
		    <a href="{{url('admin/school-gallery-album/'.$album['album_id'])}}">
		        <button type="button" class="edit-subject-group" title="Edit Group" data-toggle="modal" data-backdrop="static" data-keyboard="false">
			<i class="fas fa-edit" style="color: green;"></i>
		        </button>
		    </a>
		    </a>			    
		    <button type="button" title="Remove Group" class="delete-button"   data-id="{{$album['album_id']}}" delete-type="Album">
		        <i class="fa fa-trash"></i>
		    </button>
		</td>
	        </tr>
	    </table>
	    <div style="width:100%;border: 1px solid #cdcdcd;" class="">
	        @foreach($album['get_gallery_images'] as $images)
	        @php $img = check_file_exist($images['image_url'],'school_gallery'); @endphp
	        @if($img)
	        <div id="Image{{$images['school_gallery_id']}}" style="margin: 5px; padding: 5px; border:solid 1px #cdcdcd; display:inline-block; float: left;width:103px; height: 100px;"><img src="{{url($img)}}"  style="width:100%; height: 100%" class="fancybox"><a href="#"><div class="remove_img delete-button" data-id="{{$images['school_gallery_id']}}" delete-type="Image">x</div></a></div> 
				   <!--<div style="margin: 5px; display:inline-block; float: left;"><img src="{{url($img)}}"  style="width:103px; height: 100px;" class="fancybox"></div>--> 
	        @endif
	        @endforeach
	        <div class="clearfix"></div>
	    </div>
	</div>
	@endforeach
	@endif
        </div>
    </div>
</div>
</div>
