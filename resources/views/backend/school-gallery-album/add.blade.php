@extends('admin_panel/layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="box box-info">
                {!! Form::open(['files'=>TRUE,'id' => 'school-gallery-album-form' , 'class'=>'form-horizontal','url' => $save_url]) !!}
                @include('backend.school-gallery-album._form',['submit_button' =>$submit_button ])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@include('backend.partials.popup')
<script type="text/javascript">
    $(document).ready(function () {
        $('.fancybox').fancybox();
        $("#school-gallery-album-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                album_title: {
                    required: true
                },
                'image_url[]': {
//                    required: true,
                    extension: true,
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        jQuery.validator.addMethod("extension", function (a, b, c) {
            return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
        }, jQuery.validator.format("Only valid image formats are allowed like png, jpeg, jpg, gif"));


        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");
            var type = $(this).attr("delete-type");

            bootbox.confirm({
                message: "Are you sure to delete this "+ type +"?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('school-gallery-album/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'id': id,
                                        'type': type,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status === "success")
                                        {
                                            new PNotify({
                                                title: type +' Data',
                                                text: type +' Deleted successfully',
                                                type: 'success',
                                                width: '290px',
                                                delay: 1400
                                            });
                                            $("#"+ type+id).remove();
                                        } else
                                        {
                                            new PNotify({
                                                title: type +' Data',
                                                text: 'Somthing went wrong, please try again ',
                                                type: 'error',
                                                width: '290px',
                                                delay: 1400
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });

    });

</script>
@endsection

