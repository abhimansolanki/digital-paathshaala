<style>
    .btn-danger[disabled]{
        border:0px !important;
    }
    #sortable 
    { list-style-type: none; margin: 0; padding: 0; width: 100% !important; }
    #sortable li 
    { margin: 0 3px 3px 3px !important; width: 100% !important;  padding: 0.4em !important; padding-left: 1.5em !important; font-size: 1em !important; height: 30px !important; }
    #sortable li span
    { position: absolute !important; margin-left: -1.3em !important; }
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_fee_type')}}</li>
                </ol>
            </div>
        </div>
        <div class="bg-light panelBodyy">
           @if(Session::has('success'))
           @include('backend.partials.messages')
           @endif
            <div class="section-divider mv40">
                <span><i class="glyphicon glyphicon-list-alt"></i>&nbsp;Fee Type Detail</span>&nbsp;
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{{trans('language.fee_type')}}<span class="asterisk">*</span></span></label>
                    <label for="fee_type" class="field prepend-icon">
                        {!! Form::text('fee_type',old('fee_type',isset($fee_type['fee_type']) ? $fee_type['fee_type'] : ''), ['class' => 'gui-input', 'id' => 'fee_type','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <i class="arrow double"></i>
                    </label>
                    @if($errors->has('fee_type')) <p class="help-block">{{ $errors->first('fee_type') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <span class="radioBtnpan">{{trans('language.is_root')}}<span class="asterisk">*</span></span>
                    <label for="is_root" class="field prepend-icon">
                        {!!Form::select('is_root', $fee_type['arr_is_root'],isset($fee_type['is_root']) ? $fee_type['is_root'] : '', ['class' => 'form-control','id'=>'is_root'])!!}
                        <label for="is_root" class="field-icon">
                            <i class="fa fa-calendar-o"></i>
                        </label>
                    </label>
                    @if ($errors->has('is_root')) 
                    <p class="help-block">{{ $errors->first('is_root') }}</p>
                    @endif
                </div>
                @php $is_root = isset($fee_type['is_root']) ? $fee_type['is_root'] : ''; 
                $display = 'none';
                @endphp
                @if($is_root === 0 )
                @php $display = 'block'; @endphp
                @endif
                <div class="col-md-3" style="display:{{$display}}" id="div_fee_type_id">
                    <label><span class="radioBtnpan">{{trans('language.fee_type_root')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('fee_type_id', $fee_type['arr_fee_type'],isset($fee_type['root_id']) ? $fee_type['root_id'] : '', ['class' => 'form-control','id'=>'fee_type_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if($errors->has('fee_type_id')) 
                    <p class="help-block">{{ $errors->first('fee_type_id') }}
                    </p> @endif
                </div>

            </div>

        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}">{{ trans('language.list_fee_type')}}</span>
                    <button type="button"  id="fee_type_order" class="button btn-primary text-right pull-right custom-btn">{{trans('language.set_fee_order')}}</button>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="fee-type-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.fee_order') !!}</th>
                            <th>{!!trans('language.fee_type') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="FeeTypeOrder" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Re-order Fee Type</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <ul id="sortable" class="" style="margin-top: 2%;">
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#FeeTypeOrder").modal('hide');
        var table = $('#fee-type-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('fee-type/data')}}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'order', name: 'order'},
                {data: 'fee_type', name: 'fee_type'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('fee-type/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'fee_type_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();
                                        } 
                                        else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
        $('#FeeTypeOrder').on('hidden.bs.modal', function (e) {
            location.reload(true);
        });
        $(document).on('click', '#fee_type_order', function (e) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            datatType: 'json',
            type: 'POST',
            async: true,
            url: "{{ url('get-fee-type-order') }}",
            success: function (res)
            {
                if (res.status === 'success')
                {
                    $.each(res.arr_fee_type, function (key, value) {
                        $('#sortable').append('<li class="ui-state-default" style="" value="' + value.fee_type_id + '">' + value.fee_type + '</li>');
                    });
                } else
                {
                    $('#sortable').append('<li>No data available to sort</li>');
                }
                $("#FeeTypeOrder").modal('show');
            }
        });
    });
    
    $("#sortable").sortable({
        update: function () {
            var order = $('#sortable').sortable('toArray', {attribute: 'value'});
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                datatType: 'json',
                type: 'POST',
                async: true,
                url: "{{ url('set-fee-type-order') }}",
                data: {
                    "item": order,
                }
            }).done(function (data) {
            });
        }
    }); 
        $("#fee-type-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                fee_type: {
                    required: true,
                    // nowhitespace: true
                },
                is_root: {
                    required: true
                },
                fee_type_id: {
                    required: function (e) {
                        var is_root = $("#is_root").val();
                        if ((is_root == 0)) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },

            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
    });

    $(document).on('change', '#is_root', function (e) {

        var is_root = $(this).val();
        if ((is_root == 0) && (is_root != ''))
        {
            $("#div_fee_type_id").show();
        } else
        {
            $("#div_fee_type_id").hide();
        }

    });
</script>