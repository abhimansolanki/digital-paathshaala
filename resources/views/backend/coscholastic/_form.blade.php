<!-- Field Options -->
<style>
    .state-error{
        display: block!important;
        margin-top: 10px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    } 
</style>
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.co_scholastic')}}</li>
                </ol>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages')
        @endif
        <div class="bg-light panelBodyy">
            <div class="section-divider mv40" id="imgrouute">
                <span><i class="glyphicon glyphicon-sound-dolby"></i> &nbsp;Co Scholastic Activities &nbsp;</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.co_scholastic_name') !!}<span class="asterisk">*</span></span></label>
                    <label for="co_scholastic_name" class="field prepend-icon">
                        {!! Form::text('co_scholastic_name', old('co_scholastic_name',isset($coscholastic['co_scholastic_name']) ? $coscholastic['co_scholastic_name'] : ''), ['class' => 'gui-input', 'id' => 'co_scholastic_name','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="co_scholastic_name" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('co_scholastic_name')) <p class="help-block">{{ $errors->first('co_scholastic_name') }}</p> @endif
                </div>
                <div class="col-md-3">
                    <label><span class="radioBtnpan">{!! trans('language.co_scholastic_head') !!}</span></label>
                    <label for="co_scholastic_head" class="field prepend-icon">
                        {!! Form::text('co_scholastic_head', old('co_scholastic_head',isset($coscholastic['co_scholastic_head']) ? $coscholastic['co_scholastic_head'] : ''), ['class' => 'gui-input', 'id' => 'co_scholastic_head','pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                        <label for="co_scholastic_head" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('co_scholastic_head')) <p class="help-block">{{ $errors->first('co_scholastic_head') }}</p> @endif
                </div>
                <div class="col-md-6">
                    <label><span class="radioBtnpan">{!! trans('language.description') !!}</span></label>
                    <label for="description" class="field prepend-icon">
                        {!! Form::textarea('description', old('description',isset($coscholastic['description']) ? $coscholastic['description'] : ''), ['class' => 'gui-input', 'id' => 'description','style'=>'height:100px !important']) !!}
                        <label for="description" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                </div>
            </div>
            <div class="panel-footer text-right" style="border-top: none !important">
                {!! Form::submit($submit_button, ['class' => 'button btn-primary','name'=>'save']) !!}

            </div>
        </div>
        <!-- end .form-body section -->
    </div>
    <!-- end .form-footer section -->
</div>
<div id="sub_coscholastic" class="modal commonboxinput" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Sub Co Scholastic</h4>
            </div>
            <div class="modal-body">
                {!! Form::hidden('root_coscholastic_id','', ['class' => '','id' => 'root_coscholastic_id','readonly'=>true]) !!}
                <div class="col-md-5" style="margin-top: 20px;">
                    <label><span class="radioBtnpan">{!! trans('language.co_scholastic_name') !!}<span class="asterisk">*</span></span></label>
                    <label for="sub_co_scholastic_name" class="field prepend-icon" style="margin-top: -10px;">
                        {!! Form::text('sub_co_scholastic_name','', ['class' => '', 'id' => 'sub_co_scholastic_name','required'=>true,'pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                    </label>
                </div>
                <div class="col-md-5" style="margin-top: 20px;">
                    <label><span class="radioBtnpan">{!! trans('language.co_scholastic_head') !!}</span></label>
                    <label for="sub_co_scholastic_head" class="field prepend-icon" style="margin-top: -10px;">
                        {!! Form::text('sub_co_scholastic_head','', ['class' => '', 'id' => 'sub_co_scholastic_head','required'=>true,'pattern'=>'[a-zA-Z][a-zA-Z0-9\s]*']) !!}
                    </label>
                </div>
                <div class="clearfix"></div>
                <span class="state-error" id="scholastic_name_unique" style="display:none;padding-left: 12px;"></span>
                <div class="col-md-10">
                    <label><span class="radioBtnpan">{!! trans('language.description') !!}</span></label>
                    <label for="sub_description" class="field prepend-icon">
                        {!! Form::textarea('sub_description','', ['class' => '', 'id' => 'sub_description']) !!}
                    </label>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer closebtnforstudent" id="">
                <button type="button" class="button btn-primary" id="save_sub_coscholastic">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="close-sub-coscholastic-model">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_coscholastic')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="co-scholastic-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.co_scholastic_name') !!}</th>
                            <th>{!!trans('language.co_scholastic_head') !!}</th>
                            <th>{!!trans('language.action') !!}</th>
                            <th>{!!trans('language.sub_coscholastic') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#co-scholastic-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('coscholastic/data') }}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'co_scholastic_name', name: 'co_scholastic_name'},
                {data: 'co_scholastic_head', name: 'co_scholastic_head'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'sub_coscholastic', name: 'sub_coscholastic', orderable: false, searchable: false}
            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('coscholastic/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'section_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();

                                        } else
                                        {

                                        }
                                    }
                                });
                    }
                }
            });
        });

        $("#co-scholastic").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                co_scholastic_name: {
                    required: true,
                    // nowhitespace: true
                },
                co_scholastic_head: {
                    // nowhitespace: true
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        $(document).on('click', '.add_sub_coscholastic', function () {
            var co_scholastic_id = $(this).attr('co_scholastic_id');
            $("#root_coscholastic_id").val('');
            $("#sub_co_scholastic_name").val('');
            $("#sub_co_scholastic_head").val('');
            $("#sub_description").val('');
            $("#scholastic_name_unique").hide();
            $("#root_coscholastic_id").val(co_scholastic_id);
            $("#sub_coscholastic").modal('show');
        });

        $(document).on('click', '#save_sub_coscholastic', function () {
            $("#scholastic_name_unique").hide();
            if ($("#sub_co_scholastic_name").valid())
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('add-sub-coscholastic')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'root_id': $("#root_coscholastic_id").val(),
                        'co_scholastic_name': $("#sub_co_scholastic_name").val(),
                        'co_scholastic_head': $("#sub_co_scholastic_head").val(),
                        'description': $("#sub_description").val(),
                    },
                    success: function (response) {
                        if (response.status === 'success')
                        {
                            $('#close-sub-coscholastic-model').trigger('click');
                            table.ajax.reload();
                        } else if (response.status === 'error')
                        {
                            $("#scholastic_name_unique").text(response.message);
                            $("#scholastic_name_unique").show();
                        } else
                        {
                            $('#close-sub-coscholastic-model').trigger('click');
                            $('#server-response-message').text('Somthing went wrong, please try again');
                            $('#alertmsg-student').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        }
                    }
                });
            }
        });

    });
</script>