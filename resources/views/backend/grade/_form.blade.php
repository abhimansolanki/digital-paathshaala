<!-- Field Options -->
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">{{trans('language.add_grade')}}</li>
                </ol>
            </div>
        </div>
        @if(Session::has('success'))
        @include('backend.partials.messages')
        @endif
        <div class="bg-light panelBodyy">
            <div class="section-divider mv40">
                <span><i class="fa fa-percent"></i>&nbsp;Grade Detail</span>
            </div>
            <div class="section row" id="spy1">
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.grade_name')}}<span class="asterisk">*</span></span></label>
                    <label for="grade_name" class="field prepend-icon">
                        {!! Form::text('grade_name', old('grade_name',isset($grade['grade_name']) ? $grade['grade_name'] : ''), ['class' => 'gui-input','id' => 'grade_name','pattern'=>'[a-zA-Z][a-zA-Z0-9\s\+\-]*']) !!}
                        <label for="grade_name" class="field-icon">
                            <i class="fa fa-user"></i>
                        </label>
                    </label>
                    @if ($errors->has('grade_name')) 
                    <p class="help-block">{{ $errors->first('grade_name') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{!! trans('language.grade_alias') !!}</span></label>
                    <label for="grade_alias" class="field prepend-icon">
                        {!! Form::text('grade_alias', old('grade_alias',isset($grade['grade_alias']) ? $grade['grade_alias'] : ''), ['class' => 'gui-input', 'id' => 'grade_alias','pattern'=>'[a-zA-Z][a-zA-Z0-9\s\+\-]*']) !!}
                        <label for="grade_alias" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('grade_alias')) <p class="help-block">{{ $errors->first('grade_alias') }}</p> @endif
                </div>
                <div class="col-md-2">
                    <label for="minimum"><span class="radioBtnpan">{{trans('language.minimum')}}<span class="asterisk">*</span></span></label>
                    <label for="minimum" class="field prepend-icon">
                        {!! Form::number('minimum', old('minimum',isset($grade['minimum']) ? $grade['minimum'] : ''), ['class' => 'gui-input', 'id' => 'minimum']) !!}
                        <label for="minimum" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('minimum')) <p class="help-block">{{ $errors->first('minimum') }}</p> @endif
                </div>
                <div class="col-md-2">
                    <label for="maximum"><span class="radioBtnpan">{{trans('language.maximum')}}<span class="asterisk">*</span></span></label>
                    <label for="maximum" class="field prepend-icon">
                        {!! Form::number('maximum', old('maximum',isset($grade['maximum']) ? $grade['maximum'] : ''), ['class' => 'gui-input','id' => 'maximum']) !!}
                        <label for="maximum" class="field-icon">
                            <i class="fa fa-eye"></i>
                        </label>
                    </label>
                    @if ($errors->has('maximum')) <p class="help-block">{{ $errors->first('maximum') }}</p> @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{trans('language.academic_year')}}<span class="asterisk">*</span></span></label>
                    <label class="field select">
                        {!!Form::select('session_id', $grade['arr_session'],isset($grade['session_id']) ?$grade['session_id'] : null, ['class' => 'form-control','id'=>'session_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('session_id')) 
                    <p class="help-block">{{ $errors->first('session_id') }}</p>
                    @endif
                </div>
            </div>
	<div class="section row" id="spy1">
	    <div class="col-md-8">
                    <label><span class="radioBtnpan">Grade Comments<span class="asterisk">*</span></span></label>
                    <label class="field select">
		{!! Form::text('grade_comments',isset($grade['grade_comments']) ? $grade['grade_comments'] : '',['class'=>'form-control','style'=>'']) !!}
	        </label>
	    </div>
	</div>

        </div>
        <!-- end .form-body section -->
        <div class="panel-footer text-right">
            {!! Form::submit($submit_button, ['class' => 'button btn-primary']) !!}
        </div>
    </div>
    <!-- end .form-footer section -->
</div>
<!--View Grade list-->
<div class="tray tray-center tableCenter">
    <div class="">
        <div class="panel panel-visible" id="spy2">
            <div class="panel-heading">
                <div class="panel-title hidden-xs col-md-6">
                    <span class="glyphicon glyphicon-tasks"></span> <span class="" href="{{$redirect_url}}" class="" title="">{{ trans('language.list_grade')}}</span>
                </div>
            </div>
            <div class="panel-body pn">
                <table class="table table-bordered table-striped table-hover" id="grade-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{!!trans('language.grade_name') !!}</th>
                            <th>{!!trans('language.grade_alias') !!}</th>
                            <th>{!!trans('language.minimum') !!}</th>
                            <th>{!!trans('language.maximum') !!}</th>
                            <th>Grade Comments</th>
                            <th>{!!trans('language.action') !!}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

        var table = $('#grade-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{url('grade/data')}}",
            "aaSorting": [[0, "ASC"]],
            columns: [
                {data: 'grade_name', name: 'grade_name'},
                {data: 'grade_alias', name: 'grade_alias'},
                {data: 'minimum', name: 'minimum'},
                {data: 'maximum', name: 'maximum'},
                {data: 'grade_comments', name: 'grade_comments'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ]
        });

        $(document).on("click", ".delete-button", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id");

            bootbox.confirm({
                message: "Are you sure to delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        var token = '{!!csrf_token()!!}';
                        $.ajax(
                                {
                                    url: "{{ url('grade/delete/') }}",
                                    datatType: 'json',
                                    type: 'POST',
                                    data: {
                                        'grade_id': id,
                                    },
                                    success: function (res)
                                    {
                                        if (res.status == "success")
                                        {
                                            table.ajax.reload();

                                        } else if (res.status === "used")
                                        {
                                            $('#server-response-message').text(res.message);
                                            $('#alertmsg-student').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                        }
                                    }
                                });
                    }
                }
            });

        });
        $("#grade-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                grade_name: {
                    required: true,
                    // nowhitespace: true
                },
                session_id: {
                    required: true,
                    // nowhitespace: true
                },
                grade_comments: {
                    required: true,
                    // nowhitespace: true
                },
                minimum: {
                    required: true,
                    notEqualTo: '#maximum',
                    min: 1,
//                    max: 100,
//                    lessThan: '#maximum',
                },
                maximum: {
                    required: true,
                    notEqualTo: '#minimum',
                    min: 1,
                    max: 100,
                    greaterThan: '#minimum',
                },
            },
            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        $.validator.addMethod('greaterThan', function (value, element, param) {
            return this.optional(element) || value > parseInt($(param).val());
        }, 'Maximum marks  must be greater than minimum');

//        $.validator.addMethod('lessThan', function (value, element, param) {
//            return this.optional(element) || value < $(param).val();
//        }, 'Minimum marks  must be less than maximum');

    });
</script>

