<style type="text/css">
    .state-error {
        display: block !important;
        margin-top: 10px !important;
        padding: 0;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13px !important;
        color: #DE888A !important;
    }

    .padding_remove {
        padding: 0px 0px !important;
    }

    .tabletable input {
        text-align: center;
    }

    .form-horizontal .form-group {
        margin-left: 0px !important;
        margin-right: 0px !important;
    }
</style>
@if(!empty($arr_subject))
    <div class="tabletable">
        <div class="bs-component" id="time_table_error" style="display:none;">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="fa fa-remove pr10"></i>
                <span id="time_table_error_message"></span>
            </div>
        </div>
        <table class="table table-bordered" style="font-size: 14px;">
            <thead>
            <tr style="font-size: 14px !important; background-color: #eeeeee;">
                <th style="text-align: center; width:10%;">Exam Type</th>
                <th style="text-align: center; width:30%;">{!!trans('language.subject') !!}</th>
                <th style="text-align: center; width:15%;">Date</th>
                <th style="text-align: center; width:15%;">Start Time</th>
                <th style="text-align: center; width:15%;">End Time</th>
                {{--                <th style="text-align: center; width:15%;">Teacher</th>--}}
            </tr>
            </thead>
            <tbody>
            @php $subject_counter = 0; @endphp
            @foreach($arr_subject as $exam_type_key => $exam_type)
                @php $subject_details = $exam_type['subjects'];
	$counter = 1;
                @endphp
                @foreach($subject_details as $key => $value)
                    @php $subject_counter++; @endphp
                    <tr>
                        @if($counter == 1)
                            <td rowspan="{{ count($subject_details) }}"
                                style='font-weight:bold;'>{!! $exam_type['exam_type'] !!}</td>@endif
                        <td>{!! $value !!}</td>
                        <td>
                            <label for="exam_date" class="field prepend-icon">
                                {!! Form::text('map['.$exam_type_key.']['.$key.'][exam_date]', isset($time_table_subjects[$exam_type_key][$key][0]) ? $time_table_subjects[$exam_type_key][$key][0] : '', ['class' => 'gui-input date_picker group-element checkValid subject-date','rel'=>$exam_type_key.$key, 'readonly', 'id' => 'map['.$exam_type_key.']['.$key.'][exam_date]', 'required' => 'required']) !!}
                                <label for="exam_date" class="field-icon">
                                    <i class="fa fa-eye"></i>
                                </label>
                            </label>
                        </td>
                        <td>
                            <label for="start_time" class="field prepend-icon">
                                {!! Form::time('map['.$exam_type_key.']['.$key.'][start_time]', isset($time_table_subjects[$exam_type_key][$key][1]) ? $time_table_subjects[$exam_type_key][$key][1] : '', ['class' => 'group-element gui-input checkValid subject-start'.$exam_type_key.$key, 'id' => 'map['.$exam_type_key.']['.$key.'][start_time]', 'required' => 'required']) !!}
                                <label for="start_time" class="field-icon">
                                    <i class="fa fa-eye"></i>
                                </label>
                            </label>
                        </td>
                        <td>
                            <label for="end_time" class="field prepend-icon">
                                {!! Form::time('map['.$exam_type_key.']['.$key.'][end_time]', isset($time_table_subjects[$exam_type_key][$key][2]) ? $time_table_subjects[$exam_type_key][$key][2] : '', ['class' => 'group-element gui-input checkValid subject-end'.$exam_type_key.$key, 'id' => 'map['.$exam_type_key.']['.$key.'][end_time]', 'required' => 'required']) !!}
                                <label for="end_time" class="field-icon">
                                    <i class="fa fa-eye"></i>
                                </label>
                            </label>
                        </td>
{{--                        <td>--}}
{{--                            <label for="employee_id" class="field select">--}}
{{--                                {!! Form::select('map['.$exam_type_key.']['.$key.'][employee_id]', $arr_teachers, isset($time_table_subjects[$exam_type_key][$key][3]) ? $time_table_subjects[$exam_type_key][$key][3] : '', ['class' => 'group-element form-control', 'id' => 'map['.$exam_type_key.']['.$key.'][employee_id]', 'required' => 'required']) !!}--}}
{{--                            </label>--}}
{{--                        </td>--}}
                        {!! Form::hidden('map['.$exam_type_key.']['.$key.'][detail_subject_id]', isset($time_table_subjects[$exam_type_key][$key][3]) ? $time_table_subjects[$exam_type_key][$key][3] : '', ['class' => 'gui-input','id' => 'map['.$exam_type_key.']['.$key.'][detail_subject_id]']) !!}

                    </tr>
                    @php $counter++;  @endphp
                @endforeach
            @endforeach
            </tbody>
        </table>
        {!! Form::hidden('subject_counter', $subject_counter , ['class' => 'gui-input','id' => 'subject_counter']) !!}
    </div>
    <script>
        $(".date_picker").datepicker({
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            showButtonPanel: false,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd M yy",
            yearRange: "-10:+50"
        });
    </script>
@endif