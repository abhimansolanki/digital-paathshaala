@if(!empty($details))
{{-- {{ p($details) }} --}}
<div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">
    @include('backend.partials.loader')
    <div class="panel heading-border">
        <div id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <a href="{{url('admin/dashboard')}}"><span class="glyphicon glyphicon-home"></span>Home</a>
                    </li>
                    <li class="crumb-trail">Time Table -> {!! $details['title'] !!} -> Mapping Subject & Teacher </li>
                    {!! Form::hidden('exam_time_table_id', isset($details['exam_time_table_id']) ? $details['exam_time_table_id'] : null, ['class' => 'gui-input','id' => 'exam_time_table_id']) !!}
                    {!! Form::hidden('exam_id', isset($details['exam_id']) ? $details['exam_id'] : null, ['class' => 'gui-input','id' => 'exam_id']) !!}
                </ol>
            </div>
            <div class="topbar-right">
                <a  href="{{ url('admin/exam-time-table') }}" class="button btn-primary text-right pull-right" title="View Student">View Exam Time Table</a>
            </div>
        </div>
        @include('backend.partials.messages')
        <div class="panel-body bg-light">
            <!-- .section-divider -->
            <div class="section row" id="spy1">
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{ trans('language.title')}}</span></label>
                    <label for="" class="field prepend-icon">
                        {!!Form::text('title', $details['title'],['class'=>'form-control','id'=>'title','readonly'=>true])!!}
                    </label>
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{ trans('language.exam')}}<span class="asterisk">*</span></span></label>
                    <label for="exam_id" class="field select">
                        {!!Form::text('exam_name', $details['exam_name'],['class'=>'form-control','id'=>'exam_name','readonly'=>true])!!}
                        {{-- {!!Form::select('exam_id', $time_table_detail['arr_exam'], isset($time_table_detail['exam_id']) ? $time_table_detail['exam_id'] : '', ['class' => 'form-control','id'=>'exam_id'])!!} --}}
                    </label>
                    @if ($errors->has('exam_id')) 
                    <p class="help-block">{{ $errors->first('exam_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{ trans('language.class')}}<span class="asterisk">*</span></span></label>
                    <label for="class_id" class="field select">
                        {!!Form::select('class_id', $details['arr_class'], isset($time_table_detail['class_id']) ? $time_table_detail['class_id'] : '', ['class' => 'form-control','id'=>'class_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('class_id')) 
                    <p class="help-block">{{ $errors->first('class_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan">{{ trans('language.section')}}<span class="asterisk">*</span></span></label>
                    <label for="section_id" class="field select">
                        {!!Form::select('section_id', $details['arr_section'],isset($time_table_detail['section_id']) ? $time_table_detail['section_id'] : '', ['class' => 'form-control','id'=>'section_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                    @if ($errors->has('section_id')) 
                    <p class="help-block">{{ $errors->first('section_id') }}</p>
                    @endif
                </div>
                <div class="col-md-2">
                    <label><span class="radioBtnpan"></span></label>
                    <label for="" class="field prepend-icon">
                        {!! Form::button('Continue', ['class' => 'button btn-primary','style'=>'width:100px;margin-top: 17px;','id'=>'get_time_table_subject']) !!}   
                    </label>
                </div>

            </div>
            <!-- end .section row section -->
        </div>
        <div id="sbject-data-detail" @if($preview_data != '') style="display:inline" @else style="display:none" @endif>
            <div class="section-divider">
                <span><i class="fa fa-book"></i>&nbsp;Time Table</span>
            </div>
	 <strong>Note: Time in 24-hour's formate.</strong>
            <div id="time_table_subject_table" class="classopen mb20" style="width: 100%; margin: auto;">
                {!! isset($preview_data) ? $preview_data : '' !!}
            </div>
            <div class="clearfix"></div>
            <div class="panel-footer text-right">
                {!! Form::button($submit_button, ['class' => 'button btn-primary submitButton validForm']) !!}
            </div>
        </div>
    </div>
</div>
@endif
