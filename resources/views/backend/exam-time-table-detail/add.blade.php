@extends('admin_panel/layout')
<style>
  .errorCtrl {
    background: red;
  }
</style>
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="box box-info">
                {!! Form::open(['files'=>TRUE,'id' => 'time-table-detail-form' , 'method' => 'get', 'class'=>'form-horizontal','url' => $save_url ,'autocomplete'=>'off']) !!}
                @include('backend.exam-time-table-detail._form',[$submit_button])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var exam_class_id = $("#exam_class_id").val();

        $("#exam-class-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             * $("#section_id option").length
             ------------------------------------------ */
            ignore: [],
            rules: {
                class_id: {
                    required: true,
                },
                session_id: {
                    required: true
                },
                'section_id': {
                    required: true
                },
                'exam_type_id[]': {
                    required: true
                },
                "arr_subject_id[]": {
                    required: true
                },
            },
            messages: {
                'arr_subject_id[]': {
                    required: "Select"
                }

            },

            /* @validation error messages 
             ---------------------------------------------- */

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        $(document).on('change', '#class_id', function (e) {
            e.preventDefault();
            var class_id = $('#class_id').val();
            $('#section_id').empty();
            $('#section_id').append('<option value=""> --Select Section --</option>');
            if (class_id !== '')
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    url: "{{url('get-section-list')}}",
                    datatType: 'json',
                    type: 'POST',
                    data: {
                        'class_id': class_id,
                    },
                    beforeSend: function () {
                        $("#LoadingImage").show();
                    },
                    success: function (response) {
                        $("#LoadingImage").hide();
                        var resopose_data = [];
                        resopose_data = response.data;
                        if (response.status === 'success')
                        {
                            $.each(resopose_data, function (key, value) {
                                $('#section_id').append('<option value="' + key + '">' + value + '</option>');
                            });
                        }
                    }
                });
            }

        });
        $(document).on('click', '#get_time_table_subject', function (e) {
            $("#time_table_subject_table").html('');
            $("#sbject-data-detail").hide();
            if ($("#class_id").valid() && $("#section_id").valid())
            {
                getTimeTableSubject();
            }
        });
        function getTimeTableSubject()
        {
            $.ajax({
                url: "{{url('get-time-table-subject-data')}}",
                datatType: 'json',
                type: 'GET',
                data: $("#time-table-detail-form").serialize(),
                beforeSend: function () {
                    //$("#LoadingImage").show();
                },
                success: function (response) {
                    $("#LoadingImage").hide();
                    if (response.status === 'success')
                    {
                        $("#time_table_subject_table").html(response.data);
                        $("#sbject-data-detail").show();
                        //countMaxMarks();
                    } else
                    {
                        if(response.message != '') {
                            $('#server-response-message').text(response.message);                        
                        } else {
                            $('#server-response-message').text('No data available');
                        }
                        $('#alertmsg-student').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }

                }
            });
        }
        $(document).on('change', '#class_id,#section_id', function (e) {
            $("#time_table_subject_table").html('');
            $("#sbject-data-detail").hide();
        });

        $('#time-table-detail-form').validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                exam_id: "required",
                class_id: "required",
                section_id: "required",
            },
            messages: {
                exam_id: "Please select Exam",
                class_id: "Please select class",
                section_id: "Please select section",
            },
            onfocusout : function(element) {
                if ($(element).val()) {
                    $(element).valid();
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(".submitButton").html('Save');
                $( element ).parents( ".input-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(".submitButton").html('Save');
                $( element ).parents( ".input-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                $(".submitButton").html('Save');
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        $(document).on('click', '.validForm', function(){
            if($('#time-table-detail-form').valid()){
            //if(true){
                $('#time_table_error_message').html(null);
                $('#time_table_error').hide();
                var timeTableStatus = checkTimeTableValid();
                if(timeTableStatus.status){
                    $('#time-table-detail-form').submit();
                } else {
                    var message = '';
                    if(timeTableStatus.message == 'time_conflict') {
                        message = 'Invalid start and end time. Please correct it';
                    } else if(timeTableStatus.message == 'date_time_conflict') {
                        message = 'Invalid Time and Date. Please correct it';
                    } else {
                        message = 'Error';
                    }
                    $('#time_table_error_message').html(message);
                    $('#time_table_error').show();
                } 
            }    
        });

        function checkTimeTableValid(){
            var message = 'success';
            var status = true;
            $('.subject-date').each(function() {
                var rel = $(this).attr('rel');
                var date  = $(this).val();
                var start = $('.subject-start'+rel).val();
                var end = $('.subject-end'+rel).val();
                var start_time = Date.parse(date + ' ' + start);
                var end_time = Date.parse(date + ' ' + end);
                var teacher = $('.subject-teacher'+rel).val();
                if(start != '' && end != '' && start_time >= end_time){
                    message = "time_conflict";
                    status = false;
                } else {
                    $('.subject-date').each(function(key, value){
                        var inner_rel = $(this).attr('rel');
                        var inner_date = $(this).val();
                        if($(this).val() != '' && date != ''){
                            if(rel != inner_rel && date == inner_date) {
                                //console.log('InnerLoop --- rel' + rel + ' - inner_rel '+ inner_rel);
                                var inner_start = $('.subject-start'+inner_rel).val();
                                var inner_end = $('.subject-end'+inner_rel).val();
                                var inner_start_time = Date.parse(inner_date + ' ' + inner_start);
                                var inner_end_time = Date.parse(inner_date + ' ' + inner_end);  
                                var inner_teacher = $('.subject-teacher'+inner_rel).val();
                                if(inner_start_time >= start_time && inner_start_time <= end_time || inner_end_time >= start_time && inner_end_time <= end_time){
                                    message = "date_time_conflict";
                                    status = false;
                                }
                            }
                        }
                    });
                }
            });
            return {'status': status, 'message': message};
        }

        function getEmployeeAvailability(date, start_time, end_time, employee_id)
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                url: "{{url('exam-time-table-detail/employee-available')}}",
                datatType: 'json',
                type: 'GET',
                data: {
                    'exam_time_table_id': $('#exam_time_table_id').val(),
                    'class_id': $('#class_id').val(),
                    'section_id': $('#section_id').val(),
                    'exam_id': $('#exam_id').val(),
                    'date': date,
                    'start_time': start_time,
                    'end_time': end_time,
                    'employee_id': employee_id
                },
                beforeSend: function () {
                    //  $("#LoadingImage").show();
                },
                success: function (response) {
                    //$("#LoadingImage").hide();
                    var resopose_data = [];
                }
            });
        }

        $(document).on('change', '.group-element', function(){
        });
    });
</script>
@endsection

