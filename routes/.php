@include("header_thank")

<div class="site_common" id="siteCommoID">
	<div class="container">
		<div class="row">
			<div class="col-md-12 breadcum text-center wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
				<h1>How to Play?</h1>
				<p><a href="{{url('/')}}" title="Home">Home</a> / <span>How to Play?</span></p>
			</div>
		</div>
	</div>
</div>
<!--  Start Live And Tez Button Link Here -->
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-4">
            <div id="topheader">
              <nav class="textClass text-center">
                <ul class="nav navbar-nav">
                    <li ><a href="http://v2rsolution.co.in/gullyy_quiz_new/how_to_play_tez" title="How To Play - Tez">How To Play - Tez</a></li>
                    <li class="active"> <a href="http://v2rsolution.co.in/gullyy_quiz_new/how_to_play_live" title="How To Play - Live">How To Play - Live</a></li>
  <div class="clearfix"></div>
                </ul>
              </nav>
            </div>
<div class="clearfix"></div>
        </div>
    </div>
</div>

  <!--  End Live And Tez Button Link Here -->

  <div class="clearfix"></div>
<div class="container howtoplay_Live" id="faqestions">
   <div class="row">
      <div class="col-md-12">
	 <div class="editor-indent">
<div class="clearfix"></div>
	     <div class="panel-group" id="LiveGullyyT20">
              <h4 class="faqheading liveT20 wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"> GullyyT20 Live : How to Play? </h4>
              <h4 class="faqheading wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"> Download & Sign up </h4>
              <ul class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Download the GullyyT20 App from <a href="https://play.google.com/store/apps/details?id=com.gullyy.gullycricket" title="Download Android App" alt="Download Android App" target="_blank">Play Store</a> or <a href="https://itunes.apple.com/in/app/gullyy/id1366859048?mt=8" title="Download iOS App" alt="Download iOS App" target="_blank">App Store</a></p>
              	<div class="clearfix"></div>
              	</li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Sign up or Sign in using your mobile number and OTP</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>For a new sign up a referral code is not mandatory but if you have a code, you can use it</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>On successful sign up, you will be prompted to enter your name, proceed to update your profile</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Clicking wallet icon next to your name on lobby screen will give you access to your “Profile” which has both Gullyy coins and Gullyy cash wallet</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Update your email and verify it by clicking “verify” next to it</p>	<div class="clearfix"></div></li>
             </ul>





<h4 class="faqheading wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"> Buy Tickets </h4>
              <ul class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Click on buy tickets on the lobby and browse through upcoming live matches</p>
              	<div class="clearfix"></div>
              	</li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Click the plus button “+” to add tickets to your shop cart that is shown below on the same screen.</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>You can add tickets from multiple games and proceed to pay together</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>On the payment screen you will see the total amount to be paid including taxes and you have a choice to pay from your own funds or from the wallet - either cash or coins. If the ticket is free during promotions, nothing would be debited and you get the tickets for free</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>On successful completion of the purchase, your tickets can be found under “My Tickets” accessible  from the lobby</p>	<div class="clearfix"></div></li>
              	
             </ul>
             
             
             
             
             
             <h4 class="faqheading wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"> Shuffle & Edit Tickets </h4>
              <ul class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Tickets with the status as open can be shuffled and edited and should be edited  in order to improve your winning probability</p>
              	<div class="clearfix"></div>
              	</li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>By shuffling tickets, an entirely new ticket is presented before you and you can then edit the ticket.</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>You can add tickets from multiple games and proceed to pay together</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Editing tickets should be basis your understanding of the teams, match conditions, ground size etc.</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Click the open tickets from “My Tickets” and you will have 200 seconds to edit each ticket</p>	<div class="clearfix"></div></li>
              	
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Cells with pencil icon are editable, you can shuffle as many times to get a favorable ticket and use option or shuffle and edit and use your skills of the game and generate a winning ticket</p>	<div class="clearfix"></div></li>
              	
              	
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>All open tickets that you choose not to edit are auto locked 30 minutes before the live match begins</p>	<div class="clearfix"></div></li>
              	
             </ul>
             
             
             
             
             
               <h4 class="faqheading wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"> Game Play</h4>
              <ul class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>During the live match you can play GullyyT20 by accessing tickets from “My Tickets” with match status as live shown in green or click “Go to Live Game” from the main menu</p>
              	<div class="clearfix"></div>
              	</li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Live score is fetched from the server and numbers are auto struck</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Scoreboard is available on the left of the screen that gives updates after each over</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>The game does not expect you to be fast or quick to click the claim option. If your ticket is eligible for a claim, you will be able to by clicking the claim button that turns golden or you can access pending claims from the pending claims option from the main menu</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Claims are active for 24 hours from the trigger point and thereafter they expire and you will not be eligible for the prize</p>	<div class="clearfix"></div></li>
              	
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Cells with pencil icon are editable, you can shuffle as many times to get a favorable ticket and use option or shuffle and edit and use your skills of the game and generate a winning ticket</p>	<div class="clearfix"></div></li>
              	
              	
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>All open tickets that you choose not to edit are auto locked 30 minutes before the live match begins</p>	<div class="clearfix"></div></li>
              	
             </ul>
             
             
             
             
             
             <h4 class="faqheading wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"> Game Play : Prizes </h4>
              <h4 class="faqheading wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"> Jaldi 5</h4>
              <p class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                 <strong>Jaldi means quick</strong>, quick to get the pattern first on your ticket and not about claiming it fast. If you are eligible you will be able to claim immediate 				 
                 or within 24 hours. Jaldi 5 is when the user gets it first not just when he or she just gets anytime it during the game. Multiple users can all get it first all 				 
                together and in such case prize is distributed equally amongst the number of claimants.
                If you are not eligible, the claim button will not turn golden and claim will not show up under “Pending Claims”. It also means someone else got Jaldi 5 on their ticket 
               much before you got it.
               </p>

<h4 class="faqheading liveT201  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"> Row 1</h4>
              <p class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                 Row 1 is the first row as a pattern that gets all strikes. When the user(s) are first to get this pattern, they get a claim trigger. </p>

 <p class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"><strong>Row 1 means quick, </strong> quick to get row 1 first on your ticket and it is not about claiming it fast. If you are eligible you will be able to claim immediately or within 24 hours.</p>

<p>Multiple users can all get it first all together and in such case prize is distributed.</p>

<p>If you are not eligible, the claim button will not turn golden and claim will not show up under “Pending Claims”. It also means someone else got Row 1 on their ticket much before you got it.

               </p>
               
               
               <h4 class="faqheading liveT201  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">Row 2</h4>
              <p class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
                Row 2 is the second row as a pattern that gets all strikes. When the user(s) are first to get this pattern, they get a claim trigger.</p>

 <p class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"><strong> Row 2 means quick,</strong>  quick to get row 2 first on your ticket and it is not about claiming it fast. If you are eligible you will be able to claim immediately or within 24 hours.
</p>

Multiple users can all get it first all together and in such case prize is distributed.
If you are not eligible, the claim button will not turn golden and claim will not show up under “Pending Claims”. It also means someone else got Row 2 on their ticket much before you got it.

               </p>
               
               
               
               
                <h4 class="faqheading liveT201  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">Row 3</h4>
              <p class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
               Row 3 is the third row as a pattern that gets all strikes. When the user(s) are first to get this pattern, they get a claim trigger.</p>

 <p class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"><strong> Row 3 means quick,</strong>  , quick to get row 3 first on your ticket and it is not about claiming it fast. If you are eligible you will be able to claim immediately or within 24 hours.
</p>

Multiple users can all get it first all together and in such case prize is distributed.
If you are not eligible, the claim button will not turn golden and claim will not show up under “Pending Claims”. It also means someone else got Row 3 on their ticket much before you got it.

               </p>
               
               
               
                 <h4 class="faqheading wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">GullyyT20 free hit</h4>
              <p class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
              On a strike of 11 cells at any time during the match you are eligible to win 20 Gullyy coins. Unlike Jaldi this isn’t about getting it first. Anyone who gets 11 cells struck is eligible.
               </p>
               
                <h4 class="faqheading liveT201  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">Full House</h4>
              <p class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
              Full House is when all 15 cells get a strike. When the user(s) are first to get this pattern, they get a claim trigger. </p>

 <p>Full House means quick, quick to get Full House first on your ticket and it is not about claiming it fast. If you are eligible you will be able to claim immediately or within 24 hours. </p>
 </p>
Multiple users can all get it first all together and in such case prize is distributed.

If you are not eligible, the claim button will not turn golden and claim will not show up under “Pending Claims”. It also means someone else got Full House on their ticket much before you got it.

               </p>
               
               
               
               <h4 class="faqheading liveT201  wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"> Redemption</h4>
              <ul class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Cash and coins are transferred to your respective wallets immediately on winning</p>
              	<div class="clearfix"></div>
              	</li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Coin and cash balances can be seen under your profile option</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Cash allows redemption option that is shown next to it</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Click redeem, your email should be a verified email and thereafter you will get a claim verification email</p>	<div class="clearfix"></div></li>
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>Upon verifying your claim, Gullyy will process your request in 48 hours during business days</p>	<div class="clearfix"></div></li>
              	
              	<li>{{ Html::image('public/images/1ball.png','Single Coin', array('title'=>'Single Coin')) }}<p>You can choose to cash out using a gift voucher (minimum Rs. 50/-) or get cash to your Paytm wallet</p>	<div class="clearfix"></div></li>
              	
              	
              
              	
             </ul>
             
             </div>
	  </div>

	</div>
    </div>
</div>


@include('second_footer')
@include('footer')