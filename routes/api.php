<?php

    use Illuminate\Http\Request;

/*
      |--------------------------------------------------------------------------
      | API Routes
      |--------------------------------------------------------------------------
      |
      | Here is where you can register API routes for your application. These
      | routes are loaded by the RouteServiceProvider within a group which
      | is assigned the "api" middleware group. Enjoy building your API!
      |
     */

    Route::middleware('auth:api')->get('/user', function (Request $request)
    {
        return $request->user();
    });

    Route::get('/admin/{id}', 'api\ApiController@show');
    Route::get('/admin', 'api\ApiController@showall');

// parent's app api's 
    Route::post('/login', 'api\ApiController@parentLogin');
    Route::get('/events-list', 'api\ApiController@eventsList');
    Route::get('/get-school-gallery', 'api\ApiController@getSchoolGallery');
    Route::get('/leave-list', 'api\ApiController@leaveList');
    Route::get('/holidays-list', 'api\ApiController@holidaysList');
    Route::post('/save-student-leave', 'api\ApiController@saveStudentLeave');
    Route::post('/save-complaint', 'api\ApiController@saveComplaint');
    Route::get('/get-complaint-list', 'api\ApiController@complaintList');
    Route::post('/save-complaint-message', 'api\ApiController@saveComplaintMessage');
    Route::get('/get-work-diary', 'api\ApiController@getStudentWork');
    Route::get('/get-story', 'api\ApiController@storyList');
    Route::get('/get-student-progress', 'api\ApiController@getStudentProgress');
//    Route::get('/get-student-profile', 'api\ApiController@getStudentProfile');
    Route::get('/get-school-profile', 'api\ApiController@getSchoolProfile');
    Route::get('/get-exam-marks', 'api\ApiController@getStudentExamMarks');
    Route::get('/get-exam-time-table', 'api\ApiController@getExamTimeTable');
    Route::get('/get-student-attendance', 'api\ApiController@getStudentAttendance');
    
// teacher's app api's
    Route::post('/add-student-attendance', 'api\ApiController@addStudentAttendance');
    Route::post('/save-work', 'api\ApiController@saveStudentWork');
    Route::post('/save-student-progress', 'api\ApiController@saveStudentProgress');

// common api's 
    Route::get('/get-syllabus', 'api\ApiController@getStudentSyllabus');
    Route::get('/get-notice', 'api\ApiController@noticeList');
    Route::post('/save-user-device', 'api\ApiController@saveUserDevice');
    Route::post('/save-notification-status', 'api\ApiController@saveNotificationStatus');
    Route::post('/event', 'api\ApiController@eventsDetails');
    Route::post('/notice', 'api\ApiController@noticeDetails');
    Route::post('/work-diary', 'api\ApiController@workDiaryDetails');


    Route::get('/get-class-list', 'api\ApiController@classList');
    Route::get('/get-section-list', 'api\ApiController@sectionList');
    Route::get('/get-subject-list', 'api\ApiController@subjectList');
    
    // notification api
    Route::get('/get-unvisited-notification-device', 'api\ApiController@getUnvisitedDevice');
    Route::post('/update-visited-status', 'api\ApiController@updateVisitedStatus');
    
    Route::get('/forgot-password', 'api\ApiController@forgotPassword');
    Route::post('/change-password', 'api\ApiController@changePassword');
    Route::get('/logout', 'api\ApiController@logout');

//    Route::get('/get-fee-details','api\ApiController@getFeeDetails');
    Route::get('/get-fee-details-new','api\ApiController@getFeeDetailsNew');
//    Route::post('/pay-fees','api\ApiController@payFees');
    Route::post('/pay-fees-new','api\ApiController@payFeesNew');

    Route::get('/get-student-details','api\ApiController@getStudentDetails');

    // EClass room Apis
    Route::get('/get-chapter-list', 'api\ApiController@chapterList');
    Route::get('/get-topic-list', 'api\ApiController@topicList');
    Route::get('/e-book','api\ApiController@getEBook');
    Route::get('/e-book-details','api\ApiController@getEBookDetails');

    // Store Subscription details
    Route::post('/store-subscription-details','api\ApiController@storeSubscriptionDetails');

    Route::post('/payment/order-create', 'api\ApiController@orderCreate');

    Route::get("/e-class-details", 'api\ApiController@getEClassDetails');

    Route::get("/change-password-admin", 'api\ApiController@changePasswordForAdmin');

    