<?php

    /*
      |--------------------------------------------------------------------------
      | Web Routes
      |--------------------------------------------------------------------------
      |
      | Here is where you can register web routes for your application. These
      | routes are loaded by the RouteServiceProvider within a group which
      | contains the "web" middleware group. Now create something great!
      |
     */
//    Auth::routes();
//    Route::get('/', function ()
//    {
//        return view('welcome');
//    });


    Route::get('/clear-cache', function ()
    {
        Artisan::call('cache:clear');
        Artisan::call('config:cache');
        return redirect('/');
        // return wherever you want
    });

    /*
     * Down live website  
     */
    Route::get('/site-down', function ()
    {
        Artisan::call('down');
        return redirect('/');
        // return wherever you want
    });

    /*
     * Up live website  
     */
    Route::get('/site-up', function ()
    {
        Artisan::call('up');
        return redirect('/');
        // return wherever you want
    });



    Route::get('cron', 'CronController@sendNotification');
    Route::get('/notification-event/', 'CronController@pushNotificationEvent');
    Route::get('/notification-notice', 'CronController@pushNotificationNotice');

    Route::group(['middleware' => ['guest']], function ()
    {
        Route::get('/', 'backend\Auth\LoginController@getLoginForm');
        Route::get('admin', 'backend\Auth\LoginController@getLoginForm');
        Route::get('admin/login', 'backend\Auth\LoginController@getLoginForm');
        Route::get('privacy-policy', 'backend\Auth\LoginController@privacyPolicy')->name("privacy_policy");

        /*
         * admin password reset with link
         */
        Route::get('password/reset', 'backend\Auth\ForgotPasswordController@showLinkRequestForm');
        Route::post('password/email', 'backend\Auth\ForgotPasswordController@sendResetLinkEmail');
        Route::get('password/reset/{token}/{email}', 'backend\Auth\ResetPasswordController@showResetForm');
        Route::post('password/reset', 'backend\Auth\ResetPasswordController@reset');
    });

    Route::group(['middleware' => ['ifadmin']], function ()
    {
        // login, register, password reset
        Route::get('admin', 'backend\Auth\LoginController@getLoginForm');
        Route::get('admin/login', 'backend\Auth\LoginController@getLoginForm');
        Route::post('admin/login', 'backend\Auth\LoginController@login');
        Route::get('privacy-policy', 'backend\Auth\LoginController@privacyPolicy')->name("privacy_policy");
    });

    // cron url
    Route::get('admin/app-access', 'backend\StudentController@resetAppAccessStatus');

    Route::group(['middleware' => ['admin']], function ()
    {
        // dashboard , logout, and all modules under admin user
        Route::post('admin/logout', 'backend\Auth\LoginController@getLogout');
        Route::get('admin/dashboard', 'backend\AdminUserController@dashboard');
        // password change
        Route::get('admin/password/change', 'backend\Auth\ResetPasswordController@showChangePasswordForm');
        Route::post('admin/password-change', 'backend\Auth\ResetPasswordController@changePassword');

        //test mail
        Route::get('admin/test', 'backend\StudentController@test');
        Route::get('admin/transport-receipt/{id?}', 'backend\VehicleController@printTransportReceipt');


        Route::get('dummy/test', 'backend\StudentController@testData');
        Route::get('subject_group', 'backend\SubjectController@subjectGroupData');

        //Route::get('dummy/test', 'backend\StudentController@testData');
        // school
        Route::get('admin/school', 'backend\SchoolController@add');
        Route::post('admin/school/save/', 'backend\SchoolController@save');

        // session
        Route::get('admin/session/{id?}', 'backend\SessionController@add');
        Route::post('admin/session/save/{id?}', 'backend\SessionController@save');
//        Route::get('admin/session', 'backend\SessionController@index');
        Route::get('session/data', 'backend\SessionController@anyData');
//        Route::post('session/delete/{id?}', 'backend\SessionController@destroy');

        // school history (backup) data        
        // Route::get('admin/session-data-forward', 'backend\HistoryController@sessionDataForward');
        // Route::post('admin/session-data-forward-save', 'backend\HistoryController@sessionDataForwardSave');

        // Route::get('admin/history', 'backend\HistoryController@gradeHistory');

        // student class
        Route::get('admin/classes/{id?}', 'backend\ClassesController@add');
        Route::post('admin/classes/save/{id?}', 'backend\ClassesController@save');
//        Route::get('admin/classes', 'backend\ClassesController@index');
        Route::get('classes/data', 'backend\ClassesController@anyData');
        Route::post('classes/delete/', 'backend\ClassesController@destroy');
        Route::post('get-class-order', 'backend\ClassesController@getClassOrder');
        Route::get('class-order', 'backend\ClassesController@setClassOrder');
        Route::post('class-section/delete/', 'backend\ClassesController@deleteSectionClass');
        Route::post('get-section-strength', 'backend\ClassesController@getClassSectionStrength');

        // student class section
        Route::get('admin/section/{id?}', 'backend\SectionController@add');
        Route::post('admin/section/save/{id?}', 'backend\SectionController@save');
//        Route::get('admin/section', 'backend\SectionController@index');
        Route::get('section/data', 'backend\SectionController@anyData');
        Route::post('section/delete/', 'backend\SectionController@destroy');

        // school bank
        Route::get('admin/bank/{id?}', 'backend\BankController@add');
        Route::post('admin/bank/save/{id?}', 'backend\BankController@save');
//        Route::get('admin/bank', 'backend\BankController@index');
        Route::get('bank/data', 'backend\BankController@anyData');
        Route::post('bank/delete/', 'backend\BankController@destroy');

        // school student type
        Route::get('admin/student-type/add/{id?}', 'backend\StudentTypeController@add');
        Route::post('admin/student-type/save/{id?}', 'backend\StudentTypeController@save');
        Route::get('admin/student-type', 'backend\StudentTypeController@index');
        Route::get('student_type/data', 'backend\StudentTypeController@anyData');

        // school student enquire
        Route::get('admin/student-enquire/add/{id?}', 'backend\StudentEnquireController@add');
        Route::post('admin/student-enquire/save/{id?}', 'backend\StudentEnquireController@save');
        Route::get('admin/student-enquire', 'backend\StudentEnquireController@index');
        Route::get('student-enquire/data', 'backend\StudentEnquireController@anyData');
        Route::post('student-enquire/delete/', 'backend\StudentEnquireController@destroy');
        Route::get('admin/student-enquire/view-print/{id?}', 'backend\StudentEnquireController@viewPrint');
        Route::get('admin/birthday', 'backend\BirthdayController@student');
        Route::get('student-birthday', 'backend\BirthdayController@studentBirthday');
        //convert enquiry into admission
        Route::get('admin/student/add/{id?}/{type?}', 'backend\StudentController@add');

        // school student
        Route::get('admin/student/add/{id?}', 'backend\StudentController@add');
        Route::post('admin/student/save/{id?}', 'backend\StudentController@save');
        Route::get('admin/student', 'backend\StudentController@index');
        Route::get('student/data', 'backend\StudentController@anyData');
        Route::post('student/delete/', 'backend\StudentController@destroy');
        Route::get('admin/admission-pdf/{id?}', 'backend\StudentController@admissionPdfPrint');



        // student attendance by admin
        Route::get('admin/student-attendance/add/{id?}', 'backend\StudentAttendanceController@add');
        Route::get('admin/student-attendance-view', 'backend\StudentAttendanceController@index');
        Route::get('student-attendance-data', 'backend\StudentAttendanceController@anyData');
        Route::post('admin/student-attendance-save/{id?}', 'backend\StudentAttendanceController@save');
        Route::get('get-student-table', 'backend\StudentAttendanceController@studentTableData');

        /* Attendance Report */
        Route::get('admin/attendance-register/{view_type?}', 'backend\StudentAttendanceController@attendanceRegister');
        Route::get('attendance_register_summary', 'backend\StudentController@attendanceRegisterSummary');
        Route::get('attendance-table-header/{view_type?}/{c_date?}', 'backend\StudentAttendanceController@tableHeaderCol');
        Route::get('admin/view-student-attendance/{id?}', 'backend\StudentAttendanceController@studentAttendanceDetail');
        Route::get('admin/student-attendance-calendar/{id?}/{start?}/{end?}', 'backend\StudentAttendanceController@studentAttendanceCalendar');
        Route::get('admin/student-absence-summary', 'backend\StudentAttendanceController@studentAbsenceSummary');
        Route::get('student_absence_summary_data', 'backend\StudentAttendanceController@studentAbsenceSummaryData');
        Route::get('attendance_register', 'backend\StudentAttendanceController@attendanceRegisterReport');
        Route::get('admin/attendance-summary/{type?}', 'backend\StudentAttendanceController@studentAttendanceSummary');
        Route::get('student_attendance_summary_data', 'backend\StudentAttendanceController@studentAttendanceSummaryData');



        // Fee Circular
        Route::get('admin/fee-circular/{id?}', 'backend\FeeCircularController@add');
        Route::post('admin/fee-circular/save/{id?}', 'backend\FeeCircularController@save');
//        Route::get('admin/fee-circular', 'backend\FeeCircularController@index');
        Route::get('fee-circular/data', 'backend\FeeCircularController@anyData');
        Route::post('fee-circular/delete/', 'backend\FeeCircularController@destroy');

        // Fee Type
        Route::get('admin/fee-type/{id?}', 'backend\FeeTypeController@add');
        Route::post('admin/fee-type/save/{id?}', 'backend\FeeTypeController@save');
//        Route::get('admin/fee-type', 'backend\FeeTypeController@index');
        Route::get('fee-type/data', 'backend\FeeTypeController@anyData');
        Route::post('fee-type/delete/', 'backend\FeeTypeController@destroy');
        Route::post('set-fee-type-order', 'backend\FeeTypeController@setFeeOrder');
        Route::post('get-fee-type-order', 'backend\FeeTypeController@getFeeOrder');

        // Fee Parameter
        Route::get('admin/fee-parameter/add/{id?}', 'backend\FeeController@add');
        Route::post('admin/fee-parameter/save/{id?}', 'backend\FeeController@save');
        Route::get('admin/fee-parameter', 'backend\FeeController@index');
        Route::get('fee-parameter/data', 'backend\FeeController@anyData');
        Route::post('fee-parameter/delete/', 'backend\FeeController@destroy');
        Route::get('get-class-fee-data/{session_id?}/{class_id?}', 'backend\FeeController@getClassFee');

        // Student Fee Receipt
        //pay student fee
        Route::get('admin/student-fee-receipt/add/{id?}', 'backend\StudentFeeReceiptController@add');
        Route::get('admin/student-fee-receipt/save/{id?}', 'backend\StudentFeeReceiptController@save');

        //pay student new fee
        Route::get('admin/student-fee-receipt-new/add/{id?}', 'backend\StudentFeeReceiptNewController@add');
        Route::get('admin/student-fee-receipt-new/save/{id?}', 'backend\StudentFeeReceiptNewController@save');

        //pay student new transport fee
        Route::get('admin/student-transport-fee-receipt/add/{id?}', 'backend\StudentTransportFeeReceiptController@add');
        Route::get('admin/student-transport-fee-receipt/save/{id?}', 'backend\StudentTransportFeeReceiptController@save');
        Route::get('get-student-transport-fee-details', 'backend\StudentTransportFeeReceiptController@studentFeeDetail');
        Route::get('student_transport_fee/fee-installment/edit/', 'backend\StudentTransportFeeReceiptController@editFeeInstallment');
        Route::get('student_transport_fee/print-receipt/{student_id}/{id}', 'backend\StudentTransportFeeReceiptController@printReceipt')->name("student_transport_fee.print.receipt");

        // school class subjects
        Route::get('admin/subject', 'backend\SubjectController@add');
        Route::get('admin/subject/view', 'backend\SubjectController@index');
        Route::get('get-class-subject-data', 'backend\SubjectController@getClassSubjectData');

        // school transport(route)
        Route::get('admin/route/add/{id?}', 'backend\RouteController@add');
        Route::post('admin/route/save/{id?}', 'backend\RouteController@save');
        Route::get('admin/route', 'backend\RouteController@index');
        Route::get('route/data', 'backend\RouteController@anyData');
        Route::post('route/delete/', 'backend\RouteController@destroy');
        Route::post('remove-route-stop/delete/', 'backend\RouteController@deleteRouteStop');

        // school transport(stop-point)
        Route::get('admin/stop-point/add/{id?}', 'backend\StopPointController@add');
        Route::post('admin/stop-point/save/{id?}', 'backend\StopPointController@save');
        Route::get('admin/stop-point', 'backend\StopPointController@index');
        Route::get('stop-point/data', 'backend\StopPointController@anyData');
        Route::post('stop-point/delete/', 'backend\StopPointController@destroy');

        // school transport(vehicle-type)
        Route::get('admin/vehicle-type/add/{id?}', 'backend\VehicleTypeController@add');
        Route::post('admin/vehicle-type/save/{id?}', 'backend\VehicleTypeController@save');
        Route::get('admin/vehicle-type', 'backend\VehicleTypeController@index');
        Route::get('vehicle-type/data', 'backend\VehicleTypeController@anyData');
        Route::post('vehicle-type/delete/', 'backend\VehicleTypeController@destroy');

        // school transport(vehicle-provider)
        Route::get('admin/vehicle-provider/add/{id?}', 'backend\VehicleProviderController@add');
        Route::post('admin/vehicle-provider/save/{id?}', 'backend\VehicleProviderController@save');
        Route::get('admin/vehicle-provider', 'backend\VehicleProviderController@index');
        Route::get('vehicle-provider/data', 'backend\VehicleProviderController@anyData');
        Route::post('vehicle-provider/delete/', 'backend\VehicleProviderController@destroy');

        // school transport(vehicle)
        Route::get('admin/vehicle/add/{id?}', 'backend\VehicleController@add');
        Route::post('admin/vehicle/save/{id?}', 'backend\VehicleController@save');
        Route::get('admin/vehicle', 'backend\VehicleController@index');
        Route::get('vehicle/data', 'backend\VehicleController@anyData');
        Route::post('vehicle/delete/', 'backend\VehicleController@destroy');

        /* transport report datatable */
        Route::post('admin/get-vehicle-driver', 'backend\VehicleController@vehicleDriver');
        Route::get('admin/transport-report', 'backend\VehicleController@transportReport');
        Route::get('transport-report-data', 'backend\VehicleController@transportReportData');
        Route::get('admin/route-report-classwise', 'backend\VehicleController@routeReport');
        Route::get('route-report-classwise-data', 'backend\VehicleController@routeReportData');

        // school transport(vehicle-assigned)
        Route::get('admin/student-vehicle-assigned/add/{id?}', 'backend\StudentVehicleAssignedController@add');
        Route::post('admin/student-vehicle-assigned/save/{id?}', 'backend\StudentVehicleAssignedController@save');
        Route::get('admin/student-vehicle-assigned', 'backend\StudentVehicleAssignedController@index');
        Route::get('student-vehicle-assigned/data', 'backend\StudentVehicleAssignedController@anyData');
        Route::post('student-vehicle-assigned/delete/', 'backend\StudentVehicleAssignedController@destroy');
        // Unassigned student from assign list in add mode
        Route::get('vehicle-unassigned/delete/', 'backend\StudentVehicleAssignedController@vehicleUnassigned');

        // school exam(grade)
        Route::get('admin/grade/{id?}', 'backend\GradeController@add');
        Route::post('admin/grade/save/{id?}', 'backend\GradeController@save');
//        Route::get('admin/grade', 'backend\GradeController@index');
        Route::get('grade/data', 'backend\GradeController@anyData');
        Route::post('grade/delete/', 'backend\GradeController@destroy');

        // school event(gallery)
        Route::get('admin/event/add/{id?}', 'backend\EventController@add');
        Route::post('admin/event/save/{id?}', 'backend\EventController@save');
        Route::get('admin/event', 'backend\EventController@index');
        Route::get('event/data', 'backend\EventController@anyData');
        Route::post('event/delete/', 'backend\EventController@destroy');

        // school holiday
        Route::get('admin/holiday/{id?}', 'backend\HolidayController@add');
        Route::post('admin/holiday/save/{id?}', 'backend\HolidayController@save');
        Route::get('admin/holiday', 'backend\HolidayController@index');
        Route::get('holiday/data', 'backend\HolidayController@anyData');
        Route::post('holiday/delete/', 'backend\HolidayController@destroy');

        // school rooms
        Route::get('admin/room/add/{id?}', 'backend\RoomController@add');
        Route::post('admin/room/save/{id?}', 'backend\RoomController@save');
        Route::get('admin/room', 'backend\RoomController@index');
        Route::get('room/data', 'backend\RoomController@anyData');
        Route::post('room/delete/', 'backend\RoomController@destroy');

        // school sms template
        Route::get('admin/sms-template/add/{id?}', 'backend\SmsTemplateController@add');
        Route::post('admin/sms-template/save/{id?}', 'backend\SmsTemplateController@save');
        Route::get('admin/sms-template', 'backend\SmsTemplateController@index');
        Route::get('sms-template/data', 'backend\SmsTemplateController@anyData');
        Route::post('sms-template/delete/', 'backend\SmsTemplateController@destroy');

        // school staff(teaching and non-teaching)
        Route::get('admin/employee/add/{id?}', 'backend\EmployeeController@add');
        Route::post('admin/employee/save/{id?}', 'backend\EmployeeController@save');
        Route::get('admin/employee', 'backend\EmployeeController@index');
        Route::get('employee/data', 'backend\EmployeeController@anyData');
        Route::post('employee/delete/', 'backend\EmployeeController@destroy');

        // school staff Shift(employee)
        Route::get('admin/shift/{id?}', 'backend\ShiftController@add');
        Route::post('admin/shift/save/{id?}', 'backend\ShiftController@save');
        Route::get('shift/data', 'backend\ShiftController@anyData');
        Route::post('shift/delete/', 'backend\ShiftController@destroy');

        // school Department Shift(employee)
        Route::get('admin/department/{id?}', 'backend\DepartmentController@add');
        Route::post('admin/department/save/{id?}', 'backend\DepartmentController@save');
        Route::get('department/data', 'backend\DepartmentController@anyData');
        Route::post('department/delete/', 'backend\DepartmentController@destroy');

        // school exam(create-exam)
        Route::get('admin/create-exam', 'backend\CreateExamController@index');
        Route::get('admin/create-exam/add/{id?}', 'backend\CreateExamController@add');
        Route::post('admin/create-exam/save/{id?}', 'backend\CreateExamController@save');
        Route::get('create-exam/data', 'backend\CreateExamController@anyData');
        Route::post('create-exam/delete/', 'backend\CreateExamController@destroy');

        // school exam(exam class mapping)
        Route::get('admin/exam-class/{exam_id}/{class_exam_id?}', 'backend\ExamClassController@add');
        Route::post('admin/exam-class/save/{exam_id}/{class_exam_id?}', 'backend\ExamClassController@save');
        Route::get('exam-classes/data', 'backend\ExamClassController@anyData');
        Route::post('exam-class/delete/', 'backend\ExamClassController@destroy');

        // school exam(exam type)
        Route::get('admin/exam-type/{id?}', 'backend\ExamTypeController@add');
        Route::post('admin/exam-type/save/{id?}', 'backend\ExamTypeController@save');
        Route::get('exam-type/data', 'backend\ExamTypeController@anyData');
        Route::post('exam-type/delete/', 'backend\ExamTypeController@destroy');

        // school exam(exam category)
        Route::get('admin/exam-category/{id?}', 'backend\ExamCategoryController@add');
        Route::post('admin/exam-category/save/{id?}', 'backend\ExamCategoryController@save');
        Route::get('exam-category/data', 'backend\ExamCategoryController@anyData');
        Route::post('exam-category/delete/', 'backend\ExamCategoryController@destroy');

        // student exam marks
        Route::get('admin/student-mark/add/{id?}', 'backend\StudentMarkController@add');
        Route::get('admin/student-mark/', 'backend\StudentMarkController@index');
        Route::get('student-mark/data', 'backend\StudentMarkController@anyData');
        Route::post('admin/student-mark/save/{id?}', 'backend\StudentMarkController@save');
        Route::get('admin/student-mark/view/{id?}', 'backend\StudentMarkController@viewData');


        // student exam roll no
        Route::get('admin/exam-roll-no/', 'backend\ExamRollNumberController@add');
        Route::post('admin/exam-roll-no/save/', 'backend\ExamRollNumberController@save');
        Route::post('get-student-roll-no', 'backend\ExamRollNumberController@StudentExamRollNo');
        Route::get('student-roll-no-data', 'backend\ExamRollNumberController@getStudentExamRollNo');
        Route::get('check-roll-no', 'backend\ExamRollNumberController@checkRollNo');

        // school syllabus(exam & monthly syllabus)
        Route::get('admin/syllabus/add/{id?}', 'backend\SyllabusController@add');
        Route::post('admin/syllabus/save/{id?}', 'backend\SyllabusController@save');
        Route::get('admin/syllabus', 'backend\SyllabusController@index');
        Route::get('syllabus/data', 'backend\SyllabusController@anyData');
        Route::post('syllabus/delete/', 'backend\SyllabusController@destroy');
        Route::get('get-class-exam-subject/{session_id?}/{class_id?}/{section_id?}', 'backend\SyllabusController@getClassExam');

        // student work diary
        Route::get('admin/work-diary/add/{id?}', 'backend\WorkDiaryController@add');
        Route::post('admin/work-diary/save/{id?}', 'backend\WorkDiaryController@save');
        Route::get('admin/work-diary', 'backend\WorkDiaryController@index');
        Route::get('work-diary/data', 'backend\WorkDiaryController@anyData');
        Route::post('work-diary/delete/', 'backend\WorkDiaryController@destroy');
        Route::get('get-class-subject/{class_id?}/{section_id?}', 'backend\WorkDiaryController@getClassSubject');

        // student Leave
        Route::get('admin/student-leave', 'backend\StudentLeaveController@index');
        Route::get('student-leave/data', 'backend\StudentLeaveController@anyData');


        // school story
        Route::get('admin/story/{id?}', 'backend\StoryController@add');
        Route::post('admin/story/save/{id?}', 'backend\StoryController@save');
        Route::get('story/data', 'backend\StoryController@anyData');
        Route::post('story/delete/', 'backend\StoryController@destroy');

        // school notice
        Route::get('admin/notice/{id?}', 'backend\NoticeController@add');
        Route::post('admin/notice/save/{id?}', 'backend\NoticeController@save');
        Route::get('notice/data', 'backend\NoticeController@anyData');
        Route::post('notice/delete/', 'backend\NoticeController@destroy');

        // school co-scholastic master
        Route::get('admin/coscholastic/{id?}', 'backend\CoScholasticController@add');
        Route::post('admin/coscholastic/save/{id?}', 'backend\CoScholasticController@save');
        Route::get('coscholastic/data', 'backend\CoScholasticController@anyData');
        Route::post('coscholastic/delete/', 'backend\CoScholasticController@destroy');

        // class co-scholastic mapping
        Route::get('admin/class-coscholastic/{id?}', 'backend\ClassCoScholasticController@add');
        Route::post('admin/class-coscholastic/save/{id?}', 'backend\ClassCoScholasticController@save');
        Route::get('class-coscholastic/data', 'backend\ClassCoScholasticController@anyData');
        Route::post('class-coscholastic/delete/', 'backend\ClassCoScholasticController@destroy');

        // Complaint module data        
        Route::get('admin/complaint', 'backend\ComplaintController@index');
        Route::get('admin/complaint-detail/{id?}', 'backend\ComplaintController@getComplaintDetail');
        Route::get('complaint/data', 'backend\ComplaintController@anyData');
        Route::post('save-complaint-chat', 'backend\ComplaintController@saveComplaintChat');
        Route::get('save-complaint-status/{id}/{status}', 'backend\ComplaintController@saveComplaintStatus');

        // school exam time table
        Route::get('admin/exam-time-table', 'backend\ExamTimeTableController@index');
        Route::get('admin/exam-time-table/add/{id?}', 'backend\ExamTimeTableController@add');
        Route::post('admin/exam-time-table/save/{id?}', 'backend\ExamTimeTableController@save');
        Route::get('exam-time-table/data', 'backend\ExamTimeTableController@anyData');
        Route::post('exam-time-table/delete/', 'backend\ExamTimeTableController@destroy');

        /*
         * Exam time table detail means Time Table mapping with  class, section
         */
        //Map With Exam Time Table 
        Route::get('admin/exam-time-table-detail/{id?}', 'backend\ExamTimeTableDetailController@index');
        Route::get('admin/exam-time-table-detail/add/{id}/{detail_id?}', 'backend\ExamTimeTableDetailController@add');
        Route::get('exam-time-table-detail/save/{id?}', 'backend\ExamTimeTableDetailController@save');
        Route::get('exam-time-table-detail/delete', 'backend\ExamTimeTableDetailController@destroy');
        Route::get('exam-time-table-detail/employee-available', 'backend\ExamTimeTableDetailController@getEmployeeAvailability');
        Route::get('get-time-table-subject-data', 'backend\ExamTimeTableDetailController@getTimeTableSubject');

        // student due fee detail
        Route::get('admin/student-due-fee', 'backend\StudentFeeReceiptController@studentDueFee');
        Route::get('student_due_fee_data', 'backend\StudentFeeReceiptController@studentDueFeeData');

        Route::post('student-set-accessbility-status', 'backend\StudentFeeReceiptController@studentSetAccessbilityStatus');

        // student outstanding due fee detail
        Route::get('admin/student/total-fee-outstanding', 'backend\StudentFeeReceiptController@totalFeeOutStanding');
        Route::get('total-fee-outstanding-data', 'backend\StudentFeeReceiptController@totalFeeOutStandingData');
        Route::get('admin/student-due-transport-fee', 'backend\StudentFeeReceiptController@studentTransportDueFee');
        Route::get('student_due_transport_fee_data', 'backend\StudentFeeReceiptController@studentTransportDueFeeData');
        // ajax request
        Route::post('check-enquire-contact', 'backend\StudentEnquireController@checkParentContact');
        Route::post('verify_mobile_number', 'backend\StudentController@checkParentContact');
        Route::post('get-enquire', 'backend\StudentEnquireController@enquireData');
        Route::post('get-student-list', 'backend\StudentController@studentList');
        Route::get('get-employee-list', 'backend\EmployeeController@getEmployee');
        Route::post('get-parent-student', 'backend\StudentFeeReceiptController@parentStudentList');
        Route::post('get-student-fee-details', 'backend\StudentFeeReceiptController@studentFeeDetail');

        // New Student fee
        Route::get('get-student-fee-details-new', 'backend\StudentFeeReceiptNewController@studentFeeDetail');
        Route::get('fee-installment/edit/', 'backend\StudentFeeReceiptNewController@editFeeInstallment');

        Route::post('update-fee-details', 'backend\StudentFeeReceiptController@studentFeeDetail');
        Route::post('check-session', 'backend\SessionController@checkSession');
        Route::post('set-session', 'backend\SessionController@setSession');
        Route::post('get-section-list', 'backend\SectionController@sectionList');
        Route::post('get-fee-sub-circular', 'backend\FeeController@getFeeSubCircular');
        Route::post('fee-installment/delete/', 'backend\StudentFeeReceiptController@revertPaidFee');
        Route::post('set-class-order', 'backend\ClassesController@setClassOrder');
        Route::post('get-vehicle-provider-driver-helper', 'backend\VehicleController@getVehicleMasterData');
        Route::post('get-student', 'backend\StudentVehicleAssignedController@getStudent');
        Route::post('get-employee', 'backend\StudentVehicleAssignedController@getEmployee');
        Route::post('get-stop-fair', 'backend\StudentVehicleAssignedController@getStopFair');
        Route::post('add-section', 'backend\SectionController@addSection');
        Route::post('transport-send-sms', 'backend\VehicleController@sendTransportSMS');
        Route::post('get-stop-point-list', 'backend\RouteController@getStopPoint');
        Route::post('get-parent', 'backend\StudentController@getParent');
        Route::post('transport-fee-installment/delete/', 'backend\StudentFeeReceiptController@revertTransportPaidFee');
        Route::post('get-session', 'backend\SessionController@getSessionData');

        // subject ajax request
        Route::post('get-subject-group', 'backend\SubjectController@getSubjectGroup');
        Route::post('add-subject-group', 'backend\SubjectController@saveSubjectGroup');
        Route::post('add-subject', 'backend\SubjectController@saveSubject');
        Route::post('edit-subject', 'backend\SubjectController@editSubject');
        Route::post('edit-sub-subject', 'backend\SubjectController@editSubSubject');
        Route::post('get-group-subject-list', 'backend\SubjectController@getGroupList');
        Route::post('save-imported-subject', 'backend\SubjectController@saveImportedSubject');
        Route::post('include-group-in-result', 'backend\SubjectController@includeGroupInResult');
        Route::post('add-sub-subject', 'backend\SubjectController@saveSubSubject');
        Route::post('subject-data-delete', 'backend\SubjectController@destroy');
        Route::post('student-assign-unassign', 'backend\SubjectController@studentUnAssignedSubject');
        Route::post('set-class-subject-order', 'backend\SubjectController@setClassSubjectOrder');
        Route::post('get-class-subject-order', 'backend\SubjectController@getClassSubject');
        Route::post('check-group-name', 'backend\SubjectController@checkGroupName');
        Route::post('check-subject-name', 'backend\SubjectController@checkSubjectName');
        Route::post('get-subject-list', 'backend\SubjectController@getSubjectList');
        Route::post('get-sub-subject-list', 'backend\SubjectController@getSubSubjectList');
        Route::post('get-subject-marks-data', 'backend\ExamClassController@getClassSubjectMark');

        //Route::post('check-exam-section', 'backend\ExamClassController@checkExamSection');
        Route::post('check-sub-subject-name', 'backend\SubjectController@checkSubSubjectName');
        Route::post('get-exam-subject-data', 'backend\StudentMarkController@getStudentSubjectMark');
        Route::post('get-group', 'backend\SubjectController@getGroup');


        Route::post('add-sub-coscholastic', 'backend\CoScholasticController@addSubCoScholastic');
        Route::post('get-marks-grade', 'backend\StudentMarkController@getMarksGrade');
        Route::post('send-unregistered-enquire-sms', 'backend\StudentEnquireController@sendEnquireSMS');
        Route::post('get-scholar-number', 'backend\StudentController@createScholarNo');
        Route::post('get-age', 'backend\StudentController@getAge');
        Route::post('initialize-class-session', 'backend\SubjectController@initializeGroupSession');

        // student detail report
        Route::get('admin/student-detail', 'backend\StudentController@studentDetail');

//        Route::get('admin/attendance-register/{view_type?}', 'backend\StudentController@attendanceRegister');
//        Route::get('attendance_register_summary', 'backend\StudentController@attendanceRegisterSummary');
//        Route::get('attendance-table-header/{view_type?}/{c_date?}', 'backend\StudentController@tableHeaderCol');
//        Route::get('admin/student-attendance/{id?}', 'backend\StudentController@studentAttendanceDetail');
//        Route::get('admin/student-attendance-calendar/{id?}/{start?}/{end?}', 'backend\StudentController@studentAttendanceCalendar');
//        Route::get('admin/student-absence-summary', 'backend\StudentController@studentAbsenceSummary');
//        Route::get('student_absence_summary_data', 'backend\StudentController@studentAbsenceSummaryData');
//        Route::post('attendance_register', 'backend\StudentController@attendanceRegisterReport');
//        Route::get('admin/attendance-summary/{type?}', 'backend\StudentController@studentAttendanceSummary');
//        Route::get('student_attendance_summary_data', 'backend\StudentController@studentAttendanceSummaryData');
        //  upcoming students
        Route::get('admin/coming-soon', 'backend\AdminUserController@upcomingModule');
        Route::get('student_detail', 'backend\StudentController@studentDetailReport');
        // student promote
        Route::get('admin/student-promote', 'backend\StudentController@promoteStudent');
        Route::get('student_promote', 'backend\StudentController@promoteStudentData');
        Route::post('promote-student-save', 'backend\StudentController@promoteStudentSave');
        Route::get('admin/student-progress/{id?}/{section_id?}', 'backend\StudentController@studentProgress');
        Route::get('progress-table-header/{id?}/{section_id?}', 'backend\StudentController@getProgressHeader');
        Route::post('student_progress_report', 'backend\StudentController@studentProgressReport');

        /*
         * Send Due fee SMS 
         */
        Route::post('send-due-fee-sms', 'backend\StudentFeeReceiptController@sendDueFeeSMS');
        /*
         * Send Due fee Notification 
         */
        Route::post('send-due-fee-notification', 'backend\StudentFeeReceiptController@sendDueFeeNotification');

        // student sibling report
        Route::post('get-student-class', 'backend\StudentController@studentClass');
        Route::get('admin/student/sibling/{sibling_student?}', 'backend\StudentController@studentSibling');
        Route::get('student_sibling', 'backend\StudentController@studentSiblingReport');

        // student fee report
        Route::post('get-fee-student', 'backend\StudentFeeReceiptController@studentClass');
        Route::get('admin/student-fee-report', 'backend\StudentFeeReceiptController@studentFeeReport');
        Route::get('student_fee_report_data', 'backend\StudentFeeReceiptController@studentFeeReportData');
        Route::get('student-fee-receipt-print', 'backend\StudentFeeReceiptNewController@printStudentReceipt');

        // student transport fee report
        Route::post('get-fee-student', 'backend\StudentFeeReceiptController@studentClass');
        Route::get('admin/student-transport-fee-report', 'backend\StudentFeeReceiptController@transportFeeReport');
        Route::get('student_transport_fee_report_data', 'backend\StudentFeeReceiptController@transportFeeReportData');

        // enquire report
        Route::get('admin/student-enquire-report', 'backend\StudentEnquireController@studentEnquireReport');
        Route::get('student_enquire_report_data', 'backend\StudentEnquireController@studentEnquireReportData');

        // exam report with marksheet print
        Route::get('admin/exam-report', 'backend\StudentMarkController@examReport');
        Route::get('exam-report-data', 'backend\StudentMarkController@examReportData');
        Route::get('class-section-exam', 'backend\CreateExamController@classSectionExam');
        Route::get('admin/exam-mark-sheet', 'backend\StudentMarkController@examMarksheet');
        Route::get('exam-mark-sheet-data', 'backend\StudentMarkController@examMarksheetData');
        Route::post('print-final-marksheet', 'backend\StudentMarkController@examMarksheetPrint');

        // student daybook report
        Route::get('admin/student/daybook', 'backend\StudentFeeReceiptController@dayBookReport');
        Route::get('daybook-report-data', 'backend\StudentFeeReceiptController@dayBookReportData');

        Route::get('admin/sub-admins','backend\SubAdminController@index')->name('admin.sub-admin.index');
        Route::get('admin/sub-admins/data','backend\SubAdminController@anyData')->name('admin.sub-admin.data');
        Route::get('admin/sub-admin/add/{id?}','backend\SubAdminController@add')->name('admin.sub-admin.add');
        Route::post('admin/sub-admin/save/{id}','backend\SubAdminController@save')->name('admin.sub-admin.save');
        Route::post('admin/sub-admin/delete','backend\SubAdminController@destroy')->name('admin.sub-admin.delete');

        //Route::get('admin/app-access', 'backend\StudentController@resetAppAccessStatus');
        // User Role Module
        Route::get('admin/user-role/{id?}', 'backend\AdminUserController@userRoleAdd');
        Route::post('admin/user-role/save/{id?}', 'backend\AdminUserController@userRoleSave');
        Route::get('user-role/data', 'backend\AdminUserController@userRoleAnyData');
        Route::post('admin/user-role/delete/', 'backend\AdminUserController@userRoleDestroy');

        // Permission Module
        Route::get('admin/permission', 'backend\PermissionController@index');
        // Route::get('admin/permission/add/{id?}', 'backend\PermissionController@add');
        Route::get('admin/permission/save', 'backend\PermissionController@save');
        // Route::get('admin/permission/data', 'backend\PermissionController@anyData');
        // Route::post('admin/permission/delete/', 'backend\PermissionController@destroy');
        // Data for Partial View
        Route::get('admin/permission/view-table', 'backend\PermissionController@viewTable');

        Route::post('check-vehicle-join-date', 'backend\StudentVehicleAssignedController@checkVehicleJoinDate');


        /* Student TC Module */
        Route::get('admin/student-tc/add/{id?}', 'backend\StudentTCController@add');
        Route::post('admin/student-tc/save/{id?}', 'backend\StudentTCController@save');
        Route::get('admin/student-tc', 'backend\StudentTCController@index');
        Route::get('student-tc-data', 'backend\StudentTCController@anyData');
        Route::post('student/delete/', 'backend\StudentController@destroy');
        Route::get('admin/student-tc-print/{id?}', 'backend\StudentTCController@studentTCPrint');
        Route::get('get-student-details', 'backend\StudentTCController@getStudentDetails');

        /*  Student Left Module */
        Route::get('admin/student-left', 'backend\StudentTCController@leftStudents');
        Route::get('student-left-data', 'backend\StudentTCController@leftStudentData');
        Route::get('student-left-data/recall/{id}', 'backend\StudentTCController@recallLeftStudent')->name('student.left.recall');

//        school gallery
        Route::get('admin/school-gallery-album/{id?}', 'backend\SchoolGalleryAlbumController@add');
        Route::post('admin/school-gallery-album/save/{id?}', 'backend\SchoolGalleryAlbumController@save');
        Route::post('school-gallery-album/delete/', 'backend\SchoolGalleryAlbumController@destroy');
        Route::post('admin/import-student', 'backend\StudentController@importStudent');

        // E-Book
        Route::get('admin/e-book', 'backend\EBookController@index');
        Route::get('e-book/get-class-subjects', 'backend\EBookController@getClassSubjects');
        Route::get('e-book/data', 'backend\EBookController@getChapters');
        Route::get('e-book/add-chapter', 'backend\EBookController@addChapter');
        Route::get('e-book/delete-chapter', 'backend\EBookController@deleteChapter');
        Route::get('admin/e-book/topics/{chapter_id}', 'backend\EBookController@viewTopics')->name('view.chapter.topics');
        Route::get('e-book/topics/data', 'backend\EBookController@getChapterTopics');
        Route::post('e-book/add-topic', 'backend\EBookController@addTopic');
        Route::get('e-book/delete-topic', 'backend\EBookController@deleteTopic');
        Route::get('e-book/get-topic', 'backend\EBookController@getTopic');

        // EClass-room
        Route::get('admin/e-class-room', 'backend\EClassRoomController@index');
        Route::get('e-class-room/get-class-data', 'backend\EClassRoomController@getClassData');
        Route::get('e-class-room/save', 'backend\EClassRoomController@save');

        // Admin Users
        Route::get('admin/app-users','backend\AdminUserController@adminUsers');
        Route::get('app-users/data','backend\AdminUserController@adminUsersData');
        Route::post('admin/app-users/update-password','backend\AdminUserController@updateAdminUserPassword');
    });

    
